
#include <gtest/gtest.h>
#include <Visual/UserInterface/Input.h>

class InputTest : public ::testing::Test
{
	protected:
		InputTest( ) = default;
		virtual ~InputTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

TEST( InputTest, TheInputListenerHoldTheExpectedState )
{
	visual::ui::Input input;

	for( int i = 0; i < keysSize; ++i )
	{
		EXPECT_FALSE( input.key( i ) );
	}

	for( int i = 0; i < mouseSize; ++i )
	{
		EXPECT_FALSE( input.button( i ) );
	}
	
	EXPECT_EQ( input.getWheel( ), 0 );
	EXPECT_EQ( input.getXMovement( ), 0 );
	EXPECT_EQ( input.getYMovement( ), 0 );
	EXPECT_EQ( input.getXPosition( ), 0 );
	EXPECT_EQ( input.getYPosition( ), 0 );


	
	input.keyPressed( 'W' );
	input.keyPressed( 'A' );
	input.keyPressed( 'S' );
	input.keyPressed( 'D' );
	
	EXPECT_TRUE( input.key( 'W' ) );
	EXPECT_TRUE( input.key( 'A' ) );
	EXPECT_TRUE( input.key( 'S' ) );
	EXPECT_TRUE( input.key( 'D' ) );
	
	input.keyReleased( 'W' );
	input.keyReleased( 'S' );

	EXPECT_FALSE( input.key( 'W' ) );
	EXPECT_TRUE( input.key( 'A' ) );
	EXPECT_FALSE( input.key( 'S' ) );
	EXPECT_TRUE( input.key( 'D' ) );
	
	
	input.mousePressed( 1 );
	input.mousePressed( 2 );

	EXPECT_TRUE( input.button( 1 ) );
	EXPECT_TRUE( input.button( 2 ) );

	input.mouseReleased( 1 );
	
	EXPECT_FALSE( input.button( 1 ) );
	EXPECT_TRUE( input.button( 2 ) );

	input.readComplete( );
}

  