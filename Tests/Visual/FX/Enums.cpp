
#include <gtest/gtest.h>
#include <Visual/FX/Enums.h>

class EnumsTest : public ::testing::Test
{
	protected:
		EnumsTest( ) = default;
		virtual ~EnumsTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

using visual::fx::ColourFormat;
using visual::fx::MsaaLevel;
using visual::fx::SamplerFilter;
using visual::fx::VSyncState;

using visual::fx::toString;
using visual::fx::parseColourFormat;
using visual::fx::parseVSyncState;
using visual::fx::parseSamplerFilter;
using visual::fx::parseMsaaLevel;

TEST( EnumsTest, EnumsHaveTheirCorrectUsefulValues )
{
	const uint32_t  size8BitColour = 32;
	const uint32_t size16BitColour = 64;
	const uint32_t size32BitColour = 128;
	
	EXPECT_EQ( ColourFormat::RGBA8,   size8BitColour );
	EXPECT_EQ( ColourFormat::RGBA16, size16BitColour );
	EXPECT_EQ( ColourFormat::RGBA32, size32BitColour );

	
	const uint32_t noMsaa = 1;
	const uint32_t msaa2X = 2;
	const uint32_t msaa4X = 4;
	const uint32_t msaa8X = 8;
	
	EXPECT_EQ( MsaaLevel::MSAA_X1, noMsaa );
	EXPECT_EQ( MsaaLevel::MSAA_X2, msaa2X );
	EXPECT_EQ( MsaaLevel::MSAA_X4, msaa4X );
	EXPECT_EQ( MsaaLevel::MSAA_X8, msaa8X );
}

TEST( EnumsTest, EnumsHaveCorraspondingStringValues )
{
	EXPECT_EQ( toString( ColourFormat::RGBA8  ), "8bit" );
	EXPECT_EQ( toString( ColourFormat::RGBA16 ), "16bit" );
	EXPECT_EQ( toString( ColourFormat::RGBA32 ), "32bit" );

	EXPECT_EQ( parseColourFormat( "8bit"  ), ColourFormat::RGBA8  );
	EXPECT_EQ( parseColourFormat( "16bit" ), ColourFormat::RGBA16 );
	EXPECT_EQ( parseColourFormat( "32bit" ), ColourFormat::RGBA32 );

	EXPECT_EQ( toString( VSyncState::VSYNC_OFF  ), "off"  );
	EXPECT_EQ( toString( VSyncState::VSYNC_ON   ), "on"   );
	EXPECT_EQ( toString( VSyncState::VSYNC_FAST ), "fast" );

	EXPECT_EQ( parseVSyncState( "off"  ), VSyncState::VSYNC_OFF  );
	EXPECT_EQ( parseVSyncState( "on"   ), VSyncState::VSYNC_ON   );
	EXPECT_EQ( parseVSyncState( "fast" ), VSyncState::VSYNC_FAST );

	EXPECT_EQ( toString( SamplerFilter::ANISOTROPIC ), "anisotropic" );
	EXPECT_EQ( toString( SamplerFilter::LINEAR      ), "linear"      );
	EXPECT_EQ( toString( SamplerFilter::POINT       ), "point"       );

	EXPECT_EQ( parseSamplerFilter( "anisotropic" ), SamplerFilter::ANISOTROPIC );
	EXPECT_EQ( parseSamplerFilter( "linear"      ), SamplerFilter::LINEAR      );
	EXPECT_EQ( parseSamplerFilter( "point"       ), SamplerFilter::POINT       );


	EXPECT_EQ( parseMsaaLevel( 1 ), MsaaLevel::MSAA_X1 );
	EXPECT_EQ( parseMsaaLevel( 2 ), MsaaLevel::MSAA_X2 );
	EXPECT_EQ( parseMsaaLevel( 4 ), MsaaLevel::MSAA_X4 );
	EXPECT_EQ( parseMsaaLevel( 8 ), MsaaLevel::MSAA_X8 );
	EXPECT_EQ( parseMsaaLevel( 3 ), MsaaLevel::MSAA_X1 );
}

  