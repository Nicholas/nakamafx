
#include <gtest/gtest.h>
#include <Visual/FX/Settings.h>

class SettingsTest : public ::testing::Test
{
	protected:
		SettingsTest( ) = default;
		virtual ~SettingsTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

TEST( SettingsTest, CorrectsInvalidCPULoadBalancing )
{
	const std::string systemName = "Test Settings";
	const int numberOfThreads = 2;


	visual::fx::Settings settings( systemName );
	settings.setNumberOfWorkerThreads( numberOfThreads );


	
	EXPECT_EQ( settings.getSystemName( ), systemName );
	EXPECT_EQ( settings.getNumberOfWorkerThreads( ), numberOfThreads );
	

	settings.setMaximumAsyncJobsToExecute( 3 );
	settings.setMaximumSyncJobsToExecute( 3 );

	settings.setMinimumAsyncJobsToExecute( 3 );
	settings.setMinimumSyncJobsToExecute( 3 );

	EXPECT_EQ( settings.getMaximumAsyncJobsToExecute( ), numberOfThreads );
	EXPECT_EQ( settings.getMaximumSyncJobsToExecute( ), numberOfThreads );

	EXPECT_EQ( settings.getMinimumAsyncJobsToExecute( ), numberOfThreads );
	EXPECT_EQ( settings.getMinimumSyncJobsToExecute( ), numberOfThreads );
	
	settings.setMaximumAsyncJobsToExecute( 1 );
	settings.setMaximumSyncJobsToExecute( 1 );

	settings.setMinimumAsyncJobsToExecute( 3 );
	settings.setMinimumSyncJobsToExecute( 3 );

	EXPECT_EQ( settings.getMaximumAsyncJobsToExecute( ), 1 );
	EXPECT_EQ( settings.getMaximumSyncJobsToExecute( ), 1 );

	EXPECT_EQ( settings.getMinimumAsyncJobsToExecute( ), 1 );
	EXPECT_EQ( settings.getMinimumSyncJobsToExecute( ), 1 );

	
	settings.setNumberOfWorkerThreads( 5 );
	
	settings.setMaximumAsyncJobsToExecute( 5 );
	settings.setMaximumSyncJobsToExecute( 5 );
	
	settings.setMinimumAsyncJobsToExecute( 5 );
	settings.setMinimumSyncJobsToExecute( 5 );
	
	settings.setMaximumAsyncJobsToExecute( 3 );
	settings.setMaximumSyncJobsToExecute( 2 );

	EXPECT_EQ( settings.getMaximumAsyncJobsToExecute( ), 3 );
	EXPECT_EQ( settings.getMaximumSyncJobsToExecute( ), 2 );

	EXPECT_EQ( settings.getMinimumAsyncJobsToExecute( ), 3 );
	EXPECT_EQ( settings.getMinimumSyncJobsToExecute( ), 2 );

	
	settings.setNumberOfWorkerThreads( 1 );

	EXPECT_EQ( settings.getMaximumAsyncJobsToExecute( ), 1 );
	EXPECT_EQ( settings.getMaximumSyncJobsToExecute( ), 1 );

	EXPECT_EQ( settings.getMinimumAsyncJobsToExecute( ), 1 );
	EXPECT_EQ( settings.getMinimumSyncJobsToExecute( ), 1 );
}

  