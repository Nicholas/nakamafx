
#include <gtest/gtest.h>
#include <Visual/FX/Shapes.h>

class ShapesTest : public ::testing::Test
{
	protected:
		ShapesTest( ) = default;
		virtual ~ShapesTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

TEST( ShapesTest, FillsAVertexArrayWithCubeVertices )
{
	visual::fx::Vertex3f4f::Vertex cube[ 36 ];

	visual::fx::Shapes::fillCube( cube );
	
	EXPECT_EQ( cube[  0 ].position, glm::vec3( -1, -1,  1 ) );
	EXPECT_EQ( cube[  1 ].position, glm::vec3( -1,  1,  1 ) );
	EXPECT_EQ( cube[  2 ].position, glm::vec3(  1, -1,  1 ) );
	EXPECT_EQ( cube[  3 ].position, glm::vec3(  1, -1,  1 ) );
	EXPECT_EQ( cube[  4 ].position, glm::vec3( -1,  1,  1 ) );
	EXPECT_EQ( cube[  5 ].position, glm::vec3(  1,  1,  1 ) );
	
	EXPECT_EQ( cube[  6 ].position, glm::vec3( -1, -1, -1 ) );
	EXPECT_EQ( cube[  7 ].position, glm::vec3(  1, -1, -1 ) );
	EXPECT_EQ( cube[  8 ].position, glm::vec3( -1,  1, -1 ) );
	EXPECT_EQ( cube[  9 ].position, glm::vec3( -1,  1, -1 ) );
	EXPECT_EQ( cube[ 10 ].position, glm::vec3(  1, -1, -1 ) );
	EXPECT_EQ( cube[ 11 ].position, glm::vec3(  1,  1, -1 ) );
	
	EXPECT_EQ( cube[ 12 ].position, glm::vec3( -1,  1,  1 ) );
	EXPECT_EQ( cube[ 13 ].position, glm::vec3( -1, -1,  1 ) );
	EXPECT_EQ( cube[ 14 ].position, glm::vec3( -1,  1, -1 ) );
	EXPECT_EQ( cube[ 15 ].position, glm::vec3( -1,  1, -1 ) );
	EXPECT_EQ( cube[ 16 ].position, glm::vec3( -1, -1,  1 ) );
	EXPECT_EQ( cube[ 17 ].position, glm::vec3( -1, -1, -1 ) );
	
	EXPECT_EQ( cube[ 18 ].position, glm::vec3(  1,  1,  1 ) );
	EXPECT_EQ( cube[ 19 ].position, glm::vec3(  1,  1, -1 ) );
	EXPECT_EQ( cube[ 20 ].position, glm::vec3(  1, -1,  1 ) );
	EXPECT_EQ( cube[ 21 ].position, glm::vec3(  1, -1,  1 ) );
	EXPECT_EQ( cube[ 22 ].position, glm::vec3(  1,  1, -1 ) );
	EXPECT_EQ( cube[ 23 ].position, glm::vec3(  1, -1, -1 ) );
	
	EXPECT_EQ( cube[ 24 ].position, glm::vec3(  1,  1,  1 ) );
	EXPECT_EQ( cube[ 25 ].position, glm::vec3( -1,  1,  1 ) );
	EXPECT_EQ( cube[ 26 ].position, glm::vec3(  1,  1, -1 ) );
	EXPECT_EQ( cube[ 27 ].position, glm::vec3(  1,  1, -1 ) );
	EXPECT_EQ( cube[ 28 ].position, glm::vec3( -1,  1,  1 ) );
	EXPECT_EQ( cube[ 29 ].position, glm::vec3( -1,  1, -1 ) );
	
	EXPECT_EQ( cube[ 30 ].position, glm::vec3(  1, -1,  1 ) );
	EXPECT_EQ( cube[ 31 ].position, glm::vec3(  1, -1, -1 ) );
	EXPECT_EQ( cube[ 32 ].position, glm::vec3( -1, -1,  1 ) );
	EXPECT_EQ( cube[ 33 ].position, glm::vec3( -1, -1,  1 ) );
	EXPECT_EQ( cube[ 34 ].position, glm::vec3(  1, -1, -1 ) );
	EXPECT_EQ( cube[ 35 ].position, glm::vec3( -1, -1, -1 ) );
}

 
TEST( ShapesTest, BuildsABasicGrid )
{
	float expectedHeights[] = {
	
		1.0f,
		2.0f,
		3.0f,
		4.0f
	};


	visual::fx::Mesh< visual::fx::Vertex3f2f3f3f3f::Vertex > grid =
		visual::fx::Shapes::grid( 2, 2, 2, [ &expectedHeights ]( const int index ){ return expectedHeights[ index ]; } );

	
	
	ASSERT_EQ( grid.getVertexBuffer( ).size( ), 4 );
	EXPECT_EQ( grid.getVertexBuffer( )[ 0 ].position, glm::vec3( -1,  expectedHeights[ 0 ], -1 ) );
	EXPECT_EQ( grid.getVertexBuffer( )[ 1 ].position, glm::vec3(  1,  expectedHeights[ 1 ], -1 ) );
	EXPECT_EQ( grid.getVertexBuffer( )[ 2 ].position, glm::vec3( -1,  expectedHeights[ 2 ],  1 ) );
	EXPECT_EQ( grid.getVertexBuffer( )[ 3 ].position, glm::vec3(  1,  expectedHeights[ 3 ],  1 ) );
}
