
#include <gtest/gtest.h>
#include <Visual/FX/Camera/ChaseRig.h>
#include <Visual/FX/Camera/Camera.h>
#include <Visual/UserInterface/Input.h>
#include <Macros.h>
#include <glm/glm.hpp>

class ChaseRigTest : public ::testing::Test
{
	protected:
		ChaseRigTest( ) = default;
		virtual ~ChaseRigTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

namespace visual
{
namespace ui
{
	SHARED_PTR_TYPE_DEF( Input );
}
namespace fx
{
	SHARED_PTR_TYPE_DEF( Camera );
}
}

TEST( ChaseRigTest, MovesForward )
{
	glm::vec3 position( 0, 0, 0 );
	float distance = 0;
	visual::fx::CameraSPtr pCamera =
		std::make_shared< visual::fx::Camera >( 100, 100, 0.01f, 1000.0f, 85.0f );
	visual::ui::InputSPtr pInput =
		std::make_shared< visual::ui::Input >( );

	visual::fx::ChaseRig rig( { 0, 0, 0 }, 0, pCamera, pInput );
	
	pCamera->getEye( ) = { 0, 0, 0 };
	pCamera->getAt( )  = { 0, 0, 1 };
	pCamera->getUp( )  = { 0, 1, 0 };

	pCamera->update( );

	EXPECT_FLOAT_EQ( pCamera->getEye( ).x, 0 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).y, 0 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).z, 0 );

	rig.move( 10 );
	rig.updatePosition( 0 );

	EXPECT_FLOAT_EQ( pCamera->getEye( ).x, 0 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).y, 0 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).z, 10 );

	rig.strafe( 10 );
	rig.updatePosition( 0 );

	EXPECT_FLOAT_EQ( pCamera->getEye( ).x, 10 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).y, 0 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).z, 10 );

	rig.climb( 10 );
	rig.updatePosition( 0 );

	EXPECT_FLOAT_EQ( pCamera->getEye( ).x, 10 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).y, 10 );
	EXPECT_FLOAT_EQ( pCamera->getEye( ).z, 10 );
}

  