
#include <gtest/gtest.h>
#include <Visual/FX/Camera/Projection.h>
#include <glm/ext.hpp>

class ProjectionTest : public ::testing::Test
{
	protected:
		ProjectionTest( ) = default;
		virtual ~ProjectionTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

TEST( ProjectionTest, CreatesALeftHandedPerspectiveProjection )
{
	glm::mat4x4 expected = glm::perspectiveFovLH( glm::radians( 50.0f ), 100.0f, 100.0f, 1.0f, 10.0f );
			    expected = glm::scale( expected, { 1, 1, 0.5 } );
	const visual::fx::Projection actual( 100, 100, 1, 10, 50 );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].x, expected[ 0 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].y, expected[ 0 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].z, expected[ 0 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].w, expected[ 0 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].x, expected[ 1 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].y, expected[ 1 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].z, expected[ 1 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].w, expected[ 1 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].x, expected[ 2 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].y, expected[ 2 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].z, expected[ 2 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].w, expected[ 2 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].x, expected[ 3 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].y, expected[ 3 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].z, expected[ 3 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].w, expected[ 3 ].w );
}

TEST( ProjectionTest, CreatesALeftHandedOrthographicProjection )
{
	glm::mat4x4 expected = glm::orthoLH( -50.0f, 50.0f, -50.0f, 50.0f, 1.0f, 10.0f );
			    expected = glm::scale( expected, { 1, 1, 0.5 } );
	const visual::fx::Projection actual( 100, 100, 1, 10, 0 );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].x, expected[ 0 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].y, expected[ 0 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].z, expected[ 0 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 0 ].w, expected[ 0 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].x, expected[ 1 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].y, expected[ 1 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].z, expected[ 1 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 1 ].w, expected[ 1 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].x, expected[ 2 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].y, expected[ 2 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].z, expected[ 2 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 2 ].w, expected[ 2 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].x, expected[ 3 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].y, expected[ 3 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].z, expected[ 3 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( )[ 3 ].w, expected[ 3 ].w );
}

  