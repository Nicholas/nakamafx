
#include <gtest/gtest.h>
#include <Visual/FX/Camera/Camera.h>
#include <glm/ext.hpp>

class CameraTest : public ::testing::Test
{
	protected:
		CameraTest( ) = default;
		virtual ~CameraTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

TEST( CameraTest, CreatesACAmeraWithTheCorrectProjection )
{
	glm::mat4x4 expected = glm::perspectiveFovLH( glm::radians( 50.0f ), 100.0f, 100.0f, 1.0f, 10.0f );
			    expected = glm::scale( expected, { 1, 1, 0.5 } );
	const visual::fx::Camera actual( 100, 100, 1, 10, 50 );
	
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 0 ].x, expected[ 0 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 0 ].y, expected[ 0 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 0 ].z, expected[ 0 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 0 ].w, expected[ 0 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 1 ].x, expected[ 1 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 1 ].y, expected[ 1 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 1 ].z, expected[ 1 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 1 ].w, expected[ 1 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 2 ].x, expected[ 2 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 2 ].y, expected[ 2 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 2 ].z, expected[ 2 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 2 ].w, expected[ 2 ].w );
	
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 3 ].x, expected[ 3 ].x );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 3 ].y, expected[ 3 ].y );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 3 ].z, expected[ 3 ].z );
	EXPECT_FLOAT_EQ( actual.getProjection( 0 )[ 3 ].w, expected[ 3 ].w );
}

TEST( CameraTest, UpdateModifiesTheViewMatrix )
{
	visual::fx::Camera actual( 100, 100, 1, 10, 50 );

	//actual.update( );
	
	actual.getEye( ) = glm::vec3( 0, 0, 0 );
	actual.getAt( ) = glm::vec3( 0, 0, 1 );
	actual.getUp( ) = glm::vec3( 0, 1, 0 );

	actual.update( );
	
	EXPECT_FLOAT_EQ( actual.getView( )[ 0 ].x, 1 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 0 ].y, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 0 ].z, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 0 ].w, 0 );

	EXPECT_FLOAT_EQ( actual.getView( )[ 1 ].x, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 1 ].y, 1 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 1 ].z, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 1 ].w, 0 );

	EXPECT_FLOAT_EQ( actual.getView( )[ 2 ].x, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 2 ].y, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 2 ].z, 1 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 2 ].w, 0 );

	EXPECT_FLOAT_EQ( actual.getView( )[ 3 ].x, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 3 ].y, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 3 ].z, 0 );
	EXPECT_FLOAT_EQ( actual.getView( )[ 3 ].w, 1 );
}
  