
#include <gtest/gtest.h>
#include <Visual/FX/Maths.h>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>

class MathsTest : public ::testing::Test
{
	protected:
		MathsTest( ) = default;
		virtual ~MathsTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

using glm::vec3;
using glm::vec2;
using glm::mat4x4;
using glm::two_pi;
using glm::orthoLH;
using glm::identity;
using visual::fx::Maths;

TEST( MathsTest, CreatesARandomUintVector )
{
	const vec3 actual = Maths::randomUnitVec3( );
	
	EXPECT_TRUE( actual.x >= -1.0f );
	EXPECT_TRUE( actual.x <=  1.0f );
	EXPECT_TRUE( actual.y >= -1.0f );
	EXPECT_TRUE( actual.y <=  1.0f );
	EXPECT_TRUE( actual.z >= -1.0f );
	EXPECT_TRUE( actual.z <=  1.0f );
}

TEST( MathsTest, LimitsAnAngleTo2PiRange )
{
	float actual = two_pi< float >( );

	Maths::limitAngle( actual );
	
	EXPECT_FLOAT_EQ( actual, two_pi< float >( ) );
	
	actual = two_pi< float >( ) * 0.5f;
	Maths::limitAngle( actual );

	EXPECT_FLOAT_EQ( actual, two_pi< float >( ) * 0.5f );
	
	actual = two_pi< float >( ) * -0.5f;
	Maths::limitAngle( actual );

	EXPECT_FLOAT_EQ( actual, two_pi< float >( ) * 0.5f );
	
	actual = -two_pi< float >( );
	Maths::limitAngle( actual );

	EXPECT_FLOAT_EQ( actual, 0 );
	
	actual = two_pi< float >( ) * 2;
	Maths::limitAngle( actual );

	EXPECT_FLOAT_EQ( actual, two_pi< float >( ) );
}

TEST( MathsTest, AccumulatesSurfaceNormalsForSmoothLighting )
{
	vec3 expected( 1.0f, 1.0f, 0.0f );

	vec3 actual( 0, 0 , 0 );

	vec3 p11( 0, 0, 0 );
	vec3 p12( 0, 1, 0 );
	vec3 p13( 0, 0, 1 );

	vec3 p21( 1, 0, 0 );
	vec3 p22( 0, 0, 0 );
	vec3 p23( 0, 0, 1 );
	
	Maths::appendNormal( actual, p11, p12, p13 );
	Maths::appendNormal( actual, p21, p22, p23 );

	
	EXPECT_FLOAT_EQ( actual.x, expected.x );
	EXPECT_FLOAT_EQ( actual.y, expected.y );
	EXPECT_FLOAT_EQ( actual.z, expected.z );
}

TEST( MathsTest, CalculatesTangents )
{
	vec3 expectedTangent( 1.0f, 0.0f, 0.0f );
	vec3 expectedBitangent( 0.0f, 1.0f, 0.0f );
	
	vec3 actualTangent( 0, 0 , 0 );
	vec3 actualBitangent( 0, 0 , 0 );

	vec3 p1( 1, 0, 0 );
	vec3 p2( 0, 1, 0 );
	vec3 p3( 0, 0, 0 );

	vec2 uv1( 1, 0 );
	vec2 uv2( 0, 1 );
	vec2 uv3( 0, 0 );
	
	Maths::calculateTangents( actualTangent, actualBitangent, p1, p2, p3, uv1, uv2, uv3 );
	
	EXPECT_FLOAT_EQ( actualTangent.x, expectedTangent.x );
	EXPECT_FLOAT_EQ( actualTangent.y, expectedTangent.y );
	EXPECT_FLOAT_EQ( actualTangent.z, expectedTangent.z );
	
	EXPECT_FLOAT_EQ( actualBitangent.x, expectedBitangent.x );
	EXPECT_FLOAT_EQ( actualBitangent.y, expectedBitangent.y );
	EXPECT_FLOAT_EQ( actualBitangent.z, expectedBitangent.z );
}

TEST( MathsTest, CalculatesTheMouseRay )
{
	vec3 actual;
	const float x = 50;
	const float y = 50;
	const float width = 100;
	const float height = 100;
	const mat4x4 projection = orthoLH( 0.0f, width, 0.0f, height, 0.01f, 1.0f );
	const mat4x4 view = identity< mat4x4 >( );
	
	Maths::mouseRay( actual, x, y, width, height, projection, view );
	
	EXPECT_FLOAT_EQ( actual.x, 0.0f );
	EXPECT_FLOAT_EQ( actual.y, 0.0f );
	EXPECT_FLOAT_EQ( actual.z, 1.0f );
}

TEST( MathsTest, CalculatesPlaneIntersection )
{
	vec3 actual;
	const vec3 ray( 0, -1, 0 );
	const vec3 position( 0, 1, 0 );
	const vec3 point( 0, 0, 0 );
	const vec3 normal( 0, 1, 0);
	
	Maths::planeIntersection( actual, ray, position, point, normal );
	
	EXPECT_FLOAT_EQ( actual.x, 0.0f );
	EXPECT_FLOAT_EQ( actual.y, 0.0f );
	EXPECT_FLOAT_EQ( actual.z, 0.0f );
}
