
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <Visual/FX/Loaders/SceneParser.h>
#include <Visual/FX/Loaders/SceneBuilder.h>
#include <Visual/FX/Types/ConstantBuffers.h>
#include <Visual/FX/Types/Light.h>
#include <Macros.h>
#include <filesystem>


constexpr auto _FS_SLASH{ '/' };
constexpr auto _FS_BSLASH{ '\\' };


class SceneParserTest : public ::testing::Test
{
	protected:
		SceneParserTest( ) = default;
		virtual ~SceneParserTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

using tools::Scheduler;
using tools::almostEqual;
using visual::fx::SceneParser;
using visual::fx::SceneBuilder;
using visual::fx::LightEx;
using visual::fx::Mesh;
using visual::fx::Vertex3f2f3f3f3f;
using visual::fx::VertexParticle;
using visual::fx::GSParticleBuffer;
using visual::fx::PSParticleBuffer;
using visual::fx::PSObjectBuffer;
using visual::fx::LightEx;
using visual::fx::MeshPrimitive;
using visual::fx::PSFontBuffer;
using glm::uvec4;
using glm::vec4;
using glm::vec3;
using glm::vec2;
using glm::uvec3;
using glm::ivec3;
using glm::ivec2;
using std::vector;
using std::ostream;
using std::string;
using std::function;
using std::make_shared;
using std::shared_ptr;
using std::string;
using testing::_;
using std::filesystem::path;

namespace glm
{
	// If you can't declare the function in the class it's important that the
	// << operator is defined in the SAME namespace that defines Bar.  C++'s look-up
	// rules rely on that.
	//std::ostream& operator<<(std::ostream& os, const vec3& vec)
	//{
	//	return os << "[ " << vec.x << ", " << vec.y << ", "  << vec.z << " ]";
	//}

	void PrintTo(const ivec3& vec, ostream* os)
	{
		*os << "[ " << vec.x << ", " << vec.y << ", "  << vec.z << " ]";
	}

	void PrintTo(const uvec3& vec, ostream* os)
	{
		*os << "[ " << vec.x << ", " << vec.y << ", "  << vec.z << " ]";
	}

	void PrintTo(const vec3& vec, ostream* os)
	{
		*os << "[ " << vec.x << ", " << vec.y << ", "  << vec.z << " ]";
	}

	void PrintTo(const vec2& vec, ostream* os)
	{
		*os << "[ " << vec.x << ", " << vec.y << " ]";
	}
}

namespace visual
{
namespace fx
{
	void PrintTo(const PSObjectBuffer& material, ostream* os)
	{
		*os << "Material";
		//*os << " kA[ " << material.ka.x << ", " << material.ka.y << ", "  << material.ka.z << " ]";
		//*os << " kD[ " << material.kd.x << ", " << material.kd.y << ", "  << material.kd.z << " ]";
		//*os << " kS[ " << material.ks.x << ", " << material.ks.y << ", "  << material.ks.z << " ]";
		//*os << " R[ " << material.roughness << " ]";
		//*os << " M[ " << material.metalness << " ]";
		//*os << " B[ " << material.bumpScale.x << ", " << material.bumpScale.y << " ]";
		//*os << " I[ " << material.ior << " ]";
		//*os << " A[ " << material.sAlpha << " ]";
		//*os << " ssA[ " << material.ssAlpha << " ]";
		//*os << " ssP[ " << material.ssPower << " ]";
		//*os << " ssS[ " << material.ssScale << " ]";
		//*os << " ssB[ " << material.ssBump << " ]";
		//*os << " abs[ " << material.absorption << " ]";
	}

	void PrintTo(const Mesh< Vertex3f2f3f3f3f::Vertex >& geometry, ostream* os)
	{
		*os << "Geometry";
		*os << " [ " << geometry.getVertexBuffer( ).size( ) << ", " << geometry.getIndexBuffer( ).size( ) << " ]";
	}
}
}

namespace
{
	class MockBuilder : public SceneBuilder
	{
		public:
			MockBuilder( ) : SceneBuilder( ) { }

			MOCK_METHOD3( addSphere, void( const vec3& position, const float& radius,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD5( addPbrSphere, void(
					const vec3& position,
					const float& radius,
					const vector< string >& textures,
					const PSObjectBuffer& psProperties,
					const bool& forceSRGB ) );

			MOCK_METHOD3( addPlanet, void(
					const vec3& position,
					const float& radius,
					const vector< string >& textures ) );

			MOCK_METHOD3( addMoon, void(
					const vec3& position,
					const float& radius,
					const vector< string >& textures ) );

			MOCK_METHOD6( addHeightMap, void(
					const string& fileName,
					const vec3& position,
					const vec3& scale,
					const ivec2& size,
					const Textures& textures,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD4( addPCGHeightMap, void(
					const vec3& scale,
					const ivec2& size,
					const Textures& textures,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD5( addGsCube, void(
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD5( addCube, void(
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD7( addMesh, void(
					const string& id,
					const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD7( addBasicMesh, void(
					const string& id,
					const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD7( addClippedMesh, void(
					const string& id,
					const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& psProperties ) );

			MOCK_METHOD7( addTranslucentMesh, void(
					const string& id,
					const Mesh< visual::fx::Vertex3f2f3f3f3f::Vertex >& mesh,
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& material ) );

			MOCK_METHOD5( addWaterPlane, void(
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& material ) );

			MOCK_METHOD6( addPCGWaterPlane, void(
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSObjectBuffer& material, bool follow ) );

			MOCK_METHOD2( addPolarSkybox, void(
					const vector< string >& textures,
					const vec3& direction ) );

			MOCK_METHOD2( addCubeSkybox, void(
					const vector< string >& textures,
					const vec3& direction ) );

			MOCK_METHOD2( addBasicVolumeSkybox, void(
					const vector< string >& textures,
					const vec3& direction ) );

			MOCK_METHOD6( addParticleSystem, void(
					const Mesh< VertexParticle::Vertex >::VertexBuffer& initialState,
					const vector< string >& textures,
					const GSParticleBuffer& gsProperties,
					const PSParticleBuffer& psProperties,
					const unsigned int bufferSize,
					const string& blend ) );

			MOCK_METHOD7( addText3D, void(
					const string& font,
					const string& text,
					const vec3& position,
					const vec3& rotation,
					const vec3& scale,
					const vector< string >& textures,
					const PSFontBuffer& material ) );

			MOCK_METHOD7( addText2D, void(
					const string& font,
					const string& text,
					const vec2& position,
					const vec2& rotation,
					const vec2& scale,
					const vector< string >& textures,
					const PSFontBuffer& material ) );

			MOCK_METHOD5( addFog, void(
					float density,
					float heightFalloff,
					float rayDepth,
					float gScatter,
					float waterHeight ) );

			MOCK_METHOD1( addLight, void( const LightEx& light ) );

			MOCK_METHOD0( bake, void( void ) );
	};


#ifndef CUSTOM_TEST
#define CUSTOM_TEST( RESULT, MESSAGE ) if( ! ( RESULT ) ){ *result_listener << MESSAGE; return false; }
#endif

#ifndef PRINT_VEC_4
#define PRINT_VEC_4( VEC ) "[ " << VEC.x << ", " << VEC.y << ", " << VEC.z << ", " << VEC.w << " ]"
#endif

#ifndef PRINT_VEC_3
#define PRINT_VEC_3( VEC ) "[ " << VEC.x << ", " << VEC.y << ", " << VEC.z << " ]"
#endif

#ifndef PRINT_VEC_2
#define PRINT_VEC_2( VEC ) "[ " << VEC.x << ", " << VEC.y << " ]"
#endif

#ifndef CUSTOM_TESTER_1
#define CUSTOM_TESTER_1( LHS, RHS, MESSAGE ) \
	if( LHS != RHS ) \
	{ \
		*result_listener << MESSAGE << " Differs ( " << LHS << " != " << RHS << " )";\
		return false;\
	}
#endif

#ifndef CUSTOM_TESTER_2
#define CUSTOM_TESTER_2( LHS, RHS, MESSAGE ) \
	if( LHS != RHS ) \
	{ \
		*result_listener << MESSAGE << " Differs ( " << PRINT_VEC_2( LHS ) << " != " << PRINT_VEC_2( RHS ) << " )";\
		return false;\
	}
#endif

#ifndef CUSTOM_TESTER_3
#define CUSTOM_TESTER_3( LHS, RHS, MESSAGE ) \
	if( LHS != RHS ) \
	{ \
		*result_listener << MESSAGE << " Differs ( " << PRINT_VEC_3( LHS ) << " != " << PRINT_VEC_3( RHS ) << " )";\
		return false;\
	}
#endif

#ifndef CUSTOM_TESTER_4
#define CUSTOM_TESTER_4( LHS, RHS, MESSAGE ) \
	if( LHS != RHS ) \
	{ \
		*result_listener << MESSAGE << " Differs ( " << PRINT_VEC_4( LHS ) << " != " << PRINT_VEC_4( RHS ) << " )";\
		return false;\
	}
#endif

#ifndef CUSTOM_TESTER_H_1
#define CUSTOM_TESTER_H_1( FIELD, MESSAGE ) \
	CUSTOM_TESTER_1( PPCAT( arg., FIELD ), PPCAT( p0., FIELD ), MESSAGE )
#endif

#ifndef CUSTOM_TESTER_H_2
#define CUSTOM_TESTER_H_2( FIELD, MESSAGE ) \
	CUSTOM_TESTER_2( PPCAT( arg., FIELD ), PPCAT( p0., FIELD ), MESSAGE )
#endif

#ifndef CUSTOM_TESTER_H_3
#define CUSTOM_TESTER_H_3( FIELD, MESSAGE ) \
	CUSTOM_TESTER_3( PPCAT( arg., FIELD ), PPCAT( p0., FIELD ), MESSAGE )
#endif

#ifndef CUSTOM_TESTER_H_4
#define CUSTOM_TESTER_H_4( FIELD, MESSAGE ) \
	CUSTOM_TESTER_4( PPCAT( arg., FIELD ), PPCAT( p0., FIELD ), MESSAGE )
#endif

	MATCHER_P(MaterialIsEqual, p0, "Object Material")
	{
		CUSTOM_TEST( arg.ka == p0.ka, "kA [ " << arg.ka.x << ", " << arg.ka.y << ", " << arg.ka.z << " ] != [ " << p0.ka.x << ", " << p0.ka.y << ", " << p0.ka.z << " ]" )
		CUSTOM_TEST( arg.kd == p0.kd, "kD [ " << arg.kd.x << ", " << arg.kd.y << ", " << arg.kd.z << " ] != [ " << p0.kd.x << ", " << p0.kd.y << ", " << p0.kd.z << " ]" )
		CUSTOM_TEST( arg.ks == p0.ks, "kS [ " << arg.ks.x << ", " << arg.ks.y << ", " << arg.ks.z << " ] != [ " << p0.ks.x << ", " << p0.ks.y << ", " << p0.ks.z << " ]" )
		CUSTOM_TEST( arg.bumpScale == p0.bumpScale, "Bump Scale [ " << arg.bumpScale.x << ", " << arg.bumpScale.y << " ] != [ " << p0.bumpScale.x << ", " << p0.bumpScale.y << " ]" )
		CUSTOM_TEST( arg.roughness == p0.roughness, "Roughness [ " << arg.roughness << " ] != [ " << p0.roughness << " ]" )
		CUSTOM_TEST( arg.metalness == p0.metalness, "Metalness [ " << arg.metalness << " ] != [ " << p0.metalness << " ]" )
		CUSTOM_TEST( arg.ior == p0.ior, "IOR [ " << arg.ior << "] != [ " << p0.ior << " ]" )
		CUSTOM_TEST( arg.sAlpha == p0.sAlpha, "sAlpha [ " << arg.sAlpha << " ] != [ " << p0.sAlpha << " ]" )
		CUSTOM_TEST( arg.ssAlpha == p0.ssAlpha, "ssAlpha [ " << arg.ssAlpha << " ] != [ " << p0.ssAlpha << " ]" )
		CUSTOM_TEST( arg.ssPower == p0.ssPower, "ssPower [ " << arg.ssPower << " ] != [ " << p0.ssPower << " ]" )
		CUSTOM_TEST( arg.ssScale == p0.ssScale, "ssScale [ " << arg.ssScale << " ] != [ " << p0.ssScale << " ]" )
		CUSTOM_TEST( arg.ssBump == p0.ssBump, "ssBump [ " << arg.ssBump << " ] != [ " << p0.ssBump << " ]" )
		CUSTOM_TEST( arg.absorption == p0.absorption, "ssScale [ " << arg.absorption << " ] != [ " << p0.absorption << " ]" )

		return true;

		//return (arg.ka == p0.ka)
		//	&& (arg.kd == p0.kd)
		//	&& (arg.ks == p0.ks)
		//	&& (arg.bumpScale == p0.bumpScale)
		//	&& almostEqual(arg.roughness, p0.roughness)
		//	&& almostEqual(arg.metalness, p0.metalness)
		//	&& almostEqual(arg.ior, p0.ior)
		//	&& almostEqual(arg.sAlpha, p0.sAlpha)
		//	&& almostEqual(arg.ssAlpha, p0.ssAlpha)
		//	&& almostEqual(arg.ssPower, p0.ssPower)
		//	&& almostEqual(arg.ssScale, p0.ssScale)
		//	&& almostEqual(arg.ssBump, p0.ssBump)
		//	&& almostEqual(arg.absorption, p0.absorption);
	}

	MATCHER_P(FontMaterialIsEqual, p0, "Font Material")
	{
		CUSTOM_TEST( arg.ka == p0.ka, "kA [ " << arg.ka.x << ", " << arg.ka.y << ", " << arg.ka.z << " ] != [ " << p0.ka.x << ", " << p0.ka.y << ", " << p0.ka.z << " ]" )
		CUSTOM_TEST( arg.kd == p0.kd, "kD [ " << arg.kd.x << ", " << arg.kd.y << ", " << arg.kd.z << " ] != [ " << p0.kd.x << ", " << p0.kd.y << ", " << p0.kd.z << " ]" )
		CUSTOM_TEST( arg.ks == p0.ks, "kS [ " << arg.ks.x << ", " << arg.ks.y << ", " << arg.ks.z << " ] != [ " << p0.ks.x << ", " << p0.ks.y << ", " << p0.ks.z << " ]" )
		CUSTOM_TEST( arg.bumpScale == p0.bumpScale, "Bump Scale [ " << arg.bumpScale.x << ", " << arg.bumpScale.y << " ] != [ " << p0.bumpScale.x << ", " << p0.bumpScale.y << " ]" )
		CUSTOM_TEST( arg.roughness == p0.roughness, "Roughness [ " << arg.roughness << " ] != [ " << p0.roughness << " ]" )
		CUSTOM_TEST( arg.metalness == p0.metalness, "Metalness [ " << arg.metalness << " ] != [ " << p0.metalness << " ]" )
		CUSTOM_TEST( arg.refraction == p0.refraction, "IOR [ " << arg.refraction << "] != [ " << p0.refraction << " ]" )
		CUSTOM_TEST( arg.sAlpha == p0.sAlpha, "sAlpha [ " << arg.sAlpha << " ] != [ " << p0.sAlpha << " ]" )
		CUSTOM_TEST( arg.ssAlpha == p0.ssAlpha, "ssAlpha [ " << arg.ssAlpha << " ] != [ " << p0.ssAlpha << " ]" )
		CUSTOM_TEST( arg.ssPower == p0.ssPower, "ssPower [ " << arg.ssPower << " ] != [ " << p0.ssPower << " ]" )
		CUSTOM_TEST( arg.ssScale == p0.ssScale, "ssScale [ " << arg.ssScale << " ] != [ " << p0.ssScale << " ]" )
		CUSTOM_TEST( arg.ssBump == p0.ssBump, "ssBump [ " << arg.ssBump << " ] != [ " << p0.ssBump << " ]" )
		CUSTOM_TEST( arg.absorption == p0.absorption, "ssScale [ " << arg.absorption << " ] != [ " << p0.absorption << " ]" )

		return true;

		//return (arg.ka == p0.ka)
		//	&& (arg.kd == p0.kd)
		//	&& (arg.ks == p0.ks)
		//	&& (arg.bumpScale == p0.bumpScale)
		//	&& almostEqual(arg.roughness, p0.roughness)
		//	&& almostEqual(arg.metalness, p0.metalness)
		//	&& almostEqual(arg.ior, p0.ior)
		//	&& almostEqual(arg.sAlpha, p0.sAlpha)
		//	&& almostEqual(arg.ssAlpha, p0.ssAlpha)
		//	&& almostEqual(arg.ssPower, p0.ssPower)
		//	&& almostEqual(arg.ssScale, p0.ssScale)
		//	&& almostEqual(arg.ssBump, p0.ssBump)
		//	&& almostEqual(arg.absorption, p0.absorption);
	}

	MATCHER_P(GeometryIsEqual, p0, "Object Geometry")
	{
		CUSTOM_TESTER_1( arg.getIndexBuffer( ).size( ),
						 p0.getIndexBuffer( ).size( ),
						 "Index Buffer Sizes" )

		CUSTOM_TESTER_1( arg.getVertexBuffer( ).size( ),
						 p0.getVertexBuffer( ).size( ),
						 "Vertex Buffer Sizes" )

		CUSTOM_TESTER_1( arg.getTopology( ),
						 p0.getTopology( ),
						 "Topoligy" )

		for( int i = 0; i < arg.getIndexBuffer().size( ); ++i )
		{
			CUSTOM_TESTER_1( arg.getIndexBuffer( )[ i ],
							 p0.getIndexBuffer( )[ i ],
							 "Index " << i )
		}

		for( int i = 0; i < arg.getVertexBuffer( ).size( ); ++i )
		{
			CUSTOM_TESTER_3( arg.getVertexBuffer( )[ i ].position,
							 p0.getVertexBuffer( )[ i ].position,
							 "Vertex Position " << i )

			CUSTOM_TESTER_2( arg.getVertexBuffer( )[ i ].textureUV,
							 p0.getVertexBuffer( )[ i ].textureUV,
							 "Vertex UV Velocity " << i )

			CUSTOM_TESTER_3( arg.getVertexBuffer( )[ i ].normal,
							 p0.getVertexBuffer( )[ i ].normal,
							 "Vertex Normal " << i )

			CUSTOM_TESTER_3( arg.getVertexBuffer( )[ i ].tangent,
							 p0.getVertexBuffer( )[ i ].tangent,
							 "Vertex Tangent " << i )

			CUSTOM_TESTER_3( arg.getVertexBuffer( )[ i ].binormal,
							 p0.getVertexBuffer( )[ i ].binormal,
							 "Vertex Bitangent/Binormal " << i )
		}



		//if( arg.getIndexBuffer( ).size( ) != p0.getIndexBuffer( ).size( ) )
		//{
		//	return false;
		//}
		//
		//if( arg.getVertexBuffer( ).size( ) != p0.getVertexBuffer( ).size( ) )
		//{
		//	return false;
		//}
		//
		//if( arg.getTopology( ) != p0.getTopology( ) )
		//{
		//	return false;
		//}
		//
		//for( int i = 0; i < arg.getIndexBuffer().size( ); ++i )
		//{
		//	if( arg.getIndexBuffer( )[ i ] != p0.getIndexBuffer( )[ i ] )
		//	{
		//		return false;
		//	}
		//}
		//
		//for( int i = 0; i < arg.getVertexBuffer().size( ); ++i )
		//{
		//	if( arg.getVertexBuffer( )[ i ].position != p0.getVertexBuffer( )[ i ].position )
		//	{
		//		return false;
		//	}
		//	if( arg.getVertexBuffer( )[ i ].textureUV != p0.getVertexBuffer( )[ i ].textureUV )
		//	{
		//		return false;
		//	}
		//	if( arg.getVertexBuffer( )[ i ].normal != p0.getVertexBuffer( )[ i ].normal )
		//	{
		//		return false;
		//	}
		//	if( arg.getVertexBuffer( )[ i ].tangent != p0.getVertexBuffer( )[ i ].tangent )
		//	{
		//		return false;
		//	}
		//	if( arg.getVertexBuffer( )[ i ].binormal != p0.getVertexBuffer( )[ i ].binormal )
		//	{
		//		return false;
		//	}
		//}

		return true;
	}

	MATCHER_P(ParticleGeometryIsEqual, p0, "Particle Geometry")
	{
		CUSTOM_TESTER_1( arg.size( ), p0.size( ),
						 "Particle Vertex Buffer Sizes" )

		for( int i = 0; i < arg.size( ); ++i )
		{
			CUSTOM_TESTER_3( arg[ i ].initialPos, p0[ i ].initialPos,
							 "Particle Vertex Initial Position " << i )

			CUSTOM_TESTER_3( arg[ i ].initialVel, p0[ i ].initialVel,
							 "Particle Vertex Initial Velocity " << i )

			CUSTOM_TESTER_2( arg[ i ].size, p0[ i ].size,
							 "Particle Vertex Size " << i )

			CUSTOM_TESTER_1( arg[ i ].age, p0[ i ].age,
							 "Particle Vertex Age " << i )

			CUSTOM_TESTER_1( arg[ i ].id, p0[ i ].id,
							 "Particle Vertex ID " << i )

			CUSTOM_TESTER_1( arg[ i ].type, p0[ i ].type,
							 "Particle Vertex Type " << i )
		}

		return true;
	}

	MATCHER_P( ParticleGSConstsAreEqual, p0, "Particle GS Constants" )
	{
		CUSTOM_TESTER_H_3( emitPosW, "Particle Emmiter Position" )
		CUSTOM_TESTER_H_3( emitPosWScale, "Particle Emmiter Position Random Offset Scale" )
		CUSTOM_TESTER_H_3( emitDirW, "Particle Emmiter Direction" )
		CUSTOM_TESTER_H_3( emitDirWScale, "Particle Emmiter Direction Random Offset Scale" )
		CUSTOM_TESTER_H_2( flareSize, "Particle Flare Size" )
		CUSTOM_TESTER_H_2( ageLifeSizeScale, "Particle Flare Size Scale With Age" )
		CUSTOM_TESTER_H_1( gameTime, "Particle Game Time" )
		CUSTOM_TESTER_H_1( timeStep, "Particle Time Step" )
		CUSTOM_TESTER_H_1( emitTime, "Particle Emit Time" )
		CUSTOM_TESTER_H_1( lifeTime, "Particle Life Time" )

		CUSTOM_TESTER_H_4( startColour, "Particle Start Colour" )
		CUSTOM_TESTER_H_4( starColourScale, "Particle Start Colour Random Offset Scale" )
		CUSTOM_TESTER_H_4( endColour, "Particle End Colour" )
		CUSTOM_TESTER_H_4( endColourScale, "Particle End Colour Random Offset Scale" )

		CUSTOM_TESTER_H_4( attractor, "Particle Attractor" )

		return true;
	}

	MATCHER_P( ParticlePSConstsAreEqual, p0, "Particle PS Constants" )
	{
		CUSTOM_TESTER_H_3( Ka, "Particle Ka" )
		CUSTOM_TESTER_H_3( Kd, "Particle Kd" )
		CUSTOM_TESTER_H_3( Ks, "Particle Ks" )
		CUSTOM_TESTER_H_1( Ns, "Particle Ns" )
		CUSTOM_TESTER_H_1( bumpScaleX, "Particle bump X" )
		CUSTOM_TESTER_H_1( bumpScaleY, "Particle bump Y" )
		CUSTOM_TESTER_H_2( imageCount, "Particle Sprite Image Count" )
		CUSTOM_TESTER_H_1( frameRate, "Particle Sprite Frame Rate" )
		CUSTOM_TESTER_H_1( frameCount, "Particle Sprite Frame Count" )

		return true;
	}

	MATCHER_P( LightIsEqual, p0, "Light" )
	{
		CUSTOM_TESTER_H_4( positionWS, "Position (World Space)" )
		CUSTOM_TESTER_H_4( directionWS, "Direction (World Space)" )
		//CUSTOM_TESTER_H_4( positionVS, "Particle Position (View Space)" )
		//CUSTOM_TESTER_H_4( directionVS, "Particle Direction (View Space)e" )
		CUSTOM_TESTER_H_4( colour, "Colour" )
		CUSTOM_TESTER_H_1( spotLightAngle, "Spot Light Angle" )
		CUSTOM_TESTER_H_1( range, "Range" )
		CUSTOM_TESTER_H_1( enabled, "Eanbled" )
		CUSTOM_TESTER_H_1( type, "Type" )

		return true;
	}

	MATCHER_P(TexturesAreEqual, p0, "Object Textures")
	{
		if( arg.size( ) != p0.size( ) )
		{
			return false;
		}

		for( int i = 0; i < arg.size( ); ++i )
		{
			if( arg[ i ].fileName  != p0[ i ].fileName
			 || arg[ i ].forceSRGB != p0[ i ].forceSRGB )
			{
				return false;
			}
		}
		return true;
	}


}

TEST( SceneParserTest, ProvidesABlankScene )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene": {}
	})";

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );


	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesASphere )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"spheres":
			[
				{
					"position": [ 0, 30, -6 ],
					"radius": 2,

					"ks": [ 0.0004, 0.0004, 0.0004 ]
				}
			]
		}
	})";


	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );
	PSObjectBuffer psProperties;

	psProperties.ks = vec3( 0.0004f, 0.0004f, 0.0004f );

	EXPECT_CALL( *pBuilder, addSphere( vec3( 0, 30, -6 ), 2.0f, MaterialIsEqual( psProperties ) ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAPBRSphere )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"pbrSpheres":
			[
				{
					"position": [ -6, 25.5, 0 ],
					"radius": 2,

					"ks": [ 0.0004, 0.0004, 0.0004 ],
					"bump":  [2,-2],

					"gammaCorrect" :  false,

					"textures":
					{
						"Reflection": "Reflection.tex",
						"albedo"    : "albedo.tex",
						"normal"    : "normal.tex",
						"height"    : "height.tex",
						"roughness" : "roughness.tex",
						"metalness" : "metalness.tex",
						"occlusion" : "occlusion.tex"
					}
				}
			]
		}
	})";

	const vector< string > expectedTextureNames =
	{
		"Reflection.tex",
		"albedo.tex",
		"normal.tex",
		"height.tex",
		"roughness.tex",
		"metalness.tex",
		"occlusion.tex"
	};

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );
	PSObjectBuffer psProperties;

	psProperties.ks = vec3( 0.0004f, 0.0004f, 0.0004f );
	psProperties.bumpScale = vec2( 2,-2 );

	EXPECT_CALL( *pBuilder, addPbrSphere( vec3( -6, 25.5, 0 ), 2, expectedTextureNames, MaterialIsEqual( psProperties ), false ) ).Times( 1 );



	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAPlanet )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"planets":
			[
				{
					"position": [ -6, 25.5, 0 ],
					"radius": 2,

					"textures":
					{
						"day"     : "day.tex",
						"night"   : "night.tex",
						"bump"    : "bump.tex",
						"specular": "specular.tex",
						"cloud"   : "cloud.tex"
					}
				}
			]
		}
	})";

	const vector< string > expectedTextureNames =
	{
		"day.tex",
		"night.tex",
		"bump.tex",
		"specular.tex",
		"cloud.tex"
	};

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addPlanet( vec3( -6, 25.5, 0 ), 2, expectedTextureNames ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAMoon )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"moons":
			[
				{
					"position": [ -6, 15.5, 0 ],
					"radius": 1,

					"textures":
					{
						"surface": "surface.tex",
						"bump"   : "bump.tex"
					}
				}
			]
		}
	})";

	const vector< string > expectedTextureNames =
	{
		"surface.tex",
		"bump.tex"
	};

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addMoon( vec3( -6, 15.5, 0 ), 1, expectedTextureNames ) ).Times( 1 );


	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAnObj )
{
	const path testFile( path( __FILE__ ).parent_path( ).string( ) + "/test.obj" );
	string strPath = testFile.string( );

	for( char& _Idx : strPath )
		if ( _Idx == _FS_BSLASH)
			_Idx = _FS_SLASH;	// convert '\' to '/'


	string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"objects":
			[
				{
					"file": ")"; testJson += strPath + R"(",
					"position": [ 1, 0, 0 ],
					"rotation": [ 0, 0, 1 ],
					"scale"   : [ 1, 1, 1 ]
				}
			]
		}
	})";

	const vector< string > expectedTextureNames =
	{
		"SingleTexelWhite",
		"SingleTexelBlack"
	};

	PSObjectBuffer psProperties;
	psProperties.ka = vec3( 1, 1, 1 );
	psProperties.kd = vec3( 1, 1, 1 );
	psProperties.ks = vec3( 1, 1, 1 );

	const vec3 position( 1, 0, 0 );
	const vec3 rotation( 0, 0, 1 );
	const vec3    scale( 1, 1, 1 );

	const Mesh< Vertex3f2f3f3f3f::Vertex > geometry =
	{
		{
			{ {  1, 1,  1 }, { 0, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ { -1, 1,  1 }, { 0, 1 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ {  1, 1, -1 }, { 1, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } }
		},
		{ 0, 1, 2 },
		MeshPrimitive::TriangleList
	};


	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );


	EXPECT_CALL( *pBuilder,
				addBasicMesh(
					strPath + "1",
					GeometryIsEqual( geometry ),
					position,
					rotation,
					scale,
					expectedTextureNames,
					MaterialIsEqual( psProperties )
				) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAClippedObj )
{
	const path testFile( path( __FILE__ ).parent_path( ).string( ) + "/testClipped.obj" );
	string strPath = testFile.string( );

	for( char& _Idx : strPath )
		if ( _Idx == _FS_BSLASH)
			_Idx = _FS_SLASH;	// convert '\' to '/'


	string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"objects":
			[
				{
					"file": ")"; testJson += strPath + R"(",
					"position": [ 1, 0, 0 ],
					"rotation": [ 0, 0, 1 ],
					"scale"   : [ 1, 1, 1 ]
				}
			]
		}
	})";

	const vector< string > expectedTextureNames =
	{
		"SingleTexelWhite",
		"SingleTexelBlack"
	};

	PSObjectBuffer psProperties;
	psProperties.ka = vec3( 1, 1, 1 );
	psProperties.kd = vec3( 1, 1, 1 );
	psProperties.ks = vec3( 1, 1, 1 );

	const vec3 position( 1, 0, 0 );
	const vec3 rotation( 0, 0, 1 );
	const vec3    scale( 1, 1, 1 );

	const Mesh< Vertex3f2f3f3f3f::Vertex > geometry =
	{
		{
			{ {  1, 1,  1 }, { 0, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ { -1, 1,  1 }, { 0, 1 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ {  1, 1, -1 }, { 1, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } }
		},
		{ 0, 1, 2 },
		MeshPrimitive::TriangleList
	};


	shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );


	EXPECT_CALL( *pBuilder,
				addClippedMesh(
					strPath + "1",
					GeometryIsEqual( geometry ),
					position,
					rotation,
					scale,
					expectedTextureNames,
					MaterialIsEqual( psProperties )
				) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesATranslucentObj )
{
	const path testFile( path( __FILE__ ).parent_path( ).string( ) + "/testClipped.obj" );
	string strPath = testFile.string( );

	for( char& idx : strPath )
		if ( idx == _FS_BSLASH)
			idx = _FS_SLASH;	// convert '\' to '/'


	string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"translucentObjects":
			[
				{
					"file": ")"; testJson += strPath + R"(",
					"position": [ 1, 0, 0 ],
					"rotation": [ 0, 0, 1 ],
					"scale"   : [ 1, 1, 1 ]
				}
			]
		}
	})";

	const vector< string > expectedTextureNames =
	{
		"SingleTexelWhite",
		"SingleTexelBlack",
		"SingleTexelWhite",
		"SingleTexelWhite",
		"SingleTexelBlack",
		"SingleTexelWhite",
		"SingleTexelWhite",
		"SingleTexelWhite"
	};

	PSObjectBuffer psProperties;
	psProperties.ka = vec3( 1, 1, 1 );
	psProperties.kd = vec3( 1, 1, 1 );
	psProperties.ks = vec3( 1, 1, 1 );
	psProperties.bumpScale = vec2( 0.5f, 0.5f );

	psProperties.metalness = 0.9f;
	psProperties.roughness = 0.1f;

	psProperties.ssPower = 10;
	psProperties.ssScale = 4;
	psProperties.ssBump = 0.0f;

	psProperties.ior = 2.0f;
	psProperties.sAlpha = 0.5f;
	psProperties.ssAlpha = 0.5f;
	psProperties.absorption = 0.25f;

	const vec3 position( 1, 0, 0 );
	const vec3 rotation( 0, 0, 1 );
	const vec3    scale( 1, 1, 1 );

	const Mesh< Vertex3f2f3f3f3f::Vertex > geometry =
	{
		{
			{ {  1, 1,  1 }, { 0, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ { -1, 1,  1 }, { 0, 1 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ {  1, 1, -1 }, { 1, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } }
		},
		{ 0, 1, 2 },
		MeshPrimitive::TriangleList
	};


	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder,
				addTranslucentMesh(
					strPath + "1",
					GeometryIsEqual( geometry ),
					position,
					rotation,
					scale,
					expectedTextureNames,
					MaterialIsEqual( psProperties )
				) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesATranslucentTest )
{
	const path testFile( path( __FILE__ ).parent_path( ).string( ) + "/testClipped.obj" );
	string strPath = testFile.string( );

	for (size_t _Idx = 0; _Idx < strPath.size(); ++_Idx)
		if (strPath[_Idx] == _FS_BSLASH)
			strPath[_Idx] = _FS_SLASH;	// convert '\' to '/'


	string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"translucentTests":
			[
				{
					"file": ")"; testJson += strPath + R"(",
					"position":   [ 0, 0, 0 ],
					"rotation":   [ 0, 0, 0 ],
					"scale"   :   [ 1, 1, 1 ],

					"count":      2,

					"kaStart":    [ 1, 1, 1 ],
					"kdStart":    [ 1, 1, 1 ],
					"ksStart":    [ 1, 1, 1 ],

					"kaEnd":      [ 1, 1, 1 ],
					"kdEnd":      [ 1, 1, 1 ],
					"ksEnd":      [ 1, 1, 1 ],

					"roughness":  [ 0.5, 0.5 ],
					"metalness":  [ 1, 1 ],

					"sAlpha":     [ 0, 0 ],
					"ssAlpha":    [ 0.1, 0.1 ],
					"absorption": [ 0.1, 0.1 ],
					"ior":        [ 2, 2 ],
					"ssScale":    [ 1, 1 ],
					"ssPower":    [ 4, 4 ],
					"ssBump":     [ 0.5, 0.5 ]
				}
			]
		}
	})";

	const vector< string > expectedTextureNames =
	{
		"SingleTexelWhite",
		"SingleTexelBlack",
		"SingleTexelWhite",
		"SingleTexelWhite",
		"SingleTexelBlack",
		"SingleTexelWhite",
		"SingleTexelWhite",
		"SingleTexelWhite"
	};

	PSObjectBuffer psProperties;
	psProperties.ka         = vec3( 1, 1, 1 );
	psProperties.kd         = vec3( 1, 1, 1 );
	psProperties.ks         = vec3( 1, 1, 1 );

	psProperties.metalness  = 1.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 0.0f, 0.0f );
	psProperties.ssBump     = 0.5f;
	psProperties.ssPower    = 4;
	psProperties.ssScale    = 1;

	psProperties.ior        = 2.0f;
	psProperties.sAlpha     = 0.0f;
	psProperties.ssAlpha    = 0.1f;
	psProperties.absorption = 0.1f;

	const vec3 position1( 0, 0, 0 );
	const vec3 position2( 0, 0, 2 );
	const vec3 rotation( 0, 0, 0 );
	const vec3    scale( 1, 1, 1 );

	const Mesh< Vertex3f2f3f3f3f::Vertex > geometry =
	{
		{
			{ {  1, 1,  1 }, { 0, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ { -1, 1,  1 }, { 0, 1 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } },
			{ {  1, 1, -1 }, { 1, 0 }, { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } }
		},
		{ 0, 1, 2 },
		MeshPrimitive::TriangleList
	};


	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder,
				addTranslucentMesh(
					strPath + "1Translucent",
					GeometryIsEqual( geometry ),
					position1,
					rotation,
					scale,
					expectedTextureNames,
					MaterialIsEqual( psProperties )
				) ).Times( 1 );

	EXPECT_CALL( *pBuilder,
				addTranslucentMesh(
					strPath + "1Translucent",
					GeometryIsEqual( geometry ),
					position2,
					rotation,
					scale,
					expectedTextureNames,
					MaterialIsEqual( psProperties )
				) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAHeightMap )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"heightMaps":
			[
				{
					"file": "heightmap.raw",
					"size": [ 128, 128 ],
					"position": [ 0, 0, 0 ],
					"ka": [ 0.2, 0.2, 0.2 ],
					"kd": [ 0.8, 0.8, 0.8 ],
					"ks": [ 2.0, 2.0, 2.0 ],
					"layers":
					[
						{
							"gammaCorrect" :  true,
							"albedo"   : "albedo.tex",
							"normal"   : "normal.tex",
							"height"   : "height.tex",
							"roughness": "roughness.tex",
							"occlusion": "occlusion.tex",
							"metalness": "metalness.tex"
						},
						{
							"gammaCorrect" :  true,
							"albedo"   : "albedo.tex",
							"normal"   : "normal.tex",
							"height"   : "height.tex",
							"roughness": "roughness.tex",
							"occlusion": "occlusion.tex",
							"metalness": "metalness.tex"
						},
						{
							"gammaCorrect" :  false,
							"albedo"   : "albedo.tex",
							"normal"   : "normal.tex",
							"height"   : "height.tex",
							"roughness": "roughness.tex",
							"occlusion": "occlusion.tex",
							"metalness": "metalness.tex"
						}
					]
				}
			]
		}
	})";

	const string expectedPath = "heightmap.raw";
	const vector< SceneBuilder::LoadTexture > expectedTextureNames =
	{
		{ "albedo.tex", true },
		{ "normal.tex", false },
		{ "height.tex", false },
		{ "roughness.tex", false },
		//{ "occlusion.tex", false },
		//{ "metalness.tex", false },

		{ "albedo.tex", true },
		{ "normal.tex", false },
		{ "height.tex", false },
		{ "roughness.tex", false },
		//{ "occlusion.tex", false },
		//{ "metalness.tex", false },

		{ "albedo.tex", false },
		{ "normal.tex", false },
		{ "height.tex", false },
		{ "roughness.tex", false },
		//{ "occlusion.tex", false },
		//{ "metalness.tex", false }
	};

	PSObjectBuffer psProperties;
	psProperties.ka         = vec3( 0.2f, 0.2f, 0.2f );
	psProperties.kd         = vec3( 0.8f, 0.8f, 0.8f );
	psProperties.ks         = vec3( 2.0f, 2.0f, 2.0f );

	psProperties.metalness  = 0.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 1.0f, 1.0f );
	psProperties.ssBump     = 0.0f;
	psProperties.ssPower    = 0.0f;
	psProperties.ssScale    = 0.0f;

	psProperties.ior        = 0.0f;
	psProperties.sAlpha     = 1.0f;
	psProperties.ssAlpha    = 0.0f;
	psProperties.absorption = 0.0f;

	const vec3 position( 0, 0, 0 );
	const vec3    scale( 1, 1, 1 );
	const ivec2    size( 128, 128 );

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder,
				 addHeightMap(
					expectedPath,
					position, scale, size,
					TexturesAreEqual( expectedTextureNames ),
					MaterialIsEqual( psProperties )
				) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAPCGHeightMap )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"pcgHeightMaps":
			[
				{
					"size": [ 128, 128 ],
					"ka": [ 0.2, 0.2, 0.2 ],
					"kd": [ 0.8, 0.8, 0.8 ],
					"ks": [ 2.0, 2.0, 2.0 ],
					"layers":
					[
						{
							"gammaCorrect" :  true,
							"albedo"   : "albedo.tex",
							"normal"   : "normal.tex",
							"height"   : "height.tex",
							"roughness": "roughness.tex",
							"occlusion": "occlusion.tex",
							"metalness": "metalness.tex"
						},
						{
							"gammaCorrect" :  true,
							"albedo"   : "albedo.tex",
							"normal"   : "normal.tex",
							"height"   : "height.tex",
							"roughness": "roughness.tex",
							"occlusion": "occlusion.tex",
							"metalness": "metalness.tex"
						},
						{
							"gammaCorrect" :  false,
							"albedo"   : "albedo.tex",
							"normal"   : "normal.tex",
							"height"   : "height.tex",
							"roughness": "roughness.tex",
							"occlusion": "occlusion.tex",
							"metalness": "metalness.tex"
						}
					]
				}
			]
		}
	})";

	const vector< SceneBuilder::LoadTexture > expectedTextureNames =
	{
		{ "albedo.tex", true },
		{ "normal.tex", false },
		{ "height.tex", false },
		{ "roughness.tex", false },
		//{ "occlusion.tex", false },
		//{ "metalness.tex", false },

		{ "albedo.tex", true },
		{ "normal.tex", false },
		{ "height.tex", false },
		{ "roughness.tex", false },
		//{ "occlusion.tex", false },
		//{ "metalness.tex", false },

		{ "albedo.tex", false },
		{ "normal.tex", false },
		{ "height.tex", false },
		{ "roughness.tex", false },
		//{ "occlusion.tex", false },
		//{ "metalness.tex", false }
	};

	PSObjectBuffer psProperties;
	psProperties.ka         = vec3( 0.2f, 0.2f, 0.2f );
	psProperties.kd         = vec3( 0.8f, 0.8f, 0.8f );
	psProperties.ks         = vec3( 2.0f, 2.0f, 2.0f );

	psProperties.metalness  = 0.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 1.0f, 1.0f );
	psProperties.ssBump     = 0.0f;
	psProperties.ssPower    = 0.0f;
	psProperties.ssScale    = 0.0f;

	psProperties.ior        = 0.0f;
	psProperties.sAlpha     = 1.0f;
	psProperties.ssAlpha    = 0.0f;
	psProperties.absorption = 0.0f;

	const vec3    scale( 1, 1, 1 );
	const ivec2    size( 128, 128 );

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder,
				 addPCGHeightMap(
					scale, size,
					TexturesAreEqual( expectedTextureNames ),
					MaterialIsEqual( psProperties )
				) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesACube )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"cubes":
			[
				{
					"position": [ 0, 0, 0 ],
					"scale": [ 1, 1, 1 ],
					"rotation": [ 0, 0, 0 ],
					"ka": [ 0.1, 0.1, 0.1 ],
					"kd": [ 1.0, 1.0, 1.0 ],
					"ks": [ 0.9, 0.0, 0.0 ],
					"textures":
					{
						"albedo":    "albedo.tex",
						"normal":    "normal.tex",
						"height":    "height.tex",
						"ao":        "ao.tex",
						"bump":      "bump.tex",
						"gloss":     "gloss.tex",
						"hiGloss":   "hiGloss.tex",
						"metalness": "metalness.tex",
						"roughness": "roughness.tex"
					}
				}
			]
		}
	})";

	const vector< string > textures =
	{
		"albedo.tex",
		"normal.tex",
		"height.tex",
		"ao.tex",
		"bump.tex",
		"gloss.tex",
		"hiGloss.tex",
		"roughness.tex",
		"metalness.tex"
	};

	PSObjectBuffer psProperties;
	psProperties.ka         = vec3( 0.1f, 0.1f, 0.1f );
	psProperties.kd         = vec3( 1.0f, 1.0f, 1.0f );
	psProperties.ks         = vec3( 0.9f, 0.0f, 0.0f );

	psProperties.metalness  = 0.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 1.0f, 1.0f );
	psProperties.ssBump     = 0.0f;
	psProperties.ssPower    = 0.0f;
	psProperties.ssScale    = 0.0f;

	psProperties.ior        = 0.0f;
	psProperties.sAlpha     = 1.0f;
	psProperties.ssAlpha    = 0.0f;
	psProperties.absorption = 0.0f;

	const vec3 position( 0, 0, 0 );
	const vec3 rotation( 0, 0, 0 );
	const vec3    scale( 1, 1, 1 );

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addCube( position, rotation, scale, textures, MaterialIsEqual( psProperties ) ) )
		.Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAWaterPlane )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"water":
			[
				{
					"position": [ 0, 0, 0 ],
					"scale": [ 1, 1, 1 ],
					"rotation": [ 0, 0, 0 ],
					"ka": [ 0.1, 0.1, 0.1 ],
					"kd": [ 1.0, 1.0, 1.0 ],
					"ks": [ 0.9, 0.0, 0.0 ],
					"textures":
					{
						"layer1":    "layer1.tex",
						"layer2":    "layer2.tex"
					}
				}
			]
		}
	})";

	const vector< string > textures =
	{
		"layer1.tex",
		"layer2.tex"
	};

	PSObjectBuffer psProperties;
	psProperties.ka         = vec3( 0.1f, 0.1f, 0.1f );
	psProperties.kd         = vec3( 1.0f, 1.0f, 1.0f );
	psProperties.ks         = vec3( 0.9f, 0.0f, 0.0f );

	psProperties.metalness  = 0.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 1.0f, 1.0f );
	psProperties.ssBump     = 0.0f;
	psProperties.ssPower    = 0.0f;
	psProperties.ssScale    = 0.0f;

	psProperties.ior        = 0.0f;
	psProperties.sAlpha     = 1.0f;
	psProperties.ssAlpha    = 0.0f;
	psProperties.absorption = 0.0f;

	const vec3 position( 0, 0, 0 );
	const vec3 rotation( 0, 0, 0 );
	const vec3    scale( 1, 1, 1 );

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addWaterPlane( position, rotation, scale, textures, MaterialIsEqual( psProperties ) ) )
		.Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesAPCGWaterPlane )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"pcgWater":
			[
				{
					"position": [ 0, 0, 0 ],
					"scale": [ 1, 1, 1 ],
					"rotation": [ 0, 0, 0 ],
					"ka": [ 0.1, 0.1, 0.1 ],
					"kd": [ 1.0, 1.0, 1.0 ],
					"ks": [ 0.9, 0.0, 0.0 ],
					"followCamera": true,
					"textures":
					{
						"layer1":    "layer1.tex",
						"layer2":    "layer2.tex"
					}
				}
			]
		}
	})";

	const vector< string > textures =
	{
		"layer1.tex",
		"layer2.tex"
	};

	PSObjectBuffer psProperties;
	psProperties.ka         = vec3( 0.1f, 0.1f, 0.1f );
	psProperties.kd         = vec3( 1.0f, 1.0f, 1.0f );
	psProperties.ks         = vec3( 0.9f, 0.0f, 0.0f );

	psProperties.metalness  = 0.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 1.0f, 1.0f );
	psProperties.ssBump     = 0.0f;
	psProperties.ssPower    = 0.0f;
	psProperties.ssScale    = 0.0f;

	psProperties.ior        = 0.0f;
	psProperties.sAlpha     = 1.0f;
	psProperties.ssAlpha    = 0.0f;
	psProperties.absorption = 0.0f;

	const vec3 position( 0, 0, 0 );
	const vec3 rotation( 0, 0, 0 );
	const vec3    scale( 1, 1, 1 );

	const bool follow = true;

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addPCGWaterPlane( position, rotation, scale, textures, MaterialIsEqual( psProperties ), follow ) )
		.Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesBasicVolumeSky )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"skyBox":
			{
				"type": "BasicVolume",
				"sunDirection": [ 0, 1, 0 ],
				"stars": "stars.tex"
			}
		}
	})";

	const vector< string > textures = { "stars.tex" };

	const vec3 direction( 0, 1, 0 );

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addBasicVolumeSkybox( textures, direction ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesCubeMapSkyBox )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"skyBox":
			{
				"type": "Cube",
				"sunDirection": [ 0, 1, 0 ],
				"skyBox": "skyBox.tex"
			}
		}
	})";

	const vector< string > textures = { "skyBox.tex" };

	const vec3 direction( 0, 1, 0 );

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addCubeSkybox( textures, direction ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesPolarSkyBox )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"skyBox":
			{
				"type": "Polar",
				"sunDirection": [ 0, 1, 0 ],
				"skyMap": "skyMap.tex"
			}
		}
	})";

	const vector< string > textures = { "skyMap.tex" };

	const vec3 direction( 0, 1, 0 );

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addPolarSkybox( textures, direction ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesParicleSystem )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"particleSystems":
			[
				{
					"blend":   "add",
					"texture": "texture.tex",

					"imageCount": [ 8, 8 ],
					"frameRate": 12,
					"frameCount": 56,

					"position": [ 0, 10, 0 ],
					"velocity": [ 0, 0, 0 ],
					"size":     [ 0.1, 0.1 ],

					"positionScale":  [ 0.4, 0.1, 0.4 ],
					"directionScale": [ 0.1, 0.1, 0.1 ],

					"emitTime": 0.01,
					"lifeTime": 40.0,
					"ageSizeScale": [ 0.0, 0.0 ],

					"attractor": [ 0.0, 10.0, 0.0, 60.0 ],

					"startColour": [ 1.0, 1.0, 1.0, 1.0 ],
					"endColour":   [ 1.0, 1.0, 1.0, 1.0 ],

					"starColourScale": [ 100.0, 100.0, 100.0, 1.0 ],
					"endColourScale":  [   0.0,   0.0,   0.0, 0.0 ],

					"count": 5,
					"particles":
					[
						{
							"position": [ 0, 0, 0 ],
							"velocity": [ 0, 1, 0 ],
							"size": [ 0, 0 ],
							"type": "emitter",
							"age": 2.0
						}
					]
				}
			]
		}
	})";

	const Mesh< VertexParticle::Vertex >::VertexBuffer initialState = {
		{ { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0 }, 2, 0, 0 }
	};
	const vector< string > textures = { "texture.tex" };

	GSParticleBuffer gsProperties;
	gsProperties.emitPosW = vec3( 0, 10, 0 );
	gsProperties.emitPosWScale = vec3( 0.4, 0.1, 0.4 );
	gsProperties.emitDirW = vec3( 0, 0, 0 );
	gsProperties.emitDirWScale = vec3( 0.1, 0.1, 0.1 );
	gsProperties.flareSize = vec2( 0.1, 0.1 );
	gsProperties.ageLifeSizeScale = vec2( 0.0, 0.0 );
	gsProperties.emitTime = 0.01f;
	gsProperties.lifeTime = 40.0f;
	gsProperties.attractor = vec4( 0.0, 10.0, 0.0, 60.0 );
	gsProperties.startColour = vec4( 1.0, 1.0, 1.0, 1.0 );
	gsProperties.endColour = vec4( 1.0, 1.0, 1.0, 1.0 );
	gsProperties.starColourScale = vec4( 100.0, 100.0, 100.0, 1.0 );
	gsProperties.endColourScale = vec4( 0.0, 0.0, 0.0, 0.0 );

	PSParticleBuffer psProperties;
	psProperties.imageCount = vec2( 8, 8 );
	psProperties.frameCount = 56;
	psProperties.frameRate = 12;

	//gsProperties


	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder,
				 addParticleSystem(
					 ParticleGeometryIsEqual( initialState ),
					 textures,
					 ParticleGSConstsAreEqual( gsProperties ),
					 ParticlePSConstsAreEqual( psProperties ),
					 5,
					 "add"
				 ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesFog )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"fog":
			{
				"density": 1.0,
				"heightFalloff": 0.5,
				"rayDepth": 64,
				"gScatter": 0.76,
				"waterHeight": -100
			}
		}
	})";

	const Mesh< VertexParticle::Vertex >::VertexBuffer initialState = {
		{ { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0 }, 2, 0, 0 }
	};
	const vector< string > textures = { "texture.tex" };

	const float density = 1.0f;
	const float heightFalloff = 0.5f;
	const float rayDepth = 64;
	const float gScatter = 0.76f;
	const float waterHeight = -100;


	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addFog( density, heightFalloff, rayDepth, gScatter, waterHeight ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, ParsesLight )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"lights":
			[
				{
					"position": [ 0, 1, 0 ],
					"direction": [ 1, 0, 0 ],
					"colour": [ 20, 0, 0 ],
					"spotLightAngle": 0.0,
					"range": 5.0,
					"enabled": 1,
					"type": "point"
				}
			]
		}
	})";

	const Mesh< VertexParticle::Vertex >::VertexBuffer initialState = {
		{ { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0 }, 2, 0, 0 }
	};
	const vector< string > textures = { "texture.tex" };

	LightEx light;
	light.positionWS  = vec4( 0, 1, 0, 1 );
	light.directionWS = vec4( 1, 0, 0, 0 );
	light.positionVS  = vec4( 0, 0, 0, 0 );
	light.directionVS = vec4( 0, 0, 0, 0 );
	light.colour      = vec4( 20, 0, 0, 1 );
	light.spotLightAngle = 0.0f;
	light.range          = 5.0f;
	light.enabled        = 1;
	light.type           = 0;

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addLight( LightIsEqual( light ) ) ).Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}


TEST( SceneParserTest, Parses3DText )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"text3D":
			[
				{
					"font": "fontFile",
					"text": "The string we want",
					"position": [ 0, 0, 0 ],
					"scale": [ 1, 1, 1 ],
					"rotation": [ 0, 0, 0 ],
					"ka": [ 0.1, 0.1, 0.1 ],
					"kd": [ 1.0, 1.0, 1.0 ],
					"ks": [ 0.9, 0.0, 0.0 ],
					"textures":
					{
						"albedo": "SingleTexelWhite",
						"normal": "BlankNormal",
						"height": "SingleTexelWhite",
						"occlusion": "SingleTexelWhite",
						"bump": "SingleTexelBlack",
						"gloss": "SingleTexelBlack",
						"hiGloss": "SingleTexelBlack",
						"roughness": "SingleTexelWhite",
						"metalness": "SingleTexelBlack"
					}
				}
			]
		}
	})";

	const vector< string > textures =
	{
		"SingleTexelWhite",
		"BlankNormal",
		"SingleTexelWhite",
		"SingleTexelWhite",
		"SingleTexelBlack",
		"SingleTexelBlack",
		"SingleTexelBlack",
		"SingleTexelWhite",
		"SingleTexelBlack"
	};

	PSFontBuffer psProperties;
	psProperties.ka         = vec3( 0.1f, 0.1f, 0.1f );
	psProperties.kd         = vec3( 1.0f, 1.0f, 1.0f );
	psProperties.ks         = vec3( 0.9f, 0.0f, 0.0f );

	psProperties.metalness  = 0.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 0.0f, 0.0f );
	psProperties.ssBump     = 0.0f;
	psProperties.ssPower    = 0.0f;
	psProperties.ssScale    = 0.0f;

	psProperties.refraction = 0.0f;
	psProperties.sAlpha     = 1.0f;
	psProperties.ssAlpha    = 0.0f;
	psProperties.absorption = 0.0f;
	psProperties.properties = uvec4( 0, 0, 0, 0 );

	const vec3 position( 0, 0, 0 );
	const vec3 rotation( 0, 0, 0 );
	const vec3    scale( 1, 1, 1 );

	const string text = "The string we want";
	const string font = "fontFile";

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addText3D( font, text, position, rotation, scale, textures, FontMaterialIsEqual( psProperties ) ) )
		.Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, Parses2DText )
{
	const string testJson = R"(
	{
		"meta": {},
		"scene":
		{
			"text2D":
			[
				{
					"font": "fontFile",
					"text": "The string we want",
					"position": [ 0, 0 ],
					"scale": [ 1, 1 ],
					"rotation": [ 0, 0 ],
					"ka": [ 0.1, 0.1, 0.1 ],
					"kd": [ 1.0, 1.0, 1.0 ],
					"ks": [ 0.9, 0.0, 0.0 ],
					"textures":
					{
						"layer1":    "layer1.tex",
						"layer2":    "layer2.tex"
					}
				}
			]
		}
	})";

	const vector< string > textures =
	{
		"layer1.tex",
		"layer2.tex"
	};

	PSFontBuffer psProperties;
	psProperties.ka         = vec3( 0.1f, 0.1f, 0.1f );
	psProperties.kd         = vec3( 1.0f, 1.0f, 1.0f );
	psProperties.ks         = vec3( 0.9f, 0.0f, 0.0f );

	psProperties.metalness  = 0.0f;
	psProperties.roughness  = 0.5f;

	psProperties.bumpScale  = vec2( 0.0f, 0.0f );
	psProperties.ssBump     = 0.0f;
	psProperties.ssPower    = 0.0f;
	psProperties.ssScale    = 0.0f;

	psProperties.refraction = 0.0f;
	psProperties.sAlpha     = 1.0f;
	psProperties.ssAlpha    = 0.0f;
	psProperties.absorption = 0.0f;
	psProperties.properties = uvec4( 0, 0, 0, 0 );

	const vec2 position( 0, 0 );
	const vec2 rotation( 0, 0 );
	const vec2    scale( 1, 1 );

	const string text = "The string we want";
	const string font = "fontFile";

	const shared_ptr< MockBuilder > pBuilder = make_shared< MockBuilder >( );
	SceneParser parser( [ ]( const function< void( ) >& process ){ process( ); }, pBuilder );

	EXPECT_CALL( *pBuilder, addText2D( font, text, position, rotation, scale, textures, FontMaterialIsEqual( psProperties ) ) )
		.Times( 1 );

	EXPECT_CALL( *pBuilder, bake( ) ).Times( 1 );

	parser.tools::JsonParser::parse( testJson );
}

TEST( SceneParserTest, TODO_PARSE_ZONE )
{
	// EXPECT_CALL( *pBuilder, addGsCube( _, _, _, _, _ ) ).Times( 0 );
	// EXPECT_CALL( *pBuilder, addMesh( _, _, _, _, _, _, _ ) ).Times( 0 );
}
