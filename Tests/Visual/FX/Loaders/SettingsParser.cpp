
#include <gtest/gtest.h>
#include <Visual/FX/Loaders/SettingsParser.h>
#include <Visual/FX/Settings.h>

class SettingsParserTest : public ::testing::Test
{
	protected:
		SettingsParserTest( ) = default;
		virtual ~SettingsParserTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

TEST( SettingsParserTest, ProvidesDefaultSettings )
{
	const std::string systemName = "Test Settings";
	const int numberOfThreads = 2;

	
	visual::fx::Settings settings( systemName );
	visual::fx::SettingsParser settingsParser;

	visual::fx::SettingsSPtr pSettings = settingsParser.getSettings( );

	EXPECT_EQ( pSettings->getDefaultScene( ), settings.getDefaultScene( ) );
	
	EXPECT_EQ( pSettings->getBackBufferFormat( ), settings.getBackBufferFormat( ) );
	EXPECT_EQ( pSettings->getBackBufferSize( ), settings.getBackBufferSize( ) );
	EXPECT_EQ( pSettings->getFrameBuffers( ), settings.getFrameBuffers( ) );
	
	EXPECT_EQ( pSettings->getRenderTargetFormat( ), settings.getRenderTargetFormat( ) );
	EXPECT_EQ( pSettings->getRenderTargetSize( ), settings.getRenderTargetSize( ) );
	EXPECT_EQ( pSettings->getMsaaLevel( ), settings.getMsaaLevel( ) );

	EXPECT_EQ( pSettings->getMaximumAsyncJobsToExecute( ), settings.getMaximumAsyncJobsToExecute( ) );
	EXPECT_EQ( pSettings->getMaximumSyncJobsToExecute( ), settings.getMaximumSyncJobsToExecute( ) );
	EXPECT_EQ( pSettings->getMinimumAsyncJobsToExecute( ), settings.getMinimumAsyncJobsToExecute( ) );
	EXPECT_EQ( pSettings->getMinimumSyncJobsToExecute( ), settings.getMinimumSyncJobsToExecute( ) );
	EXPECT_EQ( pSettings->getNumberOfWorkerThreads( ), settings.getNumberOfWorkerThreads( ) );
}

TEST( SettingsParserTest, ParsesASettingsJsonString )
{
	const std::string testJson = R"(
	{
		"meta": {},
		"settings":
		{
			"title": "NaKamaFxTest",
			"defaultScene": "test.json",
			
			"windowSize": [ 1600, 900 ],
			"windowColourFormat":  "8bit",
			"fullscreen": false,
			"frameBuffers": 2,
			"vsync":  "off",

			"renderingSize": [ 1600, 900 ],
			"renderingColourFormat":  "16bit",
			"msaa": 1,

			"shadowCascade1Size": [ 4096, 4096 ],
			"shadowCascade2Size": [ 2048, 2048 ],

			"threads":
			{
				"workers": 4,
				"sync": [ 2, 4 ],
				"async": [ 1, 3 ]
			}
		}
	})";



	visual::fx::SettingsParser settingsParser;
	settingsParser.tools::JsonParser::parse( testJson );

	const visual::fx::SettingsSPtr pSettings = settingsParser.getSettings( );
	
	EXPECT_EQ( pSettings->getSystemName( ), "NaKamaFxTest" );
	EXPECT_EQ( pSettings->getDefaultScene( ), "test.json" );
	
	EXPECT_EQ( pSettings->getBackBufferFormat( ), visual::fx::ColourFormat::RGBA8 );
	EXPECT_EQ( pSettings->getBackBufferSize( ), glm::uvec2( 1600, 900 ) );
	EXPECT_EQ( pSettings->getFrameBuffers( ), 2 );

	EXPECT_EQ( pSettings->getRenderTargetFormat( ), visual::fx::ColourFormat::RGBA16 );
	EXPECT_EQ( pSettings->getRenderTargetSize( ), glm::uvec2( 1600, 900 ) );
	EXPECT_EQ( pSettings->getMsaaLevel( ), visual::fx::MsaaLevel::MSAA_X1 );
	
	EXPECT_EQ( pSettings->getNumberOfWorkerThreads( ), 4 );

	EXPECT_EQ( pSettings->getMinimumSyncJobsToExecute( ), 2 );
	EXPECT_EQ( pSettings->getMaximumSyncJobsToExecute( ), 4 );
	EXPECT_EQ( pSettings->getMinimumAsyncJobsToExecute( ), 1 );
	EXPECT_EQ( pSettings->getMaximumAsyncJobsToExecute( ), 3 );
}

  