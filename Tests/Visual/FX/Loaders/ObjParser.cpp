
#include <gtest/gtest.h>
#include <Visual/FX/Loaders/ObjParser.h>
#include <filesystem>

class ObjParserTest : public ::testing::Test
{
	protected:
		ObjParserTest( ) = default;
		virtual ~ObjParserTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }
};

using std::filesystem::path;

TEST( ObjParserTest, ProvidesDefaultSettings )
{
	const std::string systemName = "Test Settings";
	const int numberOfThreads = 2;

	
	visual::fx::ObjParser parser;
	path filePath( __FILE__ );

	path testFile( filePath.parent_path( ).string( ) + "/test.obj" );
	
	std::vector< visual::fx::ObjData > models = parser.parseObj( testFile.string( ) );

	
	ASSERT_EQ( models.size( ), 1 );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( ).size( ), 3 );
	
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 0 ].position , glm::vec3( 1, 1, 1 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 0 ].textureUV, glm::vec2( 0, 0 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 0 ].normal   , glm::vec3( 0, 1, 0 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 0 ].tangent  , glm::vec3( 0, 0, -1 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 0 ].binormal , glm::vec3( -1, 0, 0 ) );

	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 1 ].position , glm::vec3( -1, 1, 1 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 1 ].textureUV, glm::vec2( 0, 1 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 1 ].normal   , glm::vec3( 0, 1, 0 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 1 ].tangent  , glm::vec3( 0, 0, -1 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 1 ].binormal , glm::vec3( -1, 0, 0 ) );

	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 2 ].position , glm::vec3( 1, 1, -1 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 2 ].textureUV, glm::vec2( 1, 0 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 2 ].normal   , glm::vec3( 0, 1, 0 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 2 ].tangent  , glm::vec3( 0, 0, -1 ) );
	EXPECT_EQ( models[ 0 ].geometry.getVertexBuffer( )[ 2 ].binormal , glm::vec3( -1, 0, 0 ) );

	ASSERT_EQ( models[ 0 ].geometry.getIndexBuffer( ).size( ), 3 );

	EXPECT_EQ( models[ 0 ].geometry.getIndexBuffer( )[ 0 ], 0 );
	EXPECT_EQ( models[ 0 ].geometry.getIndexBuffer( )[ 1 ], 1 );
	EXPECT_EQ( models[ 0 ].geometry.getIndexBuffer( )[ 2 ], 2 );

	EXPECT_EQ( models[ 0 ].geometry.getTopology( ), visual::fx::TriangleList );

	
	EXPECT_EQ( models[ 0 ].material.name, "TEST" );
	EXPECT_EQ( models[ 0 ].material.map_Kd, "SingleTexelWhite" );
	EXPECT_EQ( models[ 0 ].material.normal, "SingleTexelBlack" );
	EXPECT_EQ( models[ 0 ].material.map_Ks, "SingleTexelWhite" );
	EXPECT_EQ( models[ 0 ].material.map_d , "SingleTexelWhite" );

	EXPECT_EQ( models[ 0 ].material.Ka, glm::vec3( 1, 1, 1 ) );
	EXPECT_EQ( models[ 0 ].material.Kd, glm::vec3( 1, 1, 1 ) );
	EXPECT_EQ( models[ 0 ].material.Ks, glm::vec3( 1, 1, 1 ) );
	EXPECT_FLOAT_EQ( models[ 0 ].material.Ns, 25.0f );
}
