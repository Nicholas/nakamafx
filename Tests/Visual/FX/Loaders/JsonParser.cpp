
#include <gtest/gtest.h>
#include <Visual/FX/Loaders/JsonParser.h>


class JsonParserTestClass final : public visual::fx::JsonParser
{
	public:
	explicit JsonParserTestClass( const tools::Scheduler& scheduler )
		: JsonParser( scheduler ) {}

	explicit JsonParserTestClass( const JsonParser& copy )
		: JsonParser( copy ) {}

	explicit JsonParserTestClass( JsonParser&& move )
		: JsonParser( move ) {}

	private:
	void parseJSON( const cJSONSPtr& pRoot ) override
	{
		pJson = pRoot;
	};

	public:
		virtual ~JsonParserTestClass( ) override = default;

		cJSONSPtr pJson;
};

class JsonParserTest : public ::testing::Test
{
	protected:
		JsonParserTest( ) = default;
		virtual ~JsonParserTest( ) = default;

		virtual void SetUp( ) override { }
		virtual void TearDown( ) override { }


};



TEST( JsonParserTest, ParsesAString )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({
		"meta":{},
		"TestString":"String"
	})";

	parser.tools::JsonParser::parse( testJson );

	std::string actual;
	const std::string expected = "String";
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	EXPECT_TRUE( parser.parse( actual, parser.pJson.get(), "TestString" ) );
	EXPECT_EQ( actual, expected );
}



TEST( JsonParserTest, ParsesABoolean )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({
		"meta":{},
		"TestBool":true
	})";

	parser.tools::JsonParser::parse( testJson );

	bool actual = false;
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestBool" ) );
	EXPECT_TRUE( actual );
}



TEST( JsonParserTest, ParsesAnInteger )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":2})";

	parser.tools::JsonParser::parse( testJson );

	int actual;
	const int expected = 2;
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses2Integers )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":[1,2]})";

	parser.tools::JsonParser::parse( testJson );

	glm::ivec2 actual;
	const glm::ivec2 expected( 1, 2 );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses3Integers )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":[1,2,3]})";

	parser.tools::JsonParser::parse( testJson );

	glm::ivec3 actual;
	const glm::ivec3 expected( 1, 2, 3 );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses4Integers )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":[1,2,3,4]})";

	parser.tools::JsonParser::parse( testJson );

	glm::ivec4 actual;
	const glm::ivec4 expected( 1, 2, 3, 4 );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}



TEST( JsonParserTest, ParsesAnUnsignedInteger )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":2})";

	parser.tools::JsonParser::parse( testJson );

	unsigned int actual;
	const unsigned int expected = 2;
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses2UnsignedIntegers )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":[1,2]})";

	parser.tools::JsonParser::parse( testJson );

	glm::uvec2 actual;
	const glm::uvec2 expected( 1, 2 );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses3UnsignedIntegers )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":[1,2,3]})";

	parser.tools::JsonParser::parse( testJson );

	glm::uvec3 actual;
	const glm::uvec3 expected( 1, 2, 3 );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses4UnsignedIntegers )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestInt":[1,2,3,4]})";

	parser.tools::JsonParser::parse( testJson );

	glm::uvec4 actual;
	const glm::uvec4 expected( 1, 2, 3, 4 );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestInt" ) );
	EXPECT_EQ( actual, expected );
}



TEST( JsonParserTest, ParsesAFloat )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestFloat":2.5})";

	parser.tools::JsonParser::parse( testJson );

	float actual;
	const float expected = 2.5f;
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestFloat" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses2Floats )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestFloat":[1.5,2.5]})";

	parser.tools::JsonParser::parse( testJson );

	glm::vec2 actual;
	const glm::vec2 expected( 1.5f, 2.5f );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestFloat" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses3Floats )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestFloat":[1.5,2.5,3.5]})";

	parser.tools::JsonParser::parse( testJson );

	glm::vec3 actual;
	const glm::vec3 expected( 1.5f, 2.5f, 3.5f );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestFloat" ) );
	EXPECT_EQ( actual, expected );
}

TEST( JsonParserTest, Parses4Floats )
{
	JsonParserTestClass parser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } );

	const std::string testJson = R"({"meta":{},"TestFloat":[1.5,2.5,3.5,4.5]})";

	parser.tools::JsonParser::parse( testJson );

	glm::vec4 actual;
	const glm::vec4 expected( 1.5f, 2.5f, 3.5f, 4.5f );
	
	EXPECT_FALSE( parser.parse( actual, parser.pJson.get(), "INVALID" ) );
	ASSERT_TRUE( parser.parse( actual, parser.pJson.get(), "TestFloat" ) );
	EXPECT_EQ( actual, expected );
}
  