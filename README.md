NaKama FX
============
---
Simply put, a 3D Rendering Engine.  
A renderer that draws some exciting things for the adventurous to explore and discover.
Currently built using DirectX 11 as the graphics API.

Please have a look at the [wiki] for more information and images about each feature.

The following image is a set of screen shots from the sample application scene.

![sample](https://bitbucket.org/Nicholas/nakamafx/wiki/Sample.jpg)

### Features

+ Physically Based Direct Lighting Model. 

+ Opaque/Transparent Cascasde Shadow Maps.
+ Partial Order Independent Translucency (A-Buffer).  
  ![Materials](https://bitbucket.org/Nicholas/nakamafx/wiki/Materials.jpg)
    + Subsurface Scattering.  
+ Forward+ Clustered Lighting.  
![Lights](https://bitbucket.org/Nicholas/nakamafx/wiki/Lights.jpg)
+ Text With Multi-channel Signed Distance Field Fonts (with SDF in the alpha channel).
+ GPU Partical System.  
![GPUP](https://bitbucket.org/Nicholas/nakamafx/wiki/GPUP.jpg)
+ Bloom.
+ Screen Space Ambiant Occlusion.
+ Ray March
    + Volumetric Fog & Analitical Height Fog.  
![Fog](https://bitbucket.org/Nicholas/nakamafx/wiki/Fog.jpg)
    + Screen Space Indirect Lighting.  
![IL](https://bitbucket.org/Nicholas/nakamafx/wiki/IL.jpg)
    + Screen Space Reflections.  
![SSR](https://bitbucket.org/Nicholas/nakamafx/wiki/SSR.jpg)
+ Texturing.
    + Bump Mapping.
    + Parralax Occlusion Mapping.  
![PO](https://bitbucket.org/Nicholas/nakamafx/wiki/PO.jpg)
    + Texture Blending.  
![TB](https://bitbucket.org/Nicholas/nakamafx/wiki/TB.jpg)
+ Dynamic Sky Box.
    + Atmospheric Scattering
    + Clouds (2D Simplex Noise)
    + Stars (3D Simplex Noise)
+ Parameter Driven Tree Generation.  
![Tree](https://bitbucket.org/Nicholas/nakamafx/wiki/Tree.jpg)

## Getting Started
### Requires

- [CMake][cmake]  
  CMake is used to generate the build files,
  it will also download other dependendent git repositories automatically during configuration.  
  3.8.0 is specified (some VS settings I wanted at the time) but I am currently using 3.14.4 now.
  
- [Git][git]  
  Not only useful for downloading this repo its also used by CMake to get our projects dependencies.  
  This will download the following, NaKama Tools with [googletest][gtest], [msdfgen], [cjson], [glm] and [gli].

- A C++ compiler for windows (DX11)  
  Tested on Windows 10 with Visual Studio 2017 / 2019.

### Includes

- [PrecompiledHeader.cmake][cmakepch] is included so that we can set the project to build with precompiled headers.
- [googletest][gtest]: The unit testing framework.
- [msdfgen]: Generate msdf and sdf bitmaps for text rendering.
- [cjson]: Parce json files.
- [glm]: The maths library.
- [gli]: Image loading with Vulkan.
- [DirecXTex]: Image loading with Direct X.
- [Dear ImGui]: An immediate mode graphical user interface toolkit.


### Projects

The solution contains a few projects that are used to create the demo applicaiton.
The repositories primary project is the NaKama-FX library which contains all the code for rendering.


#### Sample Application

- NaKama-Fx-Sample  
  A simple demo application that will construct a scene at startup to display a few of the features.  
  Set this as the 'startup project'.


#### Libraries

- NaKama-FX
- NaKama-Tools
- cjson
- gmock
- gtest
- gtest_main
- lib_msdfgen


#### Unit Tests

- NaKama-FX-Test
- NaKama-Tools-Test


[gtest]: https://github.com/google/googletest
[gtestsetup]: https://github.com/google/googletest/tree/master/googletest
[cmakepch]: https://github.com/larsch/cmake-precompiled-header
[cmake]: https://cmake.org/
[git]: https://git-scm.com/

[glm]:https://glm.g-truc.net/0.9.9/index.html
[gli]:http://gli.g-truc.net/0.8.2/index.html
[cjson]:https://github.com/DaveGamble/cJSON
[msdfgen]:https://github.com/Chlumsky/msdfgen
[DirecXTex]:https://github.com/microsoft/DirectXTex
[Dear ImGui]:https://github.com/ocornut/imgui
[wiki]:https://bitbucket.org/Nicholas/nakamafx/wiki
