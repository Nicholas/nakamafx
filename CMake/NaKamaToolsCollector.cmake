CMAKE_MINIMUM_REQUIRED( VERSION 2.8.2 )

PROJECT( NaKamaTools-download NONE )

INCLUDE( ExternalProject )

EXTERNALPROJECT_ADD( NaKamaTools
	GIT_REPOSITORY		git@bitbucket.org:Nicholas/nakamatools.git
	GIT_TAG				master
	SOURCE_DIR			"${CMAKE_BINARY_DIR}/NaKamaTools-src"
	BINARY_DIR			"${CMAKE_BINARY_DIR}/NaKamaTools-build"
	CONFIGURE_COMMAND	""
	BUILD_COMMAND		""
	INSTALL_COMMAND		""
	TEST_COMMAND		""
	UPDATE_COMMAND		""
	CMAKE_ARGS			-Wno-dev
)

