CMAKE_MINIMUM_REQUIRED( VERSION 2.8.2 )

PROJECT( msdfgen-download NONE )

INCLUDE( ExternalProject )

EXTERNALPROJECT_ADD( msdfgen
	GIT_REPOSITORY		git@bitbucket.org:Nicholas/msdfgen.git
	GIT_TAG				master
	SOURCE_DIR			"${CMAKE_BINARY_DIR}/msdfgen-src"
	BINARY_DIR			"${CMAKE_BINARY_DIR}/msdfgen-build"
	CONFIGURE_COMMAND	""
	BUILD_COMMAND		""
	INSTALL_COMMAND		""
	TEST_COMMAND		""
	UPDATE_COMMAND		""
	CMAKE_ARGS			-Wno-dev
)

