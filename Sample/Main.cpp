
#include "../Source/Visual/FX/FXFactory.h"
#include "../Source/Visual/FX/Settings.h"
#include "../Source/Visual/FX/SceneRenderer.h"
#include "../Source/Visual/FX/CommandDispatch.h"
#include "../Source/Visual/FX/Camera/Camera.h"
#include "../Source/Visual/FX/Camera/FreeFlyRig.h"
#include "../Source/Visual/FX/Loaders/SceneParser.h"
#include "../Source/Visual/FX/Generators/SceneGenerator.h"
#include "../Source/Visual/UserInterface/WindowMS.h"
#include "../Source/Visual/UserInterface/Input.h"
#include "../Source/Visual/UserInterface/WindowChange.h"
#include "../Source/Visual/UserInterface/SystemUI.h"

#include <Files/FileReader.h>
#include <Timing/Timer.h>

#include <intsafe.h>
#include <iostream>


namespace tools
{
	FORWARD_DECLARE( FileReader );
}

namespace visual
{
namespace fx
{
	FORWARD_DECLARE( SceneParser );
	FORWARD_DECLARE( SceneGenerator );
}
}

void runSample( );

int main( )
{
	runSample( );
	return 0;
}

void runSample( )
{
	visual::fx::FXFactory    factory;

	visual::fx::SettingsSPtr pSettings = factory.loadSettings( "../Bin/settings.json" );
	visual::fx::ExecutorSPtr pEngine   = factory.buildExecutor( pSettings );

	tools::Scheduler processScheduler  = [ &pEngine ]( const std::function< void( ) >& process )
	{
		pEngine->addAsync( std::make_shared< tools::FunctorProcess >( process ) );
	};

	tools::FileReaderSPtr pFileReader  = std::make_shared< tools::FileReader >( processScheduler );

	visual::ui::WindowSPtr pWindow     = factory.buildWindow( pSettings );
	visual::ui::InputSPtr  pInput      = factory.buildInputListerner( );
	visual::fx::CameraSet  cameraSet   = factory.buildCameraSet( pSettings, pInput );
	visual::ui::WindowChangeSPtr pWindowChange = factory.buildWindowChangeListener( );

	visual::ui::SystemUISPtr pUI = factory.buildSystemUI( pSettings, pInput );

	if( !pWindow->initialize( ) )
	{
		return;
	}

#if defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
	pWindow->add( [ pFileReader ]( LONG, LONG, const ::std::vector< ::std::string >& files )
	{
		for( const ::std::string& file : files )
		{
			pFileReader->readFile( file );
		}
	} );
#endif

	pWindow->add( pUI );
	pWindow->add( pInput );
	pWindow->add( pWindowChange );



	try
	{
		visual::fx::FXSystem system = factory.buildSystem(
			pSettings,
			cameraSet.pCamera,
			pWindow,
			pInput,
			pWindowChange, pUI );

		const visual::fx::SceneGeneratorSPtr pSceneGenerator = std::make_shared< visual::fx::SceneGenerator >( processScheduler, system.pSceneBuilder );
		const visual::fx::SceneParserSPtr pSceneParser = std::make_shared< visual::fx::SceneParser >( processScheduler, system.pSceneBuilder );
		pFileReader->add( ".json", pSceneParser );

		tools::Timer timer;
		timer.reset( );

		// TODO: Change -> Run the window update in a different thread and make the input, changes and file listener thread safe.
		// Tick => (( Update1 || Update2 ) => Scene Render ) || Dispatch Execute ) =>
		// Tick => (( Update1 || Update2 ) => Scene Render ) || Dispatch Execute ) =>

		visual::fx::ScheduledFunctorSPtr pTickUpdate = std::make_shared< visual::fx::ScheduledFunctor >(
			[ pEngine, &pWindow, &timer, &pInput, &pWindowChange, &system, &cameraSet, &pUI ]( )
		{
			pInput->readComplete( );
			if( pWindow->update( ) )
			{
				timer.tick( );

				if( pWindowChange->sizeChanged( ) )
				{
					cameraSet.pCamera->resize( 0, int( pWindowChange->getWidth( ) ), int( pWindowChange->getHeight( ) ) );
					std::cout << "[" << pWindowChange->getWidth( ) << ", " << pWindowChange->getHeight( ) << "]\n";
					system.updateRenderTargets( );

					pUI->setSize( { pWindowChange->getWidth( ), pWindowChange->getHeight( ) } );
				}

				system.updateScene( timer.getDelta( ) );

				cameraSet.pCameraRig->updatePosition( timer.getDelta( ) );
				cameraSet.pCamera->update( );

				pWindowChange->readComplete( );

				if( !pUI->update( timer.getDelta( ) ) )
				{
					pWindow->exit( );
				}

				system.pDispatch->resetCommandAllocators( );
			}
			else
			{
				pEngine->shutdown( );
			}
		} );

		visual::fx::ScheduledFunctorSPtr pDrawUpdate = std::make_shared< visual::fx::ScheduledFunctor >(
			[ &timer, &system ]( )
		{
			////////////////////////////////////////////
			// Work on the next frame we want to display
			system.pSceneRenderer->update( timer.getDelta( ) );
			system.pSceneRenderer->render( );
			////////////////////////////////////////////

			////////////////////////////////////////////
			// Work on the last frame
			system.pDispatch->executeCommandList( );
			system.pDispatch->flushCommandQueue( );
			////////////////////////////////////////////
		} );

		visual::fx::TaskObserverSPtr pDrawObserver = std::make_shared< visual::fx::TaskObserver >( pTickUpdate, pEngine, 0 );
		visual::fx::TaskObserverSPtr pTickObserver = std::make_shared< visual::fx::TaskObserver >( pDrawUpdate, pEngine, 0 );

		pDrawUpdate->addCompletedObserver( pDrawObserver );
		pTickUpdate->addCompletedObserver( pTickObserver );

		pDrawObserver->completedNotification( );

		pEngine->addAsync( std::make_shared< tools::FunctorProcess >( [ pSceneGenerator, pFileReader, pSettings ]( )
		{
			if( pSettings->getDefaultScene( ) == "DEMO" )
			{
				std::cout << "Building Demo Scene" << std::endl;
				pSceneGenerator->generateScene( );
			}
			else
			{
				std::cout << "Loading " << pSettings->getDefaultScene( ) << std::endl;
				pFileReader->readFile( pSettings->getDefaultScene( ) );
			}
		} ) );


		//////////
		// RUN //
		////////
		pEngine->run( );
		pEngine->join( );


		///////////////
		// SHUTDOWN //
		/////////////
		pWindow->shutdown( );

		// Delete the data explicitly before we delete the factory.
		pWindow->removeAllFileListeners( );

		pUI.reset( );
		system.pSceneBuilder.reset( );
		pFileReader.reset( );

		system.pSceneRenderer.reset( );
		system.pDispatch.reset( );
		pWindow.reset( );
	}
	catch( const ::std::runtime_error& e )
	{
		::std::cout << e.what( ) << ::std::flush;
	}
}