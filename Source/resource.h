#pragma once
// The NaKama-FX Resource Header Defines.
//
// Hear we define the resource ids that we can use to access the resource.
//
// The main resources planned here are shaders.
// We can include the shaders at build time so that we can
// cut down on shader complilation at run time.
// This will allow for a quicker start for faster iteration
// times.
//
// Project   : NaKama-Fx
// File Name : resource.h
// Date      : 15/05/2019
// Author    : Nicholas Welters

////////////////////////////
// Direct X - HLSL - CSO //
//////////////////////////
// Vertex Shaders 42'001'000
#define IDR_DX_VS_FULLSCREEN               42001000
#define IDR_DX_VS_PASS_THROUGH_PT          42001001
#define IDR_DX_VS_WORLD_PASS_THROUGH       42001002
#define IDR_DX_VS_MODEL                    42001003
#define IDR_DX_VS_MODEL_DEPTH              42001004
#define IDR_DX_VS_BLUR_H                   42001005
#define IDR_DX_VS_BLUR_V                   42001006
#define IDR_DX_VS_SKY                      42001007
#define IDR_DX_VS_SUN_VOLUME               42001008
#define IDR_DX_VS_PARTICLES_SO             42001009
#define IDR_DX_VS_MODEL_NORMALS            42001010
#define IDR_DX_VS_FONT_2D                  42001011
#define IDR_DX_VS_FONT_3D                  42001012
#define IDR_DX_VS_FONT_3D_SHADOW           42001013
#define IDR_DX_VS_PCG_HEIGHT               42001014
#define IDR_DX_VS_PCG_SHADOW               42001015
#define IDR_DX_VS_PCG_WATER                42001016
#define IDR_DX_VS_NUKLEAR_UI               42001017


// Geometry Shaders 88'002'000
#define IDR_DX_GS_SPHERE_BILLBOARD         42002000
#define IDR_DX_GS_CUBE                     42002001
#define IDR_DX_GS_PARTICLE_SO              42002002
#define IDR_DX_GS_PARTICLE_BILLBOARD       42002003
#define IDR_DX_GS_MODEL_NORMALS            42002004

// Tessellation Domain Shaders 42'003'000

// Tessellation Hull Shaders 42'004'000

// Pixel/Fragment Shaders 42'005'000
#define IDR_DX_PS_RESAMPLE                 42005000
#define IDR_DX_PS_FULLSCREEN_BLIT          42005001
#define IDR_DX_PS_HI_PASS                  42005002
#define IDR_DX_PS_BLUR                     42005003
#define IDR_DX_PS_SKY_POLAR_MAP            42005004
#define IDR_DX_PS_SKY_CUBE_MAP             42005005
#define IDR_DX_PS_SKY                      42005006
#define IDR_DX_PS_SPHERE                   42005007
#define IDR_DX_PS_SPHERE_PBR               42005008
#define IDR_DX_PS_PLANET                   42005009
#define IDR_DX_PS_MOON                     42005010
#define IDR_DX_PS_CUBE                     42005011
#define IDR_DX_PS_MODEL                    42005012
#define IDR_DX_PS_MODEL_PARALLAX           42005013
#define IDR_DX_PS_MODEL_HEIGHT             42005014
#define IDR_DX_PS_MODEL_WATER              42005015
#define IDR_DX_PS_MODEL_DEPTH_CLIPPED      42005016
#define IDR_DX_PS_MODEL_DEPTH_TRANSLUCENT  42005017
#define IDR_DX_PS_TRANSLUCENT_CONSTRUCT    42005018
#define IDR_DX_PS_TRANSLUCENT_RESOLVE      42005019
#define IDR_DX_PS_SSAO                     42005020
#define IDR_DX_PS_SSAO_BLUR_UP             42005021
#define IDR_DX_PS_SSR                      42005022
#define IDR_DX_PS_SSR_BLUR_UP              42005023
#define IDR_DX_PS_SUN_VOLUME               42005024
#define IDR_DX_PS_PARTICLE_BILLBOARD       42005025
#define IDR_DX_PS_DEBUG_RT                 42005026
#define IDR_DX_PS_MODEL_COLOUR             42005027
#define IDR_DX_PS_FONT_2D                  42005028
#define IDR_DX_PS_FONT_3D                  42005029
#define IDR_DX_PS_FONT_3D_SHADOW           42005030
#define IDR_DX_PS_MODEL_WATER_PCG          42005031
#define IDR_DX_PS_SSIL                     42005032
#define IDR_DX_PS_SSIL_BLUR_UP             42005033
#define IDR_DX_PS_TONE_MAPPING             42005034
#define IDR_DX_PS_NUKLEAR_UI               42005035

// Compute Shaders 42'06'000
#define IDR_DX_CS_ADAPT_LUMINANCE          42006000
#define IDR_DX_CS_CLUSTERING_BUILD_FRUSTUM 42006001
#define IDR_DX_CS_CLUSTERING_LIGHTS        42006002
#define IDR_DX_CS_TILE_HISTOGRAM           42006003
#define IDR_DX_CS_ACCUMULATE_HISTOGRAM     42006004
#define IDR_DX_CS_RESPONSE_CURVE           42006005


/////////////
// Vulkan //
///////////
// Vertex Shaders 401'000
//#define IDR_VK_VS_TRANSFORM_34_PC		401000

// Geometry Shaders 402'000

// Tessellation Domain Shaders 403'000

// Tessellation Hull Shaders 404'000

// Pixel/Fragment Shaders 405'000
//#define IDR_VK_FS_COLOUR_PC_1			405000

// Compute Shaders 406'000

