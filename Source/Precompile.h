
// Project   : NaKama-FX
// File Name : Precompile.h
// Date      : 20/11/2017
// Author    : Nicholas Welters

#ifndef _PRE_COMPILE_H
#define _PRE_COMPILE_H

#include <array>
#include <vector>
#include <map>
#include <unordered_map>

#include <atomic>
#include <mutex>
#include <condition_variable>
#include <thread>

#include <chrono>

#include <iostream>

#include <memory>

#include <string>

#include <utility>

#if defined( _WIN32 )

#define NOMINMAX 1
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

#ifdef ENABLE_DX_11_4
#define ENABLE_DX_11
#include <d3d11_4.h>
#elif defined ENABLE_DX_11_3
#define ENABLE_DX_11
#include <d3d11_3.h>
#elif defined ENABLE_DX_11_2
#define ENABLE_DX_11
#include <d3d11_2.h>
#elif defined ENABLE_DX_11_1
#define ENABLE_DX_11
#include <d3d11_1.h>
#elif defined ENABLE_DX_11_0
#define ENABLE_DX_11
#include <D3D11.h>
#endif

#ifdef ENABLE_DX_11

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"dxguid.lib")

#ifdef ENABLE_SHADER_RUNTIME_COMPILATION
#pragma comment(lib, "d3dcompiler.lib")
#endif

#endif // ENABLE_DX_11

#endif // _WIN32

#endif // _PRE_COMPILE_H
