#pragma once

// Window Interface class
//
// OO Window class for making and setting up a window.
//
// Project   : NaKama-Fx
// File Name : WindowXCB.h
// Date      : 13/12/2017
// Author    : Nicholas Welters

#ifndef _WINDOW_XCB_H
#define _WINDOW_XCB_H
#if defined( __linux__ )

#include <xcb/xcb.h>

#include <memory>
#include <string>
#include <list>

namespace visual
{
namespace ui
{
	enum KEY_CODES
	{
		KEY_ESCAPE = 0x9
	};

	class InputListener;
	typedef std::shared_ptr< InputListener > InputListenerSPtr;
	typedef std::list< InputListenerSPtr > InputListenerList;
	typedef InputListenerList::iterator InputListenerIterator;

	/// <summary>
	/// Window Interface class,
	/// OO Window class for making and setting up a window.
	/// </summary>
	class WindowXCB
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="title"> The window title. </param>
			/// <param name="width"> The window width. </param>
			/// <param name="height"> The window height. </param>
			WindowXCB( const std::string& title, uint16_t width, uint16_t height );

			/// <summary> dtor. </summary>
			virtual ~WindowXCB( );


			/// <summary> copy ctor. </summary>
			WindowXCB( const WindowXCB& ) = delete;

			/// <summary> move ctor. </summary>
			WindowXCB( WindowXCB&& ) = delete;

			/// <summary> copy operator. </summary>
			WindowXCB& operator=( const WindowXCB& ) = delete;

			/// <summary> move operator. </summary>
			WindowXCB&& operator=( WindowXCB&& ) = delete;


			/// <summary> Component initialization. </summary>
			/// <returns> True if a window was created, false if there was an issue. </returns>
			bool initialize( );

			/// <summary> Component shutdown and destruction. </summary>
			/// <returns> True if a window was destroyed, false if there was an issue. </returns>
			bool shutdown( );

			/// <summary> Component update. </summary>
			/// <returns> True if a window is still alive, false to stop rendering. </returns>
			bool update( );

			/// <summary> Force the update function to exit and initiate the shutdown. </summary>
			void exit( );

			/// <summary> Get window title. </summary>
			/// <returns> The window title. </returns>
			::std::string getWindowTitle();

			/// <summary> Get the window handle. </summary>
			/// <returns> The window handle. </returns>
			xcb_window_t getWindow( );

			/// <summary> Get the x server connection. </summary>
			/// <returns> The x server connection pointer. </returns>
			xcb_connection_t* getConnection( );

			/// <summary> Get the screen visual id. </summary>
			/// <returns> The screen visual id. </returns>
			xcb_visualid_t getVisualID( );


			/// <summary> Add an input listener. </summary>
			/// <param name="pListener"> A new input listener. </param>
			void      add( const InputListenerSPtr& pListener );

			/// <summary> Remove an input listener. </summary>
			/// <param name="pListener"> The input listener to remove. </param>
			bool   remove( const InputListenerSPtr& pListener );

			/// <summary> Check an input listener is registered. </summary>
			/// <param name="pListener"> The input listener to remove. </param>
			bool contains( const InputListenerSPtr& pListener );

		private:
			/// <summary> Open a connection to the x server. </summary>
			/// <returns> True if we got a xcb connection, false otherwise. </returns>
			bool initXcbConnection( );

			/// <summary> Create a window to display to and with event types. </summary>
			void setupWindow( );

			/// <summary> Respond to events read from the update loop. </summary>
			/// <param name="uiEvent"> The event that we are going to respond to. </param>
			void handleEvent( const xcb_generic_event_t *uiEvent );

		private:
			/// <summary> The window title. </summary>
			::std::string title;

			/// <summary> The window width. </summary>
			uint16_t width;

			/// <summary> The window height. </summary>
			uint16_t height;

			/// <summary> Update loop control so that we can read message events and render. </summary>
			bool quit;


			/// <summary> X server connection. </summary>
			xcb_connection_t* pConnection;

			/// <summary> The screen we are displaying on. </summary>
			xcb_screen_t* pScreen;

			/// <summary> The output window to render to. </summary>
			xcb_window_t windowHandle;

			/// <summary> The shutdown event callback type so that we can kill the window. </summary>
			xcb_intern_atom_reply_t* pAtom_wm_delete_window;


			/// <summary> The last position the mouse was on the X axis. </summary>
			long mouseX;

			/// <summary> The last position the mouse was on the X axis. </summary>
			long mouseY;


			/// <summary> Input listeners. </summary>
			InputListenerList inputListeners;
	};
} // namespace ui
} // namespace visual

# endif // __linux__
# endif // _WINDOW_XCB_H