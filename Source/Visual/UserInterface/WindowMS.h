# pragma once

/**
 * Windows window class
 *
 * OO Window class for making and setting up a window.
 *
 * @Project   : NaKama-Fx
 * @File Name : WindowMS.h
 * @Date      : 17/08/2014
 * @Author    : Nicholas Welters
 */

#ifndef _WINDOW_MS_H
#define _WINDOW_MS_H
#if defined( _WIN32 )

#include <memory>
#include <string>
#include <list>
#include <vector>
#include <functional>

#define NOMINMAX 1
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

namespace visual
{
namespace ui
{
	enum KEY_CODES
	{
		KEY_ESCAPE = VK_ESCAPE
	};
	
	class InputListener;
	typedef std::shared_ptr< InputListener > InputListenerSPtr;
	typedef std::list< InputListenerSPtr > InputListenerList;
	typedef InputListenerList::iterator InputListenerIterator;

	// TODO: Make a real interface so that we can check if the function is in the list (doesn't seem to like comparing functors).
	typedef std::function< void( LONG, LONG, const ::std::vector< ::std::string >& ) > FileInputListener;
	typedef std::list< FileInputListener > FileInputListenerList;
	typedef FileInputListenerList::const_iterator FileInputListenerIterator;

	class WindowChangeListener;
	typedef std::shared_ptr< WindowChangeListener > WindowChangeListenerSPtr;
	typedef std::list< WindowChangeListenerSPtr > WindowChangeListenerList;
	typedef WindowChangeListenerList::iterator WindowChangeListenerIterator;

	/// <summary>
	/// Window Interface class,
	/// OO Window class for making and setting up a window.
	/// </summary>
	class WindowMS final
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="hInstance"> The application instance. </param>
			/// <param name="title"> The window title. </param>
			/// <param name="width"> The window width. </param>
			/// <param name="height"> The window height. </param>
			/// <param name="fullscreen"> The window fullscreen state. </param>
			/// <param name="colourDepth"> The window colour depth. </param>
			WindowMS(
				HINSTANCE hInstance,
#ifdef UNICODE
				const std::wstring& title,
#else
				const std::string& title,
#endif
				uint16_t width, uint16_t height,
				bool fullscreen, uint16_t colourDepth
			);

			/// <summary> dtor. </summary>
			~WindowMS( );


			/// <summary> ctor. </summary>
			WindowMS( ) = delete;

			/// <summary> copy ctor. </summary>
			WindowMS( const WindowMS& ) = delete;

			/// <summary> move ctor. </summary>
			WindowMS( const WindowMS&& ) = delete;

			/// <summary> copy operator. </summary>
			WindowMS& operator=( const WindowMS& ) = delete;

			/// <summary> move operator. </summary>
			WindowMS&& operator=( WindowMS&& ) = delete;


			/// <summary> Component initialization. </summary>
			/// <returns> True if we created a window, false otherwise. </returns>
			bool initialize( );

			/// <summary> Component update. </summary>
			/// <returns> True if the window has completed reading updates and is ready to draw, false to shut down </returns>
			bool update( );

			/// <summary> Component shutdown and destruction. </summary>
			/// <returns> True if the window was destroyed ok. </returns>
			bool shutdown( );

			/// <summary> Force the update function to exit and initiate the shutdown. </summary>
			void exit( );

			/// <summary> Message handling function. </summary>
			/// <param name="hWndParam"> The window handel. </param>
			/// <param name="message"> The window message to process. </param>
			/// <param name="wParam"> Additional message information 1. </param>
			/// <param name="lParam"> Additional message information 2. </param>
			/// <returns> The return value is the result of the message processing and depends on the message sent.. </returns>
			LRESULT CALLBACK wndProc( HWND hWndParam, UINT message, WPARAM wParam, LPARAM lParam );

			/// <summary> Check if the window is active. </summary>
			bool isActive( ) const;


			// Input Listener Management.

			/// <summary> Attach an input listener. </summary>
			/// <param name="pListener"> The input listener to attach. </param>
			/// <returns> True if the listener was attached, false otherwise. </returns>
			void      add( const InputListenerSPtr& pListener );
			
			/// <summary> Remove an input listener. </summary>
			/// <param name="pListener"> The input listener to try and remove. </param>
			/// <returns> True if the listener was removed, false otherwise. </returns>
			bool   remove( const InputListenerSPtr& pListener );
			
			/// <summary> Check if an input listener is attached. </summary>
			/// <param name="pListener"> The input listener to look for. </param>
			/// <returns> True if the listener was found, false otherwise. </returns>
			bool contains( const InputListenerSPtr& pListener );


			// File Input Listener Management.

			/// <summary> Attach a file input listener. </summary>
			/// <param name="listener"> The file input listener to attach. </param>
			/// <returns> True if the listener was attached, false otherwise. </returns>
			void      add( const FileInputListener& listener );
			
			/// <summary> Remove a file input listener. </summary>
			/// <param name="listener"> The file input listener to try and remove. </param>
			/// <returns> True if the listener was removed, false otherwise. </returns>
			bool   remove( const FileInputListener& listener );
			
			/// <summary> Check if an input listener is attached. </summary>
			/// <param name="listener"> The file input listener to look for. </param>
			/// <returns> True if the listener was found, false otherwise. </returns>
			bool contains( const FileInputListener& listener );

			/// <summary> Remove all file input listeners. This is a fall back right now as functors don't seem to play nice with find. </summary>
			void removeAllFileListeners( );


			// Window Change Listener Management.

			/// <summary> Attach a window change listener. </summary>
			/// <param name="pListener"> The window change listener to attach. </param>
			/// <returns> True if the listener was attached, false otherwise. </returns>
			void      add( const WindowChangeListenerSPtr& pListener );
			
			/// <summary> Remove a window change listener. </summary>
			/// <param name="pListener"> The window change listener to try and remove. </param>
			/// <returns> True if the listener was removed, false otherwise. </returns>
			bool   remove( const WindowChangeListenerSPtr& pListener );
			
			/// <summary> Check if a window change is attached. </summary>
			/// <param name="pListener"> The window change to look for. </param>
			/// <returns> True if the listener was found, false otherwise. </returns>
			bool contains( const WindowChangeListenerSPtr& pListener );

			/// <summary> Get the hwnd so that we can attach the swap chain. </summary>
			/// <returns> A pointer to the window handle. </returns>
			HWND* getHWnd( );

		private:
			/// <summary> Build window information. </summary>
			/// <returns> True if the basic window information and functions have been registered. </returns>
			bool buildWindowInformation( );

			/// <summary> Create the window. </summary>
			/// <returns> True if we created a window, false otherwise. </returns>
			bool createWindow( );

			/// <summary> Process Raw Input. </summary>
			/// <param name="raw"> A pointer to the raw input data. </param>
			void processRawInput( RAWINPUT* raw );

			/// <summary> Process Raw Input. </summary>
			/// <param name="pBuf"> A buffer to hold the error string. </param>
			/// <param name="bufSize"> The buffers size. </param>
			/// <param name="error"> The error code. </param>
			/// <returns> A pointer to the buffer holding the error string. </returns>
			CHAR* getLastErrorText( CHAR *pBuf, ULONG bufSize, DWORD error ) const;

			/// <summary> Process Drag and Drop Input. </summary>
			/// <param name="hWndParam"> The window handel. </param>
			/// <param name="wParam"> The  event. </param>
			void processDnDInput( HWND hWndParam, WPARAM wParam );

			/// <summary> Process a change in the size of the output window. </summary>
			/// <param name="newWidth"> The new window width. </param>
			/// <param name="newHeight"> The new window height. </param>
			void processSizeChange( uint16_t newWidth, uint16_t newHeight );

		private:
			/// <summary> This application instance handle. </summary>
			HINSTANCE hInstance;

			/// <summary> The window title. </summary>
			::std::string title;

			/// <summary> The window width. </summary>
			uint16_t width;

			/// <summary> The window height. </summary>
			uint16_t height;

			/// <summary> The window fullscreen state. </summary>
			bool fullscreen;

			/// <summary> The number of bits per pixel. </summary>
			uint16_t colourDepth;

			/// <summary> The main window class name. </summary>
			std::string sClassName;

			/// <summary> Window Information </summary>
			WNDCLASSEX wcex;

			/// <summary> The Window object ID </summary>
			HWND hWnd;
			
			/// <summary> The windows active state on the system. </summary>
			bool  bActive;

			/// <summary> Local storage for the windows system message. </summary>
			MSG   msg;

			/// <summary> Local storage for mouse position to track the change in position. </summary>
			POINT point;

			/// <summary> Input listeners </summary>
			InputListenerList inputListeners;

			/// <summary> File Input listeners </summary>
			FileInputListenerList fileInputListeners;

			/// <summary> Window Change listeners </summary>
			WindowChangeListenerList windowChangeListeners;
	};

	/// <summary>
	///	Static handle on the window for message passing.
	/// Change to map so that we can support multiple windows of this class.
	///	</summary>
	static WindowMS* WindowHandle = nullptr;

	/// <summary> Message handler that pass the message to the window message handler. </summary>
	/// <param name="hWnd"> The window handel. </param>
	/// <param name="message"> The window message to process. </param>
	/// <param name="wParam"> Additional message information 1. </param>
	/// <param name="lParam"> Additional message information 2. </param>
	/// <returns> The return value is the result of the message processing and depends on the message sent.. </returns>
	static LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );
	
} // namespace ui
} // namespace visual

# endif // _WIN32
# endif // _WINDOW_MS_H