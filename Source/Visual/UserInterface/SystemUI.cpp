#include "SystemUI.h"
// User Interface Main Window.
//
//
// Project   : NaKama-Fx
// File Name : SystemUI.cpp
// Date      : 07/08/2019
// Author    : Nicholas Welters


#include "../FX/Vertex.h"
#include "Input.h"
#include "imgui/imgui.h"

namespace visual
{
namespace ui
{
	using std::min;
	using std::max;
	using std::abs;
	using std::vector;

	using glm::vec4;

	using visual::fx::Vertex3f2f4f;

	using fx::PSToneMappingBuffer;
	using fx::PSBlurBuffer;
	using fx::CSAdaptationBuffer;
	using fx::PSSunVolumeBuffer;
	using fx::PSABufferResolve;

	/// <summary> ctor. </summary>
	/// <param name="pInput"> The user input. </param>
	/// <param name="containerSize">The area where we can see and use the ui. </param>
	SystemUI::SystemUI( const InputSPtr& pInput, const glm::vec2& containerSize )
		: InputListener( )
		, pGPUData( nullptr )
		, pInput( pInput )
		, size( containerSize )
		, hidden( false )
	{
		ImGui::CreateContext();

		ImGuiIO& io = ImGui::GetIO();

		// Disable file loading and saving of window state.
		io.IniFilename = nullptr;

#if defined( _WIN32 )
		io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;         // We can honor GetMouseCursor() values (optional)
		io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;          // We can honor io.WantSetMousePos requests (optional, rarely used)
		io.BackendPlatformName = "NakamaFX_Win32";
		//io.ImeWindowHandle = hwnd;

		// Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array that we will update during the application lifetime.
		io.KeyMap[ ImGuiKey_Tab         ] = VK_TAB;
		io.KeyMap[ ImGuiKey_LeftArrow   ] = VK_LEFT;
		io.KeyMap[ ImGuiKey_RightArrow  ] = VK_RIGHT;
		io.KeyMap[ ImGuiKey_UpArrow     ] = VK_UP;
		io.KeyMap[ ImGuiKey_DownArrow   ] = VK_DOWN;
		io.KeyMap[ ImGuiKey_PageUp      ] = VK_PRIOR;
		io.KeyMap[ ImGuiKey_PageDown    ] = VK_NEXT;
		io.KeyMap[ ImGuiKey_Home        ] = VK_HOME;
		io.KeyMap[ ImGuiKey_End         ] = VK_END;
		io.KeyMap[ ImGuiKey_Insert      ] = VK_INSERT;
		io.KeyMap[ ImGuiKey_Delete      ] = VK_DELETE;
		io.KeyMap[ ImGuiKey_Backspace   ] = VK_BACK;
		io.KeyMap[ ImGuiKey_Space       ] = VK_SPACE;
		io.KeyMap[ ImGuiKey_Enter       ] = VK_RETURN;
		io.KeyMap[ ImGuiKey_Escape      ] = VK_ESCAPE;
		io.KeyMap[ ImGuiKey_KeyPadEnter ] = VK_RETURN;
		io.KeyMap[ ImGuiKey_A           ] = 'A';
		io.KeyMap[ ImGuiKey_C           ] = 'C';
		io.KeyMap[ ImGuiKey_V           ] = 'V';
		io.KeyMap[ ImGuiKey_X           ] = 'X';
		io.KeyMap[ ImGuiKey_Y           ] = 'Y';
		io.KeyMap[ ImGuiKey_Z           ] = 'Z';
#endif

		setTheme( );
	}

	/// <summary> Initialize the GPU data needed to render the UI. </summary>
	/// <param name="load"> Load A texture on the GPU. </param>
	void SystemUI::initGPUData( const LoadTexture& load, const GPUDataSPtr& pNewGPUData )
	{
		pGPUData = pNewGPUData;

		ImGuiIO& io = ImGui::GetIO();

		// Renderer
		io.BackendRendererName = "NakamaFX";
		io.BackendFlags |= ImGuiBackendFlags_RendererHasVtxOffset;  // We can honor the ImDrawCmd::VtxOffset field, allowing for large meshes.


		unsigned char* pixels;
		int width, height;
		io.Fonts->GetTexDataAsRGBA32( &pixels, &width, &height );


		vector< uint32_t > imageData( width * height );
		for( int i = 0, j = 0; i < width * height * 4; i += 4, j++ )
		{
			imageData[ j ] = ( pixels[ i + 3 ] << 24 )
						   | ( pixels[ i + 2 ] << 16 )
						   | ( pixels[ i + 1 ] << 8  )
						   | ( pixels[ i + 0 ]       );
		}

		io.Fonts->TexID = (ImTextureID)load( imageData, width, height );
	}

	SystemUI::~SystemUI( )
	{
		ImGui::DestroyContext( );
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	/// <param name="delta"> The time step. </param>
	/// <returns> True if the UI has updated and is ready to draw, false to shut down </returns>
	bool SystemUI::update( float delta )
	{
		if( !pGPUData ) return true;

		ImGuiIO& io = ImGui::GetIO( );

		pInput->setKeybourdUpdateState( !io.WantTextInput );

		io.DeltaTime = delta;      // set the time elapsed since the previous frame (in seconds)
		io.DisplaySize.x = size.x; // set the current display width
		io.DisplaySize.y = size.y; // set the current display height here

		ImGui::NewFrame( );

		if( hidden ) return true;

		ImGuiWindowFlags windowFlags = 0;

		static bool showDemo = false;
		bool ready = true;

		float zero = 0;
		float one = 1;
		float two = 2;
		float nOne = -1;

		ImGui::SetNextWindowPos(  {  15,  55 }, ImGuiCond_Once );
		ImGui::SetNextWindowSize( { 400, 350 }, ImGuiCond_Once );

		if( ImGui::Begin( "NaKama-FX", nullptr, windowFlags ) )
		{
			if( ImGui::CollapsingHeader( "Tone Mapping" ) )
			{
				PSToneMappingBuffer& toneMapping = pGPUData->getToneMapping( );

				float min = 0.00001f;
				float max = 2.0f;
				float linearMax = 20.0f;

				ImGui::SliderScalar( "Shoulder Strength", ImGuiDataType_Float, &toneMapping.shoulderStrength, &min, &max );

				ImGui::SliderScalar( "Linear Strength"  , ImGuiDataType_Float, &toneMapping.linearStrength  , &zero, &max );
				ImGui::SliderScalar( "Linear Angle"     , ImGuiDataType_Float, &toneMapping.linearAngle     , &zero, &max );
				ImGui::SliderScalar( "Linear White"     , ImGuiDataType_Float, &toneMapping.linearWhite     , &zero, &linearMax );

				ImGui::SliderScalar( "Toe Strength"     , ImGuiDataType_Float, &toneMapping.toeStrength     , &zero, &max );
				ImGui::SliderScalar( "Toe Numerator"    , ImGuiDataType_Float, &toneMapping.toeNumerator    , &zero, &max );
				ImGui::SliderScalar( "Toe Denominator"  , ImGuiDataType_Float, &toneMapping.toeDenominator  , &zero, &max );

				static bool showGraph = toneMapping.mode == 0x1;
				ImGui::Checkbox( "Show Graph View", &showGraph );
				toneMapping.mode = showGraph ? 0x1 : 0x0;

				if( ImGui::Button("Reset###Reset_ToneMapping" ) )
				{
					toneMapping.shoulderStrength =  0.22f;
					toneMapping.linearStrength   =  0.3f;
					toneMapping.linearAngle      =  0.1f;
					toneMapping.linearWhite      = 11.2f;
					toneMapping.toeStrength      =  0.2f;
					toneMapping.toeNumerator     =  0.01f;
					toneMapping.toeDenominator   =  0.3f;
				}
			}


			if( ImGui::CollapsingHeader( "Bloom" ) )
			{
				float maxBloom = 10.0f;
				static float bloomStrength = 0.0f;
				static float bloomSpread   = 1.0f;
				static float bloomFactor   = 1.0f;
				static vec4 weights( 0.25f, 0.5f, 0.75f, 1.0f );

				PSToneMappingBuffer& toneMapping = pGPUData->getToneMapping( );
				PSBlurBuffer& pBloomConsts = pGPUData->getBloom( );

				ImGui::SliderScalar( "Bloom Threshold", ImGuiDataType_Float, &pBloomConsts.threshold, &zero, &maxBloom );

				ImGui::SliderScalar( "Bloom Strength" , ImGuiDataType_Float, &bloomStrength, &zero, &two );
				ImGui::SliderScalar( "Bloom Spread"   , ImGuiDataType_Float, &bloomSpread  , &zero, &two );
				ImGui::SliderScalar( "Bloom Factor"   , ImGuiDataType_Float, &bloomFactor  , &zero, &two );

				ImGui::SliderScalar( "Bloom Weight 1" , ImGuiDataType_Float, &weights[ 0 ], &zero, &two );
				ImGui::SliderScalar( "Bloom Weight 2" , ImGuiDataType_Float, &weights[ 1 ], &zero, &two );
				ImGui::SliderScalar( "Bloom Weight 3" , ImGuiDataType_Float, &weights[ 2 ], &zero, &two );
				ImGui::SliderScalar( "Bloom Weight 4" , ImGuiDataType_Float, &weights[ 3 ], &zero, &two );

				if( ImGui::Button("Reset###Reset_Bloom" ) )
				{
					bloomStrength = 0.0f;
					bloomSpread   = 1.0f;
					bloomFactor   = 1.0f;
					weights       = { 0.25f, 0.5f, 0.75f, 1.0f };

					pBloomConsts.threshold = 3.5f;
				}

				toneMapping.bloomFactors = ( bloomSpread * weights + bloomStrength ) * bloomFactor;
			}


			if( ImGui::CollapsingHeader( "Gamma Correction" ) )
			{
				PSToneMappingBuffer& toneMapping = pGPUData->getToneMapping( );
				static float gamma = 1.0f / toneMapping.gamma;
				static float minGamma =  0.000001f;
				static float maxGamma = 10.0f;
				ImGui::SliderScalar( "1 / Gamma" , ImGuiDataType_Float, &gamma, &minGamma, &maxGamma );

				if( ImGui::Button("Reset###Reset_Gamma" ) )
				{
					gamma = 2.2f;
				}

				toneMapping.gamma = 1.0f / gamma;
			}


			if( ImGui::CollapsingHeader( "Exposure Adaption" ) )
			{
				CSAdaptationBuffer& adaption = pGPUData->getAdaptation( );

				static float min = 0.0f;
				static float max = 60.0f;
				ImGui::SliderScalar( "Increase Exposure Adaption Rate" , ImGuiDataType_Float, &adaption.TauD, &min, &max );
				ImGui::SliderScalar( "Decrease Exposure Adaption Rate" , ImGuiDataType_Float, &adaption.TauU, &min, &max );

				if( ImGui::Button("Reset###Reset_Exposure" ) )
				{
					adaption.TauU = 3.75f;
					adaption.TauD = 3.75f;
				}
			}


			if( ImGui::CollapsingHeader( "Fog" ) )
			{
				PSSunVolumeBuffer& fog = pGPUData->getFog( );
				PSABufferResolve& aBuffer = pGPUData->getABuffer( );

				static float min = 0.0f;
				static float max = 10.0f;
				static float minRayDepth = 16.0f;
				static float maxRayDepth = 512.0f;

				ImGui::SliderScalar( "Density"        , ImGuiDataType_Float, &fog.density      , &min, &max );
				ImGui::SliderScalar( "Height Falloff" , ImGuiDataType_Float, &fog.heightFalloff, &min, &max );
				ImGui::SliderScalar( "Ray Depth"      , ImGuiDataType_Float, &fog.rayDepth     , &minRayDepth, &maxRayDepth );
				ImGui::SliderScalar( "GScatter"       , ImGuiDataType_Float, &fog.gScatter     , &nOne, &one );

				static float minWaterDepth = -512.0f;
				static float maxWaterDepth = 512.0f;
				ImGui::SliderScalar( "Water Height" , ImGuiDataType_Float, &fog.waterHeight, &minWaterDepth, &maxWaterDepth );

				if( ImGui::Button("Reset###Reset_Fog" ) )
				{
					fog.density       = 0.0f;
					fog.heightFalloff = 0.2f;
					fog.rayDepth      = 128.0f;
					fog.gScatter      = 0.35f;
					fog.waterHeight   = -0.5f;
				}

				aBuffer.density = fog.density;
				aBuffer.heightFalloff = fog.heightFalloff;
			}


			if( ImGui::CollapsingHeader( "Toggle Effects" ) )
			{
				bool& ssr     = pGPUData->getUseSSR( );
				bool& ssil    = pGPUData->getUseSSIL( );
				bool& ssao    = pGPUData->getUseSSAO( );
				bool& oit     = pGPUData->getUseOIT( );
				bool& fog     = pGPUData->getUseFog( );
				bool& debug   = pGPUData->getUseDebug( );
				bool& normals = pGPUData->getUseDebugNormals( );
				bool& fps     = pGPUData->getUseDebugFPS( );
				bool& times   = pGPUData->getUseDebugTimes( );

				ImGui::Checkbox( "Enable SSR", &ssr );
				ImGui::Checkbox( "Enable SSIL", &ssil );
				ImGui::Checkbox( "Enable SSAO", &ssao );
				ImGui::Checkbox( "Enable OIT", &oit );
				ImGui::Checkbox( "Enable Fog", &fog );
				ImGui::Checkbox( "Enable Debug", &debug );
				ImGui::Checkbox( "Enable Normals", &normals );
				ImGui::Checkbox( "Enable FPS", &fps );
				ImGui::Checkbox( "Enable Times", &times );

				times = times && fps;
			}


			if( ImGui::CollapsingHeader( "UI Demo" ) )
			{
				ImGui::Checkbox( "Show Dear IMGUI Demo", &showDemo );

				if( ImGui::Button("Reset Theme" ) )
				{
					setTheme( );
				}
			}

			if( ImGui::Button("Exit" ) )
			{
				ready = false;
			}
		}

		ImGui::End( );

		if( showDemo )
		{
			ImGui::ShowDemoWindow( &showDemo );
		}

		return ready;
	}


	/// <summary> Update and process the user interface. </summary>
	/// <param name="pVertices"> A pointer to the vertex buffer to fill. </param>
	/// <param name="vertexCount"> The vertex buffer size. </param>
	/// <param name="pIndices"> A pointer to the index buffer to fill. </param>
	/// <param name="indexCount"> The index buffer size. </param>
	void SystemUI::fillBuffers(
		Vertex3f2f4f::Vertex* pVertices, const unsigned int vertexCount,
		void* pIndices , const unsigned int indexCount )
	{
		ImGui::Render();
		ImDrawData* draw_data = ImGui::GetDrawData();

		int vertexOffset = 0;
		ImDrawIdx* idx_dst = (ImDrawIdx*)pIndices;

		for (int n = 0; n < draw_data->CmdListsCount; n++)
		{
			const ImDrawList* cmd_list = draw_data->CmdLists[n];

			for( int i = 0; i < cmd_list->VtxBuffer.Size; i++ )
			{
				Vertex3f2f4f::Vertex& vertex = pVertices[ i + vertexOffset ];
				vertex.position.x = cmd_list->VtxBuffer[ i ].pos.x;
				vertex.position.y = cmd_list->VtxBuffer[ i ].pos.y;

				vertex.textureUV.x = cmd_list->VtxBuffer[ i ].uv.x;
				vertex.textureUV.y = cmd_list->VtxBuffer[ i ].uv.y;

				vertex.colour.a = float( ( cmd_list->VtxBuffer[ i ].col >> 24 ) & 0xff );
				vertex.colour.b = float( ( cmd_list->VtxBuffer[ i ].col >> 16 ) & 0xff );
				vertex.colour.g = float( ( cmd_list->VtxBuffer[ i ].col >> 8  ) & 0xff );
				vertex.colour.r = float( ( cmd_list->VtxBuffer[ i ].col       ) & 0xff );

				vertex.colour /= 255.0f;
			}

			memcpy(idx_dst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
			vertexOffset += cmd_list->VtxBuffer.Size;
			idx_dst += cmd_list->IdxBuffer.Size;
		}
	}

	/// <summary> Use the draw callback to trigger a draw in the renderer to render the UI. </summary>
	/// <param name="draw"> The renderer draw call back. </param>
	void SystemUI::draw( const SystemUI::DrawCallBack& draw )
	{
		ImDrawData* draw_data = ImGui::GetDrawData();
		ImVec2 pos = draw_data->DisplayPos;
		uint32_t vBufferOffset = 0;
		uint32_t iBufferOffset = 0;

		for (int n = 0; n < draw_data->CmdListsCount; n++)
		{
			const ImDrawList* cmd_list = draw_data->CmdLists[n];
			for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
			{
				const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
				if (pcmd->UserCallback)
				{
					pcmd->UserCallback(cmd_list, pcmd);
				}
				else
				{
					draw( pcmd->TextureId,
						  {
							pcmd->ClipRect.x - pos.x,
							pcmd->ClipRect.z - pos.x,
							pcmd->ClipRect.y - pos.y,
							pcmd->ClipRect.w - pos.y
						  },
						  pcmd->ElemCount,
						  pcmd->IdxOffset + iBufferOffset,
						  pcmd->VtxOffset + vBufferOffset );
				}
			}
			iBufferOffset += cmd_list->IdxBuffer.Size;
			vBufferOffset += cmd_list->VtxBuffer.Size;
		}
	}

	/// <summary> Update the containers/windows size. </summary>
	/// <param name="containerSize">The area where we can see and use the ui. </param>
	void SystemUI::setSize( const glm::vec2& containerSize )
	{
		size = containerSize;

		if( !pGPUData ) return;

		pGPUData->getUIBuffer( ).transform = { 2.0 / size.x, -2.0 / size.y, 0, 0 };
	}

	/// <summary> Register a key pressed event. </summary>
	/// <param name="key"> The key pressed. </param>
	void SystemUI::keyPressed( int key )
	{
		if( key < 256 )
			ImGui::GetIO( ).KeysDown[ key ] = true;

		if( key == ImGui::GetIO( ).KeyMap[ ImGuiKey_Escape ] )
			toggleVisablity( );
	}

	/// <summary> Register a key released event. </summary>
	/// <param name="key"> The key released. </param>
	void SystemUI::keyReleased( int key )
	{
		if( key < 256 )
			ImGui::GetIO( ).KeysDown[ key ] = false;
	}

	/// <summary> Register a characture key pressed event. </summary>
	/// <param name="key"> The key released. </param>
	void SystemUI::charKeyPressed( int key )
	{
		ImGui::GetIO( ).AddInputCharacter( key );
	}

	/// <summary> Register a mouse moved event. </summary>
	/// <param name="x"> The change in x. </param>
	/// <param name="y"> The change in y. </param>
	/// <param name="xPos"> The x position of the mouse on screen. </param>
	/// <param name="yPos"> The y position of the mouse on screen. </param>
	void SystemUI::mouseMoved( long x, long y, long xPos, long yPos )
	{
		ImGui::GetIO( ).MousePos = { float( xPos ), float( yPos ) };
	}

	/// <summary> Register a mouse button pressed event. </summary>
	/// <param name="button"> The button pressed. </param>
	void SystemUI::mousePressed( int button )
	{
		if( button < 6 )
			ImGui::GetIO( ).MouseDown[ button ] = true;
	}

	/// <summary> Register a mouse button released event. </summary>
	/// <param name="button"> The button pressed. </param>
	void SystemUI::mouseReleased( int button )
	{
		if( button < 6 )
			ImGui::GetIO( ).MouseDown[ button ] = false;
	}

	/// <summary> Register a mouse wheel move event. </summary>
	void SystemUI::mouseWheelMoved( long clicks )
	{
		ImGui::GetIO( ).MouseWheel += float( clicks );
	}

	void SystemUI::toggleVisablity( )
	{
		hidden = !hidden;
	}


	/// <summary> Set the UI theme. </summary>
	void SystemUI::setTheme( )
	{
		ImGuiStyle* style = &ImGui::GetStyle();
		ImVec4* colors = style->Colors;

		ImVec4 orange      = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
		ImVec4 title       = ImVec4(0.900f, 0.291f, 0.000f, 1.000f);
		ImVec4 base        = ImVec4(0.180f, 0.180f, 0.180f, 1.000f);
		ImVec4 baseHovered = ImVec4(0.220f, 0.220f, 0.220f, 1.000f);
		ImVec4 baseActive  = ImVec4(0.300f, 0.300f, 0.300f, 1.000f);

		ImVec4 headerBase    = ImVec4(0.213f, 0.213f, 0.213f, 1.000f);
		ImVec4 headerHovered = ImVec4(0.369f, 0.369f, 0.369f, 1.000f);


		ImVec4 window = ImVec4(0.100f, 0.100f, 0.100f, 1.000f);


		colors[ImGuiCol_Text]                   = ImVec4(1.000f, 1.000f, 1.000f, 0.900f);
		colors[ImGuiCol_TextDisabled]           = ImVec4(0.500f, 0.500f, 0.500f, 0.900f);
		colors[ImGuiCol_WindowBg]               = ImVec4(0.125f, 0.125f, 0.125f, 1.000f);
		colors[ImGuiCol_ChildBg]                = ImVec4(0.280f, 0.280f, 0.280f, 0.000f);
		colors[ImGuiCol_PopupBg]                = ImVec4(0.313f, 0.313f, 0.313f, 1.000f);
		colors[ImGuiCol_Border]                 = ImVec4(0.000f, 0.000f, 0.000f, 0.300f);
		colors[ImGuiCol_BorderShadow]           = ImVec4(0.000f, 0.000f, 0.000f, 0.000f);
		colors[ImGuiCol_FrameBg]                = base;
		colors[ImGuiCol_FrameBgHovered]         = baseHovered;
		colors[ImGuiCol_FrameBgActive]          = ImVec4(0.280f, 0.280f, 0.280f, 1.000f);
		colors[ImGuiCol_TitleBg]                = window;
		colors[ImGuiCol_TitleBgActive]          = title;
		colors[ImGuiCol_TitleBgCollapsed]       = window;
		colors[ImGuiCol_MenuBarBg]              = window;
		colors[ImGuiCol_ScrollbarBg]            = window;
		colors[ImGuiCol_ScrollbarGrab]          = baseHovered;
		colors[ImGuiCol_ScrollbarGrabHovered]   = headerHovered;
		colors[ImGuiCol_ScrollbarGrabActive]    = orange;
		colors[ImGuiCol_CheckMark]              = orange;
		colors[ImGuiCol_SliderGrab]             = ImVec4(0.391f, 0.391f, 0.391f, 1.000f);
		colors[ImGuiCol_SliderGrabActive]       = orange;
		colors[ImGuiCol_Button]                 = base;
		colors[ImGuiCol_ButtonHovered]          = baseHovered;
		colors[ImGuiCol_ButtonActive]           = orange;
		colors[ImGuiCol_Header]                 = headerBase;
		colors[ImGuiCol_HeaderHovered]          = headerHovered;
		colors[ImGuiCol_HeaderActive]           = orange;
		colors[ImGuiCol_Separator]              = colors[ImGuiCol_Border];
		colors[ImGuiCol_SeparatorHovered]       = ImVec4(0.391f, 0.391f, 0.391f, 1.000f);
		colors[ImGuiCol_SeparatorActive]        = orange;
		colors[ImGuiCol_ResizeGrip]             = ImVec4(1.000f, 1.000f, 1.000f, 0.250f);
		colors[ImGuiCol_ResizeGripHovered]      = ImVec4(1.000f, 1.000f, 1.000f, 0.670f);
		colors[ImGuiCol_ResizeGripActive]       = orange;
		colors[ImGuiCol_Tab]                    = base;
		colors[ImGuiCol_TabHovered]             = baseActive;
		colors[ImGuiCol_TabActive]              = orange;
		colors[ImGuiCol_TabUnfocused]           = ImVec4(0.098f, 0.098f, 0.098f, 1.000f);
		colors[ImGuiCol_TabUnfocusedActive]     = ImVec4(0.195f, 0.195f, 0.195f, 1.000f);
		//colors[ImGuiCol_DockingPreview]         = ImVec4(1.000f, 0.391f, 0.000f, 0.781f);
		//colors[ImGuiCol_DockingEmptyBg]         = ImVec4(0.180f, 0.180f, 0.180f, 1.000f);
		colors[ImGuiCol_PlotLines]              = orange;
		colors[ImGuiCol_PlotLinesHovered]       = ImVec4(0.469f, 0.469f, 0.469f, 1.000f);
		colors[ImGuiCol_PlotHistogram]          = orange;
		colors[ImGuiCol_PlotHistogramHovered]   = ImVec4(0.586f, 0.586f, 0.586f, 1.000f);
		colors[ImGuiCol_TextSelectedBg]         = ImVec4(1.000f, 1.000f, 1.000f, 0.156f);
		colors[ImGuiCol_DragDropTarget]         = orange;
		colors[ImGuiCol_NavHighlight]           = orange;
		colors[ImGuiCol_NavWindowingHighlight]  = orange;
		colors[ImGuiCol_NavWindowingDimBg]      = ImVec4(0.000f, 0.000f, 0.000f, 0.586f);
		colors[ImGuiCol_ModalWindowDimBg]       = ImVec4(0.000f, 0.000f, 0.000f, 0.586f);

		style->ChildRounding     = 4.0f;
		style->FrameBorderSize   = 0.0f;
		style->FrameRounding     = 2.0f;
		style->GrabMinSize       = 7.0f;
		style->PopupRounding     = 2.0f;
		style->ScrollbarRounding = 12.0f;
		style->ScrollbarSize     = 13.0f;
		style->TabBorderSize     = 0.0f;
		style->TabRounding       = 2.0f;
		style->GrabRounding      = 2.0f;
		style->WindowRounding    = 4.0f;

		style->WindowBorderSize = 0.0f;
	}

} // namespace directX11
} // namespace visual
