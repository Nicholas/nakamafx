
// Windows window class
//
// OO Window class for making and setting up a window.
//
// Project   : NaKama-Fx
// File Name : WindowMS.cpp
// Date      : 17-08-2014
// Author    : Nicholas Welters
#if defined( _WIN32 )

#include "WindowMS.h"
#include "InputListener.h"
#include "WindowChangeListener.h"

#include <shellapi.h>


namespace visual
{
namespace ui
{
	using std::vector;
	using std::string;
	using std::wstring;

	/// <summary> ctor. </summary>
	/// <param name="hInstance"> The application instance. </param>
	/// <param name="title"> The window title. </param>
	/// <param name="width"> The window width. </param>
	/// <param name="height"> The window height. </param>
	/// <param name="fullscreen"> The window fullscreen state. </param>
	/// <param name="colourDepth"> The window colour depth. </param>
	WindowMS::WindowMS(
		HINSTANCE hInstance,
#ifdef UNICODE
		const wstring& title,
#else
		const string& title,
#endif
		const uint16_t width, const uint16_t height,
		const bool fullscreen, const uint16_t colourDepth
	)
		: hInstance( hInstance )
		, title( title )
		, width( width )
		, height( height )
		, fullscreen( fullscreen )
		, colourDepth( colourDepth )
		, sClassName( "NaKamaDisplay" ) // Set the main window class registration name.
		, wcex( )
		, hWnd( nullptr )
		, bActive( false )
		, msg( )
		, point( )
		, inputListeners( )
		, fileInputListeners( )
		, windowChangeListeners( )
	{
		// -----------------------------------------------------------------------------------
		// Static handle on the window for message passing
		// -----------------------------------------------------------------------------------
		WindowHandle = this;
	}

	/// <summary> dtor. </summary>
	WindowMS::~WindowMS( )
	{
		if( !shutdown( ) )
		{
			printf( "Window::Destructor - Shutdown Exception\n" );
		}

		WindowHandle = nullptr;
	}

	/// <summary> Build window information. </summary>
	/// <returns> True if the basic window information and functions have been registered. </returns>
	bool WindowMS::buildWindowInformation( )
	{
		// -------------------------------------------
		// Window information
		// -------------------------------------------
		// clear out the window class memory.
		// pointer to memory location.
		// size to clear.
		ZeroMemory( &wcex, sizeof( WNDCLASSEX ) );

		wcex.cbSize        = sizeof( WNDCLASSEX );				// Specifies the size, in bytes, of this structure.
		wcex.style         = CS_HREDRAW | CS_VREDRAW;			// Specifies the class style(s).
		wcex.lpfnWndProc   = WndProc;							// Pointer to the window message procedure.
		wcex.cbClsExtra    = 0;									// The number of extra bytes to allocate following the window-class structure.
		wcex.cbWndExtra    = 0;									// The number of extra bytes to allocate following the window instance.
		wcex.hInstance     = hInstance;							// Instance that contains the window procedure for the class.
		wcex.hIcon         = LoadIcon( nullptr, IDI_WINLOGO );	// Icon resource.
		wcex.hCursor       = LoadCursor( nullptr, IDC_ARROW );	// Cursor resource.
		wcex.hbrBackground = HBRUSH( COLOR_WINDOW + 1 );		// Physical brush to be used for painting the background.
		wcex.lpszMenuName  = nullptr;							// Null-terminated character string that specifies the resource name of the class menu
		wcex.lpszClassName = sClassName.c_str( );				// Specifies the window class name
		wcex.hIconSm	   = LoadIcon( nullptr, IDI_WINLOGO );	// Icon that is associated with the window class.
		//wcex.hIcon       = LoadIcon( sp_settings->getHInstance( ), MAKEINTRESOURCE( IDI_APPLICATION ) );
		//wcex.hIconSm	   = LoadIcon( wcex.hInstance, MAKEINTRESOURCE( IDI_APPLICATION ) );

		// -------------------------------------------
		// Window Registration
		// -------------------------------------------
		if( !RegisterClassEx( &wcex ) )
		{
			CHAR msgText[ 256 ];
			const DWORD error = GetLastError( );
			getLastErrorText( msgText, sizeof( msgText ), error );

			printf( "Window::buildWindowInformation - Call to RegisterClassEx failed with error code = %d\n%s", error, msgText );

			shutdown( );

			return false;
		}

		return true;
	}

	/// <summary> Create the window. </summary>
	/// <returns> True if we created a window, false otherwise. </returns>
	bool WindowMS::createWindow( )
	{
		if( fullscreen )
		{
			DEVMODE dmScreenSettings; // Device Mode

			// Makes Sure Memory's Cleared
			memset( &dmScreenSettings, 0, sizeof( dmScreenSettings ) );

			// Size Of The Dev mode Structure
			dmScreenSettings.dmSize       = sizeof( dmScreenSettings );
			dmScreenSettings.dmPelsWidth  = width;		 // Selected Screen Width
			dmScreenSettings.dmPelsHeight = height;	     // Selected Screen Height
			dmScreenSettings.dmBitsPerPel = colourDepth; // Selected Bits Per Pixel
			dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

			// Try To Set Selected Mode And Get Results.
			// NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
			if( ChangeDisplaySettings( &dmScreenSettings, CDS_FULLSCREEN ) != DISP_CHANGE_SUCCESSFUL )
			{
				printf( "The Requested Full Screen Mode Is Not Supported By\nYour Video Card. Using Windowed Mode instead\n" );
				fullscreen = false;
			}
		}

		DWORD windowStyleFlags;
		DWORD windowExStyleFlags;

		// Test if full screen is active
		if( fullscreen )
		{
			windowStyleFlags   = WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_POPUP;
			windowExStyleFlags = WS_EX_APPWINDOW;
			//ShowCursor( false ); // Hide Mouse Pointer
		}
		else
		{
			windowStyleFlags   = WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_OVERLAPPEDWINDOW;
			windowExStyleFlags = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		}

		// Set up window dimensions
		RECT windowDimensions = { 0, 0, width, height };

		// Alters the window dimensions so the display area remains as asked for.
		AdjustWindowRectEx( &windowDimensions, windowStyleFlags, false, windowExStyleFlags );

		// Set Window dimensions
		width  = static_cast< uint32_t >( windowDimensions.right  - windowDimensions.left );
		height = static_cast< uint32_t >( windowDimensions.bottom - windowDimensions.top  );


		// -------------------------------------------
		// Create Window
		// -------------------------------------------
		hWnd = CreateWindowEx
		(
			windowExStyleFlags,	 // Specify the extended window style of the window being created.
			sClassName.c_str( ), // The name of the application.
			title.c_str( ),		 // The text that appears in the title bar.
			windowStyleFlags,	 // The type of window to create.
			CW_USEDEFAULT,		 // X-position of the window ( int ).
			CW_USEDEFAULT,		 // Y-position of the window ( int ).
			width,				 // Width of the window.
			height,				 // Height of the window.
			nullptr,				 // The parent of this window.
			nullptr,				 // Use of a menu bar for the window.
			hInstance,			 // Application ID.
			nullptr				 // Used with multiple windows.
		);

		if( !hWnd )
		{
			CHAR msgText[ 256 ];
			const DWORD error = GetLastError( );
			getLastErrorText( msgText, sizeof( msgText ), error );
			printf( "Window::createWindow - Call to CreateWindow failed with error code = %d\n%s\n", error, msgText );

			shutdown( );

			return false;
		}

		return true;
	}

	/// <summary> Component initialization. </summary>
	/// <returns> True if we created a window, false otherwise. </returns>
	bool WindowMS::initialize( )
	{
		// -------------------------------------------
		// Window information
		// -------------------------------------------
		if( !buildWindowInformation( ) )
		{
			printf( "Window::initialize( ) - Call to buildWindowInformation failed!" );
			shutdown( );

			return false;
		}

		// -------------------------------------------
		// Window creation.
		// -------------------------------------------
		if( !createWindow( ) )
		{
			printf( "Window::initialize( ) - Call to createWindow failed!" );
			shutdown( );

			return false;
		}


		RAWINPUTDEVICE Rid[ 2 ];

		Rid[ 0 ].usUsagePage = 0x01;
		Rid[ 0 ].usUsage = 0x02;
		Rid[ 0 ].dwFlags = 0;
		//Rid[ 0 ].dwFlags = RIDEV_NOLEGACY;   // adds HID mouse and also ignores legacy mouse messages
		Rid[ 0 ].hwndTarget = hWnd;

		Rid[ 1 ].usUsagePage = 0x01;
		Rid[ 1 ].usUsage = 0x06;
		Rid[ 1 ].dwFlags = 0;
		//Rid[ 1 ].dwFlags = RIDEV_NOLEGACY;   // adds HID keyboard and also ignores legacy keyboard messages
		Rid[ 1 ].hwndTarget = hWnd;

		if( !RegisterRawInputDevices( Rid, 2, sizeof( Rid[ 0 ] ) ) )
		{
			printf( "Window::initialize( ) - Call to RegisterRawInputDevices failed!" );
			shutdown( );

			return false;
		}

		// -------------------------------------------
		// Show Window and Set Focus
		// -------------------------------------------
		ShowWindow( hWnd, SW_SHOW );
		SetForegroundWindow( hWnd );
		SetFocus( hWnd );

		bActive = true;

		return true;
	}

	/// <summary> Process Raw Input. </summary>
	/// <param name="pBuf"> A buffer to hold the error string. </param>
	/// <param name="bufSize"> The buffers size. </param>
	/// <param name="error"> The error code. </param>
	/// <returns> A pointer to the buffer holding the error string. </returns>
	CHAR* WindowMS::getLastErrorText( CHAR *pBuf, const ULONG bufSize, const DWORD error ) const
	{
		LPTSTR pTemp = nullptr;

		if( bufSize < 16 )
		{
			if( bufSize > 0 )
			{
				pBuf[ 0 ] = '\0';
			}
			return( pBuf );
		}

		const DWORD retSize = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_ARGUMENT_ARRAY,
			nullptr,
			error,
			LANG_NEUTRAL,
			LPTSTR( &pTemp ),
			0,
			nullptr );

		if( !retSize || pTemp == nullptr )
		{
			pBuf[ 0 ] = '\0';
		}
		else
		{
			pTemp[ strlen( pTemp ) - 2 ] = '\0'; //remove cr and newline character
			sprintf_s( pBuf, 256, "%0.*s (0x%x)", bufSize - 16, pTemp, GetLastError( ) );
			LocalFree( HLOCAL( pTemp ) );
		}
		return( pBuf );
	}

	/// <summary> Component shutdown and destruction. </summary>
	/// <returns> True if the window was destroyed ok. </returns>
	bool WindowMS::shutdown( )
	{
		bActive = false;
		bool result = true;

		if( hWnd )
		{
			if( !ShowWindow( hWnd, SW_HIDE ) )
			{
				printf( "Window::shutdown( ) - Could Not Hide hWnd.\n" );

			}

			if( fullscreen )
			{
				ChangeDisplaySettings( nullptr, 0 ); // If So Switch Back To The Desktop
			}

			if( !DestroyWindow( hWnd ) )
			{
				printf( "Window::shutdown( ) - Could Not Release hWnd.\n" );

				CHAR msgText[ 256 ];
				getLastErrorText( msgText, sizeof( msgText ), GetLastError( ) );
				printf( "%s\n", msgText );

				result = false;
			}

			if( !UnregisterClass( sClassName.c_str( ), hInstance ) )
			{
				printf( "Window::shutdown( ) - Could Not Unregister Class.\n" );

				result = false;
			}

			hWnd = nullptr;
		}

		return result;
	}

	/// <summary> Component update. </summary>
	/// <returns> True if the window has completed reading updates and is ready to draw, false to shut down </returns>
	bool WindowMS::update( )
	{
		// -------------------------------------------
		// Message Listener
		// -------------------------------------------
		while( WM_QUIT != msg.message )
		{
			if( PeekMessage( &msg, nullptr, 0, 0, PM_REMOVE ) )
			{
				TranslateMessage( &msg ); // Translates virtual-key messages into character messages.
				DispatchMessage(  &msg ); // Used by channels to dispatch incoming remote calls.
			}
			else
			{
				// -------------------------------------------
				// If the window is active do some drawing.
				// Not working completely as expected.
				// -------------------------------------------
				if( isActive( ) )
				{
					return true;
				}

				Sleep( 50 );
			}
		}

		return false;
	}

	/// <summary> Check if the window is active. </summary>
	bool WindowMS::isActive( ) const
	{
		return bActive;
	}

	/// <summary> Force the update function to exit and initiate the shutdown. </summary>
	void WindowMS::exit( )
	{
		PostQuitMessage( 0 );
	}

	// Input Listener Management.

	/// <summary> Attach an input listener. </summary>
	/// <param name="pListener"> The input listener to attach. </param>
	/// <returns> True if the listener was attached, false otherwise. </returns>
	void WindowMS::add(const InputListenerSPtr& pListener)
	{
		inputListeners.push_back( pListener );
	}

	/// <summary> Remove an input listener. </summary>
	/// <param name="pListener"> The input listener to try and remove. </param>
	/// <returns> True if the listener was removed, false otherwise. </returns>
	bool WindowMS::remove(const InputListenerSPtr& pListener)
	{
		InputListenerIterator pos = inputListeners.end( );

		for( InputListenerIterator i = inputListeners.begin( ); i != inputListeners.end( ); ++i )
		{
			if( ( *i ) == pListener )
			{
				inputListeners.erase( i );
				return true;
			}
		}

		return false;
	}

	/// <summary> Check if an input listener is attached. </summary>
	/// <param name="pListener"> The input listener to look for. </param>
	/// <returns> True if the listener was found, false otherwise. </returns>
	bool WindowMS::contains(const InputListenerSPtr& pListener)
	{
		return find( inputListeners.begin( ), inputListeners.end( ), pListener ) != inputListeners.end( );
	}


	// File Input Listener Management.

	/// <summary> Attach a file input listener. </summary>
	/// <param name="listener"> The file input listener to attach. </param>
	/// <returns> True if the listener was attached, false otherwise. </returns>
	void WindowMS::add( const FileInputListener& listener )
	{
		fileInputListeners.push_back( listener );
	}

	/// <summary> Remove a file input listener. </summary>
	/// <param name="listener"> The file input listener to try and remove. </param>
	/// <returns> True if the listener was removed, false otherwise. </returns>
	bool WindowMS::remove( const FileInputListener& listener )
	{
		//FileInputListenerIterator i = find( fileInputListeners.begin( ), fileInputListeners.end( ), listener );
		//
		//if( i != fileInputListeners.end( ) )
		//{
		//	fileInputListeners.erase( i );
		//	return true;
		//}

		return false;
	}

	/// <summary> Remove a file input listener. </summary>
	/// <param name="listener"> The file input listener to try and remove. </param>
	/// <returns> True if the listener was removed, false otherwise. </returns>
	bool WindowMS::contains( const FileInputListener& listener )
	{
		return false;// find( fileInputListeners.begin( ), fileInputListeners.end( ), listener ) != fileInputListeners.end( );
	}

	/// <summary> Remove all file input listeners. This is a fall back right now as functors don't seem to play nice with find. </summary>
	void WindowMS::removeAllFileListeners( )
	{
		fileInputListeners.clear( );
	}


	// Window Change Listener Management.

	/// <summary> Attach a window change listener. </summary>
	/// <param name="pListener"> The window change listener to attach. </param>
	/// <returns> True if the listener was attached, false otherwise. </returns>
	void WindowMS::add( const WindowChangeListenerSPtr& pListener )
	{
		windowChangeListeners.push_back( pListener );
	}

	/// <summary> Remove a window change listener. </summary>
	/// <param name="pListener"> The window change listener to try and remove. </param>
	/// <returns> True if the listener was removed, false otherwise. </returns>
	bool WindowMS::remove( const WindowChangeListenerSPtr& pListener ) {
		WindowChangeListenerIterator pos = windowChangeListeners.end( );

		for( WindowChangeListenerIterator i = windowChangeListeners.begin( ); i != windowChangeListeners.end( ); ++i )
		{
			if( ( *i ) == pListener )
			{
				windowChangeListeners.erase( i );
				return true;
			}
		}

		return false;
	}

	/// <summary> Remove a window change listener. </summary>
	/// <param name="pListener"> The window change listener to try and remove. </param>
	/// <returns> True if the listener was removed, false otherwise. </returns>
	bool WindowMS::contains( const WindowChangeListenerSPtr& pListener ) {
		return find( windowChangeListeners.begin( ), windowChangeListeners.end( ), pListener ) != windowChangeListeners.end( );
	}

	/// <summary> Get the hwnd so that we can attach the swap chain. </summary>
	/// <returns> A pointer to the window handle. </returns>
	HWND* WindowMS::getHWnd( )
	{
		return &hWnd;
	}

	/// <summary> Process Raw Input. </summary>
	/// <param name="raw"> A pointer to the raw input data. </param>
	void WindowMS::processRawInput( RAWINPUT* raw )
	{
		if( raw->header.dwType == RIM_TYPEKEYBOARD )
		{
			if( raw->data.keyboard.Message == WM_KEYDOWN ||
				raw->data.keyboard.Message == WM_SYSKEYDOWN )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->keyPressed( raw->data.keyboard.VKey );
				}
			}
			else if( raw->data.keyboard.Message == WM_KEYUP ||
					 raw->data.keyboard.Message == WM_SYSKEYUP )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->keyReleased( raw->data.keyboard.VKey );
				}
			}
		}
		else if( raw->header.dwType == RIM_TYPEMOUSE )
		{
			if( raw->data.mouse.usFlags == MOUSE_MOVE_RELATIVE )
			{
				GetCursorPos( &point );
				ScreenToClient( hWnd, &point );
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseMoved( raw->data.mouse.lLastX, raw->data.mouse.lLastY, point.x, point.y );
				}
			}

			if( raw->data.mouse.usFlags == MOUSE_MOVE_ABSOLUTE )
			{
			}

			if( raw->data.mouse.usButtonFlags == RI_MOUSE_LEFT_BUTTON_DOWN )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mousePressed( 0 );
				}
			}
			else if( raw->data.mouse.usButtonFlags == RI_MOUSE_LEFT_BUTTON_UP )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseReleased( 0 );
				}
			}
			else if( raw->data.mouse.usButtonFlags == RI_MOUSE_RIGHT_BUTTON_DOWN )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mousePressed( 1 );
				}
			}
			else if( raw->data.mouse.usButtonFlags == RI_MOUSE_RIGHT_BUTTON_UP )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseReleased( 1 );
				}
			}
			else if( raw->data.mouse.ulButtons == RI_MOUSE_BUTTON_3_DOWN )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mousePressed( 2 );
				}
			}
			else if( raw->data.mouse.ulButtons == RI_MOUSE_BUTTON_3_UP )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseReleased( 2 );
				}
			}
			else if( raw->data.mouse.ulButtons == RI_MOUSE_BUTTON_4_DOWN )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mousePressed( 3 );
				}
			}
			else if( raw->data.mouse.ulButtons == RI_MOUSE_BUTTON_4_UP )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseReleased( 3 );
				}
			}
			else if( raw->data.mouse.ulButtons == RI_MOUSE_BUTTON_5_DOWN )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mousePressed( 4 );
				}
			}
			else if( raw->data.mouse.ulButtons == RI_MOUSE_BUTTON_5_UP )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseReleased( 4 );
				}
			}

			if( raw->data.mouse.usButtonFlags == RI_MOUSE_WHEEL )
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseWheelMoved( (short)raw->data.mouse.usButtonData / WHEEL_DELTA );
				}
			}
		}
	}

	/// <summary> Process Drag and Drop Input. </summary>
	/// <param name="hWndParam"> The window handel. </param>
	/// <param name="wParam"> The  event. </param>
	void WindowMS::processDnDInput( HWND hWndParam, const WPARAM wParam )
	{
		POINT pt;

		char lpszFile[ 80 ];
		vector< string > fileList;

		DragQueryPoint( HDROP( wParam ), &pt );

		const WORD cFiles = DragQueryFile( HDROP( wParam ), 0xFFFFFFFF, LPSTR( nullptr ), 0 );

		for( WORD i = 0; i < cFiles; i++ )
		{
			DragQueryFile( HDROP( wParam ), i, lpszFile, sizeof( lpszFile ) );

			fileList.emplace_back( lpszFile );
		}

		DragFinish( HDROP( wParam ) );

		for( FileInputListener& consume : fileInputListeners )
		{
			consume( pt.x , pt.y, fileList );
		}
	}

	/// <summary> Process a change in the size of the output window. </summary>
	/// <param name="newWidth"> The new window width. </param>
	/// <param name="newHeight"> The new window height. </param>
	void WindowMS::processSizeChange( const uint16_t newWidth, const uint16_t newHeight )
	{
		width  = newWidth;
		height = newHeight;

		for( WindowChangeListenerSPtr& windowChangeListener : windowChangeListeners )
		{
			windowChangeListener->sizeChange( width, height );
		}
	}

	/// <summary> Message handling function. </summary>
	/// <param name="hWndParam"> The window handel. </param>
	/// <param name="message"> The window message to process. </param>
	/// <param name="wParam"> Additional message information 1. </param>
	/// <param name="lParam"> Additional message information 2. </param>
	/// <returns> The return value is the result of the message processing and depends on the message sent.. </returns>
	LRESULT CALLBACK WindowMS::wndProc( HWND hWndParam, const UINT message, const WPARAM wParam, const LPARAM lParam )
	{
		(void) hWndParam;


		switch( message )
		{
			case WM_PAINT:
			{
				PAINTSTRUCT ps;
				BeginPaint( hWndParam, &ps );
				EndPaint( hWndParam, &ps );
				return 0;
			}

			// -----------------------------------------------------------------------------------
			// Check if the window is being closed.
			// -----------------------------------------------------------------------------------
			case WM_CLOSE:
			{
				PostQuitMessage( 0 );
				return 0;
			}

			// -----------------------------------------------------------------------------------
			// Watch For the Window Activate Message.
			// -----------------------------------------------------------------------------------
			case WM_ACTIVATE:
			{
				// Check Minimization State
				if( !HIWORD( wParam ) )
				{
					bActive = true;
				}
				else
				{
					bActive = false;
				}
				return 0;
			}

			// -----------------------------------------------------------------------------------
			// Watch For the Raw Input data of HIDs
			// -----------------------------------------------------------------------------------
			case WM_INPUT:
			{
				UINT dwSize = 0;

				GetRawInputData( reinterpret_cast< HRAWINPUT >( lParam ), RID_INPUT, nullptr, &dwSize, sizeof( RAWINPUTHEADER ) );

				LPBYTE lpb = new BYTE[ dwSize ];
				if( lpb == nullptr )
				{
					printf( "call to new lpb[] failed\n" );
					return 0;
				}

				if( GetRawInputData( reinterpret_cast<HRAWINPUT >( lParam ), RID_INPUT, lpb, &dwSize, sizeof( RAWINPUTHEADER ) ) != dwSize )
				{
					printf( "call to GetRawInputData didn't return the correct size, its the under pants gnomes again! failed\n" );
					return 0;
				}

				RAWINPUT* raw = reinterpret_cast< RAWINPUT* >( lpb );

				//sp_input->update( raw );
				processRawInput( raw );

				delete[ ] lpb;
				return 0;
			}

			// -----------------------------------------------------------------------------------
			// Check if a key characture is typed
			// -----------------------------------------------------------------------------------
			case WM_CHAR:
			{
				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->charKeyPressed( unsigned int( wParam ) );
				}

				return 0;
			}

			// -----------------------------------------------------------------------------------
			// Check if the window has changed size
			// -----------------------------------------------------------------------------------
			case WM_SIZE:
			{
				processSizeChange( LOWORD( lParam ), HIWORD( lParam ) );
				return 0;
			}

			// -----------------------------------------------------------------------------------
			// Intercept System Commands
			// -----------------------------------------------------------------------------------
			case WM_SYSCOMMAND:
			{
				// -----------------------------------------------------------------------------------
				// Check System Calls to stop the screen saver and power saving modes
				// -----------------------------------------------------------------------------------
				switch( wParam )
				{
					case SC_SCREENSAVE:
					case SC_MONITORPOWER:
						return 0;
				}
				break;
			}

			// -----------------------------------------------------------------------------------
			// Drag & Drop Set-up
			// -----------------------------------------------------------------------------------
			case WM_CREATE:
			{
				DragAcceptFiles( hWndParam, TRUE );
				break;
			}

			case WM_DROPFILES:
			{
				processDnDInput( hWndParam, wParam );
				break;
			}

			case WM_DESTROY:
			{
				DragAcceptFiles( hWndParam, FALSE );
				break;
			}

			default:
			{
				break;
			};
		}

		return DefWindowProc( hWndParam, message, wParam, lParam );
	}

	/// <summary> Message handler that pass the message to the window message handler. </summary>
	/// <param name="hWnd"> The window handel. </param>
	/// <param name="message"> The window message to process. </param>
	/// <param name="wParam"> Additional message information 1. </param>
	/// <param name="lParam"> Additional message information 2. </param>
	/// <returns> The return value is the result of the message processing and depends on the message sent.. </returns>
	LRESULT CALLBACK WndProc( HWND hWnd, const UINT message, const WPARAM wParam, const LPARAM lParam )
	{
		return WindowHandle->wndProc( hWnd, message, wParam, lParam );
	}

} // namespace ui
} // namespace visual
#endif // _WIN32