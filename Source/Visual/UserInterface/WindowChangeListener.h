#pragma once

// Window Change Listener. <Pure Virtual>
//
// This is the interface we will implement to get changes made to the window.
// This will be used to change the projection matrix and render targets.
//
// Project   : NaKama-Fx
// File Name : WindowChangeListener.h
// Date      : 22/11/2018
// Author    : Nicholas Welters

#ifndef _WINDOW_CHANGE_LISTENER_H
#define _WINDOW_CHANGE_LISTENER_H

namespace visual
{
namespace ui
{
	/// <summary>
	/// Window Change Listener. <Pure Virtual>
	///
	/// This is the interface we will implement to get changes made to the window.
	/// This will be used to change the projection matrix and render targets.
	/// </summary>
	class WindowChangeListener
	{
		public:
			/// <summary> dtor. </summary>
			virtual ~WindowChangeListener( ) = 0;

			/// <summary> Listen to changes in the size of the window. </summary>
			/// <param name="width"> The new window width. </param>
			/// <param name="height"> The new window height. </param>
			virtual void sizeChange( uint16_t width, uint16_t height ) = 0;
	};

	/// <summary> dtor. Impl </summary>
	inline WindowChangeListener::~WindowChangeListener( ) = default;
} // namespace ui
} // namespace visual

#endif // _WINDOW_CHANGE_LISTENER_H