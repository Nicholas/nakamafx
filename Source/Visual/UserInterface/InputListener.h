#pragma once

// User Input Listener. <Pure Virtual>
//
// This is the interface we will implement to get commands from the window.
// This lets us process key input with out knowing where or how its generated.
//
// Project   : NaKama-Fx
// File Name : InputListener.h
// Date      : 25/06/2015
// Author    : Nicholas Welters

#ifndef _INPUT_LISTENER_H
#define _INPUT_LISTENER_H

namespace visual
{
namespace ui
{
	/// <summary>
	/// User Input Listener. <Pure Virtual>
	///
	/// This is the interface we will implement to get commands from the window.
	/// This lets us process key input with out knowing where or how its generated.
	/// </summary>
	class InputListener
	{
		public:
			/// <summary> dtor. </summary>
			virtual ~InputListener( ) = 0;

			/// <summary> Register a key pressed event. </summary>
			/// <param name="key"> The key pressed. </param>
			virtual void keyPressed( int key ) = 0;

			/// <summary> Register a key released event. </summary>
			/// <param name="key"> The key released. </param>
			virtual void keyReleased( int key ) = 0;

			/// <summary> Register a characture key pressed event. </summary>
			/// <param name="key"> The key released. </param>
			virtual void charKeyPressed( int key ) = 0;

			/// <summary> Register a mouse moved event. </summary>
			/// <param name="x"> The change in x. </param>
			/// <param name="y"> The change in y. </param>
			/// <param name="xPos"> The x position of the mouse on screen. </param>
			/// <param name="yPos"> The y position of the mouse on screen. </param>
			virtual void mouseMoved( long x, long y, long xPos, long yPos ) = 0;

			/// <summary> Register a mouse button pressed event. </summary>
			/// <param name="button"> The button pressed. </param>
			virtual void mousePressed( int button ) = 0;

			/// <summary> Register a mouse button released event. </summary>
			/// <param name="button"> The button pressed. </param>
			virtual void mouseReleased( int button ) = 0;

			/// <summary> Register a mouse wheel move event. </summary>
			virtual void mouseWheelMoved( long clicks ) = 0;
	};

	/// <summary> dtor. Impl </summary>
	inline InputListener::~InputListener( ) = default;
} // namespace ui
} // namespace visual

#endif // _INPUT_LISTENER_H