
// Window Interface class
//
// OO Window class for making and setting up a window.
//
// Project   : NaKama-Fx
// File Name : WindowXCB.cpp
// Date      : 13/12/2017
// Author    : Nicholas Welters

#define __linux__

#if defined( __linux__ )

#include "WindowXCB.h"
#include "InputListener.h"

#include <algorithm>

namespace visual
{
namespace ui
{
	using std::string;
	using std::find;

	/// <summary> ctor. </summary>
	/// <param name="title"> The window title. </param>
	/// <param name="width"> The window width. </param>
	/// <param name="height"> The window height. </param>
	WindowXCB::WindowXCB( const string& title, uint16_t width, uint16_t height )
		: title( title )
		, width( width )
		, height( height )
		, quit( false )
		, pConnection( nullptr )
		, pScreen( nullptr )
		, windowHandle( )
		, pAtom_wm_delete_window( nullptr )
		, mouseX( 0 )
		, mouseY( 0 )
		, inputListeners( )
	{ }

	/// <summary> dtor. </summary>
	WindowXCB::~WindowXCB( )
	{
		if( ! shutdown( ) )
		{
			printf( "Window::Destructor - Shutdown Exception" );
		}
	}

	/// <summary> Component initialization. </summary>
	/// <returns> True if a window was created, false if there was an issue. </returns>
	bool WindowXCB::initialize( )
	{
		if( !initXcbConnection( ) )
		{
			return false;
		}

		setupWindow( );

		xcb_flush( pConnection );

		return true;
	}

	/// <summary> Component shutdown and destruction. </summary>
	/// <returns> True if a window was destroyed, false if there was an issue. </returns>
	bool WindowXCB::shutdown( )
	{
		if( pConnection )
		{
			xcb_destroy_window( pConnection, windowHandle );
			xcb_disconnect( pConnection );
			pConnection = nullptr;
		}

		if( pAtom_wm_delete_window )
		{
			free( pAtom_wm_delete_window );
			pAtom_wm_delete_window = nullptr;
		}

		return true;
	}

	/// <summary> Component update. </summary>
	/// <returns> True if a window is still alive, false to stop rendering. </returns>
	bool WindowXCB::update( )
	{
		while (!quit)
		{
			xcb_generic_event_t* event;
			event = xcb_poll_for_event( pConnection );
			if( event )
			{
				handleEvent( event );
				free( event );
			}

			return true;
		}

		return false;
	}

	/// <summary> Force the update function to exit and initiate the shutdown. </summary>
	void WindowXCB::exit( )
	{
		quit = true;
	}

	/// <summary> Get window title. </summary>
	/// <returns> The window title. </returns>
	string WindowXCB::getWindowTitle()
	{
		return title;
	}

	/// <summary> Get the window handle. </summary>
	/// <returns> The window handle. </returns>
	xcb_window_t WindowXCB::getWindow( )
	{
		return windowHandle;
	}

	/// <summary> Get the x server connection. </summary>
	/// <returns> The x server connection pointer. </returns>
	xcb_connection_t* WindowXCB::getConnection( )
	{
		return pConnection;
	}

	/// <summary> Get the screen visual id. </summary>
	/// <returns> The screen visual id. </returns>
	xcb_visualid_t WindowXCB::getVisualID( )
	{
		return pScreen->root_visual;
	}

	/// <summary> Open a connection to the x server. </summary>
	/// <returns> True if we got a xcb connection, false otherwise. </returns>
	bool WindowXCB::initXcbConnection( )
	{
		int scr;

		pConnection = xcb_connect( nullptr, &scr );
		if( !pConnection )
		{
			printf( "Could not get a xcb connection!\n" );
			fflush( stdout );
			return false;
		}

		const xcb_setup_t *setup = xcb_get_setup( pConnection);
		xcb_screen_iterator_t iter = xcb_setup_roots_iterator( setup );
		while( scr-- > 0 )
		{
			xcb_screen_next( &iter );
		}
		pScreen = iter.data;

		return true;
	}

	/// <summary> Create a window to display to and with event types. </summary>
	void WindowXCB::setupWindow( )
	{
		uint32_t value_mask;
		uint32_t value_list[ 32 ];

		windowHandle = xcb_generate_id( pConnection );

		value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
		value_list[0] = pScreen->black_pixel;
		value_list[1] =
				XCB_EVENT_MASK_KEY_PRESS |
				XCB_EVENT_MASK_KEY_RELEASE |
				XCB_EVENT_MASK_EXPOSURE |
				XCB_EVENT_MASK_STRUCTURE_NOTIFY |
				XCB_EVENT_MASK_POINTER_MOTION |
				XCB_EVENT_MASK_BUTTON_PRESS |
				XCB_EVENT_MASK_BUTTON_RELEASE;

		xcb_create_window( pConnection,
						   XCB_COPY_FROM_PARENT,
						   windowHandle, pScreen->root,
						   0, 0, width, height, 0,
						   XCB_WINDOW_CLASS_INPUT_OUTPUT,
						   pScreen->root_visual,
						   value_mask, value_list);

		/* Magic code that will send notification when window is destroyed */
		xcb_intern_atom_cookie_t cookie = xcb_intern_atom( pConnection, 1, 12, "WM_PROTOCOLS" );
		xcb_intern_atom_reply_t* reply = xcb_intern_atom_reply( pConnection, cookie, nullptr );

		xcb_intern_atom_cookie_t cookie2 = xcb_intern_atom( pConnection, 0, 16, "WM_DELETE_WINDOW" );
		pAtom_wm_delete_window = xcb_intern_atom_reply( pConnection, cookie2, nullptr );

		xcb_change_property( pConnection, XCB_PROP_MODE_REPLACE,
							 windowHandle, (*reply).atom, 4, 32, 1,
							 &(*pAtom_wm_delete_window).atom);

		string windowTitle = getWindowTitle();
		uint32_t windowTitleSize = static_cast< uint32_t >( windowTitle.size( ) );
		xcb_change_property( pConnection, XCB_PROP_MODE_REPLACE,
							 windowHandle, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
							 windowTitleSize, windowTitle.c_str( ) );

		free( reply );

		xcb_map_window( pConnection, windowHandle );
	}


#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCDFAInspection"

	/// <summary> Respond to events read from the update loop. </summary>
	/// <param name="uiEvent"> The event that we are going to respond to. </param>
	void WindowXCB::handleEvent(const xcb_generic_event_t *uiEvent)
	{
		switch (uiEvent->response_type & 0x7f)
		{
			case XCB_CLIENT_MESSAGE:
			{
				if( ( *( xcb_client_message_event_t* ) uiEvent ).data.data32[ 0 ] == ( *pAtom_wm_delete_window ).atom )
				{
					quit = true;
				}
				break;
			}
			case XCB_MOTION_NOTIFY:
			{
				xcb_motion_notify_event_t* motion;
				motion = ( xcb_motion_notify_event_t* ) uiEvent;

				long pointX = motion->event_x;
				long pointY = motion->event_y;
				long lastX = mouseX - pointX;
				long lastY = mouseY - pointY;

				mouseX = pointX;
				mouseY = pointY;

				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->mouseMoved( lastX, lastY, pointX, pointY );
				}


				break;
			}
			case XCB_BUTTON_PRESS:
			{
				xcb_button_press_event_t* press;
				press = ( xcb_button_press_event_t* ) uiEvent;

				for( InputListenerSPtr& pListener: inputListeners )
				{
					if( press->detail & XCB_BUTTON_INDEX_1 )
					{
						pListener->mousePressed( 0 );
					}
					if( press->detail & XCB_BUTTON_INDEX_2 )
					{
						pListener->mousePressed( 1 );
					}
					if( press->detail & XCB_BUTTON_INDEX_3 )
					{
						pListener->mousePressed( 2 );
					}


					if( press->detail & XCB_BUTTON_INDEX_4 )
					{
						pListener->mouseWheelMoved( 1 );
					}
					if( press->detail & XCB_BUTTON_INDEX_5 )
					{
						pListener->mouseWheelMoved( -1 );
					}

				}

				break;
			}
			case XCB_BUTTON_RELEASE:
			{
				xcb_button_press_event_t* press;
				press = ( xcb_button_press_event_t* ) uiEvent;

				for( InputListenerSPtr& pListener: inputListeners )
				{
					if( press->detail & XCB_BUTTON_INDEX_1 )
					{
						pListener->mouseReleased( 0 );
					}
					if( press->detail & XCB_BUTTON_INDEX_2 )
					{
						pListener->mouseReleased( 1 );
					}
					if( press->detail & XCB_BUTTON_INDEX_3 )
					{
						pListener->mouseReleased( 2 );
					}
				}

				break;
			}
			case XCB_KEY_PRESS:
			{
				const xcb_key_release_event_t* keyEvent;
				keyEvent = ( const xcb_key_release_event_t* ) uiEvent;

				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->keyPressed( keyEvent->detail );
				}

				break;
			}
			case XCB_KEY_RELEASE:
			{
				const xcb_key_release_event_t* keyEvent;
				keyEvent = ( const xcb_key_release_event_t* ) uiEvent;

			//	if( keyEvent->detail == VK_ESCAPE )
			//	{
			//		quit = true;
			//	}

				for( InputListenerSPtr& pListener: inputListeners )
				{
					pListener->keyReleased( keyEvent->detail );
				}

				break;
			}
			case XCB_DESTROY_NOTIFY:
			{
				quit = true;
				break;
			}
			default:
			{
				break;
			}
		}
	}
#pragma clang diagnostic pop


	/// <summary> Add an input listener. </summary>
	/// <param name="pListener"> A new input listener. </param>
	void WindowXCB::add(const InputListenerSPtr& pListener)
	{
		inputListeners.push_back( pListener );
	}

	/// <summary> Remove an input listener. </summary>
	/// <param name="pListener"> The input listener to remove. </param>
	bool WindowXCB::remove(const InputListenerSPtr& pListener)
	{
		InputListenerIterator i;
		InputListenerIterator pos = inputListeners.end( );

		for( i = inputListeners.begin( ); i != inputListeners.end( ); i++ )
		{
			if( ( *i ) == pListener )
			{
				inputListeners.erase( i );
				return true;
			}
		}

		return false;
	}

	/// <summary> Check an input listener is registered. </summary>
	/// <param name="pListener"> The input listener to look for. </param>
	bool WindowXCB::contains(const InputListenerSPtr& pListener)
	{
		return find( inputListeners.begin( ), inputListeners.end( ), pListener ) != inputListeners.end( );
	}
} // namespace ui
} // namespace visual

#endif // __linux__
