#pragma once

// Window Change Listener.
//
// Simply wraps a functor to listen to changes to the window size.
//
// Project   : NaKama-Fx
// File Name : WindowChange.h
// Date      : 22/11/2018
// Author    : Nicholas Welters

#ifndef _WINDOW_CHANGE_H
#define _WINDOW_CHANGE_H

#include "WindowChangeListener.h"

#include <functional>

namespace visual
{
namespace ui
{
	/// <summary>
	/// Window Change Listener.
	///
	/// Simply wraps a functor to listen to changes to the window size.
	/// </summary>
	class WindowChange final: public WindowChangeListener
	{
		public:
			/// <summary> ctor. </summary>
			WindowChange( );

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			WindowChange( const WindowChange& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			WindowChange( WindowChange&& temporary ) = default;


			/// <summary> dtor. </summary>
			virtual ~WindowChange( ) = default;

			
			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			WindowChange& operator=( const WindowChange& copy ) = default;
			
			/// <summary> Move assignment operator </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			WindowChange& operator=( WindowChange&& temporary ) = default;


			/// <summary> Wraps the functor called on changing the window size. </summary>
			/// <param name="newWidth"> The new window width. </param>
			/// <param name="newHeight"> The new window height. </param>
			virtual void sizeChange( uint16_t newWidth, uint16_t newHeight ) override final;
			
			/// <summary> Get the window width. </summary>
			/// <returns> The window width. </returns>
			uint16_t getWidth( ) const;
			
			/// <summary> Get the window height. </summary>
			/// <returns> The window height. </returns>
			uint16_t getHeight( ) const;
			
			/// <summary> Check if the window size changed since the last time we called read complete. </summary>
			/// <returns> True if the window changed its size, false otherwise. </returns>
			bool sizeChanged( ) const;
			
			/// <summary> Reset the internal state to signal that we have completed reading all the buffered information. </summary>
			void readComplete( );

		private:
			/// <summary>  </summary>
			bool windowSizeChanged;
			uint16_t width;
			uint16_t height;
	};
} // namespace ui
} // namespace visual

#endif // _WINDOW_CHANGE_H