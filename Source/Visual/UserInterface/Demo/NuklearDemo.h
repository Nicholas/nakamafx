

#ifndef _NUKLEAR_DEMO_UI_H
#define _NUKLEAR_DEMO_UI_H

int demo_ui_overview( struct nk_context* ctx );
int node_editor( struct nk_context* ctx );
void calculator( struct nk_context* ctx );
void skinning( struct nk_context* ctx );

#endif // _UI_PASS_DX11_H
