
// Monitors and provides input from the mouse and keyboard
//
// Project   : NaKama-Fx
// File Name : Input.cpp
// Date      : 25/06/2015
// Author    : Nicholas Welters

# include "Input.h"


namespace visual
{
namespace ui
{
	/// <summary> ctor. </summary>
	Input::Input( )
		: keyBoard( )
		, mouse( )
		, acceptKeyboardInput( true )
		, acceptMouseInput( true )
	{
		mouse.xMovement = 0;
		mouse.yMovement = 0;
		mouse.xPosition = 0;
		mouse.yPosition = 0;
		mouse.wheelMovement = 0;

		for( bool& button : mouse.buttons )
		{
			button = false;
		}

		for( bool& key : keyBoard.keys )
		{
			key = false;
		}
	}


	/// <summary> Register a key pressed event. </summary>
	/// <param name="keyID"> The key pressed. </param>
	void Input::keyPressed( const int keyID )
	{
		if( keyID < keysSize && keyID >= 0 && acceptKeyboardInput )
		{
			keyBoard.keys[ keyID ] = true;
		}
	}
	/// <summary> Register a key released event. </summary>
	/// <param name="keyID"> The key released. </param>
	void Input::keyReleased( const int keyID )
	{
		if( keyID < keysSize && keyID >= 0 && acceptKeyboardInput )
		{
			keyBoard.keys[ keyID ] = false;
		}
	}
	/// <summary> Register a characture key pressed event. </summary>
	/// <param name="key"> The key released. </param>
	void Input::charKeyPressed( int key )
	{
	}
	/// <summary> Check a keys state. </summary>
	/// <param name="keyID"> The key to check is being pressed. </param>
	/// <returns> True if pressed, false otherwise. </returns>
	bool Input::key( const int keyID ) const
	{
		if( keyID < keysSize && keyID >= 0 )
		{
			return keyBoard.keys[ keyID ];
		}

		return false;
	}


	/// <summary> Register a mouse moved event. </summary>
	/// <param name="buttonID"> The button pressed. </param>
	void Input::mousePressed( const int buttonID )
	{
		if( buttonID < mouseSize && buttonID >= 0 )
		{
			mouse.buttons[ buttonID ] = true;
		}
	}

	/// <summary> Register a mouse button pressed event. </summary>
			/// <param name="buttonID"> The button released. </param>
	void Input::mouseReleased( const int buttonID )
	{
		if( buttonID < mouseSize && buttonID >= 0 && acceptMouseInput )
		{
			mouse.buttons[ buttonID ] = false;
		}
	}

	/// <summary> Check a mouse button state. </summary>
	/// <param name="buttonID"> The button to check. </param>
	/// <returns> True if pressed, false otherwise. </returns>
	bool Input::button( const int buttonID ) const
	{
		if( buttonID < mouseSize && buttonID >= 0 && acceptMouseInput )
		{
			return mouse.buttons[ buttonID ];
		}

		return false;
	}


	/// <summary> Register a mouse button released event. </summary>
	/// <param name="x"> The change in x. </param>
	/// <param name="y"> The change in y. </param>
	/// <param name="xPos"> The x position of the mouse on screen. </param>
	/// <param name="yPos"> The y position of the mouse on screen. </param>
	void Input::mouseMoved( const long x, const long y, const long xPos, const long yPos )
	{
		if( !acceptMouseInput ) return;

		mouse.moved = true;
		mouse.xMovement += x;
		mouse.yMovement += y;

		mouse.xPosition = xPos;
		mouse.yPosition = yPos;
	}

	/// <summary> Check if the mouse changed position. </summary>
	/// <returns> True if the mouse was moved. </returns>
	bool Input::hasMouseMoved( ) const
	{
		return mouse.moved;
	}

	/// <summary> Check the movement change of the mouse on the X axis. </summary>
	/// <returns> The mouses movement on the x axis. </returns>
	long Input::getXMovement( ) const
	{
		return mouse.xMovement;
	}

	/// <summary> Check the movement change of the mouse on the Y axis. </summary>
	/// <returns> The mouses movement on the y axis. </returns>
	long Input::getYMovement( ) const
	{
		return mouse.yMovement;
	}

	/// <summary> Check the current position of the mouse on the X axis. </summary>
	/// <returns> The mouses position on the x axis. </returns>
	long Input::getXPosition( ) const
	{
		return mouse.xPosition;
	}

	/// <summary> Check the current position of the mouse on the Y axis. </summary>
	/// <returns> The mouses position on the y axis. </returns>
	long Input::getYPosition( ) const
	{
		return mouse.yPosition;
	}

	/// <summary> Register a mouse wheel move event. </summary>
	/// <param name="roll"> The change in the wheels position. </param>
	void Input::mouseWheelMoved( const long roll )
	{
		mouse.wheelMovement += roll;
	}

	/// <summary> Check the change of the mouse wheel. </summary>
	/// <returns> The change in the mouse wheel rotation. </returns>
	long Input::getWheel( ) const
	{
		return mouse.wheelMovement;
	}


	/// <summary> Reset the mouse state so that we can collect additional events again. </summary>
	void Input::readComplete( )
	{
		mouse.moved = false;
		mouse.xMovement = 0;
		mouse.yMovement = 0;
		mouse.wheelMovement = 0;
	}

	/// <summary> Toggle keyboard input if its needed by the UI or by the system. </summary>
	/// <param name="enable"> State toggle value. </param>
	void Input::setKeybourdUpdateState( bool enable )
	{
		if( !enable && acceptKeyboardInput != enable )
		{
			for( bool& key : keyBoard.keys )
			{
				key = false;
			}
		}

		acceptKeyboardInput = enable;
	}

	/// <summary> Toggle mouse input if its needed by the UI or by the system. </summary>
	/// <param name="enable"> State toggle value. </param>
	void Input::setMouseUpdateState( bool enable )
	{
		if( !enable && acceptMouseInput != enable )
		{
			for( bool& button : mouse.buttons )
			{
				button = false;
			}
		}

		acceptMouseInput = enable;
	}
} // namespace ui
} // namespace visual
