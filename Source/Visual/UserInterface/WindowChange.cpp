
// Window Change Listener.
//
// Simply wraps a functor to listen to changes to the window size.
//
// Project   : NaKama-Fx
// File Name : WindowChange.cpp
// Date      : 22/11/2018
// Author    : Nicholas Welters

#include "WindowChange.h"

namespace visual
{
namespace ui
{
	/// <summary> ctor. </summary>
	WindowChange::WindowChange( )
		: windowSizeChanged( false )
		, width( 0 )
		, height( 0 )
	{ }

	/// <summary> Wraps the functor called on changing the window size. </summary>
	/// <param name="newWidth"> The new window width. </param>
	/// <param name="newHeight"> The new window height. </param>
	void WindowChange::sizeChange( const uint16_t newWidth, const uint16_t newHeight )
	{
		windowSizeChanged = width != newWidth || height != newHeight;

		if( windowSizeChanged )
		{
			width = newWidth;
			height = newHeight;
		}
	}
	
			/// <summary> Get the window width. </summary>
			/// <returns> The window width. </returns>
	uint16_t WindowChange::getWidth( ) const
	{
		return width;
	}
	
			/// <summary> Get the window height. </summary>
			/// <returns> The window height. </returns>
	uint16_t WindowChange::getHeight( ) const
	{
		return height;
	}
	
			/// <summary> Check if the window size changed since the last time we called read complete. </summary>
			/// <returns> True if the window changed its size, false otherwise. </returns>
	bool WindowChange::sizeChanged( ) const
	{
		return windowSizeChanged;
	}
	
			/// <summary> Reset the internal state to signal that we have completed reading all the buffered information. </summary>
	void WindowChange::readComplete( )
	{ 
		windowSizeChanged = false;
	}
} // namespace ui
} // namespace visual
