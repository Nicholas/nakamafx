#pragma once

// Window Interface class header include
//
// Project   : NaKama-Fx
// File Name : WindowXCB.h
// Date      : 15/01/2018
// Author    : Nicholas Welters

// __CYGWIN__
// __APPLE__ && __MACH__
// _WIN32 || _WIN64
// __linux__
// __gnu_linux__
// __ANDROID__


#include "Visual/UserInterface/WindowXCB.h"
#include "Visual/UserInterface/WindowMS.h"
