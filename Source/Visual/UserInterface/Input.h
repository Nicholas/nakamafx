#pragma once

// Monitors and provides input from the mouse and keyboard
//
// Project   : NaKama-Fx
// File Name : Input.h
// Date      : 25/06/2015
// Author    : Nicholas Welters

#ifndef _INPUT_H
#define _INPUT_H

#include "InputListener.h"

constexpr int keysSize = 256;
constexpr int  mouseSize = 5;


namespace visual
{
namespace ui
{
	/// <summary> Monitors and provides input from the mouse and keyboard. </summary>
	class Input final : public InputListener
	{
		public:
			/// <summary> ctor. </summary>
			Input( );

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			Input( const Input& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			Input( Input&& temporary ) = default;


			/// <summary> dtor. </summary>
			virtual ~Input( ) = default;


			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			Input& operator=( const Input& copy ) = default;

			/// <summary> Move assignment operator </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			Input& operator=( Input&& temporary ) = default;


			/// <summary> Register a key pressed event. </summary>
			/// <param name="keyID"> The key pressed. </param>
			virtual void keyPressed( int keyID ) override final;

			/// <summary> Register a key released event. </summary>
			/// <param name="keyID"> The key released. </param>
			virtual void keyReleased( int keyID ) override final;

			/// <summary> Register a characture key pressed event. </summary>
			/// <param name="key"> The key released. </param>
			virtual void charKeyPressed( int key ) final override;

			/// <summary> Check a keys state. </summary>
			/// <param name="keyID"> The key to check is being pressed. </param>
			/// <returns> True if pressed, false otherwise. </returns>
			bool key( int keyID ) const;


			/// <summary> Register a mouse moved event. </summary>
			/// <param name="buttonID"> The button pressed. </param>
			virtual void mousePressed( int buttonID ) override final;

			/// <summary> Register a mouse button pressed event. </summary>
			/// <param name="buttonID"> The button released. </param>
			virtual void mouseReleased( int buttonID ) override final;

			/// <summary> Check a mouse button state. </summary>
			/// <param name="buttonID"> The button to check. </param>
			/// <returns> True if pressed, false otherwise. </returns>
			bool button( int buttonID ) const;


			/// <summary> Update the position of the mouse pointer. </summary>
			/// <param name="x"> The change in x. </param>
			/// <param name="y"> The change in y. </param>
			/// <param name="xPos"> The x position of the mouse on screen. </param>
			/// <param name="yPos"> The y position of the mouse on screen. </param>
			virtual void mouseMoved( long x, long y, long xPos, long yPos ) override final;

			/// <summary> Check if the mouse changed position. </summary>
			/// <returns> True if the mouse was moved. </returns>
			bool hasMouseMoved( ) const;

			/// <summary> Check the movement change of the mouse on the X axis. </summary>
			/// <returns> The mouses movement on the x axis. </returns>
			long getXMovement( ) const;

			/// <summary> Check the movement change of the mouse on the Y axis. </summary>
			/// <returns> The mouses movement on the y axis. </returns>
			long getYMovement( ) const;

			/// <summary> Check the current position of the mouse on the X axis. </summary>
			/// <returns> The mouses position on the x axis. </returns>
			long getXPosition( ) const;

			/// <summary> Check the current position of the mouse on the Y axis. </summary>
			/// <returns> The mouses position on the y axis. </returns>
			long getYPosition( ) const;


			/// <summary> Register a mouse wheel move event. </summary>
			/// <param name="roll"> The change in the wheels position. </param>
			virtual void mouseWheelMoved( long roll ) override final;

			/// <summary> Check the change of the mouse wheel. </summary>
			/// <returns> The change in the mouse wheel rotation. </returns>
			long getWheel( ) const;

			/// <summary> Reset the mouse state so that we can collect additional events again. </summary>
			void readComplete( );

			/// <summary> Toggle keyboard input if its needed by the UI or by the system. </summary>
			/// <param name="enable"> State toggle value. </param>
			void setKeybourdUpdateState( bool enable );

			/// <summary> Toggle mouse input if its needed by the UI or by the system. </summary>
			/// <param name="enable"> State toggle value. </param>
			void setMouseUpdateState( bool enable );

		private:
			/// <summary> Keyboard state. </summary>
			struct KeyBoard
			{
				/// <summary> Key pressed state. </summary>
				bool keys[ keysSize ];
			};

			/// <summary> Mouse state. </summary>
			struct Mouse
			{
				/// <summary> Mouse button pressed state. </summary>
				bool buttons[ mouseSize ];

				/// <summary> Mouse moved. </summary>
				bool moved;

				/// <summary> Mouse X axis change. </summary>
				long xMovement;
				/// <summary> Mouse Y axis change. </summary>
				long yMovement;

				/// <summary> Mouse X axis position. </summary>
				long xPosition;
				/// <summary> Mouse Y axis position. </summary>
				long yPosition;

				/// <summary> Mouse wheel rotation clicks. </summary>
				long wheelMovement;
			};

			/// <summary> Keyboard state. </summary>
			KeyBoard keyBoard;

			/// <summary> Mouse state. </summary>
			Mouse mouse;

			/// <summary> Toggle accepting keyboard state changes. </summary>
			bool acceptKeyboardInput;

			/// <summary> Toggle accepting mouse state changes. </summary>
			bool acceptMouseInput;


			// todo: Double buffering for safety!

	};
} // namespace ui
} // namespace visual

#endif // _INPUT_H