#pragma once
// User Interface Sub Pass.
//
//
// Project   : NaKama-Fx
// File Name : SystemUI.h
// Date      : 07/08/2019
// Author    : Nicholas Welters


#ifndef _NUKLEAR_UI_H
#define _NUKLEAR_UI_H

#include "InputListener.h"
#include "../FX/Types/ConstantBuffers.h"
#include "../FX/Vertex.h"

#include <Macros.h>

namespace visual
{
namespace ui
{
	FORWARD_DECLARE( Input );

	/// <summary> User interface Pass. </summary>
	class SystemUI : public InputListener
	{
	public:

		typedef std::function< fx::PSToneMappingBuffer& ( ) > GetToneMapFn;
		typedef std::function< fx::CSAdaptationBuffer& ( ) > GetAdaptationFn;
		typedef std::function< fx::PSSunVolumeBuffer& ( ) > GetFogFn;
		typedef std::function< fx::PSABufferResolve& ( ) > GetABufferFn;
		typedef std::function< fx::PSBlurBuffer& ( ) > GetBloomFn;
		typedef std::function< fx::VSSceneBuffer& ( ) > GetSceneFn;
		typedef std::function< fx::VSUIBuffer& ( ) > GetUIBufferFn;

		typedef std::function< bool& () > GetToggleFn;

		//struct ToggleControl
		//{
		//	std::string name;
		//	GetToggleFn getBool;
		//};

		struct GPUData
		{
			GPUData(
				const GetToneMapFn& getToneMapping,
				const GetAdaptationFn& getAdaptation,
				const GetFogFn& getFog,
				const GetABufferFn& getABuffer,
				const GetBloomFn& getBloom,
				const GetSceneFn& getScene,
				const GetToggleFn& getUseSSR,
				const GetToggleFn& getUseSSIL,
				const GetToggleFn& getUseSSAO,
				const GetToggleFn& getUseOIT,
				const GetToggleFn& getUseFog,
				const GetToggleFn& getUseDebug,
				const GetToggleFn& getUseDebugFPS,
				const GetToggleFn& getUseDebugTimes,
				const GetToggleFn& getUseDebugNormals,
				const GetUIBufferFn& getUIBuffer
			)
				: getToneMapping( getToneMapping )
				, getAdaptation( getAdaptation )
				, getFog( getFog )
				, getABuffer( getABuffer )
				, getBloom( getBloom )

				, getUseSSR( getUseSSR )
				, getUseSSIL( getUseSSIL )
				, getUseSSAO( getUseSSAO )
				, getUseOIT( getUseOIT )
				, getUseFog( getUseFog )
				, getUseDebug( getUseDebug )
				, getUseDebugFPS( getUseDebugFPS )
				, getUseDebugTimes( getUseDebugTimes )
				, getUseDebugNormals( getUseDebugNormals )
				, getUIBuffer( getUIBuffer )
			{};

			GetToneMapFn getToneMapping;
			GetAdaptationFn getAdaptation;
			GetFogFn getFog;
			GetABufferFn getABuffer;
			GetBloomFn getBloom;
			GetSceneFn getScene;
			GetToggleFn getUseSSR;
			GetToggleFn getUseSSIL;
			GetToggleFn getUseSSAO;
			GetToggleFn getUseOIT;
			GetToggleFn getUseFog;
			GetToggleFn getUseDebug;
			GetToggleFn getUseDebugFPS;
			GetToggleFn getUseDebugTimes;
			GetToggleFn getUseDebugNormals;
			GetUIBufferFn getUIBuffer;
		};
		SHARED_PTR_TYPE_DEF( GPUData )

		typedef std::function< void ( void*, const glm::ivec4&, uint32_t, uint32_t, uint32_t ) > DrawCallBack;
		typedef std::function< void* ( const std::vector< uint32_t >&, int, int ) > LoadTexture;

		/// <summary> ctor. </summary>
		/// <param name="pInput"> The direct user input listener. </param>
		/// <param name="containerSize">The area where we can see and use the ui. </param>
		SystemUI( const InputSPtr& pInput, const glm::vec2& containerSize );


		/// <summary> dtor </summary>
		virtual ~SystemUI( );


		/// <summary> ctor </summary>
		SystemUI( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		SystemUI( const SystemUI& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		SystemUI( SystemUI&& move ) = delete;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		SystemUI& operator=( const SystemUI& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		SystemUI& operator=( SystemUI&& move ) = delete;


		/// <summary> Initialize the GPU data needed to render the UI. </summary>
		/// <param name="load"> Load A texture on the GPU. </param>
		/// <param name="pGPUData"> The GPU data to edit. </param>
		void initGPUData( const LoadTexture& load, const GPUDataSPtr& pGPUData );

		/// <summary> Update and process the user interface. </summary>
		/// <param name="delta"> The time step. </param>
		/// <returns> True if the UI has updated and is ready to draw, false to shut down </returns>
		bool update( float delta );

		/// <summary> Update and process the user interface. </summary>
		/// <param name="pVertices"> A pointer to the vertex buffer to fill. </param>
		/// <param name="vertexCount"> The vertex buffer size. </param>
		/// <param name="pIndices"> A pointer to the index buffer to fill. </param>
		/// <param name="indexCount"> The index buffer size. </param>
		void fillBuffers( typename fx::Vertex3f2f4f::Vertex* pVertices, unsigned int vertexCount, void* pIndices, unsigned int indexCount );

		/// <summary> Use the draw callback to trigger a draw in the renderer to render the UI. </summary>
		/// <param name="draw"> The renderer draw call back. </param>
		void draw( const DrawCallBack& draw );

		/// <summary> Update the containers/windows size. </summary>
		/// <param name="containerSize">The area where we can see and use the ui. </param>
		void setSize( const glm::vec2& containerSize );

		/// <summary> Change the window visability state. </summary>
		void toggleVisablity( );

		/// <summary> Register a key pressed event. </summary>
		/// <param name="key"> The key pressed. </param>
		virtual void keyPressed( int key ) final override;

		/// <summary> Register a key released event. </summary>
		/// <param name="key"> The key released. </param>
		virtual void keyReleased( int key ) final override;

		/// <summary> Register a characture key pressed event. </summary>
		/// <param name="key"> The key released. </param>
		virtual void charKeyPressed( int key ) final override;

		/// <summary> Register a mouse moved event. </summary>
		/// <param name="x"> The change in x. </param>
		/// <param name="y"> The change in y. </param>
		/// <param name="xPos"> The x position of the mouse on screen. </param>
		/// <param name="yPos"> The y position of the mouse on screen. </param>
		virtual void mouseMoved( long x, long y, long xPos, long yPos ) final override;

		/// <summary> Register a mouse button pressed event. </summary>
		/// <param name="button"> The button pressed. </param>
		virtual void mousePressed( int button ) final override;

		/// <summary> Register a mouse button released event. </summary>
		/// <param name="button"> The button pressed. </param>
		virtual void mouseReleased( int button ) final override;

		/// <summary> Register a mouse wheel move event. </summary>
		virtual void mouseWheelMoved( long clicks ) final override;

	private:
		/// <summary> Set the UI theme. </summary>
		void setTheme( );

		/// <summary> User Interface Data needed to support the rendering of the UI (Font, images, vertices, ...). </summary>
		GPUDataSPtr pGPUData;

		/// <summary> The direct user input listener. </summary>
		InputSPtr pInput;

		/// <summary> The area where we can see and use the ui. </summary>
		glm::vec2 size;

		/// <summary> The UI visibility state. </summary>
		bool hidden;
	};
} // namespace directX11
} // namespace visual

#endif // _UI_PASS_DX11_H
