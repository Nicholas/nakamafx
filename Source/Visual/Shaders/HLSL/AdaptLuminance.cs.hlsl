
#include "algorithms.ColourSpace.hlsl"
#include "algorithms.ToneMapping.hlsl"

cbuffer toneMapGammaConstants : register( b0 )
{
	float TimeDelta;
	float TauU;
	float TauD;
	//float placeholder;
};

Texture2D sourceTextureSRV : register( t0 );

StructuredBuffer< float > lastLuminance : register( t1 );

RWStructuredBuffer< float > newLuminance : register( u0 );

// Slowly adjusts the scene luminance based on the previous scene luminance
// Adapt the luminance using Pattanaik's technique

[ numthreads( 1, 1, 1 ) ]
void main( )
{
	float lastLum = max( exp( lastLuminance[ 1 ] ), 0 );
	//float lastLum = lastLuminance[ 0 ];
	float currentLum = CalcLuminance( sourceTextureSRV.Load( int3( 0, 0, 10 ) ).rgb );

	float Tau = TauU;
	[flatten]
	if( lastLum > currentLum )
	{
		Tau = TauD;
	}

	// Adapt the luminance using Pattanaik's technique
	float adaptedLum = max( lastLum + ( currentLum - lastLum ) * ( 1 - exp( -TimeDelta * Tau ) ), 0.001 );

	newLuminance[ 0 ] = adaptedLum;
	newLuminance[ 1 ] = log( adaptedLum );
}
