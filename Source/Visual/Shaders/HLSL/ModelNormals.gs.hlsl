//////////////////////////////////////
// View Mesh Normal Lines Geometry //
////////////////////////////////////

//////////////
// GLOBALS //
////////////
cbuffer cbPerParticleSystem : register( b0 )
{
	float4x4 viewProjectionMatrix;
	matrix cameraProjectionMatrix;

	float4 cameraPosition;

	float gGameTime;
	float gTimeStep;

};

Texture1D textures[ 1 ];
SamplerState samplerState;

//////////////
// TYPEDEFS //
//////////////

struct Vertex
{
	float3 position  : POSITION;

	float3 normal	 : NORMAL;
	float3 tangent   : TANGENT;
	float3 binormal  : BINORMAL;
};

struct Fragment
{
	float4 position  : SV_POSITION;
	float4 colour	 : NORMAL;
};

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
[ maxvertexcount( 6 ) ]
void main( triangle Vertex gIn[ 3 ], inout LineStream<Fragment> ptStream )
{
	Fragment a;
	Fragment b;

	a.position = mul( float4( gIn[ 0 ].position, 1.0 ), viewProjectionMatrix );
	//a.colour = float4( 0, 1, 0, 0.5 ) * saturate( gGameTime );
	a.colour = float4( 0, 10, 0, 1.0 );

	b.position = mul( float4( gIn[ 0 ].position + gIn[ 0 ].normal * 0.45, 1.0 ), viewProjectionMatrix );
	b.colour = float4( 0, 0, 10, 1.0 );

	ptStream.Append( a );
	ptStream.Append( b );
	ptStream.RestartStrip( );



	a.position = mul( float4( gIn[ 1 ].position, 1.0 ), viewProjectionMatrix );
	//a.colour = float4( 0, 1, 0, 0.5 ) * saturate( gGameTime );
	a.colour = float4( 0, 10, 0, 1.0 );

	b.position = mul( float4( gIn[ 1 ].position + gIn[ 1 ].normal * 0.45, 1.0 ), viewProjectionMatrix );
	b.colour = float4( 0, 0, 10, 1.0 );

	ptStream.Append( a );
	ptStream.Append( b );
	ptStream.RestartStrip( );



	a.position = mul( float4( gIn[ 2 ].position, 1.0 ), viewProjectionMatrix );
	//a.colour = float4( 0, 1, 0, 0.5 ) * saturate( gGameTime );
	a.colour = float4( 0, 10, 0, 1.0 );

	b.position = mul( float4( gIn[ 2 ].position + gIn[ 2 ].normal * 0.45, 1.0 ), viewProjectionMatrix );
	b.colour = float4( 0, 0, 10, 1.0 );

	ptStream.Append( a );
	ptStream.Append( b );
	ptStream.RestartStrip( );
}

/******************************************************************************/
