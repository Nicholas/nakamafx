

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.Lighting.hlsl"
#include "algorithms.ParallaxMapping.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"

//////////////
// GLOBALS //
////////////
#define g_nMinSamples 1
#define g_nMaxSamples 20

Texture2D textures[ 9 ];
SamplerState samplerState;
SamplerComparisonState cmpSampler;

#define    COLOUR_TEX 0
#define     NORMAL_TX 1
#define     DEPTH_TEX 2
#define        AO_TEX 3
#define      BUMP_TEX 4
#define     GLOSS_TEX 5
#define  HI_GLOSS_TEX 6
#define ROUGHNESS_TEX 7

#define SHADOW_MAP 8

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ): SV_TARGET0
{
	fragment.normal = normalize( fragment.normal );
	fragment.view   = normalize( fragment.view );

	float PI = 3.1415926536f;

	float3 colour = kd * dLightC.rgb * 0.4;

	float t = dot( fragment.view, fragment.normal );
	t = abs( t );
	t *= PI * 0.5f;
	t = 1 - abs( cos( t ) );
	t = pow( t, 20 );
	t *= 0.35;

	colour += dLightC.rgb * t;

	return float4( colour, 1 );
}
