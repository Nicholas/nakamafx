
#ifndef _SKY_FRAGMENT
#define _SKY_FRAGMENT

struct Fragment
{
	float4 position   : SV_POSITION;
	float3 textureUVW : TEXCOORD0;
	float3 skyPos     : TEXCOORD1;
	float3 view       : TEXCOORD2;
};

#endif
