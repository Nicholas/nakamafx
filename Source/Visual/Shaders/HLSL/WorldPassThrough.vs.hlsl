
#include "type.vs.cb0.PerScene.hlsl"
#include "type.vs.cb1.PerModel.hlsl"
#include "type.iVS.Vertex.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader - Pass through point in world space
////////////////////////////////////////////////////////////////////////////////
VertexPTNTB main( VertexPTNTB vertex )
{
	VertexPTNTB fragment;

	fragment.position = mul( float4( vertex.position, 1.0f ), model ).xyz;

	fragment.textureUV = mul( float4( vertex.textureUV, 0, 0 ), model ).xy;

	fragment.normal   = normalize( mul( float4( vertex.normal, 0.0f ), model ) ).xyz;
	fragment.tangent  = normalize( mul( float4( vertex.tangent, 0.0f ), model ) ).xyz;
	fragment.binormal = normalize( mul( float4( vertex.binormal, 0.0f ), model ) ).xyz;

	return fragment;
}
