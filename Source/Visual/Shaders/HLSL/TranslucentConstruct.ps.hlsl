
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.Lighting.hlsl"
#include "algorithms.ParallaxMapping.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"
#include "algorithms.RGBM.hlsl"

#define LOG_LUV_ENCODE
#define RGBME_SCALE 8

///////////////
// TYPEDEFS //
/////////////
struct SFragment
{
	float2 refraction;
	uint surfaceReflection;
	uint subSurfaceReflection;
	uint subSurfaceRefraction;
	uint refractionIndex;
	float  depth;
};

struct SFragmentLink
{
	SFragment fragment;
	uint uNext;
};


Texture2D textures[ 14 ] : register( t0 );
SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );

#define    COLOUR_TEX 0
#define     NORMAL_TX 1
#define     DEPTH_TEX 2
#define        AO_TEX 3
#define      BUMP_TEX 4
#define     GLOSS_TEX 5
#define  HI_GLOSS_TEX 6
#define ROUGHNESS_TEX 7
#define METALNESS_TEX 8

#define DEPTH_MAP 9
#define SHADOW_1_MAP 10
#define SHADOW_2_MAP 11
#define SHADOW_TD_MAP 12
#define SHADOW_TC_MAP 13

StructuredBuffer<LightCluster> lightGridSRV : register( t14 );
StructuredBuffer< uint >  lightIndexListSRV : register( t15 );
StructuredBuffer<Light>           lightsSRV : register( t16 );

#define g_nMinSamples 1
#define g_nMaxSamples 1


// Fragment And Link Buffer
RWStructuredBuffer< SFragmentLink > FLBuffer : register( u3 );

// Start Offset Buffer
RWByteAddressBuffer StartOffsetBuffer : register( u4 );

struct DifferedData
{
	float4 normal: SV_TARGET0;
	float4 viewSP: SV_TARGET1;
	float4 F0    : SV_TARGET2;
};

struct Lighting
{
	float3 surfaceReflection;
	float3 subSurfaceReflection;
	float3 subSurfaceRefraction;
};

float3 culculateSubsurface(
	in float3 light,
	in float3 lightDirection,
	in float3 view,
	in float3 normal,
	in float3 ambiant,
	in float scale,
	in float power,
	in float distortion
)
{
	half3 vLTLight = lightDirection + normal * distortion;
	half fLTDot = pow( saturate( dot( -view, vLTLight ) ), power ) * scale;
	return light * ( fLTDot * ambiant ) * 0.1;
}

void calculateLightSSBRDF(
	in float3 radiance,
	in float3 L,
	in float3 V,
	in float3 N,
	in float3 diffuse,
	in float3 F0,
	in float roughness,
	in float3 ambiant,
	in float scale,
	in float power,
	in float distortion,
	inout Lighting lighting
)
{
	float3 H = normalize( V + L );

#ifdef FRESNEL_Schlick
	float3 F = fresnelSchlick( F0, dot( H, V ) );
#endif
#ifdef FRESNEL_Epic
	float3 F = fresnelSchlickEpic( F0, dot( V, H ) );
#endif

	float3 specular = cookTorranceSpecularBRDF( N, V, H, F, L, roughness );

	float NdotL = saturate( dot( N, L ) );

	lighting.surfaceReflection    += ( clamp( specular, 0, 2 ) + diffuse * ( 1 - F ) ) * radiance * NdotL;
	lighting.subSurfaceReflection += ambiant * radiance * ( NdotL * 0.1 + 0.25 ) / PBR_PI;
	lighting.subSurfaceRefraction += culculateSubsurface( radiance, L, V, N, ambiant, scale, power, distortion );
}


void calculateSpotLightSSBRDF(
	in float3 lightPosition,
	in float3 lightDirection,
	in float3 radiance,
	in float radius,
	in float spotlightAngle,
	in float3 fragmentPosition,
	in float3 V,
	in float3 N,
	in float3 diffuse,
	in float3 F0,
	in float roughness,
	in float3 ambiant,
	in float scale,
	in float power,
	in float distortion,
	inout Lighting lighting
)
{
	float3 L = lightPosition - fragmentPosition;
	float distance = length( L );

	if( distance > radius )
	{
		return;
	}

#ifdef POINT_FALLOFF_STD
	float fallOff = calulatePointLightFallOff( distance, radius );
#endif
#ifdef POINT_FALLOFF_Epic
	radiance *= calulatePointLightFallOffEpic( distance, radius );
#endif

	L /= distance;
	radiance *= calulateSpotLightFallOff( L, lightDirection, spotlightAngle );

	calculateLightSSBRDF( radiance, L, V, N, diffuse, F0, roughness, ambiant, scale, power, distortion, lighting );
}

void calculatePointLightSSBRDF(
	in float3 lightPosition,
	in float3 radiance,
	in float radius,
	in float3 P,
	in float3 V,
	in float3 N,
	in float3 diffuse,
	in float3 F0,
	in float roughness,
	in float3 ambiant,
	in float scale,
	in float power,
	in float distortion,
	inout Lighting lighting
)
{
	float3 L = lightPosition - P;
	float distance = length( L );

	if( distance > radius )
	{
		return;
	}

#ifdef POINT_FALLOFF_STD
	float fallOff = calulatePointLightFallOff( distance, radius );
#endif
#ifdef POINT_FALLOFF_Epic
	float fallOff = calulatePointLightFallOffEpic( distance, radius );
#endif

	radiance *= fallOff;
	L /= distance; // L = normalize( L )

	calculateLightSSBRDF( radiance, L, V, N, diffuse, F0, roughness, ambiant, scale, power, distortion, lighting );
}


Lighting calculatePBLight(
	in LightCluster cluster,
	in StructuredBuffer<uint>    lightIndexListSRV,
	in StructuredBuffer<Light>           lightsSRV,
	in float3 directionLightColour,
	in float3 position,
	in float3 albido,
	in float3 ks,
	in float3 N,
	in float3 V,
	in float metalness,
	in float roughness,
	in float ssPower,
	in float ssScale,
	in float ssBump
)
{
	roughness = roughness * 0.9f + 0.1f;
	float3      F0 = calculateF0( albido, ks, metalness );
	float3 diffuse = lambartDiffuseBDRF( albido, metalness );

	Lighting colours = ( Lighting )0;

	/////////////////
	// LOOP LIGHT //
	///////////////
	// Direction
	calculateLightSSBRDF(
		directionLightColour, dLightD,
		V, N, diffuse, F0, roughness,
		albido, ssScale, ssPower, ssBump,
		colours
	);

	// Add clustered lights
	for ( uint i = 0; i < cluster.count; i++ )
	{
		uint lightIndex = lightIndexListSRV[ cluster.offset + i ];
		Light     light =         lightsSRV[ lightIndex ];

		Lighting lightColour = ( Lighting ) 0;

		switch ( light.type )
		{
			case DIRECTIONAL_LIGHT:
			{
				calculateLightSSBRDF(
					light.colour.rgb, light.directionWS.xyz,
					V, N, diffuse, F0, roughness,
					albido, ssScale, ssPower, ssBump,
					colours
				);
				break;
			}
			case POINT_LIGHT:
			{
				calculatePointLightSSBRDF(
					light.positionWS.xyz, light.colour.rgb, light.range,
					position, V, N, diffuse, F0, roughness,
					albido, ssScale, ssPower, ssBump,
					colours
				);
				break;
			}
			case SPOT_LIGHT:
			{
				calculateSpotLightSSBRDF(
					light.positionWS.xyz, light.directionWS.xyz, light.colour.rgb, light.range, light.spotlightAngle,
					position, V, N, diffuse, F0, roughness,
					albido, ssScale, ssPower, ssBump,
					colours
				);
				break;
			}
		}
	}

	return colours;
}


//////////////////////////////////////////////////////////////////////////////////
//// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
DifferedData main( Fragment fragment )
{
	uint x = fragment.position.x;
	uint y = fragment.position.y;

	clip( textures[ DEPTH_MAP ].Load( float3( x, y, 0 ) ).r - fragment.position.z );

	float4 colourSample  = textures[ COLOUR_TEX ].Sample( samplerState, fragment.textureUV );
	fragment.normal   = normalize( fragment.normal );
	fragment.tangent  = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view     = normalize( fragment.view );

	float shadowBiasScale = saturate( abs( tan( acos( dot( fragment.normal, dLightD ) ) ) ) );

	float min1 = min( shadowSize1.x, shadowSize1.y );
	float min2 = min( shadowSize2.x, shadowSize2.y );

	float shadow1Bias = max( shadowSize1.x, shadowSize1.y ) * 1 * shadowBiasScale + min1 * 1.5f;
	float shadow2Bias = max( shadowSize2.x, shadowSize2.y ) * 2 * shadowBiasScale + min2 * 1.5f;

	float2 shadowFactor1 = getShadowCascade(
		textures[ SHADOW_1_MAP ],
		cmpSampler,
		fragment.shadow1Pos,
		shadowSize1,
		shadow1Bias
	);

	float shadowFactor2 = getShadowWithBias(
		textures[ SHADOW_2_MAP ],
		cmpSampler,
		fragment.shadow2Pos,
		shadowSize2,
		shadow2Bias
	);

	float shadowFactor = lerp( shadowFactor2, shadowFactor1.x, shadowFactor1.y );

	float4 shadowColour = getShadowColour(
		textures[ SHADOW_TD_MAP ], cmpSampler,
		textures[ SHADOW_TC_MAP ], samplerState,
		fragment.shadow1Pos,
		shadow1Bias );

	// Turn light off a sun set...
	shadowFactor *= smoothstep( 0.05, 0.3, dot( dLightD, float3( 0, 1, 0 ) ) );

	float3 lightColour = lerp( dLightC.rgb, shadowColour.rgb, ( shadowColour.a * 0.25 ) ) * shadowFactor;

	int clusterIndex = getClusterIndex( fragment.position.xy, fragment.viewPos.z, float2( targetWidth, targetHeight ), zDepths.x, zDepths.y );


	Lighting lighting = calculatePBLight(
		lightGridSRV[ clusterIndex ],
		lightIndexListSRV, lightsSRV,
		lightColour,
		fragment.worldPos,
		colourSample.rgb * kd,
		ks,
		fragment.normal,
		-fragment.view,
		metalness,
		roughness,
		ssPower,
		ssScale,
		ssBump
	);


	float invSAlpha = 1 - sAlpha;
	float invSsAlpha = 1 - ssAlpha;
	float VdotN = dot( fragment.view, fragment.normal );
	float s = step( 0, -VdotN );

	float surfaceAlpha = s * ssAlpha + (1 - s);
	lighting.surfaceReflection *= sAlpha;
	lighting.subSurfaceReflection *= invSAlpha * surfaceAlpha;
	lighting.subSurfaceRefraction *= invSAlpha * surfaceAlpha;

	// Create fragment data.
	SFragmentLink element;

	element.fragment.refractionIndex      = getUint( float4( kd * invSAlpha, absorption ) );
#ifdef LOG_LUV_ENCODE
	element.fragment.surfaceReflection    = getUint( LogLuvEncode( lighting.surfaceReflection    ) );
	element.fragment.subSurfaceReflection = getUint( LogLuvEncode( lighting.subSurfaceReflection ) );
	element.fragment.subSurfaceRefraction = getUint( LogLuvEncode( lighting.subSurfaceRefraction ) );
#else
	element.fragment.surfaceReflection = getUint( RGBMEncodeScaled( lighting.surfaceReflection, RGBME_SCALE ) );
	element.fragment.subSurfaceReflection = getUint( RGBMEncode( lighting.subSurfaceReflection ) );
	element.fragment.subSurfaceRefraction = getUint( RGBMEncode( lighting.subSurfaceRefraction ) );
#endif

	element.fragment.refraction = fragment.normal.xy * ( 1 - ior ) * 0.02f * VdotN * float2( -1, 1 );
	element.fragment.depth = fragment.viewPos.z;

	// Increment and get current pixel count.
	uint uPixelCount = FLBuffer.IncrementCounter();

	// Read and update Start Offset Buffer.
	uint uIndex = y * targetWidth + x;
	uint uStartOffsetAddress = 4 * uIndex;
	uint uOldStartOffset;

	StartOffsetBuffer.InterlockedExchange( uStartOffsetAddress, uPixelCount, uOldStartOffset );

	// Store fragment link.
	element.uNext = uOldStartOffset;
	FLBuffer[ uPixelCount ] = element;

	DifferedData data = (DifferedData)0;

	data.normal = float4( fragment.normal.rgb, 1 );
	data.viewSP = float4( fragment.viewPos, fragment.position.z * fragment.position.w );
	data.F0     = float4( lighting.surfaceReflection * sAlpha, roughness );

	return data;
}