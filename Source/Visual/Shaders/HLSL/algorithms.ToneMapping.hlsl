
#ifndef _ALGORITHM_TONE_MAPPING_HLSL
#define _ALGORITHM_TONE_MAPPING_HLSL

#include "algorithms.ColourSpace.hlsl"


// Function used by the Uncharte2D tone mapping curve
float3 U2Func(
	float3 colour,
	float ShoulderStrength,
	float LinearStrength,
	float LinearAngle,
	float ToeStrength,
	float ToeNumerator,
	float ToeDenominator
	)
{
	float3 x = colour;
	float A = ShoulderStrength;
	float B = LinearStrength;
	float C = LinearAngle;
	float D = ToeStrength;
	float E = ToeNumerator;
	float F = ToeDenominator;

	return ( ( x*( A*x + C*B ) + D*E ) / ( x*( A*x + B ) + D*F ) ) - E / F;
}

// Applies the Uncharted 2 filmic tone mapping curve
float3 ToneMapFilmicU2(
	float3 colour,
	float ShoulderStrength,
	float LinearStrength,
	float LinearAngle,
	float ToeStrength,
	float ToeNumerator,
	float ToeDenominator,
	float LinearWhite
	)
{
	float3 numerator = U2Func(
		colour,
		ShoulderStrength,
		LinearStrength,
		LinearAngle,
		ToeStrength,
		ToeNumerator,
		ToeDenominator );

	float3 denominator = U2Func(
		LinearWhite,
		ShoulderStrength,
		LinearStrength,
		LinearAngle,
		ToeStrength,
		ToeNumerator,
		ToeDenominator );

	return numerator / denominator;
}






// Approximates luminance from an RGB value
float CalcLuminance( float3 colour )
{
	return max( rgbToLuminance( colour ), 0.0001f );
}

/**
// Logarithmic mapping
float3 ToneMapLogarithmic( float3 colour )
{
	float pixelLuminance = CalcLuminance( colour );
	float toneMappedLuminance = log10( 1 + pixelLuminance ) / log10( 1 + WhiteLevel );
	return toneMappedLuminance * pow( colour / pixelLuminance, LuminanceSaturation );
}



// Drago's Logarithmic mapping
float3 ToneMapDragoLogarithmic( float3 colour )
{
	float pixelLuminance = CalcLuminance( v );
	float toneMappedLuminance = log10( 1 + pixelLuminance );
	toneMappedLuminance /= log10( 1 + WhiteLevel );
	toneMappedLuminance /= log10( 2 + 8 * ( ( pixelLuminance / WhiteLevel ) * log10( Bias ) / log10( 0.5f ) ) );
	return toneMappedLuminance * pow( colour / pixelLuminance, LuminanceSaturation );
}

// Exponential mapping
float3 ToneMapExponential( float3 colour )
{
	float pixelLuminance = CalcLuminance( colour );
	float toneMappedLuminance = 1 - exp( -pixelLuminance / WhiteLevel );
	return toneMappedLuminance * pow( colour / pixelLuminance, LuminanceSaturation );
}

// Applies Reinhard's basic tone mapping operator
float3 ToneMapReinhard( float3 colour )
{
	float pixelLuminance = CalcLuminance( colour );
	float toneMappedLuminance = pixelLuminance / ( pixelLuminance + 1 );
	return toneMappedLuminance * pow( colour / pixelLuminance, LuminanceSaturation );
}

// Applies Reinhard's modified tone mapping operator
float3 ToneMapReinhardModified( float3 colour )
{
	float pixelLuminance = CalcLuminance( colour );
	float toneMappedLuminance = pixelLuminance * ( 1.0f + pixelLuminance / ( WhiteLevel * WhiteLevel ) ) / ( 1.0f + pixelLuminance );
	return toneMappedLuminance * pow( colour / pixelLuminance, LuminanceSaturation );
}

// Applies the filmic curve from John Hable's presentation
float3 ToneMapFilmicALU( float3 colour )
{
	colour = max( 0, colour - 0.004f );
	colour = ( colour * ( 6.2f * colour + 0.5f ) ) / ( colour * ( 6.2f * colour + 1.7f ) + 0.06f );

	// result has 1/2.2 baked in
	return pow( colour, 2.2f );
}
//*/

#endif // _ALGORITHM_TONE_MAPPING_HLSL
