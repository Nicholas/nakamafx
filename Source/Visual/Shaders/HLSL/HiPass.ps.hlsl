
#include "type.iPS.TexturedFragment.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer bloom : register( b0 )
{
	float2 texelSize;
	float threshold;
	float magnatude;
	float sigma;
};

Texture2D image : register( t0 );
SamplerState samplerState : register( s0 );

StructuredBuffer< float > luminance : register( t1 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ) : SV_Target0
{
	//float4 colour =      image.Sample( samplerState, fragment.textureUV + float2(  texelSize.x,  texelSize.y ) );
	//	   colour = max( image.Sample( samplerState, fragment.textureUV + float2( -texelSize.x,  texelSize.y ) ), colour );
	//	   colour = max( image.Sample( samplerState, fragment.textureUV + float2(  texelSize.x, -texelSize.y ) ), colour );
	//	   colour = max( image.Sample( samplerState, fragment.textureUV + float2( -texelSize.x, -texelSize.y ) ), colour );
	//	   colour = max( image.Sample( samplerState, fragment.textureUV + float2( 0, 0 ) ), colour );

	uint2 px = fragment.textureUV * texelSize;

	float4 colour  = image.Load( float3( px + uint2(  0,  0 ), 0 ) );

		   colour += image.Load( float3( px + uint2(  1,  1 ), 0 ) );
		   colour += image.Load( float3( px + uint2( -1,  1 ), 0 ) );
		   colour += image.Load( float3( px + uint2(  1, -1 ), 0 ) );
		   colour += image.Load( float3( px + uint2( -1, -1 ), 0 ) );

		   colour += image.Load( float3( px + uint2( -1,  0 ), 0 ) );
		   colour += image.Load( float3( px + uint2(  0, -1 ), 0 ) );
		   colour += image.Load( float3( px + uint2(  1,  0 ), 0 ) );
		   colour += image.Load( float3( px + uint2(  0,  1 ), 0 ) );

	colour /= 9;

	float avgLuminance   = max( exp( luminance[ 1 ] ), 0.005f );
	float keyValue       = 1.03f - ( 2.0f / ( 2 + log10( avgLuminance + 1 ) ) );
	float linearExposure = ( keyValue / avgLuminance );
	float exposure       = log2( max( linearExposure, 0.0001f ) );
	exposure            -= threshold;
	colour               = exp2( exposure ) * colour;

	colour.a = 1;

	return colour;
}