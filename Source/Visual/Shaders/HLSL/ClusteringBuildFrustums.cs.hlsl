
#include "type.iCS.Default.hlsl"
#include "type.Light.hlsl"
#include "type.Frustum.hlsl"

#include "algorithms.Lighting.hlsl"
#include "algorithms.ClusteredLighting.hlsl"
#include "algorithms.IntersectionTesting.hlsl"

// Global data.
cbuffer DispatchParams : register( b0 )
{
	// The number of thread groups that have been dispatched
	uint3 numThreadGroups;
	uint p1;

	// The total number of threads dispatched thats expected to perform real work.
	uint3 numThreads;
	uint p2;

	Matrix projectionInv;
	float4 ScreenDimensions;

	float nearZPlane;
	float  farZPlane;
}

RWStructuredBuffer<Frustum> frustumsUAV : register( u0 );



 // Convert clip space coordinates to view space
float4 ClipToView( float4 clip, matrix InverseProjection )
{
	// View space position.
	float4 view = mul( InverseProjection, clip );
	// Perspective projection.
	view = view / view.w;

	return view;
}

// Convert screen space coordinates to view space.
float4 ScreenToView( float4 screen, float2 ScreenDimensions, matrix InverseProjection )
{
	// Convert to normalized texture coordinates
	//float2 texCoord = screen.xy / ScreenDimensions;
	float2 texCoord = screen.xy * ScreenDimensions;

	// Convert to clip space
	float4 clip = float4( float2( texCoord.x, 1.0f - texCoord.y ) * 2.0f - 1.0f, screen.z, screen.w );
	//float4 clip = float4( float2( texCoord.x, texCoord.y ) * 2.0f - 1.0f, screen.z, screen.w );

	return ClipToView( clip, InverseProjection );
}



[ numthreads( 1, 1, 1 ) ]
void main( ComputeShaderInput input )
{
	const float3 eyePosition = 0;

	// Compute the 4 corners of the view frustum on the far plane.
	float4 screenSpace[ 4 ];

#ifdef LEFT_HAND_PLANES
	// Top left point
	screenSpace[0] = float4(         input.dispatchThreadID.xy                                    * BLOCK_SIZE, 1.0f, 1.0f );
	// Top right point
	screenSpace[1] = float4( float2( input.dispatchThreadID.x + 1, input.dispatchThreadID.y     ) * BLOCK_SIZE, 1.0f, 1.0f );
	// Bottom left point
	screenSpace[2] = float4( float2( input.dispatchThreadID.x    , input.dispatchThreadID.y + 1 ) * BLOCK_SIZE, 1.0f, 1.0f );
	// Bottom right point
	screenSpace[3] = float4( float2( input.dispatchThreadID.x + 1, input.dispatchThreadID.y + 1 ) * BLOCK_SIZE, 1.0f, 1.0f );
#endif
#ifdef RIGHT_HAND_PLANES
	// Top left point
	screenSpace[0] = float4( input.dispatchThreadID.xy * BLOCK_SIZE, -1.0f, 1.0f );
	// Top right point
	screenSpace[1] = float4( float2( input.dispatchThreadID.x + 1, input.dispatchThreadID.y     ) * BLOCK_SIZE, -1.0f, 1.0f );
	// Bottom left point
	screenSpace[2] = float4( float2( input.dispatchThreadID.x    , input.dispatchThreadID.y + 1 ) * BLOCK_SIZE, -1.0f, 1.0f );
	// Bottom right point
	screenSpace[3] = float4( float2( input.dispatchThreadID.x + 1, input.dispatchThreadID.y + 1 ) * BLOCK_SIZE, -1.0f, 1.0f );
#endif

	float3 viewSpace[4];
	// Now convert the screen space points to view space
	for ( int i = 0; i < 4; i++ )
	{
		viewSpace[i] = ScreenToView( screenSpace[i], ScreenDimensions.zw, projectionInv ).xyz;
	}

	// Now build the frustum planes from the view space points
	Frustum frustum;
	frustum.planes[  LEFT_PLANE] = ComputePlane( eyePosition, viewSpace[2], viewSpace[0] );
	frustum.planes[ RIGHT_PLANE] = ComputePlane( eyePosition, viewSpace[1], viewSpace[3] );
	frustum.planes[   TOP_PLANE] = ComputePlane( eyePosition, viewSpace[0], viewSpace[1] );
	frustum.planes[BOTTOM_PLANE] = ComputePlane( eyePosition, viewSpace[3], viewSpace[2] );

	frustum.zFar  = nearZPlane * pow( farZPlane / nearZPlane, float( input.dispatchThreadID.z + 1 ) / numThreads.z );
	frustum.zNear = nearZPlane * pow( farZPlane / nearZPlane, float( input.dispatchThreadID.z ) / numThreads.z );

	frustum.zFar = min( max( frustum.zFar, nearZPlane ), farZPlane );
	frustum.zNear = min( max( frustum.zNear, 0 ), farZPlane );

	 // Store the computed frustum in global memory (if our thread ID is in bounds of the grid).
	if ( input.dispatchThreadID.x < numThreads.x && input.dispatchThreadID.y < numThreads.y && input.dispatchThreadID.z < numThreads.z )
	{
		uint index = input.dispatchThreadID.x + ( input.dispatchThreadID.y * numThreads.x ) + ( input.dispatchThreadID.z * numThreads.x * numThreads.y );
		frustumsUAV[ index ] = frustum;
	}
}