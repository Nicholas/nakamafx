
#ifndef _PARTICLE_FRAGMENT
#define _PARTICLE_FRAGMENT

struct Fragment
{
	float4 posH  : SV_Position;
	float4 colour: COLOR;
	float2 texC  : TEXCOORD0;
	float  age   : TEXCOORD1;
};

#endif
