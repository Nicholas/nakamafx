
#ifndef _CB_HISTOGRAM
#define _CB_HISTOGRAM

cbuffer cbHistogram : register( b0 )
{
	float inputLuminanceMin;
	float inputLuminanceMax;

	float outputLuminanceMin;
	float outputLuminanceMax;

	uint outputWidth;
	uint outputHeight;

	uint numPerTileHistograms;
	uint tilesX;
};

#endif
