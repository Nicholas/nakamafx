#define NUM_HISTOGRAM_BINS (64)

#include "type.cs.cb0.Histogram.hlsl"

//Buffer< uint > perTileHistogramSRV;
StructuredBuffer< uint > perTileHistogramSRV : register( t0 );

//RWBuffer< uint > mergedHistogramUAV : register( u0 );
RWStructuredBuffer< uint > mergedHistogramUAV : register( u0 );

/**
// Thread per tile
// WE could probably make this better and lock free by having a thread per bin
// Might it be a cache thing to wait for locks rather than wait for memory??? Look at the memory layout bellow ---> I think we can improve this with a little rotation...
#define NUM_TILES_PER_THREAD_GROUP 768
[ numthreads( NUM_TILES_PER_THREAD_GROUP, 1, 1 ) ]
void merge_histograms_cs( uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID )
{
	// each thread adds a tiled histogram to the to the overall histogram stored in gpu device memory
	uint tileIndex = globalIdx.x;

	if( tileIndex < numPerTileHistograms )
	{
		for( uint binID = 0; binID < NUM_HISTOGRAM_BINS; ++binID )
		{
			InterlockedAdd( mergedHistogramUAV[ binID ], perTileHistogramSRV[ tileIndex * NUM_HISTOGRAM_BINS + binID ] );
		}
	}
}
//*/

[ numthreads( 1, 1, 1 ) ]
void main( uint3 dispatchId : SV_DispatchThreadID )
{
	uint gID = dispatchId.x;
	uint count = 0;
	uint start = numPerTileHistograms * gID;
	uint end = start + numPerTileHistograms;

	for( uint i = start; i < end; ++i )
	{
		count += perTileHistogramSRV[ i ];
	}

	mergedHistogramUAV[ gID ] = count;
}
