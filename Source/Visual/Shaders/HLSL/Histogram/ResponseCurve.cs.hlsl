#define NUM_HISTOGRAM_BINS (64)
#define LOOP_TRIES (10)

#include "type.cs.cb0.Histogram.hlsl"

StructuredBuffer< uint > mergedHistogramSRV : register( t0 );

RWStructuredBuffer< float > responseCurveUAV : register( u0 );

groupshared float frequencyPerBin[ NUM_HISTOGRAM_BINS ];
groupshared float initialFrequencyPerBin[ NUM_HISTOGRAM_BINS ];

/**/
[ numthreads( 1, 1, 1 ) ]
void main( uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID )
{
	uint bin;
	// Compute the initial frequencies and get the data in to local shared memory.
	float T = 0.0f;

	for( bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
	{
		float frequency = float( mergedHistogramSRV[ bin ] );
		frequencyPerBin[ bin ] = frequency;
		initialFrequencyPerBin[ bin ] = frequency;
		T += frequency;
	}

	// Naive histogram adjustment
	float recipDisplayRange = 1.0 / log( outputLuminanceMax ) - log( outputLuminanceMin );
	//float recipDisplayRange = 1.0;

	// histogram bin step size in log( cd/m2 )
	float deltaB = ( inputLuminanceMax - inputLuminanceMin ) / float( NUM_HISTOGRAM_BINS );
	float tolerance = T * 0.025;
	float trimmings = 0.0f;
	uint loops = 0;
	do
	{
		// Work out the new total
		T = 0.0f;
		for( bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
		{
			T += frequencyPerBin[ bin ];
		}

		if( T < tolerance )
		{
			// The convergence is wrong -> return it back to the way it was...
			T = 0.0f;

			for( bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
			{
				frequencyPerBin[ bin ] = initialFrequencyPerBin[ bin ];
				T += frequencyPerBin[ bin ];
			}
			break;
		}

		// Compute the ceiling
		trimmings = 0.0f;
		float ceiling = T * deltaB * recipDisplayRange;

		for( bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
		{
			if( frequencyPerBin[ bin ] > ceiling )
			{
				trimmings += frequencyPerBin[ bin ] - ceiling;
				frequencyPerBin[ bin ] = ceiling;
			}
		}

		T -= trimmings;

		++loops;
	}
	while( trimmings > tolerance && loops < LOOP_TRIES );


	// Compute the cumulative distribution function, per bin
	float recipT = 1.0f / T;
	float sum = 0.0;

	for( bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
	{
		sum += frequencyPerBin[ bin ] * recipT;
		responseCurveUAV[ bin ] = sum;
		//sum = frequencyPerBin[ bin ] * recipT;
	}
}
//*/

/**
[ numthreads( 1, 1, 1 ) ]
void compute_histogram_response_curve_cs( uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID )
{
	// Compute the initial frequencies and get the data in to local shared memory.
	float T = 0.0f;

	for( uint bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
	{
		float frequency = float( mergedHistogramSRV[ bin ] );
		frequencyPerBin[ bin ] = frequency;
		T = max( T, frequency );
		//T += frequency;
	}

	//T = T == 0 ? 1 : T;
	float recipT = 1.0f / T;
	float sum;

	for( uint bin2 = 0; bin2 < NUM_HISTOGRAM_BINS; ++bin2 )
	{
		sum = frequencyPerBin[ bin2 ] * recipT;
		responseCurveUAV[ bin2 ] = sum;
	}
}
//*/

/**
[ numthreads( 1, 1, 1 ) ]
void compute_histogram_response_curve_cs( uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID )
{
	//for( uint bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
	//{
	//	responseCurveUAV[ bin ] = float( bin ) / NUM_HISTOGRAM_BINS;
	//}

	//responseCurveUAV[ 0 ] = 1.0f;
	//responseCurveUAV[ 1 ] = 0.1f;
	//
	//responseCurveUAV[ 2 ] = 0.0f;
	//responseCurveUAV[ 3 ] = 0.5f;
	//
	//responseCurveUAV[ 4 ] = 0.0f;
	//responseCurveUAV[ 5 ] = 1.0f;
	//
	//responseCurveUAV[ 6 ] = 0.0f;
	//responseCurveUAV[ 7 ] = 1.0f;

	for( uint bin = 0; bin < NUM_HISTOGRAM_BINS; ++bin )
	{
		//responseCurveUAV[ bin ] = 0.5f;
		responseCurveUAV[ bin ] = float( bin ) / NUM_HISTOGRAM_BINS;
	}
}
//*/
