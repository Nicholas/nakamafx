#define NUM_HISTOGRAM_BINS (64)
#define HISTOGRAM_TILE_SIZE_X (32)
#define HISTOGRAM_TILE_SIZE_Y (16)

#include "../algorithms.ColourSpace.hlsl"
#include "type.cs.cb0.Histogram.hlsl"

Texture2D sourceTextureSRV : register( t0 );

RWStructuredBuffer< uint > perTileHistogramUAV : register( u0 );

groupshared uint histogram[ NUM_HISTOGRAM_BINS ];


// Calculate the histogram in a set of tiles to manage local shared memory limits...
[ numthreads( HISTOGRAM_TILE_SIZE_X, HISTOGRAM_TILE_SIZE_Y, 1 ) ]
void main(
	uint3 globalIdx : SV_DispatchThreadID,
	uint3 groupIdx : SV_GroupID,
	uint gID : SV_GroupIndex )
{
	uint tile   = groupIdx.x + groupIdx.y * tilesX;

	if( gID == 0 )
	{
		for( uint i = 0; i < NUM_HISTOGRAM_BINS; ++i )
		{
			histogram[ i ] = 0;
		}
	}

	GroupMemoryBarrierWithGroupSync( );

	if( globalIdx.x < outputWidth && globalIdx.y < outputHeight )
	{
		float3 fragmentValue = sourceTextureSRV.Load( int3( globalIdx.xy, 0 ) ).rgb;
		uint bin = rgbToHistogramBin( fragmentValue, inputLuminanceMin, inputLuminanceMax, NUM_HISTOGRAM_BINS );

		InterlockedAdd( histogram[ bin ], 1u );
	}

	GroupMemoryBarrierWithGroupSync( );

	if( gID == 0 )
	{
		for( uint i = 0; i < NUM_HISTOGRAM_BINS; ++i )
		{
			uint outputIndex = tile + ( i * numPerTileHistograms );
			perTileHistogramUAV[ outputIndex ] = histogram[ i ];
		}
	}
}