

#ifndef _PLANE_SHADER_TYPE_
#define _PLANE_SHADER_TYPE_

struct Plane
{
	float3 N; // Palne Normal
	float  d; // Distance to origin
};

struct Plane2
{
	float3 N; // Palne Normal
	float3 p; // Point on the plane
};

#endif //_PLANE_SHADER_TYPE_