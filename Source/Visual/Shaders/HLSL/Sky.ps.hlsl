
#include "algorithms.Noise.hlsl"
#include "algorithms.IntersectionTesting.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "type.iPS.SkyFragment.hlsl"
#include "algorithms.hlsl"

Texture2D textures[ 9 ];
SamplerState samplerState;

// ( 6'371 Km ) Earth
# define RADIUS_PLANET    6371e3
// ( 6'420 Km ) > 99%
# define RADIUS_ATMOSPHER 6420e3

static const float RAYLEIGH_SCALE_HEIGHT   = 7994;
static const float MIE_SCALE_HEIGHT		= 1200;

static const float g						= 0.76;

static const float3 BETA_RAY = float3(  5.8e-6, 13.5e-6, 33.1e-6 ); // Rayleigh scattering coefficients at sea level
static const float3 BERA_MIE = float3( 21.0e-6, 21.0e-6, 21.0e-6 ); // Mie scattering coefficients at sea level

float3 accumulateViewRay( Ray viewRay, Sphere planet, Sphere atmosphere )
{
	float atIntMin = 1;
	float atIntMax = 1;

	if( ! intersect( viewRay, atmosphere, atIntMin, atIntMax ) )
	{
		return float3( 0, 0, 0 );
	}

	const float3 SUN_INTENSITY	= dLightC.rgb * 32;

	const int ViewRaySamples = 16;
	const int LightRaySamples = 4;

	const float3 sunDirection = dLightD;

	float segmentLength = ( atIntMax - atIntMin ) / ViewRaySamples;
	float tCurrent      = atIntMin;

	float3 sumR = float3( 0, 0, 0 );
	float3 sumM = float3( 0, 0, 0 );

	float opticalDepthR = 0;
	float opticalDepthM = 0;

	float mu = dot( viewRay.d, normalize( sunDirection ) );

	float muSqr = mu * mu;
	float gSqr = g * g;

	float phaseR = 3 / ( 16 * PI ) * ( 1 + muSqr );
	float phaseM = 3 / (  8 * PI ) * ( ( 1 - gSqr ) * ( 1 + muSqr ) / ( ( 2 + gSqr ) * pow( abs( 1 + gSqr - 2 * g * mu ), 1.2 ) ) );

	for( int i = 0; i < ViewRaySamples; i++ )
	{
		float3  samplePosition = viewRay.o + viewRay.d * ( tCurrent + 0.5 * segmentLength );
		float   height = length( samplePosition ) - planet.r;

		// compute optical depth for light

		float hr = exp( -height / RAYLEIGH_SCALE_HEIGHT ) * segmentLength;
		float hm = exp( -height / MIE_SCALE_HEIGHT      ) * segmentLength;

		opticalDepthR += hr;
		opticalDepthM += hm;

		// light optical depth

		Ray lightRay;
		lightRay.o = samplePosition;
		lightRay.d = normalize( sunDirection );

		float lmin;
		float lmax;

		intersect( lightRay, atmosphere, lmin, lmax );

		float segmentLengthLight = lmax / LightRaySamples;
		float tCurrentLight = 0;
		float opticalDepthLightR = 0;
		float opticalDepthLightM = 0;

		int j = 0;

		for( ; j < LightRaySamples ; j++ )
		{
			float3 samplePositionLight = lightRay.o + lightRay.d * ( tCurrentLight + 0.5 * segmentLengthLight );

			float heightLight = length( samplePositionLight ) - planet.r;

			if( heightLight < 0 )
			{
				j = LightRaySamples + 1;
			}
			else
			{
				opticalDepthLightR += exp( -heightLight / RAYLEIGH_SCALE_HEIGHT ) * segmentLengthLight;
				opticalDepthLightM += exp( -heightLight / MIE_SCALE_HEIGHT      ) * segmentLengthLight;

				tCurrentLight += segmentLengthLight;
			}
		}

		if( j == LightRaySamples )
		{
			float3 tau = BETA_RAY * 1.0 * ( opticalDepthR + opticalDepthLightR )
					   + BERA_MIE * 1.1 * ( opticalDepthM + opticalDepthLightM );

			float3 attenuation = exp( -tau );

			sumR += hr * attenuation;
			sumM += hm * attenuation;
		}

		tCurrent += segmentLength;
	}

	return ( SUN_INTENSITY * ( sumR * phaseR * BETA_RAY + sumM * phaseM * BERA_MIE ) );
}

float3 computeIncidentLight( Ray viewRay )
{
	Sphere planet;
	Sphere atmosphere;

	planet.c     = float3( 0, 0, 0 );
	planet.r     = RADIUS_PLANET;

	atmosphere.c = float3( 0, 0, 0 );
	atmosphere.r = RADIUS_ATMOSPHER;

	return accumulateViewRay( viewRay, planet, atmosphere );
}



float getLayer( float2 uv )
{
	float offset = ( snoise2D( uv + time ) + 1 ) * 0.25;
	offset += ( snoise2D( uv * 6 + 5000 + float2( time * 0.4, time * 0.1 ) ) + 1 ) * 0.125;
	offset += ( snoise2D( ( uv + float2( time * 0.1, time * 0.05 ) )  * 15 ) + 1 ) * 0.0625;
	offset += ( snoise2D( ( uv + time * 0.105 ) * 30 ) + 1 ) * 0.03125;
	offset += ( snoise2D( ( uv + time * 0.005 ) * 60 ) + 1 ) * 0.015625;
	offset += ( snoise2D( ( uv + time * 0.0025 ) * 80 ) + 1 ) * 0.0078125;
	offset += ( snoise2D( ( uv + time * 0.00125 ) * 150 ) + 1 ) * 0.0078125;//0.00390625;

	return smoothstep( 0.4, 0.9, offset );
}

float3 getDensity( float2 uv, float3 l )
{
	float a = getLayer( uv );

	float2 offset = l.xz * 0.0025f;

	float b = getLayer( uv + offset );
	float c = getLayer( uv + offset * 2 );
	float d = getLayer( uv + offset * 3 );
	float e = getLayer( uv + offset * 4 );
	float f = getLayer( uv + offset * 5 );

	float g = ( a + b + c + d + e + f ) * 0.166666;

	return float3( 0, g, a );
}


float4 calculateClouds( Ray viewRay )
{
	float l;
	Plane2 cloudPlane;
	cloudPlane.p = float3( 0, RADIUS_PLANET + 2500, 0 );
	cloudPlane.N = float3( 0, 1, 0 );

	if( intersect( viewRay, cloudPlane, l ) )
	{

		Ray cloudRay;
		cloudRay.o = viewRay.d * l + viewRay.o;
		cloudRay.d = dLightD;

		float3 offset = getDensity( cloudRay.o.xz * 0.0001, dLightD );

		float edgeBlend = smoothstep( 5000, 150000, abs( l ) );


		Sphere atmosphere;

		atmosphere.c = float3( 0, 0, 0 );
		atmosphere.r = RADIUS_ATMOSPHER;

		float3 colour;


		float lmin;
		float lmax;

		intersect( cloudRay, atmosphere, lmin, lmax );

		float3  samplePosition = cloudRay.o + cloudRay.d * lmax * 0.04f;

		lmax *= 0.03f;

		float   height = length( samplePosition ) - RADIUS_PLANET;

		float hr = exp( -height / RAYLEIGH_SCALE_HEIGHT ) * lmax;
		float hm = exp( -height / MIE_SCALE_HEIGHT      ) * lmax;

		float3 tau = BETA_RAY * 1.0 * ( hr ) + BERA_MIE * 1.1 * ( hm );

		float3 attenuation = exp( -tau );

		float3 sumR = hr * attenuation;
		float3 sumM = hm * attenuation;

		float mu = dot( cloudRay.d, normalize( dLightD ) );

		float muSqr = mu * mu;
		float gSqr = g * g;

		float phaseR = 3 / ( 16 * PI ) * ( 1 + muSqr );
		float phaseM = 3 / (  8 * PI ) * ( ( 1 - gSqr ) * ( 1 + muSqr ) / ( ( 2 + gSqr ) * pow( abs( 1 + gSqr - 2 * g * mu ), 1.2 ) ) );

		float sunSet = smoothstep( -0.05, 0.12, dot( dLightD, float3( 0, 1, 0 ) ) );

		colour = max( dLightC.rgb * ( sumR * phaseR * BETA_RAY + sumM * phaseM * BERA_MIE ), 0 );
		colour = lerp( colour * ( ( 0.75 - offset.g * 0.5 ) ), dLightC.rgb, sunSet );

		float blend = ( 1 - offset.g * 0.65 );

		colour = max( float3( 0.1, 0.105, 0.11 ), colour ) * blend;

		return float4( colour, ( 1 - edgeBlend ) * ( offset.b ) );
	}

	return float4( 0, 0, 0, 0 );

}

float3 calculateStars( float3 direction )
{
	float flikker = snoise3D( ( direction + time * 1.5 ) * 100 ) * 0.01f;
	float stars   = snoise3D( direction * 200 ) + flikker;
	return pow( saturate( ( stars + 1 ) * 0.5 ), 120 ) * 20;
}

struct Pixel
{
	float4 colour: SV_TARGET0;
	float4 normal: SV_TARGET1;
	float4 viewSP: SV_TARGET2;
};


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
[earlydepthstencil]
Pixel main( Fragment fragment )
{
	Ray ray;
	ray.d = normalize( fragment.textureUVW );
	ray.o = fragment.skyPos;
	ray.o.y += RADIUS_PLANET;

	float3 nLD = normalize( dLightD );
	float lDot = dot( ray.d, nLD );
	float horizon = smoothstep( -0.01, 0.01, ray.d.y );
	float3 sunLight = horizon * dLightC.rgb;
	float3 sun  = smoothstep( 0.9991, 0.9998, lDot );
	float3 moon = smoothstep( 0.99991, 0.99995, -lDot ) * 0.04;

	float3 sky    = computeIncidentLight( ray );
	float3 stars  = calculateStars( ray.d );
	float4 clouds = calculateClouds( ray );

	sky = max( sky, 0 );
	float3 skybox = lerp( ( moon + sun ) * sunLight + sky + stars, clouds.rgb, clouds.a );

	Pixel pixel;
	pixel.colour = float4( skybox, 1 );
	pixel.normal = float4( -ray.d, 0 );
	pixel.viewSP = float4( normalize( fragment.view.xyz ) * 512, 0 );

	return pixel;
}
