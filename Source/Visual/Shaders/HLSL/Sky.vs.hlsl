
#include "type.vs.cb0.PerScene.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.SkyFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexPT vertex )
{
	Fragment fragment;

	float4 position = float4( vertex.position * 128 + cameraPosition, 1.0f );
	fragment.position     = mul( position, viewProjectionMatrix );
	fragment.textureUVW = normalize( vertex.position );
	fragment.skyPos     = cameraPosition;
	fragment.view       = mul( position, view ).xyz;

	fragment.position.z = fragment.position.w - 0.0001f;

	return fragment;
}