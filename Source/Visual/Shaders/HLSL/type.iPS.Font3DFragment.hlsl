
#ifndef _FONT_3D_FRAGMENT
#define _FONT_3D_FRAGMENT

struct Fragment
{
	float4 position  : SV_POSITION;

	float3 normal	 : NORMAL;
	float3 tangent   : TANGENT;
	float3 binormal  : BINORMAL;

	float3 textureUV : TEXCOORD;

	float3 viewRay   : TEXCOORD1;
	float3 positionW : TEXCOORD2;
	float3 positionV : TEXCOORD3;
	float4 positionS1: TEXCOORD4;
	float4 positionS2: TEXCOORD5;
};

#endif
