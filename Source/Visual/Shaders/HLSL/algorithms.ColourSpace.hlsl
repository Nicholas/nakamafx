
#ifndef _ALGORITHM_COLOUR_SPACE_HLSL
#define _ALGORITHM_COLOUR_SPACE_HLSL

#define SCALE_1 1

#ifdef SCALE_1

#define R_SCALE 0.2126f
#define G_SCALE 0.7152f
#define B_SCALE 0.0722f

#endif

#ifdef SCALE_2

#define R_SCALE 0.299f
#define G_SCALE 0.587f
#define B_SCALE 0.114f

#endif

float rgbToLuminance( float3 colour )
{
	return dot( float3( R_SCALE, G_SCALE, B_SCALE ), colour );
}

float rgbToLuminance2( float3 colour )
{
	return dot( float3( 0.299f, 0.587f, 0.114f ), colour );
}

float rgbToLogLuminance( float3 colour )
{
	return log( rgbToLuminance( colour ) );
}

uint rgbToHistogramBin(
	float3 colour,
	float inputLuminanceMin,
	float inputLuminanceMax,
	uint NBINS )
{
	float luminance = rgbToLogLuminance( colour );

	float normLuminance    = luminance - inputLuminanceMin;
	float normLuminanceMax = inputLuminanceMax - inputLuminanceMin;

	return uint( float( NBINS - 1 ) * saturate( normLuminance / normLuminanceMax ) );
}

float rgbToHistogramBinFloat(
	float3 colour,
	float inputLuminanceMin,
	float inputLuminanceMax,
	uint NBINS )
{
	float luminance = rgbToLogLuminance( colour );

	float normLuminance = luminance - inputLuminanceMin;
	float normLuminanceMax = inputLuminanceMax - inputLuminanceMin;

	return float( NBINS - 1 ) * saturate( normLuminance / normLuminanceMax );
}

float Epsilon = 1e-10;

float3 RGBtoHCV(in float3 RGB)
{
	// Based on work by Sam Hocevar and Emil Persson
	float4 P = (RGB.g < RGB.b) ? float4(RGB.bg, -1.0, 2.0/3.0) : float4(RGB.gb, 0.0, -1.0/3.0);
	float4 Q = (RGB.r < P.x) ? float4(P.xyw, RGB.r) : float4(RGB.r, P.yzx);
	float C = Q.x - min(Q.w, Q.y);
	float H = abs((Q.w - Q.y) / (6 * C + Epsilon) + Q.z);
	return float3(H, C, Q.x);
}

float3 RGBtoHSV(in float3 RGB)
{
	float3 HCV = RGBtoHCV( RGB );
	float S = HCV.y / ( HCV.z + Epsilon );
	return float3(HCV.x, S, HCV.z);
}

float3 HUEtoRGB(in float H)
{
	float R = abs(H * 6 - 3) - 1;
	float G = 2 - abs(H * 6 - 2);
	float B = 2 - abs(H * 6 - 4);
	return saturate(float3(R,G,B));
}

float3 HSVtoRGB(in float3 HSV)
{
	float3 RGB = HUEtoRGB(HSV.x);
	return ((RGB - 1) * HSV.y + 1) * HSV.z;
}

#endif // _ALGORITHM_COLOUR_SPACE_HLSL
