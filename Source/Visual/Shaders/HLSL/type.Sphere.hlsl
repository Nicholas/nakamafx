

#ifndef _SPHERE_SHADER_TYPE_
#define _SPHERE_SHADER_TYPE_

struct Sphere
{
    float3 c;   // Center point.
    float  r;   // Radius.
};

#endif //_SPHERE_SHADER_TYPE_