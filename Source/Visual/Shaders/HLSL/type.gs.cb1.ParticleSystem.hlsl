
#ifndef _CB_PER_PARTICLE_SYSTEM
#define _CB_PER_PARTICLE_SYSTEM


cbuffer cbPerParticleSystem : register( b1 )
{
	float3 emitPosW; float gameTime;
	//--------------------------------------------------------------( 16 bytes )
	float3 emitDirW; float timeStep;
	//--------------------------------------------------------------( 16 bytes )
	float3 emitPosWScale; float emitTime;
	//--------------------------------------------------------------( 16 bytes )
	float3 emitDirWScale; float lifeTime;
	//--------------------------------------------------------------( 16 bytes )
	float2 flareSize;
	float2 ageLifeSizeScale;
	//--------------------------------------------------------------( 16 bytes )
	float4 startColour;
	//--------------------------------------------------------------( 16 bytes )
	float4 endColour;
	//--------------------------------------------------------------( 16 bytes )
	float4 starColourScale;
	//--------------------------------------------------------------( 16 bytes )
	float4 endColourScale;
	//--------------------------------------------------------------( 16 bytes )
	float4 attractor;
	//--------------------------------------------------------------( 16 bytes )
	//--------------------------------------------------------------( 16 * 10 = 160 bytes )
};

#endif
