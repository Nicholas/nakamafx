
#ifndef _ALGORITHMS_HLSL
#define _ALGORITHMS_HLSL

#define PI 3.1415926535897932384626433832795
#define TAU 6.28318530718

float2 toPolar( float3 direction )
{
	float2 tex;

	tex.x = atan2( direction.y, direction.x );
	tex.y = acos( direction.z );

	return tex;
}

float2 toUV( float2 polar )
{
	float2 tex;

	tex.x = saturate( ( polar.x + PI ) / TAU );
	tex.y = -polar.y / PI;

	return tex;
}


#endif// _ALGORITHMS_HLSL