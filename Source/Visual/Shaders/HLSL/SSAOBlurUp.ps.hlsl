/////////////
// GLOBALS //
/////////////
cbuffer ssaoBlurCByffer : register( b0 )
{
	// Camera projections for depth testing
	float4x4 view;
	float4x4 projection;

    // Render target texture size
    float4 targetSize;
};

Texture2D gAOTexture : register( t0 );
Texture2D gPositionTexture : register( t1 );
Texture2D gNormalTexture : register( t1 );

SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState wrapSampler : register( s2 );
SamplerState pointSampler : register( s3 );


//////////////
// TYPEDEFS //
//////////////
struct Fragment
{
    float4 position : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};

struct Pixel
{
    float4 colour0 : SV_TARGET0;
};


static const int blurSize = 4;

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment)
{
    Pixel pixel;

	float ao = 0;

	float depth = gPositionTexture.Sample( pointSampler, fragment.textureUV ).z;

	float div = 0.0f;

	for( int x = -2; x < 2; ++x )
	{
		for( int y = -2; y < 2; ++y )
		{
			float2 offset = float2( x, y ) * targetSize.zw * 0.5f;
			float2 uv = fragment.textureUV + offset;

			float sampleDepth = gPositionTexture.Sample( pointSampler, uv ).z;
			float scale = 1 -  smoothstep( 0, 0.15, abs( depth - sampleDepth ) );

			ao += gAOTexture.Sample( samplerState, uv ).r * scale;
			div += scale;
		}
	}
	ao /= max( div, 0.0000001 );

	float3 aoColour = float3( 0, 0, 0 );

	pixel.colour0.rgb = aoColour;
	pixel.colour0.a = ( 1 - ao ) * 0.95;
//	pixel.colour0.rgb = ao;
//	pixel.colour0.a = 1.0f;

	return pixel;
}
