#include "type.iCS.Default.hlsl"
#include "type.Frustum.hlsl"
#include "type.Light.hlsl"
#include "algorithms.ClusteredLighting.hlsl"
#include "algorithms.IntersectionTesting.hlsl"

cbuffer DispatchParams : register( b0 )
{
	// The number of thread groups that have been dispatched
	uint3 numThreadGroups;
	uint p1;

	// The total number of threads dispatched thats expected to perform real work.
	uint3 numThreads;
	uint p2;

	Matrix projectionInv;
	float4 ScreenDimensions;

	float nearZPlane;
	float  farZPlane;
}

StructuredBuffer< Frustum > frustumsSRV : register( t0 );
StructuredBuffer< Light >     lightsSRV : register( t1 );

// Counter for the current index into the light index list
RWStructuredBuffer< uint > lightIndexCounterUAV : register( u0 );
RWStructuredBuffer< uint >    lightIndexListUAV : register( u1 );
RWStructuredBuffer< LightCluster > lightGridUAV : register( u2 );

//groupshared uint uMinDepth;
//groupshared uint uMaxDepth;

groupshared Frustum groupFrustum;

// Light lists.
groupshared uint lightCount;
groupshared uint lightIndexStartOffset;
groupshared uint lightList[ NUMBER_OF_LIGHTS ];

void appendLight( uint lightIndex )
{
	uint index; // Index into the visible lights array.
	InterlockedAdd( lightCount, 1, index );
	if ( index < 1024 )
	{
		lightList[ index ] = lightIndex;
	}
}

// Implementation of light culling compute shader is based on the presentation
// "DirectX 11 Rendering in Battlefield 3" (2011) by Johan Andersson, DICE.
// Retrieved from: http://www.slideshare.net/DICEStudio/directx-11-rendering-in-battlefield-3
// Retrieved: July 13, 2015
// And "Forward+: A Step Toward Film-Style Shading in Real Time", Takahiro Harada (2012)
// published in "GPU Pro 4", Chapter 5 (2013) Taylor & Francis Group, LLC.
[ numthreads( BLOCK_SIZE, BLOCK_SIZE, 1 ) ]
void main( ComputeShaderInput input )
{
	int groupCoord = input.groupID.x + ( input.groupID.y * numThreadGroups.x ) + ( input.groupID.z * numThreadGroups.x * numThreadGroups.y );

	//float fDepth = DepthTextureVS.Load( int3( texCoord, 0 ) ).r;
	//uint uDepth = asuint( fDepth );
	if ( input.groupIndex == 0 ) // Avoid contention by other threads in the group.
	{
		//uMinDepth = 0xffffffff;
		//uMaxDepth = 0;
		lightCount = 0;
		groupFrustum = frustumsSRV[ groupCoord ];
	}

	GroupMemoryBarrierWithGroupSync( );

	//InterlockedMin( uMinDepth, uDepth );
	//InterlockedMax( uMaxDepth, uDepth );

	//GroupMemoryBarrierWithGroupSync();

	//float fMinDepth = asfloat( uMinDepth );
	//float fMaxDepth = asfloat( uMaxDepth );

	//// Convert depth values to view space.
	//float minDepthVS = ClipToView( float4( 0, 0, fMinDepth, 1 ) ).z;
	//float maxDepthVS = ClipToView( float4( 0, 0, fMaxDepth, 1 ) ).z;
	//float nearClipVS = ClipToView( float4( 0, 0, 0, 1 ) ).z;

	//// Clipping plane for minimum depth value
	//// (used for testing lights within the bounds of opaque geometry).
	//Plane minPlane = { float3( 0, 0, -1 ), -minDepthVS };

	// Cull lights
	// Each thread in a group will cull 1 light until all lights have been culled.
	[unroll]
	for ( uint i = input.groupIndex; i < NUMBER_OF_LIGHTS; i += BLOCK_SIZE * BLOCK_SIZE )
	{
		if ( lightsSRV[ i ].enabled )
		{
			Light light = lightsSRV[ i ];

			switch ( light.type )
			{
				case POINT_LIGHT:
				{
					Sphere sphere = { light.positionVS.xyz, light.range };

					if ( SphereInsideFrustum( sphere, groupFrustum ) ) // , nearClipVS, maxDepthVS
					{
						// Add light to light list for transparent geometry.
						appendLight( i );
					}
					break;
				}
				case SPOT_LIGHT:
				{
					float coneRadius = tan( radians( light.spotlightAngle ) ) * light.range;
					Cone cone = { light.positionVS.xyz, light.range, light.directionVS.xyz, coneRadius };
					if ( ConeInsideFrustum( cone, groupFrustum ) )
					{
						// Add light to light list for transparent geometry.
						appendLight( i );
					}
					break;
				}
				case DIRECTIONAL_LIGHT:
				{
					// Directional lights always get added to our light list.
					// (Hopefully there are not too many directional lights!)
					appendLight( i );
					break;
				}
			}
		}
	}

	// Wait till all threads in group have caught up.
	GroupMemoryBarrierWithGroupSync();

	// Update global memory with visible light buffer.
	// First update the light grid (only thread 0 in group needs to do this)
	if ( input.groupIndex == 0 )
	{
		// Update light grid.
		InterlockedAdd( lightIndexCounterUAV[ 0 ], lightCount, lightIndexStartOffset );

		LightCluster cluster = ( LightCluster )0;
		cluster.offset = lightIndexStartOffset;
		cluster.count = lightCount;

		lightGridUAV[ groupCoord ] = cluster;
	}

	GroupMemoryBarrierWithGroupSync();

	// Now update the light index list (all threads).
	for ( i = input.groupIndex; i < lightCount; i += BLOCK_SIZE * BLOCK_SIZE )
	{
		lightIndexListUAV[ lightIndexStartOffset + i ] = lightList[ i ];
	}
}



//  function CullLights( L, C, G, I )
//      Input: A set L of n lights.
//      Input: A counter C of the current index into the global light index list.
//      Input: A 2D grid G of index offset and count in the global light index list.
//      Input: A list I of global light index list.
//      Output: A 2D grid G with the current tiles offset and light count.
//      Output: A list I with the current tiles overlapping light indices appended to it.
//
//  1.  let t be the index of the current tile  ; t is the 2D index of the tile.
//  2.  let i be a local light index list       ; i is a local light index list.
//  3.  let f <- Frustum(t)                     ; f is the frustum for the current tile.
//
//  4.  for l in L                      ; Iterate the lights in the light list.
//  5.      if Cull( l, f )             ; Cull the light against the tile frustum.
//  6.          AppendLight( l, i )     ; Append the light to the local light index list.
//
//  7.  c <- AtomicInc( C, i.count )    ; Atomically increment the current index of the
//                                      ; global light index list by the number of lights
//                                      ; overlapping the current tile and store the
//                                      ; original index in c.
//
//  8.  G(t) <- ( c, i.count )          ; Store the offset and light count in the light grid.
//
//  9.  I(c) <- i                       ; Store the local light index list into the global
//                                      ; light index list.

