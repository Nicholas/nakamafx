
#include "type.gs.cb0.PerScene.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Geometry Shader
////////////////////////////////////////////////////////////////////////////////
[ maxvertexcount( 24 ) ]
void main( point VertexPTNTB gIn[ 1 ], inout TriangleStream<Fragment> triStream )
{
	float halfWidth = 0.5f * gIn[ 0 ].textureUV.x;
	float halfDepth = 0.5f * gIn[ 0 ].textureUV.x;
	float height    =        gIn[ 0 ].textureUV.y;

	float4 v[ 8 ];
	v[ 0 ] = float4( -halfWidth - 2, height, -halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );
	v[ 1 ] = float4( +halfWidth - 2, height, -halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );
	v[ 2 ] = float4( -halfWidth - 2, height, +halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );
	v[ 3 ] = float4( +halfWidth - 2, height, +halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );
	v[ 4 ] = float4( -halfWidth - 2,      0, -halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );
	v[ 5 ] = float4( +halfWidth - 2,      0, -halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );
	v[ 6 ] = float4( -halfWidth - 2,      0, +halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );
	v[ 7 ] = float4( +halfWidth - 2,      0, +halfDepth - 5, 1.0f ) + float4( gIn[ 0 ].position, 0 );

	float4 faces[ 6 ][ 4 ];

	// +Y TOP
	faces[ 0 ][ 0 ] = v[ 0 ];
	faces[ 0 ][ 1 ] = v[ 2 ];
	faces[ 0 ][ 2 ] = v[ 1 ];
	faces[ 0 ][ 3 ] = v[ 3 ];

	// -X
	faces[ 1 ][ 0 ] = v[ 2 ];
	faces[ 1 ][ 1 ] = v[ 0 ];
	faces[ 1 ][ 2 ] = v[ 6 ];
	faces[ 1 ][ 3 ] = v[ 4 ];

	// +X
	faces[ 2 ][ 0 ] = v[ 1 ];
	faces[ 2 ][ 1 ] = v[ 3 ];
	faces[ 2 ][ 2 ] = v[ 5 ];
	faces[ 2 ][ 3 ] = v[ 7 ];

	// -Z
	faces[ 3 ][ 0 ] = v[ 0 ];
	faces[ 3 ][ 1 ] = v[ 1 ];
	faces[ 3 ][ 2 ] = v[ 4 ];
	faces[ 3 ][ 3 ] = v[ 5 ];

	// +Z
	faces[ 4 ][ 0 ] = v[ 3 ];
	faces[ 4 ][ 1 ] = v[ 2 ];
	faces[ 4 ][ 2 ] = v[ 7 ];
	faces[ 4 ][ 3 ] = v[ 6 ];

	// -Y Bottom
	faces[ 5 ][ 0 ] = v[ 4 ];
	faces[ 5 ][ 1 ] = v[ 5 ];
	faces[ 5 ][ 2 ] = v[ 6 ];
	faces[ 5 ][ 3 ] = v[ 7 ];


	float2 gQuadTexC[ 4 ] =
	{
		float2( 0.0f, 1.0f ),
		float2( 1.0f, 1.0f ),
		float2( 0.0f, 0.0f ),
		float2( 1.0f, 0.0f )
	};

	float3 X = float3( 1.0f, 0.0f, 0.0f );
	float3 Y = float3( 0.0f, 1.0f, 0.0f );
	float3 Z = float3( 0.0f, 0.0f, 1.0f );

	float3 normals[ 6 ][ 3 ];
	// +Y TOP
	normals[ 0 ][ 0 ] =  Y;
	normals[ 0 ][ 1 ] =  Z;
	normals[ 0 ][ 2 ] = -X;

	// -X
	normals[ 1 ][ 0 ] = -X;
	normals[ 1 ][ 1 ] = -Z;
	normals[ 1 ][ 2 ] =  Y;

	// +X
	normals[ 2 ][ 0 ] =  X;
	normals[ 2 ][ 1 ] =  Z;
	normals[ 2 ][ 2 ] =  Y;

	// -Z
	normals[ 3 ][ 0 ] = -Z;
	normals[ 3 ][ 1 ] =  X;
	normals[ 3 ][ 2 ] =  Y;

	// +Z
	normals[ 4 ][ 0 ] =  Z;
	normals[ 4 ][ 1 ] = -X;
	normals[ 4 ][ 2 ] =  Y;

	// -Y
	normals[ 5 ][ 0 ] = -Y;
	normals[ 5 ][ 1 ] =  Z;
	normals[ 5 ][ 2 ] =  X;

	//
	// Transform quad vertices to world space and output
	// them as a triangle strip.
	//
	Fragment gOut;

	[unroll]
	for( int j = 0; j < 6; ++j )
	{
		[unroll]
		for( int i = 0; i < 4; ++i )
		{
			float4 worldPosition = faces[ j ][ i ];

			gOut.position = mul( worldPosition, viewProjectionMatrix );

			gOut.normal   = normals[ j ][ 0 ];
			gOut.tangent  = normals[ j ][ 1 ];
			gOut.binormal = normals[ j ][ 2 ];

			gOut.textureUV = gQuadTexC[ i ];

			gOut.view      = worldPosition.xyz - cameraPosition.xyz;
			gOut.worldPos  = worldPosition.xyz;
			gOut.shadow1Pos = mul( worldPosition, shadow1ProjectionMatrix );
			gOut.shadow2Pos = mul( worldPosition, shadow2ProjectionMatrix );

			gOut.lDirTS = float3( 0, 1, 1 );
			gOut.parallaxOffsetTS = float2( 0, 0 );

			gOut.viewPos = mul( worldPosition, view ).xyz;

			triStream.Append( gOut );
		}

		triStream.RestartStrip( );
	}
}

/******************************************************************************/
