
#include "type.iPS.ParticleFragment.hlsl"

//////////////
// GLOBALS //
////////////
cbuffer cbPerLight : register( b0 )
{
	// -------------------------------------------
	// Lighs
	// -------------------------------------------
	// Ambiant
	float4 aLightC;

	// Direcion
	float4 dLightC;
	float3 dLightD;
	float p4;

	// Point
	float4 pLightC;
	float3 pLightP;
	float p5;
	float pLightR;
};

cbuffer cbPerMaterial : register( b1 )
{
	// -------------------------------------------
	// Material Values
	// -------------------------------------------
	float3 Ka; float bumpScaleX;
	float3 Kd; float bumpScaleY;
	float3 Ks; float Ns;

	float2 imageCount;
	float  frameRate;
	float  frameCount;
};

Texture2D textures[ 10 ];
SamplerState pointSampler : register( s0 );
SamplerState anisoSampler : register( s1 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment pIn ): SV_TARGET0
{
	float4 colour;

	float2 texCoord = pIn.texC / imageCount;

	float frame = ( pIn.age * frameRate ) % frameCount;

	int x = frame % imageCount.x;
	int y = frame / imageCount.x;

	float2 offset = float2( x, y ) / imageCount;

	colour = textures[ 0 ].Sample( anisoSampler, texCoord + offset );
	colour.rgb *= pIn.colour.rgb * colour.a;

	clip( colour.a  - 0.001f );

//	colour.a = pIn.color.a;
//	colour += 1;

	return colour;
}
