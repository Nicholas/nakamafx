
#ifndef _USER_INTERFACE_FRAGMENT
#define _USER_INTERFACE_FRAGMENT

struct Fragment
{
	float4 position  : SV_POSITION;
	float4 colour	 : COLOR;
	float2 textureUV : TEXCOORD;
};

#endif
