
#ifndef _VERTEX_TYPES
#define _VERTEX_TYPES

struct VertexPT
{
	float3 position  : POSITION;
	float2 textureUV : TEXCOORD;
};

struct VertexPTC
{
	float3 position  : POSITION;
	float2 textureUV : TEXCOORD;
	float4 colour    : COLOR;
};

struct VertexPTNTB
{
	float3 position  : POSITION;
	float2 textureUV : TEXCOORD;
	float3 normal    : NORMAL;
	float3 tangent   : TANGENT;
	float3 binormal  : BINORMAL;
};

struct VertexChar
{
	float4 position  : POSITION;
	float4 textureUV : TEXCOORD0;
	float4 colour    : COLOR0;
	uint id          : TEXCOORD1;
};

#endif
