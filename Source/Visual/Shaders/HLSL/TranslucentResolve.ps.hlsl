
#include "algorithms.RGBM.hlsl"
#include "algorithms.Fog.hlsl"

//////////////
// DEFINES //
////////////
//#define MAX_FRAGMENTS 8
#define MAX_FRAGMENTS 16
#define MAX_RESOLVE_FRAGMENTS 16

//#define first_element_layers
#define translucent_layers
//#define transparent_layers

#define LOG_LUV_ENCODE
#define RGBME_SCALE 8

///////////////
// TYPEDEFS //
/////////////
struct PixelInputType
{
	float4 position : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};

struct SFragment
{
	float2 refraction;
	uint surfaceReflection;
	uint subSurfaceReflection;
	uint subSurfaceRefraction;
	uint refractionIndex;
	float  depth;
};

struct SFragmentLink
{
	SFragment fragment;
	uint uNext;
};

//////////////
// GLOBALS //
////////////
cbuffer perFrame : register( b0 )
{
	float width;
	float height;
	float density;
	float heightFalloff;

	float4x4 invView;
};

// Fragment And Link Buffer
StructuredBuffer< SFragmentLink > FragmentLinkSRV : register(t0);
// Start Offset Buffer
Buffer< uint > StartOffsetSRV : register(t1);

// Textures
Texture2D backBuffer : register( t2 );
Texture2D ViewSpace  : register( t3 );
SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState mirrorSampler : register( s2 );
SamplerState pointSampler : register( s3 );



/// This returns the fragments world space position.
/// If the fragment is in the sky then we create a ray thats 64.0f long.
float4 getWorldPosition( in float2 position )
{
	float4 view  = ViewSpace.Load( int3( position, 0 ) );
	float4 world = mul( float4( view.xyz, 1 ), invView );

	return float4( world.xyz, view.z );
}


float calculateAbsorbsion( float d1, float d2, float scale )
{
	return calculateFog( d2 - d1, scale * 10 );
}

float3 addVolumeLayer( in float3 base, in SFragment frontFace, in SFragment backFace )
{
	float4 ffRefraction = getFloats(frontFace.refractionIndex);

#ifdef LOG_LUV_ENCODE
	float3 ffSReflection  = LogLuvDecode( getFloats( frontFace.surfaceReflection ) );
	float3 ffSSReflection = LogLuvDecode( getFloats( frontFace.subSurfaceReflection ) );
	float3 ffSSRefraction = LogLuvDecode( getFloats( frontFace.subSurfaceRefraction ) );

	float3 bfSReflection  = LogLuvDecode( getFloats( backFace.surfaceReflection ) );
	float3 bfSSReflection = LogLuvDecode( getFloats( backFace.subSurfaceReflection ) );
	float3 bfSSRefraction = LogLuvDecode( getFloats( backFace.subSurfaceRefraction ) );
#else
	float3 ffSReflection = RGBMDencodeScaled(getFloats(frontFace.surfaceReflection), RGBME_SCALE);
	float3 ffSSReflection = RGBMDencode( getFloats( frontFace.subSurfaceReflection ) );
	float3 ffSSRefraction = RGBMDencode( getFloats( frontFace.subSurfaceRefraction ) );

	float3 bfSReflection  = RGBMDencodeScaled( getFloats( backFace.surfaceReflection ), RGBME_SCALE );
	float3 bfSSReflection = RGBMDencode( getFloats( backFace.subSurfaceReflection ) );
	float3 bfSSRefraction = RGBMDencode(getFloats(frontFace.subSurfaceRefraction));
#endif

	float absorbsion = calculateAbsorbsion( frontFace.depth, backFace.depth, ffRefraction.a );

	float3 ssReflection = ffSSReflection;
	float3 ssRefraction = ((bfSSRefraction + bfSSReflection) * 0.5f * absorbsion + ffSSRefraction) * 0.5f;

	float3 colour = 0;
	colour.rgb  = base * ffRefraction.rgb;
	colour.rgb += ssRefraction;
	colour.rgb *= ( 1 - absorbsion );
	colour.rgb += ssReflection * absorbsion;
	colour.rgb += ffSReflection;

	return colour;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
//////////////////////////////////////////////////////////////////////////////
float4 main( PixelInputType input ) : SV_Target0
{
	uint x = input.position.x;
	uint y = input.position.y;

	uint uIndex = y * width + x;

	// Read and store linked list data to the tempolary buffer.
	SFragment aData[ MAX_FRAGMENTS ];
	uint anIndex[ MAX_FRAGMENTS ];

	uint uNumFragment = 0;
	uint uNext = StartOffsetSRV.Load( uIndex );

	while ( uNext != 0xffffffff && uNumFragment < MAX_FRAGMENTS )
	{
		SFragmentLink element = FragmentLinkSRV[ uNext ];

		aData[ uNumFragment ] = element.fragment;
		anIndex[ uNumFragment ] = uNumFragment;
		++uNumFragment;
		uNext = element.uNext;
	}

	clip( uNumFragment - 1 );

	// Get the closest N squared value for optimized sorting time
	// ( We dont want to sort all the elements if we dont need to )
	uint N2 = 1 << ( int )( ceil( log2( uNumFragment ) ) );

	// make sure the array has enough elements to sort and invalid elements are out of bounds
	for( uint i = uNumFragment; i < N2; i++ )
	{
		anIndex[ i ] = i;
		aData[ i ] = ( SFragment ) 0;
		aData[ i ].depth = 10000000000000.0f;
	}

	// Bitonic Sort.
	for( uint k = 2; k <= N2; k = 2 * k )
	{
		for( int j = k >> 1; j > 0; j = j >> 1 ) // Bit Shift
		{
			for( uint i = 0; i < N2; ++i )
			{
				uint iXj = i ^ j; // BITWISE Xor

				if( iXj > i )
				{
					float iDepth   = aData[ anIndex[  i  ] ].depth;
					float iXjDepth = aData[ anIndex[ iXj ] ].depth;

					if( ( ( i & k ) == 0 ) && ( iDepth > iXjDepth ) ) // BITWISE And
					{
						int tempIndex = anIndex[  i  ];
						anIndex[  i  ] = anIndex[ iXj ];
						anIndex[ iXj ] = tempIndex;
					}
					else if( ( ( i & k ) != 0 ) && ( iDepth < iXjDepth ) ) // BITWISE And
					{
						int tempIndex = anIndex[  i  ];
						anIndex[  i  ] = anIndex[ iXj ];
						anIndex[ iXj ] = tempIndex;
					}
				}
			}
		}
	}

	// Output the final result to the frame buffer
	clip( uNumFragment - 1 );

	float4 result;

	result.rgb = backBuffer.Sample( pointSampler, input.textureUV.xy + aData[ anIndex[ 0 ] ].refraction ).rgb;
	result.a = 1;

	uint fragmentCount = min( uNumFragment, MAX_RESOLVE_FRAGMENTS );

	if( fragmentCount % 2 == 0 && fragmentCount > 0 )
	{
		for( int v = fragmentCount - 1; v > 0; v -= 2 )
		{
			result.rgb = addVolumeLayer( result.rgb, aData[ anIndex[ v - 1 ] ], aData[ anIndex[ v ] ] );
		}
	}
	else
	{
		clip( -1 );
	}

	return result;
}