
#include "type.iPS.TexturedFragment.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer perFrame : register( b0 )
{
	matrix viewProjectionMatrix;
};

cbuffer perObject : register( b1 )
{
	float2 textureSize;
	float3 padding;
};

Texture2D    colourSource : register( t0 );
Texture2D      viewSource : register( t1 );
SamplerState samplerState : register( s0 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ) : SV_TARGET0
{
	float4 pixel;

	pixel   = colourSource.Sample( samplerState, fragment.textureUV );
	pixel.a =   viewSource.Sample( samplerState, fragment.textureUV ).z;

	return pixel;
}