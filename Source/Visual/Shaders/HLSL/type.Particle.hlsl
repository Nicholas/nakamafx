
#ifndef _PARTICLE_TYPES
#define _PARTICLE_TYPES

#define PT_EMITTER 0
#define PT_FLARE   1

struct Particle
{
	float3 initialPosW : POSITION;
	float3 initialVelW : TEXCOORD0;
	float2 sizeW       : TEXCOORD1;
	float  age         : TEXCOORD2;
	float  id          : TEXCOORD3;
	uint   type        : TEXCOORD4;
};

#endif
