
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.TexturedFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader - Pass Through
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexPT vertex )
{
	Fragment fragment;

	fragment.position = float4( vertex.position, 1.0f );
	fragment.textureUV = vertex.textureUV;

	return fragment;
}
