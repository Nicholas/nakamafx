
#ifndef _ALGORITHM_BICUBIC_FILTERS_HLSL
#define _ALGORITHM_BICUBIC_FILTERS_HLSL

/// Bicubic interpolation sampling of a texture.
float4 bicubicInterpolationFast(
	in Texture2D image,
	in SamplerState imageSampler,
	in float2 uv,
	in uint mipLevel
)
{
	float3 texSize = float3( 0, 0, mipLevel );

	image.GetDimensions( texSize.x, texSize.y );

	float2 rec_nrCP = 1.0 / texSize.xy;
	float2 coord_hg = uv * texSize.xy - 0.5; // TODO: Chech to see if this is correct in dx11
	float2 index = floor( coord_hg );

	float2 f = coord_hg - index;
	float4x4 M = {
		-1,  3, -3,  1,
		 3, -6,  3,  0,
		-3,  0,  3,  0,
		 1,  4,  1,  0
	};
	M /= 6;

	float4 wx = mul( float4( f.x*f.x*f.x, f.x*f.x, f.x, 1 ), M );
	float4 wy = mul( float4( f.y*f.y*f.y, f.y*f.y, f.y, 1 ), M );
	float2 w0 = float2( wx.x, wy.x );
	float2 w1 = float2( wx.y, wy.y );
	float2 w2 = float2( wx.z, wy.z );
	float2 w3 = float2( wx.w, wy.w );

	float2 g0 = w0 + w1;
	float2 g1 = w2 + w3;
	float2 h0 = w1 / g0 - 1;
	float2 h1 = w3 / g1 + 1;

	float2 coord00 = index + h0;
	float2 coord10 = index + float2( h1.x, h0.y );
	float2 coord01 = index + float2( h0.x, h1.y );
	float2 coord11 = index + h1;

	coord00 = ( coord00 + 0.5 ) * rec_nrCP;
	coord10 = ( coord10 + 0.5 ) * rec_nrCP;
	coord01 = ( coord01 + 0.5 ) * rec_nrCP;
	coord11 = ( coord11 + 0.5 ) * rec_nrCP;

	float4 tex00 = image.SampleLevel( imageSampler, coord00, texSize.z );
	float4 tex10 = image.SampleLevel( imageSampler, coord10, texSize.z );
	float4 tex01 = image.SampleLevel( imageSampler, coord01, texSize.z );
	float4 tex11 = image.SampleLevel( imageSampler, coord11, texSize.z );

	tex00 = lerp( tex01, tex00, g0.y );
	tex10 = lerp( tex11, tex10, g0.y );

	return lerp( tex10, tex00, g0.x );
}



float4 cubicSample(
	in Texture2D image,
	in SamplerState imageSampler,
	float2 textureUV )
{
	float2 size = float2( 0, 0 );

	image.GetDimensions( size.x, size.y );

	// Pixel center and no filtering.
	float2 invSize = 1.0f / size;
	float2 uv = textureUV * size;

	float2 texelCenter = floor( uv - 0.5f )/* + 0.5f */;
	float2 fracOffset = uv - texelCenter;
	float2 fracOffset_x2 = fracOffset * fracOffset;
	float2 fracOffset_x3 = fracOffset * fracOffset_x2;


	// B-Spline weighting function
	float2 weight0 = fracOffset_x2 - 0.5f * ( fracOffset_x3 + fracOffset );
	float2 weight1 = 1.5f * fracOffset_x3 - 2.5f * fracOffset_x2 + 1.0f;
	float2 weight3 = 0.5f * ( fracOffset_x3 - fracOffset_x2 );

	float2 weight2 = 1.0f - weight0 - weight1 - weight3;


	// Texture Coords
	float2 scalingFactor0 = weight0 + weight2;
	float2 scalingFactor1 = weight2 + weight3;

	float2 f0 = weight1 / ( weight0 + weight1 );
	float2 f1 = weight3 / ( weight2 + weight3 );

	float2 texCoord0 = texelCenter - 0.75f + f0;
	float2 texCoord1 = texelCenter + 0.75f + f1;

	texCoord0 *= invSize;
	texCoord1 *= invSize;

	float4 output;
	output  = image.Sample( imageSampler, float2( texCoord0.x, texCoord0.y ) ) * scalingFactor0.x * scalingFactor0.y;
	output += image.Sample( imageSampler, float2( texCoord1.x, texCoord0.y ) ) * scalingFactor1.x * scalingFactor0.y;
	output += image.Sample( imageSampler, float2( texCoord0.x, texCoord1.y ) ) * scalingFactor0.x * scalingFactor1.y;
	output += image.Sample( imageSampler, float2( texCoord1.x, texCoord1.y ) ) * scalingFactor1.x * scalingFactor1.y;

	return output;
}

#endif // _ALGORITHM_BICUBIC_FILTERS_HLSL
