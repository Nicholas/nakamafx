
#include "type.gs.cb0.PerScene.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"


////////////////////////////////////////////////////////////////////////////////
// Geometry Shader
////////////////////////////////////////////////////////////////////////////////
[ maxvertexcount( 4 ) ]
void main( point VertexPTNTB gIn[ 1 ], inout TriangleStream<Fragment> triStream )
{
	//
	// Compute world matrix so that billboard faces the camera.
	//
	float3 look  = normalize( cameraPosition.xyz - gIn[ 0 ].position );
	float3 right = normalize( cross( float3( 0, 1, 0 ), look ) );
	float3 up = cross( look, right );
	float4x4 W;
	W[ 0 ] = float4( right, 0.0f );
	W[ 1 ] = float4( up, 0.0f );
	W[ 2 ] = float4( look, 0.0f );
	W[ 3 ] = float4( gIn[ 0 ].position, 1.0f );

	//
	// Compute 4 triangle strip vertices (quad) in local space.
	// The quad faces down the +z-axis in local space.
	//
	float halfWidth  = 0.5f * gIn[ 0 ].textureUV.x;
	float halfHeight = 0.5f * gIn[ 0 ].textureUV.y;
	float4 v[ 4 ];
	v[ 0 ] = float4( -halfWidth, -halfHeight, 0.0f, 1.0f );
	v[ 1 ] = float4( +halfWidth, -halfHeight, 0.0f, 1.0f );
	v[ 2 ] = float4( -halfWidth, +halfHeight, 0.0f, 1.0f );
	v[ 3 ] = float4( +halfWidth, +halfHeight, 0.0f, 1.0f );


	float2 gQuadTexC[ 4 ] =
	{
		float2( 0.0f, 1.0f ),
		float2( 1.0f, 1.0f ),
		float2( 0.0f, 0.0f ),
		float2( 1.0f, 0.0f )
	};
	//
	// Transform quad vertices to world space and output
	// them as a triangle strip.
	//
	Fragment gOut;

	[unroll]
	for( int i = 0; i < 4; ++i )
	{
		float4 worldPosition = mul( v[ i ], W );

		gOut.position = mul( worldPosition, viewProjectionMatrix );

		gOut.normal   = mul( float4( 0, 0, 1, 0 ), W ).xyz;
		gOut.tangent  = mul( float4( 1, 0, 0, 0 ), W ).xyz;
		gOut.binormal = mul( float4( 0, 1, 0, 0 ), W ).xyz;

		gOut.textureUV = gQuadTexC[ i ];

		gOut.view      = worldPosition.xyz - cameraPosition.xyz;
		gOut.worldPos  = worldPosition.xyz;
		gOut.shadow1Pos = float4( 1, 1, 1, 1 );
		gOut.shadow2Pos = float4( 1, 1, 1, 1 );
		gOut.viewPos    = mul( worldPosition, view ).xyz;

		gOut.lDirTS           = float3( 0, 1, 1 );
		gOut.parallaxOffsetTS = float2( 0, 0 );

		triStream.Append( gOut );
	}
}

/******************************************************************************/
