
#ifndef _TEXTURED_FRAGMENT
#define _TEXTURED_FRAGMENT

struct Fragment
{
	float4 position  : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};

#endif
