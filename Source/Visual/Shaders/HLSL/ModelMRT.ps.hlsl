
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.ParallaxMapping.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"

//////////////
// GLOBALS //
////////////
Texture2D textures[ 13 ] : register( t0 );
SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState mirrorState : register( s2 );
SamplerState pointState : register( s3 );

#define    COLOUR_TEX 0
#define     NORMAL_TX 1
#define     DEPTH_TEX 2
#define        AO_TEX 3
#define      BUMP_TEX 4
#define     GLOSS_TEX 5
#define  HI_GLOSS_TEX 6
#define ROUGHNESS_TEX 7
#define METALNESS_TEX 8


#define SHADOW_1_MAP 9
#define SHADOW_2_MAP 10
#define SHADOW_TD_MAP 11
#define SHADOW_TC_MAP 12

// Forward Clustered Lights
StructuredBuffer<LightCluster> lightGridSRV : register( t13 );
StructuredBuffer< uint >  lightIndexListSRV : register( t14 );
StructuredBuffer<Light>           lightsSRV : register( t15 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
//[ earlydepthstencil ]
Pixel main( Fragment fragment )
{
	fragment.normal   = normalize( fragment.normal );
	fragment.tangent  = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view     = normalize( fragment.view );

	float3 sNorm = fragment.normal;

	float4 colourSample  = textures[ COLOUR_TEX    ].Sample( samplerState, fragment.textureUV );
	float3 normalSample  = textures[ NORMAL_TX     ].Sample( samplerState, fragment.textureUV ).rgb;
	float  aoSample      = textures[ AO_TEX        ].Sample( samplerState, fragment.textureUV ).r;
	float  roughSample   = textures[ ROUGHNESS_TEX ].Sample( samplerState, fragment.textureUV ).r * roughness;
	float  metalSample   = textures[ METALNESS_TEX ].Sample( samplerState, fragment.textureUV ).r * metalness;

	fragment.normal = calculateBumpNormalZ(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		normalSample,
		float3( bumpScaleX, bumpScaleY, 1.0 )
	);




	float LdotSN = dot( dLightD, sNorm );
	float bias = tan( acos( LdotSN ) );

	float2 shadow1 = getShadowCascade(
		textures[ SHADOW_1_MAP ], cmpSampler,
		fragment.shadow1Pos, shadowSize1,
		bias * shadowSize1.x
	);

	float shadow2 = getShadowWithBias(
		textures[ SHADOW_2_MAP ], cmpSampler,
		fragment.shadow2Pos, shadowSize2,
		bias * shadowSize2.x * 4
	);

	// Turn light off a sun set...
	float sunSet = smoothstep( 0.05, 0.3, dot( dLightD, float3( 0, 1, 0 ) ) );
	float shadow = lerp( shadow2, shadow1.x, shadow1.y ) * saturate( LdotSN );


	float4 shadowColour = getShadowColour(
		textures[ SHADOW_TD_MAP ], cmpSampler,
		textures[ SHADOW_TC_MAP ], pointState,
		fragment.shadow1Pos,
		bias * shadowSize1.x );

	float3 lightColour = lerp( dLightC.rgb, shadowColour.rgb, shadowColour.a ) * shadow * sunSet;

	int clusterIndex = getClusterIndex( fragment.position.xy, fragment.viewPos.z, float2( targetWidth, targetHeight ), zDepths.x, zDepths.y );

	LightingInfo surface = clusterLighting(
		lightGridSRV[ clusterIndex ],
		 lightIndexListSRV, lightsSRV,
		lightColour.rgb, dLightD,
		fragment.worldPos,
		fragment.normal,
		-fragment.view,
		colourSample.rgb * kd,
		ks,
		metalSample,
		roughSample,
		aoSample
	);

	clip( 0.5f - ( 1 - colourSample.a ) );

	Pixel pixel;

	pixel.colour = float4( surface.colour, 1 );
	pixel.normal = float4( fragment.normal, 1 );
	pixel.viewSP = float4( fragment.viewPos, fragment.position.z * fragment.position.w );
	pixel.F0     = saturate( float4( surface.reflection, roughSample ) );

	return pixel;
}
