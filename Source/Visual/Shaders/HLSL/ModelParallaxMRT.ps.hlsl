
//#include "type.Light.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.ParallaxMapping.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"

//////////////
// GLOBALS //
////////////
//cbuffer cbConstants
//{
//	const int g_nMinSamples = 20;
//	const int g_nMaxSamples = 500;
//}
#define g_nMinSamples 1
#define g_nMaxSamples 20

Texture2D textures[ 13 ];
SamplerState samplerState;
SamplerComparisonState cmpSampler;

#define    COLOUR_TEX 0
#define     NORMAL_TX 1
#define     DEPTH_TEX 2
#define        AO_TEX 3
#define      BUMP_TEX 4
#define     GLOSS_TEX 5
#define  HI_GLOSS_TEX 6
#define ROUGHNESS_TEX 7
#define METALNESS_TEX 8

#define SHADOW_1_MAP 9
#define SHADOW_2_MAP 10
#define SHADOW_TD_MAP 11
#define SHADOW_TC_MAP 12

StructuredBuffer<LightCluster> lightGridSRV : register( t13 );
StructuredBuffer< uint >  lightIndexListSRV : register( t14 );
StructuredBuffer<Light>           lightsSRV : register( t15 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
[ earlydepthstencil ]
Pixel main( Fragment fragment )
{
	fragment.normal   = normalize( fragment.normal );
	fragment.tangent  = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view     = normalize( fragment.view );

	float4 parallaxUV = calculateParallaxOcclusion(
		textures[ DEPTH_TEX ], samplerState,
		fragment.textureUV,
		fragment.parallaxOffsetTS * 16,
		fragment.normal,
		fragment.view,
		int2( g_nMinSamples, g_nMaxSamples )
	);

	float parallaxShadow = calculateParallaxOcclusionShadow(
		textures[ DEPTH_TEX ], samplerState,
		fragment.textureUV,
		fragment.lDirTS,
		fragment.normal,
		-dLightD,
		int2( 1, 20 ),
		parallaxUV.zw
	);

	fragment.textureUV = parallaxUV.xy;

	float4 colourSample  = textures[ COLOUR_TEX    ].Sample( samplerState, fragment.textureUV );
	float3 normalSample  = textures[ NORMAL_TX     ].Sample( samplerState, fragment.textureUV ).rgb;
	//float specularSample = textures[ GLOSS_TEX     ].Sample( samplerState, fragment.textureUV ).r;
	float aoSample       = textures[ AO_TEX        ].Sample( samplerState, fragment.textureUV ).r;
	float roughSample    = textures[ ROUGHNESS_TEX ].Sample( samplerState, fragment.textureUV ).r * roughness;
	float metalSample    = textures[ METALNESS_TEX ].Sample( samplerState, fragment.textureUV ).r * metalness;


	float shadowBiasScale = tan( acos( dot( dLightD, fragment.normal ) ) );

	fragment.normal = calculateBumpNormalZ(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		normalSample,
		float3( bumpScaleX, bumpScaleY, 1 ) );



	float2 shadow1 = getShadowCascade(
		textures[ SHADOW_1_MAP ],
		cmpSampler,
		fragment.shadow1Pos,
		shadowSize1,
		shadowSize1.x * 1.1 * shadowBiasScale
	);

	float shadow2 = getShadowWithBias(
		textures[ SHADOW_2_MAP ],
		cmpSampler,
		fragment.shadow2Pos,
		shadowSize2,
		shadowSize2.x * 1.5 * shadowBiasScale
	);

	float day = smoothstep( 0.05, 0.3, dot( dLightD, float3( 0, 1, 0 ) ) );
	float shadow = lerp( shadow2, shadow1.r, shadow1.g ) * day * parallaxShadow;


	float4 shadowColour = getShadowColour(
		textures[ SHADOW_TD_MAP ], cmpSampler,
		textures[ SHADOW_TC_MAP ], samplerState,
		fragment.shadow1Pos,
		shadowSize1.x * 1.1 * shadowBiasScale );

	float3 lightColour = lerp( dLightC.rgb, shadowColour.rgb, shadowColour.a ) * shadow;

	int clusterIndex = getClusterIndex( fragment.position.xy, fragment.viewPos.z, float2( targetWidth, targetHeight ), zDepths.x, zDepths.y );
	LightCluster cluster = lightGridSRV[ clusterIndex ];


/////////////////////////////////
//	float z = frac( max( min( BLOCK_DEPTH * ( log( fragment.viewPos.z / zDepths.x ) / log( zDepths.y / zDepths.x ) ), BLOCK_DEPTH - 1 ), 0 ) );
//	float2 zz = 1 - pow( abs( frac( fragment.position.xy / BLOCK_SIZE ) * 2 - 1 ), 5 );
//
//	colourSample.rgb *= ( 1 - pow( abs( z * 2 - 1 ), 10 ) );
//	colourSample.rgb *= zz.x * zz.y;
/////////////////////////////////

	LightingInfo surface = clusterLighting(
		cluster, lightIndexListSRV, lightsSRV,
		lightColour.rgb, dLightD,
		fragment.worldPos,
		fragment.normal,
		-fragment.view,
		colourSample.rgb * kd,
		ks,
		metalSample,
		roughSample,
		aoSample
	);

	Pixel pixel;


	/*
	pixel.colour = float4( 0, 0, 0, 1 );

	float3 s1 = fragment.shadow1Pos.xyz /= fragment.shadow1Pos.w;

	if( s1.x <= -1.0f || s1.x >= 1.0f ||
		s1.y <= -1.0f || s1.y >= 1.0f ||
		s1.z <=  0.0f || s1.z >= 1.0f )
	{}
	else
	{
		pixel.colour.r = max( surface.colour.r, 1 - shadow1 );
	}

	float3 s2 = fragment.shadow2Pos.xyz /= fragment.shadow2Pos.w;

	if( s2.x <= -1.0f || s2.x >= 1.0f ||
		s2.y <= -1.0f || s2.y >= 1.0f ||
		s2.z <=  0.0f || s2.z >= 1.0f )
	{}
	else
	{
		pixel.colour.g = max( surface.colour.r, 1 - shadow2 );
	}
	*/

	pixel.colour = float4( surface.colour, 1 );
	pixel.normal = float4( fragment.normal.rgb, 1 );
	pixel.viewSP = float4( fragment.viewPos, fragment.position.z * fragment.position.w );
	pixel.F0     = float4( surface.reflection, roughSample );

	return pixel;
}
