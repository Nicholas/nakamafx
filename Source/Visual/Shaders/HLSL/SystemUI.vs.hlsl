
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.UIFragment.hlsl"

cbuffer cbSystemUI: register( b0 )
{
	float2 transform;
	float2 p0;
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexPTC vertex )
{
	Fragment fragment;

	float2 position = vertex.position.xy * transform + float2( -1, 1 );

	fragment.position = float4( position, vertex.position.z, 1.0f );
	fragment.textureUV = vertex.textureUV;
	fragment.colour = vertex.colour;

	return fragment;
}
