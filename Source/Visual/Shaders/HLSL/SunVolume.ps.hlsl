
#include "algorithms.ShadowMapping.hlsl"
#include "type.iPS.Textured3DFragment.hlsl"

#include "algorithms.hlsl"
#include "algorithms.Noise.hlsl"
#include "algorithms.IntersectionTesting.hlsl"
#include "algorithms.Fog.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer cbPerLight : register( b0 )
{
	float4x4 invView;
	float4x4 shadowVP1;
	float4x4 shadowVP2;

	float4 shadowSize1;
	float4 shadowSize2;

	float4 dLightC;
	float3 dLightD;
	float p0;

	float3 cameraPos;
	float waterHeight;

	float density;
	float heightFalloff;
	float rayDepth;

	// Define our lobe scattering  -1 to 1 -> 0 is isotropic
	float gScatter;
};

Texture2D ViewSpace             : register( t0 );
Texture2D ShadowMap1            : register( t1 );
Texture2D ShadowMap2            : register( t2 );
Texture2D TransparentShadowMapD : register( t3 );
Texture2D TransparentShadowMapC : register( t4 );

Texture1D random : register( t5 );

SamplerState         samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );

#define SAMPLE_COUNT 128

float4 getWorldPosition( in Fragment fragment )
{
	float4 view  = ViewSpace.Load( int3( fragment.position.xy, 0 ) );
	float4 world = mul( float4( view.xyz, 1 ), invView );

	return float4( world.xyz, view.w );
}

float3 toShadowSpace( in float3 position, in float4x4 shadowMatrix )
{
	float4 shadowPos = mul( float4( position, 1 ), shadowVP2 );

	shadowPos.xyz /= shadowPos.w;

	shadowPos.x = shadowPos.x *  0.5 + 0.5;
	shadowPos.y = shadowPos.y * -0.5 + 0.5;

	return shadowPos.xyz;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ) : SV_TARGET0
{
	//clip( density - 0.0001 );
	if( density <= 0.001 ){ return float4( 0, 0, 0, 0 ); };
	float4 worldPos = getWorldPosition( fragment );

	float3 view          = worldPos.xyz - cameraPos;
	float  viewLengthM   = length( view );
	float  viewLength    = min( viewLengthM, rayDepth );
	float3 viewDirection = normalize( view );

	float day = smoothstep( -0.1, 0.1, dot( dLightD, float3( 0, 1, 0 ) ) );

	if( cameraPos.y + viewDirection.y * 0.05f < waterHeight )
	{
		// TOOD: Make this look like the water...
		float3 water = float3( 0.05, 0.07, 0.3 );
		float a = calculateFog( viewLengthM, density * 0.5f );
		return float4( ( water + water * dLightC.rgb * day ) * ( a ), a );
	}

	view = viewDirection * viewLength;

	float3 originSP = toShadowSpace( cameraPos, shadowVP2 );
	float3  worldSP = toShadowSpace( view + cameraPos, shadowVP2 );

	float3 shadowStepDir = worldSP.xyz - originSP.xyz;
	float rayLength = length( shadowStepDir );

	float RdotV = dot( dLightD, viewDirection );

	float shadowStepSize = rayLength / SAMPLE_COUNT;
	float3 shadowStep = normalize( shadowStepDir ) * shadowStepSize;

	float3 wsStep = view * shadowStepSize / rayLength;
	float wsStepSize = length( wsStep );

	// Mie scaterring approximated with Henyey-Greenstein phase function.
	float scatter = 1.0f - gScatter * gScatter;
	scatter /= (4.0f * PI * pow(1.0f + gScatter * gScatter - (2.0f * gScatter) * RdotV, 1.5f));

	float curdensity = 0;
	float transmittance = 1;
	float lightenergy  = 0;

	[ unroll ]
	for( int i = 0;i < SAMPLE_COUNT; ++i )
	{
		float shadow = 1;
		float3 offset = shadowStep * i;
		float3 shadowSample = offset + originSP.xyz;
		float3 wsOffset = cameraPos + wsStep * i;

		float volumeFogHeightDensityAtViewer = clamp( exp( -heightFalloff * wsOffset.y ), 0, 10 );

	//	if( !( shadowSample.x <= 0.0f || shadowSample.x >= 1.0f ||
	//		   shadowSample.y <= 0.0f || shadowSample.y >= 1.0f ||
	//		   shadowSample.z <= 0.0f || shadowSample.z >= 1.0f ) )
	//	{
			shadow = ShadowMap2.SampleCmpLevelZero( cmpSampler, shadowSample.xy, shadowSample.z );
	//	}

		curdensity = saturate( volumeFogHeightDensityAtViewer * wsStepSize * scatter * shadow * density );
		float absorbtion = curdensity;
		lightenergy += absorbtion * transmittance;
		transmittance *= 1 - curdensity;
	}

	float c = calculateFogFalloff( viewLengthM, worldPos.xyz, heightFalloff, density );
	curdensity =  ( c * 0.6f ) - lightenergy;

	float absorbtion = curdensity;
	lightenergy += absorbtion * transmittance;
	transmittance *= 1 - curdensity;


	float d = 1 - transmittance;
	float3 colour = lerp( float3( 1, 1, 1 ), dLightC.rgb, day ) * lightenergy * d;

	return float4( colour, d );
}
