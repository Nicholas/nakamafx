
#ifndef _RGBM_ENCODING_ALGORITHMS_HLSL
#define _RGBM_ENCODING_ALGORITHMS_HLSL

const static float3x3 luvEncodingM = float3x3
(
    0.2209, 0.3390, 0.4184,
    0.1138, 0.6780, 0.7319,
    0.0102, 0.1130, 0.2969
);

const static float3x3 luvDecodingM = float3x3
(
     6.0014, -2.7008, -1.7996,
    -1.3320,  3.1029, -5.7721,
     0.3008, -1.0882,  5.6268
);

//const static float3x3 luvDecodingM = float3x3
//(
//     6.0013, -2.7000, -1.7995,
//    -1.3320,  3.1029, -5.7720,
//     0.3007, -1.0880,  5.6268
//);

float4 LogLuvEncode( in float3 vRGB )
{
    float4 vResult;
    float3 Xp_Y_XYZp = mul( vRGB, luvEncodingM );
    Xp_Y_XYZp = max( Xp_Y_XYZp, float3( 1e-6, 1e-6, 1e-6 ) );
    vResult.xy = Xp_Y_XYZp.xy / Xp_Y_XYZp.z;
    float Le = 2 * log2( Xp_Y_XYZp.y ) + 127;
    vResult.w = frac( Le );
    vResult.z = ( Le - ( floor( vResult.w * 255.0f ) ) / 255.0f ) / 255.0f;
    return vResult;
}

float3 LogLuvDecode( in float4 vLogLuv )
{
    float Le = vLogLuv.z * 255 + vLogLuv.w;
    float3 Xp_Y_XYZp;
    Xp_Y_XYZp.y = exp2( ( Le - 127 ) / 2 );
    Xp_Y_XYZp.z = Xp_Y_XYZp.y / vLogLuv.y;
    Xp_Y_XYZp.x = vLogLuv.x * Xp_Y_XYZp.z;

    float3 vRGB = mul( Xp_Y_XYZp, luvDecodingM );
    return max( vRGB, 0 );
}




float4 RGBMEncodeScaled( float3 colour, float scale )
{
    float4 rgbm;

    colour = sqrt( colour ) / scale;
    rgbm.a = max( max( colour.r, 1e-6 ), max( colour.g, colour.b ) );
    rgbm.a = ceil( rgbm.a * 255.0 ) / 255.0;
    rgbm.rgb = colour / rgbm.a;
    return rgbm;
}

float3 RGBMDencodeScaled( float4 colour, float scale )
{
    return pow( scale * colour.rgb * colour.a, 2 );
}

static const float rgbmScale = 6.0f;


float4 RGBMEncode( float3 colour )
{
    return RGBMEncodeScaled( colour, rgbmScale );
}

float3 RGBMDencode( float4 colour )
{
    return RGBMDencodeScaled( colour, rgbmScale );
}



float4 decodeFloatRGBA( float v )
{
    float4 enc = float4( 1.0, 255.0, 65025.0, 16581375.0 ) * v;
    enc = frac( enc );
    enc -= enc.yzww * float4( 1.0 / 255, 1.0 / 255, 1.0 / 255, 0.0 );
    return enc;
}

float encodeFloatRGBA( float4 rgba )
{
    return dot( rgba, float4( 1.0, 1/255.0, 1/65025.0, 1/16581375.0 ) );
}



float4 getFloats( uint colour )
{
    float4 fColour;

    fColour.a = ( ( colour >> 24 ) & 0xff );
    fColour.b = ( ( colour >> 16 ) & 0xff );
    fColour.g = ( ( colour >> 8 ) & 0xff );
    fColour.r = ( ( colour & 0xff ) );

    return fColour / 256.0;
}

uint getUint( float4 colour )
{
    uint4 uColour4 = clamp( colour * 255, 0, 255 );

    return uColour4.a << 24 | uColour4.b << 16 | uColour4.g << 8 | uColour4.r;
}

#endif// _RGBM_ENCODING_ALGORITHMS_HLSL