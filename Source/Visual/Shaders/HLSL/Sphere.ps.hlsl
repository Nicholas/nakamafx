
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
//#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.Lighting.hlsl"
#include "type.Light.hlsl"

struct Pixel
{
	float4 colour: SV_TARGET0;
	float4 normal: SV_TARGET1;
	float4 viewSP: SV_TARGET2;
	float4 F0    : SV_TARGET3;
};
//////////////
// GLOBALS //
////////////
float3 modNormal( float3 normal, float3 tangent, float3 bitangent, float3 offset )
{
	float3 result;
	result = offset.x * tangent
		   - offset.y * bitangent
		   + offset.z * normal;

	return normalize( result );
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	fragment.normal   = normalize( fragment.normal );
//	fragment.tangent  = normalize( fragment.tangent );
//	fragment.binormal = normalize( fragment.binormal );
	fragment.view     = normalize( fragment.view );

	float2 dist = fragment.textureUV * 2 - 1;
	float radius = dist.x * dist.x + dist.y * dist.y;

	clip( 1 - radius );


	fragment.normal = modNormal(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		float3( dist.x, dist.y, sqrt( 1 - radius ) ) );
//
//
//
//	float3 ambiantLight = ambiantLighting(
//		aLightC.rgb,
//		ka,
//		1.0f
//	);
//
//	float3 directionLight = directionLighting(
//		dLightC.rgb,
//		dLightD,
//		fragment.normal,
//		fragment.view,
//		1.0,
//		kd, ks, 16//ns
//	);
	float4 colour = float4( kd, 1 );

	if( ka.g > 0 )
	{
		//colour.rgb *= calulateSpotLightFallOff( fragment.view, ks.rgb, ka.r );
		colour.rgb *= calulateSpotLightFallOff( -fragment.normal, ks.rgb, ka.r );
	}

	Pixel pixel;

	pixel.colour = max( colour, 0 );

	pixel.normal = float4( fragment.normal, 0 );
	pixel.viewSP = float4( fragment.viewPos, 0 );
	pixel.F0     = float4( 0, 0, 0, 0 );

	return pixel;
}
