
#include "type.Light.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"


//////////////
// GLOBALS //
////////////

Texture2D textures[ 5 ] : register( t0 );
SamplerState samplerState;
SamplerComparisonState cmpSampler;

#define      DAY_TEX 0
#define    NIGHT_TEX 1
#define   NORMAL_TEX 2
#define SPECULAR_TEX 3
#define   CLOUDL_TEX 4

StructuredBuffer<LightCluster> lightGridSRV : register( t5 );
StructuredBuffer< uint >  lightIndexListSRV : register( t6 );
StructuredBuffer<Light>           lightsSRV : register( t7 );

//////////////
// TYPEDEFS //
//////////////

float3 modNormal( float3 normal, float3 tangent, float3 bitangent, float3 offset )
{
	float3 result;
	result = offset.x * tangent
		   - offset.y * bitangent
		   + offset.z * normal;

	return normalize( result );
}

static const float PI = 3.1415926536f;
float2 sphereMapTextureUV( float3 normal, out float2 dirivX, out float2 dirivY )
{
	float y = acos( normal.y ) / PI;

	float x1 = atan2( normal.z, normal.x );
	float x2 = atan2( normal.z, abs( normal.x ) );

	x1 = saturate( ( ( x1 + PI ) / ( PI * 2 ) ) );
	x2 = saturate( ( ( x2 + PI ) / ( PI * 2 ) ) );

	float2 textureUV1 = float2( x1, y );
	float2 textureUV2 = float2( x2, y );

	dirivX = ddx( textureUV2 );
	dirivY = ddy( textureUV2 );

	return textureUV1;
}


static float spinConst = -0.7f;

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	fragment.normal   = normalize( fragment.normal );
	fragment.tangent  = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view     = normalize( fragment.view );

	float2 dist = fragment.textureUV * 2 - 1;
	float radius = dist.x * dist.x + dist.y * dist.y;

	clip( 1 - radius );


	// Sphere Map Normal
	fragment.normal = modNormal(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		float3( dist.x, dist.y, sqrt( 1 - radius ) ) );

	fragment.tangent  = normalize( cross( fragment.normal, fragment.normal.y < 0.9 ? float3( 0.0, 1.0, 0.0 ) : float3( 0.0, 0.0, 1.0 ) ) );
	fragment.binormal = normalize( cross( fragment.normal, fragment.tangent ) );
	fragment.tangent  = normalize( cross( fragment.binormal, fragment.normal ) );

	float sunTheta = smoothstep( 0.4, 0.5, ( -dot( dLightD, fragment.normal ) + 1 ) * 0.5f );



	float2 dirivX;
	float2 dirivY;

	float2 tex = sphereMapTextureUV( fragment.normal, dirivX, dirivY );
	tex.x += time * spinConst;

	float4 day      = textures[ DAY_TEX ].SampleGrad( samplerState, tex, dirivX, dirivY );
	float4 night    = textures[ NIGHT_TEX ].SampleGrad( samplerState, tex, dirivX, dirivY );
	float4 normal   = textures[ NORMAL_TEX ].SampleGrad( samplerState, tex, dirivX, dirivY );
	float4 cloud    = textures[ CLOUDL_TEX ].SampleGrad( samplerState, tex, dirivX, dirivY );
	float4 specular = textures[ SPECULAR_TEX ].SampleGrad( samplerState, tex, dirivX, dirivY );

	night = pow( abs( night ), 2.2 );

	// Bump Map Normal
	fragment.normal = calculateBumpNormal(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		float3( normal.rg, 1 ),
		float3( -1, -1, 1 )
	);

	float3   albido = lerp( day, night, sunTheta ).rgb;

	float roughness = specular.r * 0.65f + 0.35f;
	float metalness = 0;

	float3      F0 = calculateF0( albido.rgb, ks, metalness );
	float3 diffuse = lambartDiffuseBDRF( albido.rgb, metalness );

	float3 V = -fragment.view;
	float3 N = fragment.normal;

	/////////////////
	// LOOP LIGHT //
	///////////////
	// Direction
	float3 fragmentColour = calculateLightBRDF( dLightC.rgb, dLightD, V, N, diffuse, F0, roughness );


	fragmentColour += albido * ( sunTheta );

	////////////////////////
	// Reflection Factor //
	//////////////////////
	float3 reflection = fresnelSchlick( F0, saturate( dot( N, V ) ) ) * ( 1 - roughness );

	Pixel pixel;

	pixel.colour = float4( fragmentColour, 1 );
	pixel.normal = float4( fragment.normal, 1 );
	pixel.viewSP = float4( 0, 0, 0, 1 );
	pixel.F0     = float4( reflection, roughness );

	return pixel;
}
