//////////////////////////////////////
// View Mesh Normal Lines Geometry //
////////////////////////////////////

#include "type.vs.cb0.PerScene.hlsl"
#include "type.vs.cb1.PerModel.hlsl"
#include "type.iVS.Vertex.hlsl"

//////////////
// TYPEDEFS //
//////////////

struct Vertex
{
	float3 position  : POSITION;

	float3 normal	 : NORMAL;
	float3 tangent   : TANGENT;
	float3 binormal  : BINORMAL;
};
////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
Vertex main( VertexPTNTB vertex )
{
	Vertex fragment;

	fragment.position = mul( float4( vertex.position, 1.0f ), model ).xyz;

	fragment.normal   = normalize( mul( float4( vertex.normal, 0.0f ), model ) ).xyz;
	fragment.tangent  = normalize( mul( float4( vertex.tangent, 0.0f ), model ) ).xyz;
	fragment.binormal = normalize( mul( float4( vertex.binormal, 0.0f ), model ) ).xyz;

	return fragment;
}
