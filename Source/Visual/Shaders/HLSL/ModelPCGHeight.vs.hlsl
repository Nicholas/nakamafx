
#include "algorithms.Noise.hlsl"
#include "type.vs.cb0.PerScene.hlsl"
#include "type.vs.cb1.PerModel.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"

void tangentData( Fragment fragment )
{
	float3 c1 = cross( fragment.normal, float3( 0.0, 0.0, 1.0 ) );
	//float3 c2 = cross( fragment.normal, float3( 0.0, 1.0, 0.0 ) );
	//if( length( c1 )>length( c2 ) )
	fragment.tangent = c1;
	//else
	//	fragment.tangent = c2;

	fragment.tangent = normalize( fragment.tangent );
	fragment.binormal = cross( fragment.normal, fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
}

void parralaxData( inout Fragment fragment )
{
	float3x3 mWorldToTangent = float3x3( fragment.tangent, fragment.binormal, fragment.normal );

	// Propagate the view and the light vectors (in tangent space):
	fragment.lDirTS = mul( mWorldToTangent, fragment.worldPos.xyz - pLightP );
	float3 viewTS = mul( mWorldToTangent, fragment.worldPos.xyz - cameraPosition );

	// Compute initial parallax displacement direction:
	float2 vParallaxDirection = normalize( viewTS.xy );

	// The length of this vector determines the furthest amount of displacement:
	float fLength = length( viewTS );
	float fParallaxLength = sqrt( fLength * fLength - viewTS.z * viewTS.z ) / viewTS.z;

	// Compute the actual reverse parallax displacement vector:
	fragment.parallaxOffsetTS = vParallaxDirection * fParallaxLength * 0.005f;
}

float nmlz( float v )
{
	return ( v + 1 ) * 0.5;

}

float getSampleHeight( float2 uv, float power, float scale )
{
	float cellScale = 0.015f;

	float2 sampleUV = uv * cellScale;

	float height  = nmlz( snoise2D( sampleUV * 0.125 ) );
		  height += ( snoise2D( sampleUV * 0.25 ) ) * 1;
		  height += ( snoise2D( sampleUV * 1 ) ) * 0.1;
		  height  = pow( height, power );

		  height *= scale;
	return height;
}


float getSampleHeight( float2 uv )
{
	return getSampleHeight( uv, 2, 25 ) - 1.2;
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexPTNTB vertex )
{
	Fragment fragment;

	float4 worldPosition = mul( float4( vertex.position, 1.0f ), model );

	worldPosition.y += getSampleHeight( worldPosition.xz );

	float h1A = getSampleHeight( worldPosition.xz + float2(  1,  1 ) );
	float h1B = getSampleHeight( worldPosition.xz + float2( -1, -1 ) );
	float h2A = getSampleHeight( worldPosition.xz + float2(  1, -1 ) );
	float h2B = getSampleHeight( worldPosition.xz + float2( -1,  1 ) );

	float3 p1A = worldPosition.xyz + float3(  1, h1A,  1 );
	float3 p1B = worldPosition.xyz + float3( -1, h1B, -1 );
	float3 p2A = worldPosition.xyz + float3(  1, h2A, -1 );
	float3 p2B = worldPosition.xyz + float3( -1, h2B,  1 );

	float3 normal = normalize( cross( normalize( p1A - p1B ), normalize( p2A - p2B ) ) );

	fragment.position = mul( worldPosition, viewProjectionMatrix );

	fragment.normal   = normalize( mul( float4( normal  , 0 ), model ) ).xyz;
	//fragment.normal   = normalize( mul( float4( vertex.normal  , 0 ), model ) ).xyz;
	fragment.tangent  = normalize( mul( float4( vertex.tangent , 0 ), model ) ).xyz;
	fragment.binormal = normalize( mul( float4( vertex.binormal, 0 ), model ) ).xyz;

	//fragment.textureUV = vertex.textureUV;
	fragment.textureUV = worldPosition.xz * 0.0078125f;

	fragment.view      = worldPosition.xyz - cameraPosition;
	fragment.worldPos  = worldPosition.xyz;
	fragment.shadow1Pos = mul( worldPosition, shadow1ProjectionMatrix );
	fragment.shadow2Pos = mul( worldPosition, shadow2ProjectionMatrix );

	fragment.viewPos = mul( worldPosition, view ).xyz;
	//fragment.viewNorm = mul( float4(  fragment.normal, 0 ), view );

	parralaxData( fragment );

	return fragment;
}