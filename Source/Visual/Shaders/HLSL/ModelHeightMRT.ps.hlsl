
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

//#define SPECULAR_BRDF_GGX
#define SPECULAR_BRDF_Schlick
//#define SPECULAR_BRDF_Beckmann

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.Noise.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"


//////////////
// GLOBALS //
////////////
// TODO: Make this a parameter
#define grassScale 32
#define  dirtScale 48
#define rockScale 32

#define g_nMinSamples 1
#define g_nMaxSamples 8

Texture2D textures[ 16 ] : register( t0 );
SamplerState samplerState         : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );

#define grassT 0
#define grassN 1
#define grassH 2
#define grassR 3

#define dirtT 4
#define dirtN 5
#define dirtH 6
#define dirtR 7

#define rockT 8
#define rockN 9
#define rockH 10
#define rockR 11

#define Shadow1Map 12
#define Shadow2Map 13
#define ShadowTDMap 14
#define ShadowTCMap 15

// Forward Clustered Lights
StructuredBuffer<LightCluster> lightGridSRV : register( t16 );
StructuredBuffer< uint >  lightIndexListSRV : register( t17 );
StructuredBuffer<Light>           lightsSRV : register( t18 );


float3 calcNormal( Fragment fragment, float3 normal, float3 scale )
{
	normal = ( normal * 2.0f ) - 1.0f;

	normal *= scale;

	normal = fragment.normal * normal.z
		+ normal.x * fragment.tangent
		- normal.y * fragment.binormal;

	return normalize( normal );
}


// TODO: Parameterize this function
float4 getHeightFactors( float2 position, float height, float3 normal )
{
	float grass = textures[ grassH ].Sample( samplerState, position * grassScale ).r;
	float dirt2 = textures[  dirtH ].Sample( samplerState, position *  dirtScale ).r;
	float dirt1 = textures[  dirtH ].Sample( samplerState, position *  dirtScale ).r * 0.3 + 0.15;
	float rock  = textures[  rockH ].Sample( samplerState, position *  rockScale ).r;


	if( height < -2.5 )
	{
		rock  = 1;
		dirt1 = 0;
		grass = 0;
		dirt2 = 0;
	}
	else if( height < -0.5 )
	{
		float blend = ( height + 2.5 ) / 2;

		float lowerHeight = rock;
		float upperHeight = dirt1;

		if( blend <= 0.2 )
		{
			blend = ( blend * 5 ) / 2;

			lowerHeight *= ( 1 - blend );
			upperHeight *= blend * 0.5;
		}
		else if( blend < 0.8 )
		{
			blend = 0.5;

			lowerHeight *= 0.5;
			upperHeight *= 0.5;
		}
		else
		{
			blend = ( ( ( blend - 0.8 ) * 5 ) ) + 0.5;

			lowerHeight *= ( 1 - blend );
			upperHeight *= blend;
		}
		// 5) Make sure blend returns to one
		float2 n = normalize( float2( lowerHeight, upperHeight ) );
		lowerHeight = n.r;
		upperHeight = n.g;

		rock = lowerHeight;
		dirt1 = upperHeight;
		grass = 0;
		dirt2 = 0;
	}
	else if( height < 0.2 )
	{
		float blend = ( height + 0.5 ) / 0.7f;

		float lowerHeight = dirt1;
		float upperHeight = grass;

		// 1) Inverse Height -> increases the minimum height
		lowerHeight = ( 1 - lowerHeight );
		upperHeight = ( 1 - upperHeight );

		// 2) SCALE HEIGHT
		lowerHeight *= saturate( blend );
		upperHeight *= saturate( ( 1 - blend ) );

		// 3) Return Height to 0 -> 1
		lowerHeight = ( 1 - lowerHeight );
		upperHeight = ( 1 - upperHeight );

		// 4) Blend over the layer
		lowerHeight *= ( 1 - blend );
		upperHeight *= blend;


		// 5) Make sure blend returns to one
		float2 n = normalize( float2( lowerHeight, upperHeight ) );
		lowerHeight = n.r;
		upperHeight = n.g;

		rock = 0;
		dirt1 = lowerHeight;
		grass = upperHeight;
		dirt2 = 0;
	}
	else
	{
		float dotProduct = saturate( dot( float3( 0, 1, 0 ), normal ) );

		float blend = saturate( pow( dotProduct, 4.5 ) );

		float lowerHeight = dirt2;
		float upperHeight = grass + 0.001;

		if( blend <= 0.2 )
		{
			lowerHeight = 1;
			upperHeight = 0;
		}
		else if( blend <= 0.6 )
		{
			blend = ( blend - 0.2 ) * 2.5;

			// 1) Inverse Height -> increases the minimum height
			lowerHeight = ( 1 - lowerHeight );
			upperHeight = ( 1 - upperHeight );

			// 2) SCALE HEIGHT
			lowerHeight *= saturate( blend * 3 );
			upperHeight *= saturate( ( 1 - blend ) * 3 );

			// 3) Return Height to 0 -> 1
			lowerHeight = ( 1 - lowerHeight );
			upperHeight = ( 1 - upperHeight );

			// 4) Blend over the layer
			lowerHeight *= ( 1 - blend );
			upperHeight *= blend;


			// 5) Make sure blend returns to one
			float2 n = normalize( float2( lowerHeight, upperHeight ) );
			lowerHeight = n.r;
			upperHeight = n.g;
		}
		else
		{
			lowerHeight = 0;
			upperHeight = 1;
		}

		rock = 0;
		dirt1 = 0;
		grass = upperHeight;
		dirt2 = lowerHeight;
	}


	return float4( grass, dirt1, rock, dirt2 );
}


float getParralaxHeight( float2 position, float2 dx, float2 dy, float height )
{
	//float grass = textures[ grassH ].SampleGrad( samplerState, position * grassScale, dx, dy ).g;
	//float rock  = textures[  dirtH ].SampleGrad( samplerState, position *  dirtScale, dx, dy ).g;
	//float dirt  = textures[  dirtH ].SampleGrad( samplerState, position *  dirtScale, dx, dy ).g;

	float rock = textures[ rockH ].SampleGrad( samplerState, position * rockScale, dx, dy ).r;

	float scale = 1 - smoothstep( -1.0, -0.2, height );
	rock = 1 - ( 1 - rock ) * scale;
	return saturate( rock ) * 0.99 + 0.01;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	fragment.normal = normalize( fragment.normal );
	fragment.tangent = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view = normalize( fragment.view );
	fragment.lDirTS = normalize( fragment.lDirTS );

	float3 sNorm = fragment.normal;
	float paralazHeightScale = 0.5f;




	// Compute all the derivatives:
	float2 dx = ddx( fragment.textureUV );
	float2 dy = ddy( fragment.textureUV );



	// +--------------------+
	// | Parallax occlusion |
	// +--------------------+
	int nNumSteps = ( int ) lerp( g_nMaxSamples, g_nMinSamples, saturate( dot( fragment.view, fragment.normal ) ) );

	float fCurrHeight = 0.0;
	float fStepSize = 1.0 / ( float ) nNumSteps;
	float fPrevHeight = 1.0;

	int    nStepIndex = 0;
	bool   bCondition = true;

	float2 vTexOffsetPerStep = fStepSize * fragment.parallaxOffsetTS * paralazHeightScale;
	float2 vTexCurrentOffset = fragment.textureUV;
	float  fCurrentBound = 1.0;
	float  fParallaxAmount = 0.0;

	float2 pt1 = 0;
	float2 pt2 = 0;

	while( nStepIndex < nNumSteps )
	{
		vTexCurrentOffset -= vTexOffsetPerStep;

		// Sample height map which in this case is stored in the alpha channel of the normal map:
		//fCurrHeight = hTexture.SampleGrad( samLinear, vTexCurrentOffset, dx, dy ).x;
		fCurrHeight = getParralaxHeight( vTexCurrentOffset, dx, dy, fragment.worldPos.y );

		fCurrentBound -= fStepSize;

		if( fCurrHeight > fCurrentBound )
		{
			pt1 = float2( fCurrentBound, fCurrHeight );
			pt2 = float2( fCurrentBound + fStepSize, fPrevHeight );

			nStepIndex = nNumSteps + 1;
		}
		else
		{
			nStepIndex++;
			fPrevHeight = fCurrHeight;
		}
	}

	float fDelta2 = pt2.x - pt2.y;
	float fDelta1 = pt1.x - pt1.y;
	float fDenominator = fDelta2 - fDelta1;

	// SM 3.0 and above requires a check for divide by zero since that operation will generate an 'Inf' number instead of 0
	[flatten]
	if( fDenominator == 0.0f )
	{
		fParallaxAmount = 0.0f;
	}
	else
	{
		fParallaxAmount = ( pt1.x * fDelta2 - pt2.x * fDelta1 ) / fDenominator;
	}

	float2 vParallaxOffset = fragment.parallaxOffsetTS * ( 1.0 - fParallaxAmount ) * paralazHeightScale;

	// The computed texture offset for the displaced point on the pseudo-extruded surface:
	fragment.textureUV = fragment.textureUV - vParallaxOffset;








	// +--------------------------------------+
	// | Point Shadow Parralax Shadow Mapping |
	// +--------------------------------------+
	//float3 pLightD = fragment.worldPos - pLightP;
	//
	//nNumSteps = ( int ) lerp( g_nMaxSamples, g_nMinSamples, saturate( dot( pLightD, fragment.normal ) ) );
	//
	//float shadowMultiplier = 0.0;
	//float numSamplesUnderSurface = 0;
	//
	//fStepSize = max( fCurrHeight / ( float ) nNumSteps, 0.02f );
	//
	//
	//float2 vParallaxDirection = normalize( -fragment.lDirTS.xy );
	//float fLength = length( fragment.lDirTS );
	//float fParallaxLength = sqrt( fLength * fLength - fragment.lDirTS.z * fragment.lDirTS.z ) / fragment.lDirTS.z;
	//
	//vTexOffsetPerStep = fStepSize * fParallaxLength * vParallaxDirection * 0.004f;
	//
	//float currentLayerHeight = fCurrHeight + fStepSize + saturate( noise( ( fragment.textureUV * 45000 ) ) ) * fStepSize;
	//float heightFromTexture;
	//
	//while( currentLayerHeight < 0.9 )
	//{
	//	vTexCurrentOffset -= vTexOffsetPerStep;
	//
	//	heightFromTexture = getParralaxHeight( vTexCurrentOffset, dx, dy, fragment.worldPos.y );
	//
	//	if( heightFromTexture > currentLayerHeight )
	//	{
	//		numSamplesUnderSurface += 1;
	//
	//		float newShadowMultiplier = pow( saturate( ( heightFromTexture - currentLayerHeight ) / ( 1 - currentLayerHeight ) ), 1 / 2.8 );
	//		shadowMultiplier = max( shadowMultiplier, newShadowMultiplier );
	//	}
	//
	//	currentLayerHeight += fStepSize;
	//}
	//
	//
	//// Shadowing factor should be 1 if there were no points under the surface
	//[flatten]
	//if( numSamplesUnderSurface < 1 )
	//{
	//	shadowMultiplier = 1;
	//}
	//else
	//{
	//	shadowMultiplier = 1.0 - shadowMultiplier;
	//}
	//
	//float parallaxShadow = saturate( shadowMultiplier );
















	float4 grassTTex = textures[ grassT ].Sample( samplerState, fragment.textureUV * grassScale );
	float4 grassNTex = textures[ grassN ].Sample( samplerState, fragment.textureUV * grassScale );
	float  grassRTex = textures[ grassR ].Sample( samplerState, fragment.textureUV * grassScale ).x;

	float4 dirtTTex = textures[ dirtT ].Sample( samplerState, fragment.textureUV * dirtScale );
	float4 dirtNTex = textures[ dirtN ].Sample( samplerState, fragment.textureUV * dirtScale );
	float  dirtRTex = textures[ dirtR ].Sample( samplerState, fragment.textureUV * dirtScale ).x;

	float4 rockTTex = textures[ rockT ].Sample( samplerState, fragment.textureUV * rockScale );
	float4 rockNTex = textures[ rockN ].Sample( samplerState, fragment.textureUV * rockScale );
	float  rockRTex = textures[ rockR ].Sample( samplerState, fragment.textureUV * rockScale ).x;



	float3 grassNormal = calcNormal( fragment, grassNTex.xyz, float3( 1.0, 1.0, 1.0 ) );
	float3  dirtNormal = calcNormal( fragment,  dirtNTex.xyz, float3( 1.0, 1.0, 1.0 ) );
	float3  rockNormal = calcNormal( fragment,  rockNTex.xyz, float3( 1.0, 1.0, 1.0 ) );



	float shiffter0 = saturate( noise( ( fragment.textureUV + 0.5 ) * 32 ) ) * 1.3 - 0.3;
	float shiffter1 = saturate( noise( fragment.textureUV * 1200 ) ) * 1.3 - 0.3;
	float shiffter2 = saturate( noise( ( fragment.textureUV ) * 800 ) ) * 1.3 - 0.3;
	float shiffter4 = saturate( noise( ( fragment.textureUV + 20 ) * 16 ) ) * 1.3 - 0.3;

	// Random Grass Colour Modification
	grassTTex.rgb += shiffter0 * 0.2 * grassTTex.rgb;
	grassTTex.rgb -= shiffter1 * 0.2 * grassTTex.rgb;
	grassTTex.rgb += shiffter2 * 0.3 * grassTTex.rgb;
	grassTTex.rgb -= shiffter4 * 0.3 * grassTTex.rgb;

	grassTTex.rgb = saturate( grassTTex.rgb );

	// Random Dirt Colour Modification
	float strata = shiffter0 * 3;
		  strata = strata + fragment.worldPos.y;
		  strata = strata * 3;
		  strata = strata * cos( fragment.worldPos.y * 0.3 );
		  strata = sin( strata );
		  strata = strata * max( shiffter1, 0.25 ) * 0.5;

	dirtTTex.rgb += strata * dirtTTex.rgb;

	dirtTTex.rgb -= shiffter0 * 0.2 * dirtTTex.rgb;
	dirtTTex.rgb += shiffter1 * 0.2 * dirtTTex.rgb;
	dirtTTex.rgb -= shiffter2 * 0.3 * dirtTTex.rgb;
	dirtTTex.rgb += shiffter4 * 0.3 * dirtTTex.rgb;



	float4 heightFactors = getHeightFactors( fragment.textureUV, fragment.worldPos.y, fragment.normal );
		   heightFactors = saturate( heightFactors );


	float4 surfaceAlbido =
		grassTTex * heightFactors.r +
		 dirtTTex * heightFactors.g +
		 rockTTex * heightFactors.b +
		 dirtTTex * heightFactors.a;

	float4 surfaceRoughness =
		grassRTex * heightFactors.r +
		 dirtRTex * heightFactors.g +
		 rockRTex * heightFactors.b +
		 dirtRTex * heightFactors.a;

	float3 surfaceNormal =
		grassNormal * heightFactors.r +
		 dirtNormal * heightFactors.g +
		 rockNormal * heightFactors.b +
		 dirtNormal * heightFactors.a;

	surfaceNormal = normalize( surfaceNormal );


	float min1 = min( shadowSize1.x, shadowSize1.y );
	float min2 = min( shadowSize2.x, shadowSize2.y );

	float LdotSN = saturate( dot( dLightD, sNorm ) );
	float shadowBiasScale = tan( acos( LdotSN ) );
	float shadowBias1 = max( shadowSize1.x, shadowSize1.y ) * 1 * shadowBiasScale + min1 * 0.02f;
	float shadowBias2 = max( shadowSize2.x, shadowSize2.y ) * 2 * shadowBiasScale + min2 * 0.05f;

	float2 shadowFactor1 = getShadowCascade( textures[ Shadow1Map ], cmpSampler, fragment.shadow1Pos, shadowSize1, shadowBias1 );
	float  shadowFactor2 = getShadowWithBias( textures[ Shadow2Map ], cmpSampler, fragment.shadow2Pos, shadowSize2, shadowBias2 );

	float shadowFactor = lerp( shadowFactor2, shadowFactor1.x, shadowFactor1.y ) ;
	float4 shadowColour = getShadowColour(
		textures[ ShadowTDMap ], cmpSampler,
		textures[ ShadowTCMap ], samplerState,
		fragment.shadow1Pos,
		shadowBias1 );

	float3 lightColour = lerp( dLightC.rgb, shadowColour.rgb, shadowColour.a ) * shadowFactor * LdotSN;

	lightColour *= smoothstep( 0.01, 0.2, dot( dLightD, float3( 0, 1, 0 ) ) );

	int clusterIndex = getClusterIndex(
		fragment.position.xy,
		fragment.viewPos.z,
		float2( targetWidth, targetHeight ),
		zDepths.x, zDepths.y
	);
	//int clusterIndex = getClusterIndex( fragment.position.xy, 0, targetWidth );
	LightCluster cluster = lightGridSRV[ clusterIndex ];


	float metalness = 0.0f;
	float ao = 1.0f;

	LightingInfo surface = clusterLighting(
		cluster, lightIndexListSRV, lightsSRV,
		lightColour.rgb, dLightD,
		fragment.worldPos,
		surfaceNormal,
		-fragment.view,
		surfaceAlbido.rgb,
		ks,
		metalness,
		saturate( surfaceRoughness.r ),
		ao
	);



	//float zSlice = zNear * pow( FarZ / NearZ, slice / numSlice );

	//float NearZ =  zDepths.x;
	//float FarZ  = zDepths.y;
	//
	//int k = max( min( BLOCK_DEPTH * ( log( fragment.viewPos.z / NearZ ) / log( FarZ / NearZ ) ), BLOCK_DEPTH - 1 ), 0 );
	//
	//float cell = k % 2;
	//
	//float b = 0;
	//
	//if( clusterIndex.z <= 0 )
	//{
	//    b = 1;
	//}
	//
	//if( clusterIndex.z > 10 )
	//{
	//    b = 1;
	//}

	//float3 lights = 0;
	//if( cluster.count == 0 )
	//{
	//    lights.rgb = 1;
	//}
	//else if( cluster.count == 1 )
	//{
	//    lights.b = 1;
	//}
	//else if( cluster.count == 2 )
	//{
	//    lights.g = 1;
	//}
	//else if( cluster.count == 3 )
	//{
	//    lights.r = 1;
	//}
	//else if( cluster.count == 4 )
	//{
	//    lights.b = 1;
	//    lights.g = 1;
	//}
	//else if( cluster.count == 5 )
	//{
	//    lights.b = 1;
	//    lights.r = 1;
	//}

	//float3 index = 0;
	//float indx = clusterIndex % 16;
	//if( indx < 4 )
	//{
	//    index.r = indx / 4.0f;
	//}
	//else if( indx < 8 )
	//{
	//    index.g = ( indx - 4 ) / 4.0f;
	//}
	//else if( indx < 12 )
	//{
	//    index.b = ( indx - 8 ) / 4.0f;
	//}
	//else if( indx < 16 )
	//{
	//    index.rgb = ( indx - 12 ) / 4.0f;
	//}

	Pixel pixel;

	//pixel.colour = float4( lights, 1 );
	//pixel.colour = float4( cluster.count.rrr, 1 );
	//pixel.colour = float4( index, 1 );
	pixel.colour = float4( surface.colour, 1 );


	//float3 t = surface.reflection.r < 0 ? float3( abs( surface.reflection.r ), 0, 0 ) : surface.reflection;
	//float3 t = surfaceRoughness.r < 0 ? float3( abs( surfaceRoughness.r ), 0, 0 ) : surfaceRoughness.rrr;
	//pixel.colour = float4( t, 1 );
	//pixel.colour = float4( saturate( surface.reflection ), 1 );
	//pixel.colour = float4( abs( surfaceRoughness.rrr ), 1 );
	pixel.normal = float4( surfaceNormal, 1 );
	pixel.viewSP = float4( fragment.viewPos, fragment.position.z * fragment.position.w );
	pixel.F0 = saturate( float4( surface.reflection, surfaceRoughness.r ) );

	return pixel;
}

