#ifndef _CLUSTERD_LIGHTING_TOOLS_
#define _CLUSTERD_LIGHTING_TOOLS_


#include "algorithms.Lighting.hlsl"
#include "type.Light.hlsl"

#define NUMBER_OF_LIGHTS 256

// Compute shader build frustum data
#ifndef BLOCK_SIZE
#pragma message( "BLOCK_SIZE undefined. Default to 16.")
#define BLOCK_SIZE 16 // should be defined by the application.
#endif

#ifndef BLOCK_DEPTH
#pragma message( "BLOCK_DEPTH undefined. Default to 24.")
#define BLOCK_DEPTH 24 // should be defined by the application.
#endif

struct LightingInfo
{
	float3 colour;
	float3 reflection;
};

int getClusterIndex( float2 positionSS, float depthVS, float2 targetSize, float nearZ, float farZ )
{
	int2 tile = ( positionSS / BLOCK_SIZE );
	int2 cells = ceil( targetSize / BLOCK_SIZE );

	int z = max( min( BLOCK_DEPTH * ( log( depthVS / nearZ ) / log( farZ / nearZ ) ), BLOCK_DEPTH - 1 ), 0 );

	return tile.x + ( tile.y * cells.x ) + ( z * cells.x * cells.y );
}

LightingInfo clusterLighting(
	in LightCluster cluster,
	in StructuredBuffer<uint>    lightIndexListSRV,
	in StructuredBuffer<Light>           lightsSRV,
	in float3 sunColour,
	in float3 sunDirection,
	in float3 position,
	in float3 N,
	in float3 V,
	in float3 albido,
	in float3 ks,
	in float metalness,
	in float roughness,
	in float ao
)
{
	LightingInfo surfaceLighting = ( LightingInfo )0;

	roughness = roughness * 0.946f + 0.05f; // Synonym: Smoothness - Minimum 0.04 and a maximum of 0.95
	float3      F0 = calculateF0( albido.rgb, ks, metalness );
	float3 diffuse = lambartDiffuseBDRF( albido.rgb, metalness );

	////////////////////////
	// Reflection Factor //
	//////////////////////
	float3 H = normalize( sunDirection + V );
	surfaceLighting.reflection = fresnelSchlick( F0, saturate( dot( V, H ) ) ) * ( 1 - roughness );

	///////////////
	// LIGHTING //
	/////////////
	// Add Global Direction Light That Has A Cascade Shadow Map
	surfaceLighting.colour = calculateLightBRDF( sunColour, sunDirection, V, N, diffuse, F0, roughness );

	// Add clustered lights
	for ( uint i = 0; i < cluster.count; i++ )
	{
		uint lightIndex = lightIndexListSRV[ cluster.offset + i ];
		Light     light =         lightsSRV[ lightIndex ];

		float3 lightColour = 0;

		switch ( light.type )
		{
			case DIRECTIONAL_LIGHT:
			{
				lightColour = calculateLightBRDF(
					light.colour.rgb, light.directionWS.xyz,
					V, N, diffuse, F0, roughness
				);
				break;
			}
			case POINT_LIGHT:
			{
				lightColour = calculatePointLightBRDF_CORRECTED(
					light.positionWS.xyz, light.colour.rgb, light.range,
					position, V, N, diffuse, F0, roughness
				);
				break;
			}
			case SPOT_LIGHT:
			{
				lightColour = calculateSpotLightBRDF(
					light.positionWS.xyz, light.directionWS.xyz, light.colour.rgb, light.range, light.spotlightAngle,
					position, V, N, diffuse, F0, roughness
				);
				break;
			}
		}

		surfaceLighting.colour += lightColour;
	}

	return surfaceLighting;
}

#endif //_CLUSTERD_LIGHTING_TOOLS_
