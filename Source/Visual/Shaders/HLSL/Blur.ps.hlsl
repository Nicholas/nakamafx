
#include "type.iPS.BlurFragment.hlsl"

/////////////
// GLOBALS //
/////////////
Texture2D image : register( t0 );
SamplerState samplerState : register( s0 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ) : SV_Target0
{
	float weights[ 5 ];
	weights[ 0 ] = 1.0f;
	weights[ 1 ] = 0.9f;
	weights[ 2 ] = 0.55f;
	weights[ 3 ] = 0.18f;
	weights[ 4 ] = 0.1f;

	// Create a normalized value to average the weights out a bit.
	float normalization = weights[ 0 ] + 2.0f * ( weights[ 1 ] + weights[ 2 ] + weights[ 3 ] + weights[ 4 ] );

	// Normalize the weights.
	weights[ 0 ] = weights[ 0 ] / normalization;
	weights[ 1 ] = weights[ 1 ] / normalization;
	weights[ 2 ] = weights[ 2 ] / normalization;
	weights[ 3 ] = weights[ 3 ] / normalization;
	weights[ 4 ] = weights[ 4 ] / normalization;

	float4 output = float4( 0.0f, 0.0f, 0.0f, 0.0f );

	output += image.Sample( samplerState, fragment.texCoord1 ) * weights[ 4 ];
	output += image.Sample( samplerState, fragment.texCoord2 ) * weights[ 3 ];
	output += image.Sample( samplerState, fragment.texCoord3 ) * weights[ 2 ];
	output += image.Sample( samplerState, fragment.texCoord4 ) * weights[ 1 ];
	output += image.Sample( samplerState, fragment.texCoord5 ) * weights[ 0 ];
	output += image.Sample( samplerState, fragment.texCoord6 ) * weights[ 1 ];
	output += image.Sample( samplerState, fragment.texCoord7 ) * weights[ 2 ];
	output += image.Sample( samplerState, fragment.texCoord8 ) * weights[ 3 ];
	output += image.Sample( samplerState, fragment.texCoord9 ) * weights[ 4 ];

	// Set the alpha channel to one.
	output.a = 1.0f;

	return output;
}