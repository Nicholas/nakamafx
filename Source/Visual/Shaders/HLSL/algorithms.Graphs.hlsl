
#ifndef _ALGORITHM_GRAPHS_HLSL
#define _ALGORITHM_GRAPHS_HLSL

#include "algorithms.ToneMapping.hlsl"



float4 plotU2ToneMappingGraph(
	float2 uv,
	float ShoulderStrength,
	float LinearStrength,
	float LinearAngle,
	float ToeStrength,
	float ToeNumerator,
	float ToeDenominator,
	float LinearWhite )
{
	float4 colour = float4( 0, 0, 0, 0 );

	bool x0 = uv.x < 0.25f;
	bool x1 = uv.x < 0.50f;
	bool x2 = uv.x < 0.75f;

	bool y0 = uv.y < 0.25f;
	bool y1 = uv.y < 0.50f;
	bool y2 = uv.y < 0.75f;

	// Graph Values
	const float MinLogX = -3.0f;
	const float MaxLogX = 3.0f;
	const float MaxY = 1.0f;

	if( x0 )
	{
		if( y0 )
		{
		}
		else if( y1 )
		{
		}
		else if( y2 )
		{
		}
		else //if( y3 )
		{
			uv.y -= 0.75;
			uv *= 4;


			//float t = i / Width;
			float t = uv.x;
			float logX = MinLogX + ( ( MaxLogX - MinLogX ) * t );

			float v = pow( 10.0f, logX );
			float3 g = max( 2 - ToneMapFilmicU2(
									v,
									ShoulderStrength,
									LinearStrength,
									LinearAngle,
									ToeStrength,
									ToeNumerator,
									ToeDenominator,
									LinearWhite ), 0 );

			g = pow( g, 1.0f / 1.2f );

			float height = uv.y * 2;
			float range = 0.005f;
			float r1 = height - range;
			float r2 = height + range;

			if( g.x > r1 && g.x < r2 )
			{
				colour.r = 1;
			}

			if( height > 1 - range && height < 1 + range )
			{
				colour.g = 1;
			}

			if( height > 0 - range && height < 0 + range )
			{
				colour.g = 1;
			}

			if( height > 2 - range && height < 2 + range )
			{
				colour.g = 1;
			}

			range *= 0.5f;
			if( t > 0.5 - range && t < 0.5 + range )
			{
				colour.g = 1;
			}

			if( t > 0.0 - range && t < 0.0 + range )
			{
				colour.g = 1;
			}
			if( t > 1.0 - range && t < 1.0 + range )
			{
				colour.g = 1;
			}

/**/
			range = 0.1f;
			float lineValue = 10.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 1;
				colour.r = 1;
				colour.g = 1;
			}

			lineValue = 9.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 8.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 7.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 6.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 5.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 4.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 3.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 2.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 1.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 0.5f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 0.2f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}

			lineValue = 0.0f;
			if( v > lineValue - range && v < lineValue + range )
			{
				colour.b = 0.2;
			}
//*/

			//if( v < 0 )
			//{
			//	colour.g = 1;
			//}
		}
	}

	colour.a = max( max( colour.r, colour.y ), colour.b );
	return colour;
}


#endif // _ALGORITHM_GRAPHS_HLSL
