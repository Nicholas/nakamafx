
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.FontMaterial.hlsl"
#include "type.iPS.Font2DFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"

#include "algorithms.Noise.hlsl"

#define STD 0
#define WARP 1

/////////////
// GLOBALS //
/////////////
Texture2DArray msdfImage : register( t0 );

SamplerState samplerState : register( s0 );

float median( float r, float g, float b )
{
	return max( min( r, g ), min( max( r, g ), b ) );
}

float getMSDFEdge( float3 msdf, float2 msdfUnit, float2 uv, float offset )
{
	msdf -= 0.5f - offset;
	float sdf = median( msdf.r, msdf.g, msdf.b );


	sdf *= dot( msdfUnit, 0.5f / fwidth( uv ) );

	return saturate( sdf + 0.5f );
}

float getSDFEdge( float sdf, float2 msdfUnit, float2 uv, float offset )
{
	sdf -= 0.5f - offset;
	sdf *= dot( msdfUnit, ( 0.5f ) / fwidth( uv ) );

	return saturate( sdf + 0.5f );
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader - Texture Sample
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ): SV_TARGET0
{
	if( properties.x == WARP )
	{
		fragment.textureUV.xy += snoise2D( ( fragment.textureUV.xy + time ) * 10.0 ) * 0.03;
		fragment.textureUV.xy += snoise2D( ( fragment.textureUV.xy + time ) * 40.0 ) * 0.02;
	}

	float4 msdfSample = msdfImage.Sample( samplerState, fragment.textureUV );

	uint3 textureSize;
	msdfImage.GetDimensions( textureSize.x, textureSize.y, textureSize.z );

	float pxRange    = 4.0f;
	float  sdfOffset = 0.0f;

	float2  sdfUnit = pxRange / textureSize.xy;

	float  sdf = getMSDFEdge( msdfSample.rgb, sdfUnit, fragment.textureUV.xy, sdfOffset );
	clip( sdf - 0.01 );

	return float4( 0, 0, 0, sdf );
}
