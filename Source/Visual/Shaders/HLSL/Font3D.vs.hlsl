

#include "type.vs.cb0.PerScene.hlsl"
#include "type.vs.cb1.PerModel.hlsl"
#include "type.iVS.Vertex.hlsl"
//#include "type.iPS.FontFragment.hlsl"
#include "type.iPS.Font3DFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader - Pass Through
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexChar characture, uint vertexID : SV_VertexID )
{
	Fragment fragment;

	// vert id 0 = 0000, uv = (0, 0)
	// vert id 1 = 0001, uv = (1, 0)
	// vert id 2 = 0010, uv = (0, 1)
	// vert id 3 = 0011, uv = (1, 1)
	float2 uv = float2( vertexID & 1, ( ( vertexID >> 1 ) & 1 ) );

	// set the position for the vertex based on which vertex it is (uv)
	//fragment.position = float4( characture.position.x + (characture.position.z * uv.x),
	//							1 - ( characture.position.y - ( characture.position.w * uv.y ) ),
	//							0, 1 );

	// set the texture coordinate based on which vertex it is (uv)
	//fragment.textureUV = float3( characture.textureUV.x + (characture.textureUV.z * uv.x),
	//							 1 - ( characture.textureUV.y + ( characture.textureUV.w * uv.y ) ),
	//							 characture.id );

	float4 offset = float4( characture.position.xy, characture.textureUV.xy );
	float4 size = float4( characture.position.zw * float2( 1, -1 ), characture.textureUV.zw );
	float4 coord  = mad( uv.xyxy, size, offset ); //offset + ( size * uv.xyxy );


	float4 worldPosition = mul( float4( coord.x, 1 - coord.y, 0, 1 ), model );

	fragment.position   = mul( worldPosition, viewProjectionMatrix );
	fragment.textureUV  = float3( coord.z, 1 - coord.w, characture.id );

	fragment.normal     = mul( float4(  0,  0, -1, 0 ), model ).xyz;
	fragment.tangent    = mul( float4(  0, -1,  0, 0 ), model ).xyz;
	fragment.binormal   = mul( float4(  1,  0,  0, 0 ), model ).xyz;

	fragment.viewRay    = cameraPosition - worldPosition.xyz;
	fragment.positionW  = worldPosition.xyz;
	fragment.positionV  = mul( worldPosition, view ).xyz;
	fragment.positionS1 = mul( worldPosition, shadow1ProjectionMatrix );
	fragment.positionS2 = mul( worldPosition, shadow2ProjectionMatrix );

	return fragment;
}
