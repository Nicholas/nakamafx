
#include "type.vs.cb0.TexelSize.hlsl"
#include "type.iPS.BlurFragment.hlsl"

///////////////
// Function //
/////////////
Fragment main( uint id: SV_VertexID )
{
	Fragment fragment;

	fragment.position.x = float( id / 2 ) * 4.0f - 1.0f;
	fragment.position.y = float( id % 2 ) * 4.0f - 1.0f;
	fragment.position.z = 0.0f;
	fragment.position.w = 1.0f;

	float2 textureUV = 0;
	textureUV.x =        float( id / 2 ) * 2.0f;
	textureUV.y = 1.0f - float( id % 2 ) * 2.0f;

	float2 sampleOffset = float2( 0.0, texelSize.y );

	fragment.texCoord1 = textureUV + sampleOffset * -4.0f;
	fragment.texCoord2 = textureUV + sampleOffset * -3.0f;
	fragment.texCoord3 = textureUV + sampleOffset * -2.0f;
	fragment.texCoord4 = textureUV + sampleOffset * -1.0f;
	fragment.texCoord5 = textureUV + sampleOffset *  0.0f;
	fragment.texCoord6 = textureUV + sampleOffset *  1.0f;
	fragment.texCoord7 = textureUV + sampleOffset *  2.0f;
	fragment.texCoord8 = textureUV + sampleOffset *  3.0f;
	fragment.texCoord9 = textureUV + sampleOffset *  4.0f;

	return fragment;
}
