
#include "algorithms.BicubicSamplers.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer ssrBlurCByffer : register( b0 )
{
	// Camera projections for depth testing
	float4x4 view;
	float4x4 projection;

	// Render target texture size
	float4 targetSize;
};

Texture2D gReflectionTexture : register( t0 );
Texture2D gColourTexture     : register( t1 );
Texture2D gSpecularTexture   : register( t2 );

SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState wrapSampler : register( s2 );
SamplerState pointSampler : register( s3 );


//////////////
// TYPEDEFS //
//////////////
struct Fragment
{
	float4 position : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};


static const int blurSize = 4;

static const int offsetSize = 2;
static const float2 offsets[ 2 ][ 4 ] = {
	{
		float2( 0,  2 ), float2( 2,  1 ),
		float2( 0, -1 ), float2( 2, -2 )
	},
	{
		float2( -1,  1 ), float2( 1,  2 ),
		float2( -1, -2 ), float2( 1, -1 )
	}
};


float scaleV( float min, float max, float v )
{
	return saturate( ( v - min ) / ( max - min ) );
}

float3 getSampleCountColour( float samples )
{
	float3 colour = 0;

	if( samples > 96 )
	{
		colour = float3( 1, 0, 0 ) * scaleV( 96, 128, samples );
	}
	else if( samples > 80 )
	{
		colour = float3( 1, 0, 1 ) * scaleV( 80, 96, samples );
	}
	else if( samples > 64 )
	{
		colour = float3( 0, 1, 0 ) * scaleV( 48, 64, samples );
	}
	else if( samples > 48 )
	{
		colour = float3( 1, 1, 0 ) * scaleV( 32, 48, samples );
	}
	else if( samples > 32 )
	{
		colour = float3( 0, 0, 1 ) * scaleV( 16, 32, samples );
	}
	else if( samples > 16 )
	{
		colour = float3( 1, 1, 1 ) * scaleV( 0, 16, samples );
	}

	return colour;
}

float3 get2DOffsetColour( float2 uv )
{
	float3 colour = 0;

	if( uv.x >= 0 )
	{
		colour.x = uv.x;
	}
	else if( uv.x < 0 )
	{
		colour.b = -uv.x;
	}

	if( uv.y >= 0 )
	{
		colour.g = 1;
	}
	else if( uv.y < 0 )
	{
		colour.b = 0.5;
	}

	return colour;
}

float4 getSampleColour( float2 originUV, float2 offset, float2 screenSizeInv, float roughness )
{
	float2 uv = originUV + offset * screenSizeInv;

	float2 edgeTest = abs( uv * 2 - 1 );
	if( edgeTest.x >= 1 || edgeTest.y >= 1 )
	{
		return float4( 0, 0, 0, 0 );
	}

	float4 reflectionSample = gReflectionTexture.Sample( pointSampler, uv );
	float3 reflectionColour = gColourTexture.SampleLevel( samplerState, reflectionSample.xy, roughness * 5 ).rgb;

	//float2 reflectionUV2 = reflectionSample.rg + normalize( reflectionUV1 - uv ) * reflectionSample.b;
	//float3 colour2 = gColourTexture.SampleLevel( pointSampler, reflectionUV2, roughness * 5 ).rgb;

	edgeTest = abs( reflectionSample.xy * 2 - 1 );
	float edgeScale = 1 - smoothstep( 0.9, 1.0, max( edgeTest.x, edgeTest.y ) );

	return float4( reflectionColour, 1 ) * reflectionSample.a * edgeScale;
}


/// Bicubic interpolation sampling of a texture.
float4 bicubicInterpolationFastSSR(
	in float2 uv,
	in float roughness
)
{
	float3 texSize = float3( 0, 0, 0 );

	gColourTexture.GetDimensions( texSize.x, texSize.y );

	float2 rec_nrCP = 1.0 / texSize.xy;
	float2 coord_hg = uv * texSize.xy - 0.5; // TODO: Chech to see if this is correct in dx11
	float2 index = floor( coord_hg );

	float2 f = coord_hg - index;
	float4x4 M = {
		-1,  3, -3,  1,
		 3, -6,  3,  0,
		-3,  0,  3,  0,
		 1,  4,  1,  0
	};
	M /= 6;

	float4 wx = mul( float4( f.x*f.x*f.x, f.x*f.x, f.x, 1 ), M );
	float4 wy = mul( float4( f.y*f.y*f.y, f.y*f.y, f.y, 1 ), M );
	float2 w0 = float2( wx.x, wy.x );
	float2 w1 = float2( wx.y, wy.y );
	float2 w2 = float2( wx.z, wy.z );
	float2 w3 = float2( wx.w, wy.w );

	float2 g0 = w0 + w1;
	float2 g1 = w2 + w3;
	float2 h0 = w1 / g0 - 1;
	float2 h1 = w3 / g1 + 1;

	float2 coord00 = index + h0;
	float2 coord10 = index + float2( h1.x, h0.y );
	float2 coord01 = index + float2( h0.x, h1.y );
	float2 coord11 = index + h1;

	coord00 = ( coord00 + 0.5 ) * rec_nrCP;
	coord10 = ( coord10 + 0.5 ) * rec_nrCP;
	coord01 = ( coord01 + 0.5 ) * rec_nrCP;
	coord11 = ( coord11 + 0.5 ) * rec_nrCP;

	float4 tex00 = getSampleColour( coord00, float2( 0, 0 ), rec_nrCP, roughness );
	float4 tex10 = getSampleColour( coord10, float2( 0, 0 ), rec_nrCP, roughness );
	float4 tex01 = getSampleColour( coord01, float2( 0, 0 ), rec_nrCP, roughness );
	float4 tex11 = getSampleColour( coord11, float2( 0, 0 ), rec_nrCP, roughness );

	tex00 = lerp( tex01, tex00, g0.y );
	tex10 = lerp( tex11, tex10, g0.y );

	return lerp( tex10, tex00, g0.x );
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment): SV_TARGET0
{
	float4 F0 = gSpecularTexture.Sample( samplerState, fragment.textureUV );
	float roughness = F0.w;

	float2 t = fragment.textureUV * targetSize.xy;
	int i = ( t.x + t.y * targetSize.y ) % offsetSize;

	float4 s = float4( 0.5, 0.5, 0.5, 1.0 );

	float4 colour  = getSampleColour( fragment.textureUV, float2( 0, 0 ), targetSize.zw, roughness );
	float4 colourA = getSampleColour( fragment.textureUV, offsets[ i ][ 0 ] * 0.5, targetSize.zw, roughness ) * s;
	float4 colourB = getSampleColour( fragment.textureUV, offsets[ i ][ 1 ] * 0.5, targetSize.zw, roughness ) * s;
	float4 colourC = getSampleColour( fragment.textureUV, offsets[ i ][ 2 ] * 0.5, targetSize.zw, roughness ) * s;
	float4 colourD = getSampleColour( fragment.textureUV, offsets[ i ][ 3 ] * 0.5, targetSize.zw, roughness ) * s;
	//
	//float3 colour = colour1.rgb;
	colour += colourA;
	colour += colourB;
	colour += colourC;
	colour += colourD;

	//float4 colour = bicubicInterpolationFastSSR( fragment.textureUV, roughness );
	float scale = colour.a;
	//scale += colourA.a;
	//scale += colourB.a;
	//scale += colourC.a;
	//scale += colourD.a;



	if( scale < 0.000001f )
	{
		return float4( 0, 0, 0, 0 );
	}
	else
	{
		colour.rgb = max( F0.rgb * ( colour.rgb / colour.a ), 0 );
	}

	return float4( colour.rgb, 1 );
}
