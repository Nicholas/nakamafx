
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"


//////////////
// GLOBALS //
////////////
Texture2D textures[ 9 ];
SamplerState samplerState;

#define    COLOUR_TEX 0

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ): SV_TARGET0
{
	float4 colourSample = textures[ COLOUR_TEX ].Sample( samplerState, fragment.textureUV );

	clip( 0.5f - ( 1 - colourSample.a ) );

	return float4( colourSample.rgb * dLightD * 0.25, 1 );
}
