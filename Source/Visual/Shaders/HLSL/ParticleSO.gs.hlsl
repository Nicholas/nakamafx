/////////////////////////////////////
// Stream Output Geometry Shaders //
///////////////////////////////////

// The stream output GS is just responsible for emitting
// new particles and destroying old particles. The logic
// programmed here will generally vary from particle system
// to particle system, as the destroy/spawn rules will be
// different.

#include "type.Particle.hlsl"
#include "type.gs.cb1.ParticleSystem.hlsl"
#include "algorithms.Noise.hlsl"

//////////////
// GLOBALS //
////////////
cbuffer cbPerFrame : register( b0 )
{
	matrix viewProjection;
	matrix shadowProjection;
	float4 eyePosition;
};

Texture1D textures[ 1 ]: register( t0 );
SamplerState samplerState: register( s0 );


//***********************************************
// HELPER FUNCTIONS *
//***********************************************
//float3 RandVec3( float offset )
//{
//	// Use game time plus offset to sample random texture.
//	float u = gameTime + offset;
//	// coordinates in [-1,1]
//	return textures[ 0 ].SampleLevel( samplerState, u, 0 ).xyz;
//}
float3 RandVec3( float2 offset )
{
	// Use game time plus offset to sample random texture.
	float2 u = gameTime + offset;
	// coordinates in [-1,1]
	//return float3( snoise2D( u + 555 ), snoise2D( u - 549 ), snoise2D( u ) );
	return float3( hash( u + 555 ), hash( u - 549 ), hash( u ) ) * 2 - 1;
}

float3 RandUnitVec3( float2 offset )
{
	float3 v = RandVec3( offset );
	// project onto unit sphere
	return normalize( v );
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
[ maxvertexcount( 2 ) ]
void main( point Particle gIn[ 1 ], inout PointStream<Particle> ptStream )
{
	float t = timeStep;
	gIn[ 0 ].age += t;

	if( gIn[ 0 ].type == PT_EMITTER )
	{
		// time to emit a new particle?
		if( gIn[ 0 ].age > emitTime )
		{
			float3 pRandom =     RandVec3( float2( gIn[ 0 ].initialPosW.x * 80.26, gIn[ 0 ].initialPosW.y * 30.248 ) + gIn[ 0 ].initialPosW.z * 10.84 );
			float3 vRandom = RandUnitVec3( float2( gIn[ 0 ].initialPosW.x * 40.55, gIn[ 0 ].initialPosW.y * 10.213 ) + gIn[ 0 ].initialPosW.z * 90.12 );

			Particle p;
			p.initialPosW = emitPosW + gIn[ 0 ].initialPosW + pRandom * emitPosWScale;
			p.initialVelW = emitDirW + gIn[ 0 ].initialVelW + vRandom * emitDirWScale;

			p.sizeW = gIn[ 0 ].sizeW + flareSize;

			p.id = pRandom.x + pRandom.y + pRandom.z;
			//p.id = 0;
			p.age = 0;
			p.type = PT_FLARE;

			ptStream.Append( p );

			// reset the time to emit
			gIn[ 0 ].age -= emitTime;
			//gIn[ 0 ].age = 0.0f;
		}

		// always keep emitters
		ptStream.Append( gIn[ 0 ] );
	}
	else
	{
		// Specify conditions to keep particle; this may vary
		// from system to system.
		if( gIn[ 0 ].age <= lifeTime )
		{
			// Single direction force
			float3 gAccelW = attractor.xyz;

			// Point mass/gravity force
			if( attractor.w > 0 )
			{
				gAccelW = ( attractor.xyz - gIn[ 0 ].initialPosW );
				float distance = length( gAccelW );
				gAccelW /= ( distance * distance );
				gAccelW *= attractor.w;
			}

			gIn[ 0 ].initialVelW  = gIn[ 0 ].initialVelW + gAccelW * t;
			gIn[ 0 ].initialPosW += gIn[ 0 ].initialVelW * t;

			ptStream.Append( gIn[ 0 ] );
		}
	}
}

/******************************************************************************/
