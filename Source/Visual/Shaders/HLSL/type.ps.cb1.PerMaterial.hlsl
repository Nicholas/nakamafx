
#ifndef _CB_PER_MATERIAL
#define _CB_PER_MATERIAL

cbuffer cbPerMaterial : register( b1 )
{
	float3 ka; float roughness;
	//--------------------------------------------------------------( 16 bytes )
	float3 kd; float metalness;
	//--------------------------------------------------------------( 16 bytes )
	float3 ks; float ssBump;
	//--------------------------------------------------------------( 16 bytes )
	float bumpScaleX;
	float bumpScaleY;
	float ssPower;
	float ssScale;
	//--------------------------------------------------------------( 16 bytes )
	float ior;
	float sAlpha;
	float ssAlpha;
	float absorption;
	//--------------------------------------------------------------( 16 bytes )
	//--------------------------------------------------------------( 16 * 5 = 80 bytes )
};

#endif
