

float3 calculateBumpNormal( 
	in float3 normal,
	in float3 tangent, 
	in float3 binormal,
	in float3 offset,
	in float3 scale )
{
	offset = ( offset * 2.0f ) - 1.0f;

	offset *= scale;

	offset = offset.z * normal
		   + offset.x * tangent
		   - offset.y * binormal;

	return normalize( offset );
}

float3 calculateBumpNormalZ(
	in float3 normal,
	in float3 tangent,
	in float3 binormal,
	in float3 offset,
	in float3 scale )
{
	offset.xy = ( offset.xy * 2.0f ) - 1.0f;

	offset *= scale;

	offset = offset.z * normal
		+ offset.x * tangent
		- offset.y * binormal;

	return normalize( offset );
}