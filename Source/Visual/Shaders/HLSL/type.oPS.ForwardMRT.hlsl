
#ifndef _FORWARD_MRT_OUTPUT
#define _FORWARD_MRT_OUTPUT

struct Pixel
{
	float4 colour: SV_TARGET0;
	float4 normal: SV_TARGET1;
	float4 viewSP: SV_TARGET2;
	float4 F0    : SV_TARGET3;
};

#endif
