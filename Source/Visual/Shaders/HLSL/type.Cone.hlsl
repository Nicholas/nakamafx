

#ifndef _CONE_SHADER_TYPE_
#define _CONE_SHADER_TYPE_

struct Cone
{
    float3 T;   // Cone tip.
    float  h;   // Height of the cone.
    float3 d;   // Direction of the cone.
    float  r;   // bottom radius of the cone.
};

#endif //_CONE_SHADER_TYPE_