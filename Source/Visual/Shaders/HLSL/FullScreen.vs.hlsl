
#include "type.iPS.TexturedFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader - Bufferless Fullscreen
////////////////////////////////////////////////////////////////////////////////
Fragment main( uint id: SV_VertexID )
{
	Fragment fragment;

	fragment.position.x = float( id / 2 ) * 4.0f - 1.0f;
	fragment.position.y = float( id % 2 ) * 4.0f - 1.0f;
	fragment.position.z = 0.0f;
	fragment.position.w = 1.0f;

	fragment.textureUV.x =        float( id / 2 ) * 2.0f;
	fragment.textureUV.y = 1.0f - float( id % 2 ) * 2.0f;

	return fragment;
}
