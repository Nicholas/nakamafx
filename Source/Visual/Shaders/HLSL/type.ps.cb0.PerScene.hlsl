
#ifndef _CB_PER_SCENE
#define _CB_PER_SCENE

cbuffer cbPerScene : register( b0 )
{
	// -------------------------------------------
	// Lighs
	// -------------------------------------------
	// Ambiant
	float4 aLightC;

	// Direcion
	float4 dLightC;
	float3 dLightD;
	float p1;

	// -------------------------------------------
	// Timing
	// -------------------------------------------
	float tick;
	float time;

	// -------------------------------------------
	// Texture Sizes
	// -------------------------------------------
	float targetWidth;
	float targetHeight;
	float2 targetSize;
	float2 shadowSize1;
	float2 shadowSize2;
	float2 zDepths;
};

#endif
