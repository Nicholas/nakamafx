
#include "type.iPS.SkyFragment.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "algorithms.hlsl"

Texture2D skyTexture;
SamplerState samplerState;

struct Pixel
{
	float4 colour: SV_TARGET0;
	float4 normal: SV_TARGET1;
	float4 viewSP: SV_TARGET2;
};

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	float2 uv = toUV( toPolar( normalize( fragment.textureUVW ) ) );
	float3 colour = skyTexture.SampleLevel( samplerState, uv, 0 ).rgb;

	Pixel pixel;
	pixel.colour = float4( colour, 1 );
	pixel.normal = float4( normalize( fragment.textureUVW ), 1 );
	pixel.viewSP = float4( fragment.view.xyz, 0 );

	return pixel;
}
