
#ifndef _CB_PER_SCENE_VS
#define _CB_PER_SCENE_VS

cbuffer cbPerScene : register( b0 )
{
	matrix viewProjectionMatrix;
	//--------------------------------------------------------------( 64 bytes )
	matrix view;
	//--------------------------------------------------------------( 64 bytes )
	matrix shadow1ProjectionMatrix;
	//--------------------------------------------------------------( 64 bytes )
	matrix shadow2ProjectionMatrix;
	//--------------------------------------------------------------( 64 bytes )
	float4 cameraPosition;
	//--------------------------------------------------------------( 16 bytes )
	float gGameTime;
	float gTimeStep;
	float2 perSceneP0;
	//--------------------------------------------------------------( 16 bytes )
	//--------------------------------------------------------------( 16 * 18 = 288 bytes )
};

#endif
