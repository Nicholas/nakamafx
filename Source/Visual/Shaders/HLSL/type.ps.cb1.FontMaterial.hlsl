
#ifndef _CB_FONT_MATERIAL
#define _CB_FONT_MATERIAL

cbuffer cbPerMaterial : register( b1 )
{
	float3 ka; float roughness;
	//--------------------------------------------------------------( 16 bytes )
	float3 kd; float metalness;
	//--------------------------------------------------------------( 16 bytes )
	float3 ks; float ssBump;
	//--------------------------------------------------------------( 16 bytes )
	float2 bumpScale;
	float ssPower;
	float ssScale;
	//--------------------------------------------------------------( 16 bytes )
	float ior;
	float sAlpha;
	float ssAlpha;
	float absorption;
	//--------------------------------------------------------------( 16 bytes )
	uint4 properties;
	//--------------------------------------------------------------( 16 bytes )
	//--------------------------------------------------------------( 16 * 6 = 96 bytes )
};

#endif
