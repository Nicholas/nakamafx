
#ifndef _CB_PER_SCENE_VS
#define _CB_PER_SCENE_VS

cbuffer perScene : register( b0 )
{
	matrix viewProjectionMatrix;
	//--------------------------------------------------------------( 64 bytes )
	matrix view;
	//--------------------------------------------------------------( 64 bytes )
	matrix shadow1ProjectionMatrix;
	//--------------------------------------------------------------( 64 bytes )
	matrix shadow2ProjectionMatrix;
	//--------------------------------------------------------------( 64 bytes )
	float3 cameraPosition; float perSceneP0;
	//--------------------------------------------------------------( 16 bytes )
	float3 pLightP;        float perSceneP1;
	//--------------------------------------------------------------( 16 bytes )
	float3 dLightD;        float perSceneP2;
	//--------------------------------------------------------------( 16 bytes )
	//--------------------------------------------------------------( 16 * 19 = 304 bytes )
};

#endif
