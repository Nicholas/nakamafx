
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"

//////////////
// GLOBALS //
////////////

Texture2D textures[ 3 ] : register( t0 );
Texture2D shadowMap[ 4 ] : register( t3 );
SamplerState samplerState;
SamplerComparisonState cmpSampler;

#define ALBEDO_TEX 0
#define NORMAL_TEX 1
#define HEIGHT_TEX 2

#define SHADOW_1_MAP 0
#define SHADOW_2_MAP 1
#define SHADOW_TD_MAP 2
#define SHADOW_TC_MAP 3

// Forward Clustered Lights
StructuredBuffer<LightCluster> lightGridSRV : register( t7 );
StructuredBuffer< uint >  lightIndexListSRV : register( t8 );
StructuredBuffer<Light>           lightsSRV : register( t9 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	fragment.normal = normalize( fragment.normal );
	fragment.tangent = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view = normalize( fragment.view );

	float4 albedo = pow( abs( textures[ ALBEDO_TEX ].Sample( samplerState, fragment.textureUV ) ), 2.2 );
	float4 normal = textures[ NORMAL_TEX ].Sample( samplerState, fragment.textureUV );
	float4 height = textures[ HEIGHT_TEX ].Sample( samplerState, fragment.textureUV );


	// Use the plane normal to bias the shadow mapping.
	float shadowBiasScale = tan( acos( dot( fragment.normal, dLightD ) ) );


	// Bump Map Normal
	/**/
	float3 sNormal = fragment.normal;
	fragment.normal = calculateBumpNormalZ(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		//normal.rgb,
		float3( normal.rg, 1 ),
		//float3( bumpScaleX, bumpScaleY, 1 )
		float3( bumpScaleX, bumpScaleY, 1 )
	);
	//*/

	// +----------+
	// | Lighting |
	// +----------+
	float min1 = min( shadowSize1.x, shadowSize1.y );
	float min2 = min( shadowSize2.x, shadowSize2.y );


	float shadow1Bias = max( shadowSize1.x, shadowSize1.y ) * 1 * shadowBiasScale + min1 * 0.1f;
	float shadow2Bias = max( shadowSize2.x, shadowSize2.y ) * 2 * shadowBiasScale + min2 * 0.1f;

	float2 shadow1 = getShadowCascade(
		shadowMap[ SHADOW_1_MAP ],
		cmpSampler,
		fragment.shadow1Pos,
		shadowSize1,
		shadow1Bias
	);

	float shadow2 = getShadowWithBias(
		shadowMap[ SHADOW_2_MAP ],
		cmpSampler,
		fragment.shadow2Pos,
		shadowSize2,
		shadow2Bias
	);

	float day = smoothstep( 0.05, 0.3, dot( dLightD, float3( 0, 1, 0 ) ) );
	float shadow = lerp( shadow2, shadow1.r, shadow1.g ) * day;

	float4 shadowColour = getShadowColour(
		shadowMap[ SHADOW_TD_MAP ],
		cmpSampler,
		shadowMap[ SHADOW_TC_MAP ],
		samplerState,
		fragment.shadow1Pos,
		shadow1Bias
	);

	float3 lightColour = lerp( dLightC.rgb, shadowColour.rgb, shadowColour.a ) * shadow;


	float metalness = 0.0f;
	float roughness = 0.8f;
	float ao = 1.0f;


	int clusterIndex = getClusterIndex( fragment.position.xy, fragment.viewPos.z, float2( targetWidth, targetHeight ), zDepths.x, zDepths.y );
	LightCluster cluster = lightGridSRV[ clusterIndex ];

	LightingInfo surface = clusterLighting(
		cluster, lightIndexListSRV, lightsSRV,
		lightColour.rgb, dLightD,
		fragment.worldPos,
		fragment.normal,
		-fragment.view,
		albedo.rgb,
		ks,
		metalness,
		roughness,
		ao
	);




	Pixel pixel;

	pixel.colour = float4( surface.colour, 1 );
	pixel.normal = float4( fragment.normal.rgb, 1 );
	pixel.viewSP = float4( fragment.viewPos, fragment.position.z * fragment.position.w );
	pixel.F0     = float4( surface.reflection, roughness );

	return pixel;
}
