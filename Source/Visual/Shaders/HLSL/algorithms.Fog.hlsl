
#ifndef _ALGORITHMS_FOG_HLSL_
#define _ALGORITHMS_FOG_HLSL_

// Std Fog.
float calculateFog( float distance, float density )
{
	return 1 - exp( -distance * density );
}

// Height Falloff Fog
float calculateFogFalloff( float distance, float3 view, float heightFalloff, float density )
{
	float volumeFogHeightDensityAtViewer = exp( -heightFalloff * view.y );
	float fogInt = distance * volumeFogHeightDensityAtViewer;

	const float slopeTheashold = 0.01f;
	float camToViewY = view.y;
	if( abs( camToViewY ) > slopeTheashold )
	{
		float t = heightFalloff * camToViewY;
		fogInt *= ( 1.0f - exp( -t ) ) / t;
	}

	return 1 - exp( -density * fogInt * 0.1f );
}

#endif // _ALGORITHMS_FOG_HLSL_