
#ifndef _FONT_FRAGMENT
#define _FONT_FRAGMENT

struct Fragment
{
	float4 position  : SV_POSITION;
	float3 textureUV : TEXCOORD0;
};

#endif
