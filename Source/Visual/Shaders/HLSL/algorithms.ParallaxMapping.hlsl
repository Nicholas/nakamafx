



float4 calculateParallaxOcclusion(
	Texture2D hTexture,
	SamplerState samlinear,
	float2 textureUV,
	float2 parallaxOffsetTS,
	float3 normal,
	float3 view,
	int2 g_nSamples )
{
	// Compute all the derivatives:
	float2 dx = ddx( textureUV );
	float2 dy = ddy( textureUV );

	//===============================================//
	// Parallax occlusion mapping offset computation //
	//===============================================//

	// Utilize dynamic flow control to change the number of samples per ray
	// depending on the viewing angle for the surface. Oblique angles require
	// smaller step sizes to achieve more accurate precision for computing displacement.
	// We express the sampling rate as a linear function of the angle between
	// the geometric normal and the view direction ray:
	int nNumSteps = ( int ) lerp( g_nSamples.y, g_nSamples.x, saturate( dot( view, normal ) ) );

	// Intersect the view ray with the height field profile along the direction of
	// the parallax offset ray (computed in the vertex shader. Note that the code is
	// designed specifically to take advantage of the dynamic flow control constructs
	// in HLSL and is very sensitive to specific syntax. When converting to other examples,
	// if still want to use dynamic flow control in the resulting assembly shader,
	// care must be applied.
	//
	// In the below steps we approximate the height field profile as piecewise linear
	// curve. We find the pair of endpoints between which the intersection between the
	// height field profile and the view ray is found and then compute line segment
	// intersection for the view ray and the line segment formed by the two endpoints.
	// This intersection is the displacement offset from the original texture coordinate.

	float fCurrHeight = 0.0;
	float fStepSize = 1.0 / ( float ) nNumSteps;
	float fPrevHeight = 1.0;

	int    nStepIndex = 0;
	bool   bCondition = true;

	float2 vTexOffsetPerStep = fStepSize * parallaxOffsetTS;
	float2 vTexCurrentOffset = textureUV;
	float  fCurrentBound = 1.0;
	float  fParallaxAmount = 0.0;

	float2 pt1 = 0;
	float2 pt2 = 0;

	while( nStepIndex < nNumSteps )
	{
		vTexCurrentOffset -= vTexOffsetPerStep;

		// Sample height map which in this case is stored in the alpha channel of the normal map:
		fCurrHeight = hTexture.SampleGrad( samlinear, vTexCurrentOffset, dx, dy ).x;

		fCurrentBound -= fStepSize;

		if( fCurrHeight > fCurrentBound )
		{
			pt1 = float2( fCurrentBound, fCurrHeight );
			pt2 = float2( fCurrentBound + fStepSize, fPrevHeight );

			nStepIndex = nNumSteps + 1;
		}
		else
		{
			nStepIndex++;
			fPrevHeight = fCurrHeight;
		}
	}

	float fDelta2 = pt2.x - pt2.y;
	float fDelta1 = pt1.x - pt1.y;
	float fDenominator = fDelta2 - fDelta1;

	// SM 3.0 and above requires a check for divide by zero since that operation will generate an 'Inf' number instead of 0
	[flatten]
	if( fDenominator == 0.0f )
	{
		fParallaxAmount = 0.0f;
	}
	else
	{
		fParallaxAmount = ( pt1.x * fDelta2 - pt2.x * fDelta1 ) / fDenominator;
	}

	float2 vParallaxOffset = parallaxOffsetTS * ( 1.0 - fParallaxAmount );
	return float4( textureUV - vParallaxOffset, vTexCurrentOffset );
}



float calculateParallaxOcclusionShadow(
	Texture2D heightMap,
	SamplerState samlinear,
	float2 textureUV,
	float3 lDirTS,
	float3 normal,
	float3 lightDirection,
	int2 g_nSamples,
	float2 vTexCurrentOffset2 )
{
	float2 vTexCurrentOffset = vTexCurrentOffset2;

	// Compute all the derivatives:
	float2 dx = ddx( textureUV );
	float2 dy = ddy( textureUV );

	int nNumSteps = ( int ) lerp( g_nSamples.y, g_nSamples.x, saturate( dot( lightDirection, normal ) ) );

	float shadowMultiplier = 0.0;
	float numSamplesUnderSurface = 0;

	float fCurrHeight = heightMap.SampleGrad( samlinear, vTexCurrentOffset, dx, dy ).x;
	float fStepSize = max( fCurrHeight / ( float ) nNumSteps, 0.01f );



	float2 vParallaxDirection = normalize( -lDirTS.xy );
	float fLength = length( lDirTS );
	float fParallaxLength = sqrt( fLength * fLength - lDirTS.z * lDirTS.z ) / lDirTS.z;

	float2 vTexOffsetPerStep = fStepSize * fParallaxLength * vParallaxDirection * 0.128f;

	float currentLayerHeight = fCurrHeight + fStepSize;
	float heightFromTexture;

	int    nStepIndex = 0;

	while( currentLayerHeight < 1 )
	{
		vTexCurrentOffset -= vTexOffsetPerStep;

		heightFromTexture = heightMap.SampleGrad( samlinear, vTexCurrentOffset, dx, dy ).x;

		if( heightFromTexture > currentLayerHeight )
		{
			numSamplesUnderSurface += 1;

			float newShadowMultiplier = pow( saturate( ( heightFromTexture - currentLayerHeight ) / ( 1 - currentLayerHeight ) ), 1 / 2.8 );
			shadowMultiplier = max( shadowMultiplier, newShadowMultiplier );
		}

		currentLayerHeight += fStepSize;
		nStepIndex++;

		if( nStepIndex == nNumSteps )
		{
			currentLayerHeight = 1;
		}
	}


	// Shadowing factor should be 1 if there were no points under the surface
	[ flatten ]
	if( numSamplesUnderSurface < 1 )
	{
		shadowMultiplier = 1;
	}
	else
	{
		shadowMultiplier = 1.0 - shadowMultiplier;
	}

	return shadowMultiplier;
}