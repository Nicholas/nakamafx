
#include "type.vs.cb0.PerScene.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.Textured3DFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexPT vertex )
{
	Fragment fragment;

	fragment.position   = float4( ( vertex.position ) * 2 + cameraPosition, 1.0f );
	fragment.position   = mul( fragment.position, viewProjectionMatrix );
	fragment.textureUVW = vertex.position;

	return fragment;
}