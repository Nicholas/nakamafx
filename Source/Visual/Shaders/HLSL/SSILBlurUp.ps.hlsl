/////////////
// GLOBALS //
/////////////
cbuffer ssaoBlurCByffer : register( b0 )
{
	// Camera projections for depth testing
	float4x4 view;
	float4x4 projection;

    // Render target texture size
    float4 targetSize;
};

Texture2D gAOTexture : register( t0 );
Texture2D gPositionTexture : register( t1 );
Texture2D gNormalTexture : register( t2 );
Texture2D gSpecularTexture   : register( t3 );

SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState wrapSampler : register( s2 );
SamplerState pointSampler : register( s3 );


static const int blurSize = 4;
static const int offsetSize = 2;
static const float2 offsets[ 2 ][ 4 ] = {
	{
		float2( 0,  2 ), float2( 2,  1 ),
		float2( 0, -1 ), float2( 2, -2 )
	},
	{
		float2( -1,  1 ), float2( 1,  2 ),
		float2( -1, -2 ), float2( 1, -1 )
	}
};


//////////////
// TYPEDEFS //
//////////////
struct Fragment
{
    float4 position : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};

struct Pixel
{
    float4 colour0 : SV_TARGET0;
};

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel mainA( Fragment fragment)
{
    Pixel pixel;
	float4 F0 = gSpecularTexture.Sample( samplerState, fragment.textureUV );
	float depth = gPositionTexture.Sample( pointSampler, fragment.textureUV ).z;

	float3 ao = 0;


	float div = 0.0f;

	for( int x = -2; x < 2; ++x )
	{
		for( int y = -2; y < 2; ++y )
		{
			float2 offset = float2( x, y ) * targetSize.zw;
			float2 uv = fragment.textureUV + offset;

			//float sampleDepth = gPositionTexture.Sample( pointSampler, uv ).z;
			float sampleDepth = gPositionTexture.Sample( samplerState, uv ).z;
			float scale = 1 -  smoothstep( 0, 0.15, abs( depth - sampleDepth ) );

			ao += gAOTexture.Sample( samplerState, uv ).rgb * scale;
			div += scale;
		}
	}
	ao /= max( div, 0.0000001 );

	float3 aoColour = float3( 0, 0, 0 );

//	pixel.colour0.rgb = aoColour;
//	pixel.colour0.a = ( 1 - ao ) * 0.95;
	pixel.colour0.rgb = ao;// * 5;// * F0.rgb;
	pixel.colour0.a = 1.0;

	return pixel;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment): SV_TARGET0
{
	float4 F0 = gSpecularTexture.Sample( pointSampler, fragment.textureUV );
	float depth = gPositionTexture.Sample( pointSampler, fragment.textureUV ).z;
	float roughness = F0.w;

	float2 t = fragment.textureUV * targetSize.xy;
	int i = ( t.x + t.y * targetSize.y ) % offsetSize;

	float4 s = float4( 0.5, 0.5, 0.5, 1.0 );

	float offsetScale = 1.0f;

	float4 colour  = gAOTexture.Sample( samplerState, fragment.textureUV );
	float4 colourA = gAOTexture.Sample( samplerState, fragment.textureUV + offsets[ i ][ 0 ] * offsetScale * targetSize.zw );
	float4 colourB = gAOTexture.Sample( samplerState, fragment.textureUV + offsets[ i ][ 1 ] * offsetScale * targetSize.zw );
	float4 colourC = gAOTexture.Sample( samplerState, fragment.textureUV + offsets[ i ][ 2 ] * offsetScale * targetSize.zw );
	float4 colourD = gAOTexture.Sample( samplerState, fragment.textureUV + offsets[ i ][ 3 ] * offsetScale * targetSize.zw );

//	colour += colourA;
//	colour += colourB;
//	colour += colourC;
//	colour += colourD;

//	float scale = colour.a;

	float sampleDepthA = gPositionTexture.Sample( pointSampler, fragment.textureUV + offsets[ i ][ 0 ] * offsetScale * targetSize.zw ).z;
	float sampleDepthB = gPositionTexture.Sample( pointSampler, fragment.textureUV + offsets[ i ][ 1 ] * offsetScale * targetSize.zw ).z;
	float sampleDepthC = gPositionTexture.Sample( pointSampler, fragment.textureUV + offsets[ i ][ 2 ] * offsetScale * targetSize.zw ).z;
	float sampleDepthD = gPositionTexture.Sample( pointSampler, fragment.textureUV + offsets[ i ][ 3 ] * offsetScale * targetSize.zw ).z;

	float scaleA = 1 -  smoothstep( 0, 0.5, abs( depth - sampleDepthA ) );
	float scaleB = 1 -  smoothstep( 0, 0.5, abs( depth - sampleDepthB ) );
	float scaleC = 1 -  smoothstep( 0, 0.5, abs( depth - sampleDepthC ) );
	float scaleD = 1 -  smoothstep( 0, 0.5, abs( depth - sampleDepthD ) );

	colour += colourA * scaleA;
	colour += colourB * scaleB;
	colour += colourC * scaleC;
	colour += colourD * scaleD;

	//colour.rgb = max( roughness * colour.rgb * F0 * 10, 0 );
	colour.rgb = max( roughness * colour.rgb * 0.1, 0 );

	return float4( colour.rgb, 1 );
}
