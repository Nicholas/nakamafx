/////////////////////////////////
// Billboard Geometry Shaders //
///////////////////////////////

// The draw GS just expands points into camera facing quads.

#include "type.Particle.hlsl"
#include "type.iPS.ParticleFragment.hlsl"
#include "type.gs.cb1.ParticleSystem.hlsl"

//////////////
// GLOBALS //
////////////
cbuffer cbPerFrame : register( b0 )
{
	matrix viewProjection;
	matrix shadowProjection1;
	matrix shadowProjection2;
	float4 eyePosition;
};

Texture1D textures[ 1 ];
SamplerState samplerState;


//***********************************************
// HELPER FUNCTIONS *
//***********************************************
float3 RandVec3( float offset )
{
	// Use game time plus offset to sample random texture.
	float u = offset;
	// coordinates in [-1,1]
	return saturate( textures[ 0 ].SampleLevel( samplerState, u, 0 ).xyz * 0.5f + 0.5f );
}

float3 RandUnitVec3( float offset )
{
	float3 v = RandVec3( offset );
	// project onto unit sphere
	return normalize( v );
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
[ maxvertexcount( 4 ) ]
void main( point Particle gIn[ 1 ], inout TriangleStream< Fragment > triStream )
{
	// do not draw emitter particles.
	if( gIn[ 0 ].type != PT_EMITTER )
	{
		float3 posW = gIn[ 0 ].initialPosW;

		float3 colour1 = pow( RandVec3( gIn[ 0 ].id ), 2 );
		float3 colour2 = RandVec3( gIn[ 0 ].id * 0.62 );

		colour1 = colour1 * starColourScale.rgb;
		colour2 = colour2 * endColourScale.rgb;

		colour1 = lerp( startColour.rgb, colour1, starColourScale.a );
		colour2 = lerp(   endColour.rgb, colour2,  endColourScale.a );

		float3 colour = lerp( colour1, colour2, gIn[ 0 ].age / lifeTime );

		//
		// Compute world matrix so that billboard faces the camera.
		//
		float3 look  = normalize( eyePosition.xyz - posW );
		float3 right = normalize( cross( float3( 0, 1, 0 ), look ) );
		float3 up    = cross( look, right );
		float4x4 W;
		W[ 0 ] = float4( right, 0.0f );
		W[ 1 ] = float4( up   , 0.0f );
		W[ 2 ] = float4( look , 0.0f );
		W[ 3 ] = float4( posW , 1.0f );
		float4x4 WVP = mul( W, viewProjection );

		//
		// Compute 4 triangle strip vertices (quad) in local space.
		// The quad faces down the +z-axis in local space.
		//
		float2 halfSize = 0.5f * ( gIn[ 0 ].sizeW + gIn[ 0 ].age * ageLifeSizeScale );
		float4 v[ 4 ];
		v[ 0 ] = float4( -halfSize.x, -halfSize.y, 0.0f, 1.0f );
		v[ 1 ] = float4( +halfSize.x, -halfSize.y, 0.0f, 1.0f );
		v[ 2 ] = float4( -halfSize.x, +halfSize.y, 0.0f, 1.0f );
		v[ 3 ] = float4( +halfSize.x, +halfSize.y, 0.0f, 1.0f );


		float2 gQuadTexC[ 4 ] =
		{
			float2( 1.0f, 1.0f ),
			float2( 0.0f, 1.0f ),
			float2( 1.0f, 0.0f ),
			float2( 0.0f, 0.0f )
		};
		//
		// Transform quad vertices to world space and output
		// them as a triangle strip.
		//
		Fragment gOut;

		[unroll]
		for( int i = 0; i < 4; ++i )
		{
			gOut.posH = mul( v[ i ], WVP );
			gOut.texC = gQuadTexC[ i ];
			gOut.colour.rgb = colour;
			gOut.colour.a   = 1;
			gOut.age = gIn[ 0 ].age;
			triStream.Append( gOut );
		}
	}
}

