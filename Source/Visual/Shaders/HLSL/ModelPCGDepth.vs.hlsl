
#include "algorithms.Noise.hlsl"
#include "type.vs.cb0.PerSceneDepth.hlsl"
#include "type.vs.cb1.PerModel.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.DepthFragment.hlsl"

float nmlz( float v )
{
	return ( v + 1 ) * 0.5;

}

float getSampleHeight( float2 uv, float power, float scale )
{
	float cellScale = 0.015f;

	float2 sampleUV = uv * cellScale;

	float height  = nmlz( snoise2D( sampleUV * 0.125 ) );
		  height += ( snoise2D( sampleUV * 0.25 ) ) * 1;
		  height += ( snoise2D( sampleUV * 1 ) ) * 0.1;
		  height  = pow( height, power );

		  height *= scale;
	return height;
}


float getSampleHeight( float2 uv )
{
	return getSampleHeight( uv, 2, 25 ) - 1.2;
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexPTNTB vertex )
{
	Fragment fragment;

	float4 worldPosition = mul( float4( vertex.position, 1.0f ), model );

	worldPosition.y += getSampleHeight( worldPosition.xz );

	fragment.position = mul( worldPosition, viewProjectionMatrix );

	//fragment.position = mul( float4( vertex.position, 1.0f ), model );
	//fragment.position = mul( fragment.position, viewProjectionMatrix );

	return fragment;
}