
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.Font2DFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader - Pass Through
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexChar characture, uint vertexID : SV_VertexID )
{
	Fragment fragment;

	// vert id 0 = 0000, uv = (0, 0)
	// vert id 1 = 0001, uv = (1, 0)
	// vert id 2 = 0010, uv = (0, 1)
	// vert id 3 = 0011, uv = (1, 1)
	float2 uv = float2(vertexID & 1, (vertexID >> 1) & 1);

	// set the position for the vertex based on which vertex it is (uv)
	fragment.position = float4( characture.position.x + (characture.position.z * uv.x),
								1 - ( characture.position.y - ( characture.position.w * uv.y ) ),
								0, 1 );
	//output.color = input.color;

	// set the texture coordinate based on which vertex it is (uv)
	fragment.textureUV = float3( characture.textureUV.x + (characture.textureUV.z * uv.x),
								 1 - ( characture.textureUV.y + ( characture.textureUV.w * uv.y ) ),
								 characture.id );

//	fragment.position = float4( uv - 0.5f, 0, 1 );
	//fragment.position.xy *= 0.05f;
	fragment.position.xy *= 0.07f;
	fragment.position.y *= 16.0f / 9.0f;
	//fragment.textureUV = uv;
	fragment.position.xy += float2( -0.9, 0.9 );

	return fragment;
}
