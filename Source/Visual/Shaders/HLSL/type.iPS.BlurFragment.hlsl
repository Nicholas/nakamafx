
#ifndef _BLUR_FRAGMENT
#define _BLUR_FRAGMENT

struct Fragment
{
	float4 position  : SV_POSITION;
	float2 texCoord1 : TEXCOORD0;
	float2 texCoord2 : TEXCOORD1;
	float2 texCoord3 : TEXCOORD2;
	float2 texCoord4 : TEXCOORD3;
	float2 texCoord5 : TEXCOORD4;
	float2 texCoord6 : TEXCOORD5;
	float2 texCoord7 : TEXCOORD6;
	float2 texCoord8 : TEXCOORD7;
	float2 texCoord9 : TEXCOORD8;
};

#endif
