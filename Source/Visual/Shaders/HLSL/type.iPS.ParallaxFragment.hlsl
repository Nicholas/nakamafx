
#ifndef _PARALLAX_FRAGMENT
#define _PARALLAX_FRAGMENT

struct Fragment
{
	float4 position  : SV_POSITION;

	float3 normal	 : NORMAL;
	float3 tangent   : TANGENT;
	float3 binormal  : BINORMAL;

	float2 textureUV : TEXCOORD;

	float3 view		 : TEXCOORD1;
	float3 worldPos	 : TEXCOORD2;
	float4 shadow1Pos: TEXCOORD3;
	float4 shadow2Pos: TEXCOORD4;

	float3 lDirTS	 : TEXCOORD5;
	float2 parallaxOffsetTS : TEXCOORD6;

	float3 viewPos	 : TEXCOORD7;
	//float3 viewNorm	 : TEXCOORD8;
};

#endif
