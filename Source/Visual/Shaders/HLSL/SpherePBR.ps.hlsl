
//#define SPECULAR_BRDF_GGX
#define SPECULAR_BRDF_Schlick
//#define SPECULAR_BRDF_Beckmann

#include "algorithms.Lighting.hlsl"
#include "algorithms.NormalMapping.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"


//////////////
// GLOBALS //
////////////
TextureCube reflectionTexture : register( t0 );
Texture2D       albedoTexture : register( t1 );
Texture2D       normalTexture : register( t2 );
Texture2D       heightTexture : register( t3 );
Texture2D    roughnessTexture : register( t4 );
Texture2D    metalnessTexture : register( t5 );
Texture2D    occlusionTexture : register( t6 );

SamplerState anisoSampler : register( s0 );

//////////////////////////
// Sphere Construction //
////////////////////////
// Modify Normal, Tangent and Bitangent to map to the sphere.
float3 modNormal( float3 normal, float3 tangent, float3 bitangent, float3 offset )
{
	float3 result;
	result = offset.x * tangent
		   - offset.y * bitangent
		   + offset.z * normal;

	return normalize( result );
}

void buildSphereNormalTangentSpace( inout Fragment fragment, in float2 dist, in float radius )
{
	fragment.normal = modNormal(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		float3( dist.x, dist.y, sqrt( 1 - radius ) ) );

	fragment.tangent  = normalize( cross( fragment.normal, float3( 0.0, 1.0, 0.0 ) ) );
	fragment.binormal = normalize( cross( fragment.normal, fragment.tangent ) );
}

/////////////////////
// Sphere Mapping //
///////////////////
static const float PI = 3.1415926536f;
float2 sphereMapTextureUV( float3 normal, out float2 dirivX, out float2 dirivY, float scale )
{
	float y = acos( normal.y ) / PI;

	float x1 = atan2( normal.z, normal.x );
	float x2 = atan2( normal.z, abs( normal.x ) );

	x1 = saturate( ( ( x1 + PI ) / ( PI * 2 ) ) );
	x2 = saturate( ( ( x2 + PI ) / ( PI * 2 ) ) );

	float2 textureUV2 = float2( x2, y ) * scale;
	dirivX = ddx( textureUV2 );
	dirivY = ddy( textureUV2 );

	return float2( x1, y ) * scale;
}



float3 calculatePBLight(
	in float3 position,
	in float3 albido,
	in float3 ks,
	in float3 N,
	in float3 V,
	in float metalness,
	in float roughness,
	out float3 reflectionFactor
	)
{
		 roughness = roughness * 0.9f + 0.1f;
	float3      F0 = calculateF0( albido.rgb, ks, metalness );
	float3 diffuse = lambartDiffuseBDRF( albido.rgb, metalness );

	////////////////////////
	// Reflection Factor //
	//////////////////////
	reflectionFactor = fresnelSchlick( F0, saturate( dot( N, V ) ) ) * ( 1 - roughness );


	float3 colour = float3( 0, 0, 0 );

	/////////////////
	// LOOP LIGHT //
	///////////////
	// Direction
	colour += calculateLightBRDF( dLightC.rgb, dLightD, V, N, diffuse, F0, roughness );

	return colour;
}


float4 brdf( inout Fragment fragment )
{
	float2 dirivX;
	float2 dirivY;
	float2 textureUV = sphereMapTextureUV( fragment.normal, dirivX, dirivY, 4 );
	textureUV.x += time;

	float rotate360 = dot( fragment.view, float3( -1, 0, 0 ) ) > 0 ? 1 : 0;
	textureUV.x += rotate360;

	float4 albido     =    albedoTexture.SampleGrad( anisoSampler, textureUV, dirivX, dirivY );
	float4 normal     =    normalTexture.SampleGrad( anisoSampler, textureUV, dirivX, dirivY );
	float4 roughnessT = roughnessTexture.SampleGrad( anisoSampler, textureUV, dirivX, dirivY );
	float4 metalnessT = metalnessTexture.SampleGrad( anisoSampler, textureUV, dirivX, dirivY );
	float4 occlusion  = occlusionTexture.SampleGrad( anisoSampler, textureUV, dirivX, dirivY );

	float roughness = max( roughnessT.r, roughnessT.g );
	float metalness = metalnessT.g;

	fragment.normal = calculateBumpNormal(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		normal.rgb,
		float3( bumpScaleX, bumpScaleY, 1 ) );

	float3 reflectionFactor = float3( 0, 0, 0 );

	float3 fragmentColour = calculatePBLight(
		fragment.worldPos,
		albido.rgb,
		ks,
		fragment.normal,
		-fragment.view,
		metalness,
		roughness,
		reflectionFactor
	);

	/////////////////
	// REFLECTION //
	///////////////
	float3 reflectionDir = reflect( fragment.view, fragment.normal );
	float3 reflection = reflectionTexture.SampleBias( anisoSampler, reflectionDir, ( roughness ) * 8 ).rgb * 10;
	fragmentColour += reflection * reflectionFactor;


	//////////////
	// AMBIANT //
	////////////
	float3 am = reflectionTexture.SampleBias( anisoSampler, fragment.normal, 15 ).rgb * 3;
	fragmentColour += am * occlusion.r * albido.rgb * ( ks * ( 1 - metalness ) );

	return float4( fragmentColour, 1 );
}

//////////////////////
// TESTING & TRIAL //
////////////////////
float4 myTestingLighting( inout Fragment fragment )
{
	float2 dirivX;
	float2 dirivY;
	float2 textureUV = sphereMapTextureUV( fragment.normal, dirivX, dirivY, 3 );

	float4 albido = albedoTexture.Sample( anisoSampler, textureUV );
	float4 normal = normalTexture.Sample( anisoSampler, textureUV );
	float4 rougness = roughnessTexture.Sample( anisoSampler, textureUV );

	// Bump Map Normal
	fragment.normal = calculateBumpNormal(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		normal.rgb,
		float3( bumpScaleX, bumpScaleY, 1 )
	);

	float3 ambiantLight = ambiantLighting(
		aLightC.rgb,
		ka,
		1.0f
	);

	float3 directionLight = directionLighting(
		dLightC.rgb,
		dLightD,
		fragment.normal,
		fragment.view,
		0.0,
		kd, ks, 16//ns
	);

	float3 light = directionLight;

	float blur = saturate( min( ( 2.9 - ( rougness.g * 3.0 ) ), 1 - rougness.r ) );

	float4 reflection = reflectionTexture.SampleBias( anisoSampler, fragment.normal, blur * 6 ) * 10;

	reflection *= pow( 1 - dot( -fragment.view, fragment.normal ), 3 ) * 0.4 + 0.04;

	return float4( light.rgb * albido.rgb + reflection.rgb, 1 );
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	fragment.normal   = normalize( fragment.normal );
	fragment.tangent  = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view     = normalize( fragment.view );

	float2 dist = fragment.textureUV * 2 - 1;
	float radius = dist.x * dist.x + dist.y * dist.y;

	clip( 0.95 - radius );

	buildSphereNormalTangentSpace( fragment, dist, radius );

	float4 colour = brdf( fragment );

	Pixel pixel;

	pixel.colour = colour;
	pixel.normal = float4( fragment.normal, 1 );
	pixel.viewSP = float4( 0, 0, 0, 1 );
	pixel.F0     = float4( 0, 0, 0, 1 );

	return pixel;
}
