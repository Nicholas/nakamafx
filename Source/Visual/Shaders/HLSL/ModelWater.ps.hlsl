
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.NormalMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"



//////////////
// GLOBALS //
////////////
//cbuffer cbConstants
//{
//	const int g_nMinSamples = 20;
//	const int g_nMaxSamples = 500;
//}
#define g_nMinSamples 1
#define g_nMaxSamples 10

Texture2D textures[ 7 ] : register( t0 );
SamplerState samplerState;
SamplerComparisonState cmpSampler;
SamplerState mirrorSampler;
SamplerState pointSampler;

#define water1 0
#define water2 1
#define refraction 2
#define Shadow1Map 3
#define Shadow2Map 4
#define ShadowTDMap 5
#define ShadowTCMap 6

StructuredBuffer<LightCluster> lightGridSRV : register( t7 );
StructuredBuffer< uint >  lightIndexListSRV : register( t8 );
StructuredBuffer<Light>           lightsSRV : register( t9 );


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	float2 water1Scale = float2( 8,  8 );
	float2 water2Scale = float2( 4, -4 );

	float3 lPosition = fragment.position.xyz;
	lPosition.xy *= targetSize;

	float depth = textures[ refraction ].Sample( pointSampler, lPosition.xy ).a;
	float depthDiff = depth - fragment.viewPos.z;

	clip( depthDiff );

	fragment.normal = normalize( fragment.normal );
	fragment.tangent = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view = normalize( fragment.view );

	float shadowBiasScale = tan( acos( dot( fragment.normal, dLightD ) ) );

	float3 mapNormal = textures[ water1 ].Sample( samplerState, fragment.textureUV * water1Scale + time ).rgb
					 + textures[ water2 ].Sample( samplerState, fragment.textureUV * water2Scale - time ).rgb;

	mapNormal *= 0.5f;

	//float3 surfaceNormal = fragment.normal;
	float3 surfaceNormal = calculateBumpNormalZ(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		mapNormal,
		float3( 1.0, -1.0, 1.0 )
	);

	//float3 reflectionNormal = fragment.normal;
	float3 reflectionNormal = calculateBumpNormalZ(
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		float3( mapNormal.xyz ),
		float3( 0.25, -0.25, 10.0 )
	);

	float refractionOffset = smoothstep( 0.002f, 1.0f, depthDiff ) * 0.02;

	float2 refPoint;
	refPoint = lPosition.xy + surfaceNormal.xz * refractionOffset;

	float4 refractionColour = textures[ refraction ].Sample( mirrorSampler, refPoint );
	float refractionDepth   = refractionColour.a;

	if( refractionDepth < fragment.viewPos.z + 0.005f )
	{
		//refractionColour.rgb = float3( 1, 0, 0 );
		refractionColour = textures[ refraction ].Sample( mirrorSampler, lPosition.xy - surfaceNormal.xz * refractionOffset * saturate( 1 - fragment.viewPos.z - refractionDepth ) );
		refractionDepth  = refractionColour.a;
	}

	float fog = smoothstep( 0.002f, 1.0f, refractionDepth - fragment.viewPos.z );
		  fog = saturate( ( 1 - pow( 1 - fog, 2 ) ) );

	float ligthEdgeScale = smoothstep( 0.005f, 0.1f, depthDiff );

	float heightOffset = saturate( dot( float3( 0, 1, 0 ), surfaceNormal ) );
	float roughOffset = 1 - smoothstep( 0.005f, 0.5f, depthDiff );
	float roughness = 0.1f + roughOffset * heightOffset * 0.9;



	float min1 = min( shadowSize1.x, shadowSize1.y );
	float min2 = min( shadowSize2.x, shadowSize2.y );

	float shadow1Bias = max( shadowSize1.x, shadowSize1.y ) * 1 * shadowBiasScale + min1 * 0.1f;
	float shadow2Bias = max( shadowSize2.x, shadowSize2.y ) * 2 * shadowBiasScale + min2 * 0.1f;

	//float4 shadowOffset = float4( surfaceNormal.xz, 0, 0 ) * 0.01f;
	float4 shadowOffset = float4( surfaceNormal.xz, 0, 0 ) * 0.01f;

	float2 shadowFactor1 = getShadowCascade(
		textures[ Shadow1Map ],
		cmpSampler,
		fragment.shadow1Pos + shadowOffset,
		shadowSize1,
		shadow1Bias
	);

	float shadowFactor2 = getShadowWithBias(
		textures[ Shadow2Map ],
		cmpSampler,
		fragment.shadow2Pos + shadowOffset,
		shadowSize2,
		shadow2Bias
	);

	float shadowFactor = lerp( shadowFactor2, shadowFactor1.x, shadowFactor1.y );

	// Turn light off a sun set...
	shadowFactor *= smoothstep( 0.05, 0.3, dot( dLightD, float3( 0, 1, 0 ) ) );


	float4 shadowColour = getShadowColour(
		textures[ ShadowTDMap ], cmpSampler,
		textures[ ShadowTCMap ], samplerState,
		fragment.shadow1Pos + shadowOffset,
		shadow1Bias );


	float3 lightColour = lerp( dLightC.rgb, shadowColour.rgb, shadowColour.a ) * shadowFactor;


	float metalness = 0.95f;
	float ao = 1.0f;

	int clusterIndex = getClusterIndex( fragment.position.xy, fragment.viewPos.z, float2( targetWidth, targetHeight ), zDepths.x, zDepths.y );
	LightCluster cluster = lightGridSRV[ clusterIndex ];

	LightingInfo surface = clusterLighting(
		cluster, lightIndexListSRV, lightsSRV,
		lightColour.rgb, dLightD,
		fragment.worldPos,
		surfaceNormal,
		-fragment.view,
		float3( 0.8, 0.9, 1.0 ),
		float3( 0.02f, 0.02f, 0.02f ), //ks
		metalness,
		roughness,
		ao
	);


	Pixel pixel;
	pixel.colour = refractionColour * ( 1 - ( fog * float4( 0.75, 0.6, 0.3, 1.0 ) ) );
	pixel.colour.rgb += surface.colour * ( ligthEdgeScale * saturate( heightOffset ) );
	pixel.colour.a = 1.0;


	pixel.normal = float4( reflectionNormal, 1.0 );
	pixel.viewSP = float4( fragment.viewPos, 1.0 );
	pixel.F0     = float4( surface.reflection, roughness );

	return pixel;
}

