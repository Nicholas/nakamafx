
#include "type.iPS.UIFragment.hlsl"

/////////////
// GLOBALS //
/////////////
Texture2D image : register( t0 );
SamplerState samplerState : register( s0 );

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader - Texture Sample
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ) : SV_Target0
{
	float4 texel = image.Sample( samplerState, fragment.textureUV );
	return fragment.colour * texel;
}
