
#include "type.vs.cb0.PerSceneDepth.hlsl"
#include "type.vs.cb1.PerModel.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.DepthFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexPTNTB vertex )
{
	Fragment fragment;

	fragment.position = mul( float4( vertex.position, 1.0f ), model );
	fragment.position = mul( fragment.position, viewProjectionMatrix );

	return fragment;
}