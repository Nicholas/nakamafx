

#ifndef _FRUSTUM_SHADER_TYPE_
#define _FRUSTUM_SHADER_TYPE_

#include "type.Plane.hlsl"

// A view frustrum for a cluster in view space.
// Six planes define the left, right top,
// bottom, front and back of the clusters frustum
#define   LEFT_PLANE 0
#define  RIGHT_PLANE 1
#define    TOP_PLANE 2
#define BOTTOM_PLANE 3

#define PLANE_COUNT 4

struct Frustum
{
    Plane planes[ 4 ];
    float zNear;
    float zFar;
};

#endif //_FRUSTUM_SHADER_TYPE_