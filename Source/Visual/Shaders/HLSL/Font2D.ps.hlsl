
#include "type.iPS.Font2DFragment.hlsl"


/////////////
// GLOBALS //
/////////////
Texture2DArray msdfImage : register( t0 );
SamplerState samplerState : register( s0 );

/// Bicubic interpolation sampling of a texture.
float4 bicubicInterpolationFast(
	in Texture2DArray image,
	in SamplerState imageSampler,
	in float3 uv,
	in uint mipLevel
)
{
	float4 texSize = float4( 0, 0, 0, mipLevel );

	image.GetDimensions( texSize.x, texSize.y, texSize.z );

	float2 rec_nrCP = 1.0 / texSize.xy;
	float2 coord_hg = uv.xy * texSize.xy - 0.5; // TODO: Chech to see if this is correct in dx11
	float2 index = floor( coord_hg );

	float2 f = coord_hg - index;
	float4x4 M = {
		-1,  3, -3,  1,
		 3, -6,  3,  0,
		-3,  0,  3,  0,
		 1,  4,  1,  0
	};
	M /= 6;

	float4 wx = mul( float4( f.x*f.x*f.x, f.x*f.x, f.x, 1 ), M );
	float4 wy = mul( float4( f.y*f.y*f.y, f.y*f.y, f.y, 1 ), M );
	float2 w0 = float2( wx.x, wy.x );
	float2 w1 = float2( wx.y, wy.y );
	float2 w2 = float2( wx.z, wy.z );
	float2 w3 = float2( wx.w, wy.w );

	float2 g0 = w0 + w1;
	float2 g1 = w2 + w3;
	float2 h0 = w1 / g0 - 1;
	float2 h1 = w3 / g1 + 1;

	float2 coord00 = index + h0;
	float2 coord10 = index + float2( h1.x, h0.y );
	float2 coord01 = index + float2( h0.x, h1.y );
	float2 coord11 = index + h1;

	coord00 = ( coord00 + 0.5 ) * rec_nrCP;
	coord10 = ( coord10 + 0.5 ) * rec_nrCP;
	coord01 = ( coord01 + 0.5 ) * rec_nrCP;
	coord11 = ( coord11 + 0.5 ) * rec_nrCP;

	float4 tex00 = image.SampleLevel( imageSampler, float3( coord00, uv.z ), texSize.a );
	float4 tex10 = image.SampleLevel( imageSampler, float3( coord10, uv.z ), texSize.a );
	float4 tex01 = image.SampleLevel( imageSampler, float3( coord01, uv.z ), texSize.a );
	float4 tex11 = image.SampleLevel( imageSampler, float3( coord11, uv.z ), texSize.a );

	tex00 = lerp( tex01, tex00, g0.y );
	tex10 = lerp( tex11, tex10, g0.y );

	return lerp( tex10, tex00, g0.x );
}

float median( float r, float g, float b )
{
	return max( min( r, g ), min( max( r, g ), b ) );
}

float getMSDFEdge( float3 msdf, float2 msdfUnit, float2 uv, float offset )
{
	msdf -= 0.5f - offset;
	float sdf = median( msdf.r, msdf.g, msdf.b );


	sdf *= dot( msdfUnit, 0.5f / fwidth( uv ) );

	return saturate( sdf + 0.5f );
}

float getSDFEdge( float sdf, float2 msdfUnit, float2 uv, float offset )
{
	sdf -= 0.5f - offset;
	sdf *= dot( msdfUnit, ( 0.5f ) / fwidth( uv ) );

	return saturate( sdf + 0.5f );
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader - Texture Sample
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment ): SV_Target0
{
	// Bicubic looks nice when we start going really big, otherwise it looks the same as linear.
	//float4 msdfSample = bicubicInterpolationFast( msdfImage, samplerState, fragment.textureUV, 0 );
	float4 msdfSample = msdfImage.Sample( samplerState, fragment.textureUV );

	uint3 textureSize;
	msdfImage.GetDimensions( textureSize.x, textureSize.y, textureSize.z );

	float pxRange    = 2.5f;
	float msdfOffset = 0.0f;
	float  sdfOffset = 0.15f;

	float2 msdfUnit = pxRange / textureSize.xy;

	float msdf = getMSDFEdge( msdfSample.rgb, msdfUnit, fragment.textureUV.xy, msdfOffset );
	float  sdf = getSDFEdge( msdfSample.a, msdfUnit * 0.5f, fragment.textureUV.xy, sdfOffset );

	float3 colour = lerp( float3( 0, 0, 0 ), float3( 1.0, 1.0, 1.0 ),  msdf );

	clip( sdf - 0.01 );

	return float4( colour, max( sdf, msdf ) );
}
