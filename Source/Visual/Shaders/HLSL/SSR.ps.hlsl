/////////////
// GLOBALS //
/////////////
cbuffer ssrCByffer : register( b0 )
{
	// Camera projections for depth testing
	float4x4 view;
	float4x4 projection;

	// Render target texture size
	float4 targetSize;
};

#define REFECTION_SAMPLE_STEP 0.015f
#define REFECTION_SAMPLE_SIZE 128
#define KERNEL_SIZE 4

tbuffer samplesTBuffer : register( t0 )
{
	float3 samples[ KERNEL_SIZE ];
}


Texture2D gPosition : register( t1 );
Texture2D gNormal   : register( t2 );
Texture2D gDepth    : register( t3 );
Texture1D Noise     : register( t4 );

SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState wrapSampler : register( s2 );
SamplerState pointSampler : register( s3 );


//////////////
// TYPEDEFS //
//////////////
struct Fragment
{
	float4 position : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};

struct Pixel
{
	float4 colour0 : SV_TARGET0;
};


float3 getPositionSS( float3 positionWS )
{
	// Update the sample position.
	float4 currentUV = mul( projection, float4( positionWS, 1 ) );

	currentUV.xy /= currentUV.w;
	currentUV.xy *= float2( 0.5f, -0.5f );
	currentUV.xy += 0.5f;

	return currentUV.xyz;
}

float4 getReflection(
	Texture2D gPositionVS,
	Texture2D gNormalWS,
	SamplerState depthSampler,
	float3 positionVS,
	float3 normalVS,
	float3 R )
{
	float DepthCheckBias = 0.001f;

	float3 currentPosition = positionVS;

	float3 currentUV = getPositionSS( positionVS );
	float2 lastUV = 0;
	float lastDepth = 0;

	float steSize = REFECTION_SAMPLE_STEP;
	float currentLength = 0.004 + smoothstep( 1, 24, currentUV.z );// + smoothstep( 46, 94, currentUV.z );

	bool searchingForReflection = true;

	float4 uv2a = float4( -1, -1, 0, 0 );

	[ unroll ]
	for( int i = 0; i < REFECTION_SAMPLE_SIZE; i++ )
	{
		if( searchingForReflection )
		{
			lastDepth = currentPosition.z;
			lastUV = currentUV.xy;

			// Make step along the reflection vector.
			currentPosition = positionVS + R * currentLength;

			// Update the sample position.
			currentUV = getPositionSS( currentPosition );

			// Get the the depth at the samples position.
			float3 sampleDepth = gPositionVS.SampleLevel( depthSampler, currentUV.xy, 0 ).xyz;


			// Get a few samples...
			// This is a way to try and get a smoother reflection.
			// There are several other ways of trying to do this to get good results...
			float maxD = sampleDepth.z;
			[ unroll ]
			for( int j = 0; j < KERNEL_SIZE; j++ )
			{
				if( abs( currentPosition.z - sampleDepth.z ) < ( 0.001f * pow( i, 1.1 ) ) )
				{
					//searchingForReflection = false;
					i = REFECTION_SAMPLE_SIZE;

					uv2a.rg = currentUV.rg;
					uv2a.b = length( uv2a.rg - lastUV );
					uv2a.a = 1;
				}

				float2 offset = samples[ j ].xy * targetSize.zw;
				sampleDepth = gPositionVS.SampleLevel( depthSampler, currentUV.xy + offset, 0 ).xyz;
				maxD = max( sampleDepth.z, maxD );
			}

			float3 updatePosition = float3( currentPosition.xy, sampleDepth.z );
			currentLength += max( ( maxD - currentPosition.z ) * 0.1, 0.025 );
		}
	}

	float3 normal = gNormalWS.SampleLevel( depthSampler, currentUV.xy, 0 ).xyz;
		   normal = normalize( mul( view, float4( normal, 0 ) ).xyz );

	uv2a.a *= smoothstep( -0.37, -0.01, dot( normal, -R ) );

	return uv2a;
}


/*
// Journal of Computer Graphics TechniquesEfficient GPU Screen - Space Ray Tracing
// Vol. 3, No. 4, 2014
// http://jcgt.org
float distanceSquared( float2 a, float2 b )
{
	a -= b;
	return dot( a, a );
}

float2 toNDC( float2 ndc, float2 screenSize )
{
	float2 screenCoord;
	screenCoord.x =          ( ndc.x + 1.0f )   * 0.5f;
	screenCoord.y = ( 1.0f - ( ndc.y + 1.0f ) ) * 0.5f;
	return screenCoord * screenSize;
}


float4 getReflection2DMarch(
	Texture2D gPositionVS,		// View Space GBuffer
	//SamplerState depthSampler,	// Texture Sampler
	float3 originVS,			// Ray Origin
	float3 rayVS,				// Ray Direction
	matrix proj,				// Screen Space [ (0,0), (maxX,maxY) ] Projection
	float4 depthBufferSize,		// The depth buffer dimensions
	float depthThickness,		// Depth buffer pixel view space depth/size
	float nearPlane,			// The camera near projection plane
	float stride,				// The pixel sampling stride
	float jitter,				// [ 0, 1 ] value to try conceal banding
	const float maxSteps,		// Maximum number of iterations
	const float maxDistance		// Maximum distance to fetch a reflection
	)
{
	// Near Plane Clipping
	//float rayLength = ( originVS.z + rayVS.z * maxDistance ) > nearPlane
	//				? ( nearPlane - originVS.z ) / rayVS.z
	//				: maxDistance;
	float rayLength = maxDistance;

	float3 endPoint = originVS + rayVS * rayLength;


	// Project in to homogeneous clip space
	float4 H0 = mul( proj, float4( originVS, 0.0f ) );
	float4 H1 = mul( proj, float4( endPoint, 0.0f ) );

	float k0 = 1.0f / H0.w;
	float k1 = 1.0f / H1.w;

	// The interpolated homogeneous version of the camera-space points
	float3 Q0 = originVS * k0;
	float3 Q1 = endPoint * k1;

	// Screen Space End Points
	float2 P0 = H0.xy * k0;
	float2 P1 = H1.xy * k1;

	P0 = toNDC( P0, depthBufferSize.xy );
	P1 = toNDC( P1, depthBufferSize.xy );

	// If the line is degenerate, make it cover at least one pixel
	// to avoid handling zero-pixel extent as a special case later
	P1 += distanceSquared( P0, P1 ) < 0.0001 ? 0.01 : 0.0;
	float2 delta = P1 - P0;

	// Permute so that the primary iteration is in x to collapse
	// all quadrant-specific DDA cases later
	bool permute = false;

	if( abs( delta.x ) < abs( delta.y ) )
	{
		permute = true;
		delta = delta.yx;
		P0 = P0.yx;
		P1 = P1.yx;
	}

	float stepDirection = sign( delta.x );
	float invdx = stepDirection / delta.x;
//	float invdx = 1 / length( delta );

	// Track the derivatives of Q and k
	float3 dQ = ( Q1 - Q0 ) * invdx;
	float  dk = ( k1 - k0 ) * invdx;
	float2 dP = float2( stepDirection, delta.y * invdx );
//	float2 dP =    delta    * invdx;

	// Scale derivatives by the desired pixel stride
	dP *= stride;
	dQ *= stride;
	dk *= stride;

	// Offset the starting values by the jitter fraction
	P0 += dP * jitter;
	Q0 += dQ * jitter;
	k0 += dk * jitter;

	// Slide P from P0 to P1, (now-homogeneous) Q from Q0 to Q1, k from k0 to k1
	float3 Q = Q0;

	// Adjust end condition for iteration direction
	float end = P1.x * stepDirection;

	float k = k0;
	float stepCount = 0;
	float prevZMaxEstimate = originVS.z;
	float rayZMin = prevZMaxEstimate;
	float rayZMax = prevZMaxEstimate;
	float sampleZ = rayZMax + 100;


	float4 uv2a = 0;

	for( float2 P = P0;
		 ( ( P.x * stepDirection ) <= end ) &&
		 ( stepCount < maxSteps ) &&
		 (	( rayZMax > sampleZ - depthThickness ) ||
			( rayZMin < sampleZ ) ) &&
		 ( sampleZ != 0 );
		 P   -= dP,
		 Q.z -= dQ.z,
		 k   -= dk,
		 ++stepCount )

	//for( float2 P = P0;
	//	( ( P.x * stepDirection ) <= end ) &&
	//	 ( stepCount < maxSteps ) &&
	//	 ( sampleZ > rayZMax + depthThickness )  &&
	//	   ( sampleZ != 0 );
	//	 P   += dP,
	//	 Q.z += dQ.z,
	//	 k   += dk,
	//	 ++stepCount )

	//for( float2 P = P0;
	//	 ( ( P.x * stepDirection ) <= end ) &&
	//	 ( stepCount < maxSteps ) &&
	//	 ( sampleZ != 0 );
	//	 P   += dP,
	//	 Q.z += dQ.z,
	//	 k   += dk,
	//	 ++stepCount )
	{
		rayZMin = prevZMaxEstimate;
		rayZMax = ( dQ.z * 0.5f + Q.z ) / ( dk * 0.5 + k );
		//rayZMax = ( Q.z + dQ.z * stepCount ) / ( k0 + dk * stepCount );

		prevZMaxEstimate = rayZMax;

		if( rayZMin > rayZMax )
		{
			float t = rayZMin;
			rayZMin = rayZMax;
			rayZMax = t;
		}

		//uv2a.xy = P;
		uv2a.xy = permute ? P.yx : P;


		sampleZ = gPositionVS.Load( int3( uv2a.xy, 0 ) ).z;
	}

	// Advance Q based on the number of steps
	//Q.xy += dQ.xy * stepCount;
	//uv2a.xy = Q * ( 1.0f / k );

	uv2a.xy *= depthBufferSize.zw;

	//uv2a.xy = 1 - invdx;
	//uv2a.xy = 1 - invdx;

	//uv2a.xy = ( 1 + dP.rg ) * 0.5f;
	//uv2a.xy = abs( dP.rg );
	//uv2a.xy = permute ? dP.gr : dP.rg;
	//uv2a.xy = P0 * depthBufferSize.zw;

//	uv2a.rgb = float3( sampleZ, rayZMin, rayZMax );

	//uv2a.a = ( rayZMax >= sampleZ - depthThickness ) && ( rayZMin < sampleZ ) ? 1 : 0;
	uv2a.a = 1;

	return uv2a;
}
*/




////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment)
{
	float2 uv = fragment.position.xy * 2 + float2( fragment.position.y % 1, 0 );
	float4 position = gPosition.Load( int3( uv, 0 ) );

	if( position.z < 0.005 || position.z > 1024.0f )
	{
		clip( -1 );
	}

	float3 normalWS = gNormal.Load( int3( uv, 0 ) ).xyz;
	float3 normalVS = normalize( mul( view, float4( normalWS, 0 ) ).xyz );

	float3 viewVS = normalize( position.xyz );

	float3 reflectionVS = normalize( reflect( viewVS, normalVS ) );

	float4 reflection = getReflection( gPosition, gNormal, pointSampler, position.xyz, normalVS, reflectionVS );

	//float4 reflection = getReflection2DMarch(
	//	gPosition,
	//	position.xyz,
	//	reflectionVS,
	//	projection,	// Screen Space [ (0,0), (maxX,maxY) ] Projection
	//	targetSize,	// The depth buffer dimensions
	//	0.01,	// Depth buffer pixel view space depth/size
	//	0.00f,	// The camera near projection plane
	//	1,		// The pixel sampling stride
	//	0,		// [ 0, 1 ] value to try conceal banding
	//	30,	// Maximum number of iterations
	//	100		// Maximum distance to fetch a reflection
	//);

	Pixel pixel;
	pixel.colour0 = reflection;
	return pixel;
}
