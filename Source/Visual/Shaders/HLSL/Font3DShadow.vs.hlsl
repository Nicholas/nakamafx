

#include "type.vs.cb0.PerScene.hlsl"
#include "type.vs.cb1.PerModel.hlsl"
#include "type.iVS.Vertex.hlsl"
#include "type.iPS.Font2DFragment.hlsl"

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader - Pass Through
////////////////////////////////////////////////////////////////////////////////
Fragment main( VertexChar characture, uint vertexID : SV_VertexID )
{
	Fragment fragment;

	// vert id 0 = 0000, uv = (0, 0)
	// vert id 1 = 0001, uv = (1, 0)
	// vert id 2 = 0010, uv = (0, 1)
	// vert id 3 = 0011, uv = (1, 1)
	float2 uv = float2( vertexID & 1, ( ( vertexID >> 1 ) & 1 ) );

	float4 offset = float4( characture.position.xy, characture.textureUV.xy );
	float4 size   = float4( characture.position.zw * float2( 1, -1 ), characture.textureUV.zw );
	float4 coord  = mad( uv.xyxy, size, offset ); //offset + ( size * uv.xyxy );

	float4 worldPosition = mul( float4( coord.x, 1 - coord.y, 0, 1 ), model );

	fragment.position   = mul( worldPosition, viewProjectionMatrix );
	fragment.textureUV  = float3( coord.z, 1 - coord.w, characture.id );

	return fragment;
}
