
#include "algorithms.ColourSpace.hlsl"
#include "algorithms.ToneMapping.hlsl"
#include "algorithms.BicubicSamplers.hlsl"
#include "type.iPS.TexturedFragment.hlsl"
#include "algorithms.Graphs.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer cbToneMapping : register( b0 )
{
	float2 imageSize;
	float2 invImageSize;

	float ShoulderStrength;

	float LinearStrength;
	float LinearAngle;
	float LinearWhite;

	float ToeStrength;
	float ToeNumerator;
	float ToeDenominator;

	float gamma;

	float4 bloomFactor;

	int mode;
	int p0;
}

SamplerState samplerState : register( s0 );
SamplerState linearState  : register( s1 );

Texture2D hdrImage : register( t0 );
Texture2D bloomlv1 : register( t1 );
Texture2D bloomlv2 : register( t2 );
Texture2D bloomlv3 : register( t3 );
Texture2D bloomlv4 : register( t4 );

StructuredBuffer< float > luminance: register( t5 );


///////////////
// Exposure //
/////////////
float4 expose( float4 colour )
{
	//float avgLuminance = luminance[ 0 ];
	float avgLuminance = exp( luminance[ 1 ] );
	avgLuminance = max( avgLuminance, 0.001f );

	float keyValue = 1.03f - ( 2.0f / ( 2 + log10( avgLuminance + 1 ) ) );
	float linearExposure = ( keyValue / avgLuminance );
	float exposure = log2( max( linearExposure, 0.0002f ) );

	return exp2( exposure ) * colour;
}

///////////////////
// Tone Mapping //
/////////////////
float3 toneMap( float3 colour )
{
	return ToneMapFilmicU2(
		colour,
		ShoulderStrength,
		LinearStrength,
		LinearAngle,
		ToeStrength,
		ToeNumerator,
		ToeDenominator,
		LinearWhite );
}

float3 toneMapLuminance( float3 colour )
{
	float  luminanceHDR = rgbToLuminance2( colour );
	float3 luminanceLDR = toneMap( luminanceHDR.rrr );
	return colour / luminanceHDR * luminanceLDR;
}

float3 collapseBloom( float2 uv )
{
	float4 colour = 0;

	colour += bloomFactor[ 0 ] * bloomlv1.Sample( linearState, uv );
	colour += bloomFactor[ 1 ] * bloomlv2.Sample( linearState, uv );
	colour += bloomFactor[ 2 ] * bicubicInterpolationFast( bloomlv3, linearState, uv, 0 );
	colour += bloomFactor[ 3 ] * bicubicInterpolationFast( bloomlv4, linearState, uv, 0 );

	return colour.rgb;
}

static const int DISPLAY_GRAPHS = 0x1;

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( in Fragment fragment) : SV_TARGET
{
	float4 colour = hdrImage.Sample( samplerState, fragment.textureUV );

	///////////////
	// Exposure //
	/////////////
	colour = expose( colour );

	////////////
	// Bloom //
	//////////
	colour.rgb += collapseBloom( fragment.textureUV );

	///////////////////
	// Tone Mapping //
	/////////////////
	colour.rgb = toneMapLuminance( colour.rgb );

	///////////////////////
	// Gamma Correction //
	/////////////////////
	colour = pow( max( colour, 0 ), gamma );

	/////////////////////
	// Debugging View //
	///////////////////
	if( mode & DISPLAY_GRAPHS )
	{
		float4 graph = plotU2ToneMappingGraph(
			fragment.textureUV,
			ShoulderStrength,
			LinearStrength,
			LinearAngle,
			ToeStrength,
			ToeNumerator,
			ToeDenominator,
			LinearWhite );

		colour.rgb = lerp( colour.rgb, graph.rgb, graph.a );
	}

	return saturate( colour );
}
