
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.FontMaterial.hlsl"
#include "type.iPS.Font3DFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.NormalMapping.hlsl"
#include "algorithms.ShadowMapping.hlsl"
#include "algorithms.ClusteredLighting.hlsl"

#include "algorithms.Noise.hlsl"

/////////////
// GLOBALS //
/////////////
Texture2DArray msdfImage : register( t0 );
Texture2D textures[ 13 ] : register( t1 );

// Forward Clustered Lights
StructuredBuffer<LightCluster> lightGridSRV : register( t14 );
StructuredBuffer< uint >  lightIndexListSRV : register( t15 );
StructuredBuffer<Light>           lightsSRV : register( t16 );

SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState mirrorState : register( s2 );
SamplerState pointState : register( s3 );

#define    COLOUR_TEX 0
#define     NORMAL_TX 1
#define     DEPTH_TEX 2
#define        AO_TEX 3
#define      BUMP_TEX 4
#define     GLOSS_TEX 5
#define  HI_GLOSS_TEX 6
#define ROUGHNESS_TEX 7
#define METALNESS_TEX 8

#define SHADOW_1_MAP 9
#define SHADOW_2_MAP 10
#define SHADOW_TD_MAP 11
#define SHADOW_TC_MAP 12

#define STD 0
#define WARP 1


/// Bicubic interpolation sampling of a texture.
float4 bicubicInterpolationFast(
	in Texture2DArray image,
	in SamplerState imageSampler,
	in float3 uv,
	in uint mipLevel
)
{
	float4 texSize = float4( 0, 0, 0, mipLevel );

	image.GetDimensions( texSize.x, texSize.y, texSize.z );

	float2 rec_nrCP = 1.0 / texSize.xy;
	float2 coord_hg = uv.xy * texSize.xy - 0.5; // TODO: Chech to see if this is correct in dx11
	float2 index = floor( coord_hg );

	float2 f = coord_hg - index;
	float4x4 M = {
		-1,  3, -3,  1,
		 3, -6,  3,  0,
		-3,  0,  3,  0,
		 1,  4,  1,  0
	};
	M /= 6;

	float4 wx = mul( float4( f.x*f.x*f.x, f.x*f.x, f.x, 1 ), M );
	float4 wy = mul( float4( f.y*f.y*f.y, f.y*f.y, f.y, 1 ), M );
	float2 w0 = float2( wx.x, wy.x );
	float2 w1 = float2( wx.y, wy.y );
	float2 w2 = float2( wx.z, wy.z );
	float2 w3 = float2( wx.w, wy.w );

	float2 g0 = w0 + w1;
	float2 g1 = w2 + w3;
	float2 h0 = w1 / g0 - 1;
	float2 h1 = w3 / g1 + 1;

	float2 coord00 = index + h0;
	float2 coord10 = index + float2( h1.x, h0.y );
	float2 coord01 = index + float2( h0.x, h1.y );
	float2 coord11 = index + h1;

	coord00 = ( coord00 + 0.5 ) * rec_nrCP;
	coord10 = ( coord10 + 0.5 ) * rec_nrCP;
	coord01 = ( coord01 + 0.5 ) * rec_nrCP;
	coord11 = ( coord11 + 0.5 ) * rec_nrCP;

	float4 tex00 = image.SampleLevel( imageSampler, float3( coord00, uv.z ), texSize.a );
	float4 tex10 = image.SampleLevel( imageSampler, float3( coord10, uv.z ), texSize.a );
	float4 tex01 = image.SampleLevel( imageSampler, float3( coord01, uv.z ), texSize.a );
	float4 tex11 = image.SampleLevel( imageSampler, float3( coord11, uv.z ), texSize.a );

	tex00 = lerp( tex01, tex00, g0.y );
	tex10 = lerp( tex11, tex10, g0.y );

	return lerp( tex10, tex00, g0.x );
}

float median( float r, float g, float b )
{
	return max( min( r, g ), min( max( r, g ), b ) );
}

float getMSDFEdge( float3 msdf, float2 msdfUnit, float2 uv, float offset )
{
	msdf -= 0.5f - offset;
	float sdf = median( msdf.r, msdf.g, msdf.b );


	sdf *= dot( msdfUnit, 0.5f / fwidth( uv ) );

	return saturate( sdf + 0.5f );
}

float getSDFEdge( float sdf, float2 msdfUnit, float2 uv, float offset )
{
	sdf -= 0.5f - offset;
	sdf *= dot( msdfUnit, ( 0.5f ) / fwidth( uv ) );

	return saturate( sdf + 0.5f );
}

LightingInfo pbrLighting(
	Fragment fragment,
	float3 colour,
	float3 surfaceNormal,
	float  roughness,
	float  metalness,
	float  ao )
{
	float LdotSN = dot( dLightD, surfaceNormal );
	float bias = tan( acos( LdotSN ) );

	float2 shadow1 = getShadowCascade(
		textures[ SHADOW_1_MAP ], cmpSampler,
		fragment.positionS1, shadowSize1,
		bias * shadowSize1.x
	);

	float shadow2 = getShadowWithBias(
		textures[ SHADOW_2_MAP ], cmpSampler,
		fragment.positionS2, shadowSize2,
		bias * shadowSize2.x * 4
	);

	// Turn light off a sun set...
	float sunSet = smoothstep( 0.05, 0.3, dot( dLightD, float3( 0, 1, 0 ) ) );
	float shadow = lerp( shadow2, shadow1.x, shadow1.y ) * saturate( LdotSN );

	float4 shadowColour = getShadowColour(
		textures[ SHADOW_TD_MAP ], cmpSampler,
		textures[ SHADOW_TC_MAP ], pointState,
		fragment.positionS1,
		bias * shadowSize1.x );

	float3 lightColour = lerp( dLightC.rgb, shadowColour.rgb, shadowColour.a ) * shadow * sunSet;

	int clusterIndex = getClusterIndex( fragment.position.xy, fragment.positionV.z, float2( targetWidth, targetHeight ), zDepths.x, zDepths.y );
	LightCluster cluster = lightGridSRV[ clusterIndex ];

	return clusterLighting(
		cluster, lightIndexListSRV, lightsSRV,
		lightColour.rgb, dLightD,
		fragment.positionW,
		fragment.normal,
		fragment.viewRay,
		colour,
		ks,
		metalness,
		roughness,
		ao );
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader - Texture Sample
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	if( properties.x == WARP )
	{
		fragment.textureUV.xy += snoise2D( ( fragment.textureUV.xy + time ) * 10.0 ) * 0.03;
		fragment.textureUV.xy += snoise2D( ( fragment.textureUV.xy + time ) * 40.0 ) * 0.02;
	}


	// Bicubic looks nice when we start going really big, otherwise it looks the same as linear.
	float4 msdfSample = bicubicInterpolationFast( msdfImage, samplerState, fragment.textureUV, 0 );
	//float4 msdfSample = msdfImage.Sample( samplerState, fragment.textureUV );

	uint3 textureSize;
	msdfImage.GetDimensions( textureSize.x, textureSize.y, textureSize.z );

	float pxRange    = 4.0f;
	float msdfOffset = 0.0f;
	float  sdfOffset = 0.05f;

	float2 msdfUnit = pxRange / textureSize.xy;
	float2  sdfUnit = pxRange / textureSize.xy;

	float msdf = getMSDFEdge( msdfSample.rgb, msdfUnit, fragment.textureUV.xy, msdfOffset );
	float  sdf = getMSDFEdge( msdfSample.rgb, sdfUnit, fragment.textureUV.xy, sdfOffset );
	//float  sdf = getSDFEdge( msdfSample.a, sdfUnit, fragment.textureUV.xy, sdfOffset );


	float4 colourSample = textures[ COLOUR_TEX    ].Sample( samplerState, fragment.textureUV.xy );
	float3 normalSample = textures[ NORMAL_TX     ].Sample( samplerState, fragment.textureUV.xy ).rgb;
	float  roughness    = textures[ ROUGHNESS_TEX ].Sample( samplerState, fragment.textureUV.xy ).r;
	float  metalness    = textures[ METALNESS_TEX ].Sample( samplerState, fragment.textureUV.xy ).r;
	float  ao           = textures[ AO_TEX        ].Sample( samplerState, fragment.textureUV.xy ).r;

	colourSample.rgb = lerp( float3( 0, 0, 0 ), colourSample.rgb, msdf );
	normalSample     = lerp( float3( 0, 0, 0 ), normalSample    , msdf );

	float a = max( sdf, msdf );

	clip( a - 0.01 );

	fragment.normal   = normalize( fragment.normal );
	fragment.tangent  = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.viewRay  = normalize( fragment.viewRay );
	float3 surfaceNormal = fragment.normal;

	if( length( normalSample ) > 0 )
	{
		fragment.normal = calculateBumpNormalZ(
			fragment.normal,
			fragment.tangent,
			fragment.binormal,
			normalSample,
			float3( bumpScale.xy, 1.0 )
		);
	}

	LightingInfo surface = pbrLighting( fragment, colourSample.rgb * kd, surfaceNormal, roughness, metalness, ao );

	Pixel pixel;

	pixel.colour = float4(  surface.colour, 1 );
	pixel.normal = float4( fragment.normal, 1 );
	pixel.viewSP = float4( fragment.positionV, fragment.position.z * fragment.position.w );
	pixel.F0     = saturate( float4( surface.reflection, roughness ) );

	return pixel;
}
