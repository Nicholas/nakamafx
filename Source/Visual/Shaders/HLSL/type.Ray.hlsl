

#ifndef _RAY_SHADER_TYPE_
#define _RAY_SHADER_TYPE_

struct Ray
{
    float3 o;   // Origin.
    float3 d;   // Direction.
};

#endif //_SPHERE_SHADER_TYPE_