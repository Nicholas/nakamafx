
#include "algorithms.ColourSpace.hlsl"
#include "algorithms.ToneMapping.hlsl"
#include "algorithms.BicubicSamplers.hlsl"
#include "type.iPS.TexturedFragment.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer cbPostFX : register( b0 )
{
	float gamma;
	float contrast;
	float saturation;
	float brightness;
}

Texture2D textures[ 8 ] : register( t0 );
SamplerState samplerState : register( s0 );
SamplerState linearState : register( s1 );

#define MRT_0 0
#define MRT_1 1
#define MRT_2 2
#define MRT_3 3
#define MRT_4 4
#define MRT_5 5
#define MRT_6 6
//#define MRT_7 7
#define MRT_D 7

StructuredBuffer< float > responseCurve : register( t8 );
StructuredBuffer< float > luminance     : register( t9 );

//////////////
// TYPEDEFS //
//////////////

//float3 correct( float3 colour )
//{
//	////////////
//	// Gamma //
//	//////////
//	colour = pow( colour, gamma );
//
//	///////////////
//	// Contrast //
//	/////////////
//	colour = ( ( colour - 0.5f ) * max( contrast + 1.0, 0 ) ) + 0.5f;
//
//	/////////////////
//	// Saturation //
//	///////////////
//	//float greyscale = dot( colour, float3( 0.33, 0.33, 0.33 ) );
//	float greyscale = dot( colour, float3( 0.2126f, 0.7152f, 0.0722f ) );
//	colour = lerp( greyscale, colour, ( saturation + 1.0 ) );
//
//	/////////////////
//	// Brightness //
//	///////////////
//	//colour += brightness;
//
//	return colour;
//}
//
//
/////////////////
//// Exposure //
///////////////
//float4 expose( float4 colour )
//{
//	//float avgLuminance = luminance[ 0 ];
//	float avgLuminance = exp( luminance[ 1 ] );
//	//float threshold = 0.0f;
//	avgLuminance = max( avgLuminance, 0.001f );
//	float keyValue = 1.03f - ( 2.0f / ( 2 + log10( avgLuminance + 1 ) ) );
//	float linearExposure = ( keyValue / avgLuminance );
//	float exposure = log2( max( linearExposure, 0.0002f ) );
//	//exposure -= threshold;
//	return exp2( exposure ) * colour;
//}
//
//float3 toneMap( float3 colour )
//{
//	// Uncharted 2 Values
//	float ShoulderStrength =  0.22f + gamma;
//	float LinearStrength   =  0.3f + saturation;
//	float LinearAngle      =  0.1f + brightness;
//	float ToeStrength      =  0.2f;
//	float ToeNumerator     =  0.01f;
//	float ToeDenominator   =  0.3f;
//	float LinearWhite      = 11.2f + contrast;
//
//#define HSV_TONE_MAP
//#ifdef HSV_TONE_MAP
//	float  luminanceHDR = rgbToLuminance2( colour );
//	float3 luminanceLDR = ToneMapFilmicU2(
//		luminanceHDR.rrr,
//		ShoulderStrength,
//		LinearStrength,
//		LinearAngle,
//		ToeStrength,
//		ToeNumerator,
//		ToeDenominator,
//		LinearWhite );
//
//	return max( colour / luminanceHDR * luminanceLDR, 0 );
//#else
//	return ToneMapFilmicU2(
//		colour,
//		ShoulderStrength,
//		LinearStrength,
//		LinearAngle,
//		ToeStrength,
//		ToeNumerator,
//		ToeDenominator,
//		LinearWhite );
//#endif
// }

float histogramToneMap( float scaleF )
{
	uint scaleID = uint( scaleF );

	float scale = scaleID < 64 ? responseCurve[ scaleID ] : 1.0f;

	float scaleLow = ( scaleID - 1 ) >= 0 ? responseCurve[ scaleID - 1 ] : ( ( scaleID - 1 ) < 64 ? 0.0f : scale );
	float scaleHigh = ( scaleID + 1 ) < 64 ? responseCurve[ scaleID + 1 ] : 1.0f;

	float l = scaleF % 1.0f;
	float a = lerp( scaleLow, scale, l );
	float b = lerp( scale, scaleHigh, l );

	//return lerp( a, b, 1 - l );
	//return max( a * b, b );
	//return scaleLow == 0.0f ? scale : a * b;
	return b;
}

float histogramToneMapFromColour( float3 colour )
{
	return rgbToHistogramBinFloat( colour.rgb, 0, 4.5, 64 );
}

static const bool TONE_MAP_TARGETS = true;
static const bool DISPLAY_TARGETS = true;
static const bool DISPLAY_GRAPHS = false;

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( in Fragment fragment) : SV_TARGET
{
	float4 colour = 0;

	//*/

	bool x0 = fragment.textureUV.x < 0.25f;
	bool x1 = fragment.textureUV.x < 0.50f;
	bool x2 = fragment.textureUV.x < 0.75f;

	bool y0 = fragment.textureUV.y < 0.25f;
	bool y1 = fragment.textureUV.y < 0.50f;
	bool y2 = fragment.textureUV.y < 0.75f;

	if( DISPLAY_TARGETS )
	{
		if( x0 )
		{
			if( y0 )
			{
//                fragment.textureUV *= 2;
				colour = textures[ MRT_1 ].Sample( samplerState, fragment.textureUV );
				colour.a = 1.0f;
			}
			else if( y1 )
			{
//                fragment.textureUV *= 2;
				colour = textures[ MRT_1 ].Sample( samplerState, fragment.textureUV );
				colour.a = 1.0f;
			}
			else if( y2 )
			{
				fragment.textureUV.y -= 0.50;
				fragment.textureUV *= 4;
				colour = textures[ MRT_3 ].Sample( samplerState, fragment.textureUV );
				colour.a = 1.0f;
			}
			else //if( y3 )
			{
				fragment.textureUV.y -= 0.75;
				fragment.textureUV *= 4;
				colour = textures[ MRT_4 ].Sample( samplerState, fragment.textureUV );
				colour.a = 1.0f;
			}
		}
		else if( x1 )
		{
			if( y0 )
			{
//                fragment.textureUV *= 2;
				colour = textures[ MRT_1 ].Sample( samplerState, fragment.textureUV );
				colour.a = 1.0f;
			}
			else if( y1 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
//                colour = textures[ MRT_0 ].Sample( samplerState, fragment.textureUV );
			}
			else if( y2 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
//                colour = textures[ MRT_0 ].Sample( samplerState, fragment.textureUV );
			}
			else //if( y3 )
			{
				fragment.textureUV.x -= 0.25;
				fragment.textureUV.y -= 0.75;
				fragment.textureUV *= 4;
				colour = textures[ MRT_5 ].Sample( samplerState, fragment.textureUV );
				colour.a = 1.0f;
			}
		}
		else if( x2 )
		{
			if( y0 )
			{
				fragment.textureUV.x += 0.5;
				fragment.textureUV *= 2;
				colour = textures[ MRT_2 ].Sample( samplerState, fragment.textureUV );
				//colour.rgb %= 1.0f;

				if( colour.b < 1.001 )
				{
					colour.rgba = 0;
					colour.g = 1;
				}
				else
				{
					colour.rgb = colour.arb % 1.0f;
				}
				colour.a = 1.0f;
			}
			else if( y1 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
//                colour = textures[ MRT_0 ].Sample( samplerState, fragment.textureUV );
			}
			else if( y2 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
//                colour = textures[ MRT_0 ].Sample( samplerState, fragment.textureUV );
			}
			else //if( y3 )
			{
				fragment.textureUV.x -= 0.50;
				fragment.textureUV.y -= 0.75;
				fragment.textureUV *= 4;
				colour = textures[ MRT_6 ].Sample( samplerState, fragment.textureUV );
				colour.a = 1.0f;
			}
		}
		else //if( x3 )
		{
			if( y0 )
			{
				fragment.textureUV.x += 0.5;
				fragment.textureUV *= 2;
				colour = textures[ MRT_2 ].Sample( samplerState, fragment.textureUV );
				//colour.rgb %= 1.0f;
				colour.rgb = colour.arb % 1.0f;
				colour.a = 1.0f;
			}
			else if( y1 )
			{
				fragment.textureUV.x += 0.5;
				fragment.textureUV *= 2;
				colour = textures[ MRT_2 ].Sample( samplerState, fragment.textureUV );
				//colour.rgb %= 1.0f;
				colour.rgb = colour.arb % 1.0f;
				colour.a = 1.0f;
			}
			else if( y2 )
			{
				fragment.textureUV.x -= 0.75;
				fragment.textureUV.y -= 0.50;
				fragment.textureUV *= 4;

				float fIndex = fragment.textureUV.x * 64;
				uint uIndex = fIndex;


				float scale = responseCurve[ uIndex ];

				float scaleLow  = ( uIndex - 1 ) >= 0 ? responseCurve[ uIndex - 1 ] : 0.0f;
				float scaleHigh = ( uIndex + 1 ) < 64 ? responseCurve[ uIndex + 1 ] : 1.0f;


				float l = fIndex % 1.0f;
				float a = lerp( scaleLow, scale, l );
				float b = lerp( scale, scaleHigh, l );

				//scale = lerp( a, b, l );
				scale = a * b;
				//scale = a;


				float step = float( uIndex ) / 64.0f;

				float diff = scale - ( 1 - fragment.textureUV.y );

				float r = abs( diff ) < 0.01 ? 1 : 0;

				colour = float4( r, 0, step, 1 );
			}
			else //if( y3 )
			{
				fragment.textureUV -= 0.75;
				fragment.textureUV *= 4;
				colour = textures[ MRT_D ].Sample( samplerState, fragment.textureUV );
				//colour.r = pow( colour.r, 800 );

				// Calculate our projection constants (you should of course do this in the app code, I'm just showing how to do it)

				float FarClipDistance = 1000.0f;
				float NearClipDistance = 0.01f;

				float ProjectionA = FarClipDistance / ( FarClipDistance - NearClipDistance );
				float ProjectionB = ( -FarClipDistance * NearClipDistance ) / ( FarClipDistance - NearClipDistance );

				colour.r = 1 - ProjectionB / ( colour.r - ProjectionA ) / FarClipDistance;

				colour.r = ( colour.r * 30 ) % 1;
				//colour.r = ProjectionB / ( colour.r - ProjectionA );
				colour.a = 1.0f;
			}
		}
	}

	if( DISPLAY_GRAPHS )
	{
		// Graph Values
		const float MinLogX = -3.0f;
		const float MaxLogX = 3.0f;
		const float MaxY = 1.0f;

		if( x0 )
		{
			if( y0 )
			{
				fragment.textureUV *= 4;

				float scale = histogramToneMap( fragment.textureUV.x * 64 );
				float diff = scale - ( 1 - fragment.textureUV.y );

				if( abs( diff ) < 0.005 )
				{
					colour.r = 1;
				}

				if( 1 - fragment.textureUV.y < 0.005 )
				{
					colour.g = 1;
				}
				colour.a = 1.0f;
			}
			else if( y1 )
			{
				fragment.textureUV *= 2;
			}
			else if( y2 )
			{
				fragment.textureUV.y -= 0.50;
				fragment.textureUV *= 4;
			}
			else //if( y3 )
			{
				fragment.textureUV.y -= 0.75;
				fragment.textureUV *= 4;


			}
		}
		else if( x1 )
		{
			if( y0 )
			{
				fragment.textureUV *= 2;
			}
			else if( y1 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
			}
			else if( y2 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
			}
			else //if( y3 )
			{
				fragment.textureUV.x -= 0.25;
				fragment.textureUV.y -= 0.75;
				fragment.textureUV *= 4;
			}
		}
		else if( x2 )
		{
			if( y0 )
			{
				fragment.textureUV.x += 0.5;
				fragment.textureUV *= 2;
			}
			else if( y1 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
			}
			else if( y2 )
			{
				fragment.textureUV += 0.25;
				fragment.textureUV *= 2;
			}
			else //if( y3 )
			{
				fragment.textureUV.x -= 0.50;
				fragment.textureUV.y -= 0.75;
				fragment.textureUV *= 4;
			}
		}
		else //if( x3 )
		{
			if( y0 )
			{
				fragment.textureUV.x += 0.75;
				fragment.textureUV *= 4;

			//colour = textures[ MRT_5 ].Sample( samplerState, fragment.textureUV );
			}
			else if( y1 )
			{
				fragment.textureUV.x += 0.75;
				fragment.textureUV *= 4;

			//colour = textures[ MRT_5 ].SampleLevel( samplerState, fragment.textureUV, 9 );
			}
			else if( y2 )
			{
				fragment.textureUV.x -= 0.75;
				fragment.textureUV.y -= 0.50;
				fragment.textureUV *= 4;

				//float lum = luminance[ 0 ];
				//colour.r = lum;
			}
			else //if( y3 )
			{
				fragment.textureUV -= 0.75;
				fragment.textureUV *= 4;

				//float logLum = luminance[ 1 ];
				//colour.r = logLum;
				//
				//if( logLum > 1 )
				//{
				//	colour.g = logLum - 1;
				//}
				//
				//if( logLum > 2 )
				//{
				//	colour.b = logLum - 2;
				//}
			}
		}


		colour = saturate( colour );
	}

	return colour;
}
