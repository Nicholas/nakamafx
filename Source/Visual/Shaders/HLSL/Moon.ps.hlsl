
#include "type.Light.hlsl"
#include "type.ps.cb0.PerScene.hlsl"
#include "type.ps.cb1.PerMaterial.hlsl"
#include "type.iPS.ParallaxFragment.hlsl"
#include "type.oPS.ForwardMRT.hlsl"

#include "algorithms.Lighting.hlsl"



//////////////
// GLOBALS //
////////////
Texture2D textures[ 2 ];
SamplerState samplerState;
SamplerComparisonState cmpSampler;

#define    COLOUR_TEX 0
#define     NORMAL_TX 1


float3 modNormal( float3 normal, float3 tangent, float3 bitangent, float3 offset )
{
	float3 result;
	result = offset.x * tangent
		   - offset.y * bitangent
		   + offset.z * normal;

	return normalize( result );
}

static const float PI = 3.1415926536f;
float2 sphereMapTextureUV( float3 normal )
{
	float2 textureUV;

	textureUV.y = acos( normal.y ) / PI;
	textureUV.x = atan2( normal.z, -normal.x );
	//textureUV.x = saturate( ( ( textureUV.x + PI ) / ( PI * 2 ) ) );
	textureUV.x = ( ( textureUV.x + PI ) / ( PI * 2 ) );

	return textureUV;

}
static float spinConst = -0.5f;

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
Pixel main( Fragment fragment )
{
	fragment.normal   = normalize( fragment.normal );
	fragment.tangent  = normalize( fragment.tangent );
	fragment.binormal = normalize( fragment.binormal );
	fragment.view     = normalize( fragment.view );

	float2 dist = fragment.textureUV * 2 - 1;
	float radius = dist.x * dist.x + dist.y * dist.y;

	clip( 1 - radius );


	fragment.normal = modNormal( 
		fragment.normal,
		fragment.tangent,
		fragment.binormal,
		float3( dist.x, dist.y, sqrt( 1 - radius ) ) );



	float3 ambiantLight = ambiantLighting(
		aLightC.rgb,
		ka,
		1.0f
	);

	float3 directionLight = directionLighting(
		dLightC.rgb,
		dLightD,
		fragment.normal,
		fragment.view,
		1.0,
		kd, ks, 16//ns
	);
    
	float2 tex = sphereMapTextureUV( fragment.normal );
	tex.x += time * spinConst;

	//float4 colour = textures[ COLOUR_TEX ].SampleLevel( samplerState, tex, 3 );
	//float4 colour = textures[ COLOUR_TEX ].SampleGrad( samplerState, tex, ddx( fragment.normal.x ), ddy( fragment.normal.y ) );
	float4 colour = textures[COLOUR_TEX].Sample(samplerState, tex);


	Pixel pixel;

	pixel.colour = float4( colour.rrr * ( ambiantLight + directionLight ), 1 );
	pixel.normal = float4( fragment.normal, 1 );
	pixel.viewSP = float4( fragment.viewPos, fragment.position.z * fragment.position.w + radius );
	pixel.F0     = float4( 0, 0, 0, 1 );

	return pixel;
}
