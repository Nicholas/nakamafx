



float2 texOffset( float u, float v, float2 texelSize )
{
	return float2( u * texelSize.x, v * texelSize.y );
}

float2 getShadowCascade(
	Texture2D shadowMap,
	SamplerComparisonState cmpSampler,
	float4 lPosition,
	float2 texelSize,
	float bias
)
{
	lPosition.xyz /= lPosition.w;

	if( lPosition.x <= -1.0f || lPosition.x >= 1.0f ||
		lPosition.y <= -1.0f || lPosition.y >= 1.0f ||
		lPosition.z <= 0.0f || lPosition.z >= 1.0f )
	{
		return float2( 1, 0 );
	}

	//float cascadeBlend = saturate( 1.2 - sqrt( lPosition.x * lPosition.x + lPosition.y * lPosition.y ) );
	float cascadeBlend = 1.0f;

	//transform clip space coords to texture space coords (-1:1 to 0:1)
	lPosition.x = lPosition.x / 2 + 0.5;
	lPosition.y = lPosition.y / -2 + 0.5;

	lPosition.z -= bias;

	//perform PCF filtering on a 4 x 4 texel neighborhood
	float x;
	float y;

	float sum;

	for( y = -1.5; y <= 1.5; y += 1.0 )
	{
		for( x = -1.5; x <= 1.5; x += 1.0 )
		{
			sum += shadowMap.SampleCmpLevelZero( cmpSampler, lPosition.xy + texOffset( x, y, texelSize ), lPosition.z );
		}
	}



	return float2( sum / 16.0f, cascadeBlend );
}

float getShadowWithBias(
	Texture2D shadowMap,
	SamplerComparisonState cmpSampler,
	float4 lPosition,
	float2 texelSize,
	float bias
)
{
	lPosition.xyz /= lPosition.w;

	if( lPosition.x <= -1.0f || lPosition.x >= 1.0f ||
		lPosition.y <= -1.0f || lPosition.y >= 1.0f ||
		lPosition.z <=  0.0f || lPosition.z >= 1.0f )
	{
		return 1;
	}

	//transform clip space coords to texture space coords (-1:1 to 0:1)
	lPosition.x = lPosition.x /  2 + 0.5;
	lPosition.y = lPosition.y / -2 + 0.5;

	lPosition.z -= bias;

	//perform PCF filtering on a 4 x 4 texel neighborhood
	float x;
	float y;

	float sum;

	for( y = -1.5; y <= 1.5; y += 1.0 )
	{
		for( x = -1.5; x <= 1.5; x += 1.0 )
		{
			sum += shadowMap.SampleCmpLevelZero( cmpSampler, lPosition.xy + texOffset( x, y, texelSize ), lPosition.z );
		}
	}

	return sum / 16.0f;
}

float getShadow(
	Texture2D shadowMap,
	SamplerComparisonState cmpSampler,
	float4 lPosition,
	float2 texelSize
)
{
	return getShadowWithBias( shadowMap, cmpSampler, lPosition, texelSize, 0.003f );
}

float4 getShadowColour(
	Texture2D shadowDepthMap,
	SamplerComparisonState shadowDepthSampler,
	Texture2D shadowColourMap,
	SamplerState shadowColourSampler,
	float4 lPosition,
	float bias
)
{
	lPosition.xyz /= lPosition.w;

	if( lPosition.x <= -1.0f || lPosition.x >= 1.0f ||
		lPosition.y <= -1.0f || lPosition.y >= 1.0f ||
		lPosition.z <= 0.0f || lPosition.z >= 1.0f )
	{
		return float4( 0, 0, 0, 0 );
	}

	//transform clip space coords to texture space coords (-1:1 to 0:1)
	lPosition.x = lPosition.x / 2 + 0.5;
	lPosition.y = lPosition.y / -2 + 0.5;

	lPosition.z -= bias;



	//float x;
	//float y;
	//float sum;
    //
	//for( y = -1.5; y <= 1.5; y += 1.0 )
	//{
	//	for( x = -1.5; x <= 1.5; x += 1.0 )
	//	{
	//		sum += shadowDepthMap.SampleCmpLevelZero( shadowDepthSampler, lPosition.xy + texOffset( x, y, float2( 4096, 4096 ) ), lPosition.z );
	//	}
	//}
    //
	//float shadow = 1 - sum / 16.0f;
	float shadow = 1 - shadowDepthMap.SampleCmpLevelZero( shadowDepthSampler, lPosition.xy, lPosition.z );

	float4 light = shadowColourMap.Sample( shadowColourSampler, lPosition.xy );

	return float4( light.rgb, light.a * shadow );
}

float4 getShadowColourPCF(
	Texture2D shadowDepthMap,
	SamplerComparisonState shadowDepthSampler,
	Texture2D shadowColourMap,
	SamplerState shadowColourSampler,
	float4 lPosition,
	float bias
)
{
	lPosition.xyz /= lPosition.w;

	if( lPosition.x <= -1.0f || lPosition.x >= 1.0f ||
		lPosition.y <= -1.0f || lPosition.y >= 1.0f ||
		lPosition.z <= 0.0f || lPosition.z >= 1.0f )
	{
		return float4( 0, 0, 0, 0 );
	}

	//transform clip space coords to texture space coords (-1:1 to 0:1)
	lPosition.x = lPosition.x / 2 + 0.5;
	lPosition.y = lPosition.y / -2 + 0.5;

	lPosition.z -= bias;



	float x;
	float y;
	float sum;

	for( y = -1.5; y <= 1.5; y += 1.0 )
	{
		for( x = -1.5; x <= 1.5; x += 1.0 )
		{
			sum += shadowDepthMap.SampleCmpLevelZero( shadowDepthSampler, lPosition.xy + texOffset( x, y, float2( 4096, 4096 ) ), lPosition.z );
		}
	}

	float shadow = 1 - sum / 16.0f;
	//float shadow = 1 - shadowDepthMap.SampleCmpLevelZero( shadowDepthSampler, lPosition.xy, lPosition.z );

	float4 light = shadowColourMap.Sample( shadowColourSampler, lPosition.xy );

	return float4( light.rgb, light.a * shadow );
}