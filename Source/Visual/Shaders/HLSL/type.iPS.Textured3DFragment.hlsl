
#ifndef _TEXTURED_3D_FRAGMENT
#define _TEXTURED_3D_FRAGMENT

struct Fragment
{
	float4 position   : SV_POSITION;
	float3 textureUVW : TEXCOORD0;
};

#endif
