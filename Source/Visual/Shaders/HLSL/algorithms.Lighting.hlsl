
#ifndef _LIGHTING_ALGORITHMS_HLSL
#define _LIGHTING_ALGORITHMS_HLSL

#ifndef PBR_PI
#define PBR_PI 3.1415926536f
#endif

//#define SPECULAR_BRDF_GGX
#define SPECULAR_BRDF_Schlick
//#define SPECULAR_BRDF_Beckmann

//#define FRESNEL_Schlick
#define FRESNEL_Epic

//#define POINT_FALLOFF_STD
#define POINT_FALLOFF_Epic

//
// -------------------------------------------
// Light Calculation
// -------------------------------------------
//
float3 lightFunction(
	float3 lightColour,
	float3 lightDirection,
	float3 normal,
	float3 viewDir,
	float specular,
	float3 Kd,
	float3 Ks,
	int Ns
	)
{
	// -------------------------------------------
	// Diffuse
	// -------------------------------------------
	float dFactor = saturate( dot( lightDirection, normal ) );

	if( dFactor <= 0.0 )
	{
		return float3( 0, 0, 0 );
	}

	float3 Id = Kd * lightColour * dFactor;

	// -------------------------------------------


	// -------------------------------------------
	// Phong
	// -------------------------------------------
	float3 R = reflect( lightDirection, normal );
	float sFactor = pow( saturate( dot( R, viewDir ) ), ( Ns * specular ) + 1 ) * specular;

	// -------------------------------------------
	// Blinn-Phongw
	// -------------------------------------------
	//float3 H = normalize( -viewDir + lightDirection );
	//float sFactor = pow( saturate( dot( H, normal ) ), Ns );


	float3 Is = Ks * lightColour * sFactor;
	// -------------------------------------------

	return Id + Is;
	//return Is;
}

float3 pointLighting(
	float3 lightColour,
	float3 lightDirection,
	float lightRange,
	float3 normal,
	float3 viewDir,
	float specular,
	float3 Kd,
	float3 Ks,
	int Ns )
{
	float distance = length( lightDirection );

	if( distance > lightRange )
		return float3( 0.0f, 0.0f, 1.0f );

	float3 Ids = lightFunction( lightColour, lightDirection / distance, normal, viewDir, specular, Kd, Ks, Ns );
	Ids /= dot( float3( 0.0f, 0.1f, 0.001f ), float3( 1.0f, distance, distance * distance ) );
	return Ids;
}

float3 directionLighting(
	float3 lightColour,
	float3 lightDirection,
	float3 normal,
	float3 viewDir,
	float specular,
	float3 Kd,
	float3 Ks,
	int Ns
)
{
	return lightFunction(
		lightColour,
		lightDirection,
		normal,
		viewDir,
		specular,
		Kd, Ks, Ns
	);
}

float3 ambiantLighting( float3 lightColour, float3 Ka, float ao )
{
	return lightColour * Ka * ao;
}








///////////
// BRDF //
/////////
//
// Bidirectional Reflection Distribution Function
//
// Cook - Torrance Specular
// Specular: F * G * NDF / 4 * PI * N.L * N.V
//
// Lambart Diffuse
// Diffuse : albido / PI
//
//
// Colour: ( Diffuse * ( 1 - metal ) * ( 1 - F ) + Specular ) * Radiance * N.L


// +-----------------+
// | Lambert Diffuse |
// +-----------------+
float3 lambartDiffuseBDRF( in float3 albido, in float metalness )
{
	return ( albido / PBR_PI ) * ( 1 - metalness );
}


// +-------------+
// | Fresnel - F |
// +-------------+
float3 fresnelSchlick( float3 F0, float cosTheta )
{
	return F0 + ( 1.0f - F0 ) * pow( 1.0f - cosTheta, 5.0f );
}

float3 fresnelSchlickEpic( float3 F0, float cosTheta )
{
	float e = ( -5.55473 * cosTheta - 6.98316 ) * cosTheta;
	return F0 + ( 1.0f - F0 ) * pow( 2, e );
}


// +-------------------------------------+
// | Normal Distribution Functions - NDF |
// +-------------------------------------+
float normalDistributionGGX( float3 N, float3 H, float roughness )
{
	float lambda = roughness * roughness * roughness * roughness;
	float NdotH = max( dot( N, H ), 0.001 );

	float denominator = NdotH * NdotH * ( lambda - 1.0f ) + 1.0f;

	return lambda / ( denominator * denominator * PBR_PI );
}

float normalDistributionBeckmann( float3 n, float3 h, float roughness )
{
	float aSq = roughness * roughness * roughness * roughness;
	float nh = dot( n, h );
	float nhSq = nh * nh;

	float a = max( PBR_PI * aSq * nhSq * nhSq, 0.001 );
	float b = nhSq - 1;
	float c = max( aSq * nhSq, 0.001 );

	return ( 1 / a ) * exp( b / c );
}


// +-------------------------+
// | Geometric Shadowing - G |
// +-------------------------+
float geometricRoughnessMapping( float roughness )
{
	//float lambda = ( roughness * roughness ) * 0.5f;
	float lambda = pow( roughness + 1, 2 ) * 0.125f;
	//float lambda = roughness * roughness;

	return lambda;
}

// Smith GGX
float geometricGGX( float3 N, float3 E, float lambda )
{
	float NdotE = max( dot( N, E ), 0.0001 );

	float denominator = NdotE + sqrt( lambda + ( 1 + lambda ) * NdotE * NdotE );

	return 2 * NdotE / denominator;
}

float geometricSmithGGX( float3 N, float3 L, float3 V, float roughness )
{
	float lambda = geometricRoughnessMapping( roughness );

	float GGX1 = geometricGGX( N, L, lambda );
	float GGX2 = geometricGGX( N, V, lambda );

	return GGX1 * GGX2;
}

// Smith Schlick GGX
float geometricSchlickGGX( float3 N, float3 E, float lambda )
{
	float NdotE = max( dot( N, E ), 0.0001 );
	return NdotE / ( NdotE * ( 1 - lambda ) + lambda );
}

float geometricSmithSchlickGGX( float3 n, float3 l, float3 v, float roughness )
{
	float lambda = geometricRoughnessMapping( roughness );

	float GGX1 = geometricSchlickGGX( n, l, lambda );
	float GGX2 = geometricSchlickGGX( n, v, lambda );

	return GGX1 * GGX2;
}

// Smith Beckmann
float geometricBeckmann( float3 n, float3 e, float alpha )
{
	float ne = saturate( dot( n, e ) );
	float a = alpha * sqrt( 1 - ne * ne );

	float c = ne / a;
	float c2 = c * c;

	return c >= 1.6 ? 1 : ( 3.535 * c + 2.181 * c2 ) / ( 1 + 2.276 * c + 2.577 * c2 );
}

float geometricSmithBeckmann( float3 n, float3 l, float3 v, float roughness )
{
	float lambda = geometricRoughnessMapping( roughness );

	float GGX1 = geometricBeckmann( n, l, lambda );
	float GGX2 = geometricBeckmann( n, v, lambda );

	return GGX1 * GGX2;
}

float3 cookTorranceSpecularBRDF( in float3 N, in float3 V, in float3 H, in float3 F, in float3 L, in float roughness )
{
#ifdef SPECULAR_BRDF_GGX
	float   G = geometricSmithGGX( N, L, V, roughness );
	float NDF = normalDistributionGGX( N, H, roughness );
#elif defined( SPECULAR_BRDF_Schlick )
	float   G = geometricSmithSchlickGGX( N, L, V, roughness );
	float NDF = normalDistributionGGX( N, H, roughness );
#elif defined( SPECULAR_BRDF_Beckmann )
	float   G = geometricSmithBeckmann( N, L, V, roughness );
	float NDF = normalDistributionBeckmann( N, H, roughness );
#endif

	float NdotL = saturate( dot( N, L ) );
	float NdotV = saturate( dot( N, V ) );

	float normalization = max( ( 4 * NdotL * NdotV ), 0.001 );

#ifdef DEBUG_LIGHTING__F_ONLY
	return F / normalization;
#elif defined( DEBUG_LIGHTING__G_ONLY )
	return G / normalization;
#elif defined(  DEBUG_LIGHTING__NDF_ONLY )
	return NDF / normalization;
#else
	return ( F * G * NDF ) / normalization;
#endif
}


float3 calculateF0( float3 albido, float3 ks, float metalness )
{
	return lerp( 0.04f, ks * albido.rgb, metalness );
}




///////////////
// Lighting //
/////////////
// +---------------------------------------------------------------------------------------+
// | Direction Light                                                                       |
// +---------------------------------------------------------------------------------------+
float3 calculateLightBRDF(
	float3 radiance,
	float3 L,
	float3 V,
	float3 N,
	float3 diffuse,
	float3 F0,
	float roughness
)
{
	float3 H = normalize( V + L );

#ifdef FRESNEL_Schlick
	float3 F = fresnelSchlick( F0, dot( H, V ) );
#elif defined( FRESNEL_Epic )
	float3 F = fresnelSchlickEpic( F0, dot( V, H ) );
#endif

	float3 specular = cookTorranceSpecularBRDF( N, V, H, F, L, roughness );

	float NdotL = saturate( dot( N, L ) );

	return ( clamp( specular, 0, 2 ) + diffuse * ( 1 - F ) ) * radiance * NdotL;
}

// +---------------------------------------------------------------------------------------+
// | Point Light                                                                           |
// +---------------------------------------------------------------------------------------+

float calulatePointLightFallOff( float distance, float radius )
{
	float fallOff = dot( float3( 0.0f, 0.1f, 0.001f ), float3( 1.0f, distance, distance * distance ) );
	fallOff *= smoothstep( radius, radius - radius * 0.1f, distance );
	return 1.0f / fallOff;
}

float calulatePointLightFallOffEpic( float distance, float radius )
{
	float n = pow( saturate( 1 - pow( distance / radius, 4 ) ), 2 );
	float d = pow( distance, 2 ) + 1;
	return n / d;
}

float3 calculatePointLightBRDF_CORRECTED(
	float3 lightPosition,
	float3 radiance,
	float radius,
	float3 P,
	float3 V,
	float3 N,
	float3 diffuse,
	float3 F0,
	float roughness
)
{
	float3 L = lightPosition - P;
	float distance = length( L );

	if( distance > radius )
		return float3( 0.0f, 0.0f, 0.0f );

#ifdef POINT_FALLOFF_STD
	float fallOff = calulatePointLightFallOff( distance, radius );
#elif defined( POINT_FALLOFF_Epic )
	float fallOff = calulatePointLightFallOffEpic( distance, radius );
#endif

	radiance *= fallOff;
	L /= distance; // L = normalize( L )

	return calculateLightBRDF( radiance, L, V, N, diffuse, F0, roughness );
}

float3 calculatePointLightBRDF(
	float3 fragmentPosition,
	float3 lightPosition,
	float3 radiance,
	float radius,
	float3 V,
	float3 N,
	float3 diffuse,
	float3 F0,
	float roughness
)
{
	return calculatePointLightBRDF_CORRECTED(
		lightPosition,
		radiance,
		radius,
		fragmentPosition,
		V,
		N,
		diffuse,
		F0,
		roughness
	);
}



// +---------------------------------------------------------------------------------------+
// | Spot Light                                                                            |
// +---------------------------------------------------------------------------------------+
float calulateSpotLightFallOff( float3 L, float3 lightDirection, float spotlightAngle )
{
	float minCos = cos( radians( spotlightAngle ) );
	float maxCos = lerp( minCos, 1, 0.5f );
	float cosAngle = dot( lightDirection, -L );

	return smoothstep( minCos, maxCos, cosAngle );
}

float3 calculateSpotLightBRDF(
	float3 lightPosition,
	float3 lightDirection,
	float3 radiance,
	float radius,
	float spotlightAngle,
	float3 fragmentPosition,
	float3 V,
	float3 N,
	float3 diffuse,
	float3 F0,
	float roughness
)
{
	float3 L = lightPosition - fragmentPosition;
	float distance = length( L );

	if( distance > radius )
		return float3( 0.0f, 0.0f, 0.0f );

#ifdef POINT_FALLOFF_STD
	float fallOff = calulatePointLightFallOff( distance, radius );
#elif defined( POINT_FALLOFF_Epic )
	radiance *= calulatePointLightFallOffEpic( distance, radius );
#endif

	L /= distance;
	radiance *= calulateSpotLightFallOff( L, lightDirection, spotlightAngle );

	return calculateLightBRDF( radiance, L, V, N, diffuse, F0, roughness );
}

#endif// _LIGHTING_ALGORITHMS_HLSL