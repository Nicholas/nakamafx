#include "algorithms.Noise.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer ssaoCByffer : register( b0 )
{
	// Camera projections for depth testing
	float4x4 view;
	float4x4 projection;

	// Render target texture size
	float4 targetSize;
};

#define KERNEL_SIZE 64

tbuffer samplesTBuffer : register( t0 )
{
	float3 samples[ KERNEL_SIZE ];
}


Texture2D gPosition : register( t1 );
Texture2D gNormal   : register( t2 );
Texture2D gDepth    : register( t3 );
Texture1D Noise     : register( t4 );

SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState wrapSampler : register( s2 );
SamplerState pointSampler : register( s3 );


//////////////
// TYPEDEFS //
//////////////
struct Fragment
{
	float4 position : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment) : SV_TARGET0
{
	float2 uv = fragment.position.xy * 2;

	float4 position = gPosition.Load( int3( uv, 0 ) );
	float bias = ( 1 - exp( -position.z * 0.01 ) ) * -0.2f;

	if( position.w == 0 )
	{
		return float4( 1, 1, 1, 0 );
	}

	float3 normal   =   gNormal.Load( int3( uv, 0 ) ).xyz;

	float3 rvec = normalize( float3(
		snoise2D( (fragment.position.xy + float2(  348.024 , -783.0125 ) ) * 1.22356 ),
		snoise2D( (fragment.position.xy + float2(    8.024 , -100.0125 ) ) * 1.22356 ),
		snoise2D( (fragment.position.xy + float2( -550.0244,  999.4654 ) ) * 1.22356 ) ) );

	normal = mul( view, float4( normal, 0 ) ).xyz;
	normal = normalize( normal );

	float3 tangent   = normalize( rvec - normal * dot( rvec, normal ) );
	float3 bitangent = normalize( cross( normal, tangent ) );
	float3x3 tbn = float3x3( tangent, bitangent, normal );

	float ao = 0.0;
	float offsetScale = 0.975f;

	for( int i = 0; i < KERNEL_SIZE; i++ )
	{
		// Get sample position;
		float3 samplePos = mul( samples[ i ].rgb, tbn );
		samplePos = samplePos * offsetScale + position.rgb;

		// Project the sample
		float4 offset = mul( projection, float4( samplePos, 1 ) );
		offset.xy /= offset.w;
		offset.xy = offset.xy * float2( 0.5f, -0.5f ) + 0.5f;

		float depth = gPosition.SampleLevel( pointSampler, offset.xy, 0 ).b;

		float rangeCheck = smoothstep( 0.0, 1.0, offsetScale / abs( position.z - depth ) );
		ao += step( depth, samplePos.z + bias ) * rangeCheck;
	}
	ao /= KERNEL_SIZE;

	ao *= smoothstep( 512, 128, position.z );

	float3 colour0 = pow( saturate( 1 - ao ), 4.2 );
	return float4( colour0, 1 );
}
