

#ifndef _LIGHT_SHADER_TYPE_
#define _LIGHT_SHADER_TYPE_

#define       POINT_LIGHT 0
#define        SPOT_LIGHT 1
#define DIRECTIONAL_LIGHT 2

/**
struct LightWorldData
{
    // Position for point and spot lights (World space).
    float4   positionWS;
    //--------------------------------------------------------------( 16 bytes )
    ///Direction for spot and directional lights (World space).
    float4   directionWS;
    //--------------------------------------------------------------( 16 bytes )
    //Color of the light. Diffuse and specular colors are not seperated.
    float4   colour;
    //--------------------------------------------------------------( 16 bytes )
    //The half angle of the spotlight cone.
    float    spotlightAngle;

    //The range of the light.
    float    range;
 
    //Disable or enable the light.
    bool     enabled;
 
    //The type of the light.
    uint     type;
    //--------------------------------------------------------------( 16 bytes )
    //--------------------------------------------------------------( 16 * 4 = 64 bytes )
};

struct LightViewData
{
    //Position for point and spot lights (View space).
    float4   positionVS;
    //--------------------------------------------------------------( 16 bytes )
    //Direction for spot and directional lights (View space).
    float4   directionVS;
    //--------------------------------------------------------------( 16 bytes )
    //--------------------------------------------------------------( 16 * 2 = 32 bytes )
};
//*/

struct Light
{
    // Position for point and spot lights (World space).
    float4   positionWS;
    //--------------------------------------------------------------( 16 bytes )
    ///Direction for spot and directional lights (World space).
    float4   directionWS;
    //--------------------------------------------------------------( 16 bytes )
    //Position for point and spot lights (View space).
    float4   positionVS;
    //--------------------------------------------------------------( 16 bytes )
    //Direction for spot and directional lights (View space).
    float4   directionVS;
    //--------------------------------------------------------------( 16 bytes )
    //Color of the light. Diffuse and specular colors are not seperated.
    float4   colour;
    //--------------------------------------------------------------( 16 bytes )
    //The half angle of the spotlight cone.
    float    spotlightAngle;

    //The range of the light.
    float    range;
 
    //Disable or enable the light.
    bool     enabled;
 
    //The type of the light.
    uint     type;
    //--------------------------------------------------------------( 16 bytes )
    //--------------------------------------------------------------( 16 * 6 = 96 bytes )
};

struct LightCluster
{
    uint offset;
    uint count;
};

#endif //_LIGHT_SHADER_TYPE_