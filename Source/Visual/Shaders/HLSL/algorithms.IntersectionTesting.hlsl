#ifndef _INTERSECTION_TESTING_TOOLS_
#define _INTERSECTION_TESTING_TOOLS_

#include "type.Plane.hlsl"
#include "type.Sphere.hlsl"
#include "type.Cone.hlsl"
#include "type.Frustum.hlsl"
#include "type.Ray.hlsl"

#define LEFT_HAND_PLANES
//#define RIGHT_HAND_PLANES

#ifdef RIGHT_HAND_PLANES
#undef LEFT_HAND_PLANES
#endif


// Note this assumes a right handed coord system...
// Therefore -> swap the cross around...
float3 computeNormalRH( float3 p0, float3 p1, float3 p2 )
{
	float3 v0 = p1 - p0;
	float3 v1 = p2 - p0;

	return normalize( cross( v0, v1 ) );
}

float3 computeNormalLH( float3 p0, float3 p1, float3 p2 )
{
	float3 v0 = p1 - p0;
	float3 v1 = p2 - p0;

	return normalize( cross( v1, v0 ) );
}

// Compute a plane from 3 non-colinear points that form a triangle.
Plane ComputePlane( float3 p0, float3 p1, float3 p2 )
{
	Plane p;

#ifdef LEFT_HAND_PLANES
	p.N = computeNormalLH( p0, p1, p2 );
#endif
#ifdef RIGHT_HAND_PLANES
	p.N = computeNormalRH( p0, p1, p2 );
#endif

	p.d = dot( p.N, p0 );

	return p;
}

// Check to see if a point is fully behind (inside the negative halfspace of) a plane.
bool PointInsidePlane( float3 p, Plane plane )
{
	return dot( plane.N, p ) - plane.d < 0;
}

// Check to see if a sphere is fully behind (inside the negative halfspace of) a plane.
// Source: Real-time collision detection, Christer Ericson (2005)
bool SphereInsidePlane( Sphere sphere, Plane plane )
{
	return dot( plane.N, sphere.c ) - plane.d < -sphere.r;
}

// Check to see if a cone if fully behind (inside the negative halfspace of) a plane.
// Source: Real-time collision detection, Christer Ericson (2005)
bool ConeInsidePlane( Cone cone, Plane plane )
{
	// Compute the farthest point on the end of the cone to the positive space of the plane.
	float3 m = cross( cross( plane.N, cone.d ), cone.d );
	float3 Q = cone.T + cone.d * cone.h - m * cone.r;

	// The cone is in the negative halfspace of the plane if both
	// the tip of the cone and the farthest point on the end of the cone to the
	// positive halfspace of the plane are both inside the negative halfspace
	// of the plane.
	return PointInsidePlane( cone.T, plane ) && PointInsidePlane( Q, plane );
}

// Check to see of a point light is partially contained within the frustum.
bool SphereInsideFrustum( Sphere sphere, Frustum frustum )
{
	bool result = true;

	// First check depth
	// Note: Here, the view vector points in the -Z axis so the
	// far depth value will be approaching -infinity.
#ifdef LEFT_HAND_PLANES
	if ( sphere.c.z - sphere.r > frustum.zFar || sphere.c.z + sphere.r < frustum.zNear )
	{
		result = false;
	}
#endif
#ifdef RIGHT_HAND_PLANES
	if ( sphere.c.z - sphere.r > frustum.zNear || sphere.c.z + sphere.r < frustum.zFar )
	{
		result = false;
	}
#endif

	// Then check frustum planes
	for ( int i = 0; i < PLANE_COUNT && result; i++ )
	{
		if ( SphereInsidePlane( sphere, frustum.planes[ i ] ) )
		{
			result = false;
		}
	}

	return result;
}

// Check to see of a spot light is partially contained within the frustum.
bool ConeInsideFrustum( Cone cone, Frustum frustum )
{
	bool result = true;

#ifdef LEFT_HAND_PLANES
	Plane nearPlane = { float3( 0, 0, -1 ), -frustum.zFar  };
	Plane  farPlane = { float3( 0, 0,  1 ),  frustum.zNear };
#endif
#ifdef RIGHT_HAND_PLANES
	Plane nearPlane = { float3( 0, 0, -1 ), -frustum.zNear };
	Plane  farPlane = { float3( 0, 0,  1 ),  frustum.zFar  };
#endif

	// First check the near and far clipping planes.
	if ( ConeInsidePlane( cone, nearPlane ) || ConeInsidePlane( cone, farPlane ) )
	{
		result = false;
	}

	// Then check frustum planes
	for ( int i = 0; i < PLANE_COUNT && result; i++ )
	{
		if ( ConeInsidePlane( cone, frustum.planes[i] ) )
		{
			result = false;
		}
	}

	return result;
}


// Ray sphere intersection.
//
// @param Ray: The ray to test.
// @param Sphere: The shere to test.
// @param out float2: The two intersection points with the
//					  sphere as distance along the ray from its
//					  origin.
// @return The success of the intersection test. False if the ray does
//			intersect the sphere.
bool intersect( in Ray ray, in Sphere sphere, out float iMin, out float iMax )
{
	float3 oc = ray.o - sphere.c;
	float b = 2.0 * dot( ray.d, oc );
	float c = dot( oc, oc ) - sphere.r * sphere.r;
	float disc = b * b - 4.0 * c;

	if( disc < 0.0 )
	{
		iMax = 1;
		iMin = 1;
		return false;
	}

	float q;

	if( b < 0.0 )
	{
		q = ( -b - sqrt( disc ) ) * 0.5;
	}
	else
	{
		q = ( -b + sqrt( disc ) ) * 0.5;
	}

	iMin = q;
	iMax = c / q;

	// make sure iMin is smaller than iMax
	if( iMin > iMax )
	{
		// if iMin is bigger than iMax swap them around
		float temp = iMin;
		iMin = iMax;
		iMax = temp;
	}

	// if iMax is less than zero, the object is in the ray's negative direction
	// and consequently the ray misses the sphere
	if( iMax < 0.0 )
	{
		iMax = 0;
		iMin = 1;
		return false;
	}
	if( iMin < 0.0 )
	{
		iMin = 0;
	}

	return true;
}

// Ray sphere intersection.
//
// @param Ray: The ray to test.
// @param Plane: The Plane to test.
// @param out float: The distance along the ray to the point of intersection with the plane.
// @return The success of the intersection test. False if the ray does
//			intersect the plane.
bool intersect( in Ray ray, in Plane2 plane, out float distance )
{
	// assuming vectors are all normalized
	float denom = dot( plane.N, ray.d );
	if( denom > 1e-6 )
	{
		float3 p0l0 = plane.p - ray.o;
		distance = dot( p0l0, plane.N ) / denom;
		return distance >= 0;
	}

	return false;
}


#endif //_INTERSECTION_TESTING_TOOLS_
