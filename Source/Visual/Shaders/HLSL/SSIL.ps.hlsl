#include "algorithms.Noise.hlsl"

/////////////
// GLOBALS //
/////////////
cbuffer ssaoCByffer : register( b0 )
{
	// Camera projections for depth testing
	float4x4 view;
	float4x4 projection;

	// Render target texture size
	float4 targetSize;
};

#define REFECTION_SAMPLE_STEP 1.1f
#define REFECTION_SAMPLE_SIZE 16
#define REFECTION_SAMPLE_KERNEL_SIZE 1
#define KERNEL_SIZE 24

tbuffer samplesTBuffer : register( t0 )
{
	float3 samples[ KERNEL_SIZE ];
}


Texture2D gColour   : register( t1 );
Texture2D gPosition : register( t2 );
Texture2D gNormal   : register( t3 );
Texture2D gDepth    : register( t4 );
Texture1D Noise     : register( t5 );

SamplerState samplerState : register( s0 );
SamplerComparisonState cmpSampler : register( s1 );
SamplerState wrapSampler : register( s2 );
SamplerState pointSampler : register( s3 );


//////////////
// TYPEDEFS //
//////////////
struct Fragment
{
	float4 position : SV_POSITION;
	float2 textureUV : TEXCOORD0;
};

float3 getPositionSS( float3 positionWS )
{
	// Update the sample position.
	float4 currentUV = mul( projection, float4( positionWS, 1 ) );

	currentUV.xy /= currentUV.w;
	currentUV.xy *= float2( 0.5f, -0.5f );
	currentUV.xy += 0.5f;

	return currentUV.xyz * float3( targetSize.xy, 1 );
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main( Fragment fragment) : SV_TARGET0
{
	//float2 uv = fragment.position.xy * 2;
	float2 uv = fragment.position.xy * 2 + float2( floor( fragment.position.y % 1 ), 0 );

	float4 positionVS = gPosition.Load( int3( uv, 0 ) );
	float bias = exp( -positionVS.z * 0.5 ) * 0.25f;

	if( positionVS.w == 0 )
	{
		return float4( 0, 0, 0, 0 );
	}

	float3 normalVS   =   gNormal.Load( int3( uv, 0 ) ).xyz;

	normalVS = mul( view, float4( normalVS, 0 ) ).xyz;
	normalVS = normalize( normalVS );

	float3 viewVS = normalize( positionVS.xyz );

	float NdotV = saturate( dot( normalVS, viewVS ) );

	//float3 rvec = float3( 0, 1, 0 );
	//float3 rvec = normalVS;
	//float3 rvec = normalize( float3(
	//	snoise2D( (fragment.position.xy + float2(  348.024 , -783.0125 ) ) * 43.22356 ),
	//	snoise2D( (fragment.position.xy + float2( -550.0244,  999.4654 ) ) * 43.22356 ),
	//	snoise2D( (fragment.position.xy + float2(    8.024 , -100.0125 ) ) * 43.22356 ) * 0.5f + 0.5f ) );
	float3 rvec = normalize( float3(
		hash( (fragment.position.xy + float2(  3.024 , -7.0125 ) ) * 1.2356 ),
		hash( (fragment.position.xy + float2( -5.0244,  9.4654 ) ) * 1.2236 ),// * 0,// * 0.25f + 0.75f,
		//hash( (fragment.position.xy + float2(  8.024 , -1.0125 ) ) * 1.2235 ) * 0.5f + 0.5f ) ); //
		hash( (fragment.position.xy + float2(  8.024 , -1.0125 ) ) * 1.2235 ) * 0.1f + 0.1f ) ); //


	//float3 tangent   = normalize( rvec - normalVS * dot( rvec, normalVS ) );
	//float3 tangent   = normalize( rvec - normalVS * dot( rvec, normalVS ) );
	float3 tangent   = normalize( cross( rvec, normalVS ) );
	float3 bitangent = normalize( cross( normalVS, tangent ) );
	float3x3 tbn = float3x3( tangent, bitangent, normalVS );

	//float3 ao = gColour.Sample( pointSampler, uv.xy ).rgb;
	float3 ao = 0;
	float offsetScale = 0.975f;

	float3 positionSS = getPositionSS( positionVS.xyz );

	float count = 0;

	/**/
	//[ fastopt ]
	for( int i = 0; i < KERNEL_SIZE; i++ )
	{
		// Get sample position;
		//float3 samplePos = mul( samples[ i ].rgb, tbn );
		//samplePos = samplePos * offsetScale + positionVS.rgb;

		// Project the sample
		//float4 offset = mul( projection, float4( samplePos, 1 ) );
		//offset.xy /= offset.w;
		//offset.xy = offset.xy * float2( 0.5f, -0.5f ) + 0.5f;
		//float3 offset = getPositionSS( samplePos );

		//float depth = gPosition.SampleLevel( pointSampler, offset.xy, 0 ).b;

		//float rangeCheck = smoothstep( 0.0, 1.0, offsetScale / abs( positionVS.z - depth ) );
		//ao += step( depth, samplePos.z + bias ) * rangeCheck;

		//float3 R = normalize( reflect( viewVS, normal ) );
		float3 R = mul( samples[ i ].rgb, tbn );

		bool searchingForReflection = true;
		float3 currentPosition = positionVS.xyz;
		float3 currentUV = positionSS;
		float2 lastUV = 0;
		float lastDepth = 0;
		//float currentLength = 1.5 + bias;
		//float currentLength = 1.5;
		float currentLength = 0.5;

		//[ unroll ]
		//[ fastopt ]
		for( int j = 0; j < REFECTION_SAMPLE_SIZE; j++ )
		{
			if( searchingForReflection )
			{
				lastDepth = currentPosition.z;
				lastUV = currentUV.xy;

				// Make step along the reflection vector.
				currentPosition = positionVS.xyz + R * currentLength;

				// Update the sample position.
				currentUV = getPositionSS( currentPosition );

				// Get the the depth at the samples position.
				//float3 sampleDepth = gPosition.Sample( pointSampler, currentUV.xy ).xyz;
				float4 sampleDepth = gPosition.Load( float3( currentUV.xy, 0 ) );


				// Get a few samples...
				// This is a way to try and get a smoother reflection.
				// There are several other ways of trying to do this to get good results...
				float maxD = sampleDepth.z;
				//[ unroll ]
				//for( int k = 0; k < KERNEL_SIZE; k++ )
				//{
					//if( abs( currentPosition.z - sampleDepth.z ) < ( 0.001f * pow( j, 1.1 ) ) )
					//if( abs( currentPosition.z - sampleDepth.z ) < 0.001f * pow( j, 1.1 )  )
					float diff = sampleDepth.z - currentPosition.z;
					float b = pow( j, 1.1 );
					//if( diff < -0.0001f )// && diff > -2.05 )
					if( diff < ( -0.0001f + b * 0.5 ) && diff > - ( 0.005 + b )  )
					{
						searchingForReflection = false;
						j = REFECTION_SAMPLE_SIZE;

						float3 colour = gColour.SampleLevel( pointSampler, currentUV.rg * targetSize.zw, 0 );
						//float3 colour = gColour.SampleLevel( samplerState, currentUV.xy * targetSize.zw, 0 );
						//float3 colour = gColour.Load( float3( currentUV.xy, 0 ) );
						float3 normal = gNormal.Load( float3( currentUV.xy, 0 ) ).xyz;
							   normal = normalize( mul( view, float4( normal, 0 ) ).xyz );

						float dist = length( positionVS.xyz - currentPosition );
						//calulatePointLightFallOff
						ao += colour * smoothstep( -0.0001, 0.01, dot( normal, -R ) ) * step( 0.0000001, sampleDepth.w ) * exp( -dist * 0.05f );
						++count;
					}

				//	float2 offset = samples[ j ].xy * targetSize.zw;
				//	sampleDepth = gPosition.Sample( pointSampler, currentUV.xy + offset ).xyz;
				//	maxD = max( sampleDepth.z, maxD );
				//}

				//float3 updatePosition = float3( currentPosition.xy, sampleDepth.z );
				//currentLength += max( ( maxD - currentPosition.z ) * 0.3, 0.075 );
				//currentLength += max( ( sampleDepth.z - currentPosition.z ) * 0.5, 2.5 );
				//currentLength += REFECTION_SAMPLE_STEP;
				//currentLength += REFECTION_SAMPLE_STEP - saturate( exp( ( currentPosition.z - sampleDepth.z ) * 0.75 ) ) * 1.0f * ( NdotV * 0.25f + 0.5 );
				currentLength += 1.5f - saturate( exp( ( currentPosition.z - sampleDepth.z ) * 0.5 ) ) * 1.2f * ( NdotV * 0.5f + 0.5 );
				//currentLength += ( NdotV * 0.5f + 0.5 ) * 2.5f;
			}
		}


	}
	//*/
	//ao /= KERNEL_SIZE;
	ao /= count;

	float3 colour0 = ao;
	return float4( colour0, 1 );
}
