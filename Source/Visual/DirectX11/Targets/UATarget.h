#pragma once

#ifndef _UNORDERED_ACCESS_TARGET_DX11_H
#define _UNORDERED_ACCESS_TARGET_DX11_H

// Unordered Access Target
//
// A generic RW buffer to be used by pixel and compute shaders
//
// Project   : NaKama-Fx
// File Name : UATarget.h
// Date      : 27-11-2018
// Author    : Nicholas Welters

#include "../APITypeDefs.h"

namespace visual
{
namespace directX11
{
	/// <summary>
	/// A wrapper for the unordered access view structures
	/// (buffers and views) that are needed to work with the data.
	/// </summary>
	class UATarget
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pColourTextures"> The Data Buffers. </param>
		/// <param name="pRenderTargetViews"> The Datas Unordered Acecss Views. </param>
		/// <param name="pShaderResourceViews"> The Datas Shader Resource Views. </param>
		UATarget(
			const std::vector< ID3D11BufferSPtr >& pBuffers,
			const std::vector< ID3D11UnorderedAccessViewSPtr >& pUnorderedAccessViews,
			const std::vector< ID3D11ShaderResourceViewSPtr >& pShaderResourceViews
		);

		/// <summary> ctor </summary>
		UATarget( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		UATarget( const UATarget& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		UATarget( UATarget&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~UATarget( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		UATarget& operator=( const UATarget& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		UATarget& operator=( UATarget&& move ) = default;


		/// <summary> Getters for the unordered access views. </summary>
		/// <returns> A vector of unordered access view pointers. </returns>
		const std::vector< ID3D11UnorderedAccessViewSPtr >& getUnorderedAccessViews( ) const;

		/// <summary> Getters for the shader resource views. </summary>
		/// <returns> A vector of shader resource view pointers. </returns>
		const std::vector< ID3D11ShaderResourceViewSPtr >& getShaderResourceViews( ) const;

	private:
		/// <summary> The data buffer for the unordered access view. </summary>
		std::vector< ID3D11BufferSPtr > pBuffers;

		/// <summary> The write view of the data. </summary>
		std::vector< ID3D11UnorderedAccessViewSPtr > pUnorderedAccessViews;

		/// <summary> The read view of the data. </summary>
		std::vector< ID3D11ShaderResourceViewSPtr > pShaderResourceViews;
	};
} // namespace directX11
} // namespace visual
# endif // _MULTIPLE_RENDER_TARGET_DX11_H
