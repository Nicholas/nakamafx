#include "UATarget.h"

// Unordered Access Target
//
// A generic RW buffer to be used by pixel and compute shaders
//
// Project   : NaKama-Fx
// File Name : UATarget.h
// Date      : 27-11-2018
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	using std::vector;

	/// <summary> ctor </summary>
	/// <param name="pColourTextures"> The Data Buffers. </param>
	/// <param name="pRenderTargetViews"> The Datas Unordered Acecss Views. </param>
	/// <param name="pShaderResourceViews"> The Datas Shader Resource Views. </param>
	UATarget::UATarget(
		const vector< ID3D11BufferSPtr >& pBuffers,
		const vector< ID3D11UnorderedAccessViewSPtr >& pUnorderedAccessViews,
		const vector< ID3D11ShaderResourceViewSPtr >& pShaderResourceViews
	)
		: pBuffers( pBuffers )
		, pUnorderedAccessViews( pUnorderedAccessViews )
		, pShaderResourceViews( pShaderResourceViews )
	{ }

	/// <summary> Getters for the unordered access views. </summary>
	/// <returns> A vector of unordered access view pointers. </returns>
	const vector< ID3D11UnorderedAccessViewSPtr >& UATarget::getUnorderedAccessViews( ) const
	{
		return pUnorderedAccessViews;
	}

	/// <summary> Getters for the shader resource views. </summary>
	/// <returns> A vector of shader resource view pointers. </returns>
	const vector< ID3D11ShaderResourceViewSPtr >& UATarget::getShaderResourceViews( ) const
	{
		return pShaderResourceViews;
	}
} // namespace directX11
} // namespace visual
