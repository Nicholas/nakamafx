#include "DepthStencilDeferredTarget.h"

// Depth Stencil Deferred Target
// 
// A readable depth stencil target that can be used for shadow mapping or depth pre-passes and such.
// 
// Project   : NaKama-Fx
// File Name : DepthStencilDeferredTarget.cpp
// Date      : 22-05-2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pTexture"> The Depth Stencils 2D Texture Data. </param>
	/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
	/// <param name="pShaderResourceView"> The Depth Stencils Shader Resource View. </param>
	/// <param name="width"> The width of the target. </param>
	/// <param name="height"> The height of the target. </param>
	DepthStencilDeferredTarget::DepthStencilDeferredTarget(
		const ID3D11Texture2DSPtr& pTexture,
		const ID3D11DepthStencilViewSPtr& pDepthStencilView,
		const ID3D11ShaderResourceViewSPtr& pShaderResourceView,
		const uint32_t width, const uint32_t height
	)
		: DepthStencilTarget( pTexture, pDepthStencilView, width, height )
		, pShaderResourceView( pShaderResourceView )
	{ }
	
	/// <summary> Get the shader resource view for the depth stencil buffer. </summary>
	/// <returns> A pointer to the shader resource view interface. </returns>
	ID3D11ShaderResourceViewSPtr DepthStencilDeferredTarget::getShaderResourceView( ) const
	{
		return pShaderResourceView;
	}
} // namespace directX11
} // namespace visual