#pragma once

// Depth Stencil Target
//
// This is the depth and stencil buffer.
// We will need this do do depth testing and some thing with the
// stencil buffer...
//
// This will only do depth testing and stenciling as it has no colour based
// render targets.
//
// Project   : NaKama-Fx
// File Name : DepthStencilTarget.h
// Date      : 22-05-2015
// Author    : Nicholas Welters

#ifndef _DEPTH_STENCIL_TARGET_DX11_H
#define _DEPTH_STENCIL_TARGET_DX11_H

#include "OutputTarget.h"

#include <memory>

namespace visual
{
namespace directX11
{
	/// <summary> A depth stencil only target. </summary>
	class DepthStencilTarget: public OutputTarget
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pTexture"> The Depth Stencils 2D Texture Data. </param>
		/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
		/// <param name="width"> The width of the target. </param>
		/// <param name="height"> The height of the target. </param>
		DepthStencilTarget(
			const ID3D11Texture2DSPtr& pTexture,
			const ID3D11DepthStencilViewSPtr& pDepthStencilView,
			uint32_t width,
			uint32_t height
		);

		/// <summary> ctor </summary>
		DepthStencilTarget( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DepthStencilTarget( const DepthStencilTarget& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DepthStencilTarget( DepthStencilTarget&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~DepthStencilTarget( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		DepthStencilTarget& operator=( const DepthStencilTarget& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		DepthStencilTarget& operator=( DepthStencilTarget&& move ) = default;


		/// <summary> Set the clear colour of the target. </summary>
		/// <param name="r"> The red value. </param>
		/// <param name="g"> The green value. </param>
		/// <param name="b"> The blue value. </param>
		/// <param name="a"> The alpha value. </param>
		virtual void setClearColour( float r, float g, float b, float a ) final override;

		/// <summary> Get the width of the output target. </summary>
		/// <returns> The width of the target. </returns>
		virtual uint32_t getWidth( ) final override;

		/// <summary> Get the height of the output target. </summary>
		/// <returns> The height of the target. </returns>
		virtual uint32_t getHeight( ) final override;

		/// <summary>
		/// Set the output target of the rendering context so that
		/// the results of the next draw calls will have the results
		/// stored here.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) final override;

		/// <summary>
		/// This is where we clear the output so that we can start again.
		/// The provided context is used to make the needed calls to reset
		/// the targets.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		/// <param name="colour"> Clear the colour. </param>
		/// <param name="depth"> Clear the depth. </param>
		virtual void reset( const DeviceContextPtr_t& pContext, bool colour, bool depth ) final override;

		/// <summary>
		/// This is where we will send the results to the output destination.
		///
		/// Here we might want to double buffer the depth buffer... (temporal depth)
		/// </summary>
		/// <param name="pContext"> The context to use to present the target. </param>
		virtual void present( const DeviceContextPtr_t& pContext ) final override;

		/// <summary> Get the depth stencil render target view. </summary>
		/// <returns> The depth stencil render target view. </returns>
		ID3D11DepthStencilViewSPtr getDepthStencilView( ) const;

	private:
		/// <summary> The Depth Stencils 2D Texture Data. </summary>
		ID3D11Texture2DSPtr pTexture = nullptr;

		/// <summary> The Depth Stencils Render Target View. </summary>
		ID3D11DepthStencilViewSPtr pDepthStencilView = nullptr;

		/// <summary> The width of the target. </summary>
		uint32_t width = 0;

		/// <summary> The height of the target. </summary>
		uint32_t height = 0;
	};
} // namespace directX11
} // namespace visual
#endif // _DEPTH_STENCIL_TARGET_DX11_H

