#pragma once

// Multiple Render Target
//
// This is the standered rendering target,
// it will have at least one colour render target.
// This will be able to hold another 7 colour targets and a depth stencil buffer.
//
// Project   : NaKama-Fx
// File Name : MultipleRenderTarget.h
// Date      : 26-05-2015
// Author    : Nicholas Welters

#ifndef _MULTIPLE_RENDER_TARGET_DX11_H
#define _MULTIPLE_RENDER_TARGET_DX11_H

#include "OutputTarget.h"

#include <memory>

namespace visual
{
namespace directX11
{
	/// <summary>
	/// A render target set that can hold 0 - 8 colour targets
	/// and 0 - 1 depth stencil target.
	/// </summary>
	class MultipleRenderTarget: public OutputTarget
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pColourTextures"> The Colour Buffers. </param>
		/// <param name="pRenderTargetViews"> The Colour Render Target Views. </param>
		/// <param name="pShaderResourceViews"> The Colour Shader Resource Views. </param>
		/// <param name="pDepthTexture"> The Depth Stencils 2D Texture Data. </param>
		/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
		/// <param name="pDepthShaderResourceViews"> The Depth Stencils Shader Resource View. </param>
		/// <param name="width"> The width of the target. </param>
		/// <param name="height"> The height of the target. </param>
		MultipleRenderTarget(
			const std::vector< ID3D11Texture2DSPtr >& pColourTextures,
			const std::vector< ID3D11RenderTargetViewSPtr >& pRenderTargetViews,
			const std::vector< ID3D11ShaderResourceViewSPtr >& pShaderResourceViews,
			const ID3D11Texture2DSPtr& pDepthTexture,
			const ID3D11DepthStencilViewSPtr& pDepthStencilView,
			const ID3D11ShaderResourceViewSPtr& pDepthShaderResourceViews,
			uint32_t width, uint32_t height
		);

		/// <summary> ctor </summary>
		MultipleRenderTarget( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		MultipleRenderTarget( const MultipleRenderTarget& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		MultipleRenderTarget( MultipleRenderTarget&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~MultipleRenderTarget( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		MultipleRenderTarget& operator=( const MultipleRenderTarget& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		MultipleRenderTarget& operator=( MultipleRenderTarget&& move ) = default;


		/// <summary> Set the clear colour of the target. </summary>
		/// <param name="r"> The red value. </param>
		/// <param name="g"> The green value. </param>
		/// <param name="b"> The blue value. </param>
		/// <param name="a"> The alpha value. </param>
		virtual void setClearColour( float r, float g, float b, float a ) final override;

		/// <summary> Get the width of the output target. </summary>
		/// <returns> The width of the target. </returns>
		virtual uint32_t getWidth( ) final override;

		/// <summary> Get the height of the output target. </summary>
		/// <returns> The height of the target. </returns>
		virtual uint32_t getHeight( ) final override;

		/// <summary>
		/// Set the output target of the rendering context so that
		/// the results of the next draw calls will have the results
		/// stored here.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) final override;

		/// <summary>
		/// This is where we clear the output so that we can start again.
		/// The provided context is used to make the needed calls to reset
		/// the targets.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		/// <param name="clearColour"> Clear the colour. </param>
		/// <param name="clearDepth"> clear the depth. </param>
		virtual void reset( const DeviceContextPtr_t& pContext, bool clearColour, bool clearDepth ) final override;

		/// <summary>
		/// This is where we will send the results to the output destination.
		/// This could be called at reset?
		/// </summary>
		/// <param name="pContext"> The context to use to present the target. </param>
		virtual void present( const DeviceContextPtr_t& pContext ) override;


		/// <summary> Get the set of render target views. </summary>
		/// <returns> A vector of render target view pointers. </returns>
		const std::vector< ID3D11RenderTargetViewSPtr >& getRenderTargetViews( ) const;

		/// <summary> Get the set of render target shader resource views. </summary>
		/// <returns> A vector of shader resource view pointers. </returns>
		const std::vector< ID3D11ShaderResourceViewSPtr >& getShaderResourceViews( ) const;

		/// <summary> Get the depth stencil shader resource view. </summary>
		/// <returns> The depth stencil shader resource view. </returns>
		ID3D11ShaderResourceViewSPtr getDepthShaderResourceViews( ) const;

		/// <summary> Get the depth stencil render target view. </summary>
		/// <returns> The depth stencil render target view. </returns>
		ID3D11DepthStencilViewSPtr getDepthStencilView( ) const;

	private:
		/// <summary> The number of colour render targets. </summary>
		size_t size = 0;

		/// <summary> The colour render target buffers. </summary>
		std::vector< ID3D11Texture2DSPtr > pColourTextures = { };

		/// <summary> The colour render target views. </summary>
		std::vector< ID3D11RenderTargetViewSPtr > pRenderTargetViews = { };

		/// <summary> The colour shader resource views. </summary>
		std::vector< ID3D11ShaderResourceViewSPtr > pShaderResourceViews = { };

		/// <summary> The depth stencil render target buffers. </summary>
		ID3D11Texture2DSPtr pDepthTexture = nullptr;

		/// <summary> The depth stencil render target views. </summary>
		ID3D11DepthStencilViewSPtr pDepthStencilView = nullptr;

		/// <summary> The depth stencil shader resource views. </summary>
		ID3D11ShaderResourceViewSPtr pDepthShaderResourceViews = nullptr;

		/// <summary> The width of the target. </summary>
		uint32_t width  = 0;

		/// <summary> The height of the target. </summary>
		uint32_t height = 0;

		/// <summary> The maximum number of render targets we can draw to. </summary>
		static const int maxTargetCount = 8;

		/// <summary> An array of render target views thts used to pass the colour targets to the GPU. </summary>
		ID3D11RenderTargetView* pTargets[ maxTargetCount ] = { nullptr, nullptr, nullptr, nullptr,
																nullptr, nullptr, nullptr, nullptr };

		/// <summary> The clear colour array layout and size enum. </summary>
		enum ColourChannel
		{
			R = 0, G = 1, B = 2, A = 3,
			COLOUR_CHANNEL_COUNT
		};

		/// <summary> The clear colour array. </summary>
		float colour[ COLOUR_CHANNEL_COUNT ] = { 0, 0, 0, 0 };
	};
} // namespace directX11
} // namespace visual
# endif // _MULTIPLE_RENDER_TARGET_DX11_H
