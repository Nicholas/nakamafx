#include "DepthStencilTarget.h"

// Depth Stencil Target
//
// This is the depth and stencil buffer.
// We will need this do do depth testing and some thing with the
// stencil buffer...
//
// This will only do depth testing and stenciling as it has no colour based
// render targets.
//
// Project   : NaKama-Fx
// File Name : DepthStencilTarget.cpp
// Date      : 22-05-2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pTexture"> The Depth Stencils 2D Texture Data. </param>
	/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
	/// <param name="width"> The width of the target. </param>
	/// <param name="height"> The height of the target. </param>
	DepthStencilTarget::DepthStencilTarget(
		const ID3D11Texture2DSPtr& pTexture,
		const ID3D11DepthStencilViewSPtr& pDepthStencilView,
		const uint32_t width,
		const uint32_t height
	)
		: pTexture( pTexture )
		, pDepthStencilView( pDepthStencilView )
		, width( width )
		, height( height )
	{ }

	/// <summary> Set the clear colour of the target. </summary>
	/// <param name="r"> The red value. </param>
	/// <param name="g"> The green value. </param>
	/// <param name="b"> The blue value. </param>
	/// <param name="a"> The alpha value. </param>
	void DepthStencilTarget::setClearColour( float r, float g, float b, float a )
	{ }

	/// <summary> Get the width of the output target. </summary>
	/// <returns> The width of the target. </returns>
	uint32_t DepthStencilTarget::getWidth( )
	{
		return width;
	}

	/// <summary> Get the height of the output target. </summary>
	/// <returns> The height of the target. </returns>
	uint32_t DepthStencilTarget::getHeight( )
	{
		return height;
	}

	/// <summary>
	/// Set the output target of the rendering context so that
	/// the results of the next draw calls will have the results
	/// stored here.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void DepthStencilTarget::set( const DeviceContextPtr_t& pContext )
	{
		pContext->OMSetRenderTargets( 0, nullptr, pDepthStencilView.get( ) );
	}

	/// <summary>
	/// This is where we clear the output so that we can start again.
	/// The provided context is used to make the needed calls to reset
	/// the targets.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	/// <param name="colour"> Clear the colour. </param>
	/// <param name="depth"> clear the depth. </param>
	void DepthStencilTarget::reset( const DeviceContextPtr_t& pContext, bool colour, bool depth )
	{
		pContext->ClearDepthStencilView(
			pDepthStencilView.get( ),
			D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
			1.0f,
			0xff );
	}

	/// <summary>
	/// This is where we will send the results to the output destination.
	///
	/// Here we might want to double buffer the depth buffer... (temporal depth)
	/// </summary>
	/// <param name="pContext"> The context to use to present the target. </param>
	void DepthStencilTarget::present( const DeviceContextPtr_t& pContext )
	{
		// Uh we don't have to do anything with the depth buffer???
	}

	/// <summary> Get the depth stencil render target view. </summary>
	/// <returns> The depth stencil render target view. </returns>
	ID3D11DepthStencilViewSPtr DepthStencilTarget::getDepthStencilView( ) const
	{
		return pDepthStencilView;
	}
} // namespace directX11
} // namespace visual