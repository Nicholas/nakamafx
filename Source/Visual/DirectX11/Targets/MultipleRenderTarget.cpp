#include "MultipleRenderTarget.h"

#ifdef WINDOWS_7
#include <D3D11.h>
#elif defined( WINDOWS_8 )
//#include <d3d11_1.h>
#include <d3d11_2.h>
#elif defined( WINDOWS_10 )
#include <d3d11_4.h>
#endif

namespace visual
{
namespace directX11
{
	using std::vector;

	/// <summary> ctor </summary>
	/// <param name="name"> The render targets set unique identifier </param>
	/// <param name="pColourTextures"> The Colour Buffers. </param>
	/// <param name="pRenderTargetViews"> The Colour Render Target Views. </param>
	/// <param name="pShaderResourceViews"> The Colour Shader Resource Views. </param>
	/// <param name="pDepthTexture"> The Depth Stencils 2D Texture Data. </param>
	/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
	/// <param name="pDepthShaderResourceViews"> The Depth Stencils Shader Resource View. </param>
	/// <param name="width"> The width of the target. </param>
	/// <param name="height"> The height of the target. </param>
	MultipleRenderTarget::MultipleRenderTarget(
		const vector< ID3D11Texture2DSPtr >& pColourTextures,
		const vector< ID3D11RenderTargetViewSPtr >& pRenderTargetViews,
		const vector< ID3D11ShaderResourceViewSPtr >& pShaderResourceViews,
		const ID3D11Texture2DSPtr& pDepthTexture,
		const ID3D11DepthStencilViewSPtr& pDepthStencilView,
		const ID3D11ShaderResourceViewSPtr& pDepthShaderResourceViews,
		const uint32_t width, const uint32_t height
	)
		: OutputTarget( )
		, size( pColourTextures.size( ) > maxTargetCount ? maxTargetCount : pColourTextures.size( ) )
		, pColourTextures( pColourTextures )
		, pRenderTargetViews( pRenderTargetViews )
		, pShaderResourceViews( pShaderResourceViews )
		, pDepthTexture( pDepthTexture )
		, pDepthStencilView( pDepthStencilView )
		, pDepthShaderResourceViews( pDepthShaderResourceViews )
		, width( width )
		, height( height )
		, pTargets{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr }
		, colour{ 0.1f, 0.1f, 0.1f, 0.0f }
	{
		for( size_t i = 0; i < size; ++i )
		{
			pTargets[ i ] = pRenderTargetViews[ i ].get( );
		}
	}

	/// <summary> Set the clear colour of the target. </summary>
	/// <param name="r"> The red value. </param>
	/// <param name="g"> The green value. </param>
	/// <param name="b"> The blue value. </param>
	/// <param name="a"> The alpha value. </param>
	void MultipleRenderTarget::setClearColour( const float r, const float g, const float b, const float a )
	{
		colour[ R ] = r;
		colour[ G ] = g;
		colour[ B ] = b;
		colour[ A ] = a;
	}

	/// <summary> Get the width of the output target. </summary>
	/// <returns> The width of the target. </returns>
	uint32_t MultipleRenderTarget::getWidth( )
	{
		return width;
	}

	/// <summary> Get the height of the output target. </summary>
	/// <returns> The height of the target. </returns>
	uint32_t MultipleRenderTarget::getHeight( )
	{
		return height;
	}

	/// <summary>
	/// Set the output target of the rendering context so that
	/// the results of the next draw calls will have the results
	/// stored here.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void MultipleRenderTarget::set( const DeviceContextPtr_t & pContext )
	{
		pContext->OMSetRenderTargets( 8, pTargets, pDepthStencilView.get() );
	}

	/// <summary>
	/// This is where we clear the output so that we can start again.
	/// The provided context is used to make the needed calls to reset
	/// the targets.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	/// <param name="clearColour"> Clear the colour. </param>
	/// <param name="clearDepth"> clear the depth. </param>
	void MultipleRenderTarget::reset( const DeviceContextPtr_t & pContext,
									  const bool clearColour,
									  const bool clearDepth )
	{
		if( clearColour )
		{
			for( size_t i = 0; i < size; i++ )
			{
				pContext->ClearRenderTargetView( pRenderTargetViews[ i ].get( ), colour );
			}
		}

		if( clearDepth )
		{
			pContext->ClearDepthStencilView( pDepthStencilView.get( ), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0xff );
		}
	}

	/// <summary>
	/// This is where we will send the results to the output destination.
	/// Though it might already be there...
	/// So far only things attached to a real system output seem to use this.
	/// </summary>
	/// <param name="pContext"> The context to use to present the target. </param>
	void MultipleRenderTarget::present( const DeviceContextPtr_t& pContext )
	{ }

	/// <summary> Get the set of render target views. </summary>
	/// <returns> A vector of render target view pointers. </returns>
	const std::vector< ID3D11RenderTargetViewSPtr >& MultipleRenderTarget::getRenderTargetViews( ) const
	{
		return pRenderTargetViews;
	}

	/// <summary> Get the set of render target shader resource views. </summary>
	/// <returns> A vector of shader resource view pointers. </returns>
	const vector< ID3D11ShaderResourceViewSPtr >& MultipleRenderTarget::getShaderResourceViews( ) const
	{
		return pShaderResourceViews;
	}

	/// <summary> Get the depth stencil shader resource view. </summary>
	/// <returns> The depth stencil shader resource view. </returns>
	ID3D11ShaderResourceViewSPtr MultipleRenderTarget::getDepthShaderResourceViews( ) const
	{
		return pDepthShaderResourceViews;
	}

	/// <summary> Get the depth stencil render target view. </summary>
	/// <returns> The depth stencil render target view. </returns>
	ID3D11DepthStencilViewSPtr MultipleRenderTarget::getDepthStencilView( ) const
	{
		return pDepthStencilView;
	}
} // namespace directX11
} // namespace visual
