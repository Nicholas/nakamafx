#pragma once

// DX11 Render target wrapper interface.
// This is used to group all render target information togetrher.
// This includes the render target surface(s),
//  the depth stencil buffer and the clear values for the colours, depth and stencil.
//
// Extending thins interface we can create multiple render targets by holding more surfaces
// or even a render target swap chain such as the DXGI swap chain but for some other device like decklink.
//
// Project   : NaKama-Fx
// File Name : MSAARenderTarget.h
// Date      : 21/02/2017
// Author    : Nicholas Welters

#ifndef _MSAA_RENDER_TARGET_DX11_H
#define _MSAA_RENDER_TARGET_DX11_H

#include "MultipleRenderTarget.h"

namespace visual
{
namespace directX11
{
	/// <summary>
	/// A render target set that can hold 0 - 8 colour targets
	/// and 0 - 1 depth stencil target.
	///
	/// Then performs a resolve pass so that we can use the
	/// resulting texture in a pass that doesn't accespt MSAA textures.
	/// </summary>
	class MSAARenderTarget: public MultipleRenderTarget
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pColourTextures"> The Colour Buffers. </param>
		/// <param name="pRenderTargetViews"> The Colour Render Target Views. </param>
		/// <param name="pShaderResourceViews"> The Colour Shader Resource Views. </param>
		/// <param name="pDepthTexture"> The Depth Stencils 2D Texture Data. </param>
		/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
		/// <param name="pDepthShaderResourceViews"> The Depth Stencils Shader Resource View. </param>
		/// <param name="width"> The width of the target. </param>
		/// <param name="height"> The height of the target. </param>
		/// <param name="pResolvedColourTextures"> The resolved texture buffers. </param>
		/// <param name="pResolvedColourSRVs"> The resolved textures sahder resource views. </param>
		/// <param name="colourFormat"> The resolve pass texture format. </param>
		MSAARenderTarget(
			const std::vector< ID3D11Texture2DSPtr >&          pColourTextures,
			const std::vector< ID3D11RenderTargetViewSPtr >&   pColourTargetViews,
			const std::vector< ID3D11ShaderResourceViewSPtr >& pColourSRVs,
			const ID3D11Texture2DSPtr&          pDepthTexture,
			const ID3D11DepthStencilViewSPtr&   pDepthStencilView,
			const ID3D11ShaderResourceViewSPtr& pDepthSRVs,

			const uint32_t& width, const uint32_t& height,

			const std::vector< ID3D11Texture2DSPtr >&          pResolvedColourTextures,
			const std::vector< ID3D11ShaderResourceViewSPtr >& pResolvedColourSRVs,

			const uint32_t& colourFormat
		);

		/// <summary> ctor </summary>
		MSAARenderTarget( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		MSAARenderTarget( const MSAARenderTarget& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		MSAARenderTarget( MSAARenderTarget&& move ) = default;


		/// <summary> dtor </summary>
		~MSAARenderTarget( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		MSAARenderTarget& operator=( const MSAARenderTarget& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		MSAARenderTarget& operator=( MSAARenderTarget&& move ) = default;

		/// <summary>
		/// This is where we will send the results to the output destination.
		/// This could be called at reset?
		/// </summary>
		/// <param name="pContext"> The context to use to present the target. </param>
		virtual void present( const DeviceContextPtr_t& pContext ) final override;

	private:
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Fields
		////////////////////////////////////////////////////////////////////////////////////////////////////
		std::vector< ID3D11Texture2DSPtr > pMSAAColourTextures = { };
		ID3D11Texture2DSPtr                pMSAADepthTexture = nullptr;

		std::vector< ID3D11Texture2DSPtr >          pResolvedColourTextures = { };
		std::vector< ID3D11ShaderResourceViewSPtr > pResolvedColourSRVs = { };

		uint32_t colourFormat = 0;
	};
} // namespace directX11
} // namespace visual

#endif // _MSAA_RENDER_TARGET_DX11_H
