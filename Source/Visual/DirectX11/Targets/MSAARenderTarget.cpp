
#include "MSAARenderTarget.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
/// 
/// DX11 Render target wrapper interface.
/// This is used to group all render target information together.
/// This includes the render target surface(s),
///  the depth stencil buffer and the clear values for the colours, depth and stencil.
/// 
/// Extending thins interface we can create multiple render targets by holding more surfaces
/// or even a render target swap chain such as the DXGI swap chain but for some other device like decklink.
/// 
/// Nicholas Welters, 21/02/2017.
///
////////////////////////////////////////////////////////////////////////////////////////////////////

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	using std::vector;

	/// <summary> ctor </summary>
	/// <param name="pColourTextures"> The Colour Buffers. </param>
	/// <param name="pRenderTargetViews"> The Colour Render Target Views. </param>
	/// <param name="pShaderResourceViews"> The Colour Shader Resource Views. </param>
	/// <param name="pDepthTexture"> The Depth Stencils 2D Texture Data. </param>
	/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
	/// <param name="pDepthShaderResourceViews"> The Depth Stencils Shader Resource View. </param>
	/// <param name="width"> The width of the target. </param>
	/// <param name="height"> The height of the target. </param>
	/// <param name="pResolvedColourTextures"> The resolved texture buffers. </param>
	/// <param name="pResolvedColourSRVs"> The resolved textures sahder resource views. </param>
	/// <param name="colourFormat"> The resolve pass texture format. </param>
	MSAARenderTarget::MSAARenderTarget(
			const vector< ID3D11Texture2DSPtr >&          pColourTextures,
			const vector< ID3D11RenderTargetViewSPtr >&   pColourTargetViews,
			const vector< ID3D11ShaderResourceViewSPtr >& pColourSRVs,
			const ID3D11Texture2DSPtr&          pDepthTexture,
			const ID3D11DepthStencilViewSPtr&   pDepthStencilView,
			const ID3D11ShaderResourceViewSPtr& pDepthSRVs,

			const uint32_t& width, const uint32_t& height,

			const vector< ID3D11Texture2DSPtr >&          pResolvedColourTextures,
			const vector< ID3D11ShaderResourceViewSPtr >& pResolvedColourSRVs,
		
			const uint32_t& colourFormat
		)
		: MultipleRenderTarget(
			pColourTextures,
			pColourTargetViews,
			pColourSRVs,
			pDepthTexture,
			pDepthStencilView,
			pDepthSRVs,
			width, height
		)
		, pMSAAColourTextures( pColourTextures )
		, pMSAADepthTexture( pDepthTexture )
		, pResolvedColourTextures( pResolvedColourTextures )
		, pResolvedColourSRVs( pResolvedColourSRVs )
		, colourFormat( colourFormat )
	{ }
	
	/// <summary>
	/// This is where we will send the results to the output destination.
	/// This could be called at reset?
	/// </summary>
	/// <param name="pContext"> The context to use to present the target. </param>
	void MSAARenderTarget::present( const DeviceContextPtr_t& pContext )
	{
		const unsigned int subresource = D3D11CalcSubresource(0, 0, 1);

		for ( vector< ID3D11Texture2DSPtr >::size_type i = 0; i < pMSAAColourTextures.size(); i++)
		{
			pContext->ResolveSubresource( 
				pResolvedColourTextures[ i ].get( ), subresource,
				    pMSAAColourTextures[ i ].get( ), subresource,
				static_cast< DXGI_FORMAT >( colourFormat )
			);
		}
	}
} // namespace directX11
} // namespace visual
