#pragma once

// Output Target
//
// This represents the result set of a sequence of draw calls.
// This class will set a render target or other UAV type.
// This will also then be able to forward that data to its
// output destination by call present.
//
// Project   : NaKama-Fx
// File Name : OutputTarget.h
// Date      : 21-05-2015
// Author    : Nicholas Welters

#ifndef _OUTPUT_TARGET_H
#define _OUTPUT_TARGET_H

#include "../APITypeDefs.h"

namespace visual
{
namespace directX11
{
	/// <summary> The draw call targets interface. </summary>
	class OutputTarget
	{
	public:
		/// <summary> pure virtual dtor. </summary>
		virtual ~OutputTarget( ) = 0;

		/// <summary> Set the clear colour of the target. </summary>
		/// <param name="r"> The red value. </param>
		/// <param name="g"> The green value. </param>
		/// <param name="b"> The blue value. </param>
		/// <param name="a"> The alpha value. </param>
		virtual void setClearColour( float r, float g, float b, float a ) = 0;

		/// <summary> Get the width of the output target. </summary>
		/// <returns> The width of the target. </returns>
		virtual uint32_t getWidth( ) = 0;

		/// <summary> Get the height of the output target. </summary>
		/// <returns> The height of the target. </returns>
		virtual uint32_t getHeight( ) = 0;

		/// <summary>
		/// Set the output target of the rendering context so that
		/// the results of the next draw calls will have the results
		/// stored here.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) = 0;

		/// <summary>
		/// This is where we clear the output so that we can start again.
		/// The provided context is used to make the needed calls to reset
		/// the targets.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		/// <param name="colour"> Clear the colour. </param>
		/// <param name="depth"> Clear the depth. </param>
		virtual void reset( const DeviceContextPtr_t& pContext, bool colour, bool depth ) = 0;

		/// <summary>
		/// This is where we will send the results to the output destination.
		/// This could be called at reset?
		/// </summary>
		/// <param name="pContext"> The context to use to present the target. </param>
		virtual void present( const DeviceContextPtr_t& pContext ) = 0;
	};

	inline OutputTarget::~OutputTarget( ) = default;
} // namespace directX11
} // namespace visual
#endif // _OUTPUT_TARGET_H
