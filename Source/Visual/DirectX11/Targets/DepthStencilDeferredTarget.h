#pragma once

// Depth Stencil Deferred Target
// 
// A readable depth stencil target that can be used for shadow mapping or depth pre-passes and such.
// 
// Project   : NaKama-Fx
// File Name : DepthStencilDeferredTarget.h
// Date      : 22-05-2015
// Author    : Nicholas Welters

#ifndef _DEPTH_STENCIL_DEFERRED_TARGET_DX11_H
#define _DEPTH_STENCIL_DEFERRED_TARGET_DX11_H

#include "DepthStencilTarget.h"

namespace visual
{
namespace directX11
{
	/// <summary> A depth stencil only target that we can access as a shader resource. </summary>
	class DepthStencilDeferredTarget: public DepthStencilTarget
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pTexture"> The Depth Stencils 2D Texture Data. </param>
		/// <param name="pDepthStencilView"> The Depth Stencils Render Target View. </param>
		/// <param name="pShaderResourceView"> The Depth Stencils Shader Resource View. </param>
		/// <param name="width"> The width of the target. </param>
		/// <param name="height"> The height of the target. </param>
		DepthStencilDeferredTarget(
			const ID3D11Texture2DSPtr& pTexture,
			const ID3D11DepthStencilViewSPtr& pDepthStencilView,
			const ID3D11ShaderResourceViewSPtr& pShaderResourceView,
			uint32_t width, uint32_t height
		);

		/// <summary> ctor </summary>
		DepthStencilDeferredTarget( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DepthStencilDeferredTarget( const DepthStencilDeferredTarget& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DepthStencilDeferredTarget( DepthStencilDeferredTarget&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~DepthStencilDeferredTarget( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		DepthStencilDeferredTarget& operator=( const DepthStencilDeferredTarget& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		DepthStencilDeferredTarget& operator=( DepthStencilDeferredTarget&& move ) = default;


		/// <summary> Get the shader resource view for the depth stencil buffer. </summary>
		/// <returns> A pointer to the shader resource view interface. </returns>
		ID3D11ShaderResourceViewSPtr getShaderResourceView( ) const;

	private:
		/// <summary> The Depth Stencils Shader Resource View. </summary>
		ID3D11ShaderResourceViewSPtr pShaderResourceView = nullptr;
	};
} // namespace directX11
} // namespace visual

#endif // _DEPTH_STENCIL_DEFERRED_TARGET_DX11_H

