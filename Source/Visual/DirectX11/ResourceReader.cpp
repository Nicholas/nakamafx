#include "ResourceReader.h"

// A simple helper class that allows us
// to read data from the applications resources.
//
// Project   : NaKama-Fx
// File Name : ResourceReader.h
// Date      : 02/04/2019
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	using std::cout;
	using std::pair;
	using std::exception;

	/// <summary> Get the data from the applications data archive. </summary>
	/// <param name="id"> The resource id. </param>
	/// <returns> The resource data and its size (in 32 bits). </returns>
	pair< uint32_t*, size_t > ResourceReader::dataFromResource( _In_ const int id )
	{
		const HMODULE hModule = nullptr;
		const HRSRC hrSrc = FindResource( hModule, MAKEINTRESOURCE( id ), RT_RCDATA );

		if( !hrSrc )
		{
			cout << "Error: Could not find the shader in the resource with ID: " << id << ".\n";
			throw exception( "Resource not found!" );
		}

		const HGLOBAL hGlobal = LoadResource( hModule, hrSrc );

		if( !hGlobal )
		{
			cout << "Error: Could not load shader from resource " << id << ".\n";
			throw exception( "Resource could not be loaded!" );
		}

		return 
		{
			static_cast< uint32_t* >( LockResource( hGlobal ) ),
			SizeofResource( nullptr, hrSrc )
		};
	}
} // namespace directX11
} // namespace visual
