#include "ColourMeshSubPass.h"
// Manage setting a constant buffer.
//
// Project   : NaKama-Fx
// File Name : ColourMeshSubPass.cpp
// Date      : 11/06/2017
// Author    : Nicholas Welters

#include "../Types/Model.h"
#include "../Types/MeshBuffer.h"
#include "../Materials/ColourMaterialBuffer.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPixelShader"> The sub pass pixel shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
	/// <param name="pPSLightingBuffer"> The sub pass pixel shader scene constants. </param>
	/// <param name="pPSSamplers"> The sub pass pixel shader texture samplers. </param>
	/// <param name="pPSResources"> The sub pass pixel shader resources. </param>
	/// <param name="offset"> The sub pass pixel shader resource start offset. </param>
	ColourMeshSubPass::ColourMeshSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
										  const ID3D11VertexShaderSPtr&      pVertexShader,
										  const ID3D11InputLayoutSPtr&       pInputLayput,
										  const ID3D11RasterizerStateSPtr&   pRasterizerState,
										  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
										  const ID3D11BlendStateSPtr&        pBlendState,
										  const SceneVSBufferSPtr&           pVSSceneBuffer,
										  const LightingPSBufferSPtr&        pPSLightingBuffer,
										  const PSSamplersSPtr&              pPSSamplers,
										  const PSResourcesSPtr&             pPSResources,
										  const uint32_t                     offset )
		: MeshSubPass( pVertexShader, pInputLayput, pRasterizerState, pDepthStencilState, pBlendState )
		, pVSSceneBuffer( pVSSceneBuffer )
		, pPixelShader( pPixelShader )
		, pPSSceneBuffer( pPSLightingBuffer )
		, pPSSamplers( pPSSamplers )
		, pClearSamplers( pPSSamplers->size( ) )
		, pPSResources( pPSResources )
		, offset( offset )
		, pClearResources( pPSResources->size( ) )
	{
		for( uint32_t i = 0; i < pPSResources->size( ); ++i )
		{
			pClearResources[ i ] = nullptr;
		}

		for( uint32_t i = 0; i < pPSSamplers->size( ); ++i )
		{
			pClearSamplers[ i ] = nullptr;
		}
	}

	/// <summary> Set the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to set the subpass constants. </param>
	void ColourMeshSubPass::set( const DeviceContextPtr_t& pContext )
	{
		pContext->PSSetShader( pPixelShader.get( ), nullptr, 0 );

		pPSSamplers->set( pContext, 0 );

		pPSSceneBuffer->set( pContext );
		pVSSceneBuffer->set( pContext );
	}

	/// <summary> Unset the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to unset the subpass constants. </param>
	void ColourMeshSubPass::unset( const DeviceContextPtr_t & pContext )
	{
		pContext->PSSetShader( nullptr, nullptr, 0 );
	}

	/// <summary> Render a model. </summary>
	/// <param name="pContext"> The device context used to render the model. </param>
	/// <param name="pModel"> The model to render. </param>
	void ColourMeshSubPass::render( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel )
	{
		pModel->getColourMaterial( )->set( pContext );

		pPSResources->set( pContext, offset );

		pModel->getMesh( )->set( pContext );
		pModel->getMesh( )->render( pContext );

		if( !pClearResources.empty( ) )
		{
			pContext->PSSetShaderResources( offset,
											static_cast< unsigned int >( pClearResources.size( ) ),
											pClearResources.data( ) );
		}
	}

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void ColourMeshSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		MeshSubPass::update( pContext, delta );
		pVSSceneBuffer->update( pContext );
		pPSSceneBuffer->update( pContext );
	}

	/// <summary> Change a subpass resource. </summary>
	/// <param name="index"> The index of the shader resource view to update. </param>
	/// <param name="pSRV"> The new shader resource view to use in the pass. </param>
	void ColourMeshSubPass::setSRV( const uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV ) const
	{
		pPSResources->set( index, pSRV );
	}

	/// <summary> Update a model. </summary>
	/// <param name="pContext"> The device context used to update the model. </param>
	/// <param name="pModel"> The model to update. </param>
	void ColourMeshSubPass::update( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel )
	{
		pModel->getColourMaterial( )->update( pContext );
	}
} // namespace directX11
} // namespace visual
