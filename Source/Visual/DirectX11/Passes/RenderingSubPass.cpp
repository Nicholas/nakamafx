#include "RenderingSubPass.h"
// Manage setting a constant buffer.
//
// Project   : NaKama-Fx
// File Name : RenderingSubPass.cpp
// Date      : 11/06/2017
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	RenderingSubPass::RenderingSubPass( const ID3D11VertexShaderSPtr& pVertexShader,
										const ID3D11InputLayoutSPtr& pInputLayput,
										const ID3D11RasterizerStateSPtr& pRasterizerState,
										const ID3D11DepthStencilStateSPtr& pDepthStencilState,
										const ID3D11BlendStateSPtr& pBlendState )
		: pVertexShader( pVertexShader )
		, pInputLayput( pInputLayput )
		, pRasterizerState( pRasterizerState )
		, pDepthStencilState( pDepthStencilState )
		, pBlendState( pBlendState )
		, blendFactor( 1.0f, 1.0f, 1.0f, 1.0f )
		, sampleMask( 0xffffffff )
		, enabled( true )
	{ }

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void RenderingSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		static_cast< void >( pContext );
		static_cast< void >( delta );
	}

	/// <summary> Render the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to render the subpass. </param>
	void RenderingSubPass::render( const DeviceContextPtr_t& pContext )
	{
		if( !enabled )
		{
			return;
		}

		pContext->IASetInputLayout( pInputLayput.get( ) );

		pContext->RSSetState( pRasterizerState.get( ) );

		pContext->OMSetBlendState( pBlendState.get( ), &blendFactor.x, sampleMask );
		pContext->OMSetDepthStencilState( pDepthStencilState.get( ), 0 );

		pContext->VSSetShader( pVertexShader.get( ), nullptr, 0 );

		renderPass( pContext );

		pContext->VSSetShader( nullptr, nullptr, 0 );
		pContext->OMSetBlendState( nullptr, nullptr, 0 );
		pContext->OMSetDepthStencilState( nullptr, 0 );
		pContext->RSSetState( nullptr );
		pContext->IASetInputLayout( nullptr );
	}

	/// <summary> Chefck what the enabled state of the sub pass is. </summary>
	/// <returns> The enabled state. </returns>
	bool RenderingSubPass::isEnabled( ) const
	{
		return enabled;
	}

	/// <summary> Set the enabled state of the sub pass. </summary>
	/// <param name="enable"> The enabled state. </param>
	void RenderingSubPass::setEnabled( const bool enable )
	{
		enabled = enable;
	}
} // namespace directX11
} // namespace visual
