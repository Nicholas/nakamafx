#pragma once
// Construct a histogram and response curve for our HDR rendering.
//
// This constructs mutiple histograms by subdeviding the frame buffer into smaller segments.
// The histograms are then accumulated to build a final histogram.
// The final histogram is used to build a response curve so that we can remap our
// HDR render target to LDR colour space.
//
// TODO: This was a first attempt and requires work as is currently displays heavy banding.
//
// Project   : NaKama-Fx
// File Name : ComputeHistogramPass.h
// Date      : 27/11/2018
// Author    : Nicholas Welters

#ifndef _COMPUTE_HISTOGRAM_PASS_DX11_H
#define _COMPUTE_HISTOGRAM_PASS_DX11_H

#include "ComputePass.h"
#include "../APITypeDefs.h"
#include "../Materials/MaterialBuffer.h"

#include <memory>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( UATarget );

	class ComputeHistogramPass : public ComputePass // DifferedComputePass
	{
	public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pTileHistogram">  Build a histogram per tile. </param>
		/// <param name="pAccumulateHistogram">  Merge the histograms to build a single histogram </param>
		/// <param name="pHistogramResponseCurve">  Create a response curve to be used during tone mapping. </param>
		/// <param name="pHistogramConstants"> The histogram compute shader constants. </param>
		/// <param name="pFrameBuffer"> The frame buffer compute shader resource. </param>
		/// <param name="pTileHistograms"> Histogram per tile buffer. </param>
		/// <param name="pHistogram"> The histogram buffer. </param>
		/// <param name="pResponseCurve"> The histogram response curve buffer. </param>
		ComputeHistogramPass( const DeviceContextPtr_t&      pContext,
							  const ID3D11ComputeShaderSPtr& pTileHistogram,
							  const ID3D11ComputeShaderSPtr& pAccumulateHistogram,
							  const ID3D11ComputeShaderSPtr& pHistogramResponseCurve,
							  const HistogramCSBufferSPtr&   pHistogramConstants,
							  const CSResourcesSPtr&         pFrameBuffer,
							  const UATargetSPtr&            pTileHistograms,
							  const UATargetSPtr&            pHistogram,
							  const UATargetSPtr&            pResponseCurve );

		/// <summary> ctor </summary>
		ComputeHistogramPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ComputeHistogramPass( const ComputeHistogramPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ComputeHistogramPass( ComputeHistogramPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~ComputeHistogramPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ComputeHistogramPass& operator=( const ComputeHistogramPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ComputeHistogramPass& operator=( ComputeHistogramPass&& move ) = delete;


		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The frame step size. </param>
		virtual void update( float delta ) override;

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		virtual void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary>  Build a histogram per tile. </summary>
		ID3D11ComputeShaderSPtr pTileHistogram;

		/// <summary>  Merge the histograms to build a single histogram </summary>
		ID3D11ComputeShaderSPtr pAccumulateHistogram;

		/// <summary>  Create a response curve to be used during tone mapping. </summary>
		ID3D11ComputeShaderSPtr pHistogramResponseCurve;

		/// <summary> The histogram compute shader constants. </summary>
		HistogramCSBufferSPtr pHistogramConstants;

		/// <summary> The frame buffer compute shader resource. </summary>
		CSResourcesSPtr pFrameBuffer;

		/// <summary> Histogram per tile buffer. </summary>
		UATargetSPtr pTileHistograms;

		/// <summary> The histogram buffer. </summary>
		UATargetSPtr pHistogram;

		/// <summary> The histogram response curve buffer. </summary>
		UATargetSPtr pResponseCurve;
	};
} // namespace directX11
} // namespace visual

#endif // _COMPUTE_HISTOGRAM_PASS_DX11_H
