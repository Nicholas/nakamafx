
#include "ComputePass.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
/// 
/// This is the renderes logical grouping of objects that need to be computed.
/// 
/// Nicholas Welters, 27/11/2018.
/// 
////////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Output\OutputTarget.h"
#include "Visual/DirectX11/Targets/OutputTarget.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	using ::std::min;
	using ::std::max;
	using ::std::abs;

	/// <summary> ctor. </summary>
	ComputePass::ComputePass( const DeviceContextPtr_t& pContext )
		: AsyncController( "Compute Pass" )
		, pContext( pContext )
	{
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	void ComputePass::update( const float delta )
	{
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to render
	///	all the object in this scene.
	/// </summary>
	void ComputePass::render( )
	{
		execute( );

		setState( );

		compute( );

		restorState( );
	}

	/// <summary> The derived classes interface to finalize its updates to create a consistent internal state. </summary>
	void ComputePass::update( )
	{
	}

	/// <summary> Allow subclasses to add to the end of the rendering pass. </summary>
	void ComputePass::finalizeState( )
	{
	}

	/// <summary>	This will set the GPU state like the render target and depth stencil buffer. </summary>
	void ComputePass::setState( )
	{
	}

	/// <summary> Put the  renderer back in a state that we know. </summary>
	void ComputePass::restorState( )
	{
		finalizeState( );
	}



	DifferedComputePass::DifferedComputePass(
		const DeviceContextPtr_t& pContext,
		const ::std::function< void( const PriorityCommandList& ) > queueCommandList,
		const int& priority
	)
		: ComputePass( pContext )
		, pContext( pContext )
		, queueCommandList( queueCommandList  )
		, priority( priority )
	{

	}

	/// <summary> 
	/// Allow subclasses to add to the end of the compute pass.
	/// This will then submit the compute commands to the command queue.
	/// </summary>
	void DifferedComputePass::finalizeState( )
	{
		ID3D11CommandList* pCommandList = nullptr;
		pContext->FinishCommandList( FALSE, &pCommandList );
		queueCommandList( { priority,{ pCommandList } } );
	}

} // namespace directX11
} // namespace visual
