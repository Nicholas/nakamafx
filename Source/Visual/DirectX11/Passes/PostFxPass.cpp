#include "PostFxPass.h"
// Post processing pass.
//
// This gets the average scene luminance for the final exposure value and then
// shifts the exposure towards that average value.
//
// This then builds the bloom blur buffers.
// It first down samples the scene to using a hi pass to collect the bright colours and
// then performs a blur,
// The image is down sampled and blured 3 additional times (4 samples 1/2, 1/4, 1/8 and 1/16)
// to build the final bloom.
//
// Project   : NaKama-Fx
// File Name : PostFxPass.cpp
// Date      : 04/12/2018
// Author    : Nicholas Welters

//#include "Output\UATarget.h"
#include "Visual/DirectX11/Targets/UATarget.h"
//#include "Output\MultipleRenderTarget.h"
#include "Visual/DirectX11/Targets/MultipleRenderTarget.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pAverageColour"> Average Image Down Sample Target. </param>
	/// <param name="pFrameBuffer"> The frame buffer that we are going to build our post processing data from. </param>
	/// <param name="pRasterizerState"> Rasterizer state. </param>
	/// <param name="pBlendState"> Blend State. </param>
	/// <param name="pDepthStencilState"> The depth stencil state. </param>
	/// <param name="pLinearSampler"> A linear sampler, we  are bluring just about everything so its all good. </param>
	/// <param name="pFullScreenVS"> A bufferless fullscreen verex shader. </param>
	/// <param name="pResamplePS"> A resampoling shader to copy the data to a new render target. </param>
	/// <param name="pAdaptionCS"> Luminance value adaptation compute shader. </param>
	/// <param name="pAdaptionConsts"> Luminance value adaptation compute shader constants. </param>
	/// <param name="pAdaptionA"> Luminance value A buffer. </param>
	/// <param name="pAdaptionB"> Luminance value B buffer. </param>
	/// <param name="pBlur1A"> Blur 1/2 A buffer. </param>
	/// <param name="pBlur1B"> Blur 1/2 B buffer. </param>
	/// <param name="pBlur2A"> Blur 1/4 A buffer. </param>
	/// <param name="pBlur2B"> Blur 1/4 B buffer. </param>
	/// <param name="pBlur3A"> Blur 1/8 A buffer. </param>
	/// <param name="pBlur3B"> Blur 1/8 B buffer. </param>
	/// <param name="pBlur4A"> Blur 1/16 A buffer. </param>
	/// <param name="pBlur4B"> Blur 1/16 B buffer. </param>
	/// <param name="pHiPassPS"> The first bloom down sampling shader. </param>
	/// <param name="pBlurVVS"> The vertical pass vertex shader of the two pass blur. </param>
	/// <param name="pBlurHVS"> The horizontal pass vertex shader of the two pass blur. </param>
	/// <param name="pBlurPS"> The pixel shader of the two pass blur. </param>
	/// <param name="pBlur1VSConsts"> Blur 1/2 vertex shader constants buffer. </param>
	/// <param name="pBlur2VSConsts"> Blur 1/4 vertex shader constants buffer. </param>
	/// <param name="pBlur3VSConsts"> Blur 1/8 vertex shader constants buffer. </param>
	/// <param name="pBlur4VSConsts"> Blur 1/16 vertex shader constants buffer. </param>
	/// <param name="pBlurPSConsts"> Blur pixel shader constants buffer. </param>
	PostFxPass::PostFxPass( const DeviceContextPtr_t&          pContext,
							const MultipleRenderTargetSPtr&    pAverageColour,
							const PSResourcesSPtr&             pFrameBuffer,
							const ID3D11RasterizerStateSPtr&   pRasterizerState,
							const ID3D11BlendStateSPtr&        pBlendState,
							const ID3D11DepthStencilStateSPtr& pDepthStencilState,
							const ID3D11SamplerStateSPtr&      pLinearSampler,
							const ID3D11VertexShaderSPtr&      pFullScreenVS,
							const ID3D11PixelShaderSPtr&       pResamplePS,

							const ID3D11ComputeShaderSPtr& pAdaptionCS,
							const AdaptationCSBufferSPtr&  pAdaptionConsts,
							const UATargetSPtr&            pAdaptionA,
							const UATargetSPtr&            pAdaptionB,

							const MultipleRenderTargetSPtr& pBlur1A,
							const MultipleRenderTargetSPtr& pBlur1B,
							const MultipleRenderTargetSPtr& pBlur2A,
							const MultipleRenderTargetSPtr& pBlur2B,
							const MultipleRenderTargetSPtr& pBlur3A,
							const MultipleRenderTargetSPtr& pBlur3B,
							const MultipleRenderTargetSPtr& pBlur4A,
							const MultipleRenderTargetSPtr& pBlur4B,

							const ID3D11PixelShaderSPtr& pHiPassPS,

							const ID3D11VertexShaderSPtr& pBlurVVS,
							const ID3D11VertexShaderSPtr& pBlurHVS,
							const ID3D11PixelShaderSPtr&  pBlurPS,

							const BlurVSBufferSPtr& pBlur1VSConsts,
							const BlurVSBufferSPtr& pBlur2VSConsts,
							const BlurVSBufferSPtr& pBlur3VSConsts,
							const BlurVSBufferSPtr& pBlur4VSConsts,
							const BlurPSBufferSPtr& pBlurPSConsts )
		: ComputePass( pContext )
		, pContext( pContext )
		, pAverageColour( pAverageColour )
		, pFrameBuffer( pFrameBuffer )
		, pFullScreenVS( pFullScreenVS )
		, pResamplePS( pResamplePS )
		, pRasterizerState( pRasterizerState )
		, pBlendState( pBlendState )
		, pDepthStencilState( pDepthStencilState )
		, pLinearSampler( pLinearSampler )

		, pAdaptionCS( pAdaptionCS )
		, pAdaptionConsts( pAdaptionConsts )
		, pAdaption{ pAdaptionA, pAdaptionB }
		, buffer( 0 )

		, pBlur1{ pBlur1A, pBlur1B }
		, pBlur2{ pBlur2A, pBlur2B }
		, pBlur3{ pBlur3A, pBlur3B }
		, pBlur4{ pBlur4A, pBlur4B }

		, pHiPassPS( pHiPassPS )
		, pBlurVVS( pBlurVVS )
		, pBlurHVS( pBlurHVS )
		, pBlurPS( pBlurPS )
		, pBlur1VSConsts( pBlur1VSConsts )
		, pBlur2VSConsts( pBlur2VSConsts )
		, pBlur3VSConsts( pBlur3VSConsts )
		, pBlur4VSConsts( pBlur4VSConsts )
		, pBlurPSConsts( pBlurPSConsts )
	{

		pBlur1VSConsts->get( ).invScreenSize.x = 1.0f / pBlur1A->getWidth( );
		pBlur1VSConsts->get( ).invScreenSize.y = 1.0f / pBlur1A->getHeight( );
		pBlur1VSConsts->update( pContext );

		pBlur2VSConsts->get( ).invScreenSize.x = 1.0f / pBlur2A->getWidth( );
		pBlur2VSConsts->get( ).invScreenSize.y = 1.0f / pBlur2A->getHeight( );
		pBlur2VSConsts->update( pContext );

		pBlur3VSConsts->get( ).invScreenSize.x = 1.0f / pBlur3A->getWidth( );
		pBlur3VSConsts->get( ).invScreenSize.y = 1.0f / pBlur3A->getHeight( );
		pBlur3VSConsts->update( pContext );

		pBlur4VSConsts->get( ).invScreenSize.x = 1.0f / pBlur4A->getWidth( );
		pBlur4VSConsts->get( ).invScreenSize.y = 1.0f / pBlur4A->getHeight( );
		pBlur4VSConsts->update( pContext );

		pBlurPSConsts->get( ).texelSize.x = 2.0f * pBlur1A->getWidth( );
		pBlurPSConsts->get( ).texelSize.y = 2.0f * pBlur1A->getHeight( );
		pBlurPSConsts->get( ).threshold = 3.5f;
		pBlurPSConsts->get( ).magantude = 0.0f;
		pBlurPSConsts->get( ).sigma     = 0.9f;
		pBlurPSConsts->update( pContext );

		pAdaptionConsts->get( ).timeDelta = 0;
		pAdaptionConsts->get( ).TauU = 3.75f;
		pAdaptionConsts->get( ).TauD = 3.75f;

	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	void PostFxPass::update( float delta )
	{
		pAdaptionConsts->get( ).timeDelta = delta;
		pAdaptionConsts->update( pContext );
		pBlurPSConsts->update( pContext );
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to perform a compute pass.
	/// </summary>
	void PostFxPass::compute( )
	{
		// Copy the frame buffer to our average colour mip map level 1.
		D3D11_RECT scissor;
		D3D11_VIEWPORT viewPort;

		scissor.left = 0;
		scissor.top = 0;
		scissor.right = pAverageColour->getWidth( );
		scissor.bottom = pAverageColour->getHeight( );

		viewPort.Width = static_cast< float >( scissor.right );
		viewPort.Height = static_cast< float >( scissor.bottom );
		viewPort.TopLeftY = 0;
		viewPort.TopLeftX = 0;
		viewPort.MinDepth = 0;
		viewPort.MaxDepth = 1;

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );

		pContext->RSSetState( pRasterizerState.get( ) );

		static const float blend[ ] = { 1.0f, 1.0f, 1.0f, 1.0f };
		pContext->OMSetBlendState( pBlendState.get( ), blend, 0xffffffff );
		pContext->OMSetDepthStencilState( pDepthStencilState.get( ), 0 );

		pAverageColour->reset( pContext, true, false );
		pAverageColour->set( pContext );
		pFrameBuffer->set( pContext, 0 );

		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pResamplePS.get( ), nullptr, 0 );

		ID3D11SamplerState* sampler[ 1 ];
		sampler[ 0 ] = pLinearSampler.get( );
		pContext->PSSetSamplers( 0, 1, sampler );

		pContext->IASetVertexBuffers( 0, 0, nullptr, nullptr, nullptr );
		pContext->IASetIndexBuffer( nullptr, DXGI_FORMAT_UNKNOWN, 0 );
		pContext->IASetInputLayout( nullptr );
		pContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
		pContext->Draw( 3, 0 );

		// Build the mip maps for the scene to get the average colour.
		pContext->GenerateMips( pAverageColour->getShaderResourceViews( )[ 0 ].get( ) );



		// Adapt the luminance
		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, 0, 0, nullptr, nullptr );

		pAdaptionConsts->set( pContext );

		const int buffer2 = ( buffer + 1 ) % 2;

		ID3D11UnorderedAccessView* uav[ ] = { nullptr };
		uav[ 0 ] = pAdaption[ buffer2 ]->getUnorderedAccessViews( )[ 0 ].get( );
		pContext->CSSetUnorderedAccessViews( 0, 1, uav, nullptr );

		ID3D11ShaderResourceView* lumBuffer[ ] = {
			pAverageColour->getShaderResourceViews( )[ 0 ].get( ),
			pAdaption[ buffer ]->getShaderResourceViews( )[ 0 ].get( )
		};
		pContext->CSSetShaderResources( 0, 2, lumBuffer );

		pContext->CSSetShader( pAdaptionCS.get( ), nullptr, 0 );

		pContext->Dispatch( 1, 1, 1 );

		buffer = buffer2;

		lumBuffer[ 0 ] = nullptr;
		lumBuffer[ 1 ] = nullptr;
		pContext->CSSetShaderResources( 0, 2, lumBuffer );

		uav[ 0 ] = nullptr;
		pContext->CSSetUnorderedAccessViews( 0, 1, uav, nullptr );
		pContext->CSSetShader( nullptr, nullptr, 0 );




		// +-------+
		// | Bloom |
		// +-------+
		scissor.right = pBlur1[ 0 ]->getWidth( );
		scissor.bottom = pBlur1[ 0 ]->getHeight( );

		viewPort.Width = static_cast< float >( scissor.right );
		viewPort.Height = static_cast< float >( scissor.bottom );

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );

		ID3D11ShaderResourceView* blur[ 1 ];
		// Down Size 1 And Hi Pass
		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pHiPassPS.get( ), nullptr, 0 );

		pBlur1[ 0 ]->reset( pContext, true, false );
		pBlur1[ 0 ]->set( pContext );

		pFrameBuffer->set( pContext, 0 );
		pBlurPSConsts->set( pContext );
		blur[ 0 ] = pAdaption[ buffer ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 1, 1, blur );

		pContext->Draw( 3, 0 );


		// Blur pass 1 V
		pBlur1VSConsts->set( pContext );

		pBlur1[ 1 ]->reset( pContext, true, false );
		pBlur1[ 1 ]->set( pContext );

		blur[ 0 ] = pBlur1[ 0 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurHVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pBlurPS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );

		// Blur pass 1 H
		blur[ 0 ] = nullptr;
		pContext->PSSetShaderResources( 0, 1, blur );

		pBlur1[ 0 ]->reset( pContext, true, false );
		pBlur1[ 0 ]->set( pContext );

		blur[ 0 ] = pBlur1[ 1 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurVVS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );


		// Down Size 2
		scissor.right = pBlur2[ 0 ]->getWidth( );
		scissor.bottom = pBlur2[ 0 ]->getHeight( );

		viewPort.Width = static_cast< float >( scissor.right );
		viewPort.Height = static_cast< float >( scissor.bottom );

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );


		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pResamplePS.get( ), nullptr, 0 );

		pBlur2[ 0 ]->reset( pContext, true, false );
		pBlur2[ 0 ]->set( pContext );

		blur[ 0 ] = pBlur1[ 0 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->Draw( 3, 0 );

		// Blur pass 2 V
		pBlur2VSConsts->set( pContext );

		pBlur2[ 1 ]->reset( pContext, true, false );
		pBlur2[ 1 ]->set( pContext );

		blur[ 0 ] = pBlur2[ 0 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurHVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pBlurPS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );

		// Blur pass 2 H
		blur[ 0 ] = nullptr;
		pContext->PSSetShaderResources( 0, 1, blur );

		pBlur2[ 0 ]->reset( pContext, true, false );
		pBlur2[ 0 ]->set( pContext );

		blur[ 0 ] = pBlur2[ 1 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurVVS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );




		// Down Size 3
		scissor.right = pBlur3[ 0 ]->getWidth( );
		scissor.bottom = pBlur3[ 0 ]->getHeight( );

		viewPort.Width = static_cast< float >( scissor.right );
		viewPort.Height = static_cast< float >( scissor.bottom );

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );


		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pResamplePS.get( ), nullptr, 0 );

		pBlur3[ 0 ]->reset( pContext, true, false );
		pBlur3[ 0 ]->set( pContext );

		blur[ 0 ] = pBlur2[ 0 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->Draw( 3, 0 );

		// Blur pass 3 V
		pBlur3VSConsts->set( pContext );

		pBlur3[ 1 ]->reset( pContext, true, false );
		pBlur3[ 1 ]->set( pContext );

		blur[ 0 ] = pBlur3[ 0 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurHVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pBlurPS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );

		// Blur pass 3 H
		blur[ 0 ] = nullptr;
		pContext->PSSetShaderResources( 0, 1, blur );

		pBlur3[ 0 ]->reset( pContext, true, false );
		pBlur3[ 0 ]->set( pContext );

		blur[ 0 ] = pBlur3[ 1 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurVVS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );




		// Down Size 4
		scissor.right = pBlur4[ 0 ]->getWidth( );
		scissor.bottom = pBlur4[ 0 ]->getHeight( );

		viewPort.Width = static_cast< float >( scissor.right );
		viewPort.Height = static_cast< float >( scissor.bottom );

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );



		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pResamplePS.get( ), nullptr, 0 );

		pBlur4[ 0 ]->reset( pContext, true, false );
		pBlur4[ 0 ]->set( pContext );

		blur[ 0 ] = pBlur3[ 0 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->Draw( 3, 0 );

		// Blur pass 4 V
		pBlur4VSConsts->set( pContext );

		pBlur4[ 1 ]->reset( pContext, true, false );
		pBlur4[ 1 ]->set( pContext );

		blur[ 0 ] = pBlur4[ 0 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurHVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pBlurPS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );

		// Blur pass 4 H
		blur[ 0 ] = nullptr;
		pContext->PSSetShaderResources( 0, 1, blur );

		pBlur4[ 0 ]->reset( pContext, true, false );
		pBlur4[ 0 ]->set( pContext );

		blur[ 0 ] = pBlur4[ 1 ]->getShaderResourceViews( )[ 0 ].get( );
		pContext->PSSetShaderResources( 0, 1, blur );

		pContext->VSSetShader( pBlurVVS.get( ), nullptr, 0 );

		pContext->Draw( 3, 0 );
	}
} // namespace directX11
} // namespace visual
