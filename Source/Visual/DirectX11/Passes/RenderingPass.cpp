#include "RenderingPass.h"
// This is the renderes logical grouping of objects that need to be rendered together
// at the same time to the same render target. This lets us split the rendering to render targets
// so that we can perform a copy inbetween if it is needed.
//
// Project   : NaKama-Fx
// File Name : RenderingPass.cpp
// Date      : 15/02/2017
// Author    : Nicholas Welters

#include "Visual/DirectX11/Targets/OutputTarget.h"
#include "RenderingSubPass.h"

#include <D3D11.h>

namespace visual
{
namespace directX11
{
	using ::std::min;
	using ::std::max;
	using ::std::abs;

	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pTarget"> The render target that we are going to draw to. </param>
	RenderingPass::RenderingPass( const DeviceContextPtr_t& pContext,
								  const OutputTargetSPtr& pTarget )
		: pContext( pContext )
		, pTarget( pTarget )
		, pSubPasses( )
		, clearColour( true )
		, clearDepth( true )
		, clearStencil( true )
		, active( true )
		, visible( true )
	{ }

	/// <summary> Set the clear colour used on the passes render target. </summary>
	/// <param name="r"> The red channel. </param>
	/// <param name="g"> The green channel. </param>
	/// <param name="b"> The blue channel. </param>
	/// <param name="a"> The alpha channel. </param>
	void RenderingPass::setClearColour( float r, float g, float b, float a )
	{
			pTarget->setClearColour( r, g, b, a );
			// BUG render targets are shared among scenes.
			// Making it confusing why you might be getting
			// a clear colour you were not expecting.
	}

	/// <summary> Add a sub pass that will draw in this pass. </summary>
	/// <param name="pSubPass"> A subpass to add to the rendering pass. </param>
	void RenderingPass::add( const RenderingSubPassSPtr& pSubPass )
	{
			pSubPasses.push_back( pSubPass );
	}

	/// <summary> Remove a sub pass that will draw in this pass. </summary>
	/// <param name="pSubPass"> A subpass to remove from the rendering pass. </param>
	void RenderingPass::remove( const RenderingSubPassSPtr& pSubPass )
	{
			const auto elementIter = std::find( pSubPasses.begin( ), pSubPasses.end( ), pSubPass );

			if( elementIter != pSubPasses.end( ) )
			{
				pSubPasses.erase( elementIter );
			}
	}

	/// <summary> Change the render target of the rendering pass. </summary>
	/// <param name="pTarget"> A new output target for the rendering pass. </param>
	void RenderingPass::setOutputTarget( const OutputTargetSPtr& pTarget )
	{
		this->pTarget = pTarget;
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	/// <param name="delta"> The time step taken to update the frame. </param>
	void RenderingPass::update( const float delta )
	{
		for( const RenderingSubPassSPtr& pSubPass : pSubPasses )
		{
			pSubPass->update( pContext, delta );
		}
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to render
	///	all the object in this scene.
	/// </summary>
	void RenderingPass::render( )
	{
		if( !isActive( ) )
		{
			return;
		}

		setState( );

		if( isVisible( ) )
		{
			for( const RenderingSubPassSPtr& pSubPass : pSubPasses )
			{
				pSubPass->render( pContext );
			}
		}

		restorState( );
	}

	/// <summary> Allow subclasses to add to the end of the rendering pass. </summary>
	void RenderingPass::finalizeState( )
	{ }

	/// <summary>	This will set the GPU state like the render target and depth stencil buffer. </summary>
	void RenderingPass::setState( ) const
	{
		pTarget->set( pContext );

		/**
		static float time = 0;
		time += 0.08f * delta;

		float t0 = 1 - abs( ( fmod( time + 0, 3.0f ) ) - 1.5f );
		float t1 = 1 - abs( ( fmod( time + 1, 3.0f ) ) - 1.5f );
		float t2 = 1 - abs( ( fmod( time + 2, 3.0f ) ) - 1.5f );

		float r = min( max( t0, 0.0f ), 1.0f );
		float g = min( max( t1, 0.0f ), 1.0f );
		float b = min( max( t2, 0.0f ), 1.0f );
		pRenderTraget->setClearColour( r, g, b, 1 );
		//*/

		pTarget->reset( pContext, isClearingColour( ), isClearingDepth( ) );

		D3D11_RECT scissor;
		D3D11_VIEWPORT viewPort;

		scissor.left   = 0;
		scissor.top    = 0;
		scissor.right  = pTarget->getWidth( );
		scissor.bottom = pTarget->getHeight( );

		viewPort.Width  = static_cast< float >(scissor.right);
		viewPort.Height = static_cast< float >(scissor.bottom);
		viewPort.TopLeftY = 0;
		viewPort.TopLeftX = 0;
		viewPort.MinDepth = 0;
		viewPort.MaxDepth = 1;

		pContext->RSSetScissorRects(1, &scissor);
		pContext->RSSetViewports(1, &viewPort);
	}

	/// <summary> Put the  renderer back in a state that we know. </summary>
	void RenderingPass::restorState( )
	{
		static ID3D11ShaderResourceView* blank[ 16 ] = {
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr
		};
		pContext->PSSetShaderResources( 0, 16, blank );

		pTarget->present( pContext );
		finalizeState( );
	}

	/// <summary> Toggle the rendering passes active state. </summary>
	/// <param name="active"> The active state to set. </param>
	void RenderingPass::setActive( bool active )
	{
			this->active = active;
	}

	/// <summary> Check the passes active state. </summary>
	bool RenderingPass::isActive( ) const
	{
		return active;
	}

	/// <summary> Toggle the rendering passes visibility state. </summary>
	/// <param name="active"> The visibility state to set. </param>
	void RenderingPass::setVisibility( bool visible )
	{
		this->visible = visible;
	}

	/// <summary> Check the passes visibility state. </summary>
	bool RenderingPass::isVisible( ) const
	{
		return visible;
	}

	/// <summary> Set what parts of the render targets get cleared. </summary>
	/// <param name="colour"> Clear the render targets colour buffers. </param>
	/// <param name="depth"> Clear the render targets depth buffer. </param>
	/// <param name="stencil"> Clear the render targets stencil buffer. </param>
	void RenderingPass::setRtClearFlags( bool colour, bool depth, bool stencil )
	{
		clearColour  = colour;
		clearDepth   = depth;
		clearStencil = stencil;
	}

	/// <summary> Check if the pass wants to clear the colour targets. </summary>
	bool RenderingPass::isClearingColour( ) const
	{
		return clearColour;
	}

	/// <summary> Check if the pass wants to clear the depth target. </summary>
	bool RenderingPass::isClearingDepth( ) const
	{
		return clearDepth;
	}

	/// <summary> Check if the pass wants to clear the stencil target. </summary>
	bool RenderingPass::isClearingStencil( ) const
	{
		return clearStencil;
	}
} // namespace directX11
} // namespace visual
