#include "TextSubPass.h"

// Manage setting a constant buffer.
//
// Project   : NaKama-Fx
// File Name : TextSubPass.cpp
// Date      : 26/05/2019
// Author    : Nicholas Welters

#include "../Types/Text.h"
#include "../Materials/FontMaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPixelShader"> The sub pass pixel shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
	/// <param name="pPSLightingBuffer"> The sub pass pixel shader scene constants. </param>
	/// <param name="pPSSamplers"> The sub pass pixel shader texture samplers. </param>
	/// <param name="pPSResources"> The sub pass pixel shader resources. </param>
	/// <param name="offset"> The sub pass pixel shader resource start offset. </param>
	TextSubPass::TextSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
							  const ID3D11VertexShaderSPtr&      pVertexShader,
							  const ID3D11InputLayoutSPtr&       pInputLayput,
							  const ID3D11RasterizerStateSPtr&   pRasterizerState,
							  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
							  const ID3D11BlendStateSPtr&        pBlendState,
							  const SceneVSBufferSPtr&           pVSSceneBuffer,
							  const LightingPSBufferSPtr&        pPSLightingBuffer,
							  const PSSamplersSPtr&              pPSSamplers,
							  const PSResourcesSPtr&             pPSResources,
							  const uint32_t                     offset )
		: RenderingSubPass( pVertexShader, pInputLayput, pRasterizerState, pDepthStencilState, pBlendState )
		, texts( )
		, pVSSceneBuffer( pVSSceneBuffer )
		, pPixelShader( pPixelShader )
		, pPSSceneBuffer( pPSLightingBuffer )
		, pPSSamplers( pPSSamplers )
		, pClearSamplers( pPSSamplers->size( ) )
		, pPSResources( pPSResources )
		, offset( offset )
		, pClearResources( pPSResources->size( ) )
	{
		for( uint32_t i = 0; i < pPSResources->size( ); ++i )
		{
			pClearResources[ i ] = nullptr;
		}

		for( uint32_t i = 0; i < pPSSamplers->size( ); ++i )
		{
			pClearSamplers[ i ] = nullptr;
		}
	}

	/// <summary> Set the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to set the subpass constants. </param>
	void TextSubPass::set( const DeviceContextPtr_t& pContext )
	{
		pContext->PSSetShader( pPixelShader.get( ), nullptr, 0 );

		pPSSamplers->set( pContext, 0 );

		pPSSceneBuffer->set( pContext );
		pVSSceneBuffer->set( pContext );
	}

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void TextSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		for( const TextSPtr& pText : texts )
		{
			update( pContext, pText );
		}
	}

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void TextSubPass::update( const DeviceContextPtr_t& pContext, const TextSPtr& pText )
	{
		pVSSceneBuffer->update( pContext );
		pPSSceneBuffer->update( pContext );

		if( pText->updateBuffer( ) )
		{
			pText->setText( pContext );
		}

		pText->update( );
		pText->getFontMaterial( )->update( pContext );
	}

	/// <summary> Render the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to render the subpass. </param>
	void TextSubPass::renderPass( const DeviceContextPtr_t& pContext )
	{
		set( pContext );

		for( const TextSPtr& pText : texts )
		{
			render( pContext, pText );
		}

		unset( pContext );
	}

	/// <summary> Render a model. </summary>
	/// <param name="pContext"> The device context used to render the model. </param>
	/// <param name="pModel"> The model to render. </param>
	void TextSubPass::render( const DeviceContextPtr_t& pContext, const TextSPtr& pText )
	{
		pText->getFontMaterial( )->set( pContext );

		pPSResources->set( pContext, offset );

		pText->set( pContext );
		pText->render( pContext );

		if( !pClearResources.empty( ) )
		{
			pContext->PSSetShaderResources( offset,
											static_cast< unsigned int >( pClearResources.size( ) ),
											pClearResources.data( ) );
		}
	}

	/// <summary> Unset the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to unset the subpass constants. </param>
	void TextSubPass::unset( const DeviceContextPtr_t & pContext )
	{ }

	/// <summary> Add a model to the subpass. </summary>
	/// <param name="pText"> The model to render in the subpass. </param>
	void TextSubPass::add( const TextSPtr & pText )
	{
		texts.push_back( pText );
	}

	/// <summary> Remove a model to the subpass. </summary>
	/// <param name="pText"> The model to remove in the subpass. </param>
	void TextSubPass::remove( const TextSPtr & pText )
	{
		texts.remove( pText );
	}

	/// <summary> Change a resource. </summary>
	/// <param name="index"> The index of the shader resource view to update. </param>
	/// <param name="pSRV"> The new shader resource view to use in the pass. </param>
	void TextSubPass::setSRV( const uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV ) const
	{
		pPSResources->set( index, pSRV );
	}
} // namespace directX11
} // namespace visual
