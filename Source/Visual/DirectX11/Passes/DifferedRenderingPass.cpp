#include "DifferedRenderingPass.h"
// This is the renderes logical grouping of objects that need to be rendered together
// at the same time to the same render target. This lets us split the rendering to render targets
// so that we can perform a copy inbetween if it is needed.
//
// But here we are building a command list that will be executed later.
//
// Project   : NaKama-Fx
// File Name : DifferedRenderingPass.h
// Date      : 15/02/2017
// Author    : Nicholas Welters

#include <D3D11.h>

namespace visual
{
namespace directX11
{
	using std::function;

	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pTarget"> The render target that we are going to draw to. </param>
	/// <param name="queueCommandList"> A function that accepts a command list with a priority. </param>
	/// <param name="priority"> The priority of the rendering pass. </param>
	DifferedRenderingPass::DifferedRenderingPass(
		const DeviceContextPtr_t& pContext,
		const OutputTargetSPtr& pTarget,
		const function< void( const PriorityCommandList& ) >& queueCommandList,
		const int priority
	)
		: RenderingPass( pContext, pTarget )
		, pContext( pContext )
		, queueCommandList( queueCommandList  )
		, priority( priority )
	{ }

	/// <summary>
	/// Allow subclasses to add to the end of the rendering pass.
	/// This will then submit the rendering commands to the command queue.
	/// </summary>
	void DifferedRenderingPass::finalizeState( )
	{
		ID3D11CommandList* pCommandList = nullptr;
		pContext->FinishCommandList( FALSE, &pCommandList );
		queueCommandList( { priority, pCommandList } );
	}
} // namespace directX11
} // namespace visual
