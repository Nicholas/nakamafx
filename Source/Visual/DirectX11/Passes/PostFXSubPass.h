#pragma once
// Render a full screen pass to be used for post processing.
//
// Project   : NaKama-Fx
// File Name : PostFXSubPass.h
// Date      : 25/01/2019
// Author    : Nicholas Welters

#ifndef _POST_FX_SUB_PASS_DX11_H
#define _POST_FX_SUB_PASS_DX11_H

#include "FullScreenSubPass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	class PostFXSubPass : public FullScreenSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPixelShader"> The sub pass pixel shader. </param>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pMesh"> The geometry used to render the pass. </param>
		/// <param name="pMaterial"> The textures and constants needed in the pass. </param>
		/// <param name="pPSPostFXBuffer"> The pixel shader constants. </param>
		/// <param name="samplers"> The texture sampler states. </param>
		PostFXSubPass( const ID3D11PixelShaderSPtr &pPixelShader,
					   const ID3D11VertexShaderSPtr& pVertexShader,
					   const ID3D11InputLayoutSPtr& pInputLayput,
					   const ID3D11RasterizerStateSPtr& pRasterizerState,
					   const ID3D11DepthStencilStateSPtr& pDepthStencilState,
					   const ID3D11BlendStateSPtr& pBlendState,
					   const MeshBufferSPtr& pMesh,
					   const PSResourcesSPtr& pMaterial,
					   const PostFXPSBufferSPtr& pPSPostFXBuffer,
					   const PSSamplersSPtr& pSamplers );

		/// <summary> ctor </summary>
		PostFXSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		PostFXSubPass( const PostFXSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		PostFXSubPass( PostFXSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~PostFXSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		PostFXSubPass& operator=( const PostFXSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		PostFXSubPass& operator=( PostFXSubPass&& move ) = default;


		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

	protected:
		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		virtual void renderPass( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The per scene pixel shader samplers. </summary>
		PostFXPSBufferSPtr pPSPostFXBuffer;
	};
} // namespace directX11
} // namespace visual

#endif // _POST_FX_SUB_PASS_DX11_H

