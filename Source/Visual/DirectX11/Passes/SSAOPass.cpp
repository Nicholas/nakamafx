#include "SSAOPass.h"
// Screen Space Ambiant Occlusion Pass.
//
// Project   : NaKama-Fx
// File Name : SSAOPass.h
// Date      : 04/12/2018
// Author    : Nicholas Welters

#include "../Targets/MultipleRenderTarget.h"
#include "../Materials/SSAOMaterialBuffer.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	using std::min;
	using std::max;
	using std::abs;

	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pRasterizerState"> Primative to Fragment Settings. </param>
	/// <param name="pBlendStateOff"> Output Merget Blend Settings during AO sampling. </param>
	/// <param name="pBlendState"> Output Merget Blend Settings during AO blur and composite. </param>
	/// <param name="pDepthStencilState"> Output Merget Depth Stencil Settings. </param>
	/// <param name="pSSAOTarget"> Ambiant occlusion frame buffer. </param>
	/// <param name="pOutputTarget"> The scene render target that we will add the AO too. </param>
	/// <param name="pFullScreenVS"> Bufferless fullscreen vertex shader. </param>
	/// <param name="pSSRShader"> Pixel shader that builds the AO buffer. </param>
	/// <param name="pSSRShaderData"> Pixel shader data to build the AO buffer. </param>
	/// <param name="pSSRUpShader"> Pixel shader that composes the AO on top of the scene. </param>
	/// <param name="pSSRUpShaderData"> Pixel shader data used to composes the AO on top of the scene. </param>
	/// <param name="pSamplers"> Pixel shader samplers. </param>
	SSAOPass::SSAOPass( const DeviceContextPtr_t&          pContext,
						const ID3D11RasterizerStateSPtr&   pRasterizerState,
						const ID3D11BlendStateSPtr&        pBlendStateOff,
						const ID3D11BlendStateSPtr&        pBlendState,
						const ID3D11DepthStencilStateSPtr& pDepthStencilState,
						const MultipleRenderTargetSPtr&    pSSAOTarget,
						const MultipleRenderTargetSPtr&    pOutputTarget,
						const ID3D11VertexShaderSPtr&      pFullScreenVS,
						const ID3D11PixelShaderSPtr&       pSSAOShader,
						const SSAOShaderBufferSPtr&        pSSAOShaderData,
						const ID3D11PixelShaderSPtr&       pSSAOUpShader,
						const SSAOShaderBufferSPtr&        pSSAOUpShaderData,
						const PSSamplersSPtr&              pSamplers )
		: ComputePass( pContext )
		, pContext( pContext )
		, pRasterizerState( pRasterizerState )
		, pBlendStateOff( pBlendStateOff )
		, pBlendState( pBlendState )
		, pDepthStencilState( pDepthStencilState )
		, pSSAOTarget( pSSAOTarget )
		, pOutputTarget( pOutputTarget )
		, pFullScreenVS( pFullScreenVS )
		, pSSAOShader( pSSAOShader )
		, pSSAOShaderData( pSSAOShaderData )
		, pSSAOUpShader( pSSAOUpShader )
		, pSSAOUpShaderData( pSSAOUpShaderData )
		, pSamplers( pSamplers )
	{
		pSSAOShaderData->setSize( { pOutputTarget->getWidth( ), pOutputTarget->getHeight( ) } );
		pSSAOUpShaderData->setSize( { pSSAOTarget->getWidth( ), pSSAOTarget->getHeight( ) } );
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	void SSAOPass::update( float delta )
	{
		pSSAOShaderData->update( pContext );
		pSSAOUpShaderData->update( pContext );
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to perform a compute pass.
	/// </summary>
	void SSAOPass::compute( )
	{
		// constants
		static const float blend[ ] = { 1.0f, 1.0f, 1.0f, 1.0f };
		ID3D11ShaderResourceView* clearTextures[] =
		{
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr
		};

		// +--------------------+
		// | SSAO & Down Sample |
		// +--------------------+
		// Set
		// - View Port
		// - Scissor Rectangle
		D3D11_RECT scissor;
		D3D11_VIEWPORT viewPort;

		scissor.left   = 0;
		scissor.top    = 0;
		scissor.right  = pSSAOTarget->getWidth( );
		scissor.bottom = pSSAOTarget->getHeight( );

		viewPort.Width  = static_cast< float >( scissor.right  );
		viewPort.Height = static_cast< float >( scissor.bottom );
		viewPort.TopLeftY = 0;
		viewPort.TopLeftX = 0;
		viewPort.MinDepth = 0;
		viewPort.MaxDepth = 1;

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );

		// Set
		// - Render Target
		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, 0, 0, nullptr, nullptr );
		pSSAOTarget->set( pContext );


		// Set
		// - Rasterizer
		// - Blend
		// - Depth Stencil
		pContext->RSSetState( pRasterizerState.get( ) );

		pContext->OMSetBlendState( pBlendStateOff.get( ), blend, 0xffffffff );
		pContext->OMSetDepthStencilState( pDepthStencilState.get( ), 0 );

		// Set
		// - Vertex Shader
		// - Pixel Shader
		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pSSAOShader.get( ), nullptr, 0 );

		// Set
		// - Samplers
		// - Textures & Constant Buffers
		pSSAOShaderData->set( pContext );
		pSamplers->set( pContext, 0 );

		// Set
		// - NULL Vertex Buffer
		// - Draw FullScreen Triangle
		pContext->IASetVertexBuffers( 0, 0, nullptr, nullptr, nullptr );
		pContext->IASetIndexBuffer( nullptr, DXGI_FORMAT_UNKNOWN, 0 );
		pContext->IASetInputLayout( nullptr );
		pContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		pContext->Draw( 3, 0 );

		pContext->PSSetShaderResources( 0, 16, clearTextures );



		// +------------------+
		// | Blur & Up Sample |
		// +------------------+
		// Set
		// - View Port
		// - Scissor Rectangle
		scissor.right = pOutputTarget->getWidth( );
		scissor.bottom = pOutputTarget->getHeight( );

		viewPort.Width = static_cast< float >( scissor.right );
		viewPort.Height = static_cast< float >( scissor.bottom );

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );

		// Set
		// - Render Target
		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, 0, 0, nullptr, nullptr );
		pOutputTarget->set( pContext );


		// Set
		// - Rasterizer
		// - Blend
		// - Depth Stencil
		pContext->RSSetState( pRasterizerState.get( ) );

		pContext->OMSetBlendState( pBlendState.get( ), blend, 0xffffffff );
		pContext->OMSetDepthStencilState( pDepthStencilState.get( ), 0 );

		// Set
		// - Vertex Shader
		// - Pixel Shader
		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pSSAOUpShader.get( ), nullptr, 0 );

		// Set
		// - Samplers
		// - Textures & Constant Buffers
		pSSAOUpShaderData->set( pContext );
		pSamplers->set( pContext, 0 );

		// Set
		// - NULL Vertex Buffer
		// - Draw FullScreen Triangle
		pContext->IASetVertexBuffers( 0, 0, nullptr, nullptr, nullptr );
		pContext->IASetIndexBuffer( nullptr, DXGI_FORMAT_UNKNOWN, 0 );
		pContext->IASetInputLayout( nullptr );
		pContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		pContext->Draw( 3, 0 );

		pContext->PSSetShaderResources( 0, 16, clearTextures );
	}
} // namespace directX11
} // namespace visual
