#pragma once
// Screen Space Ambiant Occlusion Pass.
//
// Project   : NaKama-Fx
// File Name : SSAOPass.h
// Date      : 04/12/2018
// Author    : Nicholas Welters

#ifndef _SSAO_PASS_DX11_H
#define _SSAO_PASS_DX11_H

#include "ComputePass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( MultipleRenderTarget );
	FORWARD_DECLARE( SSAOShaderBuffer );
	FORWARD_DECLARE( SSAOMaterialBuffer );

	/// <summary> Screen Space Ambiant Occlusion Pass. </summary>
	class SSAOPass : public ComputePass
	{
	public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pRasterizerState"> Primative to Fragment Settings. </param>
		/// <param name="pBlendStateOff"> Output Merget Blend Settings during AO sampling. </param>
		/// <param name="pBlendState"> Output Merget Blend Settings during AO blur and composite. </param>
		/// <param name="pDepthStencilState"> Output Merget Depth Stencil Settings. </param>
		/// <param name="pSSAOTarget"> Ambiant occlusion frame buffer. </param>
		/// <param name="pOutputTarget"> The scene render target that we will add the AO too. </param>
		/// <param name="pFullScreenVS"> Bufferless fullscreen vertex shader. </param>
		/// <param name="pSSRShader"> Pixel shader that builds the AO buffer. </param>
		/// <param name="pSSRShaderData"> Pixel shader data to build the AO buffer. </param>
		/// <param name="pSSRUpShader"> Pixel shader that composes the AO on top of the scene. </param>
		/// <param name="pSSRUpShaderData"> Pixel shader data used to composes the AO on top of the scene. </param>
		/// <param name="pSamplers"> Pixel shader samplers. </param>
		SSAOPass( const DeviceContextPtr_t&          pContext,
				  const ID3D11RasterizerStateSPtr&   pRasterizerState,
				  const ID3D11BlendStateSPtr&        pBlendStateOff,
				  const ID3D11BlendStateSPtr&        pBlendState,
				  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
				  const MultipleRenderTargetSPtr&    pSSAOTarget,
				  const MultipleRenderTargetSPtr&    pOutputTarget,
				  const ID3D11VertexShaderSPtr&      pFullScreenVS,
				  const ID3D11PixelShaderSPtr&       pSSAOShader,
				  const SSAOShaderBufferSPtr&        pSSAOShaderData,
				  const ID3D11PixelShaderSPtr&       pSSAOUpShader,
				  const SSAOShaderBufferSPtr&        pSSAOUpShaderData,
				  const PSSamplersSPtr&              pSamplers );

		/// <summary> ctor </summary>
		SSAOPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		SSAOPass( const SSAOPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		SSAOPass( SSAOPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~SSAOPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		SSAOPass& operator=( const SSAOPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		SSAOPass& operator=( SSAOPass&& move ) = delete;

		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		virtual void update( float delta ) override;

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		virtual void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> Primative to Fragment Settings. </summary>
		ID3D11RasterizerStateSPtr pRasterizerState;

		/// <summary> Output Merget Blend Settings during AO sampling. </summary>
		ID3D11BlendStateSPtr pBlendStateOff;

		/// <summary> Output Merget Blend Settings during AO blur and composite. </summary>
		ID3D11BlendStateSPtr pBlendState;

		/// <summary> Output Merget Depth Stencil Settings. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilState;

		/// <summary> AO frame buffer. </summary>
		MultipleRenderTargetSPtr pSSAOTarget;

		/// <summary> The scene render target that we will add the AO too. </summary>
		MultipleRenderTargetSPtr pOutputTarget;

		/// <summary> Bufferless fullscreen vertex shader. </summary>
		ID3D11VertexShaderSPtr pFullScreenVS;

		/// <summary> Pixel shader that builds the AO buffer. </summary>
		ID3D11PixelShaderSPtr pSSAOShader;

		/// <summary> Pixel shader data to build the AO buffer. </summary>
		SSAOShaderBufferSPtr pSSAOShaderData;

		/// <summary> Pixel shader that composes the AO on top of the scene. </summary>
		ID3D11PixelShaderSPtr pSSAOUpShader;

		/// <summary> Pixel shader data used to composes the AO on top of the scene. </summary>
		SSAOShaderBufferSPtr pSSAOUpShaderData;

		/// <summary> Pixel shader samplers. </summary>
		PSSamplersSPtr pSamplers;
	};
} // namespace directX11
} // namespace visual

#endif // _COMPUTE_HISTOGRAM_PASS_DX11_H
