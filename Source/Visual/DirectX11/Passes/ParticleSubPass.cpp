#include "ParticleSubPass.h"

// Render a full screen pass to be used for some post processing.
//
// Project   : NaKama-Fx
// File Name : ParticleSubPass.cpp
// Date      : 01/02/2019
// Author    : Nicholas Welters

#include "../Types/MeshBuffer.h"

#include "../Types/ParticleBuffer.h"
#include "Visual/FX/Vertex.h"

namespace visual
{
namespace directX11
{
	using std::max;

	/// <summary> ctor </summary>
	/// <param name="pParticleSOVertexShader"> A pass through vertex shader used in the update and draw passes. </param>
	/// <param name="pParticleSOGeometryShader"> Particle update geometry streaming out shader. </param>
	/// <param name="pBillboardGeometryShader"> Particle to billboard geometry shader. </param>
	/// <param name="pBillboardPixelShader"> Particle pixel shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pDepthStencilStateOff"> The sub pass depth stencil state. </param>
	/// <param name="pDepthStencilStateTest"> The sub pass depth stencil state. </param>
	/// <param name="pFrameGSConstants"> Per frame GS Constants. </param>
	/// <param name="pGSRandomTex"> Random data in a texture. </param>
	/// <param name="pGSSamplers"> The per scene geometry shader samplers. </param>
	/// <param name="pPSSamplers"> The per scene pixel shader samplers. </param>
	ParticleSubPass::ParticleSubPass( const ID3D11VertexShaderSPtr&       pParticleSOVertexShader,
									  const ID3D11GeometryShaderSPtr&     pParticleSOGeometryShader,
									  const ID3D11GeometryShaderSPtr&     pBillboardGeometryShader,
									  const ID3D11PixelShaderSPtr&        pBillboardPixelShader,
									  const ID3D11InputLayoutSPtr&        pInputLayput,
									  const ID3D11RasterizerStateSPtr&    pRasterizerState,
									  const ID3D11BlendStateSPtr&         pBlendState,
									  const ID3D11DepthStencilStateSPtr&  pDepthStencilStateOff,
									  const ID3D11DepthStencilStateSPtr&  pDepthStencilStateTest,
									  const FrameGSBufferSPtr&            pFrameGSConstants,
									  const ID3D11ShaderResourceViewSPtr& pGSRandomTex,
									  const GSSamplersSPtr&               pGSSamplers,
									  const PSSamplersSPtr&               pPSSamplers )
		: RenderingSubPass( pParticleSOVertexShader,
							pInputLayput,
							pRasterizerState,
							pDepthStencilStateOff,
							pBlendState )
		, particles( )
		, pFrameGSConstants( pFrameGSConstants )
		, pGSRandomTex( pGSRandomTex )
		, pParticleSOGeometryShader( pParticleSOGeometryShader )
		, pBillboardGeometryShader( pBillboardGeometryShader )
		, pBillboardPixelShader( pBillboardPixelShader )
		, pNormalRasterizerState( pRasterizerState )
		, pDepthStencilStateTest( pDepthStencilStateTest )
		, pGSSamplers( pGSSamplers )
		, pPSSamplers( pPSSamplers )
	{ }


	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void ParticleSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		for( const ParticleBufferSPtr& pParticleSystem : particles )
		{
			pParticleSystem->pGSConstants->get( ).gameTime += delta;
			pParticleSystem->pGSConstants->get( ).timeStep = delta;
			pParticleSystem->pGSConstants->update( pContext );
			pParticleSystem->pPSConstants->update( pContext );
		}
		pFrameGSConstants->update( pContext );
	}

	void ParticleSubPass::renderPass( const DeviceContextPtr_t & pContext )
	{
		pGSSamplers->set( pContext, 0 );
		pPSSamplers->set( pContext, 0 );

		ID3D11ShaderResourceView* gsResource[ 1 ] = { pGSRandomTex.get( ) };

		pContext->GSSetShaderResources( 0, 1, gsResource );

		pFrameGSConstants->set( pContext );

		pContext->GSSetShader( pParticleSOGeometryShader.get( ), nullptr, 0 );
		pContext->PSSetShader( nullptr, nullptr, 0 );

		pContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );

		constexpr unsigned int stride = sizeof( fx::VertexParticle::Vertex );
		constexpr unsigned int offset = 0;

		for( const ParticleBufferSPtr& pParticleSystem : particles )
		{
			pParticleSystem->pGSConstants->set( pContext );


			ID3D11Buffer* mStreamOutVB = pParticleSystem->getWriteBuffer( ).get( );
			pContext->SOSetTargets( 1, &mStreamOutVB, &offset );

			if( pParticleSystem->initialise )
			{
				ID3D11Buffer* p = pParticleSystem->pInitialParticles.get( );
				pContext->IASetVertexBuffers( 0, 1, &p, &stride, &offset );

				pContext->Draw( pParticleSystem->initialSize, 0 );
				pParticleSystem->initialise = false;
			}
			else
			{
				ID3D11Buffer* p = pParticleSystem->getReadBuffer( ).get( );
				pContext->IASetVertexBuffers( 0, 1, &p, &stride, &offset );

				pContext->DrawAuto( );
			}

			pParticleSystem->swap( );
		}

		ID3D11Buffer* bufferArray[ 1 ] = { nullptr };
		pContext->SOSetTargets( 1, bufferArray, &offset );

		pContext->PSSetShader( pBillboardPixelShader.get( ), nullptr, 0 );
		pContext->GSSetShader( pBillboardGeometryShader.get( ), nullptr, 0 );

		pContext->RSSetState( pNormalRasterizerState.get( ) );
		pContext->OMSetDepthStencilState( pDepthStencilStateTest.get( ), 0 );


		for( const ParticleBufferSPtr& pParticleSystem : particles )
		{
			pContext->OMSetBlendState( pParticleSystem->pBlendState.get( ), nullptr, 0xffffffff );

			ID3D11ShaderResourceView* psResource[ 1 ] = { pParticleSystem->pPSResources.get( ) };

			pContext->PSSetShaderResources( 0, 1, psResource );
			pParticleSystem->pGSConstants->set( pContext );
			pParticleSystem->pPSConstants->set( pContext );


			ID3D11Buffer* pDrawVB = pParticleSystem->getReadBuffer( ).get( );
			pContext->IASetVertexBuffers( 0, 1, &pDrawVB, &stride, &offset );
			pContext->DrawAuto( );
		}

		pContext->GSSetShader( nullptr, nullptr, 0 );
	}

	/// <summary> Add a particle system to be updated and drawn. </summary>
	/// <param name="pParticleSystem"> The particle system. </param>
	void ParticleSubPass::add( const ParticleBufferSPtr& pParticleSystem )
	{
		particles.emplace_back( pParticleSystem );
	}

	/// <summary> Remove a particle system. </summary>
	/// <param name="pParticleSystem"> The particle system to remove. </param>
	void ParticleSubPass::remove( const ParticleBufferSPtr& pParticleSystem )
	{
		particles.remove( pParticleSystem );
	}
} // namespace directX11
} // namespace visual
