#include "PostFXSubPass.h"

// Render a full screen pass to be used for post processing.
//
// Project   : NaKama-Fx
// File Name : PostFXSubPass.cpp
// Date      : 25/01/2019
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPixelShader"> The sub pass pixel shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pMesh"> The geometry used to render the pass. </param>
	/// <param name="pMaterial"> The textures and constants needed in the pass. </param>
	/// <param name="pPSPostFXBuffer"> The pixel shader constants. </param>
	/// <param name="samplers"> The texture sampler states. </param>
	PostFXSubPass::PostFXSubPass( const ID3D11PixelShaderSPtr& pPixelShader,
								  const ID3D11VertexShaderSPtr& pVertexShader,
								  const ID3D11InputLayoutSPtr& pInputLayput,
								  const ID3D11RasterizerStateSPtr& pRasterizerState,
								  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
								  const ID3D11BlendStateSPtr& pBlendState,
								  const MeshBufferSPtr& pMesh,
								  const PSResourcesSPtr& pMaterial,
								  const PostFXPSBufferSPtr& pPSPostFXBuffer,
								  const PSSamplersSPtr& pSamplers )
		: FullScreenSubPass( pPixelShader,
							 pVertexShader,
							 pInputLayput,
							 pRasterizerState,
							 pBlendState,
							 pDepthStencilState,
							 pMesh,
							 pMaterial,
							 pSamplers )
		, pPSPostFXBuffer( pPSPostFXBuffer )
	{ }

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void PostFXSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		pPSPostFXBuffer->update( pContext );
		FullScreenSubPass::update( pContext, delta );
	}

	/// <summary> Render the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to render the subpass. </param>
	void PostFXSubPass::renderPass( const DeviceContextPtr_t& pContext )
	{
		pPSPostFXBuffer->set( pContext );
		FullScreenSubPass::renderPass( pContext );
	}
} // namespace directX11
} // namespace visual
