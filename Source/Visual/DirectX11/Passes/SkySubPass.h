#pragma once
// Render a full screen pass to fill in the sky.
//
// Project   : NaKama-Fx
// File Name : SkySubPass.h
// Date      : 25/01/2019
// Author    : Nicholas Welters

#ifndef _SKY_SUB_PASS_DX11_H
#define _SKY_SUB_PASS_DX11_H

#include "RenderingSubPass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( MeshBuffer );

	/// <summary> Rendering Subpass </summary>
	class SkySubPass: public RenderingSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPixelShader"> The sub pass pixel shader. </param>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pMesh"> The geometry used to render the pass. </param>
		/// <param name="pTextures"> The textures and constants needed in the pass. </param>
		/// <param name="pVSSceneBuffer"> The vertex shader constants. </param>
		/// <param name="pPSSceneBuffer"> The pixel shader constants. </param>
		/// <param name="samplers"> The texture sampler states. </param>
		SkySubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
					const ID3D11VertexShaderSPtr&      pVertexShader,
					const ID3D11InputLayoutSPtr&       pInputLayput,
					const ID3D11RasterizerStateSPtr&   pRasterizerState,
					const ID3D11DepthStencilStateSPtr& pDepthStencilState,
					const ID3D11BlendStateSPtr&        pBlendState,
					const MeshBufferSPtr&              pMesh,
					const PSResourcesSPtr&             pTextures,
					const SceneVSBufferSPtr&           pVSSceneBuffer,
					const LightingPSBufferSPtr&        pPSSceneBuffer,
					const PSSamplersSPtr&              pSamplers );

		/// <summary> ctor </summary>
		SkySubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		SkySubPass( const SkySubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		SkySubPass( SkySubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~SkySubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		SkySubPass& operator=( const SkySubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		SkySubPass& operator=( SkySubPass&& move ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="pMaterial"> The textures and constants needed in the pass. </param>
		void setPSResources( const PSResourcesSPtr& pMaterial );

		/// <summary> Toggle the active state of the pass </summary>
		/// <param name="enable"> The state to set, true to draw the pass and false otherwise. </param>
		void enableDraw( bool enable );

	protected:
		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		virtual void renderPass( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The subpass pixel shader. </summary>
		ID3D11PixelShaderSPtr pPixelShader = nullptr;

		/// <summary> The geometry used to render the pass. </summary>
		MeshBufferSPtr pMesh = nullptr;

		/// <summary> The textures and constants needed in the pass. </summary>
		PSResourcesSPtr pTextures = nullptr;

		/// <summary> The vertex shader constants. </summary>
		SceneVSBufferSPtr pVSSceneBuffer = nullptr;

		/// <summary> The per scene vertex shader constants. </summary>
		LightingPSBufferSPtr pPSSceneBuffer = nullptr;

		/// <summary> The per scene pixel shader samplers. </summary>
		PSSamplersSPtr pPSSamplers = nullptr;

		/// <summary> The draw state toggle. </summary>
		bool enabled = false;
	};
} // namespace directX11
} // namespace visual

#endif // _SKY_SUB_PASS_DX11_H

