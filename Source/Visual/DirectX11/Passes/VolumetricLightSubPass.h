#pragma once
// A simple depth buffer sampling volumetric lighting pass.
//
// TODO: Improve performance and flexability.
//
// Project   : NaKama-Fx
// File Name : VolumetricLightSubPass.h
// Date      : 25/01/2019
// Author    : Nicholas Welters

#ifndef _VOLUMETRIC_LIGHT_SUB_PASS_DX11_H
#define _VOLUMETRIC_LIGHT_SUB_PASS_DX11_H

#include "FullScreenSubPass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Volumetric lighting using the main direction light as the source. </summary>
	class VolumetricLightSubPass : public FullScreenSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPixelShader"> The sub pass pixel shader. </param>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pMesh"> The geometry used to render the pass. </param>
		/// <param name="pTextures"> The textures and constants needed in the pass. </param>
		/// <param name="pMaterial"> The vertex shader constants. </param>
		/// <param name="pVSSceneBuffer"> The vertex shader constants. </param>
		/// <param name="pPSSunVolumeBuffer"> The pixel shader constants. </param>
		/// <param name="samplers"> The texture sampler states. </param>
		VolumetricLightSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
								const ID3D11VertexShaderSPtr&      pVertexShader,
								const ID3D11InputLayoutSPtr&       pInputLayput,
								const ID3D11RasterizerStateSPtr&   pRasterizerState,
								const ID3D11BlendStateSPtr&        pBlendState,
								const ID3D11DepthStencilStateSPtr& pDepthStencilState,
								const MeshBufferSPtr&              pMesh,
								const PSResourcesSPtr&             pMaterial,
								const SceneVSBufferSPtr&           pVSSceneBuffer,
								const SunVolumePSBufferSPtr&       pPSSunVolumeBuffer,
								const PSSamplersSPtr&              pSamplers );

		/// <summary> ctor </summary>
		VolumetricLightSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		VolumetricLightSubPass( const VolumetricLightSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		VolumetricLightSubPass( VolumetricLightSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~VolumetricLightSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		VolumetricLightSubPass& operator=( const VolumetricLightSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		VolumetricLightSubPass& operator=( VolumetricLightSubPass&& move ) = default;


		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

	protected:
		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		virtual void renderPass( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The per scene vertex shader samplers. </summary>
		SceneVSBufferSPtr pVSSceneBuffer;

		/// <summary> The per scene pixel shader samplers. </summary>
		SunVolumePSBufferSPtr pPSSunVolumeBuffer;
	};
} // namespace directX11
} // namespace visual

#endif // _VOLUMETRIC_LIGHT_SUB_PASS_DX11_H

