#pragma once

// A pass that will update and render some particles using the GPU by
// streaming out the particle updates from a geometry shader.
//
// Project   : NaKama-Fx
// File Name : ParticleSubPass.h
// Date      : 01/02/2019
// Author    : Nicholas Welters

#ifndef _PARTICLE_SUB_PASS_DX11_H
#define _PARTICLE_SUB_PASS_DX11_H

#include "RenderingSubPass.h"
#include "../APITypeDefs.h"

#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

#include <list>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( ParticleBuffer );

	/// <summary> Particle System update and rendering. </summary>
	class ParticleSubPass : public RenderingSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pParticleSOVertexShader"> A pass through vertex shader used in the update and draw passes. </param>
		/// <param name="pParticleSOGeometryShader"> Particle update geometry streaming out shader. </param>
		/// <param name="pBillboardGeometryShader"> Particle to billboard geometry shader. </param>
		/// <param name="pBillboardPixelShader"> Particle pixel shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pDepthStencilStateOff"> The sub pass depth stencil state. </param>
		/// <param name="pDepthStencilStateTest"> The sub pass depth stencil state. </param>
		/// <param name="pFrameGSConstants"> Per frame GS Constants. </param>
		/// <param name="pGSRandomTex"> Random data in a texture. </param>
		/// <param name="pGSSamplers"> The per scene geometry shader samplers. </param>
		/// <param name="pPSSamplers"> The per scene pixel shader samplers. </param>
		ParticleSubPass( const ID3D11VertexShaderSPtr&       pParticleSOVertexShader,
						 const ID3D11GeometryShaderSPtr&     pParticleSOGeometryShader,
						 const ID3D11GeometryShaderSPtr&     pBillboardGeometryShader,
						 const ID3D11PixelShaderSPtr&        pBillboardPixelShader,
						 const ID3D11InputLayoutSPtr&        pInputLayput,
						 const ID3D11RasterizerStateSPtr&    pRasterizerState,
						 const ID3D11BlendStateSPtr&         pBlendState,
						 const ID3D11DepthStencilStateSPtr&  pDepthStencilStateOff,
						 const ID3D11DepthStencilStateSPtr&  pDepthStencilStateTest,
						 const FrameGSBufferSPtr&            pFrameGSConstants,
						 const ID3D11ShaderResourceViewSPtr& pGSRandomTex,
						 const GSSamplersSPtr&               pGSSamplers,
						 const PSSamplersSPtr&               pPSSamplers );

		/// <summary> ctor </summary>
		ParticleSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ParticleSubPass( const ParticleSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ParticleSubPass( ParticleSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~ParticleSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ParticleSubPass& operator=( const ParticleSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ParticleSubPass& operator=( ParticleSubPass&& move ) = default;


		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

		/// <summary> Add a particle system to be updated and drawn. </summary>
		/// <param name="pParticleSystem"> The particle system. </param>
		virtual void add( const ParticleBufferSPtr& pParticleSystem );

		/// <summary> Remove a particle system. </summary>
		/// <param name="pParticleSystem"> The particle system to remove. </param>
		virtual void remove( const ParticleBufferSPtr& pParticleSystem );

	protected:
		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		virtual void renderPass( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The Particle Systems. </summary>
		std::list< ParticleBufferSPtr > particles = { };

		/// <summary> Per frame GS Constants. </summary>
		FrameGSBufferSPtr pFrameGSConstants = nullptr;

		/// <summary> Random data in a texture. </summary>
		ID3D11ShaderResourceViewSPtr pGSRandomTex = nullptr;

		/// <summary> Particle update geometry streaming out shader. </summary>
		ID3D11GeometryShaderSPtr pParticleSOGeometryShader = nullptr;

		/// <summary> Particle to billboard geometry shader. </summary>
		ID3D11GeometryShaderSPtr pBillboardGeometryShader = nullptr;

		/// <summary> Particle pixel shader. </summary>
		ID3D11PixelShaderSPtr pBillboardPixelShader = nullptr;

		/// <summary> The rasterizer state for this pass. </summary>
		ID3D11RasterizerStateSPtr pNormalRasterizerState = nullptr;

		/// <summary> The depth stencil state for the update pass. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilStateOff = nullptr;

		/// <summary> The depth stencil state for the draw pass. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilStateTest = nullptr;

		/// <summary> The per scene geometry shader samplers. </summary>
		GSSamplersSPtr pGSSamplers = nullptr;

		/// <summary> The per scene geometry shader samplers. </summary>
		PSSamplersSPtr pPSSamplers = nullptr;
	};
} // namespace directX11
} // namespace visual

#endif // _PARTICLE_SUB_PASS_DX11_H

