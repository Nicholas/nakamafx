#pragma once
// This is the renderes logical grouping of objects that need to be rendered together
// at the same time to the same render target. This lets us split the rendering to render targets
// so that we can perform a copy inbetween if it is needed.
//
// But here we are building a command list that will be executed later.
//
// Project   : NaKama-Fx
// File Name : DifferedRenderingPass.cpp
// Date      : 15/02/2017
// Author    : Nicholas Welters

#ifndef _DIFFERED_RENDERING_PASS_DX11_H
#define _DIFFERED_RENDERING_PASS_DX11_H

#include "RenderingPass.h"

namespace visual
{
namespace directX11
{
	/// <summary>
	/// A set of GPU instructions to render to an output target
	/// using a differed context to build a command list.
	/// </summary>
	class DifferedRenderingPass : public RenderingPass
	{
		public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pTarget"> The render target that we are going to draw to. </param>
		/// <param name="queueCommandList"> A function that accepts a command list with a priority. </param>
		/// <param name="priority"> The priority of the rendering pass. </param>
		DifferedRenderingPass(
			const DeviceContextPtr_t& pContext,
			const OutputTargetSPtr& pTarget,
			const std::function< void( const PriorityCommandList& ) >& queueCommandList,
			int priority
		);

		/// <summary> ctor </summary>
		DifferedRenderingPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DifferedRenderingPass( const DifferedRenderingPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DifferedRenderingPass( DifferedRenderingPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~DifferedRenderingPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		DifferedRenderingPass& operator=( const DifferedRenderingPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		DifferedRenderingPass& operator=( DifferedRenderingPass&& move ) = delete;


		/// <summary>
		/// Allow subclasses to add to the end of the rendering pass.
		/// This will then submit the rendering commands to the command queue.
		/// </summary>
		virtual void finalizeState( ) final override;

		private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext = nullptr;

		::std::function< void( const PriorityCommandList& ) > queueCommandList = [](const PriorityCommandList&){};

		/// <summary>
		/// The priority of this rendering in the over all pipe
		/// line so that we maintain order.
		/// </summary>
		int priority = 0;
	};
} // namespace directX11
} // namespace visual

#endif // _DIFFERED_RENDERING_PASS_DX11_H
