#include "DepthMeshSubPass.h"
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// This is used to perform a depth only pass.
//
// Project   : NaKama-Fx
// File Name : DepthMeshSubPass.cpp
// Date      : 25/01/2019
// Author    : Nicholas Welters

#include "../Types/Model.h"
#include "../Types/MeshBuffer.h"
#include "../Materials/DepthMaterialBuffer.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	using std::vector;

	/// <summary> ctor </summary>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
	DepthMeshSubPass::DepthMeshSubPass( const ID3D11VertexShaderSPtr&      pVertexShader,
										const ID3D11InputLayoutSPtr&       pInputLayput,
										const ID3D11RasterizerStateSPtr&   pRasterizerState,
										const ID3D11DepthStencilStateSPtr& pDepthStencilState,
										const ID3D11BlendStateSPtr&        pBlendState,
										const DepthVSBufferSPtr&           pVSSceneBuffer  )
		: MeshSubPass( pVertexShader, pInputLayput, pRasterizerState, pDepthStencilState, pBlendState )
		, pVSSceneBuffer( pVSSceneBuffer )
	{ }

	/// <summary> Set the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to set the subpass constants. </param>
	void DepthMeshSubPass::set( const DeviceContextPtr_t& pContext )
	{
		pVSSceneBuffer->set( pContext );
		pContext->PSSetShader( nullptr, nullptr, 0 );
	}

	/// <summary> Render a model. </summary>
	/// <param name="pContext"> The device context used to render the model. </param>
	/// <param name="pModel"> The model to render. </param>
	void DepthMeshSubPass::render( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel )
	{
		pModel->getShadowMaterial( )->set( pContext );

		pModel->getMesh( )->set( pContext );
		pModel->getMesh( )->render( pContext );
	}

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void DepthMeshSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		MeshSubPass::update( pContext, delta );
		pVSSceneBuffer->update( pContext );
	}

	/// <summary> Update a model. </summary>
	/// <param name="pContext"> The device context used to update the model. </param>
	/// <param name="pModel"> The model to update. </param>
	void DepthMeshSubPass::update( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel )
	{
		pModel->getShadowMaterial( )->update( pContext );
	}
} // namespace directX11
} // namespace visual
