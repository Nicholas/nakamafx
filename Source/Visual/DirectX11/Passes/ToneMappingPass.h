#pragma once
// Tone Mapping Sub Pass.
//
//
// Project   : NaKama-Fx
// File Name : ToneMappingPass.h
// Date      : 05/08/2019
// Author    : Nicholas Welters


#ifndef _TONE_MAPPING_PASS_DX11_H
#define _TONE_MAPPING_PASS_DX11_H

#include "ComputePass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( OutputTarget );
	FORWARD_DECLARE( ToneMappingPixelShaderBuffer );

	/// <summary> Tone Mapping Pass. </summary>
	class ToneMappingPass : public ComputePass
	{
	public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pRasterizerState"> Primative to Fragment Settings. </param>
		/// <param name="pBlendState"> Output Merget Blend Settings. </param>
		/// <param name="pDepthStencilState"> Output Merget Depth Stencil Settings. </param>
		/// <param name="pOutputTarget"> The scene render target. </param>
		/// <param name="pFullScreenVS"> Bufferless fullscreen vertex shader. </param>
		/// <param name="pToneMappingShader"> Pixel shader. </param>
		/// <param name="pToneMappingShaderData"> Pixel shader data. </param>
		/// <param name="pSamplers"> Pixel shader samplers. </param>
		ToneMappingPass( const DeviceContextPtr_t&               pContext,
						 const ID3D11RasterizerStateSPtr&        pRasterizerState,
						 const ID3D11BlendStateSPtr&             pBlendState,
						 const ID3D11DepthStencilStateSPtr&      pDepthStencilState,
						 const OutputTargetSPtr&                 pOutputTarget,
						 const ID3D11VertexShaderSPtr&           pFullScreenVS,
						 const ID3D11PixelShaderSPtr&            pToneMappingShader,
						 const ToneMappingPixelShaderBufferSPtr& pToneMappingPixelShaderData,
						 const PSSamplersSPtr&                   pSamplers );

		/// <summary> ctor </summary>
		ToneMappingPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ToneMappingPass( const ToneMappingPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ToneMappingPass( ToneMappingPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~ToneMappingPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ToneMappingPass& operator=( const ToneMappingPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ToneMappingPass& operator=( ToneMappingPass&& move ) = delete;


		/// <summary> Change the render target of the rendering pass. </summary>
		/// <param name="pTarget"> A new output target for the rendering pass. </param>
		void setOutputTarget( const OutputTargetSPtr& pTarget );

		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The time step. </param>
		virtual void update( float delta ) override;

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		virtual void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> Primative to Fragment Settings. </summary>
		ID3D11RasterizerStateSPtr pRasterizerState;

		/// <summary> Output Merget Blend Settings. </summary>
		ID3D11BlendStateSPtr pBlendState;

		/// <summary> Output Merget Depth Stencil Settings. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilState;

		/// <summary> The scene render target. </summary>
		OutputTargetSPtr pOutputTarget;

		/// <summary> ToneMapping vertex shader. </summary>
		ID3D11VertexShaderSPtr pFullScreenVS;

		/// <summary> ToneMapping pixel shader. </summary>
		ID3D11PixelShaderSPtr pToneMappingShader;

		/// <summary> Pixel shader data. </summary>
		ToneMappingPixelShaderBufferSPtr pToneMappingPixelShaderData;

		/// <summary> Pixel shader samplers. </summary>
		PSSamplersSPtr pSamplers;
	};
} // namespace directX11
} // namespace visual

#endif // _TONE_MAPPING_PASS_DX11_H
