#include "GeometryMeshSubPass.h"
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// This makes use of a geometry shader to modify the primitives.
//
// Project   : NaKama-Fx
// File Name : GeometryMeshSubPass.cpp
// Date      : 11/06/2017
// Author    : Nicholas Welters

#include "../Types/Model.h"
#include "../Types/MeshBuffer.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	using std::vector;

	/// <summary> ctor </summary>
	/// <param name="pPixelShader"> The sub pass pixel shader. </param>
	/// <param name="pGeometryShader"> The sub pass geometry shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
	/// <param name="pPSLightingBuffer"> The sub pass pixel shader scene constants. </param>
	/// <param name="pGSSceneBuffer"> The sub pass geomertry shader scene constants. </param>
	/// <param name="pPSSamplers"> The sub pass pixel shader texture samplers. </param>
	/// <param name="pPSResources"> The sub pass pixel shader resources. </param>
	/// <param name="offset"> The sub pass pixel shader resource start offset. </param>
	GeometryMeshSubPass::GeometryMeshSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
											  const ID3D11GeometryShaderSPtr&    pGeometryShader,
											  const ID3D11VertexShaderSPtr&      pVertexShader,
											  const ID3D11InputLayoutSPtr&       pInputLayput,
											  const ID3D11RasterizerStateSPtr&   pRasterizerState,
											  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
											  const ID3D11BlendStateSPtr&        pBlendState,
											  const SceneVSBufferSPtr&           pVSSceneBuffer,
											  const LightingPSBufferSPtr&        pPSLightingBuffer,
											  const SceneGSBufferSPtr&           pGSSceneBuffer,
											  const PSSamplersSPtr&              pPSSamplers,
											  const PSResourcesSPtr&             pPSResources,
											  const uint32_t                     offset )
		: ColourMeshSubPass( pPixelShader,
							 pVertexShader,
							 pInputLayput,
							 pRasterizerState,
							 pDepthStencilState,
							 pBlendState,
							 pVSSceneBuffer,
							 pPSLightingBuffer,
							 pPSSamplers,
							 pPSResources,
							 offset )
		, pGeometryShader( pGeometryShader )
		, pGSSceneBuffer( pGSSceneBuffer )
	{ }

	/// <summary> Set the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to set the subpass constants. </param>
	void GeometryMeshSubPass::set( const DeviceContextPtr_t& pContext )
	{
		ColourMeshSubPass::set( pContext );
		pContext->GSSetShader( pGeometryShader.get( ), nullptr, 0 );
		pGSSceneBuffer->set( pContext );
	}

	/// <summary> Unset the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to unset the subpass constants. </param>
	void GeometryMeshSubPass::unset( const DeviceContextPtr_t & pContext )
	{
		ColourMeshSubPass::unset( pContext );
		pContext->GSSetShader( nullptr, nullptr, 0 );
	}

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void GeometryMeshSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		ColourMeshSubPass::update( pContext, delta );
		pGSSceneBuffer->update( pContext );
	}
} // namespace directX11
} // namespace visual
