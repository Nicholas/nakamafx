#pragma once
// A generic compute pass that we can use to build simple pipelines.
//
// Project   : NaKama-Fx
// File Name : GenericComputePass.h
// Date      : 04/12/2018
// Author    : Nicholas Welters

#ifndef _GENERIC_COMPUTE_PASS_DX11_H
#define _GENERIC_COMPUTE_PASS_DX11_H

#include "ComputePass.h"
#include "../APITypeDefs.h"

#include <glm/glm.hpp>
#include <vector>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( UATarget );

	/// <summary>
	///	A generic compute shader pass that provides us the shader constant data used.
	/// </summary>
	template< typename ComputeConstantBuffer >
	class GenericComputePass final : public ComputePass
	{
	public:
		typedef std::shared_ptr< ComputeConstantBuffer > ComputeConstantBufferSPtr;

		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pComputeShader"> The compute shader that will be run. </param>
		/// <param name="pUAVs"> The unordered acces views the compute shader can write to. </param>
		/// <param name="samplers"> Samplers used in the compute pass. </param>
		/// <param name="pSRVs"> Shader resources used in the compute pass. </param>
		/// <param name="offset"> Shader resources offset. </param>
		/// <param name="pConstantBuffer"> Compute shader generic constant buffer. </param>
		/// <param name="dispatch"> Compute shader dispatch sizes. </param>
		GenericComputePass( const DeviceContextPtr_t& pContext,
							const ID3D11ComputeShaderSPtr& pComputeShader,
							const UATargetSPtr& pUAVs,
							const std::vector< ID3D11SamplerStateSPtr >& samplers,
							const std::vector< ID3D11ShaderResourceViewSPtr >& pSRVs,
							uint32_t offset,
							const ComputeConstantBufferSPtr& pConstantBuffer,
							const glm::ivec3& dispatch );

		/// <summary> dtor </summary>
		virtual ~GenericComputePass( ) = default;

		/// <summary> ctor </summary>
		GenericComputePass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		GenericComputePass( const GenericComputePass& ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		GenericComputePass( GenericComputePass&& ) = delete;

		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		GenericComputePass& operator=( const GenericComputePass& ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		GenericComputePass& operator=( GenericComputePass&& ) = delete;

		/// <summary>
		///	This is were we will update any gpu date that's changed on the cpu.
		/// </summary>
		void update( float delta ) override;

		/// <summary>
		///	Get the compute shader pass' constants buffer so that we can update them.
		/// </summary>
		ComputeConstantBufferSPtr getConstants( );

		/// <summary>
		///	Change an SRV in the compute pass.
		/// </summary>
		void setSRV( uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV );

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> The compute shader that will be run. </summary>
		ID3D11ComputeShaderSPtr pComputeShader;

		/// <summary> The unordered acces views the compute shader can write to. </summary>
		UATargetSPtr pUAVs;

		/// <summary> The unordered acces views raw pointer array. </summary>
		std::vector< ID3D11UnorderedAccessView* > pAccessViews;

		/// <summary> The unordered acces views clear pointer array. </summary>
		std::vector< ID3D11UnorderedAccessView* > pClearUAVs;

		/// <summary> The unordered acces views inital count values. </summary>
		std::vector< unsigned int > initIndexes;

		/// <summary> Samplers used in the compute pass. </summary>
		std::vector< ID3D11SamplerStateSPtr > samplers;

		/// <summary> Samplers raw pointer array. </summary>
		std::vector< ID3D11SamplerState* > pSamplers;

		/// <summary> Samplers clear pointer array. </summary>
		std::vector< ID3D11SamplerState* > pClearSamplers;

		/// <summary> Shader resources used in the compute pass. </summary>
		std::vector< ID3D11ShaderResourceViewSPtr > pSRVs;

		/// <summary> Shader resources raw pointers array. </summary>
		std::vector< ID3D11ShaderResourceView* > pShaderResources;

		/// <summary> Shader resources clear pointers array. </summary>
		std::vector< ID3D11ShaderResourceView* > pClearSRVs;

		/// <summary> Shader resources offset. </summary>
		uint32_t offset;

		/// <summary> Compute shader constant buffer defined by our template type. </summary>
		ComputeConstantBufferSPtr pConstantBuffer;

		/// <summary> Compute shader dispatch sizes. </summary>
		glm::ivec3 dispatch;
	};
} // namespace directX11
} // namespace visual

#include "GenericComputePass.hpp"

#endif // _GENERIC_COMPUTE_PASS_DX11_H
