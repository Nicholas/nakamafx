#pragma once
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// Project   : NaKama-Fx
// File Name : RenderingSubPass.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _RENDERING_SUB_PASS_DX11_H
#define _RENDERING_SUB_PASS_DX11_H

#include "../APITypeDefs.h"

#include <glm/vec4.hpp>

namespace visual
{
namespace directX11
{
	/// <summary> Rendering Subpass </summary>
	class RenderingSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		RenderingSubPass( const ID3D11VertexShaderSPtr&      pVertexShader,
						  const ID3D11InputLayoutSPtr&       pInputLayput,
						  const ID3D11RasterizerStateSPtr&   pRasterizerState,
						  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
						  const ID3D11BlendStateSPtr&        pBlendState );

		/// <summary> ctor </summary>
		RenderingSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		RenderingSubPass( const RenderingSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		RenderingSubPass( RenderingSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~RenderingSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		RenderingSubPass& operator=( const RenderingSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		RenderingSubPass& operator=( RenderingSubPass&& move ) = default;

		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta );

		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		void render( const DeviceContextPtr_t& pContext );

		/// <summary> Chefck what the enabled state of the sub pass is. </summary>
		/// <returns> The enabled state. </returns>
		bool isEnabled( ) const;

		/// <summary> Set the enabled state of the sub pass. </summary>
		/// <param name="enable"> The enabled state. </param>
		void setEnabled( bool enable );

	protected:
		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		virtual void renderPass( const DeviceContextPtr_t& pContext ) = 0;

	private:
		/// <summary> The vertex shader that we are going to use to process the mesh. </summary>
		ID3D11VertexShaderSPtr pVertexShader = nullptr;

		/// <summary>
		/// The input assembler required to render the objects in the following meshs
		/// with the Vertex shader that we are going to use.
		/// </summary>
		ID3D11InputLayoutSPtr pInputLayput = nullptr;

		/// <summary> Primative to Fragment Settings. </summary>
		ID3D11RasterizerStateSPtr pRasterizerState = nullptr;

		/// <summary> Output Merget Depth Stencil Settings. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilState = nullptr;

		/// <summary> Output Merget Blend Settings. </summary>
		ID3D11BlendStateSPtr pBlendState = nullptr;

		/// <summary> Output Merget Blend Factors. </summary>
		glm::vec4 blendFactor = { 0, 0, 0, 0 };

		/// <summary> Output Merget Sample Mask. </summary>
		uint32_t sampleMask = 0x00000000;

		/// <summary> Enable the sub pass, allow us to skip the pass so that we can toggle passes. </summary>
		bool enabled = false;
	};
} // namespace directX11
} // namespace visual

#endif // _RENDERING_SUB_PASS_DX11_H

