#include "DepthGeometryMeshSubPass.h"
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// This makes use of a geometry shader to modify the primitives during a depth only pass.
//
// Project   : NaKama-Fx
// File Name : DepthGeometryMeshSubPass.cpp
// Date      : 25/01/2019
// Author    : Nicholas Welters

#include "../Types/MeshBuffer.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	using std::vector;

	/// <summary> ctor </summary>
	/// <param name="pGeometryShader"> The sub pass geometry shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
	/// <param name="pGSSceneBuffer"> The sub pass geomertry shader scene constants. </param>
	DepthGeometryMeshSubPass::DepthGeometryMeshSubPass( const ID3D11GeometryShaderSPtr&    pGeometryShader,
														const ID3D11VertexShaderSPtr&      pVertexShader,
														const ID3D11InputLayoutSPtr&       pInputLayput,
														const ID3D11RasterizerStateSPtr&   pRasterizerState,
														const ID3D11DepthStencilStateSPtr& pDepthStencilState,
														const ID3D11BlendStateSPtr&        pBlendState,
														const DepthVSBufferSPtr&           pVSSceneBuffer,
														const SceneGSBufferSPtr&           pGSSceneBuffer )
		: DepthMeshSubPass( pVertexShader,
							pInputLayput,
							pRasterizerState,
							pDepthStencilState,
							pBlendState,
							pVSSceneBuffer )
		, pGeometryShader( pGeometryShader )
		, pGSSceneBuffer( pGSSceneBuffer )
	{ }

	/// <summary> Set the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to set the subpass constants. </param>
	void DepthGeometryMeshSubPass::set( const DeviceContextPtr_t& pContext )
	{
		DepthMeshSubPass::set( pContext );
		pContext->GSSetShader( pGeometryShader.get( ), nullptr, 0 );
		pGSSceneBuffer->set( pContext );
	}

	/// <summary> Unset the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to unset the subpass constants. </param>
	void DepthGeometryMeshSubPass::unset( const DeviceContextPtr_t & pContext )
	{
		DepthMeshSubPass::unset( pContext );
		pContext->GSSetShader( nullptr, nullptr, 0 );
	}

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void DepthGeometryMeshSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		DepthMeshSubPass::update( pContext, delta );
		pGSSceneBuffer->update( pContext );
	}
} // namespace directX11
} // namespace visual
