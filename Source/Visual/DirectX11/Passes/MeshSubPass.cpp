#include "MeshSubPass.h"

// Manage setting a constant buffer.
//
// Project   : NaKama-Fx
// File Name : MeshSubPass.cpp
// Date      : 11/06/2017
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	MeshSubPass::MeshSubPass( const ID3D11VertexShaderSPtr&      pVertexShader,
							  const ID3D11InputLayoutSPtr&       pInputLayput,
							  const ID3D11RasterizerStateSPtr&   pRasterizerState,
							  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
							  const ID3D11BlendStateSPtr&        pBlendState )
		: RenderingSubPass( pVertexShader, pInputLayput, pRasterizerState, pDepthStencilState, pBlendState )
		, models( )
	{ }

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void MeshSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		for( const ModelSPtr& pModel : models )
		{
			update( pContext, pModel );
		}
	}

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void MeshSubPass::update( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel )
	{ }

	/// <summary> Render the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to render the subpass. </param>
	void MeshSubPass::renderPass( const DeviceContextPtr_t& pContext )
	{
		set( pContext );

		for( const ModelSPtr& pModel : models )
		{
			render( pContext, pModel );
		}

		unset( pContext );
	}

	/// <summary> Unset the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to unset the subpass constants. </param>
	void MeshSubPass::unset( const DeviceContextPtr_t & pContext )
	{ }

	/// <summary> Add a model to the subpass. </summary>
	/// <param name="pModel"> The model to render in the subpass. </param>
	void MeshSubPass::add( const ModelSPtr & pModel )
	{
		models.push_back( pModel );
	}

	/// <summary> Remove a model to the subpass. </summary>
	/// <param name="pModel"> The model to remove in the subpass. </param>
	void MeshSubPass::remove( const ModelSPtr & pModel )
	{
		models.remove( pModel );
	}
} // namespace directX11
} // namespace visual
