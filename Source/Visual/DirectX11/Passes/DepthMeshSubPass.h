#pragma once
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// This is used to perform a depth only pass.
//
// Project   : NaKama-Fx
// File Name : DepthMeshSubPass.h
// Date      : 25/01/2019
// Author    : Nicholas Welters

#ifndef _DEPTH_MESH_SUB_PASS_DX11_H
#define _DEPTH_MESH_SUB_PASS_DX11_H

#include "MeshSubPass.h"
#include "../APITypeDefs.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary>  Depth Only Mesh Rendering Subpass </summary>
	class DepthMeshSubPass : public MeshSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
		DepthMeshSubPass( const ID3D11VertexShaderSPtr&      pVertexShader,
						  const ID3D11InputLayoutSPtr&       pInputLayput,
						  const ID3D11RasterizerStateSPtr&   pRasterizerState,
						  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
						  const ID3D11BlendStateSPtr&        pBlendState,
						  const DepthVSBufferSPtr&           pVSSceneBuffer );

		/// <summary> ctor </summary>
		DepthMeshSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DepthMeshSubPass( const DepthMeshSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DepthMeshSubPass( DepthMeshSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~DepthMeshSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		DepthMeshSubPass& operator=( const DepthMeshSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		DepthMeshSubPass& operator=( DepthMeshSubPass&& move ) = default;


		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

	protected:
		/// <summary> Set the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to set the subpass constants. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Render a model. </summary>
		/// <param name="pContext"> The device context used to render the model. </param>
		/// <param name="pModel"> The model to render. </param>
		virtual void render( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel ) override;

		/// <summary> Update a model. </summary>
		/// <param name="pContext"> The device context used to update the model. </param>
		/// <param name="pModel"> The model to update. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel ) override;

	private:
		/// <summary> The per scene vertex shader constants. </summary>
		DepthVSBufferSPtr pVSSceneBuffer = nullptr;
	};
} // namespace directX11
} // namespace visual

#endif // _DEPTH_MESH_SUB_PASS_DX11_H

