#pragma once
// This is the renderes logical grouping of objects that need to be computed.
//
// Project   : NaKama-Fx
// File Name : ComputePass.h
// Date      : 27/11/2018
// Author    : Nicholas Welters

#ifndef _COMPUTE_PASS_DX11_H
#define _COMPUTE_PASS_DX11_H

#include "../APITypeDefs.h"

#include <Threading/AsyncController.h>

namespace visual
{
namespace directX11
{
	class ComputePass : protected tools::AsyncController
	{
	public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		explicit ComputePass( const DeviceContextPtr_t& pContext );

		/// <summary> ctor </summary>
		ComputePass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ComputePass( const ComputePass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ComputePass( ComputePass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~ComputePass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ComputePass& operator=( const ComputePass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ComputePass& operator=( ComputePass&& move ) = delete;

		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The frame step size. </param>
		virtual void update( float delta );

		/// <summary>
		///	This is were we will set up the state of the GPU to render
		///	all the object in this scene.
		/// </summary>
		void render( );

	protected:
		/// <summary> Allow subclasses to add to the end of the compute pass. </summary>
		virtual void finalizeState( );

		/// <summary>
		/// This is where we are going to get the specialized subclass to perform our compute pass.
		/// </summary>
		virtual void compute( ) = 0;

	private:
		/// <summary>	This will set the GPU state like the render target and depth stencil buffer. </summary>
		void setState( );

		/// <summary> Put the  renderer back in a state that we know. </summary>
		void restorState( );

		/// <summary> The derived classes interface to finalize its updates to create a consistent internal state. </summary>
		virtual void update( ) final override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;
	};

	class DifferedComputePass : public ComputePass
	{
		public:
			DifferedComputePass(
			const DeviceContextPtr_t& pContext,
			const ::std::function< void( const PriorityCommandList& ) > queueCommandList,
			const int& priority
		);
		virtual ~DifferedComputePass( ) = default;

		/// <summary> 
		/// Allow subclasses to add to the end of the compute pass.
		/// This will then submit the compute commands to the command queue.
		/// </summary>
		virtual void finalizeState( ) final override;

		private:
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Fields
		////////////////////////////////////////////////////////////////////////////////////////////////////
		DeviceContextPtr_t pContext;

		::std::function< void( const PriorityCommandList& ) > queueCommandList;

		/// <summary>
		/// The priority of this rendering in the over all pipe
		/// line so that we maintain order.
		/// </summary> 
		int priority;
	};
} // namespace directX11
} // namespace visual

#endif // _COMPUTE_PASS_DX11_H
