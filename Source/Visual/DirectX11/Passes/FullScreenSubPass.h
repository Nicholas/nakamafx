#pragma once
// Render a full screen pass to be used for some post processing.
//
// Project   : NaKama-Fx
// File Name : FullScreenSubPass.h
// Date      : 25/01/2019
// Author    : Nicholas Welters

#ifndef _FULLSCREEN_SUB_PASS_DX11_H
#define _FULLSCREEN_SUB_PASS_DX11_H

#include "RenderingSubPass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( MeshBuffer );

	/// <summary> A single mesh rendering subpass </summary>
	class FullScreenSubPass: public RenderingSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPixelShader"> The sub pass pixel shader. </param>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pMesh"> The geometry used to render the pass. </param>
		/// <param name="pTextures"> The textures and constants needed in the pass. </param>
		/// <param name="pMaterial"> The vertex shader constants. </param>
		/// <param name="pPSSceneBuffer"> The pixel shader constants. </param>
		/// <param name="samplers"> The texture sampler states. </param>
		FullScreenSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
						   const ID3D11VertexShaderSPtr&      pVertexShader,
						   const ID3D11InputLayoutSPtr&       pInputLayput,
						   const ID3D11RasterizerStateSPtr&   pRasterizerState,
						   const ID3D11BlendStateSPtr&        pBlendState,
						   const ID3D11DepthStencilStateSPtr& pDepthStencilState,
						   const MeshBufferSPtr&              pMesh,
						   const PSResourcesSPtr&             pMaterial,
						   const PSSamplersSPtr&              pSamplers );

		/// <summary> ctor </summary>
		FullScreenSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		FullScreenSubPass( const FullScreenSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		FullScreenSubPass( FullScreenSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~FullScreenSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		FullScreenSubPass& operator=( const FullScreenSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		FullScreenSubPass& operator=( FullScreenSubPass&& move ) = default;

	protected:
		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		virtual void renderPass( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The subpass pixel shader. </summary>
		ID3D11PixelShaderSPtr pPixelShader;

		/// <summary> The geometry used to render the pass. </summary>
		MeshBufferSPtr pMesh;

		/// <summary> The textures and constants needed in the pass. </summary>
		PSResourcesSPtr pMaterial = nullptr;

		/// <summary> The per scene pixel shader samplers. </summary>
		PSSamplersSPtr pPSSamplers = nullptr;
	};
} // namespace directX11
} // namespace visual

#endif // _FULLSCREEN_SUB_PASS_DX11_H

