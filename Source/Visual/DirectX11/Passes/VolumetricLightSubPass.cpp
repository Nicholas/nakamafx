#include "VolumetricLightSubPass.h"
// Render a full screen pass to be used for post processing.
//
// Project   : NaKama-Fx
// File Name : VolumetricLightSubPass.cpp
// Date      : 25/01/2019
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPixelShader"> The sub pass pixel shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pMesh"> The geometry used to render the pass. </param>
	/// <param name="pTextures"> The textures and constants needed in the pass. </param>
	/// <param name="pMaterial"> The vertex shader constants. </param>
	/// <param name="pVSSceneBuffer"> The vertex shader constants. </param>
	/// <param name="pPSSunVolumeBuffer"> The pixel shader constants. </param>
	/// <param name="samplers"> The texture sampler states. </param>
	VolumetricLightSubPass::VolumetricLightSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
													const ID3D11VertexShaderSPtr&      pVertexShader,
													const ID3D11InputLayoutSPtr&       pInputLayput,
													const ID3D11RasterizerStateSPtr&   pRasterizerState,
													const ID3D11BlendStateSPtr&        pBlendState,
													const ID3D11DepthStencilStateSPtr& pDepthStencilState,
													const MeshBufferSPtr&              pMesh,
													const PSResourcesSPtr&             pMaterial,
													const SceneVSBufferSPtr&           pVSSceneBuffer,
													const SunVolumePSBufferSPtr&       pPSSunVolumeBuffer,
													const PSSamplersSPtr&              pSamplers )
		: FullScreenSubPass( pPixelShader,
							 pVertexShader,
							 pInputLayput,
							 pRasterizerState,
							 pBlendState,
							 pDepthStencilState,
							 pMesh,
							 pMaterial,
							 pSamplers )
		, pVSSceneBuffer( pVSSceneBuffer )
		, pPSSunVolumeBuffer( pPSSunVolumeBuffer )
	{ }

	/// <summary> Update the subpasses constants that might be changed. </summary>
	/// <param name="pContext"> The device context used to update the subpass contents. </param>
	/// <param name="delta"> The time taken to draw the last frame. </param>
	void VolumetricLightSubPass::update( const DeviceContextPtr_t& pContext, const float& delta )
	{
		pVSSceneBuffer->update( pContext );
		pPSSunVolumeBuffer->update( pContext );
		FullScreenSubPass::update( pContext, delta );
	}

	/// <summary> Render the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to render the subpass. </param>
	void VolumetricLightSubPass::renderPass( const DeviceContextPtr_t& pContext )
	{
		pVSSceneBuffer->set( pContext );
		pPSSunVolumeBuffer->set( pContext );
		FullScreenSubPass::renderPass( pContext );
	}
} // namespace directX11
} // namespace visual
