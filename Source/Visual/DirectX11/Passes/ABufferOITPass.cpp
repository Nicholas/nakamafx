#include "ABufferOITPass.h"
// An order independent transparency rendering pass.
//
// This construct an A-Buffer (fragment linked list per pixel) to render transparent and translucent
// models with out having to do CPU side work to make sure that the final fragment order is correct.
//
// Based on AMD's realtime concurrent linked list construction on the GPU.
//
// Project   : NaKama-Fx
// File Name : ABufferOITPass.cpp
// Date      : 04/12/2018
// Author    : Nicholas Welters

#include "../Targets/UATarget.h"
#include "../Targets/MultipleRenderTarget.h"
#include "../Targets/DepthStencilTarget.h"
#include "../Types/Model.h"
#include "../Types/MeshBuffer.h"
#include "../Materials/ColourMaterialBuffer.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pABuffer"> The unordered access views used to build our linked list. </param>
	/// <param name="pResolveTarget"> The render targets taht we are going to output the final result to. </param>
	/// <param name="pDepthBuffer"> The A buffers depths buffer when out putting our differed info. </param>
	/// <param name="pRasterizerState"> Rasterizer state. </param>
	/// <param name="pBlendState"> Blend State for the final composition pass on top of the output targets. </param>
	/// <param name="pDepthStencilStateOn"> The depth stencil state on. </param>
	/// <param name="pDepthStencilStateOff"> The depth stencil state off. </param>
	/// <param name="pInputLayputObjMesh"> The mesh vertex input layout. </param>
	/// <param name="pSamplers"> The per scene pixel shader samplers. </param>
	/// <param name="pPSResources"> The per scene pixel shader resources. </param>
	/// <param name="offset"> The per scene pixel shader resources offset. </param>
	/// <param name="pBackBuffer"> The image that we are going to render the scene on top of. </param>
	/// <param name="pVSSceneBuffer"> The scene vertex shader constants. </param>
	/// <param name="pPSLightingBuffer"> The scene lights. </param>
	/// <param name="pMeshVS"> The mesh vertex shader. </param>
	/// <param name="pABufferConstructPS"> The A-buffer construction pixel shader. </param>
	/// <param name="pABufferResolvePSBuffer"> The A-buffer resolve constants. </param>
	/// <param name="pFullScreenVS"> A bufferless fullscreen verex shader. </param>
	/// <param name="pABufferResolvePS"> The A-buffer resolve pixel shader. </param>
	ABufferOITPass::ABufferOITPass( const DeviceContextPtr_t&           pContext,
									const UATargetSPtr&                 pABuffer,
									const MultipleRenderTargetSPtr&     pResolveTarget,
									const DepthStencilTargetSPtr&       pDepthBuffer,
									const ID3D11RasterizerStateSPtr&    pRasterizerState,
									const ID3D11BlendStateSPtr&         pBlendState,
									const ID3D11DepthStencilStateSPtr&  pDepthStencilStateOn,
									const ID3D11DepthStencilStateSPtr&  pDepthStencilStateOff,
									const ID3D11InputLayoutSPtr&        pInputLayputObjMesh,
									const PSSamplersSPtr&               pSamplers,
									const PSResourcesSPtr&              pPSResources,
									const uint32_t                      offset,
									const ID3D11ShaderResourceViewSPtr& pBackBuffer,
									const SceneVSBufferSPtr&            pVSSceneBuffer,
									const LightingPSBufferSPtr&         pPSLightingBuffer,
									const ID3D11VertexShaderSPtr&       pMeshVS,
									const ID3D11PixelShaderSPtr&        pABufferConstructPS,
									const ABufferResolvePSBufferSPtr&   pABufferResolvePSBuffer,
									const ID3D11VertexShaderSPtr&       pFullScreenVS,
									const ID3D11PixelShaderSPtr&        pABufferResolvePS )
		: ComputePass( pContext )
		, pContext( pContext )
		, pABuffer( pABuffer )
		, pResolveTarget( pResolveTarget )
		, pDepthBuffer( pDepthBuffer )
		, pRasterizerState( pRasterizerState )
		, pBlendState( pBlendState )
		, pDepthStencilStateOn( pDepthStencilStateOn )
		, pDepthStencilStateOff( pDepthStencilStateOff )
		, pInputLayputObjMesh( pInputLayputObjMesh )
		, pSamplers( pSamplers )
		, pPSResources( pPSResources )
		, offset( offset )
		, pBackBuffer( pBackBuffer )
		, pVSSceneBuffer( pVSSceneBuffer )
		, pPSLightingBuffer( pPSLightingBuffer )
		, pMeshVS( pMeshVS )
		, pABufferConstructPS( pABufferConstructPS )
		, pABufferResolvePSBuffer( pABufferResolvePSBuffer )
		, pFullScreenVS( pFullScreenVS )
		, pABufferResolvePS( pABufferResolvePS )
		, scissor( )
		, viewPort( )
		, models( )
	{
		scissor.left = 0;
		scissor.top = 0;
		scissor.right = pResolveTarget->getWidth( );
		scissor.bottom = pResolveTarget->getHeight( );

		viewPort.Width = static_cast< float >( scissor.right );
		viewPort.Height = static_cast< float >( scissor.bottom );
		viewPort.TopLeftY = 0;
		viewPort.TopLeftX = 0;
		viewPort.MinDepth = 0;
		viewPort.MaxDepth = 1;

		pABufferResolvePSBuffer->get( ).screenSize.x = static_cast< float >( pResolveTarget->getWidth( ) );
		pABufferResolvePSBuffer->get( ).screenSize.y = static_cast< float >( pResolveTarget->getHeight( ) );
		pABufferResolvePSBuffer->update( pContext );
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	/// <param name="delta"> The frame step size. </param>
	void ABufferOITPass::update( float delta )
	{
		for( const ModelSPtr& pModel : models )
		{
			pModel->getColourMaterial( )->update( pContext );
		}
	}

	/// <summary> Add a mesh to be drawn during the OIT pass. </summary>
	/// <param name="pModel"> The model to render in the subpass. </param>
	void ABufferOITPass::add( const ModelSPtr & pModel )
	{
		models.push_back( pModel );
	}

	/// <summary> Remove a mesh to be drawn during the OIT pass. </summary>
	/// <param name="pModel"> The model to remove from the subpass. </param>
	void ABufferOITPass::remove( const ModelSPtr & pModel )
	{
		models.remove( pModel );
	}

	/// <summary> Change a resource. </summary>
	/// <param name="index"> The index of the shader resource view to update. </param>
	/// <param name="pSRV"> The new shader resource view to use in the pass. </param>
	void ABufferOITPass::setSRV( const uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV ) const
	{
		pPSResources->set( index, pSRV );
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to perform a compute pass.
	/// </summary>
	void ABufferOITPass::compute( )
	{
		ID3D11ShaderResourceView * ppNullSRVs[ ] =
		{
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr, nullptr, nullptr
		};


		// Clear the start offset buffer by magic value.
		static const UINT clearValueUint[ 1 ] = { 0xffffffff };
		pContext->ClearUnorderedAccessViewUint( pABuffer->getUnorderedAccessViews( )[ 0 ].get( ), clearValueUint );
		pContext->ClearUnorderedAccessViewUint( pABuffer->getUnorderedAccessViews( )[ 1 ].get( ), clearValueUint );

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );

		pContext->RSSetState( pRasterizerState.get( ) );

		static const float blend[ ] = { 1.0f, 1.0f, 1.0f, 1.0f };
		pContext->OMSetBlendState( pBlendState.get( ), blend, 0xffffffff );
		pContext->OMSetDepthStencilState( pDepthStencilStateOn.get( ), 0 );

		// +-----------------------+
		// | A Buffer Construction |
		// +-----------------------+
		// Bind UAVs.
		ID3D11UnorderedAccessView * pUAVs[ ] =
		{
			pABuffer->getUnorderedAccessViews( )[ 0 ].get( ), // p_fragmentLinkUAV
			pABuffer->getUnorderedAccessViews( )[ 1 ].get( ), // p_startOffsetUAV
		};

		// Initialize the counter value.
		UINT anInitIndices[ ] = { 0, 0 };
		const unsigned int count = sizeof( pUAVs ) / sizeof( pUAVs[ 0 ] );

		pDepthBuffer->reset( pContext, false, true );
		ID3D11DepthStencilView * pDepthStencil = pDepthBuffer->getDepthStencilView( ).get( );
		//ID3D11DepthStencilView * pDepthStencil = nullptr;

		ID3D11RenderTargetView* differedTargets[] =
		{
			pResolveTarget->getRenderTargetViews( )[ 1 ].get( ),
			pResolveTarget->getRenderTargetViews( )[ 2 ].get( ),
			pResolveTarget->getRenderTargetViews( )[ 3 ].get( )
		};

		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 3, differedTargets, pDepthStencil, 3, count, pUAVs, anInitIndices );

		pContext->VSSetShader( pMeshVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pABufferConstructPS.get( ), nullptr, 0 );

		pSamplers->set( pContext, 0 );
		pPSLightingBuffer->set( pContext );
		pVSSceneBuffer->set( pContext );

		pContext->IASetInputLayout( pInputLayputObjMesh.get( ) );

		for( const ModelSPtr& pModel : models )
		{
			pModel->getColourMaterial( )->set( pContext );

			pPSResources->set( pContext, offset );

			pModel->getMesh( )->set( pContext );
			pModel->getMesh( )->render( pContext );
		}

		// +--------------+
		// | Resolve Pass |
		// +--------------+
		pContext->PSSetShaderResources( 0, sizeof( ppNullSRVs ) / sizeof( ppNullSRVs[ 0 ] ), ppNullSRVs );
		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, 0, 0, nullptr, nullptr );

		ID3D11ShaderResourceView * ppSRVs[ ] =
		{
			pABuffer->getShaderResourceViews( )[ 0 ].get( ), // p_fragmentLinkSRV
			pABuffer->getShaderResourceViews( )[ 1 ].get( ), // p_startOffsetSRV
			pBackBuffer.get( ),
			pResolveTarget->getShaderResourceViews( )[ 1 ].get( ),
			nullptr, nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr, nullptr, nullptr
		};

		pContext->PSSetShaderResources( 0, sizeof( ppSRVs ) / sizeof( ppSRVs[ 0 ] ), ppSRVs );

		ID3D11RenderTargetView* resolveTarget[] = { pResolveTarget->getRenderTargetViews( )[ 0 ].get( ) };

		pContext->OMSetDepthStencilState( pDepthStencilStateOff.get( ), 0 );
		//pResolveTarget->set( pContext ); // Set with out the depth buffer?
		pContext->OMSetRenderTargets( 1, resolveTarget, nullptr );

		pABufferResolvePSBuffer->set( pContext );

		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pABufferResolvePS.get( ), nullptr, 0 );

		pContext->IASetVertexBuffers( 0, 0, nullptr, nullptr, nullptr );
		pContext->IASetIndexBuffer( nullptr, DXGI_FORMAT_UNKNOWN, 0 );
		pContext->IASetInputLayout( nullptr );
		pContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
		pContext->Draw( 3, 0 );

		pContext->PSSetShaderResources( 0, sizeof( ppNullSRVs ) / sizeof( ppNullSRVs[ 0 ] ), ppNullSRVs );
	}
} // namespace directX11
} // namespace visual
