#include "ToneMappingPass.h"
// Tone Mapping Sub Pass.
//
//
// Project   : NaKama-Fx
// File Name : ToneMappingPass.cpp
// Date      : 05/08/2019
// Author    : Nicholas Welters

#include "../Targets/OutputTarget.h"
#include "../Materials/ToneMappingMaterialBuffer.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	using std::min;
	using std::max;
	using std::abs;
	using std::vector;

	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pRasterizerState"> Primative to Fragment Settings. </param>
	/// <param name="pBlendState"> Output Merget Blend Settings. </param>
	/// <param name="pDepthStencilState"> Output Merget Depth Stencil Settings. </param>
	/// <param name="pOutputTarget"> The scene render target. </param>
	/// <param name="pFullScreenVS"> Bufferless fullscreen vertex shader. </param>
	/// <param name="pToneMappingShader"> Pixel shader. </param>
	/// <param name="pToneMappingShaderData"> Pixel shader data. </param>
	/// <param name="pSamplers"> Pixel shader samplers. </param>
	ToneMappingPass::ToneMappingPass( const DeviceContextPtr_t&               pContext,
									  const ID3D11RasterizerStateSPtr&        pRasterizerState,
									  const ID3D11BlendStateSPtr&             pBlendState,
									  const ID3D11DepthStencilStateSPtr&      pDepthStencilState,
									  const OutputTargetSPtr&                 pOutputTarget,
									  const ID3D11VertexShaderSPtr&           pFullScreenVS,
									  const ID3D11PixelShaderSPtr&            pToneMappingShader,
									  const ToneMappingPixelShaderBufferSPtr& pToneMappingPixelShaderData,
									  const PSSamplersSPtr&                   pSamplers )
		: ComputePass( pContext )
		, pContext( pContext )
		, pRasterizerState( pRasterizerState )
		, pBlendState( pBlendState )
		, pDepthStencilState( pDepthStencilState )
		, pOutputTarget( pOutputTarget )
		, pFullScreenVS( pFullScreenVS )
		, pToneMappingShader( pToneMappingShader )
		, pToneMappingPixelShaderData( pToneMappingPixelShaderData )
		, pSamplers( pSamplers )
	{
		pToneMappingPixelShaderData->setSize( { pOutputTarget->getWidth( ), pOutputTarget->getHeight( ) } );
	}

	/// <summary> Change the render target of the rendering pass. </summary>
	/// <param name="pTarget"> A new output target for the rendering pass. </param>
	void ToneMappingPass::setOutputTarget( const OutputTargetSPtr& pTarget )
	{
		pOutputTarget = pTarget;
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	/// <param name="delta"> The time step. </param>
	void ToneMappingPass::update( float delta )
	{
		pToneMappingPixelShaderData->update( pContext );
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to perform a compute pass.
	/// </summary>
	void ToneMappingPass::compute( )
	{
		// constants
		static const float blend[ ] = { 1.0f, 1.0f, 1.0f, 1.0f };

		// +--------------------+
		// | SSR & Down Sample |
		// +--------------------+
		// Set
		// - View Port
		// - Scissor Rectangle
		D3D11_RECT scissor;
		D3D11_VIEWPORT viewPort;

		scissor.left   = 0;
		scissor.top    = 0;
		scissor.right  = pOutputTarget->getWidth( );
		scissor.bottom = pOutputTarget->getHeight( );

		viewPort.Width  = static_cast< float >( scissor.right  );
		viewPort.Height = static_cast< float >( scissor.bottom );
		viewPort.TopLeftY = 0;
		viewPort.TopLeftX = 0;
		viewPort.MinDepth = 0;
		viewPort.MaxDepth = 1;

		pContext->RSSetScissorRects( 1, &scissor );
		pContext->RSSetViewports( 1, &viewPort );

		// Set
		// - Render Target
		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, 0, 0, nullptr, nullptr );
		pOutputTarget->set( pContext );
		pOutputTarget->reset( pContext, true, false );

		// Set
		// - Rasterizer
		// - Blend
		// - Depth Stencil
		pContext->RSSetState( pRasterizerState.get( ) );

		pContext->OMSetBlendState( pBlendState.get( ), blend, 0xffffffff );
		pContext->OMSetDepthStencilState( pDepthStencilState.get( ), 0 );

		// Set
		// - Vertex Shader
		// - Pixel Shader
		pContext->VSSetShader( pFullScreenVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pToneMappingShader.get( ), nullptr, 0 );

		// Set
		// - Samplers
		// - Textures & Constant Buffers
		pToneMappingPixelShaderData->set( pContext );
		pSamplers->set( pContext, 0 );

		// Set
		// - NULL Vertex Buffer
		// - Draw FullScreen Triangle
		pContext->IASetVertexBuffers( 0, 0, nullptr, nullptr, nullptr );
		pContext->IASetIndexBuffer( nullptr, DXGI_FORMAT_UNKNOWN, 0 );
		pContext->IASetInputLayout( nullptr );
		pContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		pContext->Draw( 3, 0 );

		ID3D11ShaderResourceView* clearTextures[] =
		{
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr
		};
		pContext->PSSetShaderResources( 0, 16, clearTextures );
	}
} // namespace directX11
} // namespace visual
