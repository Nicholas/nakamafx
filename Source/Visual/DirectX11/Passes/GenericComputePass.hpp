
// A generic compute pass that we can use to build simple pipelines.
//
// Project   : NaKama-Fx
// File Name : GenericComputePass.hpp
// Date      : 04/12/2018
// Author    : Nicholas Welters

#ifndef _GENERIC_COMPUTE_PASS_DX11_HPP
#define _GENERIC_COMPUTE_PASS_DX11_HPP

#include "GenericComputePass.h"
#include "../Targets/UATarget.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	using std::vector;
	using glm::ivec3;

	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pComputeShader"> The compute shader that will be run. </param>
	/// <param name="pUAVs"> The unordered acces views the compute shader can write to. </param>
	/// <param name="samplers"> Samplers used in the compute pass. </param>
	/// <param name="pSRVs"> Shader resources used in the compute pass. </param>
	/// <param name="offset"> Shader resources offset. </param>
	/// <param name="pConstantBuffer"> Compute shader generic constant buffer. </param>
	/// <param name="dispatch"> Compute shader dispatch sizes. </param>
	template< typename ComputeConstantBuffer >
	GenericComputePass< ComputeConstantBuffer >::GenericComputePass( const DeviceContextPtr_t&                     pContext,
																	 const ID3D11ComputeShaderSPtr&                pComputeShader,
																	 const UATargetSPtr&                           pUAVs,
																	 const vector< ID3D11SamplerStateSPtr >&       samplers,
																	 const vector< ID3D11ShaderResourceViewSPtr >& pSRVs,
																	 const uint32_t                                offset,
																	 const ComputeConstantBufferSPtr&              pConstantBuffer,
																	 const ivec3&                                  dispatch )
		: ComputePass( pContext )
		, pContext( pContext )
		, pComputeShader( pComputeShader )
		, pUAVs( pUAVs )
		, pAccessViews( )
		, pClearUAVs( )
		, initIndexes( )
		, samplers( samplers )
		, pSamplers( )
		, pClearSamplers( )
		, pSRVs( pSRVs )
		, pShaderResources( )
		, pClearSRVs( )
		, offset( offset )
		, pConstantBuffer( pConstantBuffer )
		, dispatch( dispatch )
	{
		for( const ID3D11UnorderedAccessViewSPtr& pUAV : pUAVs->getUnorderedAccessViews( ) )
		{
			pAccessViews.push_back( pUAV.get( ) );
			initIndexes.push_back( 0 );
			pClearUAVs.push_back( nullptr );
		}
		for( const ID3D11SamplerStateSPtr& pSampler : samplers )
		{
			pSamplers.push_back( pSampler.get( ) );
			pClearSamplers.push_back( nullptr );
		}
		for( const ID3D11ShaderResourceViewSPtr& pSRV : pSRVs )
		{
			pShaderResources.push_back( pSRV.get( ) );
			pClearSRVs.push_back( nullptr );
		}

		this->dispatch.x = std::max( this->dispatch.x, 1 );
		this->dispatch.y = std::max( this->dispatch.y, 1 );
		this->dispatch.z = std::max( this->dispatch.z, 1 );
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	template< typename ComputeConstantBuffer >
	void GenericComputePass< ComputeConstantBuffer >::update( float delta )
	{
		pConstantBuffer->update( pContext );
	}

	/// <summary>
	///	Get the compute shader pass' constants buffer so that we can update them.
	/// </summary>
	template< typename ComputeConstantBuffer >
	typename GenericComputePass< ComputeConstantBuffer >::ComputeConstantBufferSPtr GenericComputePass< ComputeConstantBuffer >::getConstants( )
	{
		return pConstantBuffer;
	}

	/// <summary>
	///	Change an SRV in the compute pass.
	/// </summary>
	template< typename ComputeConstantBuffer >
	void GenericComputePass<ComputeConstantBuffer>::setSRV( const uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV )
	{
		if( index < pSRVs.size( ) )
		{
			pSRVs[ index ] = pSRV;
			pShaderResources[ index ] = pSRV.get( );
		}
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to perform a compute pass.
	/// </summary>
	template< typename ComputeConstantBuffer >
	void GenericComputePass< ComputeConstantBuffer >::compute( )
	{
		// Clear the start offset buffer by magic value.
		static const UINT clearValue[ 1 ] = { 0x0 }; // { 0xffffffff };

		for( ID3D11UnorderedAccessView* pUAV : pAccessViews )
		{
			pContext->ClearUnorderedAccessViewUint( pUAV, clearValue );
		}

		// Set
		pContext->CSSetShader( pComputeShader.get( ), nullptr, 0 );
		pContext->CSSetUnorderedAccessViews( 0, static_cast< UINT >( pAccessViews.size( ) ), pAccessViews.data( ), initIndexes.data( ) );

		if( !pShaderResources.empty( ) )
		{
			pContext->CSSetShaderResources( offset, static_cast< UINT >( pShaderResources.size( ) ), pShaderResources.data( ) );
		}
		if( !pSamplers.empty( ) )
		{
			pContext->CSSetSamplers( 0, static_cast< UINT >( pSamplers.size( ) ), pSamplers.data( ) );
		}

		pConstantBuffer->set( pContext );

		pContext->Dispatch( dispatch.x, dispatch.y, dispatch.z );

		// Clear
		pContext->CSSetShader( nullptr, nullptr, 0 );
		pContext->CSSetUnorderedAccessViews( 0, static_cast< UINT >( pClearUAVs.size( ) ), pClearUAVs.data( ), nullptr );


		if( !pShaderResources.empty( ) )
		{
			pContext->CSSetShaderResources( offset, static_cast< UINT >( pClearSRVs.size( ) ), pClearSRVs.data( ) );
		}
		if( !pSamplers.empty( ) )
		{
			pContext->CSSetSamplers( 0, static_cast< UINT >( pClearSamplers.size( ) ), pClearSamplers.data( ) );
		}
	}
} // namespace directX11
} // namespace visual

#endif // _GENERIC_COMPUTE_PASS_DX11_HPP
