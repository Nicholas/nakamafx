#pragma once
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// Project   : NaKama-Fx
// File Name : ColourMeshSubPass.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _COLOUR_MESH_SUB_PASS_DX11_H
#define _COLOUR_MESH_SUB_PASS_DX11_H

#include "MeshSubPass.h"
#include "../APITypeDefs.h"
#include "../Types/ShaderResources.h"
#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Pixel Shaded Mesh Rendering Subpass </summary>
	class ColourMeshSubPass : public MeshSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPixelShader"> The sub pass pixel shader. </param>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
		/// <param name="pPSLightingBuffer"> The sub pass pixel shader scene constants. </param>
		/// <param name="pPSSamplers"> The sub pass pixel shader texture samplers. </param>
		/// <param name="pPSResources"> The sub pass pixel shader resources. </param>
		/// <param name="offset"> The sub pass pixel shader resource start offset. </param>
		ColourMeshSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
						   const ID3D11VertexShaderSPtr&      pVertexShader,
						   const ID3D11InputLayoutSPtr&       pInputLayput,
						   const ID3D11RasterizerStateSPtr&   pRasterizerState,
						   const ID3D11DepthStencilStateSPtr& pDepthStencilState,
						   const ID3D11BlendStateSPtr&        pBlendState,
						   const SceneVSBufferSPtr&           pVSSceneBuffer,
						   const LightingPSBufferSPtr&        pPSLightingBuffer,
						   const PSSamplersSPtr&              pPSSamplers,
						   const PSResourcesSPtr&             pPSResources,
						   uint32_t                           offset );

		/// <summary> ctor </summary>
		ColourMeshSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ColourMeshSubPass( const ColourMeshSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ColourMeshSubPass( ColourMeshSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~ColourMeshSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ColourMeshSubPass& operator=( const ColourMeshSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ColourMeshSubPass& operator=( ColourMeshSubPass&& move ) = default;


		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

		/// <summary> Change a subpass resource. </summary>
		/// <param name="index"> The index of the shader resource view to update. </param>
		/// <param name="pSRV"> The new shader resource view to use in the pass. </param>
		void setSRV( uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV ) const;

	protected:
		/// <summary> Set the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to set the subpass constants. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Unset the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to unset the subpass constants. </param>
		virtual void unset( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Render a model. </summary>
		/// <param name="pContext"> The device context used to render the model. </param>
		/// <param name="pModel"> The model to render. </param>
		virtual void render( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel ) override;

		/// <summary> Update a model. </summary>
		/// <param name="pContext"> The device context used to update the model. </param>
		/// <param name="pModel"> The model to update. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel ) override;

	private:
		/// <summary> The per scene vertex shader constants. </summary>
		SceneVSBufferSPtr pVSSceneBuffer = nullptr;

		/// <summary> The subpass pixel shader. </summary>
		ID3D11PixelShaderSPtr pPixelShader = nullptr;

		/// <summary> The per scene vertex shader constants. </summary>
		LightingPSBufferSPtr pPSSceneBuffer = nullptr;

		/// <summary> The per scene pixel shader samplers. </summary>
		PSSamplersSPtr pPSSamplers = nullptr;

		/// <summary> The per scene pixel shader sampler pointers. </summary>
		std::vector< ID3D11SamplerState* > pClearSamplers = { };

		/// <summary> The per scene pixel shader resources. </summary>
		PSResourcesSPtr pPSResources = nullptr;

		/// <summary> The per scene pixel shader resources offset. </summary>
		uint32_t offset = 0;

		/// <summary> A buffer to clear the resourece at the end of the subpass. </summary>
		std::vector< ID3D11ShaderResourceView* > pClearResources = { };
	};
} // namespace directX11
} // namespace visual

#endif // _COLOUR_MESH_SUB_PASS_DX11_H

