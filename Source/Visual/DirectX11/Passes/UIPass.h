#pragma once
// User Interface Sub Pass.
//
//
// Project   : NaKama-Fx
// File Name : UIPass.h
// Date      : 05/08/2019
// Author    : Nicholas Welters


#ifndef _UI_PASS_DX11_H
#define _UI_PASS_DX11_H

#include "ComputePass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace ui
{
	FORWARD_DECLARE( SystemUI );
}
namespace directX11
{
	FORWARD_DECLARE( OutputTarget );


	/// <summary> User interface Pass. </summary>
	class UIPass : public ComputePass
	{
	public:
		struct Data
		{
			ArrayBuffer index_buffer;
			ArrayBuffer vertex_buffer;
			ID3D11ShaderResourceViewSPtr font_texture_view;
		};

		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pRasterizerState"> Primative to Fragment Settings. </param>
		/// <param name="pBlendState"> Output Merget Blend Settings. </param>
		/// <param name="pDepthStencilState"> Output Merget Depth Stencil Settings. </param>
		/// <param name="pOutputTarget"> The scene render target. </param>
		/// <param name="pInputLayout"> The vertex shader input assembler state. </param>
		/// <param name="pUIVS"> Vertex shader. </param>
		/// <param name="pUIPS"> Pixel shader. </param>
		/// <param name="pUIVSData"> Vertex shader data. </param>
		/// <param name="pUIPSData"> Pixel shader data. </param>
		/// <param name="pSamplers"> Pixel shader samplers. </param>
		/// <param name="pUI"> User Interface. </param>
		/// <param name="uiData"> User Interface GPU Data. </param>
		UIPass( const DeviceContextPtr_t&          pContext,
				const ID3D11RasterizerStateSPtr&   pRasterizerState,
				const ID3D11BlendStateSPtr&        pBlendState,
				const ID3D11DepthStencilStateSPtr& pDepthStencilState,
				const OutputTargetSPtr&            pOutputTarget,
				const ID3D11InputLayoutSPtr&       pInputLayout,
				const ID3D11VertexShaderSPtr&      pUIVS,
				const ID3D11PixelShaderSPtr&       pUIPS,
				const UIVSBufferSPtr&              pUIVSData,
//				const UIPixelShaderBufferSPtr&     pUIPSData,
				const PSSamplersSPtr&              pSamplers,
				const ui::SystemUISPtr&            pUI,
				const Data&                        uiData );

		/// <summary> ctor </summary>
		UIPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		UIPass( const UIPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		UIPass( UIPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~UIPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		UIPass& operator=( const UIPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		UIPass& operator=( UIPass&& move ) = delete;


		/// <summary> Change the render target of the rendering pass. </summary>
		/// <param name="pTarget"> A new output target for the rendering pass. </param>
		void setOutputTarget( const OutputTargetSPtr& pTarget );

		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The time step. </param>
		virtual void update( float delta ) override;

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		virtual void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> Primative to Fragment Settings. </summary>
		ID3D11RasterizerStateSPtr pRasterizerState;

		/// <summary> Output Merget Blend Settings. </summary>
		ID3D11BlendStateSPtr pBlendState;

		/// <summary> Output Merget Depth Stencil Settings. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilState;

		/// <summary> The scene render target. </summary>
		OutputTargetSPtr pOutputTarget;

		/// <summary> The vertex shader input assembler state. </summary>
		ID3D11InputLayoutSPtr pInputLayout;

		/// <summary> UI vertex shader. </summary>
		ID3D11VertexShaderSPtr pUIVS;

		/// <summary> UI pixel shader. </summary>
		ID3D11PixelShaderSPtr pUIPS;

		/// <summary> Vertex shader data. </summary>
		UIVSBufferSPtr pUIVSData;

		/// <summary> Pixel shader data. </summary>
//		UIPixelShaderBufferSPtr pUIPSData;

		/// <summary> Pixel shader samplers. </summary>
		PSSamplersSPtr pSamplers;

		/// <summary> User Interface. </summary>
		ui::SystemUISPtr pUI;

		/// <summary> User Interface GPU Data. </summary>
		Data uiData;
	};
} // namespace directX11
} // namespace visual

#endif // _UI_PASS_DX11_H
