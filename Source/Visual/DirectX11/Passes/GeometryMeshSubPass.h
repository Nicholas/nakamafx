#pragma once
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// This makes use of a geometry shader to modify the primitives.
//
// Project   : NaKama-Fx
// File Name : GeometryMeshSubPass.h
// Date      : 11/06/2017
// Author    : Nicholas Welters

#ifndef _GEOMETRY_MESH_SUB_PASS_DX11_H
#define _GEOMETRY_MESH_SUB_PASS_DX11_H

#include "ColourMeshSubPass.h"
#include "../APITypeDefs.h"
#include "../Types/ShaderResources.h"
#include "../Types/Samplers.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Pixel Shaded Mesh Rendering Subpass with a geometry shader to manipulate the topography. </summary>
	class GeometryMeshSubPass : public ColourMeshSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPixelShader"> The sub pass pixel shader. </param>
		/// <param name="pGeometryShader"> The sub pass geometry shader. </param>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
		/// <param name="pPSLightingBuffer"> The sub pass pixel shader scene constants. </param>
		/// <param name="pGSSceneBuffer"> The sub pass geomertry shader scene constants. </param>
		/// <param name="pPSSamplers"> The sub pass pixel shader texture samplers. </param>
		/// <param name="pPSResources"> The sub pass pixel shader resources. </param>
		/// <param name="offset"> The sub pass pixel shader resource start offset. </param>
		GeometryMeshSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
							 const ID3D11GeometryShaderSPtr&    pGeometryShader,
							 const ID3D11VertexShaderSPtr&      pVertexShader,
							 const ID3D11InputLayoutSPtr&       pInputLayput,
							 const ID3D11RasterizerStateSPtr&   pRasterizerState,
							 const ID3D11DepthStencilStateSPtr& pDepthStencilState,
							 const ID3D11BlendStateSPtr&        pBlendState,
							 const SceneVSBufferSPtr&           pVSSceneBuffer,
							 const LightingPSBufferSPtr&        pPSLightingBuffer,
							 const SceneGSBufferSPtr&           pGSSceneBuffer,
							 const PSSamplersSPtr&              pPSSamplers,
							 const PSResourcesSPtr&             pPSResources,
							 uint32_t                           offset );

		/// <summary> ctor </summary>
		GeometryMeshSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		GeometryMeshSubPass( const GeometryMeshSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		GeometryMeshSubPass( GeometryMeshSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~GeometryMeshSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		GeometryMeshSubPass& operator=( const GeometryMeshSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		GeometryMeshSubPass& operator=( GeometryMeshSubPass&& move ) = default;


		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

	protected:
		/// <summary> Set the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to set the subpass constants. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Unset the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to unset the subpass constants. </param>
		virtual void unset( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The subpass geometry shader. </summary>
		ID3D11GeometryShaderSPtr pGeometryShader;

		/// <summary> The subpass geometry shader constants. </summary>
		SceneGSBufferSPtr pGSSceneBuffer;
	};
} // namespace directX11
} // namespace visual

#endif // _GEOMETRY_MESH_SUB_PASS_DX11_H

