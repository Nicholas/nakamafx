#include "ComputeHistogramPass.h"
// Construct a histogram and response curve for our HDR rendering.
//
// This constructs mutiple histograms by subdeviding the frame buffer into smaller segments.
// The histograms are then accumulated to build a final histogram.
// The final histogram is used to build a response curve so that we can remap our
// HDR render target to LDR colour space.
//
// Project   : NaKama-Fx
// File Name : ComputeHistogramPass.h
// Date      : 27/11/2018
// Author    : Nicholas Welters

//#include "Output\UATarget.h"
#include "Visual/DirectX11/Targets/UATarget.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	using std::ceilf;

	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pTileHistogram">  Build a histogram per tile. </param>
	/// <param name="pAccumulateHistogram">  Merge the histograms to build a single histogram </param>
	/// <param name="pHistogramResponseCurve">  Create a response curve to be used during tone mapping. </param>
	/// <param name="pHistogramConstants"> The histogram compute shader constants. </param>
	/// <param name="pFrameBuffer"> The frame buffer compute shader resource. </param>
	/// <param name="pTileHistograms"> Histogram per tile buffer. </param>
	/// <param name="pHistogram"> The histogram buffer. </param>
	/// <param name="pResponseCurve"> The histogram response curve buffer. </param>
	ComputeHistogramPass::ComputeHistogramPass( const DeviceContextPtr_t&      pContext,
												const ID3D11ComputeShaderSPtr& pTileHistogram,
												const ID3D11ComputeShaderSPtr& pAccumulateHistogram,
												const ID3D11ComputeShaderSPtr& pHistogramResponseCurve,
												const HistogramCSBufferSPtr&   pHistogramConstants,
												const CSResourcesSPtr&         pFrameBuffer,
												const UATargetSPtr&            pTileHistograms,
												const UATargetSPtr&            pHistogram,
												const UATargetSPtr&            pResponseCurve )
		: ComputePass( pContext )
		, pContext( pContext )
		, pTileHistogram( pTileHistogram )
		, pAccumulateHistogram( pAccumulateHistogram )
		, pHistogramResponseCurve( pHistogramResponseCurve )
		, pHistogramConstants( pHistogramConstants )
		, pFrameBuffer( pFrameBuffer )
		, pTileHistograms( pTileHistograms )
		, pHistogram( pHistogram )
		, pResponseCurve( pResponseCurve )
	{ }

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	/// <param name="delta"> The frame step size. </param>
	void ComputeHistogramPass::update( float delta )
	{
		pHistogramConstants->update( pContext );
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to perform a compute pass.
	/// </summary>
	void ComputeHistogramPass::compute( )
	{
		UINT initialCounts[ ] = { 0 };

		ID3D11UnorderedAccessView* nullUAV[ ] = { nullptr };
		ID3D11ShaderResourceView*  nullSRV[ ] = { nullptr };

		ID3D11UnorderedAccessView* uav[ ] = { nullptr };
		ID3D11ShaderResourceView*  srv[ ] = { nullptr };

		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, 0, 0, nullptr, nullptr );

		pHistogramConstants->set( pContext );

		///////////////////////////////////////////////////////////////////////////////////////
		// Tile Histogram Construction
		///////////////////////////////////////////////////////////////////////////////////////
		pFrameBuffer->set( pContext, 0 );

		uav[ 0 ] = pTileHistograms->getUnorderedAccessViews( )[ 0 ].get( );
		pContext->CSSetUnorderedAccessViews( 0, 1, uav, initialCounts );

		pContext->CSSetShader( pTileHistogram.get( ), nullptr, 0 );

		// TODO: Remove the magic numbers for the tile sizes.
		const float tilesX = ceilf( pHistogramConstants->get( ).outputWidth  / 32.0f );
		const float tilesY = ceilf( pHistogramConstants->get( ).outputHeight / 16.0f );

		unsigned int theadGroupX = static_cast< unsigned int >( tilesX );
		unsigned int theadGroupY = static_cast< unsigned int >( tilesY );
		unsigned int theadGroupZ = 1;

		pContext->Dispatch( theadGroupX, theadGroupY, theadGroupZ );
		///////////////////////////////////////////////////////////////////////////////////////

		pContext->CSSetShaderResources( 0, 1, nullSRV );
		pContext->CSSetUnorderedAccessViews( 0, 1, nullUAV, initialCounts );


		///////////////////////////////////////////////////////////////////////////////////////
		// Histogram Accumulation
		///////////////////////////////////////////////////////////////////////////////////////
		srv[ 0 ] = pTileHistograms->getShaderResourceViews( )[ 0 ].get( );
		pContext->CSSetShaderResources( 0, 1, srv );

		uav[ 0 ] = pHistogram->getUnorderedAccessViews( )[ 0 ].get( );
		pContext->CSSetUnorderedAccessViews( 0, 1, uav, initialCounts );

		pContext->CSSetShader( pAccumulateHistogram.get( ), nullptr, 0 );

		theadGroupX = 64;
		theadGroupY = 1;
		theadGroupZ = 1;

		pContext->Dispatch( theadGroupX, theadGroupY, theadGroupZ );
		///////////////////////////////////////////////////////////////////////////////////////

		pContext->CSSetShaderResources( 0, 1, nullSRV );
		pContext->CSSetUnorderedAccessViews( 0, 1, nullUAV, initialCounts );


		///////////////////////////////////////////////////////////////////////////////////////
		// Tone Mapping Response Curve
		///////////////////////////////////////////////////////////////////////////////////////

		srv[ 0 ] = pHistogram->getShaderResourceViews( )[ 0 ].get( );
		pContext->CSSetShaderResources( 0, 1, srv );

		initialCounts[ 0 ] = -1;
		uav[ 0 ] = pResponseCurve->getUnorderedAccessViews( )[ 0 ].get( );
		pContext->CSSetUnorderedAccessViews( 0, 1, uav, nullptr );

		pContext->CSSetShader( pHistogramResponseCurve.get( ), nullptr, 0 );

		theadGroupX = 1;
		theadGroupY = 1;
		theadGroupZ = 1;

		pContext->Dispatch( theadGroupX, theadGroupY, theadGroupZ );
		///////////////////////////////////////////////////////////////////////////////////////

		pContext->CSSetShaderResources( 0, 1, nullSRV );
		pContext->CSSetUnorderedAccessViews( 0, 1, nullUAV, initialCounts );
		pContext->CSSetShader( nullptr, nullptr, 0 );
	}
} // namespace directX11
} // namespace visual
