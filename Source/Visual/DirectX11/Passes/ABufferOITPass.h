#pragma once
// An order independent transparency rendering pass.
//
// This construct an A-Buffer (fragment linked list per pixel) to render transparent and translucent
// models with out having to do CPU side work to make sure that the final fragment order is correct.
//
// Based on AMD's realtime concurrent linked list construction on the GPU.
//
// Project   : NaKama-Fx
// File Name : ABufferOITPass.h
// Date      : 04/12/2018
// Author    : Nicholas Welters

#ifndef _A_BUFFER_OIT_PASS_DX11_H
#define _A_BUFFER_OIT_PASS_DX11_H

#include "ComputePass.h"
#include "../APITypeDefs.h"
#include "../Materials/MaterialBuffer.h"
#include "../Types/Samplers.h"

#include <memory>
#include <list>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( MultipleRenderTarget );
	FORWARD_DECLARE( DepthStencilTarget );
	FORWARD_DECLARE( UATarget );
	FORWARD_DECLARE( Model );

	/// <summary> An order independent transparency rendering pass. </summary>
	class ABufferOITPass : public ComputePass
	{
	public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pABuffer"> The unordered access views used to build our linked list. </param>
		/// <param name="pResolveTarget"> The render targets taht we are going to output the final result to. </param>
		/// <param name="pDepthBuffer"> The A buffers depths buffer when out putting our differed info. </param>
		/// <param name="pRasterizerState"> Rasterizer state. </param>
		/// <param name="pBlendState"> Blend State for the final composition pass on top of the output targets. </param>
		/// <param name="pDepthStencilStateOn"> The depth stencil state on. </param>
		/// <param name="pDepthStencilStateOff"> The depth stencil state off. </param>
		/// <param name="pInputLayputObjMesh"> The mesh vertex input layout. </param>
		/// <param name="pSamplers"> The per scene pixel shader samplers. </param>
		/// <param name="pPSResources"> The per scene pixel shader resources. </param>
		/// <param name="offset"> The per scene pixel shader resources offset. </param>
		/// <param name="pBackBuffer"> The image that we are going to render the scene on top of. </param>
		/// <param name="pVSSceneBuffer"> The scene vertex shader constants. </param>
		/// <param name="pPSLightingBuffer"> The scene lights. </param>
		/// <param name="pMeshVS"> The mesh vertex shader. </param>
		/// <param name="pABufferConstructPS"> The A-buffer construction pixel shader. </param>
		/// <param name="pABufferResolvePSBuffer"> The A-buffer resolve constants. </param>
		/// <param name="pFullScreenVS"> A bufferless fullscreen verex shader. </param>
		/// <param name="pABufferResolvePS"> The A-buffer resolve pixel shader. </param>
		ABufferOITPass( const DeviceContextPtr_t&           pContext,
						const UATargetSPtr&                 pABuffer,
						const MultipleRenderTargetSPtr&     pResolveTarget,
						const DepthStencilTargetSPtr&       pDepthBuffer,
						const ID3D11RasterizerStateSPtr&    pRasterizerState,
						const ID3D11BlendStateSPtr&         pBlendState,
						const ID3D11DepthStencilStateSPtr&  pDepthStencilStateOn,
						const ID3D11DepthStencilStateSPtr&  pDepthStencilStateOff,
						const ID3D11InputLayoutSPtr&        pInputLayputObjMesh,
						const PSSamplersSPtr&               pSamplers,
						const PSResourcesSPtr&              pPSResources,
						uint32_t                            offset,
						const ID3D11ShaderResourceViewSPtr& pBackBuffer,
						const SceneVSBufferSPtr&            pVSSceneBuffer,
						const LightingPSBufferSPtr&         pPSLightingBuffer,
						const ID3D11VertexShaderSPtr&       pMeshVS,
						const ID3D11PixelShaderSPtr&        pABufferConstructPS,
						const ABufferResolvePSBufferSPtr&   pABufferResolvePSBuffer,
						const ID3D11VertexShaderSPtr&       pFullScreenVS,
						const ID3D11PixelShaderSPtr&        pABufferResolvePS );

		/// <summary> ctor </summary>
		ABufferOITPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ABufferOITPass( const ABufferOITPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ABufferOITPass( ABufferOITPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~ABufferOITPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ABufferOITPass& operator=( const ABufferOITPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ABufferOITPass& operator=( ABufferOITPass&& move ) = delete;


		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The frame step size. </param>
		virtual void update( float delta ) override;


		/// <summary> Add a mesh to be drawn during the OIT pass. </summary>
		/// <param name="pModel"> The model to render in the subpass. </param>
		void add( const ModelSPtr& pModel );

		/// <summary> Remove a mesh to be drawn during the OIT pass. </summary>
		/// <param name="pModel"> The model to remove from the subpass. </param>
		void remove( const ModelSPtr& pModel );

		/// <summary> Change a resource. </summary>
		/// <param name="index"> The index of the shader resource view to update. </param>
		/// <param name="pSRV"> The new shader resource view to use in the pass. </param>
		void setSRV( uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV ) const;

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		virtual void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> The unordered access views used to build our linked list. </summary>
		UATargetSPtr pABuffer;

		/// <summary> The render targets taht we are going to output the final result to. </summary>
		MultipleRenderTargetSPtr pResolveTarget;

		/// <summary> The A buffers depths buffer when out putting our differed info. </summary>
		DepthStencilTargetSPtr pDepthBuffer;

		/// <summary> Rasterizer state. </summary>
		ID3D11RasterizerStateSPtr pRasterizerState;

		/// <summary> Blend State for the final composition pass on top of the output targets. </summary>
		ID3D11BlendStateSPtr pBlendState;

		/// <summary> The depth stencil state on. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilStateOn;

		/// <summary> The depth stencil state off. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilStateOff;

		/// <summary> The mesh vertex input layout. </summary>
		ID3D11InputLayoutSPtr pInputLayputObjMesh;

		/// <summary> The per scene pixel shader samplers. </summary>
		PSSamplersSPtr pSamplers;

		/// <summary> The per scene pixel shader resources. </summary>
		PSResourcesSPtr pPSResources;

		/// <summary> The per scene pixel shader resources offset. </summary>
		uint32_t offset;

		/// <summary> The image that we are going to render the scene on top of. </summary>
		ID3D11ShaderResourceViewSPtr pBackBuffer;

		/// <summary> The scene vertex shader constants. </summary>
		SceneVSBufferSPtr pVSSceneBuffer;

		/// <summary> The scene lights. </summary>
		LightingPSBufferSPtr pPSLightingBuffer;

		/// <summary> The mesh vertex shader. </summary>
		ID3D11VertexShaderSPtr pMeshVS;

		/// <summary> The A-buffer construction pixel shader. </summary>
		ID3D11PixelShaderSPtr  pABufferConstructPS;

		/// <summary> The A-buffer resolve constants. </summary>
		ABufferResolvePSBufferSPtr pABufferResolvePSBuffer;

		/// <summary> A bufferless fullscreen verex shader. </summary>
		ID3D11VertexShaderSPtr pFullScreenVS;

		/// <summary> The A-buffer resolve pixel shader. </summary>
		ID3D11PixelShaderSPtr  pABufferResolvePS;

		/// <summary> The scissor test rectangle. </summary>
		D3D11_RECT scissor;

		/// <summary> The view post volume. </summary>
		D3D11_VIEWPORT viewPort;

		/// <summary> The models rendered in this subpass. </summary>
		std::list< ModelSPtr > models;
	};
} // namespace directX11
} // namespace visual

#endif // _A_BUFFER_OIT_PASS_DX11_H
