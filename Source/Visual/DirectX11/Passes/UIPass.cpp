#include "UIPass.h"
// User Interface Sub Pass.
//
//
// Project   : NaKama-Fx
// File Name : UIPass.cpp
// Date      : 05/08/2019
// Author    : Nicholas Welters

#include "../Targets/MultipleRenderTarget.h"
#include "../Materials/ToneMappingMaterialBuffer.h"

#include "Visual/FX/Vertex.h"

#include "Visual/UserInterface/SystemUI.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	using std::min;
	using std::max;
	using std::abs;
	using std::vector;

	using glm::vec4;

	using visual::fx::Vertex3f2f4f;

	/// <summary> ctor. </summary>
	/// <param name="pContext"> The D3D11 device context. </param>
	/// <param name="pRasterizerState"> Primative to Fragment Settings. </param>
	/// <param name="pBlendState"> Output Merget Blend Settings. </param>
	/// <param name="pDepthStencilState"> Output Merget Depth Stencil Settings. </param>
	/// <param name="pOutputTarget"> The scene render target. </param>
	/// <param name="pInputLayout"> The vertex shader input assembler state. </param>
	/// <param name="pUIVS"> Vertex shader. </param>
	/// <param name="pUIPS"> Pixel shader. </param>
	/// <param name="pUIVSData"> Vertex shader data. </param>
	/// <param name="pUIPSData"> Pixel shader data. </param>
	/// <param name="pSamplers"> Pixel shader samplers. </param>
	/// <param name="pUI"> User Interface. </param>
	UIPass::UIPass( const DeviceContextPtr_t&          pContext,
					const ID3D11RasterizerStateSPtr&   pRasterizerState,
					const ID3D11BlendStateSPtr&        pBlendState,
					const ID3D11DepthStencilStateSPtr& pDepthStencilState,
					const OutputTargetSPtr&            pOutputTarget,
					const ID3D11InputLayoutSPtr&       pInputLayout,
					const ID3D11VertexShaderSPtr&      pUIVS,
					const ID3D11PixelShaderSPtr&       pUIPS,
					const UIVSBufferSPtr&              pUIVSData,
//					const UIPixelShaderBufferSPtr&     pUIPSData,
					const PSSamplersSPtr&              pSamplers,
					const ui::SystemUISPtr&            pUI,
					const Data&                        uiData )
		: ComputePass( pContext )
		, pContext( pContext )
		, pRasterizerState( pRasterizerState )
		, pBlendState( pBlendState )
		, pDepthStencilState( pDepthStencilState )
		, pOutputTarget( pOutputTarget )
		, pInputLayout( pInputLayout )
		, pUIVS( pUIVS )
		, pUIPS( pUIPS )
		, pUIVSData( pUIVSData )
//		, pUIPSData( pUIPSData )
		, pSamplers( pSamplers )
		, pUI( pUI )
		, uiData( uiData )
	{
	}

	/// <summary> Change the render target of the rendering pass. </summary>
	/// <param name="pTarget"> A new output target for the rendering pass. </param>
	void UIPass::setOutputTarget( const OutputTargetSPtr& pTarget )
	{
		pOutputTarget = pTarget;
	}

	/// <summary>
	///	This is were we will update any gpu date thats changed on the cpu.
	/// </summary>
	/// <param name="delta"> The time step. </param>
	void UIPass::update( float delta )
	{
		pUIVSData->update( pContext );
//		pUIPSData->update( pContext );

		/* Convert from command queue into draw list and draw to screen */
		/* load draw vertices & elements directly into vertex + element buffer */
		D3D11_MAPPED_SUBRESOURCE vertices;
		D3D11_MAPPED_SUBRESOURCE indices;

		ThrowIfFailed( pContext->Map( uiData.vertex_buffer.pBuffer.get( ), 0, D3D11_MAP_WRITE_DISCARD, 0, &vertices ) );
		ThrowIfFailed( pContext->Map( uiData.index_buffer.pBuffer.get( ), 0, D3D11_MAP_WRITE_DISCARD, 0, &indices ) );

		pUI->fillBuffers( (Vertex3f2f4f::Vertex*) vertices.pData, uiData.vertex_buffer.size, indices.pData, uiData.index_buffer.size );

		pContext->Unmap( uiData.vertex_buffer.pBuffer.get( ), 0 );
		pContext->Unmap( uiData.index_buffer.pBuffer.get( ), 0 );
	}

	/// <summary>
	///	This is were we will set up the state of the GPU to perform a compute pass.
	/// </summary>
	void UIPass::compute( )
	{
		const UINT stride = sizeof( Vertex3f2f4f::Vertex );
		const UINT offset = 0;
		static const float blend[ ] = { 1.0f, 1.0f, 1.0f, 1.0f };

		D3D11_VIEWPORT viewPort;

		viewPort.Width  = static_cast< float >( pOutputTarget->getWidth( ) );
		viewPort.Height = static_cast< float >( pOutputTarget->getHeight( ) );
		viewPort.TopLeftY = 0;
		viewPort.TopLeftX = 0;
		viewPort.MinDepth = 0;
		viewPort.MaxDepth = 1;

		pContext->RSSetViewports( 1, &viewPort );
		pContext->RSSetState( pRasterizerState.get( ) );

		pContext->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, 0, 0, nullptr, nullptr );
		pOutputTarget->set( pContext );
		//pOutputTarget->reset( pContext, true, false );

		pContext->OMSetBlendState( pBlendState.get( ), blend, 0xffffffff );
		pContext->OMSetDepthStencilState( pDepthStencilState.get( ), 0 );

		pContext->VSSetShader( pUIVS.get( ), nullptr, 0 );
		pContext->PSSetShader( pUIPS.get( ), nullptr, 0 );

		pUIVSData->set( pContext );
//		pUIPSData->set( pContext );
		pSamplers->set( pContext, 0 );

		ID3D11Buffer* vBuffer = uiData.vertex_buffer.pBuffer.get( );
		pContext->IASetInputLayout( pInputLayout.get( ) );
		pContext->IASetVertexBuffers( 0, 1, &vBuffer, &stride, &offset );
		pContext->IASetIndexBuffer( uiData.index_buffer.pBuffer.get( ), DXGI_FORMAT_R16_UINT, 0 );
		pContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		pUI->draw( [ this ]( void* pTexture, const glm::ivec4& scissorRect, uint32_t count, uint32_t indexOffset, uint32_t vertexOffset ) -> void
		{
			D3D11_RECT scissor;
			ID3D11ShaderResourceView* pTextureView = ( ID3D11ShaderResourceView * )pTexture;

			scissor.left   = scissorRect.x;
			scissor.right  = scissorRect.y;
			scissor.top    = scissorRect.z;
			scissor.bottom = scissorRect.w;

			pContext->PSSetShaderResources( 0, 1, &pTextureView );
			pContext->RSSetScissorRects( 1, &scissor );

			pContext->DrawIndexed( count, indexOffset, vertexOffset );
		} );

		ID3D11ShaderResourceView* clearTextures[] =
		{
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr
		};
		pContext->PSSetShaderResources( 0, 16, clearTextures );
	}
} // namespace directX11
} // namespace visual
