#pragma once

// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// Project   : NaKama-Fx
// File Name : MeshSubPass.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _MESH_SUB_PASS_DX11_H
#define _MESH_SUB_PASS_DX11_H

#include "RenderingSubPass.h"
#include "../APITypeDefs.h"

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( Model );

	/// <summary> Mesh Rendering Subpass </summary>
	class MeshSubPass : public RenderingSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		MeshSubPass( const ID3D11VertexShaderSPtr&      pVertexShader,
					 const ID3D11InputLayoutSPtr&       pInputLayput,
					 const ID3D11RasterizerStateSPtr&   pRasterizerState,
					 const ID3D11DepthStencilStateSPtr& pDepthStencilState,
					 const ID3D11BlendStateSPtr&        pBlendState );

		/// <summary> ctor </summary>
		MeshSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		MeshSubPass( const MeshSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		MeshSubPass( MeshSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~MeshSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		MeshSubPass& operator=( const MeshSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		MeshSubPass& operator=( MeshSubPass&& move ) = default;

		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

		/// <summary> Add a model to the subpass. </summary>
		/// <param name="pModel"> The model to render in the subpass. </param>
		void add(const ModelSPtr& pModel);

		/// <summary> Remove a model to the subpass. </summary>
		/// <param name="pModel"> The model to remove in the subpass. </param>
		void remove(const ModelSPtr& pModel);

	protected:
		/// <summary> Render the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to render the subpass. </param>
		virtual void renderPass( const DeviceContextPtr_t& pContext ) final override;

		/// <summary> Set the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to set the subpass constants. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) = 0;

		/// <summary> Unset the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to unset the subpass constants. </param>
		virtual void unset( const DeviceContextPtr_t& pContext );

		/// <summary> Render a model. </summary>
		/// <param name="pContext"> The device context used to render the model. </param>
		/// <param name="pModel"> The model to render. </param>
		virtual void render( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel ) = 0;

		/// <summary> Update a model. </summary>
		/// <param name="pContext"> The device context used to update the model. </param>
		/// <param name="pModel"> The model to update. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const ModelSPtr& pModel );

	private:
		/// <summary> The models rendered in this subpass. </summary>
		std::list< ModelSPtr > models;
	};
} // namespace directX11
} // namespace visual

#endif _MESH_SUB_PASS_DX11_H

