#include "FullScreenSubPass.h"
// Render a full screen pass to be used for some post processing.
//
// Project   : NaKama-Fx
// File Name : FullScreenSubPass.cpp
// Date      : 25/01/2019
// Author    : Nicholas Welters

#include "../Types/MeshBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPixelShader"> The sub pass pixel shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pMesh"> The geometry used to render the pass. </param>
	/// <param name="pTextures"> The textures and constants needed in the pass. </param>
	/// <param name="pMaterial"> The vertex shader constants. </param>
	/// <param name="pPSSceneBuffer"> The pixel shader constants. </param>
	/// <param name="pSamplers"> The texture sampler states. </param>
	FullScreenSubPass::FullScreenSubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
										  const ID3D11VertexShaderSPtr&      pVertexShader,
										  const ID3D11InputLayoutSPtr&       pInputLayput,
										  const ID3D11RasterizerStateSPtr&   pRasterizerState,
										  const ID3D11BlendStateSPtr&        pBlendState,
										  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
										  const MeshBufferSPtr&              pMesh,
										  const PSResourcesSPtr&             pMaterial,
										  const PSSamplersSPtr&              pSamplers )
		: RenderingSubPass( pVertexShader, pInputLayput, pRasterizerState, pDepthStencilState, pBlendState )
		, pPixelShader( pPixelShader )
		, pMesh( pMesh )
		, pMaterial( pMaterial )
		, pPSSamplers( pSamplers )
	{ }

	/// <summary> Render the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to render the subpass. </param>
	void FullScreenSubPass::renderPass( const DeviceContextPtr_t& pContext )
	{
		pContext->PSSetShader( pPixelShader.get( ), nullptr, 0 );

		pPSSamplers->set( pContext, 0 );
		pMaterial->set( pContext, 0 );
		pMesh->set( pContext );
		pMesh->render( pContext );
	}
} // namespace directX11
} // namespace visual
