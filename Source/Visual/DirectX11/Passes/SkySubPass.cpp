#include "SkySubPass.h"
// Render a full screen pass to fill in the sky.
//
// Project   : NaKama-Fx
// File Name : SkySubPass.cpp
// Date      : 25/01/2019
// Author    : Nicholas Welters

#include "../Types/MeshBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPixelShader"> The sub pass pixel shader. </param>
	/// <param name="pVertexShader"> The sub pass vertex shader. </param>
	/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
	/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
	/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
	/// <param name="pBlendState"> The sub pass blend state. </param>
	/// <param name="pMesh"> The geometry used to render the pass. </param>
	/// <param name="pTextures"> The textures and constants needed in the pass. </param>
	/// <param name="pVSSceneBuffer"> The vertex shader constants. </param>
	/// <param name="pPSSceneBuffer"> The pixel shader constants. </param>
	/// <param name="samplers"> The texture sampler states. </param>
	SkySubPass::SkySubPass( const ID3D11PixelShaderSPtr&       pPixelShader,
							const ID3D11VertexShaderSPtr&      pVertexShader,
							const ID3D11InputLayoutSPtr&       pInputLayput,
							const ID3D11RasterizerStateSPtr&   pRasterizerState,
							const ID3D11DepthStencilStateSPtr& pDepthStencilState,
							const ID3D11BlendStateSPtr&        pBlendState,
							const MeshBufferSPtr&              pMesh,
							const PSResourcesSPtr&             pTextures,
							const SceneVSBufferSPtr&           pVSSceneBuffer,
							const LightingPSBufferSPtr&        pPSSceneBuffer,
							const PSSamplersSPtr&              pSamplers )
		: RenderingSubPass( pVertexShader, pInputLayput, pRasterizerState, pDepthStencilState, pBlendState )
		, pPixelShader( pPixelShader )
		, pMesh( pMesh )
		, pTextures( pTextures )
		, pVSSceneBuffer( pVSSceneBuffer )
		, pPSSceneBuffer( pPSSceneBuffer )
		, pPSSamplers( pSamplers )
		, enabled( false )
	{
	}

	/// <summary> Render the subpasses constants. </summary>
	/// <param name="pContext"> The device context used to render the subpass. </param>
	void SkySubPass::renderPass( const DeviceContextPtr_t& pContext )
	{
		if( !enabled )
		{
			return;
		}

		pPSSamplers->set( pContext, 0 );
		pContext->PSSetShader( pPixelShader.get( ), nullptr, 0 );

		pVSSceneBuffer->set( pContext );
		pPSSceneBuffer->set( pContext );
		pTextures->set( pContext, 0 );

		pMesh->set( pContext );
		pMesh->render( pContext );
	}

	/// <summary> Move assignment operator </summary>
	/// <param name="pMaterial"> The textures and constants needed in the pass. </param>
	void SkySubPass::setPSResources( const PSResourcesSPtr& pMaterial )
	{
		pTextures = pMaterial;
	}

	/// <summary> Toggle the active state of the pass </summary>
	/// <param name="enable"> The state to set, true to draw the pass and false otherwise. </param>
	void SkySubPass::enableDraw( const bool enable )
	{
		enabled = enable;
	}
} // namespace directX11
} // namespace visual
