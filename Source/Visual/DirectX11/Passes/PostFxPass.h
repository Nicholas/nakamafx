#pragma once
// Post processing pass.
//
// This gets the average scene luminance for the final exposure value and then
// shifts the exposure towards that average value.
//
// This then builds the bloom blur buffers.
// It first down samples the scene to using a hi pass to collect the bright colours and
// then performs a blur,
// The image is down sampled and blured 3 additional times (4 samples 1/2, 1/4, 1/8 and 1/16)
// to build the final bloom.
//
// Project   : NaKama-Fx
// File Name : PostFxPass.h
// Date      : 04/12/2018
// Author    : Nicholas Welters

#ifndef _POST_FX_PASS_DX11_H
#define _POST_FX_PASS_DX11_H

#include "ComputePass.h"
#include "Visual/DirectX11/APITypeDefs.h"
#include "Visual/DirectX11/Materials/MaterialBuffer.h"

#include <memory>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( MultipleRenderTarget );
	FORWARD_DECLARE( UATarget );

	/// <summary>
	/// Our post processing pass. Using a bufferless vertex shader,
	/// we perform bloom, Get a "guess" of the scenes exposure.
	/// </summary>
	class PostFxPass : public ComputePass
	{
	public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pAverageColour"> Average Image Down Sample Target. </param>
		/// <param name="pFrameBuffer"> The frame buffer that we are going to build our post processing data from. </param>
		/// <param name="pRasterizerState"> Rasterizer state. </param>
		/// <param name="pBlendState"> Blend State. </param>
		/// <param name="pDepthStencilState"> The depth stencil state. </param>
		/// <param name="pLinearSampler"> A linear sampler, we  are bluring just about everything so its all good. </param>
		/// <param name="pFullScreenVS"> A bufferless fullscreen verex shader. </param>
		/// <param name="pResamplePS"> A resampoling shader to copy the data to a new render target. </param>
		/// <param name="pAdaptionCS"> Luminance value adaptation compute shader. </param>
		/// <param name="pAdaptionConsts"> Luminance value adaptation compute shader constants. </param>
		/// <param name="pAdaptionA"> Luminance value A buffer. </param>
		/// <param name="pAdaptionB"> Luminance value B buffer. </param>
		/// <param name="pBlur1A"> Blur 1/2 A buffer. </param>
		/// <param name="pBlur1B"> Blur 1/2 B buffer. </param>
		/// <param name="pBlur2A"> Blur 1/4 A buffer. </param>
		/// <param name="pBlur2B"> Blur 1/4 B buffer. </param>
		/// <param name="pBlur3A"> Blur 1/8 A buffer. </param>
		/// <param name="pBlur3B"> Blur 1/8 B buffer. </param>
		/// <param name="pBlur4A"> Blur 1/16 A buffer. </param>
		/// <param name="pBlur4B"> Blur 1/16 B buffer. </param>
		/// <param name="pHiPassPS"> The first bloom down sampling shader. </param>
		/// <param name="pBlurVVS"> The vertical pass vertex shader of the two pass blur. </param>
		/// <param name="pBlurHVS"> The horizontal pass vertex shader of the two pass blur. </param>
		/// <param name="pBlurPS"> The pixel shader of the two pass blur. </param>
		/// <param name="pBlur1VSConsts"> Blur 1/2 vertex shader constants buffer. </param>
		/// <param name="pBlur2VSConsts"> Blur 1/4 vertex shader constants buffer. </param>
		/// <param name="pBlur3VSConsts"> Blur 1/8 vertex shader constants buffer. </param>
		/// <param name="pBlur4VSConsts"> Blur 1/16 vertex shader constants buffer. </param>
		/// <param name="pBlurPSConsts"> Blur pixel shader constants buffer. </param>
		PostFxPass( const DeviceContextPtr_t&          pContext,
					const MultipleRenderTargetSPtr&    pAverageColour,
					const PSResourcesSPtr&             pFrameBuffer,
					const ID3D11RasterizerStateSPtr&   pRasterizerState,
					const ID3D11BlendStateSPtr&        pBlendState,
					const ID3D11DepthStencilStateSPtr& pDepthStencilState,
					const ID3D11SamplerStateSPtr&      pLinearSampler,
					const ID3D11VertexShaderSPtr&      pFullScreenVS,
					const ID3D11PixelShaderSPtr&       pResamplePS,

					const ID3D11ComputeShaderSPtr& pAdaptionCS,
					const AdaptationCSBufferSPtr&  pAdaptionConsts,
					const UATargetSPtr&            pAdaptionA,
					const UATargetSPtr&            pAdaptionB,

					const MultipleRenderTargetSPtr& pBlur1A,
					const MultipleRenderTargetSPtr& pBlur1B,
					const MultipleRenderTargetSPtr& pBlur2A,
					const MultipleRenderTargetSPtr& pBlur2B,
					const MultipleRenderTargetSPtr& pBlur3A,
					const MultipleRenderTargetSPtr& pBlur3B,
					const MultipleRenderTargetSPtr& pBlur4A,
					const MultipleRenderTargetSPtr& pBlur4B,

					const ID3D11PixelShaderSPtr& pHiPassPS,

					const ID3D11VertexShaderSPtr& pBlurVVS,
					const ID3D11VertexShaderSPtr& pBlurHVS,
					const ID3D11PixelShaderSPtr&  pBlurPS,

					const BlurVSBufferSPtr& pBlur1VSConsts,
					const BlurVSBufferSPtr& pBlur2VSConsts,
					const BlurVSBufferSPtr& pBlur3VSConsts,
					const BlurVSBufferSPtr& pBlur4VSConsts,
					const BlurPSBufferSPtr& pBlurPSConsts );

		/// <summary> ctor </summary>
		PostFxPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		PostFxPass( const PostFxPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		PostFxPass( PostFxPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~PostFxPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		PostFxPass& operator=( const PostFxPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		PostFxPass& operator=( PostFxPass&& move ) = delete;

		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The frame step size. </param>
		virtual void update( float delta ) override;

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		virtual void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> Average Image Down Sample Target. </summary>
		MultipleRenderTargetSPtr pAverageColour;

		/// <summary> The frame buffer that we are going to build our post processing data from. </summary>
		PSResourcesSPtr pFrameBuffer;

		/// <summary> A bufferless fullscreen verex shader. </summary>
		ID3D11VertexShaderSPtr pFullScreenVS;

		/// <summary> A resampoling shader to copy the data to a new render target. </summary>
		ID3D11PixelShaderSPtr pResamplePS;

		/// <summary> Rasterizer state. </summary>
		ID3D11RasterizerStateSPtr   pRasterizerState;

		/// <summary> Blend State. </summary>
		ID3D11BlendStateSPtr pBlendState;

		/// <summary> The depth stencil state. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilState;

		/// <summary> A linear sampler, we  are bluring just about everything so its all good. </summary>
		ID3D11SamplerStateSPtr pLinearSampler;

		// +----------------------+
		// | Luminance Adaptation |
		// +----------------------+
		/// <summary> Luminance value adaptation compute shader. </summary>
		ID3D11ComputeShaderSPtr pAdaptionCS;

		/// <summary> Luminance value adaptation compute shader constants. </summary>
		AdaptationCSBufferSPtr pAdaptionConsts;

		/// <summary> Luminance value double buffer. </summary>
		UATargetSPtr pAdaption[ 2 ];

		// +---------------------+
		// | Double Buffer Index |
		// +---------------------+
		/// <summary> The luminance double buffers current index. </summary>
		int buffer;

		// +-------+
		// | Bloom |
		// +-------+
		/// <summary> Blur 1/2 double buffer. </summary>
		MultipleRenderTargetSPtr pBlur1[ 2 ];

		/// <summary> Blur 1/4 double buffer. </summary>
		MultipleRenderTargetSPtr pBlur2[ 2 ];

		/// <summary> Blur 1/8 double buffer. </summary>
		MultipleRenderTargetSPtr pBlur3[ 2 ];

		/// <summary> Blur 1/16 double buffer. </summary>
		MultipleRenderTargetSPtr pBlur4[ 2 ];

		/// <summary> The first bloom down sampling shader. Performs a hi pass on the image to collect the bright sections. </summary>
		ID3D11PixelShaderSPtr pHiPassPS;

		/// <summary> The vertical pass vertex shader of the two pass blur. </summary>
		ID3D11VertexShaderSPtr pBlurVVS;

		/// <summary> The horizontal pass vertex shader of the two pass blur. </summary>
		ID3D11VertexShaderSPtr pBlurHVS;

		/// <summary> The pixel shader of the two pass blur, samples points provided by the vertex shaders. </summary>
		ID3D11PixelShaderSPtr pBlurPS;

		/// <summary> Blur 1/2 vertex shader constants buffer. </summary>
		BlurVSBufferSPtr pBlur1VSConsts;

		/// <summary> Blur 1/4 vertex shader constants buffer. </summary>
		BlurVSBufferSPtr pBlur2VSConsts;

		/// <summary> Blur 1/8 vertex shader constants buffer. </summary>
		BlurVSBufferSPtr pBlur3VSConsts;

		/// <summary> Blur 1/16 vertex shader constants buffer. </summary>
		BlurVSBufferSPtr pBlur4VSConsts;

		/// <summary> Blur pixel shader constants buffer. </summary>
		BlurPSBufferSPtr pBlurPSConsts;
	};
} // namespace directX11
} // namespace visual

#endif // _COMPUTE_HISTOGRAM_PASS_DX11_H
