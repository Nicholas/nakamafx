#pragma once
// Screen Space Reflection Sub Pass.
//
// This is a simple bufferless render pass that samples the current gbuffers and then
// tries to find a reflected texel so that we can try reuse as much of the info we already have.
//
// This is then blended on top of the original scene.
//
// TODO: Improve sampling technique. What will be a good sampling kernal.
// TODO: Improve the composition step. Clean up edges for a smoother blend.
// TODO: Add a cube mam fallback reflection system for when we dont have a screen space pixel.
//
// Project   : NaKama-Fx
// File Name : SSRPass.h
// Date      : 15/02/2019
// Author    : Nicholas Welters


#ifndef _SSR_PASS_DX11_H
#define _SSR_PASS_DX11_H

#include "ComputePass.h"
#include "../APITypeDefs.h"
#include "../Types/Samplers.h"

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( MultipleRenderTarget );
	FORWARD_DECLARE( SSRShaderBuffer );
	FORWARD_DECLARE( SSAOShaderBuffer );

	/// <summary> Screen Space Reflection Pass. </summary>
	class SSRPass : public ComputePass
	{
	public:
		/// <summary> ctor. </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pRasterizerState"> Primative to Fragment Settings. </param>
		/// <param name="pBlendStateOff"> Output Merget Blend Settings during reflection sampling. </param>
		/// <param name="pBlendState"> Output Merget Blend Settings during reflection blur and composite. </param>
		/// <param name="pDepthStencilState"> Output Merget Depth Stencil Settings. </param>
		/// <param name="pSSRTarget"> Reflection frame buffer. </param>
		/// <param name="pOutputTarget"> The scene render target that we will add the reflections too. </param>
		/// <param name="pFullScreenVS"> Bufferless fullscreen vertex shader. </param>
		/// <param name="pSSRShader"> Pixel shader that builds the reflection buffer. </param>
		/// <param name="pSSRShaderData"> Pixel shader data to build the reflection buffer. </param>
		/// <param name="pSSRUpShader"> Pixel shader that composes the reflection on top of the scene. </param>
		/// <param name="pSSRUpShaderData"> Pixel shader data used to composes the reflection on top of the scene. </param>
		/// <param name="pSamplers"> Pixel shader samplers. </param>
		SSRPass( const DeviceContextPtr_t&          pContext,
				 const ID3D11RasterizerStateSPtr&   pRasterizerState,
				 const ID3D11BlendStateSPtr&        pBlendStateOff,
				 const ID3D11BlendStateSPtr&        pBlendState,
				 const ID3D11DepthStencilStateSPtr& pDepthStencilState,
				 const MultipleRenderTargetSPtr&    pSSRTarget,
				 const MultipleRenderTargetSPtr&    pOutputTarget,
				 const ID3D11VertexShaderSPtr&      pFullScreenVS,
				 const ID3D11PixelShaderSPtr&       pSSRShader,
				 const SSAOShaderBufferSPtr&        pSSRShaderData,
				 const ID3D11PixelShaderSPtr&       pSSRUpShader,
				 const SSAOShaderBufferSPtr&        pSSRUpShaderData,
				 const PSSamplersSPtr&              pSamplers );

		/// <summary> ctor </summary>
		SSRPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		SSRPass( const SSRPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		SSRPass( SSRPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~SSRPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		SSRPass& operator=( const SSRPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		SSRPass& operator=( SSRPass&& move ) = delete;


		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The time step. </param>
		virtual void update( float delta ) override;

	protected:
		/// <summary>
		///	This is were we will set up the state of the GPU to perform a compute pass.
		/// </summary>
		virtual void compute( ) override;

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> Primative to Fragment Settings. </summary>
		ID3D11RasterizerStateSPtr pRasterizerState;

		/// <summary> Output Merget Blend Settings during reflection sampling. </summary>
		ID3D11BlendStateSPtr pBlendStateOff;

		/// <summary> Output Merget Blend Settings during reflection blur and composite. </summary>
		ID3D11BlendStateSPtr pBlendState;

		/// <summary> Output Merget Depth Stencil Settings. </summary>
		ID3D11DepthStencilStateSPtr pDepthStencilState;

		/// <summary> Reflection frame buffer. </summary>
		MultipleRenderTargetSPtr pSSRTarget;

		/// <summary> The scene render target that we will add the reflections too. </summary>
		MultipleRenderTargetSPtr pOutputTarget;

		/// <summary> Bufferless fullscreen vertex shader. </summary>
		ID3D11VertexShaderSPtr pFullScreenVS;

		/// <summary> Pixel shader that builds the reflection buffer. </summary>
		ID3D11PixelShaderSPtr pSSRShader;

		/// <summary> Pixel shader data to build the reflection buffer. </summary>
		SSAOShaderBufferSPtr pSSRShaderData;

		/// <summary> Pixel shader that composes the reflection on top of the scene. </summary>
		ID3D11PixelShaderSPtr pSSRUpShader;

		/// <summary> Pixel shader data used to composes the reflection on top of the scene. </summary>
		SSAOShaderBufferSPtr pSSRUpShaderData;

		/// <summary> Pixel shader samplers. </summary>
		PSSamplersSPtr pSamplers;
	};
} // namespace directX11
} // namespace visual

#endif // _SSR_PASS_DX11_H
