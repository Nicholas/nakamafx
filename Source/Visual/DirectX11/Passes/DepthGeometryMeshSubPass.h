#pragma once
// A subpass that gets called to make some draw calls to a render target bound in the rendering pass.
//
// This makes use of a geometry shader to modify the primitives during a depth only pass.
//
// Project   : NaKama-Fx
// File Name : DepthGeometryMeshSubPass.h
// Date      : 25/01/2019
// Author    : Nicholas Welters

#ifndef _DEPTH_GEOMETRY_MESH_SUB_PASS_DX11_H
#define _DEPTH_GEOMETRY_MESH_SUB_PASS_DX11_H

#include "DepthMeshSubPass.h"
#include "../APITypeDefs.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Depth Only Geometry Mesh Rendering Subpass </summary>
	class DepthGeometryMeshSubPass : public DepthMeshSubPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pGeometryShader"> The sub pass geometry shader. </param>
		/// <param name="pVertexShader"> The sub pass vertex shader. </param>
		/// <param name="pInputLayput"> The sub pass vertex input layout. </param>
		/// <param name="pRasterizerState"> The sub pass rasterizer state. </param>
		/// <param name="pDepthStencilState"> The sub pass depth stencil state. </param>
		/// <param name="pBlendState"> The sub pass blend state. </param>
		/// <param name="pVSSceneBuffer"> The sub pass vertex shader scene constants. </param>
		/// <param name="pGSSceneBuffer"> The sub pass geomertry shader scene constants. </param>
		DepthGeometryMeshSubPass( const ID3D11GeometryShaderSPtr&    pGeometryShader,
								  const ID3D11VertexShaderSPtr&      pVertexShader,
								  const ID3D11InputLayoutSPtr&       pInputLayput,
								  const ID3D11RasterizerStateSPtr&   pRasterizerState,
								  const ID3D11DepthStencilStateSPtr& pDepthStencilState,
								  const ID3D11BlendStateSPtr&        pBlendState,
								  const DepthVSBufferSPtr&           pVSSceneBuffer,
								  const SceneGSBufferSPtr&           pGSSceneBuffer );

		/// <summary> ctor </summary>
		DepthGeometryMeshSubPass( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DepthGeometryMeshSubPass( const DepthGeometryMeshSubPass& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DepthGeometryMeshSubPass( DepthGeometryMeshSubPass&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~DepthGeometryMeshSubPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		DepthGeometryMeshSubPass& operator=( const DepthGeometryMeshSubPass& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		DepthGeometryMeshSubPass& operator=( DepthGeometryMeshSubPass&& move ) = default;


		/// <summary> Update the subpasses constants that might be changed. </summary>
		/// <param name="pContext"> The device context used to update the subpass contents. </param>
		/// <param name="delta"> The time taken to draw the last frame. </param>
		virtual void update( const DeviceContextPtr_t& pContext, const float& delta ) override;

	protected:
		/// <summary> Set the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to set the subpass constants. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Unset the subpasses constants. </summary>
		/// <param name="pContext"> The device context used to unset the subpass constants. </param>
		virtual void unset( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The subpass geometry shader. </summary>
		ID3D11GeometryShaderSPtr pGeometryShader;

		/// <summary> The subpass geometry shader constants. </summary>
		SceneGSBufferSPtr pGSSceneBuffer;
	};
} // namespace directX11
} // namespace visual

#endif // _DEPTH_GEOMETRY_MESH_SUB_PASS_DX11_H

