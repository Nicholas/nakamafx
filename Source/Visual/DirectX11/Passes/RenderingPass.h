#pragma once
// This is the renderes logical grouping of objects that need to be rendered together
// at the same time to the same render target. This lets us split the rendering to render targets
// so that we can perform a copy inbetween if it is needed.
//
// Project   : NaKama-Fx
// File Name : SceneRenderer.h
// Date      : 15/02/2017
// Author    : Nicholas Welters

#ifndef _RENDERING_PASS_DX11_H
#define _RENDERING_PASS_DX11_H

#include "../APITypeDefs.h"

#include <Threading/AsyncController.h>

#include <memory>
#include <vector>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( OutputTarget );
	FORWARD_DECLARE( RenderingSubPass );

	/// <summary> A set of GPU instructions to render to an output target. </summary>
	class RenderingPass
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pContext"> The D3D11 device context. </param>
		/// <param name="pTarget"> The render target that we are going to draw to. </param>
		RenderingPass( const DeviceContextPtr_t& pContext,
					   const OutputTargetSPtr& pTarget );

		/// <summary> ctor </summary>
		RenderingPass( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		RenderingPass( const RenderingPass& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		RenderingPass( RenderingPass&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~RenderingPass( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		RenderingPass& operator=( const RenderingPass& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		RenderingPass& operator=( RenderingPass&& move ) = delete;

		/// <summary>
		///	This is were we will update any gpu date thats changed on the cpu.
		/// </summary>
		/// <param name="delta"> The time step taken to update the frame. </param>
		virtual void update( float delta );

		/// <summary>
		///	This is were we will set up the state of the GPU to render
		///	all the object in this scene.
		/// </summary>
		virtual void render( );

		/// <summary> Add a sub pass that will draw in this pass. </summary>
		/// <param name="pSubPass"> A subpass to add to the rendering pass. </param>
		void add( const RenderingSubPassSPtr& pSubPass );

		/// <summary> Remove a sub pass that will draw in this pass. </summary>
		/// <param name="pSubPass"> A subpass to remove from the rendering pass. </param>
		void remove( const RenderingSubPassSPtr& pSubPass );

		/// <summary> Change the render target of the rendering pass. </summary>
		/// <param name="pTarget"> A new output target for the rendering pass. </param>
		void setOutputTarget( const OutputTargetSPtr& pTarget );

		/// <summary> Toggle the rendering passes active state. </summary>
		/// <param name="active"> The active state to set. </param>
		void setActive( bool active );

		/// <summary> Check the passes active state. </summary>
		bool isActive( ) const;

		/// <summary> Toggle the rendering passes visibility state. </summary>
		/// <param name="visible"> The visibility state to set. </param>
		void setVisibility( bool visible );

		/// <summary> Check the passes visibility state. </summary>
		bool isVisible( ) const;

		/// <summary> Set what parts of the render targets get cleared. </summary>
		/// <param name="colour"> Clear the render targets colour buffers. </param>
		/// <param name="depth"> Clear the render targets depth buffer. </param>
		/// <param name="stencil"> Clear the render targets stencil buffer. </param>
		void setRtClearFlags( bool colour, bool depth, bool stencil );

		/// <summary> Check if the pass wants to clear the colour targets. </summary>
		bool isClearingColour( ) const;

		/// <summary> Check if the pass wants to clear the depth target. </summary>
		bool isClearingDepth( ) const;

		/// <summary> Check if the pass wants to clear the stencil target. </summary>
		bool isClearingStencil( ) const;

		/// <summary> Set the clear colour used on the passes render target. </summary>
		/// <param name="r"> The red channel. </param>
		/// <param name="g"> The green channel. </param>
		/// <param name="b"> The blue channel. </param>
		/// <param name="a"> The alpha channel. </param>
		void setClearColour( float r, float g, float b, float a );

	protected:
		/// <summary> Allow subclasses to add to the end of the rendering pass. </summary>
		virtual void finalizeState( );

	private:
		/// <summary>	This will set the GPU state like the render target and depth stencil buffer. </summary>
		void setState( ) const;

		/// <summary> Put the  renderer back in a state that we know. </summary>
		void restorState( );

	private:
		/// <summary> The D3D11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> This is the destination of the rendering of the object in this scene. </summary>
		OutputTargetSPtr pTarget;

		/// <summary>  The list of sub scenes. The grouping of objects the render using the same logic. </summary>
		std::vector< RenderingSubPassSPtr > pSubPasses;

		/// <summary> Colour clear state. </summary>
		bool clearColour;

		/// <summary> Depth clear state. </summary>
		bool clearDepth;

		/// <summary> Stencil clear state. </summary>
		bool clearStencil;

		/// <summary>
		/// This is what we will use determine if this scene should do anything such as clearing the render target.
		/// </summary>
		bool active;

		/// <summary>
		/// This is what we will use determine if this scene should be rendered but the render targets are cleared and
		/// we will perform all the other state changes.
		/// </summary>
		bool visible;
	};
} // namespace directX11
} // namespace visual

#endif // _RENDERING_PASS_DX11_H
