#pragma once

// A simple helper class that allows us
// to read data from the applications resources.
//
// Project   : NaKama-Fx
// File Name : ResourceReader.h
// Date      : 02/04/2019
// Author    : Nicholas Welters

#ifndef _RESOURCE_READER_H
#define _RESOURCE_READER_H

namespace visual
{
namespace directX11
{
	/// <summary> 
	/// A simple helper class that allows us
	/// to read data from the applications resources.
	/// </summary> 
	class ResourceReader
	{
		public:
			/// <summary> Get the data from the applications data archive. </summary>
			/// <param name="id"> The resource id. </param>
			/// <returns> The resource data and its size (in 32 bits). </returns>
			static std::pair< uint32_t*, size_t > dataFromResource( _In_ int id );
	};

} // namespace directX11
} // namespace visual

#endif _VERTEX_LAYOUT_DX11_H

