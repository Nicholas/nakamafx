#pragma once

#include "SceneRenderer.h"
//
// DX11 Scene Renderer
//
// Here we are going to perform all our drawing and computations.
// We are going to send our GPU updates from here too.
//
// Project   : NaKama-Fx
// File Name : SceneRenderer.cpp
// Date      : 15-06-2015
// Author    : Nicholas Welters
//

#include "../FX/Settings.h"
#include "../FX/Vertex.h"
#include "../FX/Camera/Camera.h"

#include "Types/Model.h"
#include "Types/Text.h"
#include "Passes//RenderingPass.h"
#include "Passes/ColourMeshSubPass.h"
#include "Passes/SkySubPass.h"
#include "Passes/TextSubPass.h"
#include "Passes/GeometryMeshSubPass.h"
#include "Passes/DepthGeometryMeshSubPass.h"
#include "Passes/ParticleSubPass.h"
#include "Passes/ComputeHistogramPass.h"
#include "Passes/ABufferOITPass.h"
#include "Passes/ToneMappingPass.h"
#include "Passes/UIPass.h"
#include "Passes/PostFXSubPass.h"
#include "Types/SceneDataSet.h"
#include "Materials/MaterialBuffer.h"
#include "Builders/PipelineBuilder.h"

#include <Macros.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <vector>
#include <limits>
#include <sstream>
#include <iomanip>

namespace visual
{
namespace fx
{
	SHARED_PTR_TYPE_DEF( Camera )
	SHARED_PTR_TYPE_DEF( Settings )
}

namespace directX11
{
	using fx::Vertex3f2f;
	using fx::Vertex3f2f3f3f3f;
	using fx::VertexParticle;
	using fx::CameraSPtr;
	using fx::SettingsSPtr;

	using glm::mat4x4;
	using glm::vec4;
	using glm::vec3;
	using glm::vec2;
	using glm::ivec3;
	using glm::normalize;
	using glm::yawPitchRoll;
	using glm::lookAtLH;
	using glm::orthoLH;
	using glm::radians;
	using glm::pi;

	using std::function;
	using std::vector;
	using std::shared_ptr;
	using std::make_shared;
	using std::numeric_limits;
	using std::min;
	using std::max;

	/// <summary> ctor </summary>
	/// <param name="pContext"> DX11 device context </param>
	/// <param name="data"> The pipeline rendering and compute passes. </param>
	/// <param name="pCamera"> The camera that we are going to draw the scene in front of. </param>
	/// <param name="pSettings"> The system settings so that we can set some shader constants. </param>
	SceneRenderer::SceneRenderer(
		const DeviceContextPtr_t& pContext,
		const PipelineBuilder::PipelineData& data,
		const CameraSPtr& pCamera,
		const SettingsSPtr& pSettings )
		: fx::SceneRenderer( )
		, pContext( pContext )
		, data( data )
		, pCamera( pCamera )
		, pSettings( pSettings )
		, pLoadedScene( nullptr )
		, sunDir( 0.00f )
		, fpsCounter( )
		, times( 4 )
		, timer( )
	{
		const vec2 shadowMap1Size = pSettings->getShadowCascade1Size( );
		const vec2 shadowMap2Size = pSettings->getShadowCascade2Size( );
		const vec2 renderTargetSize = pSettings->getRenderTargetSize( );

		vec3 lightDirection( 0.0, 0.0, 1.0 );
		lightDirection = normalize( lightDirection );

		const vec2 one( 1, 1 );

		data.pPSLightingBuffer->get( ) =
		{
			{ 10.0,  10.0,  10.0, 0 }, // Ambient Colour
			{ 100,  93,  90, 0 }, // Direction Colour
			lightDirection,      0,  // Direction
			0.0,     // Tick
			0.0,     // Time,
			renderTargetSize,
			one / renderTargetSize,
			one / shadowMap1Size,
			one / shadowMap2Size,
			{ 1, 100 }
		};

		data.pVSSceneBuffer->get( ).viewProjection =
		{
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		data.pVSShadow1Buffer->get( ).viewProjection =
		{
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		data.pVSShadow2Buffer->get( ).viewProjection =
		{
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};


		data.pPSLightingBuffer->get( ).zDepths.x = 2;
		data.pPSLightingBuffer->get( ).zDepths.y = 1024;
		data.pPSLightingBuffer->get( ).targetSize = renderTargetSize;
		data.pPSLightingBuffer->get( ).invTargetSize.x = 1.0f / renderTargetSize.x;
		data.pPSLightingBuffer->get( ).invTargetSize.y = 1.0f / renderTargetSize.y;
		data.pPSLightingBuffer->get( ).shadow1Size.x = 1.0f / shadowMap1Size.x;
		data.pPSLightingBuffer->get( ).shadow1Size.y = 1.0f / shadowMap1Size.y;
		data.pPSLightingBuffer->get( ).shadow2Size.x = 1.0f / shadowMap2Size.x;
		data.pPSLightingBuffer->get( ).shadow2Size.y = 1.0f / shadowMap2Size.y;
		data.pPSSunVolumeBuffer->get( ).shadowSize1 = vec4( shadowMap1Size, data.pPSLightingBuffer->get( ).shadow1Size );
		data.pPSSunVolumeBuffer->get( ).shadowSize2 = vec4( shadowMap2Size, data.pPSLightingBuffer->get( ).shadow2Size );

		data.pPostFXConstants->get( ).gamma = 1.0f / 2.2f;
		//pPostFXConstants->get( ).gamma      = 2.2f;
		data.pPostFXConstants->get( ).contrast   = 0.0f;
		data.pPostFXConstants->get( ).saturation = 0.0f;
		data.pPostFXConstants->get( ).brightness = 0.0f;

		data.pHistogramConstants->get( ).inputLuminanceMax = 4.5f;
		//pHistogramConstants->get( ).inputLuminanceMax = 100;
		data.pHistogramConstants->get( ).inputLuminanceMin = 0.0001f;

		data.pHistogramConstants->get( ).outputLuminanceMax = 1.0f;
		data.pHistogramConstants->get( ).outputLuminanceMin = 0.0001f;

		data.pHistogramConstants->get( ).outputWidth  = static_cast< unsigned int >( renderTargetSize.x );
		data.pHistogramConstants->get( ).outputHeight = static_cast< unsigned int >( renderTargetSize.y );

		// TODO: Remove the magic numbers for the tile sizes.
		const float tilesX = ceilf( renderTargetSize.x / 32 );
		const float tilesY = ceilf( renderTargetSize.y / 16 );

		data.pHistogramConstants->get( ).numPerTileHistograms = static_cast< unsigned int >( tilesX * tilesY );
		data.pHistogramConstants->get( ).tilesX               = static_cast< unsigned int >( tilesX );





		data.clusteredLighting.pFrustumConstruction->getConstants( )->get( ).invProjection = inverse( pCamera->getProjection( 0 ) );
		data.clusteredLighting.pFrustumConstruction->getConstants( )->get( ).nearZPlane    = data.pPSLightingBuffer->get( ).zDepths.x;  // pCamera->getNearPlane( 0 );
		data.clusteredLighting.pFrustumConstruction->getConstants( )->get( ).farZPlane     = data.pPSLightingBuffer->get( ).zDepths.y;// pCamera->getFarPlane( 0 );
		//data.clusteredLighting.pFrustumConstruction->getConstants( )->get( ).screenSize    = vec4( pCamera->getSize( 0 ), 1.0f / pCamera->getSize( 0 ) );
		data.clusteredLighting.pFrustumConstruction->getConstants( )->get( ).screenSize    = vec4( renderTargetSize, 1.0f / renderTargetSize );

		data.clusteredLighting.pFrustumConstruction->update( 0 );
		data.clusteredLighting.pFrustumConstruction->render( );

		data.pNormalSubPass->setEnabled( false );

		timer.reset( );
	}

	void SceneRenderer::update( const float delta )
	{
		timer.tick( );
		times[ 0 ] = timer.getDelta( );

		execute( );

		fpsCounter.countTick( delta );

		if( data.pToggles->enableDebugFPS )
		{
			std::stringstream infoStream;
			infoStream << std::internal << std::fixed << std::setprecision( 0 );
			infoStream << "FPS: " << std::setw( 3 ) << std::setfill( '0' ) << fpsCounter.getCountByPeriod( ) << '\n';

			if( data.pToggles->enableDebugTimes )
			{
				infoStream << std::setprecision( 2 );
				//infoStream << "FPS    : " << std::setw( 6 ) << std::setfill( '0' ) << fpsCounter.getCountByDelta( ) << '\n';
				//infoStream << "FPS Avg: " << std::setw( 6 ) << std::setfill( '0' ) << fpsCounter.getCountByPeriod( ) << '\n';
				//infoStream << "FPS 9/1: " << std::setw( 6 ) << std::setfill( '0' ) << fpsCounter.getCountByRatio( _9_1_ ) << '\n';
				//infoStream << "FPS 7/3: " << std::setw( 6 ) << std::setfill( '0' ) << fpsCounter.getCountByRatio( _7_3_ ) << "\n";
				infoStream << "---------------\n";
				infoStream << "Wait  : "  << std::setw( 5 ) << std::setfill( '0' ) << times[ 0 ] * 1000 << "ms\n";
				infoStream << "Update: "  << std::setw( 5 ) << std::setfill( '0' ) << times[ 1 ] * 1000 << "ms\n";
				infoStream << "Wait  : "  << std::setw( 5 ) << std::setfill( '0' ) << times[ 2 ] * 1000 << "ms\n";
				infoStream << "Draw  : "  << std::setw( 5 ) << std::setfill( '0' ) << times[ 3 ] * 1000 << "ms\n";
				infoStream << "---------------";
			}

			data.pLdrInfoText->setText( pContext, infoStream.str( ) );
		}
		else
		{
			data.pLdrInfoText->setText( pContext, "" );
		}


		data.pVSSceneBuffer->get().viewProjection = transpose( pCamera->getViewProjection( 0 ) );
		data.pVSSceneBuffer->get().view = transpose( pCamera->getView( ) );
		data.pVSSceneBuffer->get().cameraPosition = pCamera->getEye( );

		data.pFrameGSConstants->get().viewProjection = transpose( pCamera->getViewProjection( 0 ) );
		data.pFrameGSConstants->get().eyePosition.x = pCamera->getEye( ).x;
		data.pFrameGSConstants->get().eyePosition.y = pCamera->getEye( ).y;
		data.pFrameGSConstants->get().eyePosition.z = pCamera->getEye( ).z;
		data.pFrameGSConstants->get().eyePosition.w = 1;

		data.pGSSceneBuffer->get().viewProjection = transpose( pCamera->getViewProjection( 0 ) );
		data.pGSSceneBuffer->get( ).eyePosition.x = pCamera->getEye( ).x;
		data.pGSSceneBuffer->get( ).eyePosition.y = pCamera->getEye( ).y;
		data.pGSSceneBuffer->get( ).eyePosition.z = pCamera->getEye( ).z;
		data.pGSSceneBuffer->get( ).eyePosition.w = 1;
		data.pGSSceneBuffer->get( ).view = transpose( pCamera->getView( ) );

		data.clusteredLighting.pLightFrustumCulling->getConstants( )->get( ).farZPlane = pCamera->getFarPlane( 0 );

		data.pPSLightingBuffer->get( ).tick = delta * 0.025f;
		data.pPSLightingBuffer->get( ).time = fmod(data.pPSLightingBuffer->get( ).time + data.pPSLightingBuffer->get( ).tick, 10000000.0f );



		// /////////////////// //
		// Set Direction Light //
		// /////////////////// //
		const vec3 origin = normalize( vec3( 0.0, 1.0, -0.5 ) );

		//mat4x4 rotation = yawPitchRoll( 0.0f, 0.0f, sunDir + data.pPSLightingBuffer->get( ).time );
		mat4x4 rotation = yawPitchRoll( 0.0f, 0.0f, sunDir );
		vec3 direction = rotation * vec4( origin, 0.0f );
		direction = normalize( direction );

		data.pPSLightingBuffer->get().dLightD = direction;


		// TODO: Get this as a compute pass
		if( pLoadedScene && pLoadedScene->hasLights )
		{
			for( size_t i = 0; i < pLoadedScene->cpuLights.size( ); ++i )
			{
				pLoadedScene->cpuLights[ i ].positionVS = pCamera->getView( ) * pLoadedScene->cpuLights[ i ].positionWS;
				pLoadedScene->cpuLights[ i ].directionVS = normalize( pCamera->getView( ) * pLoadedScene->cpuLights[ i ].directionWS );
			}

			pContext->UpdateSubresource(
				pLoadedScene->gpuLights.pBuffer.get( ), 0, nullptr,
				pLoadedScene->cpuLights.data( ),
				sizeof( fx::LightEx ), 0 );
		}
		else if( !data.clusteredLighting.cpuLights.empty( ) )
		{
			for( size_t i = 0; i < data.clusteredLighting.cpuLights.size( ); ++i )
			{
				data.clusteredLighting.cpuLights[ i ].positionVS = pCamera->getView( ) * data.clusteredLighting.cpuLights[ i ].positionWS;
				data.clusteredLighting.cpuLights[ i ].directionVS = normalize( pCamera->getView( ) * data.clusteredLighting.cpuLights[ i ].directionWS );
			}

			pContext->UpdateSubresource(
				data.clusteredLighting.gpuLights.pBuffer.get( ), 0, nullptr,
				data.clusteredLighting.cpuLights.data( ),
				sizeof( fx::LightEx ), 0 );
		}



		// ///////////////////////// //
		// Set Shadow Map Projection //
		// ///////////////////////// //
		vector < ShadowCascade> shadowCascades = calculateShadowCascadeProjection( direction, 128.0f, rotation );

		data.pVSShadow1Buffer->get( ).viewProjection = shadowCascades[ 0 ].viewProjection;
		data.pVSShadow2Buffer->get( ).viewProjection = shadowCascades[ 1 ].viewProjection;


		data.pVSTranslucentShadowBuffer1->get( ).view              = shadowCascades[ 0 ].view;
		data.pVSTranslucentShadowBuffer1->get( ).viewProjection    = shadowCascades[ 0 ].viewProjection;
		data.pVSTranslucentShadowBuffer1->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pVSTranslucentShadowBuffer1->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;
		data.pVSTranslucentShadowBuffer1->get( ).cameraPosition = direction * 100.0f;
		data.pVSTranslucentShadowBuffer1->get( ).dLightD = data.pPSLightingBuffer->get( ).dLightD;
		//pVSTranslucentShadowBuffer1->get( ).pLightP = pPSLightingBuffer->get( ).pLightP;


		data.pVSTranslucentShadowBuffer2->get( ).view              = shadowCascades[ 1 ].view;
		data.pVSTranslucentShadowBuffer2->get( ).viewProjection    = shadowCascades[ 1 ].viewProjection;
		data.pVSTranslucentShadowBuffer2->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pVSTranslucentShadowBuffer2->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;
		data.pVSTranslucentShadowBuffer2->get( ).cameraPosition = direction * 100.0f;
		data.pVSTranslucentShadowBuffer2->get( ).dLightD = data.pPSLightingBuffer->get( ).dLightD;
		//pVSTranslucentShadowBuffer2->get( ).pLightP = pPSLightingBuffer->get( ).pLightP;


		data.pVSSceneBuffer->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pVSSceneBuffer->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;

		// Update vertex shader lighting values
		//pVSSceneBuffer->get().pLightP = pPSLightingBuffer->get().pLightP;
		data.pVSSceneBuffer->get().dLightD = data.pPSLightingBuffer->get().dLightD;


		// Update other (normals) geometry shader time.
		data.pGSSceneBuffer->get( ).gGameTime += data.pPSLightingBuffer->get( ).tick;
		data.pGSSceneBuffer->get( ).gTimeStep  = data.pPSLightingBuffer->get( ).tick;

		data.pFrameGSConstants->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pFrameGSConstants->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;
		data.pGSSceneBuffer->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pGSSceneBuffer->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;



		//pGSDepthSceneBuffer->get( ).gGameTime;
		//pGSDepthSceneBuffer->get( ).gTimeStep;
		data.pGSShadow1SceneBuffer->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pGSShadow1SceneBuffer->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;

		data.pGSShadow1SceneBuffer->get( ).viewProjection = shadowCascades[ 0 ].viewProjection;
		data.pGSShadow1SceneBuffer->get( ).view           = shadowCascades[ 0 ].view;
		data.pGSShadow1SceneBuffer->get( ).eyePosition.x  = data.pGSSceneBuffer->get( ).eyePosition.x;
		data.pGSShadow1SceneBuffer->get( ).eyePosition.y  = data.pGSSceneBuffer->get( ).eyePosition.y;
		data.pGSShadow1SceneBuffer->get( ).eyePosition.z  = data.pGSSceneBuffer->get( ).eyePosition.z;
		data.pGSShadow1SceneBuffer->get( ).eyePosition.w  = 1;


		data.pGSShadow2SceneBuffer->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pGSShadow2SceneBuffer->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;

		data.pGSShadow2SceneBuffer->get( ).viewProjection = shadowCascades[ 1 ].viewProjection;
		data.pGSShadow2SceneBuffer->get( ).view          = shadowCascades[ 1 ].view;
		data.pGSShadow2SceneBuffer->get( ).eyePosition.x = data.pGSSceneBuffer->get( ).eyePosition.x;
		data.pGSShadow2SceneBuffer->get( ).eyePosition.y = data.pGSSceneBuffer->get( ).eyePosition.y;
		data.pGSShadow2SceneBuffer->get( ).eyePosition.z = data.pGSSceneBuffer->get( ).eyePosition.z;
		data.pGSShadow2SceneBuffer->get( ).eyePosition.w = 1;


		// +------+
		// | SSAO |
		// +------+
		data.pSSAOShaderData->setProjection( pCamera->getProjection( 0 ) );
		data.pSSAOShaderData->setView( pCamera->getView( ) );


		// +------+
		// | SSIL |
		// +------+
		data.pSSILShaderData->setProjection( pCamera->getProjection( 0 ) );
		data.pSSILShaderData->setView( pCamera->getView( ) );


		// +------+
		// | SSR |
		// +------+
		data.pSSRShaderData->setProjection( pCamera->getProjection( 0 ) );
		data.pSSRShaderData->setView( pCamera->getView( ) );


		// +----------------------+
		// | SUN VOLUMETRIC LIGHT |
		// +----------------------+
		data.pPSSunVolumeBuffer->get( ).invView          = transpose( inverse( pCamera->getView( ) ) );
		data.pPSSunVolumeBuffer->get( ).shadowProjection1 = shadowCascades[ 0 ].viewProjection;
		data.pPSSunVolumeBuffer->get( ).shadowProjection2 = shadowCascades[ 1 ].viewProjection;

		data.pPSSunVolumeBuffer->get( ).dLightC = data.pPSLightingBuffer->get( ).dLightC;
		data.pPSSunVolumeBuffer->get( ).dLightD = data.pPSLightingBuffer->get( ).dLightD;
		data.pPSSunVolumeBuffer->get( ).cameraPos = pCamera->getEye( );

		data.pABufferResolveCBuffer->get( ).invView = data.pPSSunVolumeBuffer->get( ).invView;


		if( pLoadedScene )
		{
			ivec3 pcgPosition = pCamera->getEye( ) + pCamera->getAt( ) * 250.0f;
			pcgPosition.y = 0;

			for( const ModelSPtr& pModel : pLoadedScene->pcgHeightMaps )
			{
				pModel->setPosition( pcgPosition );
				pModel->update( );
			}
			for( const ModelSPtr& pModel : pLoadedScene->pcgWaterFollower )
			{
				pModel->setPosition( pcgPosition );
				pModel->update( );
			}
		}



		data.pNormalSubPass->setEnabled( data.pToggles->enableDebugNormals );
		data.pDebugRTView->setEnabled( data.pToggles->enableDebug );


		// //////////////////////// //
		// Update the scene buffers //
		// //////////////////////// //
		data.pShadowPass1->update( delta );
		data.pShadowPass2->update( delta );
		data.pTransparentShadowPass1->update( delta );
		data.pTransparentShadowPass2->update( delta );
		data.pOpaquePass->update( delta );
		data.pTransparentPass->update( delta );
		data.pABufferOITPass->update( delta );
		data.pSkyPass->update( delta );

		data.pOpaqueCopyPass->update( delta );
		data.pParticalPass->update( delta );
		data.pVolumetricPass->update( delta );
		data.pSSAOPass->update( delta );
		data.pSSILPass->update( delta );
		data.pSSRPass->update( delta );

		data.pBackBufferPass->update( delta );

		data.pHistogramPass->update( delta );
		data.pPostFxPass->update( delta );

		data.pToneMappingPass->update( delta );
		data.pUIPass->update( delta );

		data.clusteredLighting.pFrustumConstruction->update( delta );
		data.clusteredLighting.pLightFrustumCulling->update( delta );

		timer.tick( );
		times[ 1 ] = timer.getDelta( );
	}

	/// <summary> This is were we will set up the state of the GPU to render all the object in this scene. </summary>
	void SceneRenderer::render( )
	{
		timer.tick( );
		times[ 2 ] = timer.getDelta( );

		// ////////////// //
		// CLUSTER LIGHTS //
		// ////////////// //
		//clusteredLighting.pFrustumConstruction->render( );
		data.clusteredLighting.pLightFrustumCulling->render( );

		// /////////// //
		// SHADOW PASS //
		// /////////// //
		data.pTransparentShadowPass1->render( );
		data.pShadowPass1->render( );
		data.pShadowPass2->render( );
		data.pTransparentShadowPass2->render( );

		// ////////////////////////// //
		// OPAQUE MESH PASS //
		// ////////////////////////// //
		data.pOpaquePass->render( );

		// //////////////// //
		// AMBIANT LIGHTING //
		// //////////////// //
		if( data.pToggles->enableSSIL )
		{
			data.pSSILPass->render( );
		}
		if( data.pToggles->enableSSAO )
		{
			data.pSSAOPass->render( );
		}

		// ////// //
		// SKYBOX //
		// ////// //
		data.pSkyPass->render( );

		/**/
		// ///////////////////// //
		// TRANSPARENT MESH PASS //
		// ///////////////////// //
		data.pOpaqueCopyPass->render( );
		data.pTransparentPass->render( );

		// /////////////////////// //
		// SCREEN SPACE REFLECTION //
		// /////////////////////// //
		if( data.pToggles->enableSSR )
		{
			data.pSSRPass->render( );
		}

		// ///////////////////// //
		// TRANSLUCENT MESH PASS //
		// ///////////////////// //
		if( data.pToggles->enableOIT )
		{
			data.pOpaqueCopyPass->render( );
			data.pABufferOITPass->render( );
		}

		// ////////////////// //
		// GPU PARTICLES PASS //
		// ////////////////// //
		data.pParticalPass->render( );

		// /////////////////////////// //
		// Volumetric & Analitical Fog //
		// /////////////////////////// //
		if( data.pToggles->enableFog )
		{
			data.pVolumetricPass->render( );
		}

		///////////////////
		// Compute Pass //
		/////////////////
		if( data.pToggles->enableDebug )
		{
			data.pHistogramPass->render( );
		}
		data.pPostFxPass->render( );
		//*/

		// /////////////////////////////////////////////// //
		// Render the final scene to the swap chain output //
		// /////////////////////////////////////////////// //
		data.pToneMappingPass->render( );
		data.pBackBufferPass->render( );
		data.pUIPass->render( );

		timer.tick( );
		times[ 3 ] = timer.getDelta( );
	}

	/// <summary>
	/// Add Elements to the scene so that we can render something of interest.
	/// </summary>
	/// <param name="pPayload"> The scene data that we need to render our scene. </param>
	void SceneRenderer::setScene( const SceneDataSetSPtr& pPayload )
	{
		addUpdateEvent(
			[ this, pPayload ]( )
			{
				if( pLoadedScene )
				{
					// Remove the existing elements from the scene.
					for( const ModelSPtr& pModel : pLoadedScene->spheres )
					{
						data.pSphereSubPass->remove( pModel );
					}
					// Remove the existing elements from the scene.
					for( const ModelSPtr& pModel : pLoadedScene->pbrSpheres )
					{
						data.pPbrSphereSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->planets )
					{
						data.pPlanetSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->moons )
					{
						data.pMoonSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->cubes )
					{
						data.pCubeSubPass->remove( pModel );
						data.pGeometryShadowSubPass1->remove( pModel );
						data.pGeometryShadowSubPass2->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->tiles )
					{
						data.pTileSubPass->remove( pModel );
						data.pShadowSubPass1->remove( pModel );
						data.pShadowSubPass2->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->basicObjs )
					{
						data.pBasicObjSubPass->remove( pModel );
						data.pShadowSubPass1->remove( pModel );
						data.pShadowSubPass2->remove( pModel );

						data.pNormalSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->clippedObjs )
					{
						data.pBasicObjSubPass->remove( pModel );
						data.pClippedShadowSubPass1->remove( pModel );
						data.pClippedShadowSubPass2->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->translucent )
					{
						data.pABufferOITPass->remove( pModel );
						data.pTranslucentShadowSubPass1->remove( pModel );
						data.pTranslucentShadowSubPass2->remove( pModel );

						data.pNormalSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->heightMaps )
					{
						data.pHightMapSubPass->remove( pModel );
						data.pShadowSubPass1->remove( pModel );
						data.pShadowSubPass2->remove( pModel );

						data.pNormalSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->pcgHeightMaps )
					{
						data.pPCGHightMapSubPass->remove( pModel );
						data.pPCGShadowSubPass1->remove( pModel );
						data.pPCGShadowSubPass2->remove( pModel );
						//
						//data.pNormalSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->water )
					{
						data.pWaterSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->pcgWater )
					{
						data.pPCGWaterSubPass->remove( pModel );
					}
					for( const ModelSPtr& pModel : pLoadedScene->pcgWaterFollower )
					{
						data.pPCGWaterSubPass->remove( pModel );
					}
					for( const ParticleBufferSPtr& pParticleSystem : pLoadedScene->particles )
					{
						data.pParticalSubPass->remove( pParticleSystem );
					}

					// Text
					for( const TextSPtr& pText : pLoadedScene->text2D )
					{
						data.pLdrInfoSubPass->remove( pText );
					}
					for( const TextSPtr& pText : pLoadedScene->text3D )
					{
						data.pText3DSubPass->remove( pText );
						data.pText3DShadowSubPass1->remove( pText );
						data.pText3DShadowSubPass2->remove( pText );
					}
				}

				if( pPayload )
				{
					// Add the new elements to the scene.
					for( const ModelSPtr& pModel : pPayload->spheres )
					{
						data.pSphereSubPass->add( pModel );
					}
					// Add the new elements to the scene.
					for( const ModelSPtr& pModel : pPayload->pbrSpheres )
					{
						data.pPbrSphereSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->planets )
					{
						data.pPlanetSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->moons )
					{
						data.pMoonSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->cubes )
					{
						data.pCubeSubPass->add( pModel );
						data.pGeometryShadowSubPass1->add( pModel );
						data.pGeometryShadowSubPass2->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->tiles )
					{
						data.pTileSubPass->add( pModel );
						data.pShadowSubPass1->add( pModel );
						data.pShadowSubPass2->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->basicObjs )
					{
						data.pBasicObjSubPass->add( pModel );
						data.pShadowSubPass1->add( pModel );
						data.pShadowSubPass2->add( pModel );

						data.pNormalSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->clippedObjs )
					{
						data.pBasicObjSubPass->add( pModel );
						data.pClippedShadowSubPass1->add( pModel );
						data.pClippedShadowSubPass2->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->translucent )
					{
						data.pABufferOITPass->add( pModel );
						data.pTranslucentShadowSubPass1->add( pModel );
						data.pTranslucentShadowSubPass2->add( pModel );

						data.pNormalSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->heightMaps )
					{
						data.pHightMapSubPass->add( pModel );
						data.pShadowSubPass1->add( pModel );
						data.pShadowSubPass2->add( pModel );

						data.pNormalSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->pcgHeightMaps )
					{
						data.pPCGHightMapSubPass->add( pModel );
						data.pPCGShadowSubPass1->add( pModel );
						data.pPCGShadowSubPass2->add( pModel );
						//
						//data.pNormalSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->water )
					{
						data.pWaterSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->pcgWater )
					{
						data.pPCGWaterSubPass->add( pModel );
					}
					for( const ModelSPtr& pModel : pPayload->pcgWaterFollower )
					{
						data.pPCGWaterSubPass->add( pModel );
					}
					for( const ParticleBufferSPtr& pParticleSystem : pPayload->particles )
					{
						data.pParticalSubPass->add( pParticleSystem );
					}

					// Text
					for( const TextSPtr& pText : pPayload->text2D )
					{
						data.pLdrInfoSubPass->add( pText );
					}
					for( const TextSPtr& pText : pPayload->text3D )
					{
						data.pText3DSubPass->add( pText );
						data.pText3DShadowSubPass1->add( pText );
						data.pText3DShadowSubPass2->add( pText );
					}


					// Sky Box
					data.pPolarSky->enableDraw( false );
					data.pCubeSky->enableDraw( false );
					data.pBasicVolumeSky->enableDraw( false );

					if( pPayload->enablePolarSky )
					{
						data.pPolarSky->enableDraw( true );
						data.pPolarSky->setPSResources( pPayload->polarSky );
					}
					else if( pPayload->enableCubeSky )
					{
						data.pCubeSky->enableDraw( true );
						data.pCubeSky->setPSResources( pPayload->cubeSky );
					}
					else if( pPayload->enableBasicVolumeSky )
					{
						data.pBasicVolumeSky->enableDraw( true );
						data.pBasicVolumeSky->setPSResources( pPayload->basicVolumeSky );
					}

					sunDir = acos( pPayload->sunDirection.y );

					// Fog
					data.pPSSunVolumeBuffer->get( ).density = pPayload->density;

					data.pPSSunVolumeBuffer->get( ).heightFalloff = pPayload->heightFalloff;
					data.pPSSunVolumeBuffer->get( ).rayDepth = pPayload->rayDepth;
					data.pPSSunVolumeBuffer->get( ).gScatter = pPayload->gScatter;
					data.pPSSunVolumeBuffer->get( ).waterHeight = pPayload->waterHeight;

					data.pABufferResolveCBuffer->get( ).density = pPayload->density;
					data.pABufferResolveCBuffer->get( ).heightFalloff = pPayload->heightFalloff;


					// Lights
					if( pPayload->hasLights )
					{
						data.clusteredLighting.pLightFrustumCulling->setSRV( 1, pPayload->gpuLights.pSrv );

						data.     pSphereSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.  pPbrSphereSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.  pPbrSphereSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.     pPlanetSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.       pMoonSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.       pCubeSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.       pTileSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.   pBasicObjSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.   pHightMapSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.      pWaterSubPass->setSRV( 7, pPayload->gpuLights.pSrv );
						data.   pPCGWaterSubPass->setSRV( 7, pPayload->gpuLights.pSrv );
						data.    pABufferOITPass->setSRV( 7, pPayload->gpuLights.pSrv );
						data.pPCGHightMapSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
						data.     pText3DSubPass->setSRV( 6, pPayload->gpuLights.pSrv );
					}
					else
					{
						data.clusteredLighting.pLightFrustumCulling->setSRV( 1, data.clusteredLighting.gpuLights.pSrv );

						data.     pSphereSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.  pPbrSphereSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.  pPbrSphereSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.     pPlanetSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.       pMoonSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.       pCubeSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.       pTileSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.   pBasicObjSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.   pHightMapSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.      pWaterSubPass->setSRV( 7, data.clusteredLighting.gpuLights.pSrv );
						data.   pPCGWaterSubPass->setSRV( 7, data.clusteredLighting.gpuLights.pSrv );
						data.    pABufferOITPass->setSRV( 7, data.clusteredLighting.gpuLights.pSrv );
						data.pPCGHightMapSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
						data.     pText3DSubPass->setSRV( 6, data.clusteredLighting.gpuLights.pSrv );
					}
				}

				pLoadedScene = pPayload;
			}
		);
	}

	/// <summary> Change the output target for the scene. </summary>
	/// <param name="pOutputTarget"> The new output target to use for the scene. </param>
	/// <param name="width"> The new target width. </param>
	/// <param name="height"> The new target height. </param>
	void SceneRenderer::setOutputTarget(
		const OutputTargetSPtr& pOutputTarget,
		const float& width,
		const float& height ) const
	{
		data.pBackBufferPass->setOutputTarget( pOutputTarget );
		data.pToneMappingPass->setOutputTarget( pOutputTarget );
		data.pUIPass->setOutputTarget( pOutputTarget );


		// Update constant buffers the need the frame size...
		// Only update them if the draw to render targets change size
		//pPSLightingBuffer->get( ).targetSize.x = 1.0f / width;
		//pPSLightingBuffer->get( ).targetSize.y = 1.0f / height;
	}




	//////////////////////////////////////
	// Getters and Setters for Testing //
	////////////////////////////////////

	void SceneRenderer::setGamma( const float gamma ) const
	{
		data.pPostFXConstants->get( ).gamma = gamma;
	}

	void SceneRenderer::setContrast( const float contrast ) const
	{
		data.pPostFXConstants->get( ).contrast = contrast;
	}

	void SceneRenderer::setSaturation( const float saturation ) const
	{
		data.pPostFXConstants->get( ).saturation = saturation;
	}

	void SceneRenderer::setBrightness( const float brightness ) const
	{
		data.pPostFXConstants->get( ).brightness = brightness;
	}

	void SceneRenderer::setSunDir( const float theta )
	{
		sunDir = theta;
	}

	float SceneRenderer::getGamma( ) const
	{
		return data.pPostFXConstants->get( ).gamma;
	}

	float SceneRenderer::getContrast( ) const
	{
		return data.pPostFXConstants->get( ).contrast;
	}

	float SceneRenderer::getSaturation( ) const
	{
		return max( data.pPostFXConstants->get( ).saturation, -1.0f );
	}

	float SceneRenderer::getBrightness( ) const
	{
		return data.pPostFXConstants->get( ).brightness;
	}

	float SceneRenderer::getSunDir( ) const
	{
		return sunDir;
	}

	void SceneRenderer::toggleNormals( ) const
	{
		data.pToggles->enableDebugNormals = !data.pToggles->enableDebugNormals;
	}

	void SceneRenderer::toggleFPS( )
	{
		data.pToggles->enableDebugFPS = !data.pToggles->enableDebugFPS;
	}

	void SceneRenderer::toggleTimes( )
	{
		data.pToggles->enableDebugTimes = !data.pToggles->enableDebugTimes;
	}













	/// <summary> Calculate the view and projections for each cascade shadow map. </summary>
	/// <param name="direction"> The light source direction. </param>
	/// <param name="depth"> The depth of the shaow map (far plane). </param>
	/// <param name="rotation"> The light sources orientation. </param>
	vector< SceneRenderer::ShadowCascade > SceneRenderer::calculateShadowCascadeProjection(
		const vec3& direction,
		const float depth,
		const mat4x4& rotation ) const
	{
		vector< ShadowCascade > cascades;
		ShadowCascade cascade = {};

		const vec3 nEye( 0, 0, 0 );
		const vec3 nAt( -direction.x, -direction.y, -direction.z );
		vec3 nUp( 0, 1, 0 );

		//nAt = ( rotation * vec4( nAt.x, nAt.y, nAt.z, 0 ) );
		nUp = ( rotation * vec4( nUp.x, nUp.y, nUp.z, 0 ) );

		cascade.view = lookAtLH( nEye, nAt, nUp );

		const mat4x4 camInv = inverse( pCamera->getView( ) );

		const vec2 size = pCamera->getSize( 0 );
		float aspect = size.y / size.x;

		const float fovH = pCamera->getFOV( 0 ) * 0.5f;
		//const float fovV = fovH;
		const float fovV = fovH * aspect;

		const float fovHRad = radians( fovH );
		const float fovVRad = radians( fovV );

		const float tanHalfFovH = tanf( fovHRad );
		const float tanHalfFovV = tanf( fovVRad );

		const float nearPlane = pCamera->getNearPlane( 0 );
		const float  farPlane = pCamera->getFarPlane( 0 );

		const float depths[ ] = { nearPlane, 32, 96 };
		const float shadowSize[ ] = { 4096.0f, 2048.0f };

		for( int i = 0; i < 2; i++ )
		{
			const vec2 xPoints = vec2( nearPlane, depths[ i + 1 ] ) * tanHalfFovH;
			const vec2 yPoints = vec2( nearPlane, depths[ i + 1 ] ) * tanHalfFovV;

			vector< vec4 > frustumCorners = {

				{  xPoints.x,  yPoints.x, depths[ i ], 1.0f },
				{ -xPoints.x,  yPoints.x, depths[ i ], 1.0f },
				{  xPoints.x, -yPoints.x, depths[ i ], 1.0f },
				{ -xPoints.x, -yPoints.x, depths[ i ], 1.0f },

				{  xPoints.y,  yPoints.y, depths[ i + 1 ], 1.0f },
				{ -xPoints.y,  yPoints.y, depths[ i + 1 ], 1.0f },
				{  xPoints.y, -yPoints.y, depths[ i + 1 ], 1.0f },
				{ -xPoints.y, -yPoints.y, depths[ i + 1 ], 1.0f }
			};

			float minX = numeric_limits< float >::max( );
			float maxX = numeric_limits< float >::lowest( );

			float minY = numeric_limits< float >::max( );
			float maxY = numeric_limits< float >::lowest( );

			float minZ = numeric_limits< float >::max( );
			float maxZ = numeric_limits< float >::lowest( );


			vector< vec4 > frustumCornersWS;

			for( vec4& corner : frustumCorners )
			{
				const vec4 vWS = camInv * corner;
				frustumCornersWS.push_back( vWS );

				corner = cascade.view * vWS;

				minX = min( minX, corner.x );
				maxX = max( maxX, corner.x );

				minY = min( minY, corner.y );
				maxY = max( maxY, corner.y );

				minZ = min( minZ, corner.z );
				maxZ = max( maxZ, corner.z );
			}

			/**/

			//float diagonalLength = abs( frustumCornersWS[ 0 ].x - frustumCornersWS[ 1 ].x );
			float diagonalLength = length( frustumCornersWS[ 7 ] - frustumCornersWS[ 0 ] );
			diagonalLength += 2;
			float pixSize = diagonalLength / shadowSize[ i ] * 2;

			vec3 vBorderOffset = vec3( diagonalLength, diagonalLength, diagonalLength ) - abs( vec3( maxX, maxY, maxZ ) - vec3( minX, minY, minZ ) ) * 0.5f;

			maxX += vBorderOffset.x;
			maxY += vBorderOffset.y;
			maxZ += vBorderOffset.z;

			minX -= vBorderOffset.x;
			minY -= vBorderOffset.y;
			minZ -= vBorderOffset.z;

			float pixelSizeX = pixSize;
			float pixelSizeY = pixSize;

			//float pixelSizeX = abs( right - left ) / ( shadowSize[ i ] ) * 0.5f;
			//float pixelSizeY = abs( top - bottom ) / ( shadowSize[ i ] ) * 0.5f;
			//float pixelSizeX = ( depths[ i + 1 ] - depths[ i ] ) * 2 / shadowSize[ i ];
			//float pixelSizeY = ( depths[ i + 1 ] - depths[ i ] ) * 2 / shadowSize[ i ];
			//float pixelSizeX = ( depths[ i + 1 ] - depths[ i ] ) * 2 / ( shadowSize[ i ] + 1 );
			//float pixelSizeY = ( depths[ i + 1 ] - depths[ i ] ) * 2 / ( shadowSize[ i ] + 1 );

			maxX /= pixelSizeX;
			maxX = floor( maxX );
			maxX *= pixelSizeX;

			minX /= pixelSizeX;
			minX  = floor( minX );
			minX *= pixelSizeX;


			minY /= pixelSizeY;
			minY = floor( minY );
			minY *= pixelSizeY;

			maxY /= pixelSizeY;
			maxY = floor( maxY );
			maxY *= pixelSizeY;


			maxZ /= pixSize;
			maxZ = floor( maxZ );
			maxZ *= pixSize;

			minZ /= pixSize;
			minZ = floor( minZ );
			minZ *= pixSize;
			//*/





			//float sizeScale = max( pCamera->getEye( ).y * 0.5f - 10.0f, 0.0f );
			const float sizeScale = 0.0f;

			const float right = maxX + sizeScale;
			const float left = minX - sizeScale;

			const float bottom = minY - sizeScale;
			const float top = maxY + sizeScale;

			const mat4x4 projection = orthoLH( left, right, bottom, top, minZ - depth, maxZ + depth );

			cascade.viewProjection = projection * cascade.view;
			cascade.viewProjection = transpose( cascade.viewProjection );

			cascades.push_back( cascade );
		}

		return cascades;
	}
} // namespace directX11
} // namespace visual

