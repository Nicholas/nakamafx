#include "CommandDispatch.h"
// Controls the execute api commands to the GPU.
//
// Project   : NaKama-Fx
// File Name : CommandDispatch.cpp
// Date      : 15/02/2017
// Author    : Nicholas Welters

#include "Visual/FX/SwapChain.h"

namespace visual
{
namespace directX11
{
	using fx::SwapChainSPtr;

	/// <summary> ctor </summary>
	/// <param name="pContext"> The d3d11 device context. </param>
	/// <param name="pRenderingCommands"> A list of priority queues of command buffers created by an indirect context. </param>
	/// <param name="pAsyncEventBuffer"> A list of events that might need to run only once with the device context . </param>
	CommandDispatch::CommandDispatch(
		const DeviceContextPtr_t& pContext,
		const BufferedCommandQueueSPtr& pRenderingCommands,
		const AsyncEventBufferSPtr& pAsyncEventBuffer )
		: fx::CommandDispatch( )
		, pContext( pContext )
		, pRenderingCommands( pRenderingCommands )
		, pAsyncEventBuffer( pAsyncEventBuffer )
		, swapchains( )
	{ }

	/// <summary>
	/// Reset the command allocator memory so that we can reuse it to record new commands.
	///	This should only be called after we know that the commands in the allocator have all
	///	finished executing. see FlushCommandQueue( ) which uses a fence to sync the GPU and CPU
	/// </summary>
	void CommandDispatch::resetCommandAllocators( )
	{ }

	/// <summary>	Send the command lists to the GPU for execution. </summary>
	void CommandDispatch::executeCommandList( )
	{
		for( const AsyncEvent& action : pAsyncEventBuffer->readAll( ) )
		{
			action( pContext );
		}

		if( pRenderingCommands->size( ) > 0 )
		{
			CommandQueue commands = pRenderingCommands->front( );
			pRenderingCommands->pop( );
			while( !commands.empty( ) )
			{
				PriorityCommandList commandList = commands.top( );
				commands.pop( );

				pContext->ExecuteCommandList( commandList.pCommandList, FALSE );
				commandList.pCommandList->Release( );
			}

		}

		for( const SwapChainSPtr& pSwapChain : swapchains )
		{
			pSwapChain->present( );
		}
	}

	/// <summary>
	/// Wait for the last set of command list to complete execution.
	///	This is to make sure that we can sync the GPU and the CPU.
	///
	///	Use this to make sure the command queue is empty.
	///	This doesn't mean the list has been executed, only that
	///	the command queue has completed what ever was scheduled
	///	before the fence.
	/// </summary>
	void CommandDispatch::flushCommandQueue( )
	{ }

	/// <summary>
	/// Here we are going to give the executor the swapchain so that it can
	///	schedule the call to present. Making sure that the present event
	/// doesn't execute until the rendering commands have
	///	finished.
	/// </summary>
	/// <param name="swapchain">
	/// A swap chain to add to the system so that we call present after
	/// executing all out commands for the frame.
	/// </param>
	void CommandDispatch::addSwapChain( const SwapChainSPtr& swapchain )
	{
		swapchains.emplace_back( swapchain );
	}

	/// <summary>
	/// Remove the current swap chain. This allows us to
	/// destroy the swapchain and create a new one.
	/// </summary>
	void CommandDispatch::clearSwapChain( )
	{
		swapchains.clear( );
	}

	/// <summary>
	/// Get the command list buffer so that we can send commands
	/// to it from a pass that uses an indirect context.
	/// </summary>
	/// <returns> A list of priority queues of command buffers created by an indirect context. </returns>
	BufferedCommandQueueSPtr CommandDispatch::getBufferedCommandQueue( ) const
	{
		return pRenderingCommands;
	}
} // namespace directX11
} // namespace visual
