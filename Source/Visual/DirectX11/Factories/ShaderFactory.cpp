#include "ShaderFactory.h"

// Shader Program factory so that we can instruct the GPU how to process our data,
// 
// Project   : NaKama-Fx
// File Name : ShaderFactory.cpp
// Date      : 19/06/2015
// Author    : Nicholas Welters

#include "Visual/DirectX11/ResourceReader.h"

#include <d3d11.h>
#include <d3dcompiler.h>

namespace visual
{
namespace directX11
{
	using std::cout;
	using std::endl;
	using std::to_string;
	using std::string;
	using std::pair;
	using std::shared_ptr;
	using std::exception;
	
	/// <summary> copy ctor </summary>
	/// <param name="pDevice"> The device connection to our gpu so that we can wrapp the shader construction here. </param>
	ShaderFactory::ShaderFactory( _In_ const ID3D11DeviceCPtr& pDevice )
	: pDevice( pDevice )
	,   vertexShaderLibrary( ID3D11_FACTORY( ID3D11VertexShader   ), DX11_DELETER )
	,    pixelShaderLibrary( ID3D11_FACTORY( ID3D11PixelShader    ), DX11_DELETER )
	, geometryShaderLibrary( ID3D11_FACTORY( ID3D11GeometryShader ), DX11_DELETER )
	,     hullShaderLibrary( ID3D11_FACTORY( ID3D11HullShader     ), DX11_DELETER )
	,   domainShaderLibrary( ID3D11_FACTORY( ID3D11DomainShader   ), DX11_DELETER )
	,  computeShaderLibrary( ID3D11_FACTORY( ID3D11ComputeShader  ), DX11_DELETER )
	{
	}
	


	// +----------------------+
	// | Shader from resource |
	// +----------------------+
	
	/// <summary> Load a compiled vertex shader from the resource id </summary>
	/// <param name="id"> The vertex shader resource id. </param>
	/// <returns> The vertex shader. </returns>
	ID3D11VertexShaderSPtr ShaderFactory::getVertexShader( _In_ const int id )
	{
		const string name = "Vertex Shader [" + to_string( id ) + "]";

		const ID3D11VertexShaderFactory factory = [ this, id, name ]( ) -> ID3D11VertexShader*
		{
			const pair< uint32_t*, size_t > compiledShader = ResourceReader::dataFromResource( id );

			ID3D11VertexShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateVertexShader(
				compiledShader.first,
				compiledShader.second,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateVertexShader()" << endl;
			}

			setDebugName( pShader, name );

			return pShader;
		};
		return vertexShaderLibrary.checkOut( name, factory );
	}
	
	/// <summary> Load a compiled pixel shader from the resource id </summary>
	/// <param name="id"> The pixel shader resource id. </param>
	/// <returns> The pixel shader. </returns>
	ID3D11PixelShaderSPtr ShaderFactory::getPixelShader( _In_ const int id )
	{
		const string name = "Pixel Shader [" + to_string( id ) + "]";

		const ID3D11PixelShaderFactory factory = [ this, id, name ]( ) -> ID3D11PixelShader*
		{
			const pair< uint32_t*, size_t > compiledShader = ResourceReader::dataFromResource( id );

			ID3D11PixelShader* pShader = nullptr;
			const HRESULT result = pDevice->CreatePixelShader(
				compiledShader.first,
				compiledShader.second,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreatePixelShader()" << endl;
			}

			setDebugName( pShader, name );

			return pShader;
		};
		return pixelShaderLibrary.checkOut( name, factory );
	}
	
	/// <summary> Load a compiled geometry shader from the resource id </summary>
	/// <param name="id"> The geometry shader resource id. </param>
	/// <returns> The geometry shader. </returns>
	ID3D11GeometryShaderSPtr ShaderFactory::getGeometryShader( _In_ const int id )
	{
		const string name = "Geometry Shader [" + to_string( id ) + "]";

		const ID3D11GeometryShaderFactory factory = [ this, id, name ]( ) -> ID3D11GeometryShader*
		{
			const pair< uint32_t*, size_t > compiledShader = ResourceReader::dataFromResource( id );

			ID3D11GeometryShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateGeometryShader(
				compiledShader.first,
				compiledShader.second,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateGeometryShader()" << endl;
			}

			setDebugName( pShader, name );

			return pShader;
		};
		return geometryShaderLibrary.checkOut( name, factory );
	}
	
	/// <summary> Load a compiled geometry shader from the resource id </summary>
	/// <param name="id"> The geometry shader resource id. </param>
	/// <param name="layout"> The geometry shader streaming out structure. </param>
	/// <param name="size"> The number of elements in the layout. </param>
	/// <returns> The geometry shader. </returns>
	ID3D11GeometryShaderSPtr ShaderFactory::getGeometryShaderOS(
		_In_ const int id,
		_In_ const D3D11_SO_DECLARATION_ENTRY* layout,
		_In_ const int size )
	{
		const string name = "Geometry Shader OS [" + to_string( id ) + "]";

		const ID3D11GeometryShaderFactory factory = [ this, id, name, layout, size ]( ) -> ID3D11GeometryShader*
		{
			const pair< uint32_t*, size_t > compiledShader = ResourceReader::dataFromResource( id );

			// Create the vertex shader from the buffer.
			ID3D11GeometryShader* pShader = nullptr;

			const HRESULT result = pDevice->CreateGeometryShaderWithStreamOutput(
				compiledShader.first,
				compiledShader.second,
				layout,
				size,
				nullptr,
				0,
				0,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateGeometryShaderWithStreamOutput()" << endl;
			}

			setDebugName( pShader, name );

			return pShader;
		};
		return geometryShaderLibrary.checkOut( name, factory );
	}
	
	/// <summary> Load a compiled hull shader from the resource id </summary>
	/// <param name="id"> The hull shader resource id. </param>
	/// <returns> The hull shader. </returns>
	ID3D11HullShaderSPtr ShaderFactory::getHullShader( _In_ const int id )
	{
		const string name = "Hull Shader [" + to_string( id ) + "]";

		const ID3D11HullShaderFactory factory = [ this, id, name ]( ) -> ID3D11HullShader*
		{
			const pair< uint32_t*, size_t > compiledShader = ResourceReader::dataFromResource( id );

			ID3D11HullShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateHullShader(
				compiledShader.first,
				compiledShader.second,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateHullShader()" << endl;
			}

			setDebugName( pShader, name );

			return pShader;
		};
		return hullShaderLibrary.checkOut( name, factory );
	}
	
	/// <summary> Load a compiled domain shader from the resource id </summary>
	/// <param name="id"> The domain shader resource id. </param>
	/// <returns> The domain shader. </returns>
	ID3D11DomainShaderSPtr ShaderFactory::getDomainShader( _In_ const int id )
	{
		const string name = "Domain Shader [" + to_string( id ) + "]";

		const ID3D11DomainShaderFactory factory = [ this, id, name ]( ) -> ID3D11DomainShader*
		{
			const pair< uint32_t*, size_t > compiledShader = ResourceReader::dataFromResource( id );

			ID3D11DomainShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateDomainShader(
				compiledShader.first,
				compiledShader.second,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateDomainShader()" << endl;
			}

			setDebugName( pShader, name );

			return pShader;
		};
		return domainShaderLibrary.checkOut( name, factory );
	}
	
	/// <summary> Load a compiled compute shader from the resource id </summary>
	/// <param name="id"> The compute shader resource id. </param>
	/// <returns> The compute shader. </returns>
	ID3D11ComputeShaderSPtr ShaderFactory::getComputeShader( _In_ const int id )
	{
		const string name = "Compute Shader [" + to_string( id ) + "]";

		const ID3D11ComputeShaderFactory factory = [ this, id, name ]( ) -> ID3D11ComputeShader*
		{
			const pair< uint32_t*, size_t > compiledShader = ResourceReader::dataFromResource( id );

			ID3D11ComputeShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateComputeShader(
				compiledShader.first,
				compiledShader.second,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateComputeShader()" << endl;
			}

			setDebugName( pShader, name );

			return pShader;
		};
		return computeShaderLibrary.checkOut( name, factory );
	}

	

	// +------------------+
	// | Shader from file |
	// +------------------+
	
	/// <summary> Compile and load a vertex shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <returns> The compiled shader. </returns>
	ID3D11VertexShaderSPtr ShaderFactory::getVertexShader(
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel )
	{
		const ID3D11VertexShaderFactory factory = [ this, fileName, entryPoint, shaderModel ]( ) -> ID3D11VertexShader*
		{
			// -----------------------------------------------------------------------------------
			// Compile the Shader
			// -----------------------------------------------------------------------------------
			ID3DBlob* pCompiledShader = compileFromFile( fileName, entryPoint, shaderModel );
			
			// -----------------------------------------------------------------------------------
			// Create the vertex shader from the buffer.
			// -----------------------------------------------------------------------------------
			ID3D11VertexShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateVertexShader(
				pCompiledShader->GetBufferPointer(),
				pCompiledShader->GetBufferSize(),
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateVertexShader()" << endl;
			}

			setDebugName( pShader, fileName );

			pCompiledShader->Release();
			pCompiledShader = nullptr;

			return pShader;
		};
		return vertexShaderLibrary.checkOut( fileName + entryPoint, factory );
	}
	
	/// <summary> Compile and load a pixel shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <returns> The compiled shader. </returns>
	ID3D11PixelShaderSPtr ShaderFactory::getPixelShader(
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel )
	{
		const ID3D11PixelShaderFactory factory = [ this, fileName, entryPoint, shaderModel ]( ) -> ID3D11PixelShader*
		{
			// -----------------------------------------------------------------------------------
			// Compile the Shader
			// -----------------------------------------------------------------------------------
			ID3DBlob* pCompiledShader = compileFromFile( fileName, entryPoint, shaderModel );
				
			// -----------------------------------------------------------------------------------
			// Create the pixel shader from the buffer.
			// -----------------------------------------------------------------------------------
			ID3D11PixelShader* pShader = nullptr;
			const HRESULT result = pDevice->CreatePixelShader(
				pCompiledShader->GetBufferPointer(),
				pCompiledShader->GetBufferSize(),
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreatePixelShader()" << endl;
			}

			setDebugName( pShader, fileName );

			pCompiledShader->Release();
			pCompiledShader = nullptr;

			return pShader;
		};
		return pixelShaderLibrary.checkOut( fileName + entryPoint, factory );
	}
	
	/// <summary> Compile and load a geometry shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <returns> The compiled shader. </returns>
	ID3D11GeometryShaderSPtr ShaderFactory::getGeometryShader(
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel )
	{
		const ID3D11GeometryShaderFactory factory = [ this, fileName, entryPoint, shaderModel ]( ) -> ID3D11GeometryShader*
		{
			// -----------------------------------------------------------------------------------
			// Compile the Shader
			// -----------------------------------------------------------------------------------
			ID3DBlob* pCompiledShader = compileFromFile( fileName, entryPoint, shaderModel );
			
			// Create the vertex shader from the buffer.
			ID3D11GeometryShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateGeometryShader(
				pCompiledShader->GetBufferPointer(),
				pCompiledShader->GetBufferSize(),
				nullptr,
				&pShader
			);
			
			if( FAILED( result ) )
			{
				cout << "Failed -> CreateGeometryShader()" << endl;
			}

			setDebugName( pShader, fileName );

			pCompiledShader->Release();
			pCompiledShader = nullptr;
			
			return pShader;
		};
		return geometryShaderLibrary.checkOut( fileName + entryPoint, factory );
	}
	
	/// <summary> Compile and load a geometry shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <param name="layout"> The geometry shader streaming out structure. </param>
	/// <param name="size"> The number of elements in the layout. </param>
	/// <returns> The compiled shader. </returns>
	ID3D11GeometryShaderSPtr ShaderFactory::getGeometryShaderOS( 
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel,
		_In_ const D3D11_SO_DECLARATION_ENTRY * layout,
		_In_ const int size )
	{
		const ID3D11GeometryShaderFactory factory = [ this, fileName, entryPoint, shaderModel, layout, size ]( ) -> ID3D11GeometryShader*
		{
			// -----------------------------------------------------------------------------------
			// Compile the Shader
			// -----------------------------------------------------------------------------------
			ID3DBlob* pCompiledShader = compileFromFile( fileName, entryPoint, shaderModel );

			// Create the vertex shader from the buffer.
			ID3D11GeometryShader* pShader = nullptr;

			const HRESULT result = pDevice->CreateGeometryShaderWithStreamOutput(
				pCompiledShader->GetBufferPointer( ),
				pCompiledShader->GetBufferSize( ),
				layout,
				size,
				nullptr,
				0,
				0,
				nullptr,
				&pShader
			);

			if( FAILED( result ) )
			{
				cout << "Failed -> CreateGeometryShaderWithStreamOutput()" << endl;
			}

			setDebugName( pShader, fileName );

			pCompiledShader->Release( );
			pCompiledShader = nullptr;

			return pShader;
		};
		return geometryShaderLibrary.checkOut( fileName + entryPoint, factory );
	}
	
	/// <summary> Compile and load a hull shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <returns> The compiled shader. </returns>
	ID3D11HullShaderSPtr ShaderFactory::getHullShader(
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel )
	{
		const ID3D11HullShaderFactory factory = [ this, fileName, entryPoint, shaderModel ]( ) -> ID3D11HullShader*
		{
			// -----------------------------------------------------------------------------------
			// Compile the Shader
			// -----------------------------------------------------------------------------------
			ID3DBlob* pCompiledShader = compileFromFile( fileName, entryPoint, shaderModel );
			
			// -----------------------------------------------------------------------------------
			// Create the hull shader from the buffer.
			// -----------------------------------------------------------------------------------
			ID3D11HullShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateHullShader(
				pCompiledShader->GetBufferPointer(),
				pCompiledShader->GetBufferSize(),
				nullptr,
				&pShader
			);
			
			if( FAILED( result ) )
			{
				cout << "Failed -> CreateHullShader()" << endl;
			}

			setDebugName( pShader, fileName );

			pCompiledShader->Release();
			pCompiledShader = nullptr;
			
			return pShader;
		};
		return hullShaderLibrary.checkOut( fileName + entryPoint, factory );
	}
	
	/// <summary> Compile and load a domain shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <returns> The compiled shader. </returns>
	ID3D11DomainShaderSPtr ShaderFactory::getDomainShader(
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel )
	{
		const ID3D11DomainShaderFactory factory = [ this, fileName, entryPoint, shaderModel ]( ) -> ID3D11DomainShader*
		{
			// -----------------------------------------------------------------------------------
			// Compile the Shader
			// -----------------------------------------------------------------------------------
			ID3DBlob* pCompiledShader = compileFromFile( fileName, entryPoint, shaderModel );
			
			// -----------------------------------------------------------------------------------
			// Create the domain shader from the buffer.
			// -----------------------------------------------------------------------------------
			ID3D11DomainShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateDomainShader(
				pCompiledShader->GetBufferPointer(),
				pCompiledShader->GetBufferSize(),
				nullptr,
				&pShader
			);
			
			if( FAILED( result ) )
			{
				cout << "Failed -> CreateDomainShader()" << endl;
			}

			setDebugName( pShader, fileName );

			pCompiledShader->Release();
			pCompiledShader = nullptr;
			
			return pShader;
		};
		return domainShaderLibrary.checkOut( fileName + entryPoint, factory );
	}
	
	/// <summary> Compile and load a compute shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <returns> The compiled shader. </returns>
	ID3D11ComputeShaderSPtr ShaderFactory::getComputeShader(
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel )
	{
		const ID3D11ComputeShaderFactory factory = [ this, fileName, entryPoint, shaderModel ]( ) -> ID3D11ComputeShader*
		{
			// -----------------------------------------------------------------------------------
			// Compile the Shader
			// -----------------------------------------------------------------------------------
			ID3DBlob* pCompiledShader = compileFromFile( fileName, entryPoint, shaderModel );
			
			// -----------------------------------------------------------------------------------
			// Create the compute shader from the buffer.
			// -----------------------------------------------------------------------------------
			ID3D11ComputeShader* pShader = nullptr;
			const HRESULT result = pDevice->CreateComputeShader(
				pCompiledShader->GetBufferPointer( ),
				pCompiledShader->GetBufferSize( ),
				nullptr,
				&pShader
			);
			
			if( FAILED( result ) )
			{
				cout << "Failed -> CreateComputeShader()" << endl;
			}

			setDebugName( pShader, fileName );

			pCompiledShader->Release( );
			pCompiledShader = nullptr;
			
			return pShader;
		};
		return computeShaderLibrary.checkOut( fileName + entryPoint, factory );
	}
	
	/// <summary> Compile a vertex shader that is defined in the vertex header. </summary>
	/// <param name="vertex"> The vertex string id/name. </param>
	/// <param name="shaderCode"> The vertexs simple shader. </param>
	/// <returns> The compiled shader. </returns>
	ID3DBlob* ShaderFactory::compileVertexShaderWithInputSignature(
		_In_ const string& vertex,
		_In_ const string& shaderCode )
	{
		ID3DBlob* error = nullptr;
		ID3DBlob* pCompiledShader = nullptr;

		//////////////////////////////////////
		// Compile the vertex shader code. //
		////////////////////////////////////
		const HRESULT result = D3DCompile(
			shaderCode.c_str( ),
			shaderCode.size( ),
			vertex.c_str( ),
			nullptr,
			nullptr,
			"main",
			"vs_5_0",
			D3DCOMPILE_ENABLE_STRICTNESS,
			0,
			&pCompiledShader,
			&error
		);

		if( FAILED( result ) )
		{
			if( error )
			{
				blobErrorMessage( vertex, error );
				throw DxException( result, "D3DCompile - Compilation Failed", vertex, __LINE__ );
			}
			// If there was nothing in the error message then it simply could not find the shader file itself.
			else
			{
				throw DxException( result, "D3DCompile - Unknown Error:", vertex, __LINE__ );
			}
		}

		return pCompiledShader;
	}
	
	/// <summary> Compile and a shader from file. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="entryPoint"> The shaders entry point name. </param>
	/// <param name="shaderModel"> The shaders model. </param>
	/// <returns> The compiled shader blob data. </returns>
	ID3DBlob* ShaderFactory::compileFromFile(
		_In_ const string& fileName,
		_In_ const string& entryPoint,
		_In_ const string& shaderModel ) const
	{
		ID3DBlob * error = nullptr;
	
		ID3DBlob* pCompiledShader = nullptr;

		size_t converted = 0;
		const size_t newSize = fileName.length() + 1;
		wchar_t wideCharString[4092];
		mbstowcs_s(&converted, &wideCharString[0], newSize, fileName.c_str(), fileName.length());

		bool built = false;

		while( !built )
		{
			const HRESULT result = D3DCompileFromFile(
				wideCharString,
				nullptr,
				D3D_COMPILE_STANDARD_FILE_INCLUDE,
				entryPoint.c_str( ),
				shaderModel.c_str( ),
				D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG,
				0,
				&pCompiledShader,
				&error
			);

			if( FAILED( result ) )
			{
				// If the shader failed to compile it should have written something to the error message.
				if( error )
				{
					blobErrorMessage( fileName, error );
					throw DxException( result, "D3DCompileFromFile( " + fileName + " )", __FILE__, __LINE__ );
				}
				// If there was nothing in the error message then it simply could not find the shader file itself.
				else
				{
					cout << "Failed :: D3DCompileFromFile( " << fileName << " ) - Missing Shader File" << endl;
					throw DxException( result, "D3DCompileFromFile( " + fileName + " ) - File not found!", __FILE__, __LINE__ );
				}
			}
			else if( error )
			{
				blobErrorMessage( fileName, error, false );
				built = true;
			}
			else
			{
				built = true;
			}
		}

		return pCompiledShader;
	}
	
	/// <summary> Prints out the contents of the shader compilation result blob. </summary>
	/// <param name="fileName"> The shaders file path. </param>
	/// <param name="pError"> The compilation message blob. </param>
	/// <param name="isError"> A flag used to identify warnings vs error messages. </param>
	void ShaderFactory::blobErrorMessage(
		_In_ const string& fileName,
		_In_ ID3DBlob* pError,
		_In_ const bool isError )
	{
		string errorString;

		// Get a pointer to the error message text buffer.
		const char* compileErrors = static_cast< char* >( pError->GetBufferPointer( ) );

		// Get the length of the message.
		const size_t bufferSize = pError->GetBufferSize( );

		# ifdef _TEXT_OUT_ERRORS_
			ofstream fout;
			// Open a file to write the error message to.
			fout.open("Shader_Error.txt");
		# endif

		// Write out the error message.
		for( size_t i = 0; i < bufferSize; ++i )
		{
			# ifdef _TEXT_OUT_ERRORS_
				fout << compileErrors[ i ];
			# endif

			errorString += compileErrors[ i ];
		}
	
		# ifdef _TEXT_OUT_ERRORS_
			// Close the file.
			fout.close();
		# endif

		if( isError )
		{
			// Pop a message up on the screen to notify the user to check the text file for compile errors.
			cout << "#######################################\n" << endl;
			cout << fileName << " :: Compilation Error Message\n\n" << errorString << endl;
		}
		else
		{
			cout << "---------------------------------------\n" << endl;
			cout << fileName << " :: Compilation Warning Message\n\n" << errorString << endl;
		}
	}
} // namespace directX11
} // namespace visual
