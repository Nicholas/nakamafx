#pragma once
// DirectX 11 Device factory.
//
// Creates our DirectX 11 instance and the link to the GPU that we are going to use for rendering.
//
// Project   : NaKama-Fx
// File Name : DeviceFactory.h
// Date      : 07/12/2016
// Author    : Nicholas Welters

#ifndef _DEVICE_FACTORY_DX11_H
#define _DEVICE_FACTORY_DX11_H

#include "../APITypeDefs.h"

namespace visual
{
namespace directX11
{
	class DeviceFactory
	{
	public:
		/// <summary> ctor </summary>
		DeviceFactory( );

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DeviceFactory( const DeviceFactory& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DeviceFactory( DeviceFactory&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~DeviceFactory( );


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		DeviceFactory& operator=( const DeviceFactory& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		DeviceFactory& operator=( DeviceFactory&& move ) = default;




		/// <summary>
		/// Create our API connection to the GPU so that we can modify data
		/// on the GPU with the device and send commands using the context.
		/// </summary>
		void initialiseDevice( _Out_ DevicePackage& package );

		/// <summary> The  older APIs are going to make getting individual
		///			  gpu information hard but its ok as I should only need
		///			  that once we use DX12 or Vulkan. Thus this is here as
		///			  a nice to have thing.
		/// </summary>
		Features getGpuInfo( _In_ const ID3D11DeviceCPtr& pDevice ) const;

		/// <summary> Create a DXGI swap-chain for a DX12 device to a win32 window.
		/// </summary>
		/// <param name="hwnd">The window the swap chain displays to.</param>
		/// <param name="features">The features struct to see if we should allow tearing.</param>
		/// <param name="pDevice">The device we will link the swap chain to.</param>
		/// <returns> A IDXGISwapChain4Ptr linked to a DX12 device and its resources. </returns>
		SwapChainPtr_t createSwapChain(
			_In_ HWND* hwnd,
			_In_ uint32_t bufferCount,
			_In_ const Features& features,
			_In_ const ID3D11DeviceCPtr& pDevice
		) const;

	private:
		/// <summary>
		/// Here we are going to create an instance.
		/// Then select a GPU and create a connection to it.
		/// </summary>
		void initialise( );

		/// <summary> Create the DXGI factory to get our adapters. </summary>
		void initialiseFactory( );

		/// <summary> Create the DXGI adapter to create our device. </summary>
		void initialiseAdapter( );

		/// <summary> Print out the provided adaptors information. </summary>
		void dumpAdapterInfo( _In_ const AdapterPtr_t& pAdapter, _In_opt_ bool showResolutions = false ) const;

		/// <summary> Helper for DumpGPU so that we can get the supported texture formats. </summary>
		static void dumpTextureSupport( _In_ const ID3D11DeviceCPtr& pDevice );


	private:
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Fields
		////////////////////////////////////////////////////////////////////////////////////////////////////
		ID3D11DebugCPtr pDebug;

		FactoryPtr_t pFactory;
		AdapterPtr_t pAdapter;
	};
} // namespace directX11
} // namespace visual

#endif // _DEVICE_FACTORY_DX11_H
