#include "FontFactory.h"

// DirectX 11 Font factory.
//
// Loads in the information needed to render a font.
// This uses msdfgen to generate msfda textures.
//
// Project   : NaKama-Fx
// File Name : FontFactory.h
// Date      : 29/05/2019
// Author    : Nicholas Welters

#include "../Types/Font.h"
#include "ImageFactory.h"
#include "Visual/DirectX11/APITypeDefs.h"

#include <msdfgen.h>
#include <ext/import-font.h>


namespace visual
{
namespace directX11
{
	using std::make_shared;
	using std::to_string;
	using std::vector;
	using std::string;
	using std::numeric_limits;

	using glm::vec2;
	using glm::vec4;
	using glm::uvec3;
	using glm::uvec4;
	using glm::clamp;

	using msdfgen::FreetypeHandle;
	using msdfgen::FontHandle;
	using msdfgen::Vector2;
	using msdfgen::Shape;
	using msdfgen::Bitmap;
	using msdfgen::initializeFreetype;
	using msdfgen::loadFont;
	using msdfgen::getFontScale;
	using msdfgen::getFontWhitespaceWidth;
	using msdfgen::loadGlyph;
	using msdfgen::edgeColoringSimple;
	using msdfgen::generateMSDF;
	using msdfgen::generateSDF;
	using msdfgen::getKerning;
	using msdfgen::destroyFont;
	using msdfgen::deinitializeFreetype;

	/// <summary> ctor </summary>
	/// <param name="pDevice"> Our GPU device. </param>
	/// <param name="pImageFactory"> The image factory to manage the texture data structures. </param>
	FontFactory::FontFactory(
		_In_ const ID3D11DeviceCPtr& pDevice,
		_In_ const  ImageFactorySPtr&  pImageFactory )
		: pDevice( pDevice )
		, pImageFactory( pImageFactory )
		, fontLibrary( [ ]( FontBuilder& build ){ return build( ); }, []( Font* pFont ){ SAFE_DELETE( pFont ); } )
	{ }



	/// <summary> Create render targets so that we can collect colour information. </summary>
	/// <param name="pSwapChain"> Hardware swapchain to create a render target view for. </param>
	FontSPtr FontFactory::createFont( _In_ const string& font )
	{
		return fontLibrary.checkOut( font, [ this, font ]( )->Font*
		{
			const string charList = "!\"#$%&'()*+,-./0123456789:;,=.?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

			const uint32_t size = 32;
			const uvec3 glyphTextureSize( size, size, charList.size( ) );

			FreetypeHandle* ft = initializeFreetype( );

			if( !ft )
			{
				return nullptr;
			}

			Font* pFont = new Font;

			pFont->name = font;

			FontHandle* fontHandle = loadFont( ft, pFont->name.c_str( ) );

			if( ! fontHandle )
			{
				return nullptr;
			}
			vector< vector< uint32_t > > superMSDF( glyphTextureSize.z );

			double dFontScale = 0.0;
			double fontSpaceWidth = 0.0f;
			double fontTabWidth = 0.0f;

			const double range = 6.0;
			const vec2 scale( 0.8, 0.8 );

			getFontScale( dFontScale, fontHandle );
			getFontWhitespaceWidth( fontSpaceWidth, fontTabWidth, fontHandle );

			const Vector2 realScale( scale.x * ( glyphTextureSize.x / dFontScale )
									, scale.y * ( glyphTextureSize.y / dFontScale ) );

			pFont->size = int( dFontScale );
			pFont->lineHeight = 1.0f;
			pFont->baseHeight = 0.0f;
			pFont->textureSize = { glyphTextureSize.x, glyphTextureSize.y };

			pFont->leftPadding   = 0.0f;
			pFont->rightPadding  = 0.0f;
			pFont->topPadding    = 0.0f;
			pFont->bottomPadding = 0.0f;

			pFont->widthSpace = float( fontSpaceWidth * scale.x / dFontScale );
			pFont->widthTab   = float( fontTabWidth * scale.x / dFontScale );

			const auto addChar = [ &superMSDF, &glyphTextureSize, &fontHandle, &charList, &range, &scale, &realScale, &dFontScale, &pFont ]( uint32_t index )
			{
				Shape shape;

				Bitmap< float, 3 > msdf( glyphTextureSize.x, glyphTextureSize.y );
				Bitmap< float, 1 >  sdf( glyphTextureSize.x, glyphTextureSize.y );

				double charAdvance = 0.0;

				if( ! loadGlyph( shape, fontHandle, charList[ index ], &charAdvance ) )
				{
						return;
				}

				double l = numeric_limits< double >::max( );
				double b = numeric_limits< double >::max( );
				double r = numeric_limits< double >::lowest( );
				double t = numeric_limits< double >::lowest( );

				shape.normalize( );
				shape.bounds( l, b, r, t );

				const float fontScale = float( dFontScale );
				const float fontRange = float( range );

				const vec2 tr( r, t );
				const vec2 bl( l, b );

				const vec2 shapeCenter = ( tr + bl ) * 0.5f;
				const vec2 texCenter   = fontScale * 0.5f / scale;
				const vec2 translate   = texCenter - shapeCenter;
				const Vector2 texTranslate( translate.x, translate.y );

				edgeColoringSimple( shape, 3.0 );

				generateMSDF( msdf, shape, range, realScale, texTranslate );
				generateSDF (  sdf, shape, range, realScale, texTranslate );

				// Collect float texture data and convert it to a uint colour
				for( uint32_t y = 0; y < glyphTextureSize.y; ++y )
				{
					for( uint32_t x = 0; x < glyphTextureSize.x; ++x )
					{
						const vec4 fTexel =
						{
							msdf( x, glyphTextureSize.y - ( y + 1 ) )[ 0 ],
							msdf( x, glyphTextureSize.y - ( y + 1 ) )[ 1 ],
							msdf( x, glyphTextureSize.y - ( y + 1 ) )[ 2 ],
								sdf( x, glyphTextureSize.y - ( y + 1 ) )[ 0 ]
						};

						const uvec4 uTexel = clamp( fTexel * 255.0f, 0.0f, 255.0f );

						superMSDF[ index ][ x + y * glyphTextureSize.x ] =
							uTexel.a << 24 | uTexel.b << 16 | uTexel.g << 8 | uTexel.r;
					}
				}

				// Collect charactures kerning data
				uint32_t kerningCount  = 0;
				uint32_t kerningOffset = static_cast< uint32_t >( pFont->kerningInfo.size( ) );

				for( uint32_t z = 0; z < glyphTextureSize.z; ++z )
				{
					double kerning = 0.0f;

					if( getKerning( kerning, fontHandle, charList[ z ], charList[ index ] ) )
					{
						kerning /= float( fontScale );
						kerning *= realScale.x;
						pFont->kerningInfo.push_back( { charList[ z ], charList[ index ], float( kerning ) } );
						kerningCount++;
					}
				}

				kerningOffset = kerningCount > 0 ? kerningOffset : 0;


				vec2 offset = { bl.x - range, bl.y - range - range };
				vec2 uv   = bl - fontRange * 0.5f;
				vec2 size = tr - bl + fontRange;

				uv     += translate;
				uv     *= scale / fontScale;
				size   *= scale / fontScale;
				offset *= scale / fontScale;

				float xAdvance;
				xAdvance = float( charAdvance ) * scale.x / fontScale;

				pFont->charList.push_back( {
					charList[ index ],
					index,
					uv, size,
					size, offset,
					xAdvance,
					kerningCount, kerningOffset
					} );
			};

			for( uint32_t z = 0; z < glyphTextureSize.z; ++z )
			{
				superMSDF[ z ].resize( glyphTextureSize.x * glyphTextureSize.y, 0x00000000 );
				addChar( z );
			}

			pFont->charTextureArray = pImageFactory->createSrv2DArrayFromDataSet( font, superMSDF, glyphTextureSize );

			destroyFont( fontHandle );

			deinitializeFreetype(ft);

			return pFont;
		} );
	}
} // namespace directX11
} // namespace visual
