#pragma once
//
// DirectX 11 Data factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : DataFactory.h
// Date      : 07/12/2016
// Author    : Nicholas Welters

#ifndef _DATA_FACTORY_DX11_H
#define _DATA_FACTORY_DX11_H

#include "../APITypeDefs.h"

#include <Macros.h>

namespace visual
{
namespace ui
{
	FORWARD_DECLARE( SystemUI );
}

namespace fx
{
	FORWARD_DECLARE( SceneRenderer );
	FORWARD_DECLARE( Camera );
	FORWARD_DECLARE( Settings );
}

namespace directX11
{
	FORWARD_DECLARE( TextureFactory );
	FORWARD_DECLARE(  DeviceFactory );
	FORWARD_DECLARE(  ShaderFactory );
	FORWARD_DECLARE(  BufferFactory );
	FORWARD_DECLARE(  TargetFactory );
	FORWARD_DECLARE(   StateFactory );
	FORWARD_DECLARE(   ImageFactory );
	FORWARD_DECLARE(    MeshFactory );
	FORWARD_DECLARE(    FontFactory );

	FORWARD_DECLARE( PipelineBuilder );
	FORWARD_DECLARE( SceneBuilder );

	FORWARD_DECLARE( CommandDispatch );
	FORWARD_DECLARE( Scene );
	FORWARD_DECLARE( SceneRenderer );
	FORWARD_DECLARE( HwndSwapChain );


	/// <summary> Direct X 11 GPU interface factory to help use build GPU objects </summary>
	class DataFactory
	{
	public:
		/// <summary> ctor </summary>
		DataFactory( );

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DataFactory( const DataFactory& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DataFactory( DataFactory&& move ) = default;


		/// <summary> dtor </summary>
		~DataFactory( );


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		DataFactory& operator=( const DataFactory& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		DataFactory& operator=( DataFactory&& move ) = default;


		/// <summary> Initialise a DX11 D3D device. </summary>
		void initialise( );

		/// <summary>
		/// Get the API's command buffer executor.
		/// This is used to send our commands to the GPU.
		/// </summary>
		CommandDispatchSPtr getFXCommandExecutor( _In_ size_t bufferingSize ) const;

		/// <summary> Get a handle to the hardware swapchain. </summary>
		/// <param name="hwnd"> The HWND pointer to the window that we will render to. </param>
		/// <param name="width"> The HWND width. </param>
		/// <param name="height"> The HWND height. </param>
		/// <param name="bufferCount"> The number of buffers to use in the swap chain. </param>
		HwndSwapChainSPtr createSwapChain( _In_    HWND* hwnd,
										   _In_ uint32_t width,
										   _In_ uint32_t height,
										   _In_ uint32_t bufferCount ) const;

		/// <summary> Create a rendering pipeline that can draw a scene. </summary>
		/// <param name="pSettings"> The scene settings. </param>
		/// <param name="pHwndSwapchain"> The pipelines final output target. </param>
		/// <param name="pExecutor"> The command dispatch so that we can build and execute indirect command lists. </param>
		/// <param name="pCamera"> The camera to draw the scene in front of. </param>
		/// <param name="pUI"> The user interface to render. </param>
		/// <returns> A new scene renderer. </returns>
		SceneRendererSPtr createSceneRenderer( _In_ const fx::SettingsSPtr& pSettings,
											   _In_ const HwndSwapChainSPtr& pHwndSwapchain,
											   _In_ const CommandDispatchSPtr& pExecutor,
											   _In_ const fx::CameraSPtr& pCamera,
											   _In_ const ui::SystemUISPtr& pUI ) const;

		/// <summary>
		/// Create a scene builder that we can use to build object for a scene concurrently.
		/// </summary>
		/// <param name="pScene"> The scene we are going use the builder for. </param>
		/// <returns> A new scene builder. </returns>
		SceneBuilderSPtr createSceneBuilder( _In_ const SceneRendererSPtr& pScene );

	protected:
		/// <summary> Get the data buffer factory. </summary>
		/// <returns> The data buffer factory. </returns>
		BufferFactorySPtr getBufferFactory( ) const;

		/// <summary> Get the texture factory. </summary>
		/// <returns> The texture factory. </returns>
		ImageFactorySPtr  getImageFactory( )  const;

		/// <summary> Get the render target factory. </summary>
		/// <returns> The render target factory. </returns>
		TargetFactorySPtr getTargetFactory( ) const;

		/// <summary> Get the geometry factory. </summary>
		/// <returns> The geometry factory. </returns>
		MeshFactorySPtr   getMeshFactory( )   const;

		/// <summary> Get the shader application factory. </summary>
		/// <returns> The shader application factory. </returns>
		ShaderFactorySPtr getShaderFactory( ) const;

		/// <summary> Get the renderer states factory. </summary>
		/// <returns> The renderer states factory. </returns>
		StateFactorySPtr  getStateFactory( )  const;

		/// <summary> Get the font factory. </summary>
		/// <returns> The font factory. </returns>
		FontFactorySPtr  getFontFactory( )  const;

		/// <summary> Get the rendering pipeline builder. </summary>
		/// <returns> The rendering pipeline builder. </returns>
		PipelineBuilderSPtr  getPipelineBuilder( )  const;

	private:
		/// <summary> Get the device factory. </summary>
		/// <returns> The device factory. </returns>
		DeviceFactorySPtr  getDeviceFactory( )  const;

	private:
		/// <summary> The device interface factory. </summary>
		DeviceFactorySPtr  pDeviceFactory;

		/// <summary> The data buffer factory. </summary>
		BufferFactorySPtr  pBufferFactory;

		/// <summary> The shader application factory. </summary>
		ShaderFactorySPtr  pShaderFactory;

		/// <summary> The geometry factory. </summary>
		MeshFactorySPtr    pMeshFactory;

		/// <summary> The texture factory. </summary>
		ImageFactorySPtr   pImageFactory;

		/// <summary> The render target factory. </summary>
		TargetFactorySPtr  pTargetFactory;

		/// <summary> The renderer states factory. </summary>
		StateFactorySPtr   pStateFactory;

		/// <summary> The font factory. </summary>
		FontFactorySPtr    pFontFactory;

		/// <summary> The rendering pipeline builder. </summary>
		PipelineBuilderSPtr pPipelineBuilder;

		/// <summary> DX11 Device. </summary>
		ID3D11DeviceCPtr		pDevice;

		/// <summary> DX11 Device Context. </summary>
		ID3D11DeviceContextCPtr pContext;

#ifdef USE_DX11_1
		/// <summary> DX11.1 Device. </summary>
		ID3D11Device1CPtr pDevice1;

		/// <summary> DX11.1 Device Context. </summary>
		ID3D11DeviceContext1CPtr pContext1;
#endif
#ifdef USE_DX11_2
		/// <summary> DX11.2 Device. </summary>
		ID3D11Device2CPtr pDevice2;

		/// <summary> DX11.2 Device Context. </summary>
		ID3D11DeviceContext2CPtr pContext2;
#endif
#ifdef USE_DX11_3
		/// <summary> DX11.3 Device. </summary>
		ID3D11Device3CPtr pDevice3;

		/// <summary> DX11.3 Device Context. </summary>
		ID3D11DeviceContext3CPtr pContext3;
#endif
#ifdef USE_DX11_4
		/// <summary> DX11.4 Device. </summary>
		ID3D11Device4CPtr pDevice4;

		/// <summary> DX11.5 Device. </summary>
		ID3D11Device5CPtr pDevice5;
#endif

		/// <summary> The latest interface for a DX11 Device Context. </summary>
		DeviceContextPtr_t pContextT;

		/// <summary> A struct holding all the things we need to make sure is supported before using it. </summary>
		Features features;
	};

	SHARED_PTR_TYPE_DEF( DataFactory );
} // namespace directX11
} // namespace visual

#endif // _DATA_FACTORY_DX11_H
