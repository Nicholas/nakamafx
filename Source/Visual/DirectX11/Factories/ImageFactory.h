#pragma once

// DirectX 11 Image factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : ImageFactory.h
// Date      : 21/02/2017
// Author    : Nicholas Welters

#ifndef _IMAGE_FACTORY_DX11_H
#define _IMAGE_FACTORY_DX11_H

#include "Visual/DirectX11/APITypeDefs.h"

#include <glm/glm.hpp>

#include <future>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( BufferFactory );

	typedef std::promise   < ID3D11ShaderResourceViewSPtr >    ID3D11ShaderResourceViewPromise;
	typedef std::shared_ptr< ID3D11ShaderResourceViewPromise > ID3D11ShaderResourceViewPromiseSPtr;

	/// <summary>
	/// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
	/// </summary>
	class ImageFactory
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="pDevice"> Our GPU device. </param>
			/// <param name="pBufferFactory"> The buffer factory to manage the generic data structures. </param>
			explicit ImageFactory(
				_In_ const ID3D11DeviceCPtr& pDevice,
				_In_ const BufferFactorySPtr& pBufferFactory );

			/// <summary> ctor </summary>
			ImageFactory( ) = delete;

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			ImageFactory( const ImageFactory& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="move"> The temporary to move. </param>
			ImageFactory( ImageFactory&& move ) = default;


			/// <summary> dtor </summary>
			~ImageFactory( ) = default;


			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			ImageFactory& operator=( const ImageFactory& copy ) = default;

			/// <summary> Move assignment operator </summary>
			/// <param name="move"> The temporary to move. </param>
			ImageFactory& operator=( ImageFactory&& move ) = default;


			// +-------+
			// | ASYNC |
			// +-------+

			/// <summary>
			/// Create a view of a texture for our shaders to use.
			/// We use this to load in a texture "asynchornisly". This is false (maybe)
			/// when loading formats that are ready to be used on the GPU such as dds.
			/// If the texture needs to be processed on the GPU such (eg, generate mips)
			/// the processing will be added run at the start of a frame update. Once complete
			/// the promise is resolved and contains a texture ready to use for rendering.
			/// </summary>
			/// <param name="fileName"> The file path to load the texture from. </param>
			/// <param name="forceSRGB"> Enable srgb use for the texture on the GPU. </param>
			/// <returns> A promise to a shader resource view. </returns>
			ID3D11ShaderResourceViewPromiseSPtr createSrv(
				_In_ const std::string& fileName,
				_In_opt_ bool forceSRGB = false );

			/// <summary> Set the async callback to provide a valid context. </summary>
			/// <param name="dispatch"> The trampoline function that calls a functor to pass through a valid context. </param>
			void setDispatch( const AsyncDispatcher& dispatch );


			// +------+
			// | SYNC |
			// +------+

			/// <summary> Create some texture memory space for us so that we can store some data here. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="pDescriptor"> The textures layout and usage description. </param>
			/// <param name="pSubresourceData"> The initial data to be stored on the GPU. </param>
			/// <returns> A 1D texture. </returns>
			ID3D11Texture1DSPtr createTexture(
				_In_ const std::string& id,
				_In_ D3D11_TEXTURE1D_DESC* pDescriptor,
				_In_ D3D11_SUBRESOURCE_DATA* pSubresourceData = nullptr );

			/// <summary> Create some texture memory space for us so that we can store some data here. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="pDescriptor"> The textures layout and usage description. </param>
			/// <param name="pSubresourceData"> The initial data to be stored on the GPU. </param>
			/// <returns> A 2D texture. </returns>
			ID3D11Texture2DSPtr createTexture(
				_In_ const std::string& id,
				_In_ D3D11_TEXTURE2D_DESC* pDescriptor,
				_In_ D3D11_SUBRESOURCE_DATA* pSubresourceData = nullptr );

			/// <summary> Create some texture memory space for us so that we can store some data here. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="pDescriptor"> The textures layout and usage description. </param>
			/// <param name="pSubresourceData"> The initial data to be stored on the GPU. </param>
			/// <returns> A 3D texture. </returns>
			ID3D11Texture3DSPtr createTexture(
				_In_ const std::string& id,
				_In_ D3D11_TEXTURE3D_DESC* pDescriptor,
				_In_ D3D11_SUBRESOURCE_DATA* pSubresourceData = nullptr );

			/// <summary> Create a view of a texture for our shaders to use. </summary>
			/// <param name="fileName"> The file path to load the texture from. </param>
			/// <returns> A shader resource view of the data from a texture file. </returns>
			ID3D11ShaderResourceViewSPtr createSrv(
				_In_ const DeviceContextPtr_t& pContext,
				_In_ const std::string& fileName,
				_In_opt_ bool forceSRGB = false );

			/// <summary> Create a view of a resource for our shaders to use. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="pDescriptor"> The shader resource view layout and usage description. </param>
			/// <param name="pResource"> The resource data. </param>
			/// <returns> A shader resource view of the provided texture data. </returns>
			ID3D11ShaderResourceViewSPtr createSrv(
				_In_ const std::string& id,
				_In_ const D3D11_SHADER_RESOURCE_VIEW_DESC* pDescriptor,
				_In_ const ID3D11ResourceSPtr& pResource );

			/// <summary> Create a view of a random 1D float[4] array. </summary>
			/// <returns> A shader resource view of the random data. </returns>
			ID3D11ShaderResourceViewSPtr createSrv1DFromRandom( );

			/// <summary> Create a view of a float[4] 2D array. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="data"> The views initial data. </param>
			/// <param name="size"> The size of the texture to create with the texture data. </param>
			/// <returns> A shader resource view of the data. </returns>
			ID3D11ShaderResourceViewSPtr createSrv2DFromDataSet(
				_In_ const std::string& id,
				_In_ const std::vector< glm::vec4 >& data,
				_In_ const glm::uvec2& size );

			/// <summary> Create a view of a R8G8B8A8 2D array. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="data"> The views initial data. </param>
			/// <param name="size"> The size of the texture to create with the texture data. </param>
			/// <returns> A shader resource view of the data. </returns>
			ID3D11ShaderResourceViewSPtr createSrv2DFromDataSet(
				_In_ const std::string& id,
				_In_ const std::vector< uint32_t >& data,
				_In_ const glm::uvec2& size );

			/// <summary> Create a view of a float[4] 3D array. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="data"> The views initial data. </param>
			/// <param name="size"> The size of the texture to create with the texture data. </param>
			/// <returns> A shader resource view of the data. </returns>
			ID3D11ShaderResourceViewSPtr createSrv2DArrayFromDataSet(
				_In_ const std::string& id,
				_In_ const std::vector< std::vector< glm::vec4 > >& data,
				_In_ const glm::uvec3& size );

			/// <summary> Create a view of a R8G8B8A8 3D array. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="data"> The views initial data. </param>
			/// <param name="size"> The size of the texture to create with the texture data. </param>
			/// <returns> A shader resource view of the data. </returns>
			ID3D11ShaderResourceViewSPtr createSrv2DArrayFromDataSet(
				_In_ const std::string& id,
				_In_ const std::vector< std::vector< uint32_t > >& data,
				_In_ const glm::uvec3& size );

			/// <summary> Create a cube map with a single solid colour. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="colour"> The colour of the cube map. </param>
			/// <returns> A shader resource view of the data. </returns>
			ID3D11ShaderResourceViewSPtr createSrv2DCubeFromColour(
				_In_ const std::string& id,
				_In_ const glm::vec4& colour );

			/// <summary> Create a view of a float[3] array. </summary>
			/// <param name="id"> The textures unique id. </param>
			/// <param name="data"> The views initial data. </param>
			/// <returns> A shader resource view of the data. </returns>
			ID3D11ShaderResourceViewSPtr createSrvFloat3TextureBuffer(
				_In_ const std::string& id,
				_In_ const std::vector< glm::vec3 >& data );

			/// <summary> Create structured buffer that we can update on the cpu. </summary>
			/// <param name="id"> The structured buffers unique id. </param>
			/// <param name="count"> The number of elements in the buffer. </param>
			/// <param name="stride"> The size of each element in the buffer. </param>
			/// <param name="pData"> The initial data to use. </param>
			UpdateableSrv createUpdateableStructuredBuffer(
				_In_ const ::std::string& id,
				_In_ uint32_t count,
				_In_ uint32_t stride,
				_In_ const void* pData );

		private:
			/// <summary> Check the extention to see if we can load the entire texture asynchronisly from the rendering pipeline. </summary>
			/// <param name="fileName"> The file path to the texture to load. </param>
			/// <returns> We can load the entire texture with the device only if true. false if we need a context. </returns>
			static bool supportsConcurrentLoad( _In_ const std::string& filePath );

			/// <summary> Check the extention is .dds. </summary>
			/// <param name="fileName"> The file path to the texture to load. </param>
			/// <returns> True if the file path ends in ".dds". </returns>
			static bool isDds( _In_ const std::string& filePath );

		private:
			/// <summary> Our GPU device. </summary>
			ID3D11DeviceCPtr pDevice;

			/// <summary> The buffer factory to manage the generic data structures. </summary>
			BufferFactorySPtr pBufferFactory;

			/// <summary> The 1D texture flyweight manager. </summary>
			ID3D11Texture1DLibrary texture1DLibrary;

			/// <summary> The 2D texture flyweight manager. </summary>
			ID3D11Texture2DLibrary texture2DLibrary;

			/// <summary> The 3D texture flyweight manager. </summary>
			ID3D11Texture3DLibrary texture3DLibrary;

			/// <summary> The shader resource view flyweight manager. </summary>
			ID3D11ShaderResourceViewLibrary shaderResourceViewLibrary;

			/// <summary>
			/// Async Texture Loading.
			///
			/// A system to place instructions in to the GPU pipeline
			/// when we require a context.
			/// </summary>
			AsyncDispatcher dispatch;
	};
} // namespace directX11
} // namespace visual

#endif // _TEXTURE_FACTORY_DX11_H
