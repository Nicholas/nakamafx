#pragma once

// DirectX 11 Mesh factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : MeshFactory.h
// Date      : 17/11/2015
// Author    : Nicholas Welters

#ifndef _MESH_FACTORY_DX11_H
#define _MESH_FACTORY_DX11_H

#include "BufferFactory.h"
#include "Visual/DirectX11/APITypeDefs.h"
#include "Visual/DirectX11/Types/Vertex.h"
#include "Visual/DirectX11/Types/MeshBuffer.h"
#include "Visual/FX/Mesh.h"
#include "Visual/FX/Vertex.h"

#include <Macros.h>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( BufferFactory );
	FORWARD_DECLARE( ShaderFactory );
	FORWARD_DECLARE( MeshBuffer );

	/// <summary> Geometry factroy so that we can draw objects. </summary>
	class MeshFactory
	{
	private:
		typedef fx::Mesh< fx::VertexParticle::Vertex >::VertexBuffer ParticleVertexBuffer;

	public:
		/// <summary> ctor </summary>
		/// <param name="pDevice"> Our GPU device. </param>
		/// <param name="pBufferFactory"> The buffer factory to manage the generic data structures. </param>
		/// <param name="pShaderFactory"> The shader factory to build vertex input layout validation shaders. </param>
		explicit MeshFactory( _In_ const ID3D11DeviceCPtr& pDevice,
							  _In_ const BufferFactorySPtr& pBufferFactory,
							  _In_ const ShaderFactorySPtr& pShaderFactory );

		/// <summary> ctor </summary>
		MeshFactory( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		MeshFactory( const MeshFactory& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		MeshFactory( MeshFactory&& move ) = default;


		/// <summary> dtor </summary>
		~MeshFactory( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		MeshFactory& operator=( const MeshFactory& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		MeshFactory& operator=( MeshFactory&& move ) = default;



		/// <summary> Create a vertex input layout. </summary>
		/// <template name="VertexType"> The vertex type to builds an input layout for. </template>
		/// <returns> The vertex input layout. </returns>
		template< class VertexType >
		ID3D11InputLayoutSPtr createInputLayout( );


		/// <summary> Create a mesh buffer on the GPU using CPU data. </summary>
		/// <template name="VertexType"> The vertex type build a buffer of. </template>
		/// <param name="id"> The mesh buffers unique string id. </param>
		/// <param name="mesh"> The mesh data to transfer to the GPU. </param>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		template< class VertexType >
		MeshBufferSPtr createMeshBuffer( _In_ const std::string& id,
										 _In_ const fx::Mesh< typename VertexType::Vertex >& mesh );

		/// <summary> Create a height map. </summary>
		/// <param name="fileName"> The location to a raw height map. </param>
		/// <param name="size"> The dimentions of the raw height map data. </param>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		MeshBufferSPtr createHeightMap( _In_ const std::string& fileName, _In_ const glm::uvec2& size );

		/// <summary> Create a coordinate star, the vertices and colours match the axis. </summary>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		MeshBufferSPtr createCoordStarMesh( );

		/// <summary> Create a cube. </summary>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		MeshBufferSPtr createSlabMesh( );

		/// <summary> Create a point mesh buffer. </summary>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		MeshBufferSPtr createPointMesh( );

		/// <summary> Create a flat plane. </summary>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		MeshBufferSPtr createPlane( );

		/// <summary> Create a single triangle that is used to cover the entire screen in a 2D pass. </summary>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		MeshBufferSPtr createFullScreenPassMesh( ); // TODO: Remove and use the bufferless fullscreen pass

		/// <summary> Create a cube that is used to cover the entire screen in a 3D pass. </summary>
		/// <returns> A GPU mesh buffer to draw the geometry. </returns>
		MeshBufferSPtr createFullScreenPassCube( );


		/// <summary> Create a initial state particle vertex buffer. </summary>
		/// <returns> The GPU buffer data. </returns>
		ID3D11BufferSPtr getInitParticleBuffer( _In_ const std::string& id,
												_In_ const ParticleVertexBuffer& initialState ) const;

		/// <summary> Create an update state particle vertex buffer. </summary>
		/// <returns> The GPU buffer data. </returns>
		ID3D11BufferSPtr getParticleBuffer( _In_ const std::string& name, _In_ unsigned int size ) const;

		/// <summary> Create a default fire particle vertex buffer. </summary>
		/// <returns> The GPU buffer data. </returns>
		ID3D11BufferSPtr getFireParticleBuffer( ) const;

		/// <summary> Create a cpu updatable vertex buffer using the specified vertex type. </summary>
		/// <template name="VertexType"> The vertex type build a buffer of. </template>
		/// <param name="id"> The vertex buffer unique string id. </param>
		/// <param name="size"> A vertex buffer size. </param>
		/// <returns> The buffer and the number of elements in the buffer. </returns>
		template< class VertexType >
		ArrayBuffer createDynamicVertexBuffer( _In_ const std::string& id,
											   _In_ uint32_t size );

		/// <summary> Create a cpu updatable vertex buffer using the specified vertex type. </summary>
		/// <template name="IndexType"> The index type build a buffer of. </template>
		/// <param name="id"> The index buffer unique string id. </param>
		/// <param name="size"> A index buffer size. </param>
		/// <returns> The buffer and the number of elements in the buffer. </returns>
		template< class IndexType >
		ArrayBuffer createDynamicIndexBuffer( _In_ const std::string& id,
											  _In_ uint32_t size );


	private:
		/// <summary> Create a vertex input layout. </summary>
		/// <param name="name"> The vertex type unique string id. </param>
		/// <param name="pDescriptor"> The vertex layour discriptor. </param>
		/// <param name="shader"> A shader that validates the input layout. </param>
		/// <param name="size"> The number of elements in the vertex. </param>
		/// <returns> The vertex input layout. </returns>
		ID3D11InputLayoutSPtr createInputLayoutImpl( _In_ const std::string& name,
													 _In_ const D3D11_INPUT_ELEMENT_DESC* pDescriptor,
													 _In_ const std::string& shader,
													 _In_ uint32_t size );

		/// <summary> Create a vertex buffer using the specified vertex type. </summary>
		/// <template name="VertexType"> The vertex type build a buffer of. </template>
		/// <param name="id"> The vertex buffer unique string id. </param>
		/// <param name="buffer"> A vector of vertices to store in the buffer. </param>
		/// <returns> The buffer and the number of elements in the buffer. </returns>
		template< class VertexType >
		ArrayBuffer createVertexBuffer( _In_ const std::string& id,
										_In_ const std::vector< typename VertexType::Vertex >& buffer );

		/// <summary> Create an index buffer. </summary>
		/// <param name="id"> The vertex buffer unique string id. </param>
		/// <param name="buffer"> A vector of indecise to store in the buffer. </param>
		/// <returns> The buffer and the number of elements in the buffer. </returns>
		ArrayBuffer createIndexBuffer( _In_ const std::string& id,
									   _In_ const std::vector< uint32_t >& buffer ) const;

	private:
		/// <summary> Our GPU device. </summary>
		ID3D11DeviceCPtr pDevice;

		/// <summary> The buffer factory to manage the generic data structures. </summary>
		BufferFactorySPtr pBufferFactory;

		/// <summary> The shader factory to build vertex input layout validation shaders. </summary>
		ShaderFactorySPtr pShaderFactory;

		/// <summary> The unique set of input layouts loaded in the system. </summary>
		ID3D11InputLayoutLibrary inputLayoutLibrary;
	};


	/// <summary> Create a vertex input layout. </summary>
	/// <template name="VertexType"> The vertex type to buils an input layout for. </template>
	/// <returns> The vertex input layout. </returns>
	template< class VertexType >
	ID3D11InputLayoutSPtr MeshFactory::createInputLayout( )
	{
		typedef VertexLayout< VertexType > Vertex;
		return createInputLayoutImpl( Vertex::vertexName,
									  Vertex::description,
									  Vertex::vertexShader,
									  VertexType::size );
	}

	/// <summary> Create a mesh buffer on the GPU using CPU data. </summary>
	/// <template name="VertexType"> The vertex type build a buffer of. </template>
	/// <param name="id"> The mesh buffers unique string id. </param>
	/// <param name="mesh"> The mesh data to transfer to the GPU. </param>
	/// <returns> The buffer and the number of elements in the buffer. </returns>
	template< class VertexType >
	MeshBufferSPtr MeshFactory::createMeshBuffer( _In_ const std::string& id,
												  _In_ const fx::Mesh< typename VertexType::Vertex>& mesh )
	{
		VertexBuffer pVertexBuffer = createVertexBuffer< VertexType >( id + "_VERTEX_BUFFER", mesh.getVertexBuffer( ) );
		IndexBuffer  pIndexBuffer  = createIndexBuffer( id + "_INDEX_BUFFER", mesh.getIndexBuffer( ) );

		if( !pVertexBuffer.pBuffer )
		{
			std::cout << "Failed -> createVertexBuffer : \"" << id << "_VERTEX_BUFFER\" )";
			return nullptr;
		}

		if( !pIndexBuffer.pBuffer )
		{
			std::cout << "Failed -> createIndexBuffer : \"" << id << "\"_INDEX_BUFFER\" )";
			return nullptr;
		}

		D3D11_PRIMITIVE_TOPOLOGY topology = MeshBuffer::PrimativeMap[ mesh.getTopology( ) ];

		return std::make_shared< MeshBuffer >( pVertexBuffer, pIndexBuffer, topology );
	}

	/// <summary> Create a vertex buffer using the specified vertex type. </summary>
	/// <template name="VertexType"> The vertex type build a buffer of. </template>
	/// <param name="id"> The vertex buffer unique string id. </param>
	/// <param name="buffer"> A vector of vertices to store in the buffer. </param>
	/// <returns> The buffer and the number of elements in the buffer. </returns>
	template< class VertexType >
	ArrayBuffer MeshFactory::createVertexBuffer( _In_ const std::string& id,
												 _In_ const std::vector< typename VertexType::Vertex >& buffer )
	{
		const uint32_t vertexSize  = static_cast< uint32_t    >( sizeof( typename VertexType::Vertex ) );
		const uint32_t vertexCount = static_cast< uint32_t    >( buffer.size( ) );
		const void*    pBuffer     = static_cast< const void* >( buffer.data( ) );
		const uint32_t bufferSize  = vertexSize * vertexCount;

		return { vertexCount, pBufferFactory->createVertexBuffer( id, pBuffer, bufferSize ), vertexSize };
	}

	/// <summary> Create a vertex buffer using the specified vertex type. </summary>
	/// <template name="VertexType"> The vertex type build a buffer of. </template>
	/// <param name="id"> The vertex buffer unique string id. </param>
	/// <param name="buffer"> A vector of vertices to store in the buffer. </param>
	/// <returns> The buffer and the number of elements in the buffer. </returns>
	template< class VertexType >
	ArrayBuffer MeshFactory::createDynamicVertexBuffer( _In_ const std::string& id,
														_In_ const uint32_t size )
	{
		const uint32_t vertexSize = static_cast< uint32_t >( sizeof( typename VertexType::Vertex ) );
		const uint32_t bufferSize = vertexSize * size;

		return { size, pBufferFactory->createDynamicVertexBuffer( id, bufferSize ), vertexSize };
	}

	/// <summary> Create a cpu updatable vertex buffer using the specified vertex type. </summary>
	/// <template name="IndexType"> The index type build a buffer of. </template>
	/// <param name="id"> The index buffer unique string id. </param>
	/// <param name="size"> A index buffer size. </param>
	/// <returns> The buffer and the number of elements in the buffer. </returns>
	template< class IndexType >
	ArrayBuffer MeshFactory::createDynamicIndexBuffer( _In_ const std::string& id,
													   _In_ uint32_t size )
	{
		const uint32_t indexSize  = static_cast< uint32_t >( sizeof( IndexType ) );
		const uint32_t bufferSize = indexSize * size;

		return { size, pBufferFactory->createDynamicIndexBuffer( id, bufferSize ), indexSize };
	}
} // namespace directX11
} // namespace visual

#endif // _MESH_FACTORY_DX11_H
