#pragma once

// Shader Program factory so that we can instruct the GPU how to process our data,
// 
// Project   : NaKama-Fx
// File Name : ShaderFactory.h
// Date      : 19/06/2015
// Author    : Nicholas Welters


#ifndef _DX11_SHADER_MANAGER_H
#define _DX11_SHADER_MANAGER_H

#include "Visual/DirectX11/APITypeDefs.h"
#include "Visual/DirectX11/Types/Vertex.h"

typedef interface ID3D10Blob* LPD3D10BLOB;
typedef ID3D10Blob ID3DBlob;

namespace visual
{
namespace directX11
{
	/// <summary>
	/// Shader Program factory so that we can instruct the GPU how to process our data,
	/// </summary>
	class ShaderFactory
	{
		public:
			/// <summary> copy ctor </summary>
			/// <param name="pDevice"> The device connection to our gpu so that we can wrapp the shader construction here. </param>
			explicit ShaderFactory( _In_ const ID3D11DeviceCPtr& pDevice );

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			ShaderFactory( _In_ const ShaderFactory& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="move"> The temporary to move. </param>
			ShaderFactory( _In_ ShaderFactory&& move ) = default;


			/// <summary> dtor </summary>
			virtual ~ShaderFactory( ) = default;


			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			ShaderFactory& operator=( _In_ const ShaderFactory& copy ) = default;

			/// <summary> Move assignment operator </summary>
			/// <param name="move"> The temporary to move. </param>
			ShaderFactory& operator=( _In_ ShaderFactory&& move ) = default;


			// +----------------------+
			// | Shader from resource |
			// +----------------------+
			
			/// <summary> Load a compiled vertex shader from the resource id </summary>
			/// <param name="id"> The vertex shader resource id. </param>
			/// <returns> The vertex shader. </returns>
			ID3D11VertexShaderSPtr getVertexShader( _In_ int id );
			
			/// <summary> Load a compiled pixel shader from the resource id </summary>
			/// <param name="id"> The pixel shader resource id. </param>
			/// <returns> The pixel shader. </returns>
			ID3D11PixelShaderSPtr getPixelShader( _In_ int id );
			
			/// <summary> Load a compiled geometry shader from the resource id </summary>
			/// <param name="id"> The geometry shader resource id. </param>
			/// <returns> The geometry shader. </returns>
			ID3D11GeometryShaderSPtr getGeometryShader( _In_ int id );
			
			/// <summary> Load a compiled hull shader from the resource id </summary>
			/// <param name="id"> The hull shader resource id. </param>
			/// <returns> The hull shader. </returns>
			ID3D11HullShaderSPtr getHullShader( _In_ int id );
			
			/// <summary> Load a compiled domain shader from the resource id </summary>
			/// <param name="id"> The domain shader resource id. </param>
			/// <returns> The domain shader. </returns>
			ID3D11DomainShaderSPtr getDomainShader( _In_ int id );
			
			/// <summary> Load a compiled compute shader from the resource id </summary>
			/// <param name="id"> The compute shader resource id. </param>
			/// <returns> The compute shader. </returns>
			ID3D11ComputeShaderSPtr getComputeShader( _In_ int id );


			// +------------------+
			// | Shader from file |
			// +------------------+
			
			/// <summary> Compile and load a vertex shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader. </returns>
			ID3D11VertexShaderSPtr getVertexShader(
				_In_ const std::string& fileName,
				_In_opt_ const std::string& entryPoint = "main",
				_In_opt_ const std::string& shaderModel = "vs_5_0" );
			
			/// <summary> Compile and load a pixel shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader. </returns>
			ID3D11PixelShaderSPtr getPixelShader(
				_In_ const std::string& fileName,
				_In_opt_ const std::string& entryPoint = "main",
				_In_opt_ const std::string& shaderModel = "ps_5_0" );
			
			/// <summary> Compile and load a geometry shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPointid"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader. </returns>
			ID3D11GeometryShaderSPtr getGeometryShader(
				const std::string& fileName,
				_In_opt_ const std::string& entryPoint = "main",
				_In_opt_ const std::string& shaderModel = "gs_5_0" );
			
			/// <summary> Compile and load a hull shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader. </returns>
			ID3D11HullShaderSPtr getHullShader(
				_In_ const std::string& fileName,
				_In_opt_ const std::string& entryPoint = "main",
				_In_opt_ const std::string& shaderModel = "hs_5_0" );
			
			/// <summary> Compile and load a domain shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader. </returns>
			ID3D11DomainShaderSPtr getDomainShader(
				_In_ const std::string& fileName,
				_In_opt_ const std::string& entryPoint = "main",
				_In_opt_ const std::string& shaderModel = "ds_5_0" );
			
			/// <summary> Compile and load a compute shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader. </returns>
			ID3D11ComputeShaderSPtr getComputeShader(
				_In_ const std::string& fileName,
				_In_opt_ const std::string& entryPoint = "main",
				_In_opt_ const std::string& shaderModel = "cs_5_0" );

			
			
			/// <summary> Load a compiled geometry shader from the resource id that streams out vertices. </summary>
			/// <template name="VertexType"> The streamed out vertex type. </template>
			/// <param name="id"> The streaming out geometry shader resource id. </param>
			/// <returns> The streaming out geometry shader. </returns>
			template< class VertexType >
			ID3D11GeometryShaderSPtr getGeometryShaderOS( _In_ const int id )
			{
				return getGeometryShaderOS( id,
											VertexLayout< VertexType >::soDescription,
											VertexType::size );
			}
			
			
			/// <summary> Compile and load a geometry shader from file that streams out vertices. </summary>
			/// <template name="VertexType"> The streamed out vertex type. </template>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader. </returns>
			template< class VertexType >
			ID3D11GeometryShaderSPtr getGeometryShaderOS(
				_In_ const std::string& fileName,
				_In_opt_ const std::string& entryPoint = "main",
				_In_opt_ const std::string& shaderModel = "gs_5_0" )
			{
				return getGeometryShaderOS( fileName,
											entryPoint,
											shaderModel,
											VertexLayout< VertexType >::soDescription,
											VertexType::size );
			}
			
			/// <summary> Compile a vertex shader that is defined in the vertex header. </summary>
			/// <param name="vertex"> The vertex string id/name. </param>
			/// <param name="shaderCode"> The vertexs simple shader. </param>
			/// <returns> The compiled shader. </returns>
			static ID3DBlob* compileVertexShaderWithInputSignature( const std::string& vertex,
																	const std::string& shaderCode );

		private:
			/// <summary> Load a compiled geometry shader from the resource id </summary>
			/// <param name="id"> The geometry shader resource id. </param>
			/// <param name="layout"> The geometry shader streaming out structure. </param>
			/// <param name="size"> The number of elements in the layout. </param>
			/// <returns> The geometry shader. </returns>
			ID3D11GeometryShaderSPtr getGeometryShaderOS( _In_ int id,
														  _In_ const D3D11_SO_DECLARATION_ENTRY* layout,
														  _In_ int size );
		
			/// <summary> Compile and load a geometry shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <param name="layout"> The geometry shader streaming out structure. </param>
			/// <param name="size"> The number of elements in the layout. </param>
			/// <returns> The compiled shader. </returns>
			ID3D11GeometryShaderSPtr getGeometryShaderOS( _In_ const std::string& fileName,
														  _In_ const std::string& entryPoint,
														  _In_ const std::string& shaderModel,
														  _In_ const D3D11_SO_DECLARATION_ENTRY * layout,
														  _In_ int size );

			// +---------------------------------------+
			// | Shader complication from a text file. |
			// +---------------------------------------+

			/// <summary> Compile a shader from file. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="entryPoint"> The shaders entry point name. </param>
			/// <param name="shaderModel"> The shaders model. </param>
			/// <returns> The compiled shader blob data. </returns>
			ID3DBlob* compileFromFile( _In_ const std::string& fileName,
									   _In_ const std::string& entryPoint,
									   _In_ const std::string& shaderModel ) const;
			
			/// <summary> Prints out the contents of the shader compilation result blob. </summary>
			/// <param name="fileName"> The shaders file path. </param>
			/// <param name="pError"> The compilation message blob. </param>
			/// <param name="isError"> A flag used to identify warnings vs error messages. </param>
			static void blobErrorMessage( _In_ const std::string& fileName,
										  _In_ ID3DBlob* pError,
										  _In_opt_ bool isError = true );

		private:
			/// <summary> DX11 Device handle so that we can give it data. </summary>
			ID3D11DeviceCPtr pDevice;

			/// <summary> Vertex shader library (flyweight). </summary>
			ID3D11VertexShaderLibrary vertexShaderLibrary;

			/// <summary> Pixel shader library (flyweight). </summary>
			ID3D11PixelShaderLibrary pixelShaderLibrary;

			/// <summary> Geometry shader library (flyweight). </summary>
			ID3D11GeometryShaderLibrary geometryShaderLibrary;

			/// <summary> Hull shader library (flyweight). </summary>
			ID3D11HullShaderLibrary hullShaderLibrary;

			/// <summary> Domain shader library (flyweight). </summary>
			ID3D11DomainShaderLibrary domainShaderLibrary;

			/// <summary> Compute shader library (flyweight). </summary>
			ID3D11ComputeShaderLibrary computeShaderLibrary;

	};
} // namespace directX11
} // namespace visual

#endif // _DX11_SHADER_MANAGER_H