#pragma once

// DirectX 11 Font factory.
//
// Loads in the information needed to render a font.
// This uses msdfgen to generate msfda textures.
//
// Project   : NaKama-Fx
// File Name : FontFactory.h
// Date      : 29/05/2019
// Author    : Nicholas Welters

#ifndef _FONT_FACTORY_DX11_H
#define _FONT_FACTORY_DX11_H

#include "Visual/DirectX11/APITypeDefs.h"

#include <glm/glm.hpp>

#include <future>

namespace msdfgen {
	class FontHandle;
}

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( BufferFactory );
	FORWARD_DECLARE( ImageFactory );
	FORWARD_DECLARE( Font );
	//FORWARD_DECLARE_STRUCT( CharInfo );
	//FORWARD_DECLARE_STRUCT( KerningInfo );

	typedef tools::DefaultBookDeleter< Font > FontDeleter;
	typedef std::function< Font* () > FontBuilder;
	typedef tools::Library< Font, std::string, FontDeleter, FontBuilder > FontLibrary;

	/// <summary>
	/// Create font structures so that we can render some text using some font.
	/// </summary>
	class FontFactory
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pDevice"> Our GPU device. </param>
		/// <param name="pImageFactory"> The image factory to manage the texture data structures. </param>
		explicit FontFactory(
			_In_ const ID3D11DeviceCPtr& pDevice,
			_In_ const  ImageFactorySPtr&  pImageFactory );

		/// <summary> ctor </summary>
		FontFactory( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		FontFactory( const FontFactory& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		FontFactory( FontFactory&& move ) = default;


		/// <summary> dtor </summary>
		~FontFactory( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		FontFactory& operator=( const FontFactory& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		FontFactory& operator=( FontFactory&& move ) = default;



		/// <summary> Create render targets so that we can collect colour information. </summary>
		/// <param name="pSwapChain"> Hardware swapchain to create a render target view for. </param>
		FontSPtr createFont( _In_ const std::string& font );

	private:
		/// <summary> Our GPU device. </summary>
		ID3D11DeviceCPtr pDevice;

		/// <summary> The texture factory that manages the image like data. </summary>
		ImageFactorySPtr pImageFactory;

		/// <summary> The font flyweight. </summary>
		FontLibrary fontLibrary;
	};
} // namespace directX11
} // namespace visual

#endif // _TARGET_FACTORY_DX11_H
