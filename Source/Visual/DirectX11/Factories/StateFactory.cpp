#include "StateFactory.h"

// DirectX 11 State factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : StateFactory.cpp
// Date      : 07/12/2016
// Author    : Nicholas Welters

#include "ShaderFactory.h"
#include "Visual/FX/Shapes.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	using std::make_shared;
	using std::to_string;
	using std::string;
	using fx::SamplerBounds;
	using fx::SamplerFilter;
	using fx::Blend;

	struct StateFactory::Library
	{
		Library( );

		/// <summary> The sampler states to sample textures. </summary>
		ID3D11SamplerStateLibrary samplerStateLibrary;

		/// <summary>
		/// The depth stencil state, telling us how to read
		/// and update the buffer when we draw.
		/// </summary>
		ID3D11DepthStencilStateLibrary depthStencilStates;

		/// <summary>
		/// The rasterizer state, telling us how to construct
		/// fragments from our primatives.
		/// </summary>
#if defined( USE_DX11_3 )
		ID3D11RasterizerState2Library rasterizerStates;
#elif defined( USE_DX11_1 )
		ID3D11RasterizerState1Library rasterizerStates;
#else
		ID3D11RasterizerStateLibrary rasterizerStates;
#endif

		/// <summary>
		/// The blend state, telling us how to update
		/// the render targets when we return from the fragment shader.
		/// </summary>
#if defined( USE_DX11_1 )
		ID3D11BlendState1Library blendStates;
#else
		ID3D11BlendStateLibrary blendStates;
#endif
	};

	StateFactory::Library::Library( )
		: samplerStateLibrary( ID3D11_FACTORY( ID3D11SamplerState      ), DX11_DELETER )
		, depthStencilStates ( ID3D11_FACTORY( ID3D11DepthStencilState ), DX11_DELETER )

#ifdef USE_DX11_3
		, rasterizerStates  ( ID3D11_FACTORY( ID3D11RasterizerState2  ), DX11_DELETER )
#elif defined USE_DX11_1
		, rasterizerStates  ( ID3D11_FACTORY( ID3D11RasterizerState1  ), DX11_DELETER )
#else
		, rasterizerStates  ( ID3D11_FACTORY( ID3D11RasterizerState  ), DX11_DELETER )
#endif

#ifdef USE_DX11_1
		, blendStates       ( ID3D11_FACTORY( ID3D11BlendState1 ), DX11_DELETER )
#else
		, blendStates       ( ID3D11_FACTORY( ID3D11BlendState ), DX11_DELETER )
#endif
	{ }

	/// <summary> ctor </summary>
	/// <param name="pDevice"> Our GPU device. </param>
	/// <param name="pBufferFactory"> The buffer factory to manage the generic data structures. </param>
	StateFactory::StateFactory( _In_ const ID3D11DeviceCPtr& pDevice,
#ifdef USE_DX11_1
								_In_ const ID3D11Device1CPtr& pDevice1,
#endif
#ifdef USE_DX11_3
								_In_ const ID3D11Device3CPtr& pDevice3
#endif
	)
		: pDevice( pDevice )
#ifdef USE_DX11_1
		, pDevice1( pDevice1 )
#endif
#ifdef USE_DX11_3
		, pDevice3( pDevice3 )
#endif
		, pLibrary( make_shared< Library >( ) )
	{ }


	/// <summary> Create a texture sampler states to be used on the GPU in a shader. </summary>
	/// <param name="uvMode"> How the GPU handles out of bounds sampling texture coordinates. </param>
	/// <param name="filter"> How the GPU handles sampling the texture buffer and returning a colour. </param>
	/// <param name="comparitor"> Set the sampler to be used on a depth buffer for depth testing and shadow mapping. </param>
	/// <returns> A texture sampler state. </returns>
	ID3D11SamplerStateSPtr StateFactory::createSamplerState( _In_ SamplerBounds uvMode,
															 _In_ SamplerFilter filter,
															 _In_opt_ bool comparitor ) const
	{
		string name = "DEFAULT_";
		name += comparitor ? "COMPARITOR" :
							 "SAMPLER";
		name += uvMode == fx::MIRROR ? "MIRROR" :
				uvMode == fx::WRAP   ? "WRAP"   :
									   "CLAMP";
		name += filter == fx::ANISOTROPIC ? "ANISOTROPIC" :
				filter == fx::LINEAR      ? "LINEAR"      :
											"POINT";

		// Request a nice good old texture sampler.
		return pLibrary->samplerStateLibrary.checkOut(name, [ this, uvMode, filter, comparitor, name ]( )
		{
			// -----------------------------------------------------------------------------------
			// Anisotropic wrapped texture.
			// -----------------------------------------------------------------------------------
			D3D11_SAMPLER_DESC samplerDescriptor;
			if( comparitor )
			{
				samplerDescriptor.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
				//samplerDescriptor.Filter = D3D11_FILTER_COMPARISON_ANISOTROPIC;
				samplerDescriptor.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;

				if( filter == fx::LINEAR )
				{
					samplerDescriptor.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
				}
				else
				{
					samplerDescriptor.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
				}
			}
			else
			{
				samplerDescriptor.ComparisonFunc = D3D11_COMPARISON_ALWAYS;

				if( filter == fx::ANISOTROPIC )
				{
					samplerDescriptor.Filter = D3D11_FILTER_ANISOTROPIC;
				}
				else if( filter == fx::LINEAR )
				{
					samplerDescriptor.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
				}
				else
				{
					samplerDescriptor.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
				}
			}

			if( uvMode == fx::MIRROR )
			{
				samplerDescriptor.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
				samplerDescriptor.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
				samplerDescriptor.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
			}
			else if( uvMode == fx::WRAP )
			{
				samplerDescriptor.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDescriptor.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDescriptor.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
			}
			else
			{
				samplerDescriptor.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
				samplerDescriptor.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
				samplerDescriptor.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
			}


			samplerDescriptor.MipLODBias = 0.0f;
			samplerDescriptor.MaxAnisotropy = 16;
			samplerDescriptor.BorderColor[0] = 0.0f;
			samplerDescriptor.BorderColor[1] = 0.0f;
			samplerDescriptor.BorderColor[2] = 0.0f;
			samplerDescriptor.BorderColor[3] = 0.0f;
			samplerDescriptor.MinLOD = 0;
			samplerDescriptor.MaxLOD = D3D11_FLOAT32_MAX;

			ID3D11SamplerState* pSamplerState = nullptr;
			ThrowIfFailed( pDevice->CreateSamplerState(&samplerDescriptor, &pSamplerState ) );

			setDebugName( pSamplerState, name );
			return pSamplerState;
		});
	}


	/// <summary> Create a depth stencil state. </summary>
	/// <param name="write"> Update the depth stencil when we draw a pixel. </param>
	/// <param name="test"> Test the pixels depth with the depth buffer and if it is bellow the surface the pixel is discarded. </param>
	/// <returns> A depth stencil state. </returns>
	ID3D11DepthStencilStateSPtr StateFactory::createDepthStencilState( _In_opt_ bool write,
																	   _In_opt_ bool test ) const
	{
		string id = "DEPTH_STENCIL_STATE_";
		id += write ? "+W" : "-w";
		id += test  ? "+T" : "-t";

		return pLibrary->depthStencilStates.checkOut( id, [ this, id, write, test ]( )
		{
			D3D11_DEPTH_STENCIL_DESC descriptor;
			ZeroMemory( &descriptor, sizeof( descriptor ) );

			// Set up the description of the stencil state.
			descriptor.DepthEnable    = test ? TRUE : FALSE;
			descriptor.DepthFunc      = D3D11_COMPARISON_LESS;
			descriptor.DepthWriteMask = write ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;

			descriptor.StencilEnable    = ( test && write ) ? TRUE : FALSE;
			descriptor.StencilReadMask  = 0xFF;
			descriptor.StencilWriteMask = 0xFF;

			// Stencil operations if pixel is front-facing.
			descriptor.FrontFace.StencilFailOp      = D3D11_STENCIL_OP_KEEP;
			descriptor.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
			descriptor.FrontFace.StencilPassOp      = D3D11_STENCIL_OP_KEEP;
			descriptor.FrontFace.StencilFunc        = D3D11_COMPARISON_ALWAYS;

			// Stencil operations if pixel is back-facing.
			descriptor.BackFace.StencilFailOp      = D3D11_STENCIL_OP_KEEP;
			descriptor.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
			descriptor.BackFace.StencilPassOp      = D3D11_STENCIL_OP_KEEP;
			descriptor.BackFace.StencilFunc        = D3D11_COMPARISON_ALWAYS;

			ID3D11DepthStencilState* pDepthStencilState = nullptr;
			ThrowIfFailed( pDevice->CreateDepthStencilState( &descriptor, &pDepthStencilState ) );

			setDebugName( pDepthStencilState, id );
			return pDepthStencilState;
		});
	}

	/// <summary> Create a rasterizer state. </summary>
	/// <param name="wireframe"> Only fill in the trriangle edges rather than the entire trienagle. </param>
	/// <param name="backfaceCulling"> Discared triangles that point away from the viewer. </param>
	/// <param name="ccw"> Set the rasterizer to use counter clockwise triangles as front facing triangles. </param>
	/// <returns> A rasterizerl state. </returns>
	RasterizerStateSPtr_t StateFactory::createRasterizerState( _In_ bool wireframe,
															   _In_ bool backfaceCulling,
															   _In_ bool ccw ) const
	{
		string id			  = "Rasterizer";
		id += wireframe		  ? "Wire" :
							    "Solid";
		id += backfaceCulling ? "Culling" :
								"NotCulling";
		id += ccw			  ? "CCW" :
								"CW";

		return pLibrary->rasterizerStates.checkOut(id, [ this, id, wireframe, backfaceCulling, ccw ]( )
		{
#ifdef USE_DX11_3
			D3D11_RASTERIZER_DESC2 descriptor;
#elif defined USE_DX11_1
			D3D11_RASTERIZER_DESC1 descriptor;
#else
			D3D11_RASTERIZER_DESC descriptor;
#endif
			ZeroMemory( &descriptor, sizeof( D3D11_RASTERIZER_DESC ) );

			descriptor.FillMode              = wireframe ? D3D11_FILL_WIREFRAME : D3D11_FILL_SOLID;
			descriptor.CullMode              = backfaceCulling ? D3D11_CULL_BACK : D3D11_CULL_NONE;
			descriptor.FrontCounterClockwise = ccw ? TRUE : FALSE;
			descriptor.DepthBias			 = 0;
			descriptor.DepthBiasClamp		 = 0.0f;
			descriptor.SlopeScaledDepthBias  = 0.0f;
			descriptor.DepthClipEnable		 = TRUE;
			descriptor.MultisampleEnable	 = FALSE;
			//descriptor.ScissorEnable		 = FALSE;
			descriptor.ScissorEnable		 = TRUE;
			descriptor.AntialiasedLineEnable = FALSE;

#ifdef USE_DX11_3
			descriptor.ForcedSampleCount	 = 0;
			descriptor.ConservativeRaster    = D3D11_CONSERVATIVE_RASTERIZATION_MODE_OFF;

			ID3D11RasterizerState2* pRasterizerState = nullptr;
			ThrowIfFailed( pDevice3->CreateRasterizerState2( &descriptor, &pRasterizerState ) );
#elif defined USE_DX11_1
			descriptor.ForcedSampleCount	 = 0;

			ID3D11RasterizerState1* pRasterizerState = nullptr;
			ThrowIfFailed( pDevice1->CreateRasterizerState1( &descriptor, &pRasterizerState ) );
#else
			ID3D11RasterizerState* pRasterizerState = nullptr;
			ThrowIfFailed( pDevice->CreateRasterizerState( &descriptor, &pRasterizerState ) );
#endif

			setDebugName( pRasterizerState, id );
			return pRasterizerState;
		} );
	}

	/// <summary> Create a blend state. </summary>
	/// <param name="alphaBlend"> The type of blend state to create. </param>
	/// <returns> A blend state. </returns>
	BlendStateSPtr_t StateFactory::createBlendState( Blend alphaBlend ) const
	{
		string id = "Blend";
		id += to_string( static_cast< int >( alphaBlend ) );

		return pLibrary->blendStates.checkOut( id, [ this, id, alphaBlend ]( )
		{
#ifdef USE_DX11_1
			D3D11_BLEND_DESC1 descriptor;
#else
			D3D11_BLEND_DESC descriptor;
#endif
			ZeroMemory( &descriptor, sizeof( D3D11_BLEND_DESC ) );

			descriptor.AlphaToCoverageEnable  = FALSE;
			descriptor.IndependentBlendEnable = FALSE;

			descriptor.RenderTarget[ 0 ].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
			descriptor.RenderTarget[ 0 ].BlendOp      = D3D11_BLEND_OP_ADD;
			descriptor.RenderTarget[ 0 ].BlendOpAlpha = D3D11_BLEND_OP_ADD;

			if( alphaBlend == fx::ALPHA_BLEND
				|| alphaBlend == fx::ALT_ALPHA_BLEND
				|| alphaBlend == fx::ADD_BLEND
				|| alphaBlend == fx::PRE_MULTIPLIED_ALPHA_BLEND )
			{
				descriptor.RenderTarget[ 0 ].BlendEnable = TRUE;
			}
			else
			{
				descriptor.RenderTarget[ 0 ].BlendEnable = FALSE;
			}

			if( alphaBlend == fx::ALT_ALPHA_BLEND )
			{
				descriptor.RenderTarget[ 0 ].SrcBlend  = D3D11_BLEND_SRC_ALPHA;
				descriptor.RenderTarget[ 0 ].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

				descriptor.RenderTarget[ 0 ].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
				descriptor.RenderTarget[ 0 ].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
			}
			else if( alphaBlend == fx::PRE_MULTIPLIED_ALPHA_BLEND )
			{
				descriptor.RenderTarget[ 0 ].SrcBlend  = D3D11_BLEND_ONE;
				descriptor.RenderTarget[ 0 ].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

				descriptor.RenderTarget[ 0 ].SrcBlendAlpha  = D3D11_BLEND_ONE;
				descriptor.RenderTarget[ 0 ].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
			}
			else if( alphaBlend == fx::ADD_BLEND )
			{
				descriptor.RenderTarget[ 0 ].SrcBlend  = D3D11_BLEND_ONE;
				descriptor.RenderTarget[ 0 ].DestBlend = D3D11_BLEND_ONE;

				descriptor.RenderTarget[ 0 ].SrcBlendAlpha  = D3D11_BLEND_ONE;
				descriptor.RenderTarget[ 0 ].DestBlendAlpha = D3D11_BLEND_ONE;
			}
			else
			{
				descriptor.RenderTarget[ 0 ].SrcBlend  = D3D11_BLEND_SRC_ALPHA;
				descriptor.RenderTarget[ 0 ].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
				//descriptor.RenderTarget[ 0 ].DestBlend = D3D11_BLEND_ONE;

				descriptor.RenderTarget[ 0 ].SrcBlendAlpha  = D3D11_BLEND_ZERO;
				descriptor.RenderTarget[ 0 ].DestBlendAlpha = D3D11_BLEND_ZERO;
			}

#ifdef USE_DX11_1
			descriptor.RenderTarget[ 0 ].LogicOpEnable = FALSE;
			descriptor.RenderTarget[ 0 ].LogicOp	   = D3D11_LOGIC_OP_NOOP;

			ID3D11BlendState1* pBlendState = nullptr;
			ThrowIfFailed( pDevice1->CreateBlendState1( &descriptor, &pBlendState ) );
#else
			ID3D11BlendState* pBlendState = nullptr;
			ThrowIfFailed( pDevice->CreateBlendState( &descriptor, &pBlendState ) );
#endif

			setDebugName( pBlendState, id );
			return pBlendState;
		} );
	}
} // namespace directX11
} // namespace visual
