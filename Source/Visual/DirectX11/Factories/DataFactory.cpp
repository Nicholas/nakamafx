#include "DataFactory.h"
//
// DirectX 11 Data factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : DataFactory.h
// Date      : 07/12/2016
// Author    : Nicholas Welters

#include "TargetFactory.h"
#include "ImageFactory.h"
#include "BufferFactory.h"
#include "DeviceFactory.h"
#include "ShaderFactory.h"
#include "MeshFactory.h"
#include "FontFactory.h"
#include "StateFactory.h"
#include "../Builders/PipelineBuilder.h"
#include "../Builders/SceneBuilder.h"
#include "../SwapChain/HwndSwapChain.h"
#include "../Targets/MultipleRenderTarget.h"
#include "../SceneRenderer.h"
#include "../CommandDispatch.h"

namespace visual
{
namespace directX11
{
	using fx::SettingsSPtr;
	using fx::CameraSPtr;

	using std::make_shared;
	using std::runtime_error;
	using std::vector;
	using std::string;

	/// <summary> ctor </summary>
	DataFactory::DataFactory( )
		: pDeviceFactory( nullptr )
		, pBufferFactory( nullptr )
		, pShaderFactory( nullptr )
		, pMeshFactory( nullptr )
		, pImageFactory( nullptr )
		, pTargetFactory( nullptr )
		, pStateFactory( nullptr )
		, pFontFactory( nullptr )
		, pPipelineBuilder( nullptr )

		, pDevice( nullptr )
		, pContext( nullptr )

#ifdef USE_DX11_1
		, pDevice1( nullptr )
		, pContext1( nullptr )
#endif
#ifdef USE_DX11_2
		, pDevice2( nullptr )
		, pContext2( nullptr )
#endif
#ifdef USE_DX11_3
		, pDevice3( nullptr )
		, pContext3( nullptr )
#endif
#ifdef USE_DX11_4
		, pDevice4( nullptr )
		, pDevice5( nullptr )
#endif
		, pContextT( nullptr )
		, features( )
	{ }

	DataFactory::~DataFactory( )
	{
		// Delete the helper factories.
		pFontFactory.reset( );
		pStateFactory.reset( );
		pShaderFactory.reset( );
		pMeshFactory.reset( );
		pBufferFactory.reset( );
		pImageFactory.reset( );
		pTargetFactory.reset( );
		pPipelineBuilder.reset( );

		pDevice.Reset( );
		pContext.Reset( );

#ifdef USE_DX11_1
		pDevice1.Reset( );
		pContext1.Reset( );
#endif
#ifdef USE_DX11_2
		pDevice2.Reset( );
		pContext2.Reset( );
#endif
#ifdef USE_DX11_3
		pDevice3.Reset( );
		pContext3.Reset( );
#endif
#ifdef USE_DX11_4
		pDevice4.Reset( );
		pDevice5.Reset( );
#endif

		pContextT.Reset( );

		// Destroy last! Debug info should run last so that we can
		// see if there are any issues.
		pDeviceFactory.reset( );
	}

	/// <summary> Initialise a DX11 D3D device. </summary>
	void DataFactory::initialise( )
	{
		pDeviceFactory = make_shared< DeviceFactory >( );
		DevicePackage devices;
		pDeviceFactory->initialiseDevice( devices );

		 pBufferFactory = make_shared<  BufferFactory >( devices );
		 pShaderFactory = make_shared<  ShaderFactory >( devices.pDevice );
		  pImageFactory = make_shared<   ImageFactory >( devices.pDevice, pBufferFactory );
		 pTargetFactory = make_shared<  TargetFactory >( devices.pDevice, pBufferFactory, pImageFactory );
		   pMeshFactory = make_shared<    MeshFactory >( devices.pDevice, pBufferFactory, pShaderFactory );
		  pStateFactory = make_shared<   StateFactory >( devices.pDevice
#ifdef USE_DX11_1
													   , devices.pDevice1
#endif
#ifdef USE_DX11_3
													   , devices.pDevice3
#endif
		);

		pFontFactory = make_shared< FontFactory >( devices.pDevice, pImageFactory );

		pPipelineBuilder = make_shared< PipelineBuilder >( pStateFactory,
														   pShaderFactory,
														   pBufferFactory,
														   pMeshFactory,
														   pImageFactory,
														   pTargetFactory,
														   pFontFactory );

		pDevice  = devices.pDevice;
		pContext = devices.pContext;

#ifdef USE_DX11_1
		pDevice1  = devices.pDevice1;
		pContext1 = devices.pContext1;
#endif
#ifdef USE_DX11_2
		pDevice2  = devices.pDevice2;
		pContext2 = devices.pContext2;
#endif
#ifdef USE_DX11_3
		pDevice3  = devices.pDevice3;
		pContext3 = devices.pContext3;
#endif
#ifdef USE_DX11_4
		pDevice4 = devices.pDevice4;
		pDevice5 = devices.pDevice5;
#endif

#ifdef USE_DX11_3
		pContextT = pContext3;
#elif defined USE_DX11_2
		pContextT = pContext2;
#elif defined USE_DX11_1
		pContextT = pContext1;
#else
		pContextT = pContext;
#endif

		features = pDeviceFactory->getGpuInfo( pDevice );
	}

	/// <summary> Get the data buffer factory. </summary>
	/// <returns> The data buffer factory. </returns>
	BufferFactorySPtr DataFactory::getBufferFactory( ) const
	{
		if( !pBufferFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pBufferFactory;
	}

	/// <summary> Get the texture factory. </summary>
	/// <returns> The texture factory. </returns>
	ImageFactorySPtr DataFactory::getImageFactory( ) const
	{
		if( !pImageFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pImageFactory;
	}

	/// <summary> Get the render target factory. </summary>
	/// <returns> The render target factory. </returns>
	TargetFactorySPtr DataFactory::getTargetFactory( ) const
	{
		if( !pTargetFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pTargetFactory;
	}

	/// <summary> Get the geometry factory. </summary>
	/// <returns> The geometry factory. </returns>
	MeshFactorySPtr DataFactory::getMeshFactory( ) const
	{
		if( !pMeshFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pMeshFactory;
	}

	/// <summary> Get the shader application factory. </summary>
	/// <returns> The shader application factory. </returns>
	ShaderFactorySPtr DataFactory::getShaderFactory( ) const
	{
		if( !pShaderFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pShaderFactory;
	}

	/// <summary> Get the renderer states factory. </summary>
	/// <returns> The renderer states factory. </returns>
	StateFactorySPtr DataFactory::getStateFactory( ) const
	{
		if( !pStateFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pStateFactory;
	}

	/// <summary> Get the font factory. </summary>
	/// <returns> The font factory. </returns>
	FontFactorySPtr DataFactory::getFontFactory( ) const
	{
		if( !pFontFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pFontFactory;
	}

	/// <summary> Get the rendering pipeline builder. </summary>
	/// <returns> The rendering pipeline builder. </returns>
	PipelineBuilderSPtr DataFactory::getPipelineBuilder( ) const
	{
		if( !pPipelineBuilder )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pPipelineBuilder;
	}

	/// <summary> Get the device factory. </summary>
	/// <returns> The device factory. </returns>
	DeviceFactorySPtr DataFactory::getDeviceFactory( ) const
	{
		if( !pDeviceFactory )
		{
			throw runtime_error( "Rendering API has not been Initialised!" );
		}

		return pDeviceFactory;
	}







	/// <summary> Get the API's command buffer executor.
	///			  This is used to send our commands to the GPU.
	/// </summary>
	CommandDispatchSPtr DataFactory::getFXCommandExecutor( _In_ size_t bufferingSize ) const
	{
		BufferedCommandQueueSPtr pRenderingCommands = make_shared< BufferedCommandQueue >( );
		AsyncEventBufferSPtr pAsyncEventBuffer = make_shared< AsyncEventBuffer >( );

		getImageFactory( )->setDispatch( [ pAsyncEventBuffer ]( const AsyncEvent& action )
		{
			pAsyncEventBuffer->push( action );
		} );

		return make_shared< CommandDispatch >( pContextT, pRenderingCommands, pAsyncEventBuffer);
	}

	/// <summary> Get the API's command buffer executor.
	///			  This is used to send our commands to the GPU.
	/// </summary>
	/// <param name="hwnd">The HWND pointer to the window that we will render to.</param>
	HwndSwapChainSPtr DataFactory::createSwapChain(
		_In_    HWND* hwnd,
		_In_ uint32_t width,
		_In_ uint32_t height,
		_In_ uint32_t bufferCount ) const
	{
		SwapChainPtr_t pSwapChain = getDeviceFactory( )->createSwapChain( hwnd, 2, features, pDevice );

		SHARED_PTR_TYPE_DEF( MultipleRenderTarget );
		MultipleRenderTargetSPtr pSwapChainRenderTarget = make_shared< MultipleRenderTarget >(
			vector< ID3D11Texture2DSPtr >{ nullptr },
			vector< ID3D11RenderTargetViewSPtr >{ getTargetFactory()->createSwapChainRtv( pSwapChain ) },
			vector< ID3D11ShaderResourceViewSPtr >{ nullptr },
			nullptr,
			getTargetFactory()->createDepthStencil("Swap Chain Depth Stencil",width, height),
			nullptr,
			width, height
		);

		HwndSwapChainSPtr pHwndSwapchain = make_shared< HwndSwapChain >( false, pSwapChainRenderTarget, pSwapChain );

		return pHwndSwapchain;
	}

	/// <summary> Create a rendering pipeline that can draw a scene. </summary>
	/// <param name="pSettings"> The scene settings. </param>
	/// <param name="pHwndSwapchain"> The pipelines final output target. </param>
	/// <param name="pExecutor"> The command dispatch so that we can build and execute indirect command lists. </param>
	/// <param name="pCamera"> The camera to draw the scene in front of. </param>
	/// <param name="pUI"> The user interface to render. </param>
	/// <returns> A new scene renderer. </returns>
	SceneRendererSPtr DataFactory::createSceneRenderer(
		_In_ const SettingsSPtr& pSettings,
		_In_ const HwndSwapChainSPtr& pHwndSwapchain,
		_In_ const CommandDispatchSPtr& pExecutor,
		_In_ const CameraSPtr& pCamera,
		   _In_ const ui::SystemUISPtr& pUI ) const
	{
		// The scene that has the swapchain as its render target.
		// Add subscenes to this to draw on the swapchains backbuffer
		return make_shared< SceneRenderer >(
			pContextT,
			pPipelineBuilder->buildScenePipeline( pHwndSwapchain->getOutputTarget( ), pSettings, pContextT, pUI ),
			pCamera,
			pSettings
		);
	}

	/// <summary>
	/// Create a scene builder that we can use to build object for a scene concurrently.
	/// </summary>
	/// <param name="pScene"> The scene we are going use the builder for. </param>
	/// <returns> A new scene builder. </returns>
	SceneBuilderSPtr DataFactory::createSceneBuilder( _In_ const SceneRendererSPtr& pScene )
	{
		SceneBuilder::TransferFunction transfer = [ pScene ]( const SceneDataSetSPtr& pData )
		{
			pScene->setScene( pData );
		};

		return make_shared< SceneBuilder >( transfer, pStateFactory, pBufferFactory, pMeshFactory, pImageFactory, pFontFactory );
	}
} // namespace directX11
} // namespace visual
