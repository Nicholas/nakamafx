
#include "DeviceFactory.h"

// DirectX 11 Device factory.
//
// Creates our DirectX 11 instance and the link to the GPU that we are going to use for rendering.
//
// Project   : NaKama-Fx
// File Name : DeviceFactory.cpp
// Date      : 07/12/2016
// Author    : Nicholas Welters

#ifdef ENABLE_DX_11_4
#include <d3d11_4.h>
#define USE_DX11_1
#define USE_DX11_2
#define USE_DX11_3
#define USE_DX11_4
#elif defined ENABLE_DX_11_3
#include <d3d11_3.h>
#define USE_DX11_1
#define USE_DX11_2
#define USE_DX11_3
#elif defined ENABLE_DX_11_2
#include <d3d11_2.h>
#define USE_DX11_1
#define USE_DX11_2
#elif defined ENABLE_DX_11_1
#include <d3d11_1.h>
#define USE_DX11_1
#elif defined ENABLE_DX_11_0
#include <D3D11.h>
#endif

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>

#if defined(NDEBUG) && defined(__GNUC__)
#define U_ASSERT_ONLY __attribute__((unused))
#else
#define U_ASSERT_ONLY
#endif

namespace visual
{
namespace directX11
{
	using std::runtime_error;
	using std::cout;
	using std::wcout;
	using std::fixed;
	using std::setprecision;
	using std::internal;
	using std::endl;
	using std::setw;
	using std::left;
	using std::vector;
	using std::string;
	using tools::almostEqual;
	using tools::Array;


	DeviceFactory::DeviceFactory( )
		: pDebug( nullptr )
		, pFactory( nullptr )
		, pAdapter( nullptr )
	{
		initialise( );
	}

	DeviceFactory::~DeviceFactory()
	{
		pAdapter.Reset();
		pFactory.Reset();

#ifdef _DEBUG
		if (pDebug)
		{
#ifdef USE_DX11_4
			pDebug->ReportLiveDeviceObjects( D3D11_RLDO_SUMMARY | D3D11_RLDO_IGNORE_INTERNAL );
			//pDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL | D3D11_RLDO_IGNORE_INTERNAL);
			//pDebug->ReportLiveDeviceObjects( D3D11_RLDO_SUMMARY );
#elif defined USE_DX11_2
			pDebug->ReportLiveDeviceObjects( D3D11_RLDO_DETAIL | D3D11_RLDO_IGNORE_INTERNAL );
			//pDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
			//pDebug->ReportLiveDeviceObjects( D3D11_RLDO_SUMMARY );
#endif
			pDebug.Reset();
		}
#endif
	}


	/// <summary>
	/// Here we are going to create an instance.
	/// Then select a GPU and create a connection to it.
	/// </summary>
	void DeviceFactory::initialise( )
	{
		cout << "Initialising DirectX 11" << endl;

		cout << "\n";
		cout << "API Version: " << D3D11_SDK_VERSION << endl;

		initialiseFactory( );
		initialiseAdapter( );
	}

	/// <summary> Create the DXGI factory and the adapter to create our device. </summary>
	void DeviceFactory::initialiseFactory( )
	{
		// -----------------------------------------------------------------------------------
		// DXGI Create Factory
		// -----------------------------------------------------------------------------------
		// the factory enumerates the set of adapters that are available in the system. Therefore,
		// if you change the adapters in a system, you must destroy and recreate the IDXGIFactory1 object
		
#if defined USE_DX11_2 && defined _DEBUG
		ThrowIfFailed( CreateDXGIFactory2( DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS( &pFactory ) ) );
#elif defined USE_DX11_2
		ThrowIfFailed( CreateDXGIFactory2( 0x00000000, IID_PPV_ARGS( &pFactory ) ) );
#else
		ThrowIfFailed( CreateDXGIFactory1( IID_PPV_ARGS( &pFactory ) ) );
#endif
	}

	/// <summary> Create the DXGI adapter to create our device. </summary>
	void DeviceFactory::initialiseAdapter( )
	{
		UINT i = 0;
		D3D_FEATURE_LEVEL featureLevels[ ] =
		{
#ifdef USE_DX11_1
			D3D_FEATURE_LEVEL_11_1,
#endif
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};
		const UINT numFeatureLevels = Array::size( featureLevels );

		UINT createDeviceFlags = 0; // D3D11_CREATE_DEVICE_SWITCH_TO_REF

# ifdef _DEBUG
		// When running in debug mode this should allow
		// us to collect more information about the rendering
		// state
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
# endif
		
#ifdef USE_DX11_WIN7
		// This was done to run a DX11.0 only device (Radion HD6970)
		IDXGIAdapterCPtr p_adapter;
		while (pFactory->EnumAdapters( i, &p_adapter ) != DXGI_ERROR_NOT_FOUND)
		{
#else
		IDXGIAdapter1CPtr p_adapter;
		while (pFactory->EnumAdapters1( i, &p_adapter ) != DXGI_ERROR_NOT_FOUND)
		{
			++i;

			DXGI_ADAPTER_DESC1 desc;
			p_adapter->GetDesc1( &desc );

			if( desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE )
			{
				continue; // Don't select the Basic Render Driver adapter.
			}
#endif
			// Check to see if the adapter supports Direct3D, but don't create the
			// actual device yet.
			if( !pAdapter && SUCCEEDED( D3D11CreateDevice( 
				p_adapter.Get( ),
				D3D_DRIVER_TYPE_UNKNOWN,
				nullptr,
				createDeviceFlags,
				featureLevels,
				numFeatureLevels,
				D3D11_SDK_VERSION,
				nullptr,
				nullptr,
				nullptr ) ) )
			{
#ifdef USE_DX11_WIN7
				pAdapter = p_adapter;
#else
				if( FAILED( p_adapter.As( &pAdapter ) ) )
				{
					cout << "Failed to convert adapter 1 to adapter 3!\n";
				}
#endif

				cout << "Device created\n";
			}
		}

		if( !pAdapter )
		{
			cout << "Call to EnumAdapters failed to create IDXGIAdapter!\n";
			throw runtime_error("Call to EnumAdapters failed to create IDXGIAdapter!\n");
		}
			
		setDebugName( pAdapter , "Adaptor"  );
	}

	
	/// <summary>
	/// Create our API connection to the GPU so that we can modify data
	/// on the GPU with the device and send commands using the context.
	/// </summary>
	void DeviceFactory::initialiseDevice( _Out_ DevicePackage& package )
	{
		HRESULT result( S_OK );

		D3D_DRIVER_TYPE   driverType;
		D3D_FEATURE_LEVEL featureLevel;

		D3D_DRIVER_TYPE driverTypes[ ] =
		{
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE,
		};

		const UINT numDriverTypes = Array::size( driverTypes );

		D3D_FEATURE_LEVEL featureLevels[ ] =
		{
#ifdef USE_DX12
		// Features from the Windows 10 SDK & DX12
			D3D_FEATURE_LEVEL_12_1,
			D3D_FEATURE_LEVEL_12_0,
#endif

#ifdef USE_DX11_4
#elif defined USE_DX11_3
#elif defined USE_DX11_2
#elif defined USE_DX11_1
			D3D_FEATURE_LEVEL_11_1,
#endif
			D3D_FEATURE_LEVEL_11_0,
		
		// Features that exist but we don't want
		//	D3D_FEATURE_LEVEL_10_1,
		//	D3D_FEATURE_LEVEL_10_0,
		//	D3D_FEATURE_LEVEL_9_3,
		//	D3D_FEATURE_LEVEL_9_2,
		//	D3D_FEATURE_LEVEL_9_1,
		};
		const UINT numFeatureLevels = Array::size( featureLevels );

#ifdef USE_DX11_1
		UINT createDeviceFlags = 0;//D3D11_CREATE_DEVICE_VIDEO_SUPPORT; This option prevents us from being able to create a graphics debugged device.
#else
		UINT createDeviceFlags = 0;
#endif

# ifdef _DEBUG
		// When running in debug mode this should allow
		// us to collect more information about the rendering
		// state
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
# endif

		// -----------------------------------------------------------------------------------
		//Create the Direct3D Device
		// -----------------------------------------------------------------------------------
		for( UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++ )
		{
			driverType = driverTypes[ driverTypeIndex ];

			result = D3D11CreateDevice
			(
				nullptr,			    // Pointer to a IDXGIAdapter.
				driverType,			    // The type of driver for the device.
				nullptr,			    // A handle to the DLL that implements a software rasterizer.
				createDeviceFlags,	    // Device creation flags.
				featureLevels,		    // feature levels of the device.
				numFeatureLevels,	    //
				D3D11_SDK_VERSION,	    // Bit flag that indicates the version of the SDK.
				&package.pDevice,	    // Address of a pointer to an ID3D11Device Interface
				&featureLevel,		    // The feature level of the device
				&package.pContext	    // Address of pointer to  the Imidiate contrext
			);

			// Check to see if we can exit our loop because we
			// have a device, context and swap chain.
			if( SUCCEEDED( result ) )
			{
				driverTypeIndex = numDriverTypes;
			}
		}

		// Check to see if we have created a DX11 device, context and swap chain.
		if( FAILED( result ) )
		{
			cout << "Call to D3D11CreateDevice failed!\n";
			throw runtime_error( "Failed to initialize a renderer. Call to D3D11CreateDevice failed!" );
		}


#ifdef USE_DX11_4
		result = package.pDevice.As( &package.pDevice5 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11Device5 failed!\n";
		}

		result = package.pDevice.As( &package.pDevice5 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11Device4 failed!\n";
		}
#endif
#ifdef USE_DX11_3
		result = package.pDevice.As( &package.pDevice3 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11Device3 failed!\n";
		}

		result = package.pContext.As( &package.pContext3 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11DeviceContext3 failed!\n";
		}
#endif
#ifdef USE_DX11_2
		result = package.pDevice.As( &package.pDevice2 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11Device2 failed!\n";
		}

		result = package.pContext.As( &package.pContext2 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11DeviceContext2 failed!\n";
		}
#endif
#ifdef USE_DX11_1
		result = package.pDevice.As( &package.pDevice1 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11Device1 failed!\n";
		}

		result = package.pContext.As( &package.pContext1 );

		if( FAILED( result ) )
		{
			cout << "Call to get ID3D11DeviceContext1 failed!\n";
		}
#endif

		setDebugName( package.pDevice , "Device"  );
		setDebugName( package.pContext, "Context" );

# ifdef _DEBUG
		//ID3D11Debug* p_debug;
		//pDevice->QueryInterface( __uuidof( ID3D11Debug ), reinterpret_cast<void**>( &p_debug ) );
		//pDevice->QueryInterface( &p_debug );
		//pDebug = DebugSPtr( p_debug, releaser );
		//pDevice->QueryInterface( &pDebug );
		//pDevice->QueryInterface( IID_PPV_ARGS( &pDebug ) ); // This is why the function can't be const!
		package.pDevice.As( &pDebug );
#endif
	}




	/// <summary> Create a DXGI swapchain for a DX12 device to a win32 window.
	/// </summary>
	/// <param name="hwnd">The window the the swap chain displays to.</param>
	/// <param name="features">The features struct to see if we should allow tearing.</param>
	/// <param name="pDevice">The device we will link the swap chain to.</param>
	/// <returns> A IDXGISwapChain4Ptr linked to a DX12 device and its resources. </returns>
	SwapChainPtr_t DeviceFactory::createSwapChain(
		_In_ HWND* hwnd,
		_In_ const uint32_t bufferCount,
		_In_ const Features& features,
		_In_ const ID3D11DeviceCPtr& pDevice
	) const
	{
		SwapChainPtr_t pSwapChain;

#ifdef USE_DX11_1

		IDXGISwapChain1CPtr pSwapchain;
		DXGI_SWAP_CHAIN_FULLSCREEN_DESC* pFullscreenDesc = nullptr;
		DXGI_SWAP_CHAIN_DESC1 descriptor;
		ZeroMemory( &descriptor, sizeof DXGI_SWAP_CHAIN_DESC1 );

		descriptor.Width  = 0; // Obtained from the HWND
		descriptor.Height = 0; // Obtained from the HWND
		descriptor.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		descriptor.Stereo = false;
		descriptor.SampleDesc.Count   = 1;
		descriptor.SampleDesc.Quality = 0;
		descriptor.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // DXGI_USAGE_BACK_BUFFER implicit
		descriptor.BufferCount = bufferCount; // DXGI will apparently block after 3 (tripple buffering).
		descriptor.AlphaMode   = DXGI_ALPHA_MODE_IGNORE;

#ifdef USE_DX11_4
		descriptor.Scaling    = DXGI_SCALING_NONE;
		descriptor.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		descriptor.Flags      = features.allowTearing ? DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING : 0;

		//  Win 10 flip discard (Allow the desktop window manager to access my private swapchain). DXGI_SWAP_EFFECT_DISCARD bitblit win 8 and earlier...

		// Allow tearing in windowed mode. Fullscreen doesn't need this but its recomended.
		// For the likes of gsync and freesync variable refreash rates
		// Protect our content. Prevent other applications from being able to capture our image.
		// DXGI_SWAP_CHAIN_FLAG_RESTRICTED_CONTENT,
		// DXGI_SWAP_CHAIN_FLAG_RESTRICT_SHARED_RESOURCE_DRIVER,
		// DXGI_SWAP_CHAIN_FLAG_DISPLAY_ONLY
#else
		descriptor.Scaling    = DXGI_SCALING_STRETCH;     // DXGI_SCALING_NONE
		descriptor.SwapEffect = DXGI_SWAP_EFFECT_DISCARD; // DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL
		descriptor.Flags = 0;
#endif

		ThrowIfFailed( pFactory->CreateSwapChainForHwnd(
			pDevice.Get( ), *hwnd, &descriptor, pFullscreenDesc, nullptr,
			&pSwapchain
		) );
		
#ifdef USE_DX11_2
		ThrowIfFailed( pSwapchain.As( &pSwapChain ) );
#else
		pSwapChain = pSwapchain;
#endif

		// Make sure that DXGI stops trying to be nice to use and give use print screen and alt+enter functions.
		ThrowIfFailed( pFactory->MakeWindowAssociation( *hwnd, DXGI_MWA_NO_WINDOW_CHANGES ) );

#else

		IDXGISwapChainCPtr pSwapchain;
		DXGI_SWAP_CHAIN_DESC descriptor;
		ZeroMemory( &descriptor, sizeof DXGI_SWAP_CHAIN_DESC );

		descriptor.BufferDesc.Width  = 0; // Obtained from the HWND
		descriptor.BufferDesc.Height = 0; // Obtained from the HWND
		descriptor.BufferDesc.RefreshRate.Numerator   = 0;
		descriptor.BufferDesc.RefreshRate.Denominator = 1;
		descriptor.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		descriptor.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
		descriptor.BufferDesc.Scaling = DXGI_MODE_SCALING_STRETCHED;
		descriptor.SampleDesc.Count = 1;
		descriptor.SampleDesc.Quality = 0;
		descriptor.BufferCount = bufferCount;
		descriptor.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		descriptor.Flags = 0;
		descriptor.OutputWindow = *hwnd;
		descriptor.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		descriptor.Windowed = true;

		ThrowIfFailed( pFactory->CreateSwapChain( pDevice.Get( ), &descriptor, &pSwapchain ) );
		pSwapChain = pSwapchain;
#endif

		setDebugName( pSwapChain, "HwndSwapChain" );

		return pSwapChain;
	}






	/// <summary> The  older APIs are going to make getting individual
	///			  gpu information hard but its ok as I should only need
	///			  that once we use DX12 or Vulkan. Thus this is here as
	///			  a nice to have thing.
	/// </summary>
	Features DeviceFactory::getGpuInfo( _In_ const ID3D11DeviceCPtr& pDevice ) const
	{
		const auto intBoolToString = [ ]( _In_ const BOOL& boolean )
		{
			return boolean ? "True" : "False";
		};

		cout << left;

		//-------------------------------
		// GPU Details
		//-------------------------------
		dumpAdapterInfo( pAdapter, true );

#ifdef USE_DX11_4
		const auto boolToString = [ ]( _In_ const bool& boolean )
		{
			return boolean ? "True" : "False";
		};

		Features features;
		//-------------------------------
		// DXGI Features
		//-------------------------------
		BOOL allowTearing = FALSE;
		const HRESULT hr = pFactory->CheckFeatureSupport( DXGI_FEATURE_PRESENT_ALLOW_TEARING, &allowTearing, sizeof( allowTearing ) );
		features.allowTearing = SUCCEEDED( hr ) && allowTearing;

		cout << "\nDXGI Features\n";
		cout << "\tVariable Refresh Rate: " << boolToString( features.allowTearing ) << "\n";
#else
		const Features features;
#endif

		//-------------------------------
		// D3D Features
		//-------------------------------
		cout << "\nD3D Features\n";


		//-------------------------------------------------------------------------------------------------------------
		D3D11_FEATURE_DATA_THREADING threadingFeatures;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_THREADING, &threadingFeatures, sizeof( threadingFeatures ) ) ) )
		{
			cout << "\n\t+ Threading\n";

			cout << "\t  - Driver Command Lists       = " << intBoolToString( threadingFeatures.DriverCommandLists ) << "\n";
			//TRUE means command lists are supported by the current driver;
			//FALSE means that the API will emulate deferred contexts and command lists with software.
			cout << "\t  - Driver Concurrent Creates  = " << intBoolToString( threadingFeatures.DriverConcurrentCreates ) << "\n";
			//TRUE means resources can be created concurrently on multiple threads while drawing;
			//FALSE means that the presence of coarse synchronization will prevent concurrency.
		}
		//-------------------------------------------------------------------------------------------------------------
		D3D11_FEATURE_DATA_DOUBLES doubleFeatures;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_DOUBLES, &doubleFeatures, sizeof( doubleFeatures ) ) ) )
		{
			cout << "\n\t+ Doubles\n";
			cout << "\t  - Double Precision Float Shader Ops = " << intBoolToString( doubleFeatures.DoublePrecisionFloatShaderOps ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.1
//#if defined( WINDOWS_8 ) || defined( WINDOWS_10 )
		D3D11_FEATURE_DATA_ARCHITECTURE_INFO architectureFeatures;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_ARCHITECTURE_INFO, &architectureFeatures, sizeof( architectureFeatures ) ) ) )
		{
			cout << "\n\t+ Architecture\n";
			cout << "\t  - Tile Based Deferred Renderer      = " << intBoolToString( architectureFeatures.TileBasedDeferredRenderer ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.1
		D3D11_FEATURE_DATA_SHADER_MIN_PRECISION_SUPPORT precisionSupport;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_SHADER_MIN_PRECISION_SUPPORT, &precisionSupport, sizeof( precisionSupport ) ) ) )
		{
			cout << "\n\t+ Shader Min Precision Support\n";
			cout << "\t  - Pixel Shader Min Precision            = " << precisionSupport.PixelShaderMinPrecision << "\n";
			cout << "\t  - All Other Shader Stages Min Precision = " << precisionSupport.AllOtherShaderStagesMinPrecision << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.2
		D3D11_FEATURE_DATA_MARKER_SUPPORT markerSupport;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_MARKER_SUPPORT, &markerSupport, sizeof( markerSupport ) ) ) )
		{
			cout << "\n\t+ Marker Support\n";
			cout << "\t  - Profile                               = " << intBoolToString( markerSupport.Profile ) << " (Specifies whether the hardware and driver support a GPU profiling technique that can be used with development tools.)\n";
		}
//#endif
		//-------------------------------------------------------------------------------------------------------------
//#ifdef WINDOWS_10
		D3D11_FEATURE_DATA_GPU_VIRTUAL_ADDRESS_SUPPORT gpuVirtualAddressSupport;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_GPU_VIRTUAL_ADDRESS_SUPPORT, &gpuVirtualAddressSupport, sizeof( gpuVirtualAddressSupport ) ) ) )
		{
			cout << "\n\t+ GPU Virtual Address Support\n";
			cout << "\t  - Max GPU Virtual Address Bits Per Process  = " << gpuVirtualAddressSupport.MaxGPUVirtualAddressBitsPerProcess << "\n";
			cout << "\t  - Max GPU Virtual Address Bits Per Resource = " << gpuVirtualAddressSupport.MaxGPUVirtualAddressBitsPerResource << "\n";
		}
//#endif
		//------------------------------------------------------------------------------------------------------------- 11.1
//#if defined( WINDOWS_8 ) || defined( WINDOWS_10 )
		D3D11_FEATURE_DATA_D3D9_OPTIONS d3d9Options;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D9_OPTIONS, &d3d9Options, sizeof( d3d9Options ) ) ) )
		{
			cout << "\n\t+ D3D9 Options\n";
			cout << "\t  - Full Non Pow 2 Texture Support                                        = " << intBoolToString( d3d9Options.FullNonPow2TextureSupport ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.2
		D3D11_FEATURE_DATA_D3D9_OPTIONS1 d3d9Options1;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D9_OPTIONS1, &d3d9Options1, sizeof( d3d9Options1 ) ) ) )
		{
			cout << "\n\t+ D3D9 Options 1\n";
			cout << "\t  - Full Non Pow 2 Texture Support                                        = " << intBoolToString( d3d9Options1.FullNonPow2TextureSupported ) << "\n";
			cout << "\t  - Simple Instancing Supported                                           = " << intBoolToString( d3d9Options1.SimpleInstancingSupported ) << "\n";
			cout << "\t  - Depth As Texture With Less Equal Comparison Filter Supported          = " << intBoolToString( d3d9Options1.DepthAsTextureWithLessEqualComparisonFilterSupported ) << "\n";
			cout << "\t  - Texture Cube Face Render Target With Non Cube Depth Stencil Supported = " << intBoolToString( d3d9Options1.TextureCubeFaceRenderTargetWithNonCubeDepthStencilSupported ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.1
		D3D11_FEATURE_DATA_D3D9_SHADOW_SUPPORT d3d9ShadowSupport;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D9_SHADOW_SUPPORT, &d3d9ShadowSupport, sizeof( d3d9ShadowSupport ) ) ) )
		{
			cout << "\n\t+ D3D9 Shadow Support\n";
			cout << "\t  - Supporteds Depth As Texture With Less Equal Comparison Filter         = " << intBoolToString( d3d9ShadowSupport.SupportsDepthAsTextureWithLessEqualComparisonFilter ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.2
		D3D11_FEATURE_DATA_D3D9_SIMPLE_INSTANCING_SUPPORT d3d9SimpleInstancingSupport;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D9_SIMPLE_INSTANCING_SUPPORT, &d3d9SimpleInstancingSupport, sizeof( d3d9SimpleInstancingSupport ) ) ) )
		{
			cout << "\n\t+ D3D9 Simple Instancing Support\n";
			cout << "\t  - Simple Instancing Supported                                           = " << intBoolToString( d3d9SimpleInstancingSupport.SimpleInstancingSupported ) << "\n";
		}
		//-------------------------------------------------------------------------------------------------------------
		D3D11_FEATURE_DATA_D3D10_X_HARDWARE_OPTIONS d3d10xHardwareOptions;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D10_X_HARDWARE_OPTIONS, &d3d10xHardwareOptions, sizeof( d3d10xHardwareOptions ) ) ) )
		{
			cout << "\n\t+ D3D10 X Hardware Options\n";
			cout << "\t  - Compute Shaders Plus Raw And Structured Buffers Via Shader 4.x        = " << intBoolToString( d3d10xHardwareOptions.ComputeShaders_Plus_RawAndStructuredBuffers_Via_Shader_4_x ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.1
		D3D11_FEATURE_DATA_D3D11_OPTIONS d3d11Options;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D11_OPTIONS, &d3d11Options, sizeof( d3d11Options ) ) ) )
		{
			cout << "\n\t+ D3D11 Options\n";
			cout << "\t  - Output Merger Logic Op                       = " << intBoolToString( d3d11Options.OutputMergerLogicOp ) << "\n";
			cout << "\t  - UAV Only Rendering Forced Sample Count       = " << intBoolToString( d3d11Options.UAVOnlyRenderingForcedSampleCount ) << "\n";
			cout << "\t  - Discard APIs Seen By Driver                  = " << intBoolToString( d3d11Options.DiscardAPIsSeenByDriver ) << "\n"; // driver supports the ID3D11DeviceContext1::DiscardView and ID3D11DeviceContext1::DiscardResource method
			cout << "\t  - Flags For Update And Copy Seen By Driver     = " << intBoolToString( d3d11Options.FlagsForUpdateAndCopySeenByDriver ) << "\n"; // driver supports new semantics for copy and update that are exposed by the ID3D11DeviceContext1::CopySubresourceRegion1 and ID3D11DeviceContext1::UpdateSubresource1 methods
			cout << "\t  - Clear View                                   = " << intBoolToString( d3d11Options.ClearView ) << "\n";
			cout << "\t  - Copy With Overlap                            = " << intBoolToString( d3d11Options.CopyWithOverlap ) << "\n";
			cout << "\t  - Constant Buffer Partial Update               = " << intBoolToString( d3d11Options.ConstantBufferPartialUpdate ) << "\n";
			cout << "\t  - Constant Buffer Offsetting                   = " << intBoolToString( d3d11Options.ConstantBufferOffsetting ) << "\n";
			cout << "\t  - Map No Overwrite On Dynamic Constant Buffer  = " << intBoolToString( d3d11Options.MapNoOverwriteOnDynamicConstantBuffer ) << "\n";
			cout << "\t  - Map No Overwrite On Dynamic Buffer SRV       = " << intBoolToString( d3d11Options.MapNoOverwriteOnDynamicBufferSRV ) << "\n";
			cout << "\t  - Multisample RTV With Forced Sample Count One = " << intBoolToString( d3d11Options.MultisampleRTVWithForcedSampleCountOne ) << "\n";
			cout << "\t  - SAD4 Shader Instructions                     = " << intBoolToString( d3d11Options.SAD4ShaderInstructions ) << "\n";
			cout << "\t  - Extended Doubles Shader Instructions         = " << intBoolToString( d3d11Options.ExtendedDoublesShaderInstructions ) << "\n";
			cout << "\t  - Extended Resource Sharing                    = " << intBoolToString( d3d11Options.ExtendedResourceSharing ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.2
		D3D11_FEATURE_DATA_D3D11_OPTIONS1 d3d11Options1;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D11_OPTIONS1, &d3d11Options1, sizeof( d3d11Options1 ) ) ) )
		{
			cout << "\n\t+ D3D11 Options 1\n";
			cout << "\t  - Min Max Filtering                           = " << intBoolToString( d3d11Options1.MinMaxFiltering ) << "\n";
			cout << "\t  - Clear View Also Supports Depth Only Formats = " << intBoolToString( d3d11Options1.ClearViewAlsoSupportsDepthOnlyFormats ) << "\n";
			cout << "\t  - Map On Default Buffers                      = " << intBoolToString( d3d11Options1.MapOnDefaultBuffers ) << "\n";
			cout << "\t  - Tiled Resources Tier                        = ";
			switch( d3d11Options1.TiledResourcesTier )
			{
			case D3D11_TILED_RESOURCES_NOT_SUPPORTED: cout << "Not Supported"; break;
			case D3D11_TILED_RESOURCES_TIER_1: cout << "Tier 1"; break;
			case D3D11_TILED_RESOURCES_TIER_2: cout << "Tier 2"; break;
//#ifdef WINDOWS_10
			case D3D11_TILED_RESOURCES_TIER_3: cout << "Tier 3"; break;
//#endif
			}
			cout << "\n";
		}
//#endif
		//------------------------------------------------------------------------------------------------------------- 11.3
//#ifdef WINDOWS_10
		D3D11_FEATURE_DATA_D3D11_OPTIONS2 d3d11Options2;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D11_OPTIONS2, &d3d11Options2, sizeof( d3d11Options2 ) ) ) )
		{
			cout << "\n\t+ D3D11 Options 2\n";
			cout << "\t  - PS Specified Stencil Ref Supported = " << intBoolToString( d3d11Options2.PSSpecifiedStencilRefSupported ) << "\n";
			cout << "\t  - Typed UAV Load Additional Formats  = " << intBoolToString( d3d11Options2.TypedUAVLoadAdditionalFormats ) << "\n";
			cout << "\t  - ROVs Supported                     = " << intBoolToString( d3d11Options2.ROVsSupported ) << "\n";
			cout << "\t  - Map On Default Textures            = " << intBoolToString( d3d11Options2.MapOnDefaultTextures ) << "\n";
			cout << "\t  - Standard Swizzle                   = " << intBoolToString( d3d11Options2.StandardSwizzle ) << "\n";
			cout << "\t  - Unified Memory Architecture        = " << intBoolToString( d3d11Options2.UnifiedMemoryArchitecture ) << "\n";
			cout << "\t  - Tiled Resources Tier               = ";
			switch( d3d11Options2.TiledResourcesTier )
			{
			case D3D11_TILED_RESOURCES_NOT_SUPPORTED: cout << "Not Supported"; break;
			case D3D11_TILED_RESOURCES_TIER_1: cout << "Tier 1"; break;
			case D3D11_TILED_RESOURCES_TIER_2: cout << "Tier 2"; break;
			case D3D11_TILED_RESOURCES_TIER_3: cout << "Tier 3"; break;
			}
			cout << "\n";
			cout << "\t  - Conservative Rasterization Tier    = ";
			switch( d3d11Options2.ConservativeRasterizationTier )
			{
			case D3D11_CONSERVATIVE_RASTERIZATION_NOT_SUPPORTED: cout << "Not Supported"; break;
			case D3D11_CONSERVATIVE_RASTERIZATION_TIER_1: cout << "Tier 1"; break;
			case D3D11_CONSERVATIVE_RASTERIZATION_TIER_2: cout << "Tier 2"; break;
			case D3D11_CONSERVATIVE_RASTERIZATION_TIER_3: cout << "Tier 3"; break;
			}
			cout << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.4
		D3D11_FEATURE_DATA_D3D11_OPTIONS3 d3d11Options3;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D11_OPTIONS3, &d3d11Options3, sizeof( d3d11Options3 ) ) ) )
		{
			cout << "\n\t+ D3D11 Options 3\n";
			cout << "\t  - VP And RT Array Index From Any Shader Feeding Rasterizer = " << intBoolToString( d3d11Options3.VPAndRTArrayIndexFromAnyShaderFeedingRasterizer ) << "\n";
		}
		//------------------------------------------------------------------------------------------------------------- 11.4
#ifdef USE_DX11_4
		D3D11_FEATURE_DATA_D3D11_OPTIONS4 d3d11Options4;
		if( SUCCEEDED( pDevice->CheckFeatureSupport( D3D11_FEATURE_D3D11_OPTIONS4, &d3d11Options4, sizeof( d3d11Options4 ) ) ) )
		{
			cout << "\n\t+ D3D11 Options 4\n";
			cout << "\t  - Extended NV12 Shared Texture Supported = " << intBoolToString( d3d11Options4.ExtendedNV12SharedTextureSupported ) << "\n";
		}
#endif
//#endif


		//-------------------------------------------------------------------------------------------------------------
		dumpTextureSupport( pDevice );

		cout << internal;

		return features;
	}

	/// <summary> Print out the provided adaptors information.
	/// </summary>
	void DeviceFactory::dumpAdapterInfo( _In_ const AdapterPtr_t& pAdapter, _In_opt_ bool showResolutions ) const
	{
		DXGI_ADAPTER_DESC adapterDesc;
		if( FAILED( pAdapter->GetDesc( &adapterDesc ) ) )
		{
			cout << "Call to GetDesc1 failed to create DXGI_ADAPTER_DESC1!\n";
			return;
		}

		const auto printGb = [ ]( size_t bytes )
		{
			double gb = floor( double( bytes ) / double( 1024 * 1024 * 10.24 ) ) / 100;

			if( gb == 0 )
			{
				cout << "0.00";
			}
			else
			{
				cout << gb;
			}

			cout << " GB\n";
		};

		cout << "\t---------------------------------------------------\n";
		wcout << "\t Description       : " << adapterDesc.Description << "\n";
		cout << "\t  - Vendor ID      : " << adapterDesc.VendorId << "\n";
		cout << "\t  - Device ID      : " << adapterDesc.DeviceId << "\n";
		cout << "\t  - SubSys ID      : " << adapterDesc.SubSysId << "\n";
		cout << "\t  - Revision       : " << adapterDesc.Revision << "\n";
		cout << "\t  - Dedicated VRAM : "; printGb( adapterDesc.DedicatedVideoMemory );
		cout << "\t  - Dedicated RAM  : "; printGb( adapterDesc.DedicatedSystemMemory );
		cout << "\t  - Shared RAM     : "; printGb( adapterDesc.SharedSystemMemory );



#ifdef USE_DX11_1
		DXGI_ADAPTER_DESC2 adapterDesc2;
		if( FAILED( pAdapter->GetDesc2( &adapterDesc2 ) ) )
		{
			cout << "Call to GetDesc2 failed to create DXGI_ADAPTER_DESC2!\n";
			return;
		}

		cout << "\t  - Preemption FX  : ";

		switch( adapterDesc2.GraphicsPreemptionGranularity )
		{
		case DXGI_GRAPHICS_PREEMPTION_DMA_BUFFER_BOUNDARY:
			cout << "DMA Buffer\n";
			break;
		case DXGI_GRAPHICS_PREEMPTION_PRIMITIVE_BOUNDARY:
			cout << "Primative\n";
			break;
		case DXGI_GRAPHICS_PREEMPTION_TRIANGLE_BOUNDARY:
			cout << "Triangle\n";
			break;
		case DXGI_GRAPHICS_PREEMPTION_PIXEL_BOUNDARY:
			cout << "Pixel\n";
			break;
		case DXGI_GRAPHICS_PREEMPTION_INSTRUCTION_BOUNDARY:
			cout << "Instruction\n";
			break;
		}

		cout << "\t  - Preemption GP  : ";

		switch( adapterDesc2.ComputePreemptionGranularity )
		{
		case DXGI_COMPUTE_PREEMPTION_DMA_BUFFER_BOUNDARY:
			cout << "DMA Buffer\n";
			break;
		case DXGI_COMPUTE_PREEMPTION_DISPATCH_BOUNDARY:
			cout << "Dispatch\n";
			break;
		case DXGI_COMPUTE_PREEMPTION_THREAD_GROUP_BOUNDARY:
			cout << "Thread Group\n";
			break;
		case DXGI_COMPUTE_PREEMPTION_THREAD_BOUNDARY:
			cout << "Thread\n";
			break;
		case DXGI_COMPUTE_PREEMPTION_INSTRUCTION_BOUNDARY:
			cout << "Instruction\n";
			break;
		}
#endif


		DXGI_OUTPUT_DESC outputDesc;
		IDXGIOutputCPtr pOutput0;
		OutputPtr_t pOutput;
		UINT i = 0;
		HRESULT result;
		// Enumerate the adapter outputs (monitors).
		while( pAdapter->EnumOutputs( i, &pOutput0 ) != DXGI_ERROR_NOT_FOUND )
		{
#if defined USE_DX11_1
			result = pOutput0.As( &pOutput );
			if( FAILED( result ) )
			{
				break;
			}
#else
			pOutput = pOutput0;
#endif

			result = pOutput->GetDesc( &outputDesc );
			if( FAILED( result ) )
			{
				break;
			}

			wcout << "\t " << outputDesc.DeviceName << "\n";

			if( showResolutions )
			{
				const DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;// 32bit RGBA colour 0xRRGGBBAA
				const UINT flags = DXGI_ENUM_MODES_INTERLACED;		// Get all the interlaced modes as well as the progressive modes.

				UINT numModes;

#if defined USE_DX11_1
				result = pOutput->GetDisplayModeList1( format, flags, &numModes, nullptr );
#else
				result = pOutput->GetDisplayModeList( format, flags, &numModes, nullptr );
#endif
				if( FAILED( result ) )
				{
					break;
				}

#if defined USE_DX11_1
				vector< DXGI_MODE_DESC1 > modes;
				modes.resize( numModes );
				result = pOutput->GetDisplayModeList1( format, flags, &numModes, &modes[ 0 ] );
#else
				vector< DXGI_MODE_DESC > modes;
				modes.resize( numModes );
				result = pOutput->GetDisplayModeList( format, flags, &numModes, &modes[ 0 ] );
#endif

				if( FAILED( result ) )
				{
					break;
				}

#if defined USE_DX11_1
				for( const DXGI_MODE_DESC1& desc : modes )
#else
				for( const DXGI_MODE_DESC& desc : modes )
#endif
				{
					/**/
					if( desc.Scaling != DXGI_MODE_SCALING_UNSPECIFIED )
					{
						continue;
					}
					//*/

					cout << "\t  - " << setw( 4 ) << desc.Width
						<< " x " << setw( 4 ) << desc.Height;

					switch( desc.ScanlineOrdering )
					{
						case DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED:
							//cout << "UNSPECIFIED";
							cout << " ";
							break;
						case DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE:
							//cout << "PROGRESSIVE";
							cout << "p";
							break;
						case DXGI_MODE_SCANLINE_ORDER_UPPER_FIELD_FIRST:
							//cout << "UPPER_FIELD_FIRST";
							cout << "i";
							break;
						case DXGI_MODE_SCANLINE_ORDER_LOWER_FIELD_FIRST:
							//cout << "LOWER_FIELD_FIRST";
							cout << "i\\";
							break;
					}

					const unsigned int precision = 0;
					const unsigned int width = 3;
					cout << " @ "
						<< fixed << setw( width ) << setprecision( precision )
						<< ( static_cast<float>( desc.RefreshRate.Numerator ) / desc.RefreshRate.Denominator ) << "hz ";

					/**
					cout << " Scaling: ";

					switch( desc.Scaling )
					{
						case DXGI_MODE_SCALING_UNSPECIFIED:
							cout << "UNSPECIFIED";
							break;
						case DXGI_MODE_SCALING_CENTERED:
							cout << "CENTERED   ";
							break;
						case DXGI_MODE_SCALING_STRETCHED:
							cout << "STRETCHED  ";
							break;
					}
					//*/

					const double ratio = static_cast<double>( desc.Width ) / desc.Height;
					const double ratio1 = 16.0 / 9.0;
					const double ratio5 =  3.0 / 2.0;
					const double ratio2 =  4.0 / 3.0;
					const double ratio6 =  5.0 / 3.0;
					const double ratio3 =  5.0 / 4.0;
					const double ratio4 =  8.0 / 5.0;
					const double ratio7 = 17.0 / 9.0;
					const double ratio8 = 21.0 / 9.0;
					const int ulp = 2;


					if( almostEqual( ratio, ratio1, ulp ) ) { cout << "(16: 9)"; }
					else if( desc.Width == 2715 && desc.Height == 1527 ) { cout << "(16:~9)"; }
					else if( almostEqual( ratio, ratio2, ulp ) ) { cout << "( 4: 3)"; }
					else if( almostEqual( ratio, ratio3, ulp ) ) { cout << "( 5: 4)"; }
					else if( almostEqual( ratio, ratio4, ulp ) ) { cout << "( 8: 5)"; }
					else if( almostEqual( ratio, ratio5, ulp ) ) { cout << "( 3: 2)"; }
					else if( almostEqual( ratio, ratio6, ulp ) ) { cout << "( 5: 3)"; }
					else if( almostEqual( ratio, ratio7, ulp ) ) { cout << "(17: 9)"; }
					else if( almostEqual( ratio, ratio8, ulp ) ) { cout << "(21: 9)"; }
					else { cout << "(  :  )"; }

					// 16:9
					if     ( desc.Width ==  854 && desc.Height ==  480 ) { cout << " WVGA (NTSC)"; }
					else if( desc.Width == 1024 && desc.Height ==  576 ) { cout << " PAL"; }
					else if( desc.Width == 1280 && desc.Height ==  720 ) { cout << " HD"; }
					else if( desc.Width == 1920 && desc.Height == 1080 ) { cout << " FHD"; }
					else if( desc.Width == 2560 && desc.Height == 1440 ) { cout << " WQHD"; }
					else if( desc.Width == 3840 && desc.Height == 2160 ) { cout << " UHD-1"; }

					// 4:3
					else if( desc.Width ==  320 && desc.Height ==  240 ) { cout << " QVGA"; }
					else if( desc.Width ==  384 && desc.Height ==  288 ) { cout << " SIF"; }
					else if( desc.Width ==  640 && desc.Height ==  480 ) { cout << " VGA (NTSC)"; }
					else if( desc.Width ==  768 && desc.Height ==  576 ) { cout << " PAL"; }
					else if( desc.Width ==  800 && desc.Height ==  600 ) { cout << " SVGA"; }
					else if( desc.Width == 1024 && desc.Height ==  768 ) { cout << " XGA"; }
					else if( desc.Width == 1152 && desc.Height ==  864 ) { cout << " XGA+"; }
					else if( desc.Width == 1440 && desc.Height == 1050 ) { cout << " SXGA+"; }
					else if( desc.Width == 1600 && desc.Height == 1200 ) { cout << " UXGA"; }
					else if( desc.Width == 2048 && desc.Height == 1536 ) { cout << " QXGA"; }

					// 8:5
					else if( desc.Width == 2560 && desc.Height == 1600 ) { cout << " WQXGA"; }
					else if( desc.Width == 1920 && desc.Height == 1200 ) { cout << " WUCGA"; }
					else if( desc.Width == 1680 && desc.Height == 1050 ) { cout << " WSXGA"; }
					else if( desc.Width == 1280 && desc.Height ==  800 ) { cout << " WXGA"; }
					else if( desc.Width ==  320 && desc.Height ==  200 ) { cout << " CGA"; }

					// 5:4
					else if( desc.Width == 1280 && desc.Height == 1024 ) { cout << " SXGA"; }
					else if( desc.Width == 2560 && desc.Height == 2048 ) { cout << " QSXGA"; }

					// 3:2
					else if( desc.Width ==  480 && desc.Height ==  320 ) { cout << " HVGA"; }

					// 21:9
					else if( desc.Width == 2560 && desc.Height == 1080 ) { cout << " UWHD"; }

					// 
					else if( desc.Width == 3440 && desc.Height == 1440 ) { cout << " WSVGA"; }

					// ~ 17:9
					else if( desc.Width ==  800 && desc.Height ==  480 ) { cout << " WVGA"; }
					else if( desc.Width == 2048 /*&&desc.Height==1080*/) { cout << " \"Cinema\" 2K"; }
					else if( desc.Width == 4096 /*&&desc.Height==2160*/) { cout << " \"Cinema\" 4K"; }
					else if( desc.Width == 7680 && desc.Height == 4320 ) { cout << " 8K"; }

					// tv
					else if( desc.Height == 480 ) { cout << " NTSC*"; }
					else if( desc.Height == 576 ) { cout << " PAL*"; }

					// NVidia DSR Options (on my machine mines UHD-1)
					else if( desc.Width == 2715 && desc.Height == 1527 ) { cout << " DSR - HD (2.00x)"; }

#if defined USE_DX11_1
					if( desc.Stereo ) { cout << " Stereo"; }
#endif
					cout << "\n";
				}
			}

			++i;
		}
		//if( FAILED( result ) )
		//{
		//	return;
		//}
	}

	/// <summary> Some helpers here to check DX features. </summary>

	inline bool formatSuppoted1( _In_ const ID3D11DeviceCPtr& pDevice, _In_ const DXGI_FORMAT Format, _In_ const D3D11_FORMAT_SUPPORT support1 )
	{
		D3D11_FEATURE_DATA_FORMAT_SUPPORT formatSupprt = { Format, static_cast<UINT>( support1 ) };
		return FAILED( pDevice->CheckFeatureSupport( D3D11_FEATURE_FORMAT_SUPPORT, &formatSupprt, sizeof( formatSupprt ) ) );
	}

	inline bool formatSuppoted2( _In_ const ID3D11DeviceCPtr& pDevice, _In_ const DXGI_FORMAT Format, _In_ const D3D11_FORMAT_SUPPORT2 support2 )
	{
		D3D11_FEATURE_DATA_FORMAT_SUPPORT formatSupprt = { Format, static_cast<UINT>( support2 ) };
		return FAILED( pDevice->CheckFeatureSupport( D3D11_FEATURE_FORMAT_SUPPORT2, &formatSupprt, sizeof( formatSupprt ) ) );
	}

	inline UINT msaaSupport( _In_ const ID3D11DeviceCPtr& pDevice, _In_ const DXGI_FORMAT Format, _In_ const UINT count )
	{
		UINT result = 0;
		return FAILED( pDevice->CheckMultisampleQualityLevels( Format, count, &result ) );
	}

	inline void printFormatSupport1( _In_ const ID3D11DeviceCPtr& pDevice, _In_ const DXGI_FORMAT Format, _In_ const D3D11_FORMAT_SUPPORT support1, _In_ const string& supportName )
	{
		if( formatSuppoted1( pDevice, Format, support1 ) )
		{
			cout << "\t\t" << supportName << "\n";
		}
	}

	inline void printFormatSupport2( _In_ const ID3D11DeviceCPtr& pDevice, _In_ const DXGI_FORMAT Format, _In_ const D3D11_FORMAT_SUPPORT2 support2, _In_ const string& supportName )
	{
		if( formatSuppoted2( pDevice, Format, support2 ) )
		{
			cout << "\t\t" << supportName << "\n";
		}
	}

	inline void printFormatSupport( _In_ const ID3D11DeviceCPtr& pDevice, _In_ const DXGI_FORMAT Format )
	{
		cout << "MSAA:";
		bool msaaSupported = false;
		for( UINT i = 2U; i <= D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT; i *= 2 )
		{
			const UINT qualityLevel = msaaSupport( pDevice, Format, i );
			if( qualityLevel > 0 )
			{
				cout << " " << i << "x";
		
				if( qualityLevel > 1 )
				{
					cout << "[" << qualityLevel << "]";
				}
		
				msaaSupported = true;
			}
		}
		
		if( !msaaSupported )
		{
			cout << "Not Supported";
		}

		/**/
		cout << "\n";


		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_MIP, "MIP" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_MIP_AUTOGEN, "MIP_AUTOGEN" ); // Depricated!
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_BUFFER, "BUFFER" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_DISPLAY, "DISPLAY" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_BLENDABLE, "BLENDABLE" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_SO_BUFFER, "SO_BUFFER" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_TEXTURE1D, "TEXTURE1D" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_TEXTURE2D, "TEXTURE2D" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_TEXTURE3D, "TEXTURE3D" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_TEXTURECUBE, "TEXTURECUBE" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_RENDER_TARGET, "RENDER_TARGET" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_DEPTH_STENCIL, "DEPTH_STENCIL" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_IA_INDEX_BUFFER, "IA_INDEX_BUFFER" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_IA_VERTEX_BUFFER, "IA_VERTEX_BUFFER" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_MULTISAMPLE_LOAD, "MULTISAMPLE_LOAD" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_MULTISAMPLE_RESOLVE, "MULTISAMPLE_RESOLVE" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_MULTISAMPLE_RENDERTARGET, "MULTISAMPLE_RENDERTARGET" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_CAST_WITHIN_BIT_LAYOUT, "CAST_WITHIN_BIT_LAYOUT" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_BACK_BUFFER_CAST, "BACK_BUFFER_CAST" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_TYPED_UNORDERED_ACCESS_VIEW, "TYPED_UNORDERED_ACCESS_VIEW" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_SHADER_LOAD, "SHADER_LOAD" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_SHADER_SAMPLE, "SHADER_SAMPLE" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_SHADER_SAMPLE_MONO_TEXT, "SHADER_SAMPLE_MONO_TEXT" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_SHADER_SAMPLE_COMPARISON, "SHADER_SAMPLE_COMPARISON" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_SHADER_GATHER, "SHADER_GATHER" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_SHADER_GATHER_COMPARISON, "SHADER_GATHER_COMPARISON" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_CPU_LOCKABLE, "CPU_LOCKABLE" ); // Depricated!
//#if defined( WINDOWS_8 ) || defined( WINDOWS_10 )
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_DECODER_OUTPUT, "DECODER_OUTPUT" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_VIDEO_ENCODER, "VIDEO_ENCODER" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_VIDEO_PROCESSOR_OUTPUT, "VIDEO_PROCESSOR_OUTPUT" );
		printFormatSupport1( pDevice, Format, D3D11_FORMAT_SUPPORT_VIDEO_PROCESSOR_INPUT, "VIDEO_PROCESSOR_INPUT" );
//#endif


		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_ATOMIC_ADD, "UAV_ATOMIC_ADD" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_ATOMIC_BITWISE_OPS, "UAV_ATOMIC_BITWISE_OPS" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_ATOMIC_COMPARE_STORE_OR_COMPARE_EXCHANGE, "UAV_ATOMIC_COMPARE_STORE_OR_COMPARE_EXCHANGE" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_ATOMIC_EXCHANGE, "UAV_ATOMIC_EXCHANGE" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_ATOMIC_SIGNED_MIN_OR_MAX, "UAV_ATOMIC_SIGNED_MIN_OR_MAX" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_ATOMIC_UNSIGNED_MIN_OR_MAX, "UAV_ATOMIC_UNSIGNED_MIN_OR_MAX" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_TYPED_LOAD, "UAV_TYPED_LOAD" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_UAV_TYPED_STORE, "UAV_TYPED_STORE" );
//#if defined( WINDOWS_8 ) || defined( WINDOWS_10 )
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_OUTPUT_MERGER_LOGIC_OP, "OUTPUT_MERGER_LOGIC_OP" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_TILED, "TILED" );
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_SHAREABLE, "SHAREABLE" ); // Depricated!
//#endif
//#ifdef WINDOWS_10
		printFormatSupport2( pDevice, Format, D3D11_FORMAT_SUPPORT2_MULTIPLANE_OVERLAY, "MULTIPLANE_OVERLAY" );
//#endif
		//*/
	}

#define PrintFormat( FORMAT )\
	cout << "\t" << setw( 38 ) << left << #FORMAT << ": ";\
	printFormatSupport( pDevice, FORMAT );\
	cout << "\n";

	/// <summary> Helper for DumpGPU so that we can get the supported texture formats. </summary>
	void DeviceFactory::dumpTextureSupport( _In_ const ID3D11DeviceCPtr& pDevice )
	{
		cout << "\n";

		PrintFormat( DXGI_FORMAT_R8G8B8A8_UNORM );
		PrintFormat( DXGI_FORMAT_R16G16B16A16_FLOAT );
		PrintFormat( DXGI_FORMAT_R32G32B32A32_FLOAT );
		PrintFormat( DXGI_FORMAT_D24_UNORM_S8_UINT );

		/**
		PrintFormat( DXGI_FORMAT_R32G32_FLOAT );
//#if defined( WINDOWS_8 ) || defined( WINDOWS_10 )
		PrintFormat( DXGI_FORMAT_AYUV );
//#elif defined( WINDOWS_7 )
		cout << "\t" << setw( 38 ) << left << "DXGI_FORMAT_AYUV" << ": Not Supported";
//#endif
		//*/

		/**
		PrintFormat( DXGI_FORMAT_R32G32B32A32_TYPELESS );
		PrintFormat( DXGI_FORMAT_R32G32B32A32_UINT );
		PrintFormat( DXGI_FORMAT_R32G32B32A32_SINT );
		PrintFormat( DXGI_FORMAT_R32G32B32_TYPELESS );
		PrintFormat( DXGI_FORMAT_R32G32B32_FLOAT );
		PrintFormat( DXGI_FORMAT_R32G32B32_UINT );
		PrintFormat( DXGI_FORMAT_R32G32B32_SINT );
		PrintFormat( DXGI_FORMAT_R16G16B16A16_TYPELESS );
		PrintFormat( DXGI_FORMAT_R16G16B16A16_FLOAT );
		PrintFormat( DXGI_FORMAT_R16G16B16A16_UNORM );
		PrintFormat( DXGI_FORMAT_R16G16B16A16_UINT  );
		PrintFormat( DXGI_FORMAT_R16G16B16A16_SNORM );
		PrintFormat( DXGI_FORMAT_R16G16B16A16_SINT );
		PrintFormat( DXGI_FORMAT_R32G32_TYPELESS );
		PrintFormat( DXGI_FORMAT_R32G32_UINT );
		PrintFormat( DXGI_FORMAT_R32G32_SINT );
		PrintFormat( DXGI_FORMAT_R32G8X24_TYPELESS );
		PrintFormat( DXGI_FORMAT_D32_FLOAT_S8X24_UINT );
		PrintFormat( DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS );
		PrintFormat( DXGI_FORMAT_X32_TYPELESS_G8X24_UINT );
		PrintFormat( DXGI_FORMAT_R10G10B10A2_TYPELESS );
		PrintFormat( DXGI_FORMAT_R10G10B10A2_UNORM );
		PrintFormat( DXGI_FORMAT_R10G10B10A2_UINT );
		PrintFormat( DXGI_FORMAT_R11G11B10_FLOAT );
		PrintFormat( DXGI_FORMAT_R8G8B8A8_TYPELESS );
		PrintFormat( DXGI_FORMAT_R8G8B8A8_UNORM_SRGB );
		PrintFormat( DXGI_FORMAT_R8G8B8A8_UINT );
		PrintFormat( DXGI_FORMAT_R8G8B8A8_SNORM  );
		PrintFormat( DXGI_FORMAT_R8G8B8A8_SINT );
		PrintFormat( DXGI_FORMAT_R16G16_TYPELESS );
		PrintFormat( DXGI_FORMAT_R16G16_FLOAT );
		PrintFormat( DXGI_FORMAT_R16G16_UNORM );
		PrintFormat( DXGI_FORMAT_R16G16_UINT  );
		PrintFormat( DXGI_FORMAT_R16G16_SNORM );
		PrintFormat( DXGI_FORMAT_R16G16_SINT  );
		PrintFormat( DXGI_FORMAT_R32_TYPELESS );
		PrintFormat( DXGI_FORMAT_D32_FLOAT );
		PrintFormat( DXGI_FORMAT_R32_FLOAT );
		PrintFormat( DXGI_FORMAT_R32_UINT );
		PrintFormat( DXGI_FORMAT_R32_SINT );
		PrintFormat( DXGI_FORMAT_R24G8_TYPELESS );
		PrintFormat( DXGI_FORMAT_D24_UNORM_S8_UINT );
		PrintFormat( DXGI_FORMAT_R24_UNORM_X8_TYPELESS );
		PrintFormat( DXGI_FORMAT_X24_TYPELESS_G8_UINT );
		PrintFormat( DXGI_FORMAT_R8G8_TYPELESS );
		PrintFormat( DXGI_FORMAT_R8G8_UNORM );
		PrintFormat( DXGI_FORMAT_R8G8_UINT );
		PrintFormat( DXGI_FORMAT_R8G8_SNORM );
		PrintFormat( DXGI_FORMAT_R8G8_SINT );
		PrintFormat( DXGI_FORMAT_R16_TYPELESS );
		PrintFormat( DXGI_FORMAT_R16_FLOAT );
		PrintFormat( DXGI_FORMAT_D16_UNORM );
		PrintFormat( DXGI_FORMAT_R16_UNORM );
		PrintFormat( DXGI_FORMAT_R16_UINT );
		PrintFormat( DXGI_FORMAT_R16_SNORM );
		PrintFormat( DXGI_FORMAT_R16_SINT );
		PrintFormat( DXGI_FORMAT_R8_TYPELESS );
		PrintFormat( DXGI_FORMAT_R8_UNORM );
		PrintFormat( DXGI_FORMAT_R8_UINT  );
		PrintFormat( DXGI_FORMAT_R8_SNORM );
		PrintFormat( DXGI_FORMAT_R8_SINT  );
		PrintFormat( DXGI_FORMAT_A8_UNORM );
		PrintFormat( DXGI_FORMAT_R1_UNORM );
		PrintFormat( DXGI_FORMAT_R9G9B9E5_SHAREDEXP );
		PrintFormat( DXGI_FORMAT_R8G8_B8G8_UNORM );
		PrintFormat( DXGI_FORMAT_G8R8_G8B8_UNORM );
		PrintFormat( DXGI_FORMAT_BC1_TYPELESS );
		PrintFormat( DXGI_FORMAT_BC1_UNORM );
		PrintFormat( DXGI_FORMAT_BC1_UNORM_SRGB );
		PrintFormat( DXGI_FORMAT_BC2_TYPELESS );
		PrintFormat( DXGI_FORMAT_BC2_UNORM );
		PrintFormat( DXGI_FORMAT_BC2_UNORM_SRGB );
		PrintFormat( DXGI_FORMAT_BC3_TYPELESS );
		PrintFormat( DXGI_FORMAT_BC3_UNORM );
		PrintFormat( DXGI_FORMAT_BC3_UNORM_SRGB );
		PrintFormat( DXGI_FORMAT_BC4_TYPELESS );
		PrintFormat( DXGI_FORMAT_BC4_UNORM );
		PrintFormat( DXGI_FORMAT_BC4_SNORM );
		PrintFormat( DXGI_FORMAT_BC5_TYPELESS );
		PrintFormat( DXGI_FORMAT_BC5_UNORM );
		PrintFormat( DXGI_FORMAT_BC5_SNORM );
		PrintFormat( DXGI_FORMAT_B5G6R5_UNORM );
		PrintFormat( DXGI_FORMAT_B5G5R5A1_UNORM );
		PrintFormat( DXGI_FORMAT_B8G8R8A8_UNORM );
		PrintFormat( DXGI_FORMAT_B8G8R8X8_UNORM );
		PrintFormat( DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM );
		PrintFormat( DXGI_FORMAT_B8G8R8A8_TYPELESS );
		PrintFormat( DXGI_FORMAT_B8G8R8A8_UNORM_SRGB );
		PrintFormat( DXGI_FORMAT_B8G8R8X8_TYPELESS );
		PrintFormat( DXGI_FORMAT_B8G8R8X8_UNORM_SRGB );
		PrintFormat( DXGI_FORMAT_BC6H_TYPELESS );
		PrintFormat( DXGI_FORMAT_BC6H_UF16 );
		PrintFormat( DXGI_FORMAT_BC6H_SF16 );
		PrintFormat( DXGI_FORMAT_BC7_TYPELESS );
		PrintFormat( DXGI_FORMAT_BC7_UNORM );
		PrintFormat( DXGI_FORMAT_BC7_UNORM_SRGB );
		PrintFormat( DXGI_FORMAT_AYUV );
		PrintFormat( DXGI_FORMAT_Y410 );
		PrintFormat( DXGI_FORMAT_Y416 );
		PrintFormat( DXGI_FORMAT_NV12 );
		PrintFormat( DXGI_FORMAT_P010 );
		PrintFormat( DXGI_FORMAT_P016 );
		PrintFormat( DXGI_FORMAT_420_OPAQUE );
		PrintFormat( DXGI_FORMAT_YUY2 );
		PrintFormat( DXGI_FORMAT_Y210 );
		PrintFormat( DXGI_FORMAT_Y216 );
		PrintFormat( DXGI_FORMAT_NV11 );
		PrintFormat( DXGI_FORMAT_AI44 );
		PrintFormat( DXGI_FORMAT_IA44 );
		PrintFormat( DXGI_FORMAT_P8 );
		PrintFormat( DXGI_FORMAT_A8P8 );
		PrintFormat( DXGI_FORMAT_B4G4R4A4_UNORM );

		PrintFormat( DXGI_FORMAT_P208 );
		PrintFormat( DXGI_FORMAT_V208 );
		PrintFormat( DXGI_FORMAT_V408 );
		//*/


		cout << "\n";
	}
} // namespace directX11
} // namespace visual
