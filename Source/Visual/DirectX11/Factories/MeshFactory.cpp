#include "MeshFactory.h"

// DirectX 11 Mesh factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : MeshFactory.cpp
// Date      : 17/11/2015
// Author    : Nicholas Welters

#include "ShaderFactory.h"
#include "Visual/FX/Shapes.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	using std::make_shared;
	using std::to_string;
	using std::vector;
	using std::string;
	using glm::vec3;
	using glm::vec2;
	using glm::uvec2;
	using fx::Vertex3f2f3f3f3f;
	using fx::Vertex3f2f;
	using fx::VertexParticle;
	using fx::Mesh;
	using fx::MeshPrimitive;
	using fx::Shapes;

	/// <summary> ctor </summary>
	/// <param name="pDevice"> Our GPU device. </param>
	/// <param name="pBufferFactory"> The buffer factory to manage the generic data structures. </param>
	/// <param name="pShaderFactory"> The shader factory to build vertex input layout validation shaders. </param>
	MeshFactory::MeshFactory( _In_ const ID3D11DeviceCPtr& pDevice,
							  _In_ const BufferFactorySPtr& pBufferFactory,
							  _In_ const ShaderFactorySPtr& pShaderFactory )
		: pDevice( pDevice )
		, pBufferFactory( pBufferFactory )
		, pShaderFactory( pShaderFactory )
		, inputLayoutLibrary( ID3D11_FACTORY( ID3D11InputLayout ), DX11_DELETER )
	{ }



	/// <summary> Create a vertex input layout. </summary>
	/// <param name="name"> The vertex type unique string id. </param>
	/// <param name="pDescriptor"> The vertex layour discriptor. </param>
	/// <param name="shader"> A shader that validates the input layout. </param>
	/// <param name="size"> The number of elements in the vertex. </param>
	/// <returns> The vertex input layout. </returns>
	ID3D11InputLayoutSPtr MeshFactory::createInputLayoutImpl( _In_ const string& name,
															  _In_ const D3D11_INPUT_ELEMENT_DESC* pDescriptor,
															  _In_ const string& shader,
															  _In_ const uint32_t size )
	{
		return inputLayoutLibrary.checkOut( name, [ this, name, pDescriptor, shader, size ]( )
		{
			ID3D11InputLayout* pInputLayout = nullptr;
			ID3DBlob* pVertexShaderBuffer = pShaderFactory->compileVertexShaderWithInputSignature( name, shader );

			ThrowIfFailed( pDevice->CreateInputLayout( pDescriptor,
													   size,
													   pVertexShaderBuffer->GetBufferPointer( ),
													   pVertexShaderBuffer->GetBufferSize( ),
													   &pInputLayout ) );

			pVertexShaderBuffer->Release( );

			setDebugName( pInputLayout, name );
			return pInputLayout;
		} );
	}



	/// <summary> Create an index buffer. </summary>
	/// <param name="id"> The vertex buffer unique string id. </param>
	/// <param name="buffer"> A vector of indecise to store in the buffer. </param>
	/// <returns> The buffer and the number of elements in the buffer. </returns>
	ArrayBuffer MeshFactory::createIndexBuffer( _In_ const string& id,
												_In_ const vector< uint32_t >& buffer ) const
	{
		return { static_cast< uint32_t >( buffer.size( ) ), pBufferFactory->createIndexBuffer( id, buffer ), sizeof( uint32_t ) };
	}


	/// <summary> Create a height map mesh buffer. </summary>
	/// <param name="fileName"> The location to a raw height map. </param>
	/// <param name="size"> The dimentions of the raw height map data. </param>
	/// <returns> A GPU mesh buffer to draw the geometry. </returns>
	MeshBufferSPtr MeshFactory::createHeightMap( _In_ const string& fileName, _In_ const uvec2& size )
	{
		// Mesh Dimentions
		const UINT heightMapSize = size.x * size.y;
		BYTE* heightMap = new BYTE[ heightMapSize ];

		// open file and test for a fail.
		// r - Read Only
		// b - Binary Format
		FILE* pFile = nullptr;
		int result = fopen_s( &pFile, fileName.c_str( ), "rb" );

		if( result != 0 )
		{
			MessageBox( nullptr,
						"Call to fopen_s( &pFile, fileName, \"rb\" )",
						"HeightMap File Loader",
						MB_ICONEXCLAMATION | MB_OK );
		}
		else
		{
			// Read binary data from file.
			fread( heightMap, 1, heightMapSize, pFile );

			result = ferror( pFile );
			if( result )
			{
				MessageBox( nullptr,
							"File Error - EOF?",
							"HeightMap File Loader",
							MB_ICONEXCLAMATION | MB_OK );

			}

			// Close the opened RAW file.
			fclose( pFile );
		}


		const Mesh< Vertex3f2f3f3f3f::Vertex > mesh = Shapes::heightMap( heightMap, size.x, 1.0f, -1.0, 25 );

		if( heightMap )
		{
			delete[] heightMap;
		}

		return createMeshBuffer< Vertex3f2f3f3f3f >( "HEIGHT_MAP_" + fileName, mesh );
	}

	/// <summary> Create a coordinate star mesh buffer, the vertices and colours match the axis. </summary>
	/// <returns> A GPU mesh buffer to draw the geometry. </returns>
	MeshBufferSPtr MeshFactory::createCoordStarMesh( )
	{
		return createMeshBuffer< Vertex3f2f3f3f3f >( "COORD_STAR", Shapes::coordStar( ) );
	}

	/// <summary> Create a cube mesh buffer. </summary>
	/// <returns> A GPU mesh buffer to draw the geometry. </returns>
	MeshBufferSPtr MeshFactory::createSlabMesh( )
	{
		return createMeshBuffer< Vertex3f2f3f3f3f >( "TEST_SLATE", Shapes::testSlate( ) );
	}

	/// <summary> Create a point mesh buffer. </summary>
	/// <returns> A GPU mesh buffer to draw the geometry. </returns>
	MeshBufferSPtr MeshFactory::createPointMesh( )
	{
		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( 1 );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( 1 );

		vBuff[ 0 ] = { { 0, 0, 0 },{ 1, 1 },{ 0, 0, 0 },{ 0, 0, 0 },{ 0, 0, 0 } };
		iBuff[ 0 ] = 0;

		const Mesh< Vertex3f2f3f3f3f::Vertex > mesh = Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::PointList );

		return createMeshBuffer< Vertex3f2f3f3f3f >( "TEST_SPHERE", mesh );
	}

	/// <summary> Create a flat plane. </summary>
	/// <returns> A GPU mesh buffer to draw the geometry. </returns>
	MeshBufferSPtr MeshFactory::createPlane( )
	{
		return createMeshBuffer< Vertex3f2f3f3f3f >( "TRI_STRIP_PLANE", Shapes::grid( 3, 1.0f, 0.0f ) );
	}

	/// <summary> Create a single triangle that is used to cover the entire screen in a 2D pass. </summary>
	/// <returns> A GPU mesh buffer to draw the geometry. </returns>
	MeshBufferSPtr MeshFactory::createFullScreenPassMesh( )
	{
		return createMeshBuffer< Vertex3f2f >( "FULL_SCREEN_PASS", Shapes::fullScreenTri( ) );
	}

	/// <summary> Create a cube that is used to cover the entire screen in a 3D pass. </summary>
	/// <returns> A GPU mesh buffer to draw the geometry. </returns>
	MeshBufferSPtr MeshFactory::createFullScreenPassCube( )
	{
		return createMeshBuffer< Vertex3f2f >( "FULL_SCREEN_CUBE", Shapes::fullScreenCube( ) );
	}



	/// <summary> Create a initial state particle vertex buffer. </summary>
	/// <returns> The GPU buffer data. </returns>
	ID3D11BufferSPtr MeshFactory::getInitParticleBuffer( _In_ const string& id,
														 _In_ const ParticleVertexBuffer& initialState ) const
	{
		const uint32_t bufferSize = static_cast< uint32_t >( initialState.size( ) * sizeof( VertexParticle::Vertex ) );
		return pBufferFactory->createVertexBuffer( id + "_VERTEX_BUFFER", initialState.data( ), bufferSize );
	}

	/// <summary> Create an update state particle vertex buffer. </summary>
	/// <returns> The GPU buffer data. </returns>
	ID3D11BufferSPtr MeshFactory::getParticleBuffer( _In_ const string& name, _In_ const unsigned int size ) const
	{
		return pBufferFactory->createSoVertexBuffer( name, size * sizeof( VertexParticle::Vertex ) );
	}

	/// <summary> Create a default fire particle vertex buffer. </summary>
	/// <returns> The GPU buffer data. </returns>
	ID3D11BufferSPtr MeshFactory::getFireParticleBuffer( ) const
	{
		Mesh< VertexParticle::Vertex >::VertexBuffer vBuff( 4 );

		const float offset = 0.05f;

		vBuff[ 0 ] = {
			vec3( offset, 0.0f, offset ),
			vec3( 0.0f, 0.0f, 0.0f ),
			vec2( 0.0f, 0.0f ),
			0.0f, 0.0f, 0
		};

		vBuff[ 1 ] = {
			vec3( -offset, 0.0f, offset ),
			vec3( 0.0f, 0.0f, 0.0f ),
			vec2( 0.0f, 0.0f ),
			0.0f, 0.0f, 0
		};

		vBuff[ 2 ] = {
			vec3( -offset, 0.0f, -offset ),
			vec3( 0.0f, 0.0f, 0.0f ),
			vec2( 0.0f, 0.0f ),
			0.0f, 0.0f, 0
		};

		vBuff[ 3 ] = {
			vec3( offset, 0.0f, -offset ),
			vec3( 0.0f, 0.0f, 0.0f ),
			vec2( 0.0f, 0.0f ),
			0.0f, 0.0f, 0
		};

		return getInitParticleBuffer( "FIRE_INIT_VERTEX_BUFFER", vBuff );
	}
} // namespace directX11
} // namespace visual
