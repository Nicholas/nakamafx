#pragma once

// DirectX 11 Buffer factory.
//
// Creates our DirectX 11 memory buffers so that we can place data on the GPU.
//
// Project   : NaKama-Fx
// File Name : BufferFactory.h
// Date      : 01/04/2019
// Author    : Nicholas Welters

#ifndef _BUFFER_FACTORY_DX11_H
#define _BUFFER_FACTORY_DX11_H

#include "../APITypeDefs.h"

namespace visual
{
namespace directX11
{
	class BufferFactory
	{
	public:
		/// <summary> ctor </summary>
		explicit BufferFactory( DevicePackage devicePackage );

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		BufferFactory( const BufferFactory& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		BufferFactory( BufferFactory&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~BufferFactory( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		BufferFactory& operator=( const BufferFactory& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		BufferFactory& operator=( BufferFactory&& move ) = default;



		/// <summary> Create a buffer on the GPU. </summary>
		/// <param name="id"> The buffers unique name. Only one should be created. </param>
		/// <param name="descriptor"> The buffer descriptor. </param>
		/// <param name="initialData"> The initial buffer data. </param>
		/// <returns> A pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createBuffer(
			const std::string& id,
			const D3D11_BUFFER_DESC& descriptor,
			const D3D11_SUBRESOURCE_DATA& initialData
		);

		/// <summary> Create a buffer on the GPU. </summary>
		/// <param name="id"> The buffers unique name. Only one should be created. </param>
		/// <param name="descriptor"> The buffer descriptor. </param>
		/// <returns> A pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createBuffer(
			const std::string& id,
			const D3D11_BUFFER_DESC& descriptor
		);


		// +-----------+
		// | Contatnts |
		// +-----------+

		/// <summary> Creates a default usage constant buffer with the provided size in bytes. </summary>
		/// <param name="id"> The buffers unique name. Only one should be created. </param>
		/// <param name="byteWidth"> The size the buffer in bytes. </param>
		/// <returns> A com pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createConstantBuffer( const std::string& id, uint32_t byteWidth );

		/// <summary> Creates a default usage constant buffer with an offset and type. </summary>
		/// <param name="id"> The buffers unique name. Only one should be created. </param>
		/// <returns> A com pointer to the buffer on the GPU. </returns>
		template< class ConstantBufferType >
		std::shared_ptr< ConstantBufferType > createConstantBuffer( const std::string& id, uint32_t offset );


		// +----------+
		// | Vertices |
		// +----------+

		/// <summary> Create a static read only Vertex Buffer </summary>
		/// <param name="id"> The buffers unique name. Only one should be created.</param>
		/// <param name="pBuffer"> The vertex buffer data. </param>
		/// <param name="bufferSize"> The size the of the vertex buffer. </param>
		/// <returns> A pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createVertexBuffer(
			const std::string& id,
			const void* pBuffer,
			uint32_t bufferSize
		);

		/// <summary> Create a streaming out Vertex Buffer </summary>
		/// <param name="id"> The buffers unique name. Only one should be created.</param>
		/// <param name="bufferSize"> The size the of the vertex buffer. </param>
		/// <returns> A pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createSoVertexBuffer(
			const std::string& id,
			uint32_t bufferSize
		);

		/// <summary> Create a writable Vertex Buffer </summary>
		/// <param name="id"> The buffers unique name. Only one should be created.</param>
		/// <param name="bufferSize"> The size the of the vertex buffer. </param>
		/// <returns> A pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createDynamicVertexBuffer(
			const std::string& id,
			uint32_t bufferSize
		);



		// +----------+
		// | Indecies |
		// +----------+

		/// <summary> Create a static read only Index Buffer </summary>
		/// <param name="id"> The buffers unique name. Only one should be created.</param>
		/// <param name="buffer"> The index buffer data. </param>
		/// <returns> A pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createIndexBuffer(
			const std::string& id,
			const std::vector< uint32_t >& buffer
		);

		/// <summary> Create a writable Vertex Buffer </summary>
		/// <param name="id"> The buffers unique name. Only one should be created.</param>
		/// <param name="bufferSize"> The size the of the index buffer. </param>
		/// <returns> A pointer to the buffer on the GPU. </returns>
		ID3D11BufferSPtr createDynamicIndexBuffer(
			const std::string& id,
			uint32_t bufferSize
		);


	private:
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Fields
		////////////////////////////////////////////////////////////////////////////////////////////////////
		DevicePackage devicePackage;

		ID3D11BufferLibrary buffers;
	};

	/// <summary> Creates a default usage constant buffer with an offset and type. </summary>
	/// <param name="id"> The buffers unique name. Only one should be created. </param>
	/// <returns> A com pointer to the buffer on the GPU. </returns>
	template<class ConstantBufferType >
	std::shared_ptr< ConstantBufferType > BufferFactory::createConstantBuffer( const std::string& id, const uint32_t offset )
	{
		return std::make_shared< ConstantBufferType >( offset, createConstantBuffer( id, sizeof( ConstantBufferType::Constants ) ) );
	}
} // namespace directX11
} // namespace visual

#endif // _BUFFER_FACTORY_DX11_H
