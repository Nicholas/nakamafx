#pragma once

// DirectX 11 Target factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : TargetFactory.h
// Date      : 21/02/2017
// Author    : Nicholas Welters

#ifndef _TARGET_FACTORY_DX11_H
#define _TARGET_FACTORY_DX11_H

#include "Visual/DirectX11/APITypeDefs.h"
#include "Visual/FX/Enums.h"

#include <glm/glm.hpp>

#include <future>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( BufferFactory );
	FORWARD_DECLARE( ImageFactory );
	FORWARD_DECLARE( MultipleRenderTarget );
	FORWARD_DECLARE( DepthStencilDeferredTarget );
	FORWARD_DECLARE( UATarget );

	// TODO: Move this to the API types?
	struct UAVDescriptor
	{
		std::string id;
		uint32_t count;
		uint32_t stride;

		bool isUint32;
		bool addCounter;
		bool hasRawView;
	};

	/// <summary>
	/// Render target and unordered access view factory.
	/// </summary>
	class TargetFactory
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pDevice"> Our GPU device. </param>
		/// <param name="pBufferFactory"> The buffer factory to manage the generic data structures. </param>
		/// <param name="pImageFactory"> The image factory to manage the texture data structures. </param>
		explicit TargetFactory(
			_In_ const ID3D11DeviceCPtr& pDevice,
			_In_ const BufferFactorySPtr& pBufferFactory,
			_In_ const  ImageFactorySPtr&  pImageFactory );

		/// <summary> ctor </summary>
		TargetFactory( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		TargetFactory( const TargetFactory& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		TargetFactory( TargetFactory&& move ) = default;


		/// <summary> dtor </summary>
		~TargetFactory( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		TargetFactory& operator=( const TargetFactory& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		TargetFactory& operator=( TargetFactory&& move ) = default;



		/// <summary> Create render targets so that we can collect colour information. </summary>
		/// <param name="pSwapChain"> Hardware swapchain to create a render target view for. </param>
		ID3D11RenderTargetViewSPtr createSwapChainRtv( _In_ const SwapChainPtr_t& pSwapChain );

		/// <summary> Create render targets so that we can collect colour information. </summary>
		/// <param name="id"> The unique id of the render target. </param>
		/// <param name="pDescriptor"> The render target view descriptor. </param>
		/// <param name="pTexture"> The texture data to base the render target off. </param>
		ID3D11RenderTargetViewSPtr createRenderTraget(
			_In_ const std::string& id,
			_In_ D3D11_RENDER_TARGET_VIEW_DESC* pDescriptor,
			_In_ const ID3D11Texture2DSPtr& pTexture );

		/// <summary> Create a depth stencil so that we can collect depth and stencil information. </summary>
		/// <param name="id"> The unique id of the depth stencil. </param>
		/// <param name="pDescriptor"> The depth stencil view descriptor. </param>
		/// <param name="pTexture"> The texture data to base the depth stencil off. </param>
		ID3D11DepthStencilViewSPtr createDepthStencil(
			_In_ const std::string& id,
			_In_ D3D11_DEPTH_STENCIL_VIEW_DESC* pDescriptor,
			_In_ const ID3D11Texture2DSPtr& pTexture );

		/// <summary> Create a depth stencil so that we can collect depth and stencil information. </summary>
		/// <param name="id"> The unique id of the depth stencil. </param>
		/// <param name="width"> The width of the depth stencil. </param>
		/// <param name="height"> The height of the depth stencil. </param>
		ID3D11DepthStencilViewSPtr createDepthStencil(
			_In_ const std::string& id,
			_In_ int width,
			_In_ int height );


		/// <summary>
		/// Create render targets so that we can collect colour information.
		/// The render target includes mip maps so that we can generate them easily
		/// after we have drawn to the render target.
		/// </summary>
		/// <param name="id"> The render targets unique id. </param>
		/// <param name="width"> The render target width. </param>
		/// <param name="height"> The render target height. </param>
		/// <param name="mips"> The number of mip map levels. </param>
		/// <param name="format"> The render targets format. </param>
		MultipleRenderTargetSPtr getMipMappedRenderTarget(
			_In_ const std::string& id,
			_In_ uint32_t width,
			_In_ uint32_t height,
			_In_ uint32_t mips,
			_In_ fx::ColourFormat format );

		/// <summary> Create a set of render targets so that we can collect colour information. </summary>
		/// <param name="id"> The render targets unique id. </param>
		/// <param name="width"> The render target width. </param>
		/// <param name="height"> The render target height. </param>
		/// <param name="msaa"> The number of samples to use for the render targets (MSAA Level). </param>
		/// <param name="count"> The number of render targets. </param>
		/// <param name="format"> The render targets format. </param>
		/// <param name="addDepthBuffer"> Include a depth stencil buffer in the render target set. </param>
		MultipleRenderTargetSPtr getMultipleRenderTarget(
			_In_ const std::string& id,
			_In_ uint32_t width,
			_In_ uint32_t height,
			_In_ uint32_t msaa,
			_In_ uint32_t count,
			_In_ fx::ColourFormat format,
			_In_ bool addDepthBuffer = true );

		/// <summary>
		/// Create a render target that only has a depth stencil
		/// that can be used for depth pre passes and shadow mapping.
		/// </summary>
		/// <param name="id"> The render targets unique id. </param>
		/// <param name="width"> The render target width. </param>
		/// <param name="height"> The render target height. </param>
		DepthStencilDeferredTargetSPtr createShadowMap(
			_In_ const std::string& id,
			_In_ uint32_t width,
			_In_ uint32_t height );



		/// <summary>
		/// Create an unordered access view rendering target
		/// that we can update using the GPU.
		/// </summary>
		/// <param name="id"> The render targets unique id. </param>
		/// <param name="pDescriptor"> The UAVs layout description. </param>
		/// <param name="pResource"> Ther resource that we will view. </param>
		ID3D11UnorderedAccessViewSPtr createUnorderedAccessView(
			_In_ const std::string& id,
			_In_ D3D11_UNORDERED_ACCESS_VIEW_DESC* pDescriptor,
			_In_ const ID3D11ResourceSPtr& pResource );

		/// <summary>
		/// Create an unordered access view rendering target
		/// that we can update using the GPU.
		/// </summary>
		/// <param name="id"> The targets unique id. </param>
		/// <param name="uavDescriptors">
		/// A list of descriptors for each unordered access
		/// view needed in the set.
		/// </param>
		UATargetSPtr createUATarget(
			_In_ const std::string& id,
			_In_ const std::vector< UAVDescriptor >& uavDescriptors );

		/// <summary>
		/// Create an unordered access view rendering target holding unsigned intigers.
		/// </summary>
		/// <param name="id"> The targets unique id. </param>
		/// <param name="count"> The number of ellements in the UAV. </param>
		UATargetSPtr createUintUATarget(
			_In_ const std::string& id,
			_In_ uint32_t count );

		/// <summary>
		/// Create an unordered access view rendering target holding floating point numbers.
		/// </summary>
		/// <param name="id"> The targets unique id. </param>
		/// <param name="count"> The number of ellements in the UAV. </param>
		UATargetSPtr createFloatUATarget(
			_In_ const ::std::string& id,
			_In_ uint32_t count );

		/// <summary>
		/// Create a a specialized UAV that is used to construct an A-Buffer on the GPU.
		/// </summary>
		/// <param name="id"> The targets unique id. </param>
		/// <param name="width"> The rendering resolution width. </param>
		/// <param name="height"> The rendering resolution height. </param>
		/// <param name="count"> The number of ellements in the UAV. </param>
		UATargetSPtr createABufferUATarget(
			_In_ const std::string& id,
			_In_ uint32_t width,
			_In_ uint32_t height,
			_In_ uint32_t count );

	private:
		/// <summary> Our GPU device. </summary>
		ID3D11DeviceCPtr pDevice;

		/// <summary> The buffer factory to manage the generic data structures. </summary>
		BufferFactorySPtr pBufferFactory;

		/// <summary> The texture factory that manages the image like data. </summary>
		ImageFactorySPtr pImageFactory;

		/// <summary> The render target view flyweight. </summary>
		ID3D11RenderTargetViewLibrary renderTargetViewLibrary;

		/// <summary> The depth stencil view flyweight. </summary>
		ID3D11DepthStencilViewLibrary depthStencilViewLibrary;

		/// <summary> The unordered access view flyweight. </summary>
		ID3D11UnorderedAccessViewLibrary unorderedAccessViewLibrary;
	};
} // namespace directX11
} // namespace visual

#endif // _TARGET_FACTORY_DX11_H
