#include "TargetFactory.h"

// DirectX 11 Target factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : TargetFactory.cpp
// Date      : 21/02/2017
// Author    : Nicholas Welters

#include "BufferFactory.h"
#include "ImageFactory.h"
#include "Visual/DirectX11/APITypeDefs.h"
#include "Visual/DirectX11/Targets/MultipleRenderTarget.h"
#include "Visual/DirectX11/Targets/MSAARenderTarget.h"
#include "Visual/DirectX11/Targets/DepthStencilDeferredTarget.h"
#include "Visual/DirectX11/Targets/UATarget.h"

#include <d3d11.h>


namespace visual
{
namespace directX11
{
	using std::make_shared;
	using std::to_string;
	using std::vector;
	using std::string;
	using fx::ColourFormat;

	inline DXGI_FORMAT toDxgiFormatTargetFactory( const ColourFormat format )
	{
		switch( format )
		{
			case fx::RGBA32:
				return DXGI_FORMAT_R32G32B32A32_FLOAT;

			case fx::RGBA16:
				return DXGI_FORMAT_R16G16B16A16_FLOAT;

			case fx::RGBA8:
			default:
				return DXGI_FORMAT_B8G8R8A8_UNORM;
		}
	}

	/// <summary> ctor </summary>
	/// <param name="pDevice"> Our GPU device. </param>
	/// <param name="pBufferFactory"> The buffer factory to manage the generic data structures. </param>
	/// <param name="pImageFactory"> The image factory to manage the texture data structures. </param>
	TargetFactory::TargetFactory(
		_In_ const ID3D11DeviceCPtr& pDevice,
		_In_ const BufferFactorySPtr& pBufferFactory,
		_In_ const  ImageFactorySPtr&  pImageFactory )
		: pDevice( pDevice )
		, pBufferFactory( pBufferFactory )
		,  pImageFactory(  pImageFactory )
		, renderTargetViewLibrary   ( ID3D11_FACTORY( ID3D11RenderTargetView    ), DX11_DELETER )
		, depthStencilViewLibrary   ( ID3D11_FACTORY( ID3D11DepthStencilView    ), DX11_DELETER )
		, unorderedAccessViewLibrary( ID3D11_FACTORY( ID3D11UnorderedAccessView ), DX11_DELETER )
	{ }

	/// <summary> Create render targets so that we can collect colour information. </summary>
	/// <param name="pSwapChain"> Hardware swapchain to create a render target view for. </param>
	ID3D11RenderTargetViewSPtr TargetFactory::createSwapChainRtv( _In_ const SwapChainPtr_t& pSwapChain )
	{
		return renderTargetViewLibrary.checkOut( "HwndSwapChain", [ this, pSwapChain ]( ) -> ID3D11RenderTargetView*
		{
			ID3D11Texture2D*    pRenderTargetTextures = nullptr;
			ID3D11RenderTargetView* pRenderTargetView = nullptr;

			// Get Swap Chain Buffer
			//	A zero-based buffer index, only has access to the first buffer.
			//	The type of interface used to manipulate the buffer.
			//	A pointer to a back-buffer interface.
			ThrowIfFailed(pSwapChain->GetBuffer( 0,
												 __uuidof( ID3D11Texture2D ),
												 reinterpret_cast< LPVOID* >( &pRenderTargetTextures ) ) );

			ThrowIfFailed(pDevice->CreateRenderTargetView( pRenderTargetTextures,
														   nullptr,
														   &pRenderTargetView ) );

			pRenderTargetTextures->Release( );

			setDebugName( pRenderTargetView, "HwndSwapChain RTV" );
			return pRenderTargetView;
		} );
	}

	/// <summary> Create render targets so that we can collect colour information. </summary>
	/// <param name="id"> The unique id of the render target. </param>
	/// <param name="pDescriptor"> The render target view descriptor. </param>
	/// <param name="pTexture"> The texture data to base the render target off. </param>
	ID3D11RenderTargetViewSPtr TargetFactory::createRenderTraget(
		_In_ const string& id,
		_In_ D3D11_RENDER_TARGET_VIEW_DESC* pDescriptor,
		_In_ const ID3D11Texture2DSPtr& pTexture )
	{
		return renderTargetViewLibrary.checkOut( id, [ this, id, pDescriptor, pTexture ]( ) -> ID3D11RenderTargetView*
		{
			ID3D11RenderTargetView* pRenderTargetView = nullptr;
			ThrowIfFailed(pDevice->CreateRenderTargetView( pTexture.get(), pDescriptor, &pRenderTargetView ) );

			setDebugName( pRenderTargetView, id );
			return pRenderTargetView;
		} );
	}

	/// <summary> Create a depth stencil so that we can collect depth and stencil information. </summary>
	/// <param name="id"> The unique id of the depth stencil. </param>
	/// <param name="pDescriptor"> The depth stencil view descriptor. </param>
	/// <param name="pTexture"> The texture data to base the depth stencil off. </param>
	ID3D11DepthStencilViewSPtr TargetFactory::createDepthStencil(
		_In_ const string& id,
		_In_ D3D11_DEPTH_STENCIL_VIEW_DESC* pDescriptor,
		_In_ const ID3D11Texture2DSPtr& pTexture )
	{
		return depthStencilViewLibrary.checkOut( id, [ this, id, pDescriptor, pTexture ]( )
		{
			ID3D11DepthStencilView* pDepthStencilView = nullptr;
			ThrowIfFailed( pDevice->CreateDepthStencilView( pTexture.get( ), pDescriptor, &pDepthStencilView ) );

			setDebugName( pDepthStencilView, id );

			return pDepthStencilView;
		} );
	}

	/// <summary> Create a depth stencil so that we can collect depth and stencil information. </summary>
	/// <param name="id"> The unique id of the depth stencil. </param>
	/// <param name="width"> The width of the depth stencil. </param>
	/// <param name="height"> The height of the depth stencil. </param>
	ID3D11DepthStencilViewSPtr TargetFactory::createDepthStencil(
		_In_ const string& id,
		_In_ const int width,
		_In_ const int height )
	{
		return depthStencilViewLibrary.checkOut(id, [ this, id, width, height ]( )
		{
			// Define depth stencil texture
			D3D11_TEXTURE2D_DESC textureDescriptor;
			ZeroMemory(&textureDescriptor, sizeof(textureDescriptor));

			textureDescriptor.Width              = width;				// Texture width (in texels).
			textureDescriptor.Height             = height;				// Texture height (in texels).
			textureDescriptor.MipLevels          = 1;					// Number of subtextures. Use 1 for a multisampled texture; or 0 to generate a full set of subtextures.
			textureDescriptor.ArraySize          = 1;					// Number of textures in the texture array.
			textureDescriptor.SampleDesc.Count   = 1;					// SampleDesc. Structure that specifies multisampling parameters for the texture. The number of multisamples per pixel.
			textureDescriptor.SampleDesc.Quality = 0;					// The image quality level.
			textureDescriptor.Usage              = D3D11_USAGE_DEFAULT;	// Value that identifies how the texture is to be read from and written to.
			textureDescriptor.CPUAccessFlags     = 0;					// Flags to specify the types of CPU access allowed.
			textureDescriptor.MiscFlags          = 0;					// Flags that identifies other, less common resource options
			textureDescriptor.Format             = DXGI_FORMAT_D24_UNORM_S8_UINT;
			textureDescriptor.BindFlags          = D3D11_BIND_DEPTH_STENCIL;

			// // Create the depth stencil view
			D3D11_DEPTH_STENCIL_VIEW_DESC viewDescriptor;
			ZeroMemory(&viewDescriptor, sizeof(viewDescriptor));

			viewDescriptor.Format = textureDescriptor.Format;
			viewDescriptor.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D; // Pointer to an array of subresource descriptions
			viewDescriptor.Texture2D.MipSlice = 0;						  // Address of a pointer to the created texture

			// Create depth stencil texture
			ID3D11Texture2DSPtr pTexture = pImageFactory->createTexture( id + "_TEXTURE", &textureDescriptor );

			// // Create the depth stencil view
			ID3D11DepthStencilView* pDepthStencilView = nullptr;
			ThrowIfFailed( pDevice->CreateDepthStencilView( pTexture.get( ), &viewDescriptor, &pDepthStencilView) );

			//pTexture->Release( );

			setDebugName( pDepthStencilView, id );
			return pDepthStencilView;
		} );
	}


	/// <summary>
	/// Create render targets so that we can collect colour information.
	/// The render target includes mip maps so that we can generate them easily
	/// after we have drawn to the render target.
	/// </summary>
	/// <param name="id"> The render targets unique id. </param>
	/// <param name="width"> The render target width. </param>
	/// <param name="height"> The render target height. </param>
	/// <param name="mips"> The number of mip map levels. </param>
	/// <param name="format"> The render targets format. </param>
	MultipleRenderTargetSPtr TargetFactory::getMipMappedRenderTarget(
		_In_ const string& id,
		_In_ const uint32_t width,
		_In_ const uint32_t height,
		_In_ const uint32_t mips,
		_In_ const ColourFormat format )
	{
		D3D11_TEXTURE2D_DESC            texDescriptor;
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDescriptor;
		D3D11_RENDER_TARGET_VIEW_DESC   rtvDescriptor;

		ZeroMemory( &texDescriptor, sizeof( texDescriptor ) );
		ZeroMemory( &srvDescriptor, sizeof( srvDescriptor ) );
		ZeroMemory( &rtvDescriptor, sizeof( rtvDescriptor ) );

		// Define colour texture
		texDescriptor.Width          = width;
		texDescriptor.Height         = height;
		texDescriptor.MipLevels      = mips;
		texDescriptor.ArraySize      = 1;
		texDescriptor.Usage          = D3D11_USAGE_DEFAULT;
		texDescriptor.CPUAccessFlags = 0;
		texDescriptor.MiscFlags      = D3D11_RESOURCE_MISC_GENERATE_MIPS;
		texDescriptor.BindFlags      = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		texDescriptor.Format         = toDxgiFormatTargetFactory( format );
		texDescriptor.SampleDesc.Count   = 1;
		texDescriptor.SampleDesc.Quality = 0;

		// Setup the description of the shader resource view.
		srvDescriptor.Format        = texDescriptor.Format;
		srvDescriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDescriptor.Texture2D.MostDetailedMip = 0;
		srvDescriptor.Texture2D.MipLevels       = mips;

		// Setup the description of the render target view.
		rtvDescriptor.Format        = texDescriptor.Format;
		rtvDescriptor.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDescriptor.Texture2D.MipSlice = 0;


		// Create colour resources
		vector< ID3D11Texture2DSPtr          > pColourTex;
		vector< ID3D11RenderTargetViewSPtr   > pColourRTV;
		vector< ID3D11ShaderResourceViewSPtr > pColourSRV;

		pColourTex.emplace_back( pImageFactory->createTexture( id + "TEX", &texDescriptor ) );
		pColourSRV.emplace_back( pImageFactory->createSrv(     id + "SRV", &srvDescriptor, pColourTex[ 0 ] ) );
		pColourRTV.emplace_back(           createRenderTraget( id + "RTV", &rtvDescriptor, pColourTex[ 0 ] ) );

		// Create depth resources
		ID3D11Texture2DSPtr          pDepthTex( nullptr );
		ID3D11ShaderResourceViewSPtr pDepthSRV( nullptr );
		ID3D11DepthStencilViewSPtr   pDepthDSV( nullptr );

		return make_shared< MultipleRenderTarget >(
			pColourTex, pColourRTV, pColourSRV,
			pDepthTex,  pDepthDSV,  pDepthSRV,
			width, height );
	}

	/// <summary> Create a set of render targets so that we can collect colour information. </summary>
	/// <param name="id"> The render targets unique id. </param>
	/// <param name="width"> The render target width. </param>
	/// <param name="height"> The render target height. </param>
	/// <param name="msaa"> The number of samples to use for the render targets (MSAA Level). </param>
	/// <param name="count"> The number of render targets. </param>
	/// <param name="format"> The render targets format. </param>
	/// <param name="addDepthBuffer"> Include a depth stencil buffer in the render target set. </param>
	MultipleRenderTargetSPtr TargetFactory::getMultipleRenderTarget(
		_In_ const string& id,
		_In_ const uint32_t width,
		_In_ const uint32_t height,
		_In_ const uint32_t msaa,
		_In_ const uint32_t count,
		_In_ const ColourFormat format,
		_In_ const bool addDepthBuffer )
	{
		const DXGI_FORMAT outputFormat = toDxgiFormatTargetFactory( format );

		D3D11_TEXTURE2D_DESC            colourTexDescriptor;
		D3D11_RENDER_TARGET_VIEW_DESC   colourRTVDescriptor;
		D3D11_SHADER_RESOURCE_VIEW_DESC colourSRVDescriptor;

		D3D11_TEXTURE2D_DESC            depthTexDescriptor;
		D3D11_DEPTH_STENCIL_VIEW_DESC   depthDSVDescriptor;
		D3D11_SHADER_RESOURCE_VIEW_DESC depthSRVDescriptor;

		ZeroMemory( &colourTexDescriptor, sizeof colourTexDescriptor );
		ZeroMemory( &colourRTVDescriptor, sizeof colourRTVDescriptor );
		ZeroMemory( &colourSRVDescriptor, sizeof colourSRVDescriptor );

		ZeroMemory( &depthTexDescriptor, sizeof depthTexDescriptor );
		ZeroMemory( &depthDSVDescriptor, sizeof depthDSVDescriptor );
		ZeroMemory( &depthSRVDescriptor, sizeof depthSRVDescriptor );


		// Define colour texture buffer
		colourTexDescriptor.Width          = width;
		colourTexDescriptor.Height         = height;
		colourTexDescriptor.MipLevels      = 1;
		colourTexDescriptor.ArraySize      = 1;
		colourTexDescriptor.Usage          = D3D11_USAGE_DEFAULT;
		colourTexDescriptor.CPUAccessFlags = 0;
		colourTexDescriptor.MiscFlags      = 0;
		colourTexDescriptor.BindFlags      = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		colourTexDescriptor.Format         = outputFormat;
		colourTexDescriptor.SampleDesc.Count   = msaa;
		colourTexDescriptor.SampleDesc.Quality = 0;

		// Setup the description of the shader resource view.
		colourSRVDescriptor.Format = colourTexDescriptor.Format;
		colourSRVDescriptor.Texture2D.MostDetailedMip = 0;
		colourSRVDescriptor.Texture2D.MipLevels       = 1;

		// Setup the description of the render target view.
		colourRTVDescriptor.Format = colourTexDescriptor.Format;
		colourRTVDescriptor.Texture2D.MipSlice = 0;


		// Define depth texture buffer
		depthTexDescriptor.Width          = width;
		depthTexDescriptor.Height         = height;
		depthTexDescriptor.MipLevels      = 1;
		depthTexDescriptor.ArraySize      = 1;
		depthTexDescriptor.Usage          = D3D11_USAGE_DEFAULT;
		depthTexDescriptor.CPUAccessFlags = 0;
		depthTexDescriptor.MiscFlags      = 0;
		depthTexDescriptor.BindFlags      = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		depthTexDescriptor.Format         = DXGI_FORMAT_R32_TYPELESS;
		depthTexDescriptor.SampleDesc.Count   = msaa;
		depthTexDescriptor.SampleDesc.Quality = 0;

		// // Define the depth shader resource view
		depthSRVDescriptor.Format = DXGI_FORMAT_R32_FLOAT;
		depthSRVDescriptor.Texture2D.MostDetailedMip = 0;
		depthSRVDescriptor.Texture2D.MipLevels       = 1;

		// // Define the depth view
		depthDSVDescriptor.Format = DXGI_FORMAT_D32_FLOAT;
		depthDSVDescriptor.Texture2D.MipSlice = 0;


		MultipleRenderTargetSPtr pRenderTarget;

		if (msaa == 1)
		{
			colourSRVDescriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			colourRTVDescriptor.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			 depthSRVDescriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			 depthDSVDescriptor.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

			// Create colour resources
			vector< ID3D11Texture2DSPtr          > pColourTex;
			vector< ID3D11RenderTargetViewSPtr   > pColourRTV;
			vector< ID3D11ShaderResourceViewSPtr > pColourSRV;

			for( uint32_t i = 0; i < count; i++ )
			{
				pColourTex.emplace_back( pImageFactory->createTexture( id + to_string( i ) + "TEX", &colourTexDescriptor ) );
				pColourSRV.emplace_back( pImageFactory->createSrv(     id + to_string( i ) + "SRV", &colourSRVDescriptor, pColourTex[ i ] ) );
				pColourRTV.emplace_back(           createRenderTraget( id + to_string( i ) + "RTV", &colourRTVDescriptor, pColourTex[ i ] ) );
			}

			// Create depth resources
			ID3D11Texture2DSPtr          pDepthTex( nullptr );
			ID3D11ShaderResourceViewSPtr pDepthSRV( nullptr );
			ID3D11DepthStencilViewSPtr   pDepthDSV( nullptr );

			if( addDepthBuffer )
			{
				pDepthTex = pImageFactory->createTexture( id + "_DEPTH_TEX", &depthTexDescriptor );
				pDepthSRV = pImageFactory->createSrv    ( id + "_DEPTH_SRV", &depthSRVDescriptor, pDepthTex );
				pDepthDSV =           createDepthStencil( id + "_DEPTH_RTV", &depthDSVDescriptor, pDepthTex );
			}

			pRenderTarget = make_shared< MultipleRenderTarget >(
				pColourTex, pColourRTV, pColourSRV,
				pDepthTex,  pDepthDSV,  pDepthSRV,
				width, height );
		}
		else
		{
			colourSRVDescriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
			colourRTVDescriptor.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
			 depthSRVDescriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
			 depthDSVDescriptor.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

			// Create colour resources
			vector< ID3D11Texture2DSPtr          > pMSAAColourTex;
			vector< ID3D11RenderTargetViewSPtr   > pMSAAColourRTV;
			vector< ID3D11ShaderResourceViewSPtr > pMSAAColourSRV;

			for( uint32_t i = 0; i < count; i++ )
			{
				pMSAAColourTex.emplace_back( pImageFactory->createTexture( id + to_string( i ) + "_MSAA_TEX", &colourTexDescriptor ) );
				pMSAAColourSRV.emplace_back( pImageFactory->createSrv(     id + to_string( i ) + "_MSAA_SRV", &colourSRVDescriptor, pMSAAColourTex[ i ] ) );
				pMSAAColourRTV.emplace_back(           createRenderTraget( id + to_string( i ) + "_MSAA_RTV", &colourRTVDescriptor, pMSAAColourTex[ i ] ) );
			}

			// Create depth resources
			ID3D11Texture2DSPtr          pMSAADepthTex( nullptr );
			ID3D11ShaderResourceViewSPtr pMSAADepthSRV( nullptr );
			ID3D11DepthStencilViewSPtr   pMSAADepthDSV( nullptr );

			if( addDepthBuffer )
			{
				pMSAADepthTex = pImageFactory->createTexture( id + "_MSAA_DEPTH_TEX", &depthTexDescriptor );
				pMSAADepthSRV = pImageFactory->createSrv(     id + "_MSAA_DEPTH_SRV", &depthSRVDescriptor, pMSAADepthTex );
				pMSAADepthDSV =           createDepthStencil( id + "_MSAA_DEPTH_RTV", &depthDSVDescriptor, pMSAADepthTex );
			}

			// Resolve Surfaces
			vector< ID3D11Texture2DSPtr          > pColourTex;
			vector< ID3D11ShaderResourceViewSPtr > pColourSRV;

			colourTexDescriptor.SampleDesc.Count   = 1;
			colourTexDescriptor.SampleDesc.Quality = 0;
			colourTexDescriptor.BindFlags     = D3D11_BIND_SHADER_RESOURCE;

			colourSRVDescriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

			for( uint32_t i = 0; i < count; i++ )
			{
				pColourTex.emplace_back( pImageFactory->createTexture( id + to_string( i ) + "TEX", &colourTexDescriptor ) );
				pColourSRV.emplace_back( pImageFactory->createSrv(     id + to_string( i ) + "SRV", &colourSRVDescriptor, pColourTex[ 0 ] ) );
			}

			pRenderTarget = make_shared< MSAARenderTarget >(
				pMSAAColourTex, pMSAAColourRTV, pMSAAColourSRV,
				pMSAADepthTex,  pMSAADepthDSV,  pMSAADepthSRV,
				width, height,
				pColourTex, pColourSRV,
				static_cast< uint32_t >( outputFormat ) );
		}

		return pRenderTarget;
	}

	/// <summary>
	/// Create a render target that only has a depth stencil
	/// that can be used for depth pre passes and shadow mapping.
	/// </summary>
	/// <param name="id"> The render targets unique id. </param>
	/// <param name="width"> The render target width. </param>
	/// <param name="height"> The render target height. </param>
	DepthStencilDeferredTargetSPtr TargetFactory::createShadowMap(
		_In_ const string& id,
		_In_ uint32_t width,
		_In_ uint32_t height )
	{
		D3D11_TEXTURE2D_DESC            texDescriptor;
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDescriptor;
		D3D11_DEPTH_STENCIL_VIEW_DESC   dsvDescriptor;

		ZeroMemory( &texDescriptor, sizeof( texDescriptor ) );
		ZeroMemory( &srvDescriptor, sizeof( srvDescriptor ) );
		ZeroMemory( &dsvDescriptor, sizeof( dsvDescriptor ) );

		// Define depth texture
		texDescriptor.Width			 = width;				// Texture width (in texels).
		texDescriptor.Height		 = height;				// Texture height (in texels).
		texDescriptor.MipLevels		 = 1;					// Number of subtextures. Use 1 for a multisampled texture; or 0 to generate a full set of subtextures.
		texDescriptor.ArraySize		 = 1;					// Number of textures in the texture array.
		texDescriptor.Usage			 = D3D11_USAGE_DEFAULT;	// Value that identifies how the texture is to be read from and written to.
		texDescriptor.CPUAccessFlags = 0;					// Flags to specify the types of CPU access allowed.
		texDescriptor.MiscFlags		 = 0;					// Flags that identifies other, less common resource options
		texDescriptor.BindFlags		 = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		texDescriptor.Format		 = DXGI_FORMAT_R32_TYPELESS;
		texDescriptor.SampleDesc.Count   = 1;				// SampleDesc. Structure that specifies multisampling parameters for the texture. The number of multisamples per pixel.
		texDescriptor.SampleDesc.Quality = 0;				// The image quality level.

		// // Define the depth shader resource view
		srvDescriptor.Format        = DXGI_FORMAT_R32_FLOAT;
		srvDescriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDescriptor.Texture2D.MostDetailedMip = 0;
		srvDescriptor.Texture2D.MipLevels = 1;

		// // Define the depth view
		dsvDescriptor.Format        = DXGI_FORMAT_D32_FLOAT;
		dsvDescriptor.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvDescriptor.Texture2D.MipSlice = 0;

		// Create depth resources
		vector< ID3D11Texture2DSPtr          > pColourTex;
		vector< ID3D11RenderTargetViewSPtr   > pColourRTV;
		vector< ID3D11ShaderResourceViewSPtr > pColourSRV;

		ID3D11Texture2DSPtr          pDepthTex = pImageFactory->createTexture( id + "_DEPTH_TEX", &texDescriptor );
		ID3D11ShaderResourceViewSPtr pDepthSRV = pImageFactory->createSrv(     id + "_DEPTH_SRV", &srvDescriptor, pDepthTex );
		ID3D11DepthStencilViewSPtr  pDepthDSV  =           createDepthStencil( id + "_DEPTH_RTV", &dsvDescriptor, pDepthTex );

		return make_shared< DepthStencilDeferredTarget >(
			pDepthTex, pDepthDSV, pDepthSRV,
			width, height );
	}


	/// <summary>
	/// Create an unordered access view rendering target
	/// that we can update using the GPU.
	/// </summary>
	/// <param name="id"> The render targets unique id. </param>
	/// <param name="pDescriptor"> The UAVs layout description. </param>
	/// <param name="pResource"> Ther resource that we will view. </param>
	ID3D11UnorderedAccessViewSPtr TargetFactory::createUnorderedAccessView(
		_In_ const string& id,
		_In_ D3D11_UNORDERED_ACCESS_VIEW_DESC* pDescriptor,
		_In_ const ID3D11ResourceSPtr& pResource )
	{
		return unorderedAccessViewLibrary.checkOut( id, [ this, id, pDescriptor, pResource ] ( )
		{
			ID3D11UnorderedAccessView* pUAV;
			ThrowIfFailed( pDevice->CreateUnorderedAccessView( pResource.get( ), pDescriptor, &pUAV ) );

			setDebugName( pUAV, id );
			return pUAV;
		} );
	}

	/// <summary>
	/// Create an unordered access view rendering target
	/// that we can update using the GPU.
	/// </summary>
	/// <param name="id"> The render targets unique id. </param>
	/// <param name="uavDescriptors">
	/// A list of descriptors for each unordered access
	/// view needed in the set.
	/// </param>
	UATargetSPtr TargetFactory::createUATarget(
		_In_ const string& id,
		_In_ const vector< UAVDescriptor >& uavDescriptors )
	{
		D3D11_BUFFER_DESC                bufDescriptor;
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescriptor;
		D3D11_SHADER_RESOURCE_VIEW_DESC  srvDescriptor;

		ZeroMemory( &bufDescriptor, sizeof( bufDescriptor ) );
		ZeroMemory( &uavDescriptor, sizeof( uavDescriptor ) );
		ZeroMemory( &srvDescriptor, sizeof( srvDescriptor ) );

		bufDescriptor.Usage          = D3D11_USAGE_DEFAULT;
		bufDescriptor.BindFlags      = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
		bufDescriptor.CPUAccessFlags = 0;

		uavDescriptor.ViewDimension       = D3D11_UAV_DIMENSION_BUFFER;
		uavDescriptor.Buffer.FirstElement = 0;

		srvDescriptor.ViewDimension       = D3D11_SRV_DIMENSION_BUFFER;
		srvDescriptor.Buffer.FirstElement = 0;

		vector< ID3D11BufferSPtr > pBuffers;
		vector< ID3D11UnorderedAccessViewSPtr > pUnorderedAccessViews;
		vector< ID3D11ShaderResourceViewSPtr > pShaderResourceViews;

		for( const UAVDescriptor& descriptor : uavDescriptors )
		{
			bufDescriptor.StructureByteStride = descriptor.stride;
			bufDescriptor.ByteWidth           = descriptor.count * descriptor.stride;
			uavDescriptor.Buffer.NumElements  = descriptor.count;
			srvDescriptor.Buffer.NumElements  = descriptor.count;


			bufDescriptor.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			uavDescriptor.Format = DXGI_FORMAT_UNKNOWN;
			srvDescriptor.Format = DXGI_FORMAT_UNKNOWN;

			if( descriptor.isUint32 && descriptor.hasRawView )
			{
				bufDescriptor.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;

				uavDescriptor.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
				uavDescriptor.Format = DXGI_FORMAT_R32_TYPELESS;

				srvDescriptor.Format = DXGI_FORMAT_R32_UINT;
			}

			if( descriptor.addCounter )
			{
				uavDescriptor.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_COUNTER;
			}
			else if( !descriptor.hasRawView )
			{
				uavDescriptor.Buffer.Flags = 0;
			}

			const string bufferID = descriptor.id + "_Buffer";
			const string uavID    = descriptor.id + "_UAV";
			const string srvID    = descriptor.id + "_SRV";

			ID3D11BufferSPtr pBuffer = pBufferFactory->createBuffer( bufferID, bufDescriptor );

			pUnorderedAccessViews.emplace_back( createUnorderedAccessView( uavID, &uavDescriptor, pBuffer ) );
			pShaderResourceViews.emplace_back(  pImageFactory->createSrv(  srvID, &srvDescriptor, pBuffer ) );
			pBuffers.emplace_back( pBuffer );
		}

		return make_shared< UATarget >( pBuffers, pUnorderedAccessViews, pShaderResourceViews );
	}

	/// <summary>
	/// Create an unordered access view rendering target holding unsigned intigers.
	/// </summary>
	/// <param name="id"> The targets unique id. </param>
	/// <param name="count"> The number of ellements in the UAV. </param>
	UATargetSPtr TargetFactory::createUintUATarget(
		_In_ const string& id,
		_In_ const UINT count )
	{
		vector< UAVDescriptor > uavDescriptors( 1 );
		uavDescriptors[ 0 ].id         = id;
		uavDescriptors[ 0 ].count      = count;
		uavDescriptors[ 0 ].stride     = 4 * sizeof( BYTE );
		uavDescriptors[ 0 ].addCounter = false;
		uavDescriptors[ 0 ].isUint32   = true;
		uavDescriptors[ 0 ].hasRawView = false;

		return createUATarget( id, uavDescriptors );
	}

	/// <summary>
	/// Create an unordered access view rendering target holding floating point numbers.
	/// </summary>
	/// <param name="id"> The targets unique id. </param>
	/// <param name="count"> The number of ellements in the UAV. </param>
	UATargetSPtr TargetFactory::createFloatUATarget(
		_In_ const string& id,
		_In_ const UINT count )
	{
		vector< UAVDescriptor > uavDescriptors( 1 );
		uavDescriptors[ 0 ].id = id;
		uavDescriptors[ 0 ].count = count;
		uavDescriptors[ 0 ].stride = sizeof( float );
		uavDescriptors[ 0 ].addCounter = true;
		uavDescriptors[ 0 ].isUint32 = false;
		uavDescriptors[ 0 ].hasRawView = false;

		return createUATarget( id, uavDescriptors );
	}

	/// <summary>
	/// Create a a specialized UAV that is used to construct an A-Buffer on the GPU.
	/// </summary>
	/// <param name="id"> The targets unique id. </param>
	/// <param name="width"> The rendering resolution width. </param>
	/// <param name="height"> The rendering resolution height. </param>
	/// <param name="count"> The number of ellements in the UAV. </param>
	UATargetSPtr TargetFactory::createABufferUATarget(
		_In_ const string& id,
		_In_ const uint32_t width,
		_In_ const uint32_t height,
		_In_ const uint32_t count )
	{
		vector< UAVDescriptor > uavDescriptors( 2 );
		uavDescriptors[ 0 ].id         = id + "_Link";
		uavDescriptors[ 0 ].count      = width * height * count;
		uavDescriptors[ 0 ].stride     = sizeof( float ) * 5 + ( sizeof( BYTE ) * 4 );
		uavDescriptors[ 0 ].addCounter = true;
		uavDescriptors[ 0 ].isUint32   = false;
		uavDescriptors[ 0 ].hasRawView = false;

		uavDescriptors[ 1 ].id         = id + "_Index";
		uavDescriptors[ 1 ].count      = width * height;
		uavDescriptors[ 1 ].stride     = sizeof( BYTE ) * 4;
		uavDescriptors[ 1 ].addCounter = false;
		uavDescriptors[ 1 ].isUint32   = true;
		uavDescriptors[ 1 ].hasRawView = true;

		return createUATarget( id, uavDescriptors );
	}
} // namespace directX11
} // namespace visual
