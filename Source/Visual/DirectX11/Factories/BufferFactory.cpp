
#include "BufferFactory.h"

// DirectX 11 Buffer factory.
//
// Creates our DirectX 11 memory buffers so that we can place data on the GPU.
//
// Project   : NaKama-Fx
// File Name : BufferFactory.cpp
// Date      : 01/04/2019
// Author    : Nicholas Welters

#ifdef ENABLE_DX_11_4
#include <d3d11_4.h>
#define USE_DX11_1
#define USE_DX11_2
#define USE_DX11_3
#define USE_DX11_4
#elif defined ENABLE_DX_11_3
#include <d3d11_3.h>
#define USE_DX11_1
#define USE_DX11_2
#define USE_DX11_3
#elif defined ENABLE_DX_11_2
#include <d3d11_2.h>
#define USE_DX11_1
#define USE_DX11_2
#elif defined ENABLE_DX_11_1
#include <d3d11_1.h>
#define USE_DX11_1
#elif defined ENABLE_DX_11_0
#include <D3D11.h>
#endif

#include <string>
#include <utility>
#include <iomanip>

namespace visual
{
namespace directX11
{
	using std::move;
	using std::string;
	using std::vector;

	/// <summary> ctor </summary>
	BufferFactory::BufferFactory( DevicePackage devicePackage )
		: devicePackage( move( devicePackage ) )
		, buffers( ID3D11_FACTORY( ID3D11Buffer ), DX11_DELETER )
	{ }

	/// <summary> Create a buffer on the GPU. </summary>
	/// <param name="id"> The buffers unique name. Only one should be created. </param>
	/// <param name="descriptor"> The buffer descriptor. </param>
	/// <param name="initialData"> The initial buffer data. </param>
	/// <returns> A pointer to the buffer on the GPU. </returns>
	ID3D11BufferSPtr BufferFactory::createBuffer(
		const string& id,
		const D3D11_BUFFER_DESC& descriptor,
		const D3D11_SUBRESOURCE_DATA& initialData )
	{
		return buffers.checkOut( id, [ this, &id, &descriptor, &initialData ]( ) -> ID3D11Buffer*
		{
			ID3D11Buffer* pBuffer;

			// This method returns E_OUTOFMEMORY if there is insufficient memory to create the buffer.
			ThrowIfFailed( devicePackage.pDevice->CreateBuffer( &descriptor, &initialData, &pBuffer ) );

			setDebugName( pBuffer, id );

			return pBuffer;
		} );
	}

	/// <summary> Create a buffer on the GPU. </summary>
	/// <param name="id"> The buffers unique name. Only one should be created. </param>
	/// <param name="descriptor"> The buffer descriptor. </param>
	/// <param name="initialData"> The initial buffer data. </param>
	/// <returns> A pointer to the buffer on the GPU. </returns>
	ID3D11BufferSPtr BufferFactory::createBuffer(
		const string& id,
		const D3D11_BUFFER_DESC& descriptor )
	{
		return buffers.checkOut( id, [ this, &id, &descriptor ]( ) -> ID3D11Buffer*
		{
			ID3D11Buffer* pBuffer;

			// This method returns E_OUTOFMEMORY if there is insufficient memory to create the buffer.
			ThrowIfFailed( devicePackage.pDevice->CreateBuffer( &descriptor, nullptr, &pBuffer ) );

			setDebugName( pBuffer, id );

			return pBuffer;
		} );
	}


	// +-----------+
	// | Contatnts |
	// +-----------+

	/// <summary> Creates a default usage constant buffer with the provided size in bytes.
	/// </summary>
	/// <param name="id">The buffers unique name. Only one should be created.</param>
	/// <param name="byteWidth">The size the buffer in bytes.</param>
	/// <returns>A com pointer to the buffer on the GPU.</returns>
	ID3D11BufferSPtr BufferFactory::createConstantBuffer( const string& id, const uint32_t byteWidth )
	{
		D3D11_BUFFER_DESC descriptor;

		descriptor.Usage               = D3D11_USAGE_DEFAULT;
		descriptor.ByteWidth           = byteWidth;
		descriptor.BindFlags           = D3D11_BIND_CONSTANT_BUFFER;
		descriptor.CPUAccessFlags      = 0;
		descriptor.MiscFlags           = 0;
		descriptor.StructureByteStride = 0;

		return createBuffer( id, descriptor );
	}


	// +----------+
	// | Vertices |
	// +----------+

	/// <summary> Create a static read only Vertex Buffer </summary>
	/// <param name="id"> The buffers unique name. Only one should be created.</param>
	/// <param name="pBuffer"> The vertex buffer data. </param>
	/// <param name="bufferSize"> The size the of the vertex buffer. </param>
	/// <returns> A pointer to the buffer on the GPU. </returns>
	ID3D11BufferSPtr BufferFactory::createVertexBuffer(
		const string& id,
		const void* pBuffer,
		const uint32_t bufferSize )
	{
		D3D11_BUFFER_DESC descriptor;
		D3D11_SUBRESOURCE_DATA initialData;

		descriptor.Usage               = D3D11_USAGE_DEFAULT;
		descriptor.CPUAccessFlags      = 0;
		descriptor.ByteWidth           = bufferSize;
		descriptor.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
		descriptor.MiscFlags           = 0;
		descriptor.StructureByteStride = 0;

		initialData.pSysMem            = pBuffer;
		initialData.SysMemPitch        = 0;
		initialData.SysMemSlicePitch   = 0;

		return createBuffer( id, descriptor, initialData );
	}

	/// <summary> Create a streaming out Vertex Buffer </summary>
	/// <param name="id"> The buffers unique name. Only one should be created.</param>
	/// <param name="bufferSize"> The size the of the vertex buffer. </param>
	/// <returns> A pointer to the buffer on the GPU. </returns>
	ID3D11BufferSPtr BufferFactory::createSoVertexBuffer(
		const string& id,
		const uint32_t bufferSize )
	{
		D3D11_BUFFER_DESC descriptor;

//		descriptor.Usage		   = D3D11_USAGE_DYNAMIC;
//		descriptor.CPUAccessFlags  = D3D11_CPU_ACCESS_WRITE;
		descriptor.Usage               = D3D11_USAGE_DEFAULT;
		descriptor.CPUAccessFlags      = 0;
		descriptor.ByteWidth           = bufferSize;
		descriptor.BindFlags           = D3D11_BIND_VERTEX_BUFFER | D3D11_BIND_STREAM_OUTPUT;
		descriptor.MiscFlags           = 0;
		descriptor.StructureByteStride = 0;

		return createBuffer( id, descriptor );
	}

	/// <summary> Create a static read only Vertex Buffer </summary>
	/// <param name="id"> The buffers unique name. Only one should be created.</param>
	/// <param name="bufferSize"> The size the of the vertex buffer. </param>
	/// <returns> A pointer to the buffer on the GPU. </returns>
	ID3D11BufferSPtr BufferFactory::createDynamicVertexBuffer(
		const string& id,
		const uint32_t bufferSize )
	{
		D3D11_BUFFER_DESC descriptor;

		descriptor.Usage               = D3D11_USAGE_DYNAMIC;
		descriptor.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
		descriptor.ByteWidth           = bufferSize;
		descriptor.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
		descriptor.MiscFlags           = 0;
		descriptor.StructureByteStride = 0;

		return createBuffer( id, descriptor );
	}



	// +----------+
	// | Indecies |
	// +----------+

	/// <summary> Create a static read only Index Buffer </summary>
	/// <param name="id"> The buffers unique name. Only one should be created.</param>
	/// <param name="buffer"> The index buffer data. </param>
	/// <returns> A pointer to the buffer on the GPU. </returns>
	ID3D11BufferSPtr BufferFactory::createIndexBuffer(
		const string& id,
		const vector< uint32_t >& buffer
	)
	{
		D3D11_BUFFER_DESC descriptor;
		D3D11_SUBRESOURCE_DATA initialData;

		descriptor.Usage               = D3D11_USAGE_DEFAULT;
		descriptor.ByteWidth           = sizeof( uint32_t ) * static_cast< uint32_t >( buffer.size( ) );
		descriptor.BindFlags           = D3D11_BIND_INDEX_BUFFER;
		descriptor.CPUAccessFlags      = 0;
		descriptor.MiscFlags           = 0;
		descriptor.StructureByteStride = 0;

		initialData.pSysMem          = buffer.data( );
		initialData.SysMemPitch      = 0;
		initialData.SysMemSlicePitch = 0;

		return createBuffer( id, descriptor, initialData );
	}

	/// <summary> Create a static read only Vertex Buffer </summary>
	/// <param name="id"> The buffers unique name. Only one should be created.</param>
	/// <param name="bufferSize"> The size the of the index buffer. </param>
	/// <returns> A pointer to the buffer on the GPU. </returns>
	ID3D11BufferSPtr BufferFactory::createDynamicIndexBuffer(
		const string& id,
		const uint32_t bufferSize )
	{
		D3D11_BUFFER_DESC descriptor;

		descriptor.Usage               = D3D11_USAGE_DYNAMIC;
		descriptor.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
		descriptor.ByteWidth           = bufferSize;
		descriptor.BindFlags           = D3D11_BIND_INDEX_BUFFER;
		descriptor.MiscFlags           = 0;
		descriptor.StructureByteStride = 0;

		return createBuffer( id, descriptor );
	}
} // namespace directX11
} // namespace visual
