#include "ImageFactory.h"

// DirectX 11 Image factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : ImageFactory.cpp
// Date      : 21/02/2017
// Author    : Nicholas Welters

#include "BufferFactory.h"

#include "Visual/DirectX11/APITypeDefs.h"
#include "Visual/DirectX11/DirectXTex/DDSTextureLoader/DDSTextureLoader.h"
#include "Visual/DirectX11/DirectXTex/WICTextureLoader/WICTextureLoader.h"

#include <glm/glm.hpp>
#include <d3d11.h>


namespace visual
{
namespace directX11
{
	using std::uniform_real_distribution;
	using std::default_random_engine;
	using std::make_shared;
	using std::string;
	using std::vector;
	using std::cout;
	using std::endl;
	using std::equal;

	using DirectX::CreateDDSTextureFromFileEx;
	using DirectX::CreateWICTextureFromFileEx;

	using glm::vec3;
	using glm::vec4;
	using glm::uvec2;
	using glm::uvec3;

	/// <summary> ctor </summary>
	/// <param name="pDevice"> Our GPU device. </param>
	/// <param name="pBufferFactory"> The buffer factory to manage the generic data structures. </param>
	ImageFactory::ImageFactory(
		_In_ const ID3D11DeviceCPtr& pDevice,
		_In_ const BufferFactorySPtr& pBufferFactory )
		: pDevice( pDevice )
		, pBufferFactory( pBufferFactory )
		, texture1DLibrary         ( ID3D11_FACTORY( ID3D11Texture1D          ), DX11_DELETER )
		, texture2DLibrary         ( ID3D11_FACTORY( ID3D11Texture2D          ), DX11_DELETER )
		, texture3DLibrary         ( ID3D11_FACTORY( ID3D11Texture3D          ), DX11_DELETER )
		, shaderResourceViewLibrary( ID3D11_FACTORY( ID3D11ShaderResourceView ), DX11_DELETER )
		, dispatch( [ ]( const AsyncEvent& ) { } )
	{
	}



		// +-------+
		// | ASYNC |
		// +-------+

	/// <summary>
	/// Create a view of a texture for our shaders to use.
	/// We use this to load in a texture "asynchornisly". This is false (maybe)
	/// when loading formats that are ready to be used on the GPU such as dds.
	/// If the texture needs to be processed on the GPU such (eg, generate mips)
	/// the processing will be added run at the start of a frame update. Once complete
	/// the promise is resolved and contains a texture ready to use for rendering.
	/// </summary>
	/// <param name="fileName"> The file path to load the texture from. </param>
	/// <param name="forceSRGB"> Enable srgb use for the texture on the GPU. </param>
	/// <returns> A promise to a shader resource view. </returns>
	ID3D11ShaderResourceViewPromiseSPtr ImageFactory::createSrv(
		_In_ const string& fileName,
		_In_opt_ const bool forceSRGB )
	{
		ID3D11ShaderResourceViewPromiseSPtr pPromise = make_shared< ID3D11ShaderResourceViewPromise >( );

		if( supportsConcurrentLoad( fileName ) )
		{
			pPromise->set_value( createSrv( nullptr, fileName, forceSRGB ) );
		}
		else
		{
			dispatch( [ this, fileName, forceSRGB, pPromise ]( const DeviceContextPtr_t& pContext )
			{
				pPromise->set_value( createSrv( pContext, fileName, forceSRGB ) );
			} );
		}

		return pPromise;
	}

	/// <summary> Set the async callback to provide a valid context. </summary>
	/// <param name="dispatch"> The trampoline function that calls a functor to pass through a valid context. </param>
	void ImageFactory::setDispatch( _In_ const AsyncDispatcher& dispatch )
	{
		this->dispatch = dispatch;
	}



		// +------+
		// | SYNC |
		// +------+

	/// <summary> Create some texture memory space for us so that we can store some data here. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="pDescriptor"> The textures layout and usage description. </param>
	/// <param name="pSubresourceData"> The initial data to be stored on the GPU. </param>
	/// <returns> A 1D texture. </returns>
	ID3D11Texture1DSPtr ImageFactory::createTexture(
		_In_ const string& id,
		_In_ D3D11_TEXTURE1D_DESC* pDescriptor,
		_In_ D3D11_SUBRESOURCE_DATA* pSubresourceData )
	{
		return texture1DLibrary.checkOut(id, [this, pDescriptor, pSubresourceData, id]
		{
			ID3D11Texture1D* pTexture = nullptr;
			ThrowIfFailed( pDevice->CreateTexture1D(pDescriptor, pSubresourceData, &pTexture) );
			setDebugName( pTexture, id );

			return pTexture;
		} );
	}

	/// <summary> Create some texture memory space for us so that we can store some data here. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="pDescriptor"> The textures layout and usage description. </param>
	/// <param name="pSubresourceData"> The initial data to be stored on the GPU. </param>
	/// <returns> A 2D texture. </returns>
	ID3D11Texture2DSPtr ImageFactory::createTexture(
		_In_ const string& id,
		_In_ D3D11_TEXTURE2D_DESC* pDescriptor,
		_In_ D3D11_SUBRESOURCE_DATA* pSubresourceData )
	{
		return texture2DLibrary.checkOut(id, [this, pDescriptor, pSubresourceData, id]
		{
			ID3D11Texture2D* pTexture = nullptr;
			ThrowIfFailed( pDevice->CreateTexture2D(pDescriptor, pSubresourceData, &pTexture) );
			setDebugName( pTexture, id );

			return pTexture;
		} );
	}

	/// <summary> Create some texture memory space for us so that we can store some data here. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="pDescriptor"> The textures layout and usage description. </param>
	/// <param name="pSubresourceData"> The initial data to be stored on the GPU. </param>
	/// <returns> A 3D texture. </returns>
	ID3D11Texture3DSPtr ImageFactory::createTexture(
		_In_ const string& id,
		_In_ D3D11_TEXTURE3D_DESC* pDescriptor,
		_In_ D3D11_SUBRESOURCE_DATA* pSubresourceData )
	{
		return texture3DLibrary.checkOut(id, [this, pDescriptor, pSubresourceData, id]
		{
			ID3D11Texture3D* pTexture = nullptr;
			ThrowIfFailed( pDevice->CreateTexture3D(pDescriptor, pSubresourceData, &pTexture) );
			setDebugName( pTexture, id );

			return pTexture;
		} );
	}

	/// <summary> Create a view of a texture for our shaders to use. </summary>
	/// <param name="fileName"> The file path to load the texture from. </param>
	/// <returns> A shader resource view of the data from a texture file. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv(
		_In_ const DeviceContextPtr_t& pContext,
		_In_ const string& fileName,
		_In_opt_ const bool forceSRGB )
	{
		return shaderResourceViewLibrary.checkOut(fileName, [this, pContext, fileName, forceSRGB]
		{
			HRESULT result;
			ID3D11ShaderResourceView * pShaderResource;

			size_t converted = 0;
			size_t newSize   = fileName.length() + 1;
			wchar_t wcstring[ 4092 ];

			mbstowcs_s(&converted, &wcstring[ 0 ], newSize, fileName.c_str(), fileName.length());

			if (isDds(fileName))
			{
				result = CreateDDSTextureFromFileEx(
					pDevice.Get( ), wcstring, 0,
					D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, forceSRGB,
					nullptr, &pShaderResource, nullptr );
			}
			else
			{
				result = CreateWICTextureFromFileEx(
					pDevice.Get( ), pContext.Get( ), wcstring, 0,
					D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, forceSRGB,
					nullptr, &pShaderResource );
			}

			//ThrowIfFailed( result );
			if( FAILED( result ) )
			{
				cout << "File: \"" << fileName << "\" not found!" << endl;
				return pShaderResource;
			}
			setDebugName( pShaderResource, fileName );

			return pShaderResource;
		});
	}

	/// <summary> Create a view of a resource for our shaders to use. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="pDescriptor"> The shader resource view layout and usage description. </param>
	/// <param name="pResource"> The resource data. </param>
	/// <returns> A shader resource view of the provided texture data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv(
		_In_ const string& id,
		_In_ const D3D11_SHADER_RESOURCE_VIEW_DESC* pDescriptor,
		_In_ const ID3D11ResourceSPtr& pResource )
	{
		// Make ourselves a shiny new resource view...
		return shaderResourceViewLibrary.checkOut( id, [ this, pDescriptor, pResource, id ]
		{
			ID3D11ShaderResourceView * pShaderResourceView = nullptr;
			ThrowIfFailed( pDevice->CreateShaderResourceView( pResource.get( ), pDescriptor, &pShaderResourceView ) );
			setDebugName( pShaderResourceView, id );

			return pShaderResourceView;
		});
	}

	/// <summary> Create a view of a random 1D float[4] array. </summary>
	/// <returns> A shader resource view of the random data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv1DFromRandom( )
	{
		return shaderResourceViewLibrary.checkOut("Random 1D", [this]() -> ID3D11ShaderResourceView*
		{
			D3D11_SUBRESOURCE_DATA initData;
			D3D11_TEXTURE1D_DESC textureDecriptor;
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDecriptor;
			ZeroMemory( &initData       , sizeof initData        );
			ZeroMemory( &textureDecriptor, sizeof textureDecriptor );
			ZeroMemory( &viewDecriptor  , sizeof viewDecriptor   );


			// Create the random data.
			vec4 randomValues[ 1024 ];
			uniform_real_distribution< float > randomFloats( -1.0, 1.0 );
			default_random_engine generator;

			for( vec4& randomValue : randomValues )
			{
				randomValue.x = randomFloats( generator );
				randomValue.y = randomFloats( generator );
				randomValue.z = randomFloats( generator );
				randomValue.w = randomFloats( generator );
			}

			// Create the texture.
			initData.pSysMem          = randomValues;
			initData.SysMemPitch      = 1024 * sizeof( vec4 );
			initData.SysMemSlicePitch = 1024 * sizeof( vec4 );

			textureDecriptor.Width     = 1024;
			textureDecriptor.MipLevels = 1;
			textureDecriptor.Format    = DXGI_FORMAT_R32G32B32A32_FLOAT;
			textureDecriptor.Usage     = D3D11_USAGE_IMMUTABLE;
			textureDecriptor.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			textureDecriptor.MiscFlags = 0;
			textureDecriptor.ArraySize = 1;
			textureDecriptor.CPUAccessFlags = 0;

			ID3D11Texture1DSPtr pRandomTexture = createTexture(
				"Random 1D Texture",
				&textureDecriptor,
				&initData );

			// Create the resource view.
			viewDecriptor.Format                    = textureDecriptor.Format;
			viewDecriptor.ViewDimension             = D3D11_SRV_DIMENSION_TEXTURE1D;
			viewDecriptor.Texture1D.MipLevels       = textureDecriptor.MipLevels;
			viewDecriptor.Texture1D.MostDetailedMip = 0;

			ID3D11ShaderResourceView * pShaderResourceView = nullptr;
			ThrowIfFailed( pDevice->CreateShaderResourceView(
				pRandomTexture.get( ),
				&viewDecriptor,
				&pShaderResourceView ) );

			setDebugName( pShaderResourceView, "Random 1D" );

			return pShaderResourceView;
		});
	}

	/// <summary> Create a view of a float[4] 2D array. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="data"> The views initial data. </param>
	/// <param name="size"> The size of the texture to create with the texture data. </param>
	/// <returns> A shader resource view of the data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv2DFromDataSet(
		_In_ const string& id,
		_In_ const vector< vec4 >& data,
		_In_ const uvec2& size )
	{
		if( data.size( ) != size.x * size.y )
		{
			throw std::exception( "The texture data and the sizes provided do not match" );
		}

		return shaderResourceViewLibrary.checkOut( id, [ this, id, data, size ]( ) -> ID3D11ShaderResourceView*
		{
			D3D11_SUBRESOURCE_DATA initData;
			D3D11_TEXTURE2D_DESC textureDecriptor;
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDecriptor;
			ZeroMemory( &initData        , sizeof initData         );
			ZeroMemory( &textureDecriptor, sizeof textureDecriptor );
			ZeroMemory( &viewDecriptor   , sizeof viewDecriptor    );

			// Create the texture.
			initData.pSysMem          = data.data( );
			initData.SysMemPitch      = size.x * sizeof( vec4 );
			initData.SysMemSlicePitch = 0;

			textureDecriptor.Width     = size.x;
			textureDecriptor.Height    = size.y;
			textureDecriptor.MipLevels = 1;
			textureDecriptor.Format    = DXGI_FORMAT_R32G32B32A32_FLOAT;
			textureDecriptor.Usage     = D3D11_USAGE_IMMUTABLE;
			textureDecriptor.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			textureDecriptor.MiscFlags = 0;
			textureDecriptor.ArraySize = 1;
			textureDecriptor.SampleDesc.Count = 1;
			textureDecriptor.SampleDesc.Quality = 0;
			textureDecriptor.CPUAccessFlags = 0;

			ID3D11Texture2DSPtr pTexture = createTexture( id, &textureDecriptor, &initData );

			// Create the resource view.
			viewDecriptor.Format                    = textureDecriptor.Format;
			viewDecriptor.ViewDimension             = D3D11_SRV_DIMENSION_TEXTURE2D;
			viewDecriptor.Texture2D.MipLevels       = textureDecriptor.MipLevels;
			viewDecriptor.Texture2D.MostDetailedMip = 0;

			ID3D11ShaderResourceView * pShaderResourceView = nullptr;
			ThrowIfFailed( pDevice->CreateShaderResourceView(
				pTexture.get( ),
				&viewDecriptor,
				&pShaderResourceView ) );

			setDebugName( pShaderResourceView, id );

			return pShaderResourceView;
		});
	}


	/// <summary> Create a view of a R8G8B8A8 2D array. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="data"> The views initial data. </param>
	/// <param name="size"> The size of the texture to create with the texture data. </param>
	/// <returns> A shader resource view of the data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv2DFromDataSet(
		_In_ const string& id,
		_In_ const vector< uint32_t >& data,
		_In_ const uvec2& size )
	{
		if( data.size( ) != size.x * size.y )
		{
			throw std::exception( "The texture data and the sizes provided do not match" );
		}

		return shaderResourceViewLibrary.checkOut( id, [ this, id, data, size ]( ) -> ID3D11ShaderResourceView*
		{
			D3D11_SUBRESOURCE_DATA initData;
			D3D11_TEXTURE2D_DESC textureDecriptor;
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDecriptor;
			ZeroMemory( &initData        , sizeof initData         );
			ZeroMemory( &textureDecriptor, sizeof textureDecriptor );
			ZeroMemory( &viewDecriptor   , sizeof viewDecriptor    );

			// Create the texture.
			initData.pSysMem          = data.data( );
			initData.SysMemPitch      = size.x * sizeof( uint32_t );
			initData.SysMemSlicePitch = 0;

			textureDecriptor.Width     = size.x;
			textureDecriptor.Height    = size.y;
			textureDecriptor.MipLevels = 1;
			textureDecriptor.Format    = DXGI_FORMAT_R8G8B8A8_UNORM;
			textureDecriptor.Usage     = D3D11_USAGE_IMMUTABLE;
			textureDecriptor.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			textureDecriptor.MiscFlags = 0;
			textureDecriptor.ArraySize = 1;
			textureDecriptor.SampleDesc.Count = 1;
			textureDecriptor.SampleDesc.Quality = 0;
			textureDecriptor.CPUAccessFlags = 0;

			ID3D11Texture2DSPtr pTexture = createTexture( id, &textureDecriptor, &initData );

			// Create the resource view.
			viewDecriptor.Format                    = textureDecriptor.Format;
			viewDecriptor.ViewDimension             = D3D11_SRV_DIMENSION_TEXTURE2D;
			viewDecriptor.Texture2D.MipLevels       = textureDecriptor.MipLevels;
			viewDecriptor.Texture2D.MostDetailedMip = 0;

			ID3D11ShaderResourceView * pShaderResourceView = nullptr;
			ThrowIfFailed( pDevice->CreateShaderResourceView(
				pTexture.get( ),
				&viewDecriptor,
				&pShaderResourceView ) );

			setDebugName( pShaderResourceView, id );

			return pShaderResourceView;
		});
	}


	/// <summary> Create a view of a float[4] 3D array. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="data"> The views initial data. </param>
	/// <param name="size"> The size of the texture to create with the texture data. </param>
	/// <returns> A shader resource view of the data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv2DArrayFromDataSet(
		_In_ const string& id,
		_In_ const vector< vector< vec4 > >& data,
		_In_ const uvec3& size )
	{
		if( data.size( ) != size.x * size.y * size.z )
		{
			throw std::exception( "The texture data and the sizes provided do not match" );
		}

		return shaderResourceViewLibrary.checkOut( id, [ this, id, data, size ]( ) -> ID3D11ShaderResourceView*
		{
			vector< D3D11_SUBRESOURCE_DATA > initData( size.z );
			D3D11_TEXTURE2D_DESC textureDecriptor;
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDecriptor;
			ZeroMemory( &textureDecriptor, sizeof textureDecriptor );
			ZeroMemory( &viewDecriptor   , sizeof viewDecriptor    );

			// Create the texture.
			for( uint32_t i = 0; i < size.z; ++i )
			{
				ZeroMemory( &initData[ i ], sizeof D3D11_SUBRESOURCE_DATA );
				initData[ i ].pSysMem          = data[ i ].data( );
				initData[ i ].SysMemPitch      = size.x * sizeof( vec4 );
				initData[ i ].SysMemPitch      = size.x * sizeof( vec4 );
				initData[ i ].SysMemSlicePitch = size.y * initData[ i ].SysMemPitch;
			}

			textureDecriptor.Width     = size.x;
			textureDecriptor.Height    = size.y;
			textureDecriptor.MipLevels = 1;
			textureDecriptor.Format    = DXGI_FORMAT_R32G32B32A32_FLOAT;
			textureDecriptor.Usage     = D3D11_USAGE_IMMUTABLE;
			textureDecriptor.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			textureDecriptor.MiscFlags = 0;
			textureDecriptor.ArraySize = size.z;
			textureDecriptor.SampleDesc.Count = 1;
			textureDecriptor.SampleDesc.Quality = 0;
			textureDecriptor.CPUAccessFlags = 0;

			ID3D11Texture2DSPtr pTexture = createTexture( id, &textureDecriptor, initData.data( ) );

			// Create the resource view.
			viewDecriptor.Format        = textureDecriptor.Format;
			viewDecriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
			viewDecriptor.Texture2DArray.MipLevels       = textureDecriptor.MipLevels;
			viewDecriptor.Texture2DArray.MostDetailedMip = 0;
			viewDecriptor.Texture2DArray.FirstArraySlice = 0;
			viewDecriptor.Texture2DArray.ArraySize       = size.z;

			ID3D11ShaderResourceView * pShaderResourceView = nullptr;
			ThrowIfFailed( pDevice->CreateShaderResourceView(
				pTexture.get( ),
				&viewDecriptor,
				&pShaderResourceView ) );

			setDebugName( pShaderResourceView, id );

			return pShaderResourceView;
		});
	}

	/// <summary> Create a view of a R8G8B8A8 3D array. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="data"> The views initial data. </param>
	/// <param name="size"> The size of the texture to create with the texture data. </param>
	/// <returns> A shader resource view of the data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv2DArrayFromDataSet(
		_In_ const std::string& id,
		_In_ const std::vector< std::vector< uint32_t > >& data,
		_In_ const glm::uvec3& size )
	{
		if( data.size( ) != size.z )
		{
			throw std::exception( "The texture data and the sizes provided do not match" );
		}

		return shaderResourceViewLibrary.checkOut( id, [ this, id, data, size ]( ) -> ID3D11ShaderResourceView*
		{
			vector< D3D11_SUBRESOURCE_DATA > initData( size.z );
			D3D11_TEXTURE2D_DESC textureDecriptor;
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDecriptor;
			ZeroMemory( &textureDecriptor, sizeof textureDecriptor );
			ZeroMemory( &viewDecriptor   , sizeof viewDecriptor    );

			// Create the texture.
			for( uint32_t i = 0; i < size.z; ++i )
			{
				ZeroMemory( &initData[ i ], sizeof D3D11_SUBRESOURCE_DATA );
				initData[ i ].pSysMem          = data[ i ].data( );
				initData[ i ].SysMemPitch      = size.x * sizeof( int );
				initData[ i ].SysMemPitch      = size.x * sizeof( int );
				initData[ i ].SysMemSlicePitch = size.y * initData[ i ].SysMemPitch;
			}

			textureDecriptor.Width     = size.x;
			textureDecriptor.Height    = size.y;
			textureDecriptor.MipLevels = 1;
			textureDecriptor.Format    = DXGI_FORMAT_R8G8B8A8_UNORM;
			textureDecriptor.Usage     = D3D11_USAGE_IMMUTABLE;
			textureDecriptor.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			textureDecriptor.MiscFlags = 0;
			textureDecriptor.ArraySize = size.z;
			textureDecriptor.SampleDesc.Count   = 1;
			textureDecriptor.SampleDesc.Quality = 0;
			textureDecriptor.CPUAccessFlags = 0;

			ID3D11Texture2DSPtr pTexture = createTexture( id, &textureDecriptor, initData.data( ) );

			// Create the resource view.
			viewDecriptor.Format        = textureDecriptor.Format;
			viewDecriptor.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
			viewDecriptor.Texture2DArray.MipLevels       = textureDecriptor.MipLevels;
			viewDecriptor.Texture2DArray.MostDetailedMip = 0;
			viewDecriptor.Texture2DArray.FirstArraySlice = 0;
			viewDecriptor.Texture2DArray.ArraySize       = size.z;

			ID3D11ShaderResourceView * pShaderResourceView = nullptr;
			ThrowIfFailed( pDevice->CreateShaderResourceView(
				pTexture.get( ),
				&viewDecriptor,
				&pShaderResourceView ) );

			setDebugName( pShaderResourceView, id );

			return pShaderResourceView;
		});
	}

	/// <summary> Create a cube map with a single solid colour. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="colour"> The colour of the cube map. </param>
	/// <returns> A shader resource view of the data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrv2DCubeFromColour(
		_In_ const std::string& id,
		_In_ const vec4& colour )
	{
		return shaderResourceViewLibrary.checkOut( id, [ this, id, colour ]( ) -> ID3D11ShaderResourceView*
		{
			D3D11_SUBRESOURCE_DATA initData[ 6 ];
			D3D11_TEXTURE2D_DESC textureDecriptor;
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDecriptor;
			ZeroMemory( &initData        , sizeof initData         );
			ZeroMemory( &textureDecriptor, sizeof textureDecriptor );
			ZeroMemory( &viewDecriptor   , sizeof viewDecriptor    );

			// Create the texture.
			for( int i = 0; i < 6; ++i )
			{
				initData[ i ].pSysMem          = &colour;
				initData[ i ].SysMemPitch      = sizeof( vec4 );
				initData[ i ].SysMemSlicePitch = 0;
			}

			textureDecriptor.Width     = 1;
			textureDecriptor.Height    = 1;
			textureDecriptor.MipLevels = 1;
			textureDecriptor.Format    = DXGI_FORMAT_R32G32B32A32_FLOAT;
			textureDecriptor.Usage     = D3D11_USAGE_IMMUTABLE;
			textureDecriptor.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			textureDecriptor.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;
			textureDecriptor.ArraySize = 6;
			textureDecriptor.SampleDesc.Count = 1;
			textureDecriptor.SampleDesc.Quality = 0;
			textureDecriptor.CPUAccessFlags = 0;

			ID3D11Texture2DSPtr pTexture = createTexture( id, &textureDecriptor, &initData[ 0 ] );

			// Create the resource view.
			viewDecriptor.Format                    = textureDecriptor.Format;
			viewDecriptor.ViewDimension             = D3D11_SRV_DIMENSION_TEXTURECUBE;
			viewDecriptor.Texture2D.MipLevels       = textureDecriptor.MipLevels;
			viewDecriptor.Texture2D.MostDetailedMip = 0;

			ID3D11ShaderResourceView * pShaderResourceView = nullptr;
			ThrowIfFailed( pDevice->CreateShaderResourceView(
				pTexture.get( ),
				&viewDecriptor,
				&pShaderResourceView ) );

			setDebugName( pShaderResourceView, id );

			return pShaderResourceView;
		});
	}

	/// <summary> Create a view of a float[3] array. </summary>
	/// <param name="id"> The textures unique id. </param>
	/// <param name="data"> The views initial data. </param>
	/// <returns> A shader resource view of the data. </returns>
	ID3D11ShaderResourceViewSPtr ImageFactory::createSrvFloat3TextureBuffer(
		_In_ const string& id,
		_In_ const vector< vec3 >& data )
	{
		return shaderResourceViewLibrary.checkOut( id + "SRV", [ this, id, data ]( ) -> ID3D11ShaderResourceView*
		{
			D3D11_SUBRESOURCE_DATA initData;
			D3D11_BUFFER_DESC bufferDecriptor;
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDecriptor;
			ZeroMemory( &initData       , sizeof initData        );
			ZeroMemory( &bufferDecriptor, sizeof bufferDecriptor );
			ZeroMemory( &viewDecriptor  , sizeof viewDecriptor   );

			// Create the texture.
			initData.pSysMem          = data.data( );
			initData.SysMemPitch      = static_cast< uint32_t >( data.size( ) * sizeof( float ) * 3 );
			initData.SysMemSlicePitch = 0;

			bufferDecriptor.Usage     = D3D11_USAGE_DEFAULT;
			bufferDecriptor.ByteWidth = initData.SysMemPitch;
			bufferDecriptor.BindFlags = D3D11_BIND_SHADER_RESOURCE;

			ID3D11BufferSPtr pTBuffer = pBufferFactory->createBuffer( id, bufferDecriptor, initData );


			viewDecriptor.Format        = DXGI_FORMAT_R32G32B32_FLOAT;
			viewDecriptor.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;

			viewDecriptor.Buffer.FirstElement = 0;
			viewDecriptor.Buffer.NumElements = static_cast< unsigned int >( data.size( ) );

			ID3D11ShaderResourceView * pShaderResourceView = nullptr;

			ThrowIfFailed( pDevice->CreateShaderResourceView(
				pTBuffer.get( ),
				&viewDecriptor,
				&pShaderResourceView ) );

			setDebugName( pShaderResourceView, id );

			//pTBuffer->Release( ); // Keep in mind that when using vulkan throwing things like this away might not be the best plan.

			return pShaderResourceView;
		} );
	}

	/// <summary> Check the extention to see if we can load the entire texture asynchronisly from the rendering pipeline. </summary>
	/// <param name="fileName"> The file path to the texture to load. </param>
	/// <returns> We can load the entire texture with the device only if true. false if we need a context. </returns>
	bool ImageFactory::supportsConcurrentLoad( _In_ const string& filePath )
	{
		return isDds(filePath);
	}

	/// <summary> Check the extention is .dds. </summary>
	/// <param name="fileName"> The file path to the texture to load. </param>
	/// <returns> True if the file path ends in ".dds". </returns>
	bool ImageFactory::isDds( _In_ const string & filePath)
	{
		const auto endsWith = [ ]( const string& value, const string& ending) -> bool
		{
			if (ending.size() > value.size()) return false;
			return equal(ending.rbegin(), ending.rend(), value.rbegin());
		};
		return endsWith(filePath, "dds");
	}

	/// <summary> Create structured buffer that we can update on the cpu. </summary>
	/// <param name="id"> The structured buffers unique id. </param>
	/// <param name="count"> The number of elements in the buffer. </param>
	/// <param name="stride"> The size of each element in the buffer. </param>
	/// <param name="pData"> The initial data to use. </param>
	UpdateableSrv ImageFactory::createUpdateableStructuredBuffer(
		_In_ const string& id,
		_In_ const uint32_t count,
		_In_ const uint32_t stride,
		_In_ const void* pData )
	{
		UpdateableSrv srv;

		D3D11_SUBRESOURCE_DATA       data;
		D3D11_BUFFER_DESC            bufferDescriptor;
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDescriptor;

		ZeroMemory( &data            , sizeof data             );
		ZeroMemory( &bufferDescriptor, sizeof bufferDescriptor );
		ZeroMemory(    &srvDescriptor, sizeof srvDescriptor    );

		bufferDescriptor.Usage               = D3D11_USAGE_DEFAULT;
		bufferDescriptor.BindFlags           = D3D11_BIND_SHADER_RESOURCE;
		bufferDescriptor.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
		bufferDescriptor.StructureByteStride = stride;
		bufferDescriptor.ByteWidth           = count * stride;
		bufferDescriptor.MiscFlags           = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

		srvDescriptor.ViewDimension       = D3D11_SRV_DIMENSION_BUFFER;
		srvDescriptor.Format              = DXGI_FORMAT_UNKNOWN;
		srvDescriptor.Buffer.FirstElement = 0;
		srvDescriptor.Buffer.NumElements  = count;

		data.pSysMem          = pData;
		data.SysMemPitch      = count * stride;
		data.SysMemSlicePitch = 0;

		const string bufferId = id + "_Buffer";
		const string    srvId = id + "_SRV";

		srv.pBuffer = pBufferFactory->createBuffer( bufferId, bufferDescriptor, data );
		srv.pSrv    = createSrv( srvId, &srvDescriptor, srv.pBuffer );

		return srv;
	}
} // namespace directX11
} // namespace visual
