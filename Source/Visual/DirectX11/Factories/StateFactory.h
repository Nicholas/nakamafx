#pragma once

// DirectX 11 State factory.
//
// Creates our GPU data and keeps it in a flyweight so that we can share access to the data.
//
// Project   : NaKama-Fx
// File Name : StateFactory.h
// Date      : 07/12/2016
// Author    : Nicholas Welters

#ifndef _STATE_FACTORY_DX11_H
#define _STATE_FACTORY_DX11_H

#include "Visual/DirectX11/APITypeDefs.h"
#include "Visual/FX/Enums.h"

#include <Macros.h>

namespace visual
{
namespace directX11
{
	/// <summary> Rendering pipe line state factory functions. </summary>
	class StateFactory
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pDevice"> Our GPU device. </param>
		explicit StateFactory( _In_ const ID3D11DeviceCPtr& pDevice,
#ifdef USE_DX11_1
							   _In_ const ID3D11Device1CPtr& pDevice1,
#endif
#ifdef USE_DX11_3
							   _In_ const ID3D11Device3CPtr& pDevice3
#endif
		);

		/// <summary> ctor </summary>
		StateFactory( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		StateFactory( const StateFactory& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		StateFactory( StateFactory&& move ) = default;


		/// <summary> dtor </summary>
		~StateFactory( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		StateFactory& operator=( const StateFactory& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		StateFactory& operator=( StateFactory&& move ) = default;


		/// <summary> Create a texture sampler states to be used on the GPU in a shader. </summary>
		/// <param name="uvMode"> How the GPU handles out of bounds sampling texture coordinates. </param>
		/// <param name="filter"> How the GPU handles sampling the texture buffer and returning a colour. </param>
		/// <param name="comparitor"> Set the sampler to be used on a depth buffer for depth testing and shadow mapping. </param>
		/// <returns> A texture sampler state. </returns>
		ID3D11SamplerStateSPtr createSamplerState( _In_ fx::SamplerBounds uvMode,
												   _In_ fx::SamplerFilter filter,
												   _In_opt_ bool comparitor = false ) const;

		/// <summary> Create a depth stencil state. </summary>
		/// <param name="write"> Update the depth stencil when we draw a pixel. </param>
		/// <param name="test"> Test the pixels depth with the depth buffer and if it is bellow the surface the pixel is discarded. </param>
		/// <returns> A depth stencil state. </returns>
		ID3D11DepthStencilStateSPtr createDepthStencilState( _In_opt_ bool write = true,
															 _In_opt_ bool test  = true ) const;

		/// <summary> Create a rasterizer state. </summary>
		/// <param name="wireframe"> Only fill in the trriangle edges rather than the entire trienagle. </param>
		/// <param name="backfaceCulling"> Discared triangles that point away from the viewer. </param>
		/// <param name="ccw"> Set the rasterizer to use counter clockwise triangles as front facing triangles. </param>
		/// <returns> A rasterizerl state. </returns>
		RasterizerStateSPtr_t createRasterizerState( _In_ bool wireframe,
													 _In_ bool backfaceCulling,
													 _In_ bool ccw ) const;

		/// <summary> Create a blend state. </summary>
		/// <param name="alphaBlend"> The type of blend state to create. </param>
		/// <returns> A blend state. </returns>
		BlendStateSPtr_t createBlendState( _In_ fx::Blend alphaBlend ) const;

	private:
		/// <summary> Our GPU device. </summary>
		ID3D11DeviceCPtr pDevice;
#ifdef USE_DX11_1
		ID3D11Device1CPtr pDevice1;
#endif
#ifdef USE_DX11_3
		ID3D11Device3CPtr pDevice3;
#endif

		/// <summary> Our GPU state data collection. </summary>
		FORWARD_DECLARE_STRUCT( Library )
		LibrarySPtr pLibrary;
	};
} // namespace directX11
} // namespace visual

#endif // _STATE_FACTORY_DX11_H
