#pragma once
// Controls the execute API commands to the GPU.
//
// Project   : NaKama-Fx
// File Name : CommandDispatch.h
// Date      : 15/02/2017
// Author    : Nicholas Welters

#ifndef _COMMAND_DISPATCH_DX11_H
#define _COMMAND_DISPATCH_DX11_H

#include "Visual/FX/CommandDispatch.h"
#include "APITypeDefs.h"

#include <Macros.h>

namespace visual
{
namespace fx
{
	FORWARD_DECLARE( SwapChain );
}
namespace directX11
{
	/// <summary> This class controls the execute API commands to the GPU. </summary>
	class CommandDispatch : public fx::CommandDispatch
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pContext"> The d3d11 device context. </param>
		/// <param name="pRenderingCommands"> A list of priority queues of command buffers created by an indirect context. </param>
		/// <param name="pAsyncEventBuffer"> A list of events that might need to run only once with the device context . </param>
		CommandDispatch(
			const DeviceContextPtr_t& pContext,
			const BufferedCommandQueueSPtr& pRenderingCommands,
			const AsyncEventBufferSPtr& pAsyncEventBuffer );

		/// <summary> ctor </summary>
		CommandDispatch( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		CommandDispatch( const CommandDispatch& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		CommandDispatch( CommandDispatch&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~CommandDispatch( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		CommandDispatch& operator=( const CommandDispatch& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		CommandDispatch& operator=( CommandDispatch&& move ) = default;

		/// <summary>
		/// Reset the command allocator memory so that we can reuse it to record new commands.
		///	This should only be called after we know that the commands in the allocator have all
		///	finished executing. see FlushCommandQueue( ) which uses a fence to sync the GPU and CPU
		/// </summary>
		virtual void resetCommandAllocators( ) final override;

		/// <summary>	Send the command lists to the GPU for execution. </summary>
		virtual void executeCommandList( ) final override;

		/// <summary>
		/// Wait for the last set of command list to complete execution.
		///	This is to make sure that we can sync the GPU and the CPU.
		///
		///	Use this to make sure the command queue is empty.
		///	This doesn't mean the list has been executed, only that
		///	the command queue has completed what ever was scheduled
		///	before the fence.
		/// </summary>
		virtual void flushCommandQueue( ) final override;

		/// <summary>
		/// Here we are going to give the executor the swapchain so that it can
		///	schedule the call to present. Making sure that the present event
		/// doesn't execute until the rendering commands have
		///	finished.
		/// </summary>
		/// <param name="swapchain">
		/// A swap chain to add to the system so that we call present after
		/// executing all out commands for the frame.
		/// </param>
		void addSwapChain( const fx::SwapChainSPtr& swapchain );

		/// <summary>
		/// Remove the current swap chain. This allows us to
		/// destroy the swapchain and create a new one.
		///
		/// TODO: Improve the removal method so that we can keep our other swap chains in place
		/// </summary>
		void clearSwapChain( );

		/// <summary>
		/// Get the command list buffer so that we can send commands
		/// to it from a pass that uses an indirect context.
		/// </summary>
		/// <returns> A list of priority queues of command buffers created by an indirect context. </returns>
		BufferedCommandQueueSPtr getBufferedCommandQueue( ) const;

	private:
		/// <summary> The d3d11 device context. </summary>
		DeviceContextPtr_t pContext;

		/// <summary> A list of priority queues of command buffers created by an indirect context. </summary>
		BufferedCommandQueueSPtr pRenderingCommands;

		/// <summary> A list of events that might need to run only once with the device context . </summary>
		AsyncEventBufferSPtr pAsyncEventBuffer;

		/// <summary> The output swap chains. Needed so that we can execute present at the right time. </summary>
		std::vector< fx::SwapChainSPtr > swapchains;
	};
} // namespace directX11
} // namespace visual

#endif // _COMMAND_DISPATCH_DX11_H
