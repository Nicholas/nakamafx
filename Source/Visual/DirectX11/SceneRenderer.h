#pragma once
//
// DX11 Scene Renderer
//
// Here we are going to perform all our drawing and computations.
// We are going to send our GPU updates from here too.
//
// Project   : NaKama-Fx
// File Name : SceneRenderer.h
// Date      : 15-06-2015
// Author    : Nicholas Welters
//

#ifndef _SCENE_DX11_H
#define _SCENE_DX11_H

#include "../FX/SceneRenderer.h"

#include "APITypeDefs.h"
#include "Builders/PipelineBuilder.h"

#include <Timing/Timer.h>
#include <Timing/TickCounter.h>

#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	FORWARD_DECLARE( Camera );
	FORWARD_DECLARE( Settings );
	FORWARD_DECLARE( TickCounter );
}

namespace directX11
{
	FORWARD_DECLARE_STRUCT( SceneDataSet );

	class SceneRenderer : public fx::SceneRenderer
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pContext"> DX11 device context </param>
		/// <param name="data"> The pipeline rendering and compute passes. </param>
		/// <param name="pCamera"> The camera that we are going to draw the scene in front of. </param>
		/// <param name="pSettings"> The system settings so that we can set some shader constants. </param>
		SceneRenderer(
			const DeviceContextPtr_t& pContext,
			const PipelineBuilder::PipelineData& data,
			const fx::CameraSPtr& pCamera,
			const fx::SettingsSPtr& pSettings );

		/// <summary> ctor </summary>
		SceneRenderer( ) = delete;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		SceneRenderer( const SceneRenderer& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		SceneRenderer( SceneRenderer&& move ) = delete;


		/// <summary> dtor </summary>
		virtual ~SceneRenderer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		SceneRenderer& operator=( const SceneRenderer& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		SceneRenderer& operator=( SceneRenderer&& move ) = delete;


		/// <summary>
		/// This is where we will pas the CPU side data to the GPU.
		/// We will hold this until we know that the values we have are
		/// the real values we are going to draw with.
		/// Only one update to the GPU per frame.
		/// </summary>
		/// <param name="delta"> The time step to take for the update. </param>
		virtual void update( float delta ) final override;

		/// <summary> This is were we will set up the state of the GPU to render all the object in this scene. </summary>
		virtual void render( ) final override;

		/// <summary>
		/// Add Elements to the scene so that we can render something of interest.
		/// </summary>
		/// <param name="pPayload"> The scene data that we need to render our scene. </param>
		void setScene( const SceneDataSetSPtr& pPayload );

		/// <summary> Change the output target for the scene. </summary>
		/// <param name="pOutputTarget"> The new output target to use for the scene. </param>
		/// <param name="width"> The new target width. </param>
		/// <param name="height"> The new target height. </param>
		void setOutputTarget(
			const OutputTargetSPtr& pOutputTarget,
			const float& width,
			const float& height ) const;

		//////////////////////////////////////
		// Getters and Setters for Testing //
		////////////////////////////////////
		void setGamma( float gamma ) const;
		void setContrast( float contrast ) const;
		void setSaturation( float saturation ) const;
		void setBrightness( float brightness ) const;
		void setSunDir( float theta );

		float getGamma( ) const;
		float getContrast( ) const;
		float getSaturation( ) const;
		float getBrightness( ) const;
		float getSunDir( ) const;

		void toggleNormals( ) const;
		void toggleFPS( );
		void toggleTimes( );

	private:
		/// <summary> DX11 device context </summary>
		DeviceContextPtr_t pContext;

		/// <summary> The pipeline rendering and compute passes. </summary>
		PipelineBuilder::PipelineData data;

		/// <summary> The camera that we are going to draw the scene in front of. </summary>
		fx::CameraSPtr pCamera;

		/// <summary> The system settings so that we can set some shader constants. </summary>
		fx::SettingsSPtr pSettings;

		/// <summary> The scene data that we need to render our scene. </summary>
		SceneDataSetSPtr pLoadedScene;

		/// <summary> The scene direction lights direction. </summary>
		float sunDir;

		/// <summary> Count the number of frames per second. </summary>
		tools::TickCounter fpsCounter;

		/// <summary> Get some timing for some of the sub sections. </summary>
		std::vector< float > times;

		/// <summary> A timer that we can use to time our sections. </summary>
		tools::Timer timer;

	private:
		/// <summary> The view and projection matrices for cascade shadow maps. </summary>
		struct ShadowCascade
		{
			glm::mat4x4 view;
			glm::mat4x4 viewProjection;
		};

		/// <summary>
		/// Calculate the view and projections for each cascade shadow map.
		///
		/// TODO: Move the shadow map in one pixel steps.
		/// </summary>
		/// <param name="direction"> The light source direction. </param>
		/// <param name="depth"> The depth of the shaow map (far plane). </param>
		/// <param name="rotation"> The light sources orientation. </param>
		std::vector< ShadowCascade > calculateShadowCascadeProjection(
			const glm::vec3& direction,
			float depth,
			const glm::mat4x4& rotation ) const;
	};
} // namespace directX11
} // namespace visual

# endif // _SCENE_DX11_H
