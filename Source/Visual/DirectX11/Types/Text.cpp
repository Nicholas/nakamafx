#include "Text.h"
// This holds a mesh buffer and a material buffer that will
// be used for rendering the object in a scene.
//
// Project   : NaKama-Fx
// File Name : Text.cpp
// Date      : 26/05/2019
// Author    : Nicholas Welters

#include "../Materials/MaterialBuffer.h"
#include "../Materials/FontMaterialBuffer.h"

#include "Visual/FX/Vertex.h"

#include <glm/gtc/matrix_transform.hpp>

namespace visual
{
namespace directX11
{
	using glm::vec3;

	/// <summary> ctor </summary>
	/// <param name="text"> The text contents of the buffer. </param>
	/// <param name="pFont"> The mesh buffer to give the model shape. </param>
	/// <param name="pString"> The characture instance buffer. </param>
	/// <param name="pFontMaterial"> The constants used to render the model. </param>
	/// <param name="pShadowMaterial"> The constants used to render the model in a depth only pass. </param>
	Text::Text( const std::string& text,
				const FontSPtr& pFont,
				const CharBuffer& pString,
				const FontMaterialBufferSPtr& pFontMaterial,
				const DepthMaterialBufferSPtr&  pShadowMaterial )
		: fx::Model( )
		, text( text )
		, pFont( pFont )
		, pString( pString )
		, pFontMaterial( pFontMaterial )
		, pShadowMaterial( pShadowMaterial )
		, buildBuffer( true )
	{ }

	/// <summary>
	/// Have an external check to see if we mush update the buffers.
	/// </summary>
	/// <returns> True if we must call setText. </returns>
	bool Text::updateBuffer( ) const
	{
		return buildBuffer;
	}

	/// <summary>
	/// Update the text buffer with a new string..
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void Text::setText( const DeviceContextPtr_t& pContext, const std::string& text )
	{
		this->text = text;
		uint32_t numberOfCharacters = 0;
		std::vector< fx::VertexChar::Vertex > instanceUpdateList( pString.size ); // Could just use the mapped pointer....

		/**/
		const glm::vec2 pos     = { 0, 0 };
		const glm::vec2 scale   = { 1, 1 };
		const glm::vec2 padding = { 1, 1 };
		const glm::vec2 topLeft = {          pos.x   * 2.0f - 1.0f
								  , ( 1.0f - pos.y ) * 2.0f - 1.0f };


		float x = topLeft.x;
		float y = topLeft.y;

		const float horizontalPadding = ( pFont->leftPadding + pFont->rightPadding ) * padding.x;
		const float   verticalPadding = ( pFont->topPadding + pFont->bottomPadding ) * padding.y;


		int newLines = 0;

		char lastCharacture = -1;
		for( const char& c: text )
		{

			if( c == '\0' )
			{
				continue;
			}

			if( c == ' ' )
			{
				x += ( pFont->widthSpace - horizontalPadding ) * scale.x;
				continue;
			}

			if( c == '\t' )
			{
				x += ( pFont->widthTab - horizontalPadding ) * scale.x;
				continue;
			}

			if( c == '\n' )
			{
				// Advance to a new line
				lastCharacture = -1;
				newLines++;
				x = topLeft.x;
				y = topLeft.y + newLines * pFont->lineHeight;
				continue;
			}


			const CharInfo* pInfo = pFont->getChar( c );

			if( pInfo == nullptr )
			{
				continue;
			}

			if( numberOfCharacters >= pString.size )
			{
				break;
			}

			float kerning = 0.0f;
			if( lastCharacture != -1 )
			{
				kerning = pFont->getKerning( lastCharacture, pInfo );
			}

			instanceUpdateList[ numberOfCharacters ] = {
					glm::vec4( x + ((pInfo->offset.x + kerning) * scale.x), y - (pInfo->offset.y * scale.y), pInfo->size.x * scale.x, pInfo->size.y * scale.y ),
					glm::vec4( pInfo->uv.x, pInfo->uv.y, pInfo->tSize.x, pInfo->tSize.y ),
					glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f ),
					pInfo->id };

			numberOfCharacters++;

			x += ( pInfo->xAdvance - horizontalPadding ) * scale.x;
			lastCharacture = c;
		}

		for( uint32_t i = numberOfCharacters; i < pString.size; ++i )
		{
			instanceUpdateList[ numberOfCharacters ] = {
					glm::vec4( 0, 0, 0, 0 ),
					glm::vec4( 0, 0, 0, 0 ),
					glm::vec4( 0, 0, 0, 0 ),
					0 };
		}
		//*/

		//for( uint32_t i = numberOfCharacters; i < pString.size; ++i )
		//{
		//	instanceUpdateList[ i ] = {
		//			glm::vec4( i, 0, 1, 1 ),
		//			glm::vec4( 0, 0, 1, 1 ),
		//			glm::vec4( 0, 0, 0, 0 ),
		//			i };
		//}


		ID3D11Buffer* pStringBuffer = pString.pBuffer.get( );

		D3D11_MAPPED_SUBRESOURCE resource;
		pContext->Map( pStringBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource );
		memcpy(resource.pData, &instanceUpdateList[ 0 ], pString.size * pString.elementSize );
		pContext->Unmap( pStringBuffer, 0 );

		buildBuffer = false;
	}

	/// <summary>
	/// Update the text buffer with a new string..
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void Text::setText( const DeviceContextPtr_t& pContext )
	{
		setText( pContext, text );
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void Text::set( const DeviceContextPtr_t& pContext ) const
	{
		// Set vertex buffer stride and offset.
		unsigned int stride = sizeof( fx::VertexChar::Vertex );
		unsigned int offset = 0;

		// Set the vertex buffer to active in the input assembler so it can be rendered.
		ID3D11Buffer* pStringBuffer = pString.pBuffer.get( );
		pContext->IASetVertexBuffers( 0, 1, &pStringBuffer, &stride, &offset );

		// Set the vertex buffer to active in the input assembler so it can be rendered.
		pContext->IASetIndexBuffer( nullptr, DXGI_FORMAT_UNKNOWN, 0 );

		// Set the type of primitive that should be rendered from this vertex buffer.
		pContext->IASetPrimitiveTopology( D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP );
	}

	/// <summary>
	/// Calls a draw from the GPU so that
	/// we can send some data through the GPU.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void Text::render( const DeviceContextPtr_t& pContext ) const
	{
		// Render the triangle.
		//pContext->DrawIndexed( indices.size, 0, 0 );
		pContext->DrawInstanced( 4, pString.size, 0, 0 );
		//pContext->DrawInstanced( 4, 2, 0, 0 );

	}

	/// <summary> Get the constants used to render the model. </summary>
	/// <returns> The model material. </returns>
	FontMaterialBufferSPtr Text::getFontMaterial( ) const
	{
		return pFontMaterial;
	}

	/// <summary> Get the constants used to render the model in a depth only pass. </summary>
	/// <returns> The model depth only material. </returns>
	DepthMaterialBufferSPtr Text::getShadowMaterial( ) const
	{
		return pShadowMaterial;
	}

	/// <summary> The callback that will be used to update the gpu data when the matrix is updated. </summary>
	/// <param name="world"> The models world matrix. </param>
	void Text::onMatrixChanged( const glm::mat4x4& world )
	{
		pFontMaterial->setModelMatrix( transpose( world ) );
	}
} // namespace fx
} // namespace visual

