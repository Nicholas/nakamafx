#pragma once

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : ShaderResource.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _SHADER_RESOURECES_DX11_H
#define _SHADER_RESOURECES_DX11_H

#include "../APITypeDefs.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	/// <summary> Vertex Shader Resources. </summary>
	class VSSetResources
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numViews,
						 _In_ ID3D11ShaderResourceView* const* ppShaderResourceViews );
	};

	/// <summary> Geometry Shader Resources. </summary>
	class GSSetResources
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numViews,
						 _In_ ID3D11ShaderResourceView* const* ppShaderResourceViews );
	};

	/// <summary> Pixel Shader Resources. </summary>
	class PSSetResources
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numViews,
						 _In_ ID3D11ShaderResourceView* const* ppShaderResourceViews );
	};

	/// <summary> Compute Shader Resources. </summary>
	class CSSetResources
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numViews,
						 _In_ ID3D11ShaderResourceView* const* ppShaderResourceViews );
	};


	/// <summary> Manage setting shader resource view sets. </summary>
	template< class GpuResourceSetter >
	class ShaderResources: public GpuResourceSetter
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pResources"> The shader resource views. </param>
		explicit ShaderResources( _In_ const std::vector< ID3D11ShaderResourceViewSPtr >& pResources );

		/// <summary> ctor </summary>
		ShaderResources( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ShaderResources( const ShaderResources& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ShaderResources( ShaderResources&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~ShaderResources( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ShaderResources& operator=( const ShaderResources& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ShaderResources& operator=( ShaderResources&& move ) = default;


		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The device context to register calls to. </param>
		/// <param name="startSlot"> The resource buffer starting slot. </param>
		virtual void set( _In_ const DeviceContextPtr_t& pContext, _In_ uint32_t startSlot ) const;

		/// <summary> Change a resourece in the resource set. </summary>
		/// <param name="index"> The resourse index. </param>
		/// <param name="pSRV"> The new shader resource view to use. </param>
		virtual void set( uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV );

		/// <summary> Get the number of resources in the the set. </summary>
		/// <returns> The number of resources in the set. </returns>
		virtual uint32_t size( ) const;

	private:
		/// <summary> The shader resource vector that we can send to the api. </summary>
		std::vector< ID3D11ShaderResourceView* > resources;

		/// <summary> The shader resource. </summary>
		std::vector< ID3D11ShaderResourceViewSPtr > pResources;
	};

	/// <summary> ctor </summary>
	/// <param name="pResources"> The shader resource views. </param>
	template< class GpuResourceSetter >
	ShaderResources< GpuResourceSetter >::ShaderResources( _In_ const std::vector< ID3D11ShaderResourceViewSPtr >& pResources )
		: GpuResourceSetter( )
		, resources( pResources.size( ) )
		, pResources( pResources )
	{
		for( size_t i = 0; i < pResources.size( ); ++i )
		{
			resources[ i ] = pResources[ i ].get( );
		}
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The device context to register calls to. </param>
	/// <param name="startSlot"> The resource buffer starting slot. </param>
	template<class GpuResourceSetter>
	void ShaderResources< GpuResourceSetter >::set( _In_ const DeviceContextPtr_t & pContext, _In_ const uint32_t startSlot ) const
	{
		if( resources.empty( ) )
		{
			return;
		}

		GpuResourceSetter::set( pContext,
								startSlot,
								static_cast< uint32_t >( resources.size( ) ),
								resources.data( ) );
	}

	/// <summary> Change a resourece in the resource set. </summary>
	/// <param name="index"> The resourse index. </param>
	/// <param name="pSRV"> The new shader resource view to use. </param>
	template<class GpuResourceSetter>
	void ShaderResources< GpuResourceSetter >::set( uint32_t index, const ID3D11ShaderResourceViewSPtr& pSRV )
	{
		if( index < resources.size( ) )
		{
			pResources[ index ] = pSRV;
			resources[ index ] = pSRV.get( );
		}
	}

	/// <summary> Get the number of resources in the the set. </summary>
	/// <returns> The number of resources in the set. </returns>
	template<class GpuResourceSetter>
	uint32_t ShaderResources< GpuResourceSetter >::size( ) const
	{
		return static_cast< uint32_t >( pResources.size( ) );
	}

	typedef ShaderResources< VSSetResources > VSResources;
	typedef ShaderResources< GSSetResources > GSResources;
	typedef ShaderResources< PSSetResources > PSResources;
	typedef ShaderResources< CSSetResources > CSResources;

	SHARED_PTR_TYPE_DEF( VSResources )
	SHARED_PTR_TYPE_DEF( GSResources )
	SHARED_PTR_TYPE_DEF( PSResources )
	SHARED_PTR_TYPE_DEF( CSResources )

} // namespace directX11
} // namespace visual

#endif _SHADER_RESOURECES_DX11_H

