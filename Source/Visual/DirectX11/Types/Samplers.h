#pragma once

// Manage setting shader samplers.
//
// Project   : NaKama-Fx
// File Name : Samplers.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _SAMPLERS_DX11_H
#define _SAMPLERS_DX11_H

#include "../APITypeDefs.h"

namespace visual
{
namespace directX11
{
	/// <summary> Vertex Shader Samplers. </summary>
	class VSSetSamplers
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numSamplers,
						 _In_ ID3D11SamplerState* const* ppSamplers );
	};

	/// <summary> Geometry Shader Samplers. </summary>
	class GSSetSamplers
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numSamplers,
						 _In_ ID3D11SamplerState* const* ppSamplers );
	};

	/// <summary> Pixel Shader Samplers. </summary>
	class PSSetSamplers
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numSamplers,
						 _In_ ID3D11SamplerState* const* ppSamplers );
	};

	/// <summary> Compute Shader Samplers. </summary>
	class CSSetSamplers
	{
	protected:
		static void set( _In_ const DeviceContextPtr_t& pContext,
						 _In_ uint32_t startSlot,
						 _In_ uint32_t numSamplers,
						 _In_ ID3D11SamplerState* const* ppSamplers );
	};


	/// <summary> Manage setting shader resource view sets. </summary>
	template< class GpuResourceSetter >
	class Samplers: public GpuResourceSetter
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pSamplers"> The shader resource views. </param>
		explicit Samplers( _In_ const std::vector< ID3D11SamplerStateSPtr >& pSamplers );

		/// <summary> ctor </summary>
		Samplers( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		Samplers( const Samplers& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		Samplers( Samplers&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~Samplers( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		Samplers& operator=( const Samplers& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		Samplers& operator=( Samplers&& move ) = default;


		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The device context to register calls to. </param>
		/// <param name="startSlot"> The resource buffer starting slot. </param>
		virtual void set( _In_ const DeviceContextPtr_t& pContext, _In_ uint32_t startSlot ) const;

		/// <summary> Get the number of resources in the the set. </summary>
		/// <returns> The number of resources in the set. </returns>
		virtual uint32_t size( ) const;

	private:
		/// <summary> The shader resource vector that we can send to the api. </summary>
		std::vector< ID3D11SamplerState* > samplers;

		/// <summary> The shader resource. </summary>
		std::vector< ID3D11SamplerStateSPtr > pSamplers;
	};

	/// <summary> ctor </summary>
	/// <param name="pSamplers"> The shader resource views. </param>
	template< class GpuResourceSetter >
	Samplers< GpuResourceSetter >::Samplers( _In_ const std::vector< ID3D11SamplerStateSPtr >& pSamplers )
		: GpuResourceSetter( )
		, samplers( pSamplers.size( ) )
		, pSamplers( pSamplers )
	{
		for( size_t i = 0; i < pSamplers.size( ); ++i )
		{
			samplers[ i ] = pSamplers[ i ].get( );
		}
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The device context to register calls to. </param>
	/// <param name="startSlot"> The resource buffer starting slot. </param>
	template<class GpuResourceSetter>
	void Samplers< GpuResourceSetter >::set( _In_ const DeviceContextPtr_t & pContext, _In_ const uint32_t startSlot ) const
	{
		if( samplers.empty( ) )
		{
			return;
		}

		GpuResourceSetter::set( pContext,
								startSlot,
								static_cast< uint32_t >( samplers.size( ) ),
								samplers.data( ) );
	}

	/// <summary> Get the number of resources in the the set. </summary>
	/// <returns> The number of resources in the set. </returns>
	template<class GpuResourceSetter>
	uint32_t Samplers< GpuResourceSetter >::size( ) const
	{
		return static_cast< uint32_t >( pSamplers.size( ) );
	}

	typedef Samplers< VSSetSamplers > VSSamplers;
	typedef Samplers< GSSetSamplers > GSSamplers;
	typedef Samplers< PSSetSamplers > PSSamplers;
	typedef Samplers< CSSetSamplers > CSSamplers;

	SHARED_PTR_TYPE_DEF( VSSamplers )
	SHARED_PTR_TYPE_DEF( GSSamplers )
	SHARED_PTR_TYPE_DEF( PSSamplers )
	SHARED_PTR_TYPE_DEF( CSSamplers )

} // namespace directX11
} // namespace visual

#endif // _SAMPLERS_DX11_H

