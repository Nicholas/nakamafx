#include "ShaderResources.h"

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : ShaderResource.cpp
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	void VSSetResources::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numViews,
							  ID3D11ShaderResourceView* const* ppShaderResourceViews )
	{
		pContext->VSSetShaderResources( startSlot, numViews, ppShaderResourceViews );
	}

	void GSSetResources::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numViews,
							  ID3D11ShaderResourceView* const* ppShaderResourceViews )
	{
		pContext->GSSetShaderResources( startSlot, numViews, ppShaderResourceViews );
	}

	void PSSetResources::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numViews,
							  ID3D11ShaderResourceView* const* ppShaderResourceViews )
	{
		pContext->PSSetShaderResources( startSlot, numViews, ppShaderResourceViews );
	}

	void CSSetResources::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numViews,
							  ID3D11ShaderResourceView* const* ppShaderResourceViews )
	{
		pContext->CSSetShaderResources( startSlot, numViews, ppShaderResourceViews );
	}
} // namespace directX11
} // namespace visual
