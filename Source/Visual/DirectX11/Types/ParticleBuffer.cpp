#include "ParticleBuffer.h"
// Particle Buffer class
//
// This holds our GPU based particle system information.
//
// Project   : NaKama-Fx
// File Name : ParticleBuffer.cpp
// Date      : 01/02/2019
// Author    : Nicholas Welters

#include <utility>

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="initialSize"> The initial particle buffer size. </param>
	/// <param name="pInitialBuffer"> The initial state of the particles syayem (the starting state). </param>
	/// <param name="pUpdateBufferA"> Storage for the current particle states and double buffered for updates. </param>
	/// <param name="pUpdateBufferB"> Storage for the current particle states and double buffered for updates. </param>
	/// <param name="pGSConstants"> The geometry shader constnats needed to expand the particles. </param>
	/// <param name="pPSConstants"> The pixle shader constants needed to draw the particles. </param>
	/// <param name="pPSResources"> The  pixel shader resources needed to draw the particles. </param>
	/// <param name="pBlendState"> The way we will add the particles drawn to the output frame buffer. </param>
	ParticleBuffer::ParticleBuffer( const int                           initialSize,
									const ID3D11BufferSPtr&             pInitialBuffer,
									const ID3D11BufferSPtr&             pUpdateBufferA,
									const ID3D11BufferSPtr&             pUpdateBufferB,
									const ParticleGSBufferSPtr&         pGSConstants,
									const ParticlePSBufferSPtr&         pPSConstants,
									const ID3D11ShaderResourceViewSPtr& pPSResources,
									const ID3D11BlendStateSPtr&         pBlendState )
		: initialSize( initialSize )
		, pInitialParticles( pInitialBuffer )
		, initialise( true )
		, pGSConstants( pGSConstants )
		, pPSConstants( pPSConstants )
		, pPSResources( pPSResources )
		, pBlendState( pBlendState )
		, pUpdateParticles{ pUpdateBufferA, pUpdateBufferB }
		, read( 0 )
		, write( 1 )
	{ }

	/// <summary> Exchange the front and back buffer indeces. </summary>
	void ParticleBuffer::swap( )
	{
		std::swap( read, write );
	}

	/// <summary> Get the particle buffer that will be read on the GPU. </summary>
	const ID3D11BufferSPtr& ParticleBuffer::getReadBuffer( ) const
	{
		return pUpdateParticles[ read ];
	}

	/// <summary> Get the particle buffer that will be update to on the GPU. </summary>
	const ID3D11BufferSPtr& ParticleBuffer::getWriteBuffer( ) const
	{
		return pUpdateParticles[ write ];
	}
} // namespace directX11
} // namespace visual

