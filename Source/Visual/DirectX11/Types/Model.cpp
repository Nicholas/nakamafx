#include "Model.h"
// This holds a mesh buffer and a material buffer that will
// be used for rendering the object in a scene.
//
// Project   : NaKama-Fx
// File Name : Model.cpp
// Date      : 05/06/2017
// Author    : Nicholas Welters

#include "../Materials/MaterialBuffer.h"
#include "../Materials/ColourMaterialBuffer.h"

#include <glm/gtc/matrix_transform.hpp>

namespace visual
{
namespace directX11
{
	using glm::vec3;

	/// <summary> ctor </summary>
	/// <param name="pMesh"> The mesh buffer to give the model shape. </param>
	/// <param name="pColourMaterial"> The constants used to render the model. </param>
	/// <param name="pShadowMaterial"> The constants used to render the model in a depth only pass. </param>
	Model::Model( const MeshBufferSPtr& pMesh,
				  const ColourMaterialBufferSPtr& pColourMaterial,
				  const DepthMaterialBufferSPtr& pShadowMaterial )
		: fx::Model( )
		, pMesh(pMesh)
		, pColourMaterial(pColourMaterial)
		, pShadowMaterial(pShadowMaterial)
	{ }

	/// <summary> Get the models mesh buffer. </summary>
	/// <returns> The model geometry. </returns>
	MeshBufferSPtr Model::getMesh( ) const
	{
		return pMesh;
	}

	/// <summary> Get the constants used to render the model. </summary>
	/// <returns> The model material. </returns>
	ColourMaterialBufferSPtr Model::getColourMaterial( ) const
	{
		return pColourMaterial;
	}

	/// <summary> Get the constants used to render the model in a depth only pass. </summary>
	/// <returns> The model depth only material. </returns>
	DepthMaterialBufferSPtr Model::getShadowMaterial( ) const
	{
		return pShadowMaterial;
	}

	/// <summary> The callback that will be used to update the gpu data when the matrix is updated. </summary>
	/// <param name="world"> The models world matrix. </param>
	void Model::onMatrixChanged( const glm::mat4x4& world )
	{
		pColourMaterial->setModelMatrix( transpose( world ) );
	}
} // namespace fx
} // namespace visual

