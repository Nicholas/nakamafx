#include "ConstantBuffer.h"

// Manage setting a constant buffer.
//
// Project   : NaKama-Fx
// File Name : ConstantBuffer.cpp
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	void VSSetConstant::set( const DeviceContextPtr_t & pContext,
							 const uint32_t startSlot,
							 ID3D11Buffer* const* ppConstantBuffers )
	{
		pContext->VSSetConstantBuffers ( startSlot, 1, ppConstantBuffers );
	}

	void GSSetConstant::set( const DeviceContextPtr_t & pContext,
							 const uint32_t startSlot,
							 ID3D11Buffer* const* ppConstantBuffers )
	{
		pContext->GSSetConstantBuffers ( startSlot, 1, ppConstantBuffers );
	}

	void PSSetConstant::set( const DeviceContextPtr_t & pContext,
							 const uint32_t startSlot,
							 ID3D11Buffer* const* ppConstantBuffers )
	{
		pContext->PSSetConstantBuffers ( startSlot, 1, ppConstantBuffers );
	}

	void CSSetConstant::set( const DeviceContextPtr_t & pContext,
							 const uint32_t startSlot,
							 ID3D11Buffer* const* ppConstantBuffers )
	{
		pContext->CSSetConstantBuffers ( startSlot, 1, ppConstantBuffers );
	}
} // namespace directX11
} // namespace visual
