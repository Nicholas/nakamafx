#include "Vertex.h"

// A wrapper for vertices so taht they contain the info needed for Direct X 11.
//
// Project   : NaKama-Fx
// File Name : Vertex.h
// Date      : 12/11/2014
// Author    : Nicholas Welters

#include "Visual/FX/Vertex.h"

namespace visual
{
namespace directX11
{
	using std::string;
	using fx::Vertex3f4f2f3f;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex3f4f2f3f >::vertexName = "Vertex3f4f2f3f";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex3f4f2f3f >::description[ Vertex::size ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT   , 0, 0                           , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR"   , 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT      , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL"  , 0, DXGI_FORMAT_R32G32B32_FLOAT   , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex3f4f2f3f >::soDescription[ Vertex::size ] =
	{
		{ 0, "POSITION", 0, 0, 3, 0 },
		{ 0, "COLOR"   , 0, 0, 4, 0 },
		{ 0, "TEXCOORD", 0, 0, 2, 0 },
		{ 0, "NORMAL"  , 0, 0, 3, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex3f4f2f3f >::vertexShader = R"(
		struct VertexInputType
		{
			float3 position  : POSITION;
			float4 colour    : COLOR;
			float2 textureUV : TEXCOORD;
			float3 normal    : NORMAL;
		};

		float4 main( VertexInputType input ) : SV_POSITION
		{ return float4( input.position, 1 ); })";


	using fx::Vertex3f2f;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex3f2f >::vertexName = "Vertex3f2f";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex3f2f >::description[ Vertex::size ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0                           , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT   , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex3f2f >::soDescription[ Vertex::size ] =
	{
		{ 0, "POSITION", 0, 0, 3, 0 },
		{ 0, "TEXCOORD", 0, 0, 2, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex3f2f >::vertexShader = R"(
		struct VertexInputType
		{
			float3 position  : POSITION;
			float2 textureUV : TEXCOORD;
		};

		float4 main( VertexInputType input ) : SV_POSITION
		{ return float4( input.position, 1 ); })";


	using fx::Vertex3f2f3f;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex3f2f3f >::vertexName = "Vertex3f2f3f";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex3f2f3f >::description[ Vertex::size ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0                           , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT   , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL"  , 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex3f2f3f >::soDescription[ Vertex::size ] =
	{
		{ 0, "POSITION", 0, 0, 3, 0 },
		{ 0, "TEXCOORD", 0, 0, 2, 0 },
		{ 0, "NORMAL"  , 0, 0, 3, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex3f2f3f >::vertexShader = R"(
		struct VertexInputType
		{
			float3 position  : POSITION;
			float2 textureUV : TEXCOORD;
			float3 normal    : NORMAL;
		};

		float4 main( VertexInputType input ) : SV_POSITION
		{ return float4( input.position, 1 ); })";


	using fx::Vertex3f2f4f;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex3f2f4f >::vertexName = "Vertex3f2f4f";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex3f2f4f >::description[ Vertex::size ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT   , 0, 0                           , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT      , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR"   , 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex3f2f4f >::soDescription[ Vertex::size ] =
	{
		{ 0, "POSITION", 0, 0, 3, 0 },
		{ 0, "TEXCOORD", 0, 0, 2, 0 },
		{ 0, "COLOR"   , 0, 0, 4, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex3f2f4f >::vertexShader = R"(
		struct VertexInputType
		{
			float3 position  : POSITION;
			float2 textureUV : TEXCOORD;
			float4 colour    : COLOR;
		};

		float4 main( VertexInputType input ) : SV_POSITION
		{ return float4( input.position, 1 ); })";


	using fx::Vertex3f2f3f3f3f;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex3f2f3f3f3f >::vertexName = "Vertex3f2f3f3f3f";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex3f2f3f3f3f >::description[ Vertex::size ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0                           , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT   , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL"  , 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT" , 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex3f2f3f3f3f >::soDescription[ Vertex::size ] =
	{
		{ 0, "POSITION", 0, 0, 3, 0 },
		{ 0, "TEXCOORD", 0, 0, 2, 0 },
		{ 0, "NORMAL"  , 0, 0, 3, 0 },
		{ 0, "TANGENT" , 0, 0, 3, 0 },
		{ 0, "BINORMAL", 0, 0, 3, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex3f2f3f3f3f >::vertexShader = R"(
		struct VertexInputType
		{
			float3 position  : POSITION;
			float2 textureUV : TEXCOORD;
			float3 normal    : NORMAL;
			float3 tangent   : TANGENT;
			float3 binormal  : BINORMAL;
		};

		float4 main( VertexInputType input ) : SV_POSITION
		{ return float4( input.position, 1 ); })";


	using fx::Vertex3f2f2f1f1f1u;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex3f2f2f1f1f1u >::vertexName = "Vertex3f2f2f1f1f1u";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex3f2f2f1f1f1u >::description[ Vertex::size ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0                           , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT   , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 2, DXGI_FORMAT_R32_FLOAT      , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 3, DXGI_FORMAT_R32_FLOAT      , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 4, DXGI_FORMAT_R32_UINT       , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex3f2f2f1f1f1u >::soDescription[ Vertex::size ] =
	{
		{ 0, "POSITION", 0, 0, 3, 0 },
		{ 0, "TEXCOORD", 0, 0, 3, 0 },
		{ 0, "TEXCOORD", 1, 0, 2, 0 },
		{ 0, "TEXCOORD", 2, 0, 1, 0 },
		{ 0, "TEXCOORD", 3, 0, 1, 0 },
		{ 0, "TEXCOORD", 4, 0, 1, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex3f2f2f1f1f1u >::vertexShader = R"(
		struct VertexInputType
		{
			float3 initialPos: POSITION;
			float3 initialVel: TEXCOORD0;
			float2 size      : TEXCOORD1;
			float age        : TEXCOORD2;
			float id         : TEXCOORD3;
			uint type        : TEXCOORD4;
		};

		float4 main( VertexInputType input ) : SV_POSITION
		{ return float4( input.initialPos, 1 ); })";


	using fx::Vertex4f4f4f1u;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex4f4f4f1u >::vertexName = "Vertex4f4f4f1u";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex4f4f4f1u >::description[ Vertex::size ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0                           , D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "COLOR"   , 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32_UINT          , 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex4f4f4f1u >::soDescription[ Vertex::size ] =
	{
		{ 0, "POSITION", 0, 0, 4, 0 },
		{ 0, "TEXCOORD", 0, 0, 4, 0 },
		{ 0, "COLOR"   , 0, 0, 4, 0 },
		{ 0, "TEXCOORD", 1, 0, 1, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex4f4f4f1u >::vertexShader = R"(
		struct VertexInputType
		{
			float4 position : POSITION;
			float4 textureUV: TEXCOORD0;
			float4 colour   : COLOR0;
			uint   id       : TEXCOORD1;
		};

		float4 main( VertexInputType input, uint vertexID : SV_VertexID ) : SV_POSITION
		{ return float4( 1, 1, 1, 1 ); })";


	using fx::Vertex4x4f;

	/// <summary> A unique string to identify this vertex type. </summary>
	template<>
	const string VertexLayout< Vertex4x4f >::vertexName = "Vertex4x4f";

	/// <summary> Initialise the vertex layout so that we can use later. </summary>
	template<>
	D3D11_INPUT_ELEMENT_DESC VertexLayout< Vertex4x4f >::description[ Vertex::size ] =
	{
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0                           , D3D11_INPUT_PER_VERTEX_DATA, 1 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 1 },
		{ "TEXCOORD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 1 },
		{ "TEXCOORD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 1 },
	};

	/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
	template<>
	D3D11_SO_DECLARATION_ENTRY VertexLayout< Vertex4x4f >::soDescription[ Vertex::size ] =
	{
		{ 0, "TEXCOORD", 0, 0, 4, 0 },
		{ 0, "TEXCOORD", 1, 0, 4, 0 },
		{ 0, "TEXCOORD", 2, 0, 4, 0 },
		{ 0, "TEXCOORD", 3, 0, 1, 0 }
	};

	/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
	template<>
	const string VertexLayout< Vertex4x4f >::vertexShader = R"(
		struct VertexInputType
		{
			float4 world0: TEXCOORD0;
			float4 world1: TEXCOORD1;
			float4 world2: TEXCOORD2;
			float4 world3: TEXCOORD3;
		};

		float4 main( VertexInputType input, uint vertexID : SV_VertexID ) : SV_POSITION
		{ return float4( 0, 0, 0, 0 ); })";
} // namespace directX11
} // namespace visual
