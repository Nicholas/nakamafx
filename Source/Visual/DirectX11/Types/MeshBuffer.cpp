#include "MeshBuffer.h"

// Mesh Buffer class
//
// This holds our GPU based mesh information, a vertex and index buffer and topology.
//
// Project   : NaKama-Fx
// File Name : MeshBuffer.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	using fx::MeshPrimitive;

	D3D11_PRIMITIVE_TOPOLOGY MeshBuffer::PrimativeMap[ MeshPrimitive::SIZE ] = {
		D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
		D3D11_PRIMITIVE_TOPOLOGY_LINELIST,
		D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
		D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED
	};

	/// <summary> ctor </summary>
	/// <param name="vertices"> The vertex buffer. </param>
	/// <param name="indices"> The index buffer. </param>
	/// <param name="topology"> The vertices topology layout. </param>
	MeshBuffer::MeshBuffer( const VertexBuffer& vertices, const IndexBuffer& indices, const D3D11_PRIMITIVE_TOPOLOGY& topology )
		: vertices( vertices )
		, indices( indices )
		, topology( topology )
	{
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void MeshBuffer::set( const DeviceContextPtr_t& pContext ) const
	{
		// Set vertex buffer stride and offset.
		unsigned int stride = vertices.elementSize;
		unsigned int offset = 0;

		ID3D11Buffer* pVertexBuffer = vertices.pBuffer.get();
		ID3D11Buffer* pIndexBuffer = indices.pBuffer.get();

		// Set the vertex buffer to active in the input assembler so it can be rendered.
		pContext->IASetVertexBuffers( 0, 1, &pVertexBuffer, &stride, &offset );

		// Set the index buffer to active in the input assembler so it can be rendered.
		pContext->IASetIndexBuffer( pIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );

		// Set the type of primitive that should be rendered from this vertex buffer.
		pContext->IASetPrimitiveTopology( topology );
	}

	/// <summary>
	/// Calls a draw from the GPU so that
	/// we can send some data through the GPU.
	/// </summary>
	/// <param name="pContext"> The context to set the target too. </param>
	void MeshBuffer::render( const DeviceContextPtr_t& pContext ) const
	{
		// Render the triangle.
		pContext->DrawIndexed( indices.size, 0, 0 );
	}
} // namespace directX11
} // namespace visual

