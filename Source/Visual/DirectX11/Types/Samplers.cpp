#include "Samplers.h"

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : ShaderResource.cpp
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	void VSSetSamplers::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numSamplers,
							  ID3D11SamplerState* const* ppSamplers )
	{
		pContext->VSSetSamplers( startSlot, numSamplers, ppSamplers );
	}

	void GSSetSamplers::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numSamplers,
							  ID3D11SamplerState* const* ppSamplers )
	{
		pContext->GSSetSamplers( startSlot, numSamplers, ppSamplers );
	}

	void PSSetSamplers::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numSamplers,
							  ID3D11SamplerState* const* ppSamplers )
	{
		pContext->PSSetSamplers( startSlot, numSamplers, ppSamplers );
	}

	void CSSetSamplers::set( const DeviceContextPtr_t & pContext,
							  const uint32_t startSlot,
							  const uint32_t numSamplers,
							  ID3D11SamplerState* const* ppSamplers )
	{
		pContext->CSSetSamplers( startSlot, numSamplers, ppSamplers );
	}
} // namespace directX11
} // namespace visual
