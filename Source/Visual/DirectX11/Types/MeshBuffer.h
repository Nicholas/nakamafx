#pragma once

// Mesh Buffer class
//
// This holds our GPU based mesh information, a vertex and index buffer and topology.
//
// Project   : NaKama-Fx
// File Name : MeshBuffer.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _DX11_MESH_BUFFER_H
#define _DX11_MESH_BUFFER_H

#include "../APITypeDefs.h"
#include "Visual/FX/Mesh.h"

#include <d3d11.h>

namespace visual
{
namespace directX11
{
	typedef ArrayBuffer VertexBuffer;
	typedef ArrayBuffer IndexBuffer;

	/// <summary> The mesh buffers. A vertex and index buffer and topology </summary>
	class MeshBuffer
	{
		public:
			static D3D11_PRIMITIVE_TOPOLOGY PrimativeMap[ fx::MeshPrimitive::SIZE ];

		public:
			/// <summary> ctor </summary>
			/// <param name="vertices"> The vertex buffer. </param>
			/// <param name="indices"> The index buffer. </param>
			/// <param name="topology"> The vertices topology layout. </param>
			MeshBuffer( const VertexBuffer& vertices,
						const IndexBuffer& indices,
						const D3D11_PRIMITIVE_TOPOLOGY& topology );

			/// <summary> ctor </summary>
			MeshBuffer( )=default;

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			MeshBuffer( const MeshBuffer& copy )=default;

			/// <summary> move ctor </summary>
			/// <param name="move"> The temporary to move. </param>
			MeshBuffer( MeshBuffer&& move )=default;


			/// <summary> dtor </summary>
			~MeshBuffer( )=default;


			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			MeshBuffer& operator=( const MeshBuffer& copy )=default;

			/// <summary> Move assignment operator </summary>
			/// <param name="move"> The temporary to move. </param>
			MeshBuffer& operator=( MeshBuffer&& move )=default;

			/// <summary>
			/// Set the buffers as active on the GPU so that
			/// they can be used for the next draw call.
			/// </summary>
			/// <param name="pContext"> The context to set the target too. </param>
			void set( const DeviceContextPtr_t& pContext ) const;

			/// <summary>
			/// Calls a draw from the GPU so that
			/// we can send some data through the GPU.
			/// </summary>
			/// <param name="pContext"> The context to set the target too. </param>
			void render( const DeviceContextPtr_t& pContext ) const;

		private:
			/// <summary> The list of vertices. </summary>
			VertexBuffer vertices = { 0, nullptr, 0 };

			/// <summary>
			/// Our index buffer for the vertex buffer
			/// so that we can produce primitives.
			/// </summary>
			IndexBuffer  indices = { 0, nullptr, 0 };

			/// <summary> The primitive structure of the vertex buffer. </summary>
			D3D11_PRIMITIVE_TOPOLOGY topology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
	};
} // namespace directX11
} // namespace visual

#endif // _DX11_MESH_BUFFER_H
