#pragma once
// Particle Buffer class
//
// This holds our GPU based particle system information.
//
// Currently we are using two intagers to switch between the read and write buffers.
// We could just use pointers here and swap those rather than the indeces.
//
// Project   : NaKama-Fx
// File Name : ParticleBuffer.h
// Date      : 01/02/2019
// Author    : Nicholas Welters

#ifndef _PARTICLE_BUFFER_DX11_H
#define _PARTICLE_BUFFER_DX11_H

#include "../APITypeDefs.h"
#include "../Materials/MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Particle System data ans state. </summary>
	class ParticleBuffer
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="initialSize"> The initial particle buffer size. </param>
		/// <param name="pInitialBuffer"> The initial state of the particles syayem (the starting state). </param>
		/// <param name="pUpdateBufferA"> Storage for the current particle states and double buffered for updates. </param>
		/// <param name="pUpdateBufferB"> Storage for the current particle states and double buffered for updates. </param>
		/// <param name="pGSConstants"> The geometry shader constnats needed to expand the particles. </param>
		/// <param name="pPSConstants"> The pixle shader constants needed to draw the particles. </param>
		/// <param name="pPSResources"> The  pixel shader resources needed to draw the particles. </param>
		/// <param name="pBlendState"> The way we will add the particles drawn to the output frame buffer. </param>
		ParticleBuffer( int                                 initialSize,
						const ID3D11BufferSPtr&             pInitialBuffer,
						const ID3D11BufferSPtr&             pUpdateBufferA,
						const ID3D11BufferSPtr&             pUpdateBufferB,
						const ParticleGSBufferSPtr&         pGSConstants,
						const ParticlePSBufferSPtr&         pPSConstants,
						const ID3D11ShaderResourceViewSPtr& pPSResources,
						const ID3D11BlendStateSPtr&         pBlendState );

		/// <summary> ctor </summary>
		ParticleBuffer( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ParticleBuffer( const ParticleBuffer& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ParticleBuffer( ParticleBuffer&& move ) = default;


		/// <summary> dtor </summary>
		~ParticleBuffer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ParticleBuffer& operator=( const ParticleBuffer& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ParticleBuffer& operator=( ParticleBuffer&& move ) = default;


		/// <summary> Exchange the front and back buffer indeces. </summary>
		void swap( );

		/// <summary> Get the particle buffer that will be read on the GPU. </summary>
		/// <returns> The buffer to read from. </returns>
		const ID3D11BufferSPtr& getReadBuffer( ) const;

		/// <summary> Get the particle buffer that will be update to on the GPU. </summary>
		/// <returns> The buffer to write to. </returns>
		const ID3D11BufferSPtr& getWriteBuffer( ) const;

	public:
		/// <summary> The initial particle buffer size. </summary>
		int initialSize = 0;

		/// <summary> The initial state of the particles syayem (the starting state). </summary>
		ID3D11BufferSPtr pInitialParticles = nullptr;

		/// <summary>
		/// A flag to indicate if the particle system needed to use the
		/// initial state buffer to generate the frames particles.
		/// </summary>
		bool initialise = true;

		/// <summary> The geometry shader constnats needed to expand the particles. </summary>
		ParticleGSBufferSPtr pGSConstants = nullptr;

		/// <summary> The pixle shader constants needed to draw the particles. </summary>
		ParticlePSBufferSPtr pPSConstants = nullptr;

		/// <summary> The  pixel shader resources needed to draw the particles. </summary>
		ID3D11ShaderResourceViewSPtr pPSResources = nullptr;

		/// <summary> The way we will add the particles drawn to the output frame buffer. </summary>
		ID3D11BlendStateSPtr pBlendState = nullptr;

	private:
		/// <summary> Double buffered storage for the current particle states and the last updates. </summary>
		ID3D11BufferSPtr pUpdateParticles[ 2 ] = { nullptr, nullptr };

		/// <summary>
		/// The index of the 'read' buffer in particle system. If initialise
		/// is true then the initial buffer is used instead.
		/// </summary>
		int read = 0;

		/// <summary>
		/// The index of the 'write' buffer for updates,
		/// its then used to draw the particles for the frame.
		/// </summary>
		int write = 1;
	};
} // namespace directX11
} // namespace visual

#endif // _PARTICLE_BUFFER_DX11_H
