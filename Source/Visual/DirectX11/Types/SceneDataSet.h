#pragma once
// A rendered scenes data set so that we can move the whole lot around in one go.
//
// Project   : NaKama-Fx
// File Name : SceneDataSet.h
// Date      : 07/12/2016
// Author    : Nicholas Welters

#ifndef _SCENE_DATA_SET_DX11_H
#define _SCENE_DATA_SET_DX11_H

#include "ShaderResources.h"
#include "Visual/FX/Types/Light.h"

#include <vector>
#include <Macros.h>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( Model );
	FORWARD_DECLARE( Text );
	FORWARD_DECLARE( ParticleBuffer );

	// This will hold the elements of the scene that must be drawn
	struct SceneDataSet
	{
		std::vector< ModelSPtr > spheres;
		std::vector< ModelSPtr > pbrSpheres;
		std::vector< ModelSPtr > planets;
		std::vector< ModelSPtr > moons;
		std::vector< ModelSPtr > cubes;
		std::vector< ModelSPtr > tiles;
		std::vector< ModelSPtr > basicObjs;
		std::vector< ModelSPtr > clippedObjs;
		std::vector< ModelSPtr > translucent;
		std::vector< ModelSPtr > heightMaps;
		std::vector< ModelSPtr > pcgHeightMaps;
		std::vector< ModelSPtr > water;
		std::vector< ModelSPtr > pcgWater;
		std::vector< ModelSPtr > pcgWaterFollower;
		std::vector< TextSPtr > text2D;
		std::vector< TextSPtr > text3D;

		// Particle Systems
		std::vector< ParticleBufferSPtr > particles;

		// +-----------------+
		// | Sky Box Options |
		// +-----------------+
		bool enablePolarSky = false;
		PSResourcesSPtr polarSky;
		bool enableCubeSky = false;
		PSResourcesSPtr cubeSky;
		bool enableBasicVolumeSky = false;
		PSResourcesSPtr basicVolumeSky;
		glm::vec3 sunDirection;

		// Fog
		float density = 0;
		float heightFalloff = 1;
		float rayDepth = 0;
		float gScatter = 0;
		float waterHeight = 0;

		// Lights
		bool hasLights = false;
		UpdateableSrv gpuLights;
		std::vector< fx::LightEx > cpuLights;
	};
} // namespace directX11
} // namespace visual

#endif // _SCENE_DATA_SET_DX11_H