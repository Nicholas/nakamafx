#pragma once

// A wrapper for vertices so taht they contain the info needed for Direct X 11.
//
// Project   : NaKama-Fx
// File Name : Vertex.h
// Date      : 12/11/2014
// Author    : Nicholas Welters

#ifndef _VERTEX_LAYOUT_DX11_H
#define _VERTEX_LAYOUT_DX11_H

#include <d3d11.h>
#include <string>

namespace visual
{
namespace directX11
{
	/// <summary> 
	/// A vertex description object that uses template specialization
	/// to provide a DX11 layout for the vertex.
	/// </summary> 
	template< class VertexType >
	class VertexLayout
	{
		public:
			typedef VertexType Vertex;

			/// <summary> A unique string to identify this vertex type. </summary>
			static const std::string vertexName;

			/// <summary> Input layout describing the vertex to the GFX card. </summary>
			static D3D11_INPUT_ELEMENT_DESC description[ Vertex::size ];

			/// <summary> Stream out layout describing the vertex to the GFX card. </summary>
			static D3D11_SO_DECLARATION_ENTRY soDescription[ Vertex::size ];

			/// <summary> This is a simple vertex shader that can be used to validate the Input layout </summary>
			static const std::string vertexShader;
	};

} // namespace directX11
} // namespace visual

#endif _VERTEX_LAYOUT_DX11_H

