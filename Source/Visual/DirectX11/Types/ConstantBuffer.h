#pragma once

// Manage setting a constant buffer.
//
// Project   : NaKama-Fx
// File Name : ConstantBuffer.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _CONSTANT_BUFFER_DX11_H
#define _CONSTANT_BUFFER_DX11_H

#include "../APITypeDefs.h"

#include <d3d11.h>


#define DECLARE_VS_CONSTANT_BUFFER( BASE_TYPE, NAME ) DECLARE_CONSTANT_BUFFER( VS, BASE_TYPE, NAME )
#define DECLARE_GS_CONSTANT_BUFFER( BASE_TYPE, NAME ) DECLARE_CONSTANT_BUFFER( GS, BASE_TYPE, NAME )
#define DECLARE_PS_CONSTANT_BUFFER( BASE_TYPE, NAME ) DECLARE_CONSTANT_BUFFER( PS, BASE_TYPE, NAME )
#define DECLARE_CS_CONSTANT_BUFFER( BASE_TYPE, NAME ) DECLARE_CONSTANT_BUFFER( CS, BASE_TYPE, NAME )

namespace visual
{
namespace directX11
{
	/// <summary> Vertex Shader Constants. </summary>
	class VSSetConstant
	{
	protected:
		static void set( const DeviceContextPtr_t& pContext,
						 uint32_t startSlot,
						 ID3D11Buffer* const* ppConstantBuffers );
	};

	/// <summary> Geometry Shader Constants. </summary>
	class GSSetConstant
	{
	protected:
		static void set( const DeviceContextPtr_t& pContext,
						 uint32_t startSlot,
						 ID3D11Buffer* const* ppConstantBuffers );
	};

	/// <summary> Pixel Shader Constants. </summary>
	class PSSetConstant
	{
	protected:
		static void set( const DeviceContextPtr_t& pContext,
						 uint32_t startSlot,
						 ID3D11Buffer* const* ppConstantBuffers );
	};

	/// <summary> Compute Shader Constants. </summary>
	class CSSetConstant
	{
	protected:
		static void set( const DeviceContextPtr_t& pContext,
						 uint32_t startSlot,
						 ID3D11Buffer* const* ppConstantBuffers );
	};


	/// <summary> Manage setting shader resource view sets. </summary>
	template< class ConsantType, class GpuConstantSetter >
	class ConstantBuffer : public GpuConstantSetter
	{
	public:
		typedef ConsantType Constants;

	public:
		/// <summary> ctor </summary>
		/// <param name="startSlot"> The shader constant buffer binding slot. </param>
		/// <param name="pBuffer"> The shader constant buffer. </param>
		ConstantBuffer( _In_ uint32_t startSlot, _In_ const ID3D11BufferSPtr& pBuffer );

		/// <summary> ctor </summary>
		ConstantBuffer( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ConstantBuffer( const ConstantBuffer& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ConstantBuffer( ConstantBuffer&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~ConstantBuffer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ConstantBuffer& operator=( const ConstantBuffer& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ConstantBuffer& operator=( ConstantBuffer&& move ) = default;

		/// <summary>
		/// Copy the changes made to the material
		/// so that it can be used in a shader to render.
		/// </summary>
		/// <param name="pContext"> The context to update the context with. </param>
		void update( _In_ const DeviceContextPtr_t& pContext );

		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The context to set the contrext with. </param>
		virtual void set( _In_ const DeviceContextPtr_t& pContext );

		/// <summary>
		/// Get a reference to the constant buffers
		/// so that we can update the data.
		/// Note: This will trigger a data update.
		/// </summary>
		/// <returns> A referance to the constants CPU staging buffer. </returns>
		Constants& get( );

	private:
		/// <summary> The constant buffer dirty flag. </summary>
		bool updateBuffer = false;

		/// <summary> The constants buffer binding slot. </summary>
		uint32_t startSlot = 0;

		/// <summary> The constants buffer array that we send to the api. </summary>
		ID3D11Buffer* buffer[ 1 ] = { nullptr };

		/// <summary> The constants buffer. </summary>
		ID3D11BufferSPtr pBuffer = nullptr;

		/// <summary> The CPU staging constants buffer. </summary>
		Constants constants;
	};

	/// <summary> ctor </summary>
	/// <param name="startSlot"> The shader constant buffer binding slot. </param>
	/// <param name="pBuffer"> The shader constant buffer. </param>
	template< class ConsantType, class GpuConstantSetter >
	ConstantBuffer< ConsantType, GpuConstantSetter >::ConstantBuffer( _In_ const uint32_t          startSlot,
																	  _In_ const ID3D11BufferSPtr& pBuffer )
		: GpuConstantSetter( )
		, updateBuffer( true )
		, startSlot( startSlot )
		, buffer{ pBuffer.get( ) }
		, pBuffer( pBuffer )
		, constants( )
	{ }

	/// <summary>
	/// Copy the changes made to the material
	/// so that it can be used in a shader to render.
	/// </summary>
	/// <param name="pContext"> The context to update the context with. </param>
	template< class ConsantType, class GpuConstantSetter >
	void ConstantBuffer< ConsantType, GpuConstantSetter >::update( _In_ const DeviceContextPtr_t& pContext )
	{
		if( updateBuffer )
		{
			updateBuffer = false;
			pContext->UpdateSubresource( buffer[ 0 ], 0, nullptr, &constants, 0, 0 );
		}
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the contrext with. </param>
	template< class ConsantType, class GpuConstantSetter >
	void ConstantBuffer< ConsantType, GpuConstantSetter >::set( _In_ const DeviceContextPtr_t& pContext )
	{
		GpuConstantSetter::set( pContext, startSlot, buffer );
	}

	/// <summary>
	/// Get a reference to the constant buffers
	/// so that we can update the data.
	/// Note: This will trigger a data update.
	/// </summary>
	/// <returns> A referance to the constants CPU staging buffer. </returns>
	template< class ConsantType, class GpuConstantSetter >
	typename ConstantBuffer< ConsantType, GpuConstantSetter >::Constants&
		ConstantBuffer< ConsantType, GpuConstantSetter >::get( )
	{
		updateBuffer = true;
		return constants;
	}
} // namespace directX11
} // namespace visual

#endif _SHADER_RESOURECES_DX11_H

