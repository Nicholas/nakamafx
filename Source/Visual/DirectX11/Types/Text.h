#pragma once
// This is a text buffer,
// this will handle drawing our text and managing the text buffers.
//
// Project   : NaKama-Fx
// File Name : Text.h
// Date      : 26/05/2019
// Author    : Nicholas Welters

#ifndef _TEXT_DX11_H
#define _TEXT_DX11_H

#include "../APITypeDefs.h"
#include "Font.h"
#include "Visual/FX/Types/Model.h"
#include <Macros.h>

namespace visual
{
namespace directX11
{
	typedef ArrayBuffer CharBuffer;

	FORWARD_DECLARE( FontMaterialBuffer );
	FORWARD_DECLARE( DepthMaterialBuffer );
	FORWARD_DECLARE( Font );

	class Text : public fx::Model
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="text"> The text contents of the buffer. </param>
		/// <param name="pFont"> The string font info so that we can build the charactures. </param>
		/// <param name="pString"> The characture instance buffer. </param>
		/// <param name="pFontMaterial"> The constants used to render the model. </param>
		/// <param name="pShadowMaterial"> The constants used to render the model in a depth only pass. </param>
		Text( const std::string& text,
			  const FontSPtr& pFont,
			  const CharBuffer& pString,
			  const FontMaterialBufferSPtr& pFontMaterial,
			  const DepthMaterialBufferSPtr&  pShadowMaterial );

		/// <summary> ctor </summary>
		Text( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		Text( const Text& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		Text( Text&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~Text( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		Text& operator=( const Text& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		Text& operator=( Text&& move ) = default;

		/// <summary>
		/// Have an external check to see if we mush update the buffers.
		/// </summary>
		/// <returns> True if we must call setText. </returns>
		bool updateBuffer( ) const;

		/// <summary>
		/// Update the text buffer with a new string..
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		/// <param name="text"> The new contents of the buffer. </param>
		void setText( const DeviceContextPtr_t& pContext, const std::string& text );

		/// <summary>
		/// Update the text buffer with a new string..
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		void setText( const DeviceContextPtr_t& pContext );

		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		void set( const DeviceContextPtr_t& pContext ) const;

		/// <summary>
		/// Calls a draw from the GPU so that
		/// we can send some data through the GPU.
		/// </summary>
		/// <param name="pContext"> The context to set the target too. </param>
		void render( const DeviceContextPtr_t& pContext ) const;

		/// <summary> Get the constants used to render the model. </summary>
		/// <returns> The model material. </returns>
		FontMaterialBufferSPtr getFontMaterial( ) const;

		/// <summary> Get the constants used to render the model in a depth only pass. </summary>
		/// <returns> The model depth only material. </returns>
		DepthMaterialBufferSPtr  getShadowMaterial( ) const;

	protected:
		/// <summary> The callback that will be used to update the gpu data when the matrix is updated. </summary>
		/// <param name="world"> The models world matrix. </param>
		virtual void onMatrixChanged( const glm::mat4x4& world ) final override;

	private:
		/// <summmary> This is the text contents of the buffer. </summary>
		std::string text;

		/// <summary> The string font info so that we can build the charactures. </summary>
		FontSPtr pFont;

		/// <summary> The list of charactures and there properties to draw them. </summary>
		CharBuffer pString = { 0, nullptr };

		/// <summary> The constants used to render the model. </summary>
		FontMaterialBufferSPtr pFontMaterial;

		/// <summary> The constants used to render the model in a depth only pass. </summary>
		DepthMaterialBufferSPtr  pShadowMaterial;

		/// <summmary> A flag used to trigger the reconstruction of the buffer. </summary>
		bool buildBuffer;
	};
} // namespace fx
} // namespace visual

#endif // _TEXT_DX11_H
