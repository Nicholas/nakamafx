#pragma once
// This holds a mesh buffer and a material buffer that will
// be used for rendering the object in a scene.
//
// Project   : NaKama-Fx
// File Name : Model.h
// Date      : 05/06/2017
// Author    : Nicholas Welters

#ifndef _MODEL_DX11_H
#define _MODEL_DX11_H

#include <Visual/FX/Types/Model.h>
#include <Macros.h>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( MeshBuffer );
	FORWARD_DECLARE( ColourMaterialBuffer );
	FORWARD_DECLARE( DepthMaterialBuffer );

	class Model : public fx::Model
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pMesh"> The mesh buffer to give the model shape. </param>
		/// <param name="pColourMaterial"> The constants used to render the model. </param>
		/// <param name="pShadowMaterial"> The constants used to render the model in a depth only pass. </param>
		Model( const MeshBufferSPtr&           pMesh,
			   const ColourMaterialBufferSPtr& pColourMaterial,
			   const DepthMaterialBufferSPtr&  pShadowMaterial );

		/// <summary> ctor </summary>
		Model( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		Model( const Model& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		Model( Model&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~Model( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		Model& operator=( const Model& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		Model& operator=( Model&& move ) = default;


		/// <summary> Get the models mesh buffer. </summary>
		/// <returns> The model geometry. </returns>
		MeshBufferSPtr getMesh( ) const;

		/// <summary> Get the constants used to render the model. </summary>
		/// <returns> The model material. </returns>
		ColourMaterialBufferSPtr getColourMaterial( ) const;

		/// <summary> Get the constants used to render the model in a depth only pass. </summary>
		/// <returns> The model depth only material. </returns>
		DepthMaterialBufferSPtr  getShadowMaterial( ) const;

	protected:
		/// <summary> The callback that will be used to update the gpu data when the matrix is updated. </summary>
		/// <param name="world"> The models world matrix. </param>
		virtual void onMatrixChanged( const glm::mat4x4& world ) final override;

	private:
		/// <summary> The mesh buffer to give the model shape. </summary>
		MeshBufferSPtr pMesh;

		/// <summary> The constants used to render the model. </summary>
		ColourMaterialBufferSPtr pColourMaterial;

		/// <summary> The constants used to render the model in a depth only pass. </summary>
		DepthMaterialBufferSPtr  pShadowMaterial;
	};
} // namespace fx
} // namespace visual

#endif // _MODEL_H
