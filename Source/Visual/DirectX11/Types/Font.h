#pragma once
// A simple uh class to hold some font information for us.
//
// Project   : NaKama-Fx
// File Name : Font.h
// Date      : 26/05/2019
// Author    : Nicholas Welters

#ifndef _FONT_DX11_H
#define _FONT_DX11_H

#include "../APITypeDefs.h"
#include <glm/glm.hpp>
#include <vector>

namespace visual
{
namespace directX11
{
	struct CharInfo
		{
			char characture = '\0';
			uint32_t id = 0;

			glm::vec2 uv = { 0, 0 }; // Texture Coords

			glm::vec2 tSize  = { 0, 0 }; // Size of char on texture
			glm::vec2 size   = { 0, 0 }; // Size of char in screen coords

			// x: Offset from current position to left side of char
			// y: Offset from top of line to top of char
			glm::vec2 offset = { 0, 0 };

			float xAdvance = 0; // How far to move right to the next char

			uint32_t kerningCount  = 0;
			uint32_t kerningOffset = 0;
		};

		struct KerningInfo
		{
			char firstId  = '\0';
			char secondId = '\0';
			float amount      = 0;
		};

		class Font
		{
			public:
			std::string name;

			int size;

			float lineHeight;
			float baseHeight;

			glm::ivec2 textureSize;

			//int numberOfCharactures;

			std::vector< CharInfo > charList;

			std::vector< KerningInfo > kerningInfo;

			float leftPadding;
			float rightPadding;
			float topPadding;
			float bottomPadding;

			float widthSpace;
			float widthTab;

			ID3D11ShaderResourceViewSPtr charTextureArray;

			const CharInfo* getChar( const char characture ) const
			{
				for( const CharInfo& info: charList )
				{
					if( info.characture == characture )
					{
						return &info;
					}
				}

				return nullptr;
			}

			float getKerning( const char last, const CharInfo* current ) const
			{
				if( current->kerningCount > 0 )
				{
					for( const KerningInfo& kerning: kerningInfo )
					{
						if( kerning.firstId == last && kerning.secondId == current->characture )
						{
							return kerning.amount;
						}
					}
				}

				return 0.0f;
			}
		};
} // namespace fx
} // namespace visual

#endif // _FONT_DX11_H
