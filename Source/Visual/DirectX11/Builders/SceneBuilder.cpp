#include "SceneBuilder.h"
// Interface for building a scene and that will
// then insert the scene into the system.
//
// Project   : NaKama-Tools
// File Name : SceneBuilder.cpp
// Date      : 07/03/2018
// Author    : Nicholas Welters

#include "Visual/DirectX11/Factories/MeshFactory.h"
#include "Visual/DirectX11/Factories/ImageFactory.h"
#include "Visual/DirectX11/Factories/BufferFactory.h"
#include "Visual/DirectX11/Factories/StateFactory.h"
#include "Visual/DirectX11/Factories/FontFactory.h"
#include "Visual/DirectX11/Types/ParticleBuffer.h"

#include "Visual/DirectX11/Materials/ColourMaterialBuffer.h"
#include "Visual/DirectX11/Materials/FontMaterialBuffer.h"
#include "Visual/DirectX11/Materials/DepthMaterialBuffer.h"

#include "../Types/SceneDataSet.h"
#include "../Types/Model.h"
#include "../Types/Text.h"

#include "Visual/FX/Shapes.h"

namespace visual
{
namespace directX11
{
	using std::vector;
	using std::string;
	using std::make_shared;
	using std::to_string;

	using glm::vec2;
	using glm::vec3;
	using glm::ivec2;

	using fx::PSFontBuffer;
	using fx::PSObjectBuffer;
	using fx::GSParticleBuffer;
	using fx::PSParticleBuffer;
	using fx::Mesh;
	using fx::Vertex3f2f3f3f3f;
	using fx::VertexChar;
	using fx::VertexParticle;
	using fx::Blend;
	using fx::LightEx;

	SHARED_PTR_TYPE_DEF( Text );

	/// <summary> ctor </summary>
	SceneBuilder::SceneBuilder( const TransferFunction&  transfer,
								const StateFactorySPtr&  pStateFactory,
								const BufferFactorySPtr& pBufferFactory,
								const MeshFactorySPtr&   pMeshFactory,
								const ImageFactorySPtr&  pImageFactory,
								const FontFactorySPtr&   pFontFactory )
		: fx::SceneBuilder( )
		, transfer( transfer )
		, pPayload( make_shared< SceneDataSet >( ) )
		, pBufferFactory( pBufferFactory )
		, pMeshFactory( pMeshFactory )
		, pImageFactory( pImageFactory )
		, pStateFactory( pStateFactory )
		, pFontFactory( pFontFactory )
	{ }

	/// <summary> add a sphere to the new scene. </summary>
	/// <param name="position"> the position of the sphere. </param>
	/// <param name="radius"> The radius of the sphere. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addSphere(
		const vec3& position,
		const float& radius,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;
		vector< ID3D11ShaderResourceViewSPtr > psTextures;

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "Sphere" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "Sphere" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		pPayload->spheres.push_back( make_shared< Model >(
			pMeshFactory->createPointMesh( ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			)
		);

		pPayload->spheres.back( )->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pPayload->spheres.back( )->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pPayload->spheres.back( )->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );

		pPayload->spheres.back( )->getColourMaterial( )->setAbsorption( psProperties.absorption );
		pPayload->spheres.back( )->getColourMaterial( )->setRoughnessScale( psProperties.roughness );
		pPayload->spheres.back( )->getColourMaterial( )->setMetalnessScale( psProperties.metalness );
		pPayload->spheres.back( )->getColourMaterial( )->setSAlpha( psProperties.sAlpha );
		pPayload->spheres.back( )->getColourMaterial( )->setSsAlpha( psProperties.ssAlpha );
		pPayload->spheres.back( )->getColourMaterial( )->setSsPower( psProperties.ssPower );
		pPayload->spheres.back( )->getColourMaterial( )->setSsScale( psProperties.ssScale );
		pPayload->spheres.back( )->getColourMaterial( )->setIor( psProperties.ior );
		pPayload->spheres.back( )->getColourMaterial( )->setSsBump( psProperties.ssBump );

		pPayload->spheres.back( )->setPosition( position );
		pPayload->spheres.back( )->setScale( { radius * 2, radius * 2, 1 } );
		pPayload->spheres.back( )->update( );
	}

	/// <summary> add a sphere to the new scene. </summary>
	/// <param name="position"> the position of the sphere. </param>
	/// <param name="radius"> The radius of the sphere. </param>
	/// <param name="textures"> The textures for the sphere. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	/// <param name="forceSRGB"> force the albido texture to be gamma corrected. </param>
	void SceneBuilder::addPbrSphere(
		const vec3& position,
		const float& radius,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties,
		const bool& forceSRGB )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;


		int i = 0;

		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName, forceSRGB && i == 1 ) );
			i++;
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "PBRSphere" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "PBRSphere" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		ModelSPtr pSphere = make_shared< Model >(
			pMeshFactory->createPointMesh( ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) ) );

		pSphere->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pSphere->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pSphere->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );
		pSphere->getColourMaterial( )->setBumpScale( psProperties.bumpScale );

		pSphere->setPosition( position );
		pSphere->setScale( { radius * 2, radius * 2, 1 } );
		pSphere->update( );

		pPayload->pbrSpheres.push_back( pSphere );
	}

	/// <summary> add a planet to the new scene. </summary>
	/// <param name="position"> the position of the sphere. </param>
	/// <param name="radius"> The radius of the sphere. </param>
	/// <param name="textures"> The textures to load and use for the planets surface. </param>
	void SceneBuilder::addPlanet( const vec3& position, const float& radius, const vector< string >& textures )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "planet" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "planet" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		pPayload->planets.push_back( make_shared< Model >(
			pMeshFactory->createPointMesh( ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			)
		);

		pPayload->planets.back( )->getColourMaterial( )->setAmbientReflectivity( { 0.1, 0.1, 0.1 } );
		pPayload->planets.back( )->getColourMaterial( )->setDiffuseReflectivity( { 0.8, 0.8, 0.8 } );
		pPayload->planets.back( )->getColourMaterial( )->setSpecularReflectivity( { 2.0, 2.0, 2.0 } );
		//pPayload->planets.back( )->getColourMaterial( )->setSpecularPower( 20 );

		pPayload->planets.back( )->setPosition( position );
		pPayload->planets.back( )->setScale( { radius * 2, radius * 2, 1 } );
		pPayload->planets.back( )->update( );
	}

	/// <summary> add a moon to the new scene. </summary>
	/// <param name="position"> the position of the sphere. </param>
	/// <param name="radius"> The radius of the sphere. </param>
	/// <param name="textures"> The textures to load and use for the planets surface. </param>
	void SceneBuilder::addMoon( const vec3& position, const float& radius, const vector< string >& textures )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "Moon" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "Moon" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		pPayload->moons.push_back( make_shared< Model >(
			pMeshFactory->createPointMesh( ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			)
		);

		pPayload->moons.back( )->getColourMaterial( )->setAmbientReflectivity( { 0.1, 0.1, 0.1 } );
		pPayload->moons.back( )->getColourMaterial( )->setDiffuseReflectivity( { 0.7, 0.7, 0.7 } );
		pPayload->moons.back( )->getColourMaterial( )->setSpecularReflectivity( { 0.3, 0.3, 0.3 } );
		//pPayload->moons.back( )->getColourMaterial( )->setSpecularPower( 8 );

		pPayload->moons.back( )->setPosition( position );
		pPayload->moons.back( )->setScale( { radius * 2, radius * 2, 1 } );
		pPayload->moons.back( )->update( );
	}

	/// <summary> add a height map to the new scene. </summary>
	/// <param name="fileName"> The file name of the raw image. </param>
	/// <param name="position"> The position of the height map. </param>
	/// <param name="size"> The size of the raw image. </param>
	/// <param name="textures"> The textures for the 3 layers. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addHeightMap(
		const string& fileName,
		const vec3& position,
		const vec3& scale,
		const ivec2& size,
		const Textures& textures,
		const PSObjectBuffer& psProperties )
	{
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const LoadTexture& texture : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( texture.fileName, texture.forceSRGB ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pGroundVSBuffer = pBufferFactory->createConstantBuffer( fileName + "_GROUND_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pGroundPSBuffer = pBufferFactory->createConstantBuffer( fileName + "_GROUND_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		ColourMaterialBufferSPtr pGroundMaterial = make_shared< ColourMaterialBuffer >(
			make_shared< BasicVSBuffer >( 1, pGroundVSBuffer ),
			make_shared< BasicPSBuffer >( 1, pGroundPSBuffer ),
			make_shared< PSResources >( psTextures ) );

		pGroundMaterial->setAmbientReflectivity( psProperties.ka );
		pGroundMaterial->setDiffuseReflectivity( psProperties.kd );
		pGroundMaterial->setSpecularReflectivity( psProperties.ks );
		//pGroundMaterial->setSpecularPower( ns );

		MeshBufferSPtr pMesh = pMeshFactory->createHeightMap( fileName, { size.x, size.y } );
		//MeshBufferSPtr pMesh = pFactory->getHeightMap("Data/WorldMap/E3.raw");
		ModelSPtr pHeightMap = make_shared< Model >(
			pMesh,
			pGroundMaterial,
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pGroundVSBuffer ) )
			);

		pHeightMap->setPosition( position );
		pHeightMap->setScale( scale );
		//pHeightMap->setScale( { 1, 1, 1 } );
		pHeightMap->update( );

		pPayload->heightMaps.push_back( pHeightMap );
	}

	/// <summary> add a randomly generated height map to the new scene. </summary>
	/// <param name="fileName"> The file name of the raw image. </param>
	/// <param name="size"> The size of the raw image. </param>
	/// <param name="textures"> The textures for the 3 layers. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addPCGHeightMap(
		const vec3& scale,
		const ivec2& size,
		const Textures& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;
		const string id = to_string( tempUniqueID );

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const LoadTexture& texture : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( texture.fileName, texture.forceSRGB ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		const ID3D11BufferSPtr pGroundVSBuffer = pBufferFactory->createConstantBuffer( id + "_PCG_GROUND_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		const ID3D11BufferSPtr pGroundPSBuffer = pBufferFactory->createConstantBuffer( id + "_PCG_GROUND_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		ColourMaterialBufferSPtr pGroundMaterial = make_shared< ColourMaterialBuffer >(
			make_shared< BasicVSBuffer >( 1, pGroundVSBuffer ),
			make_shared< BasicPSBuffer >( 1, pGroundPSBuffer ),
			make_shared< PSResources >( psTextures ) );

		pGroundMaterial->setAmbientReflectivity( psProperties.ka );
		pGroundMaterial->setDiffuseReflectivity( psProperties.kd );
		pGroundMaterial->setSpecularReflectivity( psProperties.ks );
		//pGroundMaterial->setSpecularPower( ns );

		const Mesh< Vertex3f2f3f3f3f::Vertex > mesh = fx::Shapes::grid( size.x, 1.0f, -1.0 );
		const MeshBufferSPtr pMesh = pMeshFactory->createMeshBuffer< Vertex3f2f3f3f3f >( "PCG_HEIGHT_MESH" + id, mesh );

		ModelSPtr pHeightMap = make_shared< Model >(
			pMesh,
			pGroundMaterial,
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pGroundVSBuffer ) )
			);

		pHeightMap->setPosition( { 0, 0, 0 } );
		pHeightMap->setScale( scale );
		//pHeightMap->setScale( { 1, 1, 1 } );
		pHeightMap->update( );

		pPayload->pcgHeightMaps.push_back( pHeightMap );
	}

	/// <summary> add a geometry shader cube to the new scene. </summary>
	/// <param name="position"> the position of the sphere. </param>
	/// <param name="rotation"> The rotation of the cube. </param>
	/// <param name="scale"> The size of the cube. </param>
	/// <param name="textures"> The textures to load and use for the cube surface. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addGsCube(
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "MAP_GS_CUBE_" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "MAP_GS_CUBE_" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		pPayload->cubes.push_back( make_shared< Model >(
			pMeshFactory->createPointMesh( ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			)
		);

		pPayload->cubes.back( )->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pPayload->cubes.back( )->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pPayload->cubes.back( )->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );
		//pPayload->cubes.back( )->getColourMaterial( )->setSpecularPower( ns );
		pPayload->cubes.back( )->getColourMaterial( )->setBumpScale( psProperties.bumpScale );

		pPayload->cubes.back( )->setPosition( position );
		pPayload->cubes.back( )->setRotation( rotation );
		pPayload->cubes.back( )->setScale( scale );
		pPayload->cubes.back( )->update( );
	}

	/// <summary> add a cube to the new scene. </summary>
	/// <param name="position"> the position of the sphere. </param>
	/// <param name="rotation"> The rotation of the cube. </param>
	/// <param name="scale"> The size of the cube. </param>
	/// <param name="textures"> The textures to load and use for the cube surface. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addCube(
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "MAP_CUBE_" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "MAP_CUBE_" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		pPayload->tiles.push_back( make_shared< Model >(
			pMeshFactory->createSlabMesh( ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			)
			);

		pPayload->tiles.back( )->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pPayload->tiles.back( )->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pPayload->tiles.back( )->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );
		//pPayload->tiles.back( )->getColourMaterial( )->setSpecularPower( ns );

		pPayload->tiles.back( )->setPosition( position );
		pPayload->tiles.back( )->setRotation( rotation );
		pPayload->tiles.back( )->setScale( scale );
		pPayload->tiles.back( )->update( );
	}

	/// <summary> add a complex custom mesh to the new scene. </summary>
	/// <param name="id"> The mesh id. </param>
	/// <param name="mesh"> the position of the sphere. </param>
	/// <param name="position"> The mesh position. </param>
	/// <param name="rotation"> The mesh rotation. </param>
	/// <param name="scale"> The mesh scale. </param>
	/// <param name="textures"> The textures to load and use. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addMesh(
		const string& id,
		const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( id + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( id + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		pPayload->tiles.push_back( make_shared< Model >(
			pMeshFactory->createMeshBuffer< Vertex3f2f3f3f3f >( id, mesh ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			)
		);

		pPayload->tiles.back( )->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pPayload->tiles.back( )->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pPayload->tiles.back( )->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );
		//pPayload->tiles.back( )->getColourMaterial( )->setSpecularPower( ns );

		pPayload->tiles.back( )->setPosition( position );
		pPayload->tiles.back( )->setRotation( rotation );
		pPayload->tiles.back( )->setScale( scale );
		pPayload->tiles.back( )->update( );
	}

	/// <summary> add a complex custom mesh to the new scene that uses basic textures. </summary>
	/// <param name="id"> The mesh id. </param>
	/// <param name="mesh"> the position of the sphere. </param>
	/// <param name="position"> The mesh position. </param>
	/// <param name="rotation"> The mesh rotation. </param>
	/// <param name="scale"> The mesh scale. </param>
	/// <param name="textures"> The textures to load and use. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addBasicMesh(
		const string& id,
		const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "BASIC_" + id + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "BASIC_" + id + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		ModelSPtr pModel = make_shared< Model >(
			pMeshFactory->createMeshBuffer< Vertex3f2f3f3f3f >( id, mesh ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			);

		pModel->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pModel->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pModel->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );

		pModel->getColourMaterial( )->setAbsorption( psProperties.absorption );
		pModel->getColourMaterial( )->setRoughnessScale( psProperties.roughness );
		pModel->getColourMaterial( )->setMetalnessScale( psProperties.metalness );
		pModel->getColourMaterial( )->setSAlpha( psProperties.sAlpha );
		pModel->getColourMaterial( )->setSsAlpha( psProperties.ssAlpha );
		pModel->getColourMaterial( )->setSsPower( psProperties.ssPower );
		pModel->getColourMaterial( )->setSsScale( psProperties.ssScale );
		pModel->getColourMaterial( )->setIor( psProperties.ior );
		pModel->getColourMaterial( )->setSsBump( psProperties.ssBump );
		pModel->getColourMaterial( )->setBumpScale( psProperties.bumpScale );

		pModel->setPosition( position );
		pModel->setRotation( rotation );
		pModel->setScale( scale );
		pModel->update( );

		pPayload->basicObjs.push_back( pModel );
	}

	/// <summary> add a complex custom mesh to the new scene that uses basic textures. </summary>
	/// <param name="id"> The mesh id. </param>
	/// <param name="mesh"> the position of the sphere. </param>
	/// <param name="position"> The mesh position. </param>
	/// <param name="rotation"> The mesh rotation. </param>
	/// <param name="scale"> The mesh scale. </param>
	/// <param name="textures"> The textures to load and use. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addClippedMesh(
		const string& id,
		const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "CLIPPED_" + id + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "CLIPPED_" + id + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		ModelSPtr pModel = make_shared< Model >(
			pMeshFactory->createMeshBuffer< Vertex3f2f3f3f3f >( id, mesh ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			);

		pModel->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pModel->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pModel->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );

		pModel->getColourMaterial( )->setAbsorption( psProperties.absorption );
		pModel->getColourMaterial( )->setRoughnessScale( psProperties.roughness );
		pModel->getColourMaterial( )->setMetalnessScale( psProperties.metalness );
		pModel->getColourMaterial( )->setSAlpha( psProperties.sAlpha );
		pModel->getColourMaterial( )->setSsAlpha( psProperties.ssAlpha );
		pModel->getColourMaterial( )->setSsPower( psProperties.ssPower );
		pModel->getColourMaterial( )->setSsScale( psProperties.ssScale );
		pModel->getColourMaterial( )->setIor( psProperties.ior );
		pModel->getColourMaterial( )->setSsBump( psProperties.ssBump );
		pModel->getColourMaterial( )->setBumpScale( psProperties.bumpScale );

		pModel->setPosition( position );
		pModel->setRotation( rotation );
		pModel->setScale( scale );
		pModel->update( );

		pPayload->clippedObjs.push_back( pModel );
	}

	/// <summary> add a complex custom translucent mesh to the new scene. </summary>
	/// <param name="id"> The mesh id. </param>
	/// <param name="mesh"> the position of the sphere. </param>
	/// <param name="position"> The mesh position. </param>
	/// <param name="rotation"> The mesh rotation. </param>
	/// <param name="scale"> The mesh scale. </param>
	/// <param name="textures"> The textures to load and use. </param>
	/// <param name="psProperties"> The objects material properties. </param>
	void SceneBuilder::addTranslucentMesh(
		const string& id,
		const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( id + "Trans" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( id + "Trans" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );

		pPayload->translucent.push_back( make_shared< Model >(
			pMeshFactory->createMeshBuffer< Vertex3f2f3f3f3f >( id, mesh ),
			make_shared< ColourMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< BasicPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( psTextures ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			)
			);

		pPayload->translucent.back( )->getColourMaterial( )->setAmbientReflectivity( psProperties.ka );
		pPayload->translucent.back( )->getColourMaterial( )->setDiffuseReflectivity( psProperties.kd );
		pPayload->translucent.back( )->getColourMaterial( )->setSpecularReflectivity( psProperties.ks );

		pPayload->translucent.back( )->getColourMaterial( )->setAbsorption( psProperties.absorption );
		pPayload->translucent.back( )->getColourMaterial( )->setRoughnessScale( psProperties.roughness );
		pPayload->translucent.back( )->getColourMaterial( )->setMetalnessScale( psProperties.metalness );
		pPayload->translucent.back( )->getColourMaterial( )->setSAlpha( psProperties.sAlpha );
		pPayload->translucent.back( )->getColourMaterial( )->setSsAlpha( psProperties.ssAlpha );
		pPayload->translucent.back( )->getColourMaterial( )->setSsPower( psProperties.ssPower );
		pPayload->translucent.back( )->getColourMaterial( )->setSsScale( psProperties.ssScale );
		pPayload->translucent.back( )->getColourMaterial( )->setIor( psProperties.ior );
		pPayload->translucent.back( )->getColourMaterial( )->setSsBump( psProperties.ssBump );
		pPayload->translucent.back( )->getColourMaterial( )->setBumpScale( psProperties.bumpScale );

		pPayload->translucent.back( )->setPosition( position );
		pPayload->translucent.back( )->setRotation( rotation );
		pPayload->translucent.back( )->setScale( scale );
		pPayload->translucent.back( )->update( );
	}



	/// <summary> add a water plane to the new scene. </summary>
	/// <param name="position"> the position of the water plane. </param>
	/// <param name="rotation"> The rotation of the water plane. </param>
	/// <param name="scale"> The size of the water plane. </param>
	/// <param name="textures"> The textures to load and use for the water plane surface. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addWaterPlane(
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pWaterVSBuffer = pBufferFactory->createConstantBuffer( "WATER" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pWaterPSBuffer = pBufferFactory->createConstantBuffer( "WATER" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );


		ColourMaterialBufferSPtr pWaterMaterial = make_shared< ColourMaterialBuffer >(
			make_shared< BasicVSBuffer >( 1, pWaterVSBuffer ),
			make_shared< BasicPSBuffer >( 1, pWaterPSBuffer ),
			make_shared< PSResources >( psTextures ) );

		pWaterMaterial->setAmbientReflectivity( psProperties.ka );
		pWaterMaterial->setDiffuseReflectivity( psProperties.kd );
		pWaterMaterial->setSpecularReflectivity( psProperties.ks );
		//pWaterMaterial->setSpecularPower( ns );


		ModelSPtr pWaterPlane = make_shared< Model >(
			pMeshFactory->createPlane( ),
			pWaterMaterial,
			nullptr
		);

		pWaterPlane->setPosition( position );
		pWaterPlane->setRotation( rotation );
		pWaterPlane->setScale( scale );
		pWaterPlane->update( );

		pPayload->water.push_back( pWaterPlane );
	}

	/// <summary> add a water plane that follows the camera to the new scene. </summary>
	/// <param name="position"> the position of the water plane. </param>
	/// <param name="rotation"> The rotation of the water plane. </param>
	/// <param name="scale"> The size of the water plane. </param>
	/// <param name="textures"> The textures to load and use for the water plane surface. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	/// <param name="follow"> Set the plane to follow the camera. </param>
	void SceneBuilder::addPCGWaterPlane(
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSObjectBuffer& psProperties,
		bool follow )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		ID3D11BufferSPtr pWaterVSBuffer = pBufferFactory->createConstantBuffer( "PCG_WATER" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pWaterPSBuffer = pBufferFactory->createConstantBuffer( "PCG_WATER" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( BasicPSBuffer::Constants ) );


		ColourMaterialBufferSPtr pWaterMaterial = make_shared< ColourMaterialBuffer >(
			make_shared< BasicVSBuffer >( 1, pWaterVSBuffer ),
			make_shared< BasicPSBuffer >( 1, pWaterPSBuffer ),
			make_shared< PSResources >( psTextures ) );

		pWaterMaterial->setAmbientReflectivity( psProperties.ka );
		pWaterMaterial->setDiffuseReflectivity( psProperties.kd );
		pWaterMaterial->setSpecularReflectivity( psProperties.ks );
		//pWaterMaterial->setSpecularPower( ns );


		ModelSPtr pWaterPlane = make_shared< Model >(
			pMeshFactory->createPlane( ),
			pWaterMaterial,
			nullptr
		);

		pWaterPlane->setPosition( position );
		pWaterPlane->setRotation( rotation );
		pWaterPlane->setScale( scale );
		pWaterPlane->update( );

		if( follow )
		{
			pPayload->pcgWaterFollower.push_back( pWaterPlane );
		}
		else
		{
			pPayload->pcgWater.push_back( pWaterPlane );
		}
	}


	/// <summary> Set the sky box for the scene to use a single polar mapped texture for the sky. </summary>
	/// <param name="textures"> The sky texture. </param>
	/// <param name="direction"> The sun direction in the sky. </param>
	void SceneBuilder::addPolarSkybox(
		const vector< string >& textures,
		const vec3& direction )
	{
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		pPayload->enablePolarSky = true;
		pPayload->enableCubeSky = pPayload->enableBasicVolumeSky = false;
		pPayload->polarSky = make_shared< PSResources >( psTextures );
	}

	/// <summary> Set the sky box for the scene to use a cube mapped texture for the sky. </summary>
	/// <param name="textures"> The sky texture. </param>
	/// <param name="direction"> The sun direction in the sky. </param>
	void SceneBuilder::addCubeSkybox(
		const vector< string >& textures,
		const vec3& direction )
	{
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		pPayload->enableCubeSky = true;
		pPayload->enablePolarSky = pPayload->enableBasicVolumeSky = false;
		pPayload->cubeSky = make_shared< PSResources >( psTextures );
	}

	/// <summary> Set the sky box for the scene to use a cube mapped texture for the sky. </summary>
	/// <param name="textures"> The stars texture. </param>
	/// <param name="direction"> The starting sun direction. </param>
	void SceneBuilder::addBasicVolumeSkybox(
		const vector< string >& textures,
		const vec3& direction )
	{
		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		pPayload->enableBasicVolumeSky = true;
		pPayload->enablePolarSky = pPayload->enableCubeSky = false;
		pPayload->basicVolumeSky = make_shared< PSResources >( psTextures );
		pPayload->sunDirection = direction;
	}




	/// <summary> Add a particle system to the scene. </summary>
	/// <param name="initialState"> The initial state of the particle system. </param>
	/// <param name="textures"> The particles texture. </param>
	/// <param name="gsProperties"> The geometry shader constants. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	/// <param name="bufferSize"> The size of the particle buffer. </param>
	/// <param name="blend"> The blend used for the particle system. </param>
	void SceneBuilder::addParticleSystem(
		const Mesh< VertexParticle::Vertex >::VertexBuffer& initialState,
		const vector< string >& textures,
		const GSParticleBuffer& gsProperties,
		const PSParticleBuffer& psProperties,
		const unsigned int bufferSize,
		const string& blend )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		// Add a fire particle system to the scene.
		ParticleGSBufferSPtr pGSBuffer =
			make_shared< ParticleGSBuffer >(
				1, pBufferFactory->createConstantBuffer( "PARTICLE_" + to_string( tempUniqueID ) + "_CONSTANTS_GS", sizeof( ParticleGSBuffer::Constants ) ) );


		ParticlePSBufferSPtr pPSBuffer =
			make_shared< ParticlePSBuffer >(
				1, pBufferFactory->createConstantBuffer( "PARTICLE_" + to_string( tempUniqueID ) + "_CONSTANTS_PS", sizeof( ParticlePSBuffer::Constants ) ) );

		pGSBuffer->get( ) = gsProperties;
		pPSBuffer->get( ) = psProperties;

		const Blend blendType = blend == "override" ? fx::WRITE_OVER : fx::ADD_BLEND;

		ParticleBufferSPtr buffer = make_shared< ParticleBuffer >(
			static_cast< int >( initialState.size( ) ),	  // The number of init particles
			pMeshFactory->getInitParticleBuffer( "PARTICLE_" + to_string( tempUniqueID ) + "_INIT_BUFFER", initialState ),
			pMeshFactory->getParticleBuffer( "PARTICLE_" + to_string( tempUniqueID ) + "_BUFFER_A", bufferSize ),
			pMeshFactory->getParticleBuffer( "PARTICLE_" + to_string( tempUniqueID ) + "_BUFFER_B", bufferSize ),
			pGSBuffer,
			pPSBuffer,
			psTextures[ 0 ],
			pStateFactory->createBlendState( blendType )
		);

		pPayload->particles.emplace_back( buffer );
	}

	/// <summary> add a 3D text the new scene. </summary>
	/// <param name="font"> the font type to use for the string. </param>
	/// <param name="text"> the string contents. </param>
	/// <param name="position"> the position of the text. </param>
	/// <param name="rotation"> The rotation of the text. </param>
	/// <param name="scale"> The size of the text. </param>
	/// <param name="textures"> The textures to load and use for the text. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addText3D(
		const std::string& font,
		const std::string& text,
		const vec3& position,
		const vec3& rotation,
		const vec3& scale,
		const vector< string >& textures,
		const PSFontBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		FontSPtr pFont = pFontFactory->createFont( font );
		if( !pFont )
		{
			pFont = pFontFactory->createFont( R"(C:\Windows\Fonts\arialbd.ttf)" );
		}

		vector< ID3D11ShaderResourceViewSPtr > fontTextures = loadTextures( textures );

		vector< ID3D11ShaderResourceViewSPtr > psTextures = { pFont->charTextureArray };

		for( const ID3D11ShaderResourceViewSPtr& pTexture: fontTextures )
		{
			psTextures.push_back( pTexture );
		}

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "3D_TEXT_" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );

		FontMaterialBufferSPtr pMaterial = make_shared< FontMaterialBuffer >(
			make_shared< BasicVSBuffer >( 1, pVSBuffer ),
			pBufferFactory->createConstantBuffer< FontPSBuffer >( "3D_TEXT_" + to_string( tempUniqueID ) + "_MATERIAL_PS", 1 ),
			make_shared< PSResources >( psTextures ) );

		pMaterial->setAmbientReflectivity( psProperties.ka );
		pMaterial->setDiffuseReflectivity( psProperties.kd );
		pMaterial->setSpecularReflectivity( psProperties.ks );
		pMaterial->setProperties( psProperties.properties );

		TextSPtr pText = make_shared< Text >(
			text,
			pFont,
			pMeshFactory->createDynamicVertexBuffer< VertexChar::Vertex >( "3D Text Buffer " + to_string( tempUniqueID ), static_cast< uint32_t >( text.size( ) ) ),
			pMaterial,
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
		);

		pText->setPosition( position );
		pText->setRotation( rotation );
		pText->setScale( scale );
		pText->update( );

		pPayload->text3D.push_back( pText );
	}

	/// <summary> add a 2D text the new scene. </summary>
	/// <param name="font"> the font type to use for the string. </param>
	/// <param name="text"> the string contents. </param>
	/// <param name="position"> the position of the text. </param>
	/// <param name="rotation"> The rotation of the text. </param>
	/// <param name="scale"> The size of the text. </param>
	/// <param name="textures"> The textures to load and use for the text. </param>
	/// <param name="psProperties"> The pixel shader constants. </param>
	void SceneBuilder::addText2D(
		const std::string& font,
		const std::string& text,
		const vec2& position,
		const vec2& rotation,
		const vec2& scale,
		const vector< string >& textures,
		const PSFontBuffer& psProperties )
	{
		static int tempUniqueID = 0;
		tempUniqueID++;

		FontSPtr pFont = pFontFactory->createFont( font );
		if( !pFont )
		{
			pFont = pFontFactory->createFont( R"(C:\Windows\Fonts\arialbd.ttf)" );
		}

		vector< ID3D11ShaderResourceViewSPtr > fontTextures = loadTextures( textures );

		vector< ID3D11ShaderResourceViewSPtr > psTextures = { pFont->charTextureArray };

		for( const ID3D11ShaderResourceViewSPtr& pTexture: fontTextures )
		{
			psTextures.push_back( pTexture );
		}



		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "TEXT_" + to_string( tempUniqueID ) + "_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "TEXT_" + to_string( tempUniqueID ) + "_MATERIAL_PS", sizeof( FontPSBuffer::Constants ) );


		FontMaterialBufferSPtr pMaterial = make_shared< FontMaterialBuffer >(
			make_shared< BasicVSBuffer >( 1, pVSBuffer ),
			make_shared< FontPSBuffer >( 1, pPSBuffer ),
			make_shared< PSResources >( psTextures ) );

		pMaterial->setAmbientReflectivity( psProperties.ka );
		pMaterial->setDiffuseReflectivity( psProperties.kd );
		pMaterial->setSpecularReflectivity( psProperties.ks );
		pMaterial->setProperties( psProperties.properties );

		TextSPtr pText = make_shared< Text >(
			text,
			pFont,
			pMeshFactory->createDynamicVertexBuffer< VertexChar::Vertex >( "Text Buffer " + to_string( tempUniqueID ), static_cast< uint32_t >( text.size( ) ) ),
			pMaterial,
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
		);

		pText->setPosition( { position.x, position.y, 0 } );
		pText->setRotation( rotation );
		pText->setScale( { scale.x, scale.y, 1 } );
		pText->update( );

		pPayload->text2D.push_back( pText );
	}

	/// <summary> Set the worlds fog interactions. </summary>
	/// <param name="density"> The fog density. </param>
	/// <param name="heightFalloff"> The scenes fog height fall off rate. </param>
	/// <param name="gScatter"> The fog reflectivity direction -1 to 1 with 0 being isotropic. </param>
	/// <param name="waterHeight"> The scenes water height so that we can change the fog. </param>
	void SceneBuilder::addFog(
		float density,
		float heightFalloff,
		float rayDepth,
		float gScatter,
		float waterHeight )
	{
		pPayload->density = density;
		pPayload->heightFalloff = heightFalloff;
		pPayload->rayDepth = rayDepth;
		pPayload->gScatter = gScatter;
		pPayload->waterHeight = waterHeight;
	}

	/// <summary>
	/// Add a light to the scene.
	///
	/// Type 0: A point light
	/// Type 1: A spot light
	/// Type 2: A direction light
	///
	/// The light structure is reused between lights... for now
	/// </summary>
	/// <param name="light"> The light to add to the scene. </param>
	void SceneBuilder::addLight( const LightEx& light )
	{
		pPayload->cpuLights.emplace_back( light );
	}

	/// <summary>
	/// Finalise the new scene object and
	/// had it off to the scene renderer.
	/// </summary>
	void SceneBuilder::bake( )
	{
		static int lightBufferCounter = 0;

		if( !pPayload->cpuLights.empty( ) )
		{
			pPayload->gpuLights = pImageFactory->createUpdateableStructuredBuffer(
				"LIGHTS_" + to_string( ++lightBufferCounter ),
				static_cast<uint32_t>( pPayload->cpuLights.size( ) ),
				sizeof( LightEx ),
				pPayload->cpuLights.data( )
			);
			pPayload->hasLights = true;
		}


		transfer( pPayload );
		pPayload = make_shared< SceneDataSet >( );
	}


	/// <summary> Loads the textures needed. </summary>
	/// <param name="textures"> The texture file names to load. </param>
	/// <returns> The textures ready to use. </returns>
	vector< ID3D11ShaderResourceViewSPtr > SceneBuilder::loadTextures( const vector< string >& textures ) const
	{
		// TODO: Fix the deadlock that we can have here.
		// Say the system is loading an application but we shut down the renderer... The promise is never completed

		vector< ID3D11ShaderResourceViewSPtr > psTextures;
		vector< ID3D11ShaderResourceViewPromiseSPtr > psTexturePromises;
		for( const string& fileName : textures )
		{
			psTexturePromises.push_back( pImageFactory->createSrv( fileName ) );
		}

		for( const ID3D11ShaderResourceViewPromiseSPtr& pPromise : psTexturePromises )
		{
			psTextures.push_back( pPromise->get_future( ).get( ) );
		}

		return psTextures;
	}
} // namespace fx
} // namespace visual