#include "PipelineBuilder.h"

// DirectX 11 Pipeline factory.
//
// Build the rendering pipeline that will be used to create our final frame.
//
// Project   : NaKama-Fx
// File Name : PipelineBuilder.h
// Date      : 15/04/2019
// Author    : Nicholas Welters


#include "Visual/FX/Settings.h"
#include "Visual/FX/Enums.h"
#include "Visual/FX/Vertex.h"
#include "Visual/FX/Types/Frustum.h"

#include "../Passes/RenderingPass.h"
#include "../Targets/MultipleRenderTarget.h"
#include "../Targets/DepthStencilDeferredTarget.h"
#include "../Targets/UATarget.h"
#include "../Factories/MeshFactory.h"
#include "../Factories/BufferFactory.h"
#include "../Factories/ShaderFactory.h"
#include "../Factories/ImageFactory.h"
#include "../Factories/TargetFactory.h"
#include "../Factories/StateFactory.h"
#include "../Factories/FontFactory.h"

#include "../Materials/MaterialBuffer.h"

#include "../Passes/ComputeHistogramPass.h"
#include "../Passes/PostFxPass.h"
#include "../Passes/ABufferOITPass.h"
#include "../Passes/SSAOPass.h"
#include "../Passes/SSRPass.h"
#include "../Passes/DepthMeshSubPass.h"
#include "../Passes/DepthGeometryMeshSubPass.h"
#include "../Passes/ColourMeshSubPass.h"
#include "../Passes/GeometryMeshSubPass.h"
#include "../Passes/SkySubPass.h"
#include "../Passes/FullScreenSubPass.h"
#include "../Passes/VolumetricLightSubPass.h"
#include "../Passes/ParticleSubPass.h"
#include "../Passes/PostFXSubPass.h"
#include "../Passes/TextSubPass.h"
#include "../Passes/ToneMappingPass.h"
#include "../Passes/UIPass.h"

#include "../Types/Model.h"
#include "../Types/Font.h"
#include "../Types/Text.h"
#include "../Materials/ColourMaterialBuffer.h"
#include "../Materials/FontMaterialBuffer.h"
#include "../Materials/DepthMaterialBuffer.h"
#include "../Materials/ToneMappingMaterialBuffer.h"

#include "Visual/UserInterface/SystemUI.h"

#include "resource.h"

#include <msdfgen.h>
#include <ext/import-font.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <vector>
#include <limits>

namespace visual
{
namespace directX11
{
	using fx::Vertex3f2f;
	using fx::Vertex3f2f3f3f3f;
	using fx::VertexParticle;
	using fx::VertexChar;
	using fx::Vertex3f2f4f;
	using fx::CameraSPtr;

	using glm::mat4x4;
	using glm::vec4;
	using glm::vec3;
	using glm::vec2;
	using glm::uvec2;
	using glm::uvec3;
	using glm::uvec4;
	using glm::ivec2;
	using glm::normalize;
	using glm::yawPitchRoll;
	using glm::lookAtLH;
	using glm::orthoLH;
	using glm::radians;

	using glm::pi;

	using std::function;
	using std::vector;
	using std::shared_ptr;
	using std::make_shared;
	using std::numeric_limits;
	using std::min;
	using std::max;

#pragma once // Visual stuidio 2019 got that bug again where it fucks up with out this

	/// <summary> ctor </summary>
	/// <param name=""> The state factory. </param>
	/// <param name=""> The state factory. </param>
	/// <param name=""> The shader factory. </param>
	/// <param name=""> The buffer factory. </param>
	/// <param name=""> The mesh factory. </param>
	/// <param name=""> The image factory. </param>
	/// <param name=""> The target factory. </param>
		/// <param name=""> The font factory. </param>
	PipelineBuilder::PipelineBuilder( const  StateFactorySPtr& pStateFactory,
									  const ShaderFactorySPtr& pShaderFactory,
									  const BufferFactorySPtr& pBufferFactory,
									  const   MeshFactorySPtr& pMeshFactory,
									  const  ImageFactorySPtr& pImageFactory,
									  const TargetFactorySPtr& pTargetFactory,
									  const FontFactorySPtr& pFontFactory )
		:  pStateFactory( pStateFactory )
		, pShaderFactory( pShaderFactory )
		, pBufferFactory( pBufferFactory )
		,   pMeshFactory( pMeshFactory )
		,  pImageFactory( pImageFactory )
		, pTargetFactory( pTargetFactory )
		,   pFontFactory( pFontFactory )
	{ }

	PipelineBuilder::PipelineData PipelineBuilder::buildScenePipeline( const OutputTargetSPtr&   pOutputTarget,
																	   const fx::SettingsSPtr&   pSettings,
																	   const DeviceContextPtr_t& pContext,
																	   const ui::SystemUISPtr&  pUI ) const
	{
		const vec2 shadowMap1Size = pSettings->getShadowCascade1Size( );
		const vec2 shadowMap2Size = pSettings->getShadowCascade2Size( );
		const vec2 renderTargetSize = pSettings->getRenderTargetSize( );
		const int msaaLevel = pSettings->getMsaaLevel( ); // TODO: Get MSAA Working On The System...

		PipelineData data;
		AdditionalData aData;

		data.pToggles = make_shared< PipelineData::ToggleData >( );
		data.pToggles->enableDebug = false;
		data.pToggles->enableFog   = true;
		data.pToggles->enableOIT   = true;
		data.pToggles->enableSSAO  = false;
		data.pToggles->enableSSIL  = true;
		data.pToggles->enableSSR   = true;
		data.pToggles->enableDebugNormals = false;
		data.pToggles->enableDebugFPS     = false;
		data.pToggles->enableDebugTimes   = false;

		initDifferedRenderTargets( aData, shadowMap1Size, shadowMap2Size, renderTargetSize, msaaLevel );
		initSamplers( aData );
		initConstantBuffers( data, aData, renderTargetSize );

		aData.defSamplers = make_shared< PSSamplers >( vector< ID3D11SamplerStateSPtr >
		{
			aData.pSamplers[ HiQualityWrap ],
			aData.pSamplers[ Comparison ],
			aData.pSamplers[ HiQualityMirror ],
			aData.pSamplers[ Point ]
		} );

		initShaders( aData );
		initInputLayouts( aData );
		initRasterizerStates( aData );
		initBlendStates( aData );
		initDepthStencilStates( aData );
		initFullScreenGeometry( aData );
		initDefaultTextures( data );

		initLightClusteringPasses( data, aData, pContext, renderTargetSize );

		initOpaqueShadowPass1( data, aData, pContext );
		initOpaqueShadowPass2( data, aData, pContext );
		initTransparentShadowPass1( data, aData, pContext );
		initTransparentShadowPass2( data, aData, pContext );

		initOpaquePass( data, aData, pContext );

		data.pOpaqueCopyPass = createBlitCopyPass( aData.pOpaqueCloneTarget, aData.pMRTarget, data, aData, pContext );

		initTransparentPass( data, aData, pContext );
		initABufferOITPass( data, aData, pContext, renderTargetSize );

		initSkyBoxPass( data, aData, pContext );

		initSSAOPass( data, aData, pContext );
		initSSRPass( data, aData, pContext );
		initVolumetricLightPass( data, aData, pContext );
		initBloomPass( data, aData, pContext );
		initHistogramPass( data, aData, pContext, renderTargetSize, { 32, 16 } );

		initParticlePass( data, aData, pContext );

		initResolveBBPass( data, aData, pContext, pOutputTarget, pUI );

		return data;
	}

	/// <summary> Build the render targets that are going to be used. </summary>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="sm1Size"> The 1st cascade shadow map size. </param>
	/// <param name="sm2Size"> The 2nd cascade shadow map size. </param>
	/// <param name="rtSize"> The render target size. </param>
	/// <param name="msaa"> The msaa sample count. </param>
	void PipelineBuilder::initDifferedRenderTargets( AdditionalData& aData,
													 const uvec2& sm1Size,
													 const uvec2& sm2Size,
													 const uvec2& rtSize,
													 const int msaa ) const
	{
		const int mrt = 4;
		const uint32_t w = static_cast< uint32_t >( rtSize.x );
		const uint32_t h = static_cast< uint32_t >( rtSize.y );

		aData.pShadowTarget1 = pTargetFactory->createShadowMap( "Shadow1", sm1Size.x, sm1Size.y );
		aData.pShadowTarget2 = pTargetFactory->createShadowMap( "Shadow2", sm2Size.x, sm2Size.y );

		aData.pTransparentShadowTarget1 = pTargetFactory->getMultipleRenderTarget( "ShadowT", sm1Size.x, sm1Size.y, 1, 1, fx::RGBA16, true );
		aData.pTransparentShadowTarget2 = pTargetFactory->getMultipleRenderTarget( "Shadow2", sm2Size.x, sm2Size.y, 1, 1, fx::RGBA16, true );

		aData.pMRTarget          = pTargetFactory->getMultipleRenderTarget( "MRT_OpaqueScene"  , rtSize.x , rtSize.y ,  msaa, mrt, fx::RGBA16 );
		aData.pOpaqueCloneTarget = pTargetFactory->getMultipleRenderTarget( "MRT_OpaqueClone"  , rtSize.x , rtSize.y ,  msaa,   1, fx::RGBA16 );
		aData.pVolumeRT          = pTargetFactory->getMultipleRenderTarget( "MRT_OpaqueScene"  , rtSize.x , rtSize.y ,  msaa,   1, fx::RGBA16, false );
		aData.pAORT              = pTargetFactory->getMultipleRenderTarget( "AO TARGET"        , w / 2    , h / 2    ,     1,   1, fx::RGBA16, false );
		aData.pReflectionRT      = pTargetFactory->getMultipleRenderTarget( "REFLECTION TARGET", w / 2    , h / 2    ,     1,   1, fx::RGBA16, false );

		aData.pMipMappedColourRT = pTargetFactory->getMipMappedRenderTarget( "AverageBlur", 1024, 1024, 11, fx::RGBA16 );

		aData.pBlur1A = pTargetFactory->getMultipleRenderTarget( "Blur level 1 A", w /  2, h /  2, 1, 1, fx::RGBA16, false );
		aData.pBlur1B = pTargetFactory->getMultipleRenderTarget( "Blur level 1 B", w /  2, h /  2, 1, 1, fx::RGBA16, false );

		aData.pBlur2A = pTargetFactory->getMultipleRenderTarget( "Blur level 2 A", w /  4, h /  4, 1, 1, fx::RGBA16, false );
		aData.pBlur2B = pTargetFactory->getMultipleRenderTarget( "Blur level 2 B", w /  4, h /  4, 1, 1, fx::RGBA16, false );

		aData.pBlur3A = pTargetFactory->getMultipleRenderTarget( "Blur level 3 A", w /  8, h /  8, 1, 1, fx::RGBA16, false );
		aData.pBlur3B = pTargetFactory->getMultipleRenderTarget( "Blur level 3 B", w /  8, h /  8, 1, 1, fx::RGBA16, false );

		aData.pBlur4A = pTargetFactory->getMultipleRenderTarget( "Blur level 4 A", w / 16, h / 16, 1, 1, fx::RGBA16, false );
		aData.pBlur4B = pTargetFactory->getMultipleRenderTarget( "Blur level 4 B", w / 16, h / 16, 1, 1, fx::RGBA16, false );

		aData.pAdaptationA = pTargetFactory->createFloatUATarget( "AdaptationA", 2 );
		aData.pAdaptationB = pTargetFactory->createFloatUATarget( "AdaptationB", 2 );
	}

	/// <summary> Build the texture samplers used. </summary>
	/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initSamplers( AdditionalData& aData ) const
	{
		aData.pSamplers[ Point           ] = pStateFactory->createSamplerState( fx::MIRROR, fx::POINT, false );
		aData.pSamplers[ HiQualityWrap   ] = pStateFactory->createSamplerState( fx::WRAP  , fx::ANISOTROPIC, false );
		aData.pSamplers[ HiQualityMirror ] = pStateFactory->createSamplerState( fx::MIRROR, fx::ANISOTROPIC, false );
		aData.pSamplers[ Comparison      ] = pStateFactory->createSamplerState( fx::MIRROR, fx::LINEAR, true );
		aData.pSamplers[ PointWrap       ] = pStateFactory->createSamplerState( fx::WRAP  , fx::POINT, false );
		aData.pSamplers[ PointClamp      ] = pStateFactory->createSamplerState( fx::CLAMP , fx::POINT, false );
		aData.pSamplers[ LinearMirror    ] = pStateFactory->createSamplerState( fx::MIRROR, fx::LINEAR, false );
		aData.pSamplers[ LinearClamp     ] = pStateFactory->createSamplerState( fx::CLAMP , fx::LINEAR, false );
	}

	/// <summary> Build the constant buffers used. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initConstantBuffers( PipelineData& data, AdditionalData& aData, const glm::uvec2& rtSize ) const
	{

		data.pVSShadow1Buffer = pBufferFactory->createConstantBuffer< DepthVSBuffer >( "SHADOW_1_CONSTANTS_VS", 0 );
		data.pVSShadow2Buffer = pBufferFactory->createConstantBuffer< DepthVSBuffer >( "SHADOW_2_CONSTANTS_VS", 0 );

		data.pVSTranslucentShadowBuffer1 = pBufferFactory->createConstantBuffer< SceneVSBuffer >( "TRANSLUCENT_SHADOW_1_CONSTANTS_VS", 0 );
		data.pVSTranslucentShadowBuffer2 = pBufferFactory->createConstantBuffer< SceneVSBuffer >( "TRANSLUCENT_SHADOW_2_CONSTANTS_VS", 0 );

		data.pVSSceneBuffer      = pBufferFactory->createConstantBuffer< SceneVSBuffer     >( "SCENE_CONSTANTS_VS", 0 );
		data.pPSLightingBuffer   = pBufferFactory->createConstantBuffer< LightingPSBuffer  >( "LIGHTING_CONSTANTS_PS", 0 );
		data.pPostFXConstants    = pBufferFactory->createConstantBuffer< PostFXPSBuffer    >( "POST_FX_CONSTANTS_PS", 0 );
		data.pHistogramConstants = pBufferFactory->createConstantBuffer< HistogramCSBuffer >( "HISTOGRAM_CONSTANTS_CS", 0 );

		data.pGSSceneBuffer        = pBufferFactory->createConstantBuffer< SceneGSBuffer >( "NORMAL_CONSTANTS_GS", 0 );
		data.pGSShadow1SceneBuffer = pBufferFactory->createConstantBuffer< SceneGSBuffer >( "SHADOW_1_CONSTANTS_GS", 0 );
		data.pGSShadow2SceneBuffer = pBufferFactory->createConstantBuffer< SceneGSBuffer >( "SHADOW_2_CONSTANTS_GS", 0 );
		data.pPSSunVolumeBuffer    = pBufferFactory->createConstantBuffer< SunVolumePSBuffer >( "SUN_VOLUME_PS", 0 );

		data.pFrameGSConstants    = pBufferFactory->createConstantBuffer< FrameGSBuffer >( "PARTICLE_FRAME_CONSTANTS_GS", 0 );


		data.pABufferResolveCBuffer = pBufferFactory->createConstantBuffer< ABufferResolvePSBuffer >( "A_BUFFER_RESOLVE_CONSTANTS_PS", 0 );


		std::uniform_real_distribution< float > randomFloats( 0.0, 1.0 );
		//const std::poisson_distribution< float > randomFloats( 0.5f );
		//std::default_random_engine generator( int( time( nullptr ) ) );
		//std::default_random_engine generator( 955489 );
		std::default_random_engine generator( 975654489 );

		const int ssilSampleCount = 64;
		vector< vec3 > ssilSamples( ssilSampleCount );

		for( size_t i = 0; i < ssilSamples.size( ); ++i )
		{
			// Random Positioning
			ssilSamples[ i ].x = randomFloats( generator ) * 2.0f - 1.0f;
			ssilSamples[ i ].y = randomFloats( generator ) * 2.0f - 1.0f;
			ssilSamples[ i ].z = randomFloats( generator ) * 1.25f;

			// Scale based on index for a set distribution.
			//float randomOffset = i / 64.0f;
			//      randomOffset = randomOffset * randomOffset;

			ssilSamples[ i ] = normalize( ssilSamples[ i ] );
		}

		const int ssrSampleCount = 64;
		vector< vec3 > ssrSamples( ssrSampleCount );

		vec3 invSize( 1 / rtSize.x, 1 / rtSize.y, 1 );

		for( size_t i = 0; i < ssrSamples.size( ); ++i )
		{
			ssrSamples[ i ].x = randomFloats( generator ) * 2.0f - 1.0f;
			ssrSamples[ i ].y = randomFloats( generator ) * 2.0f - 1.0f;
			ssrSamples[ i ].z = randomFloats( generator );

			// Scale based on index for a set distribution.
			const float randomOffset = i / 64.0f;
			//randomOffset = randomOffset * randomOffset;

			// clamp the value to 0.1 minimum;
			const float weightOffset = 0.1f + randomOffset * ( 1.0f - 0.1f );

			//ssrSamples[ i ] = normalize( ssrSamples[ i ] ) * weightOffset;// * invSize;
			ssrSamples[ i ] = normalize( ssrSamples[ i ] );// * weightOffset;// * invSize;
		}

		const int ssaoSampleCount = 64;
		vector< vec3 > ssaoSamples( ssaoSampleCount );

		for( size_t i = 0; i < ssaoSamples.size( ); ++i )
		{
			// Random Positioning
			ssaoSamples[ i ].x = randomFloats( generator ) * 2.0f - 1.0f;
			ssaoSamples[ i ].y = randomFloats( generator ) * 2.0f - 1.0f;
			ssaoSamples[ i ].z = randomFloats( generator );

			// Scale based on index for a set distribution.
			float randomOffset = i / 64.0f;
			      randomOffset = randomOffset * randomOffset;

			// clamp the value to 0.1 minimum;
			const float weightOffset = 0.1f + randomOffset * ( 1.0f - 0.1f );

			ssaoSamples[ i ] = normalize( ssaoSamples[ i ] ) * weightOffset;
		}




		data.pSSAOShaderData = make_shared< SSAOShaderBuffer >(
			pBufferFactory->createConstantBuffer< SSAOPSBuffer >( "SSAO_CONSTANTS_CS", 0 ),
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >
			{
				pImageFactory->createSrvFloat3TextureBuffer( "SSAOKernel", ssaoSamples ),
				aData.pMRTarget->getShaderResourceViews( )[ 2 ],
				aData.pMRTarget->getShaderResourceViews( )[ 1 ],
				aData.pMRTarget->getDepthShaderResourceViews( ),
				pImageFactory->createSrv1DFromRandom( )
			} )	);


		data.pSSAOUpShaderData = make_shared< SSAOShaderBuffer >(
			pBufferFactory->createConstantBuffer< SSAOPSBuffer >( "SSAO_UP_CONSTANTS_CS", 0 ),
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >
			{
				aData.pAORT->getShaderResourceViews( )[ 0 ],
				aData.pMRTarget->getShaderResourceViews( )[ 2 ],
				aData.pMRTarget->getShaderResourceViews( )[ 1 ],
				aData.pMRTarget->getShaderResourceViews( )[ 3 ],
			} ) );




		data.pSSILShaderData = make_shared< SSAOShaderBuffer >(
			pBufferFactory->createConstantBuffer< SSAOPSBuffer >( "SSIL_CONSTANTS_CS", 0 ),
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >
			{
				pImageFactory->createSrvFloat3TextureBuffer( "SSILKernel", ssilSamples ),
			//	aData.pMipMappedColourRT->getShaderResourceViews( )[ 0 ],
				aData.pMRTarget->getShaderResourceViews( )[ 0 ],
				aData.pMRTarget->getShaderResourceViews( )[ 2 ],
				aData.pMRTarget->getShaderResourceViews( )[ 1 ],
				aData.pMRTarget->getDepthShaderResourceViews( ),
				pImageFactory->createSrv1DFromRandom( )
			} )	);


		data.pSSILUpShaderData = make_shared< SSAOShaderBuffer >(
			pBufferFactory->createConstantBuffer< SSAOPSBuffer >( "SSIL_UP_CONSTANTS_CS", 0 ),
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >
			{
				aData.pAORT->getShaderResourceViews( )[ 0 ],
				aData.pMRTarget->getShaderResourceViews( )[ 2 ],
				aData.pMRTarget->getShaderResourceViews( )[ 1 ],
				aData.pMRTarget->getShaderResourceViews( )[ 3 ],
			} ) );


		data.pSSRShaderData = make_shared< SSAOShaderBuffer >(
			pBufferFactory->createConstantBuffer< SSAOPSBuffer >( "SSR_CONSTANTS_CS", 0 ),
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >
			{
				pImageFactory->createSrvFloat3TextureBuffer( "SSRKernel", ssrSamples ),
				aData.pMRTarget->getShaderResourceViews( )[ 2 ],
				aData.pMRTarget->getShaderResourceViews( )[ 1 ],
				aData.pMRTarget->getDepthShaderResourceViews( ),
				pImageFactory->createSrv1DFromRandom( )
			} ) );


		data.pSSRUpShaderData = make_shared< SSAOShaderBuffer >(
			pBufferFactory->createConstantBuffer< SSAOPSBuffer >( "SSR_UP_CONSTANTS_CS", 0 ),
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >
			{
				aData.pReflectionRT->getShaderResourceViews( )[ 0 ],
				aData.pMipMappedColourRT->getShaderResourceViews( )[ 0 ],
				aData.pMRTarget->getShaderResourceViews( )[ 3 ],
			} ) );
	}

	/// <summary> Build the shaders used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initShaders( AdditionalData& aData ) const
	{
		aData.pFullScreenVS    = pShaderFactory->getVertexShader( IDR_DX_VS_FULLSCREEN );
		aData.pObjVS           = pShaderFactory->getVertexShader( IDR_DX_VS_MODEL );
		aData.pPCGHeightMapVS  = pShaderFactory->getVertexShader( IDR_DX_VS_PCG_HEIGHT );
		aData.pPCGShadowMapVS  = pShaderFactory->getVertexShader( IDR_DX_VS_PCG_SHADOW );
		aData.pPCGWaterVS      = pShaderFactory->getVertexShader( IDR_DX_VS_PCG_WATER );

		aData.pShadowVS        = pShaderFactory->getVertexShader( IDR_DX_VS_MODEL_DEPTH );
		aData.pPassThroughPTVS = pShaderFactory->getVertexShader( IDR_DX_VS_PASS_THROUGH_PT );
		aData.pSkyVS           = pShaderFactory->getVertexShader( IDR_DX_VS_SKY );
		aData.pPointSphereVS   = pShaderFactory->getVertexShader( IDR_DX_VS_WORLD_PASS_THROUGH );

		aData.pSphereGS        = pShaderFactory->getGeometryShader( IDR_DX_GS_SPHERE_BILLBOARD );
		aData.pCubeGS          = pShaderFactory->getGeometryShader( IDR_DX_GS_CUBE );

		aData.pTranslucentShadowPS = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL_DEPTH_TRANSLUCENT );
		aData.pClippedShadowPS     = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL_DEPTH_CLIPPED );

		aData.pSpherePS    = pShaderFactory->getPixelShader( IDR_DX_PS_SPHERE );
		aData.pPbrSpherePS = pShaderFactory->getPixelShader( IDR_DX_PS_SPHERE_PBR );
		aData.pPlanetPS    = pShaderFactory->getPixelShader( IDR_DX_PS_PLANET );
		aData.pMoonPS      = pShaderFactory->getPixelShader( IDR_DX_PS_MOON );
		aData.pCubePS      = pShaderFactory->getPixelShader( IDR_DX_PS_CUBE );
		aData.pParallaxPS  = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL_PARALLAX );
		aData.pObjPS       = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL );
		aData.pHeightMapPS = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL_HEIGHT );

		aData.pWaterPS          = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL_WATER );
		aData.pWaterPcgPS       = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL_WATER_PCG );
		aData.pFullScreenBlitPS = pShaderFactory->getPixelShader( IDR_DX_PS_FULLSCREEN_BLIT );
		aData.pResamplePS       = pShaderFactory->getPixelShader( IDR_DX_PS_RESAMPLE );

		aData.pABufferConstructPS = pShaderFactory->getPixelShader( IDR_DX_PS_TRANSLUCENT_CONSTRUCT );
		aData.pABufferResolvePS   = pShaderFactory->getPixelShader( IDR_DX_PS_TRANSLUCENT_RESOLVE );

		//aData.pSkyPS      = pShaderFactory->getPixelShader( "../Bin/Data/Shaders/Sky.ps.hlsl", "pMainTex" );
		aData.pSkyPS      = pShaderFactory->getPixelShader( IDR_DX_PS_SKY );
		aData.pPolarSkyPS = pShaderFactory->getPixelShader( IDR_DX_PS_SKY_POLAR_MAP );
		aData.pCubeSkyPS  = pShaderFactory->getPixelShader( IDR_DX_PS_SKY_CUBE_MAP );

		aData.pSSAOPS       = pShaderFactory->getPixelShader( IDR_DX_PS_SSAO );
		aData.pSSAOBlurUpPS = pShaderFactory->getPixelShader( IDR_DX_PS_SSAO_BLUR_UP );

		aData.pSSILPS       = pShaderFactory->getPixelShader( IDR_DX_PS_SSIL );
		aData.pSSILBlurUpPS = pShaderFactory->getPixelShader( IDR_DX_PS_SSIL_BLUR_UP );

		aData.pSSRPS       = pShaderFactory->getPixelShader( IDR_DX_PS_SSR );
		aData.pSSRBlurUpPS = pShaderFactory->getPixelShader( IDR_DX_PS_SSR_BLUR_UP );

		aData.pSunVolumeVS = pShaderFactory->getVertexShader( IDR_DX_VS_SUN_VOLUME );
		aData.pSunVolumePS = pShaderFactory->getPixelShader( IDR_DX_PS_SUN_VOLUME );

		aData.pBlurVVS  = pShaderFactory->getVertexShader( IDR_DX_VS_BLUR_V );
		aData.pBlurHVS  = pShaderFactory->getVertexShader( IDR_DX_VS_BLUR_H );
		aData.pBlurPS   = pShaderFactory->getPixelShader( IDR_DX_PS_BLUR );
		aData.pHiPassPS = pShaderFactory->getPixelShader( IDR_DX_PS_HI_PASS );

		aData.pAdaptionCS = pShaderFactory->getComputeShader( IDR_DX_CS_ADAPT_LUMINANCE );

		aData.pClusteringBuildFrustumsCS = pShaderFactory->getComputeShader( IDR_DX_CS_CLUSTERING_BUILD_FRUSTUM );
		aData.pClusteringLightsCS        = pShaderFactory->getComputeShader( IDR_DX_CS_CLUSTERING_LIGHTS   );

		aData.pTileHistogramCS          = pShaderFactory->getComputeShader( IDR_DX_CS_TILE_HISTOGRAM );
		aData.pAccumulateHistogramCS    = pShaderFactory->getComputeShader( IDR_DX_CS_ACCUMULATE_HISTOGRAM );
		aData.pHistogramResponseCurveCS = pShaderFactory->getComputeShader( IDR_DX_CS_RESPONSE_CURVE );

		aData.pParticleSOVertexShader   = pShaderFactory->getVertexShader(IDR_DX_VS_PARTICLES_SO );
		aData.pParticleSOGeometryShader = pShaderFactory->getGeometryShaderOS< VertexParticle >( IDR_DX_GS_PARTICLE_SO );
		aData.pBillboardGeometryShader  = pShaderFactory->getGeometryShader( IDR_DX_GS_PARTICLE_BILLBOARD );
		aData.pBillboardPixelShader     = pShaderFactory->getPixelShader( IDR_DX_PS_PARTICLE_BILLBOARD );

		aData.pDebugRTPS                = pShaderFactory->getPixelShader( IDR_DX_PS_DEBUG_RT );
		aData.pToneMappingPS            = pShaderFactory->getPixelShader( IDR_DX_PS_TONE_MAPPING );

		// DEBUG
		aData.pMeshNormalsVS  = pShaderFactory->getVertexShader( IDR_DX_VS_MODEL_NORMALS );
		aData.pMeshNormalsGS  = pShaderFactory->getGeometryShader( IDR_DX_GS_MODEL_NORMALS );
		aData.pColourPS       = pShaderFactory->getPixelShader( IDR_DX_PS_MODEL_COLOUR );

		aData.pFont2DVS       = pShaderFactory->getVertexShader( IDR_DX_VS_FONT_2D );
		aData.pFont3DVS       = pShaderFactory->getVertexShader( IDR_DX_VS_FONT_3D );
		aData.pFont3DShadowVS = pShaderFactory->getVertexShader( IDR_DX_VS_FONT_3D_SHADOW );
		aData.pFont2DPS       = pShaderFactory->getPixelShader( IDR_DX_PS_FONT_2D );
		aData.pFont3DPS       = pShaderFactory->getPixelShader( IDR_DX_PS_FONT_3D );
		aData.pFont3DShadowPS = pShaderFactory->getPixelShader( IDR_DX_PS_FONT_3D_SHADOW );

		aData.pUIVS = pShaderFactory->getVertexShader( IDR_DX_VS_NUKLEAR_UI );
		aData.pUIPS = pShaderFactory->getPixelShader( IDR_DX_PS_NUKLEAR_UI );
	}

	/// <summary> Build the input layouts used. </summary>
	/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initInputLayouts( AdditionalData & aData ) const
	{
		aData.pInputLayoutObjMesh    = pMeshFactory->createInputLayout< Vertex3f2f3f3f3f >( );
		aData.pInputLayoutPT         = pMeshFactory->createInputLayout< Vertex3f2f >( );
		aData.pInputLayoutParticle   = pMeshFactory->createInputLayout< VertexParticle >( );
		aData.pInputLayoutCharacture = pMeshFactory->createInputLayout< VertexChar >( );
		aData.pInputLayoutUI         = pMeshFactory->createInputLayout< Vertex3f2f4f >( );
	}

	/// <summary> Build the rasterizer states used. </summary>
	/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initRasterizerStates( AdditionalData & aData ) const
	{
		aData.pRasterizerCCW = pStateFactory->createRasterizerState( false, true , true  );
		aData.pRasterizerAll = pStateFactory->createRasterizerState( false, false, false );
		aData.pRasterizerCW  = pStateFactory->createRasterizerState( false, true , false );
	}

	/// <summary> Build the blend states used. </summary>
	/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initBlendStates( AdditionalData & aData ) const
	{
		aData.pBlendWriteOver = pStateFactory->createBlendState( fx::WRITE_OVER );
		aData.pBlendPreMulA   = pStateFactory->createBlendState( fx::PRE_MULTIPLIED_ALPHA_BLEND );
		aData.pBlendAdd       = pStateFactory->createBlendState( fx::ADD_BLEND );
		aData.pBlendAlt       = pStateFactory->createBlendState( fx::ALT_ALPHA_BLEND );
		aData.pBlendStd       = pStateFactory->createBlendState( fx::ALPHA_BLEND );
	}

	/// <summary> Build the depth stencil states used. </summary>
	/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initDepthStencilStates( AdditionalData & aData ) const
	{
		aData.pDepthStencilStateOn   = pStateFactory->createDepthStencilState( true );
		aData.pDepthStencilStateOff  = pStateFactory->createDepthStencilState( false, false );
		aData.pDepthStencilStateTest = pStateFactory->createDepthStencilState( false, true );
	}

	/// <summary> Build geometry to be used in fullscreen passes. </summary>
	/// <param name="aData"> The additional data to fill. </param>
	void PipelineBuilder::initFullScreenGeometry( AdditionalData& aData ) const
	{
		aData.pFullScreenPlane = pMeshFactory->createFullScreenPassMesh( );
		aData.pFullScreenCube  = pMeshFactory->createFullScreenPassCube( );
	}

	/// <summary> Build geometry to be used in fullscreen passes. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	void PipelineBuilder::initDefaultTextures( PipelineData& data ) const
	{
		data.pRandom1D    = pImageFactory->createSrv1DFromRandom( );
		data.pDefaultCube = pImageFactory->createSrv2DCubeFromColour( "CUBE_BLACK", { 0, 0, 0, 1 } );

		data.pDefault2D = pImageFactory->createSrv2DFromDataSet( "DEFAULT",
			{
				{ 1, 0, 0, 1 }, { 0, 1, 0, 1 },
				{ 0, 0, 1, 1 }, { 1, 1, 0, 1 }
			},
			{ 2, 2 } );

		data.pWhite2D = pImageFactory->createSrv2DFromDataSet( "SingleTexelWhite", vector< vec4 >( { { 1, 1, 1, 1 } } ), { 1, 1 } );
		data.pBlack2D = pImageFactory->createSrv2DFromDataSet( "SingleTexelBlack", vector< vec4 >( { { 0, 0, 0, 1 } } ), { 1, 1 } );
		data.pGray2D  = pImageFactory->createSrv2DFromDataSet( "SingleTexelGray" , vector< vec4 >( { { 0.5f, 0.5f, 0.5f, 1 } } ), { 1, 1 } );
		data.pBlankNormal2D = pImageFactory->createSrv2DFromDataSet( "BlankNormal" , vector< vec4 >( { { 0.5f, 0.5f, 1.0f, 1 } } ), { 1, 1 } );

		const int dotSize = 32;
		const int dotMid = dotSize / 2;
		vector< vec4 > dotData( dotSize * dotSize );

		for( int i = 0; i < dotSize; ++i )
		{
			for( int j = 0; j < dotSize; ++j )
			{
				vec2 r( i, j );
				r /= dotSize;
				r -= 0.5f;
				r *= 2.0f;

				const float l = std::max( 1.0f - glm::length( r ), 0.0f );
				const int index = i * dotSize + j;
				dotData[ index ] = vec4( l, l, l, 1 );
			}
		}
		data.pDot2D = pImageFactory->createSrv2DFromDataSet( "DefaultDot" , dotData, { dotSize, dotSize } );

		vector< vec4 >::size_type voronoiSize = 512;
		vector< vec4 > voronoiData( voronoiSize * voronoiSize );

		std::uniform_real_distribution< float > randomFloats = std::uniform_real_distribution< float >( 0.1, 0.9 );
		//std::default_random_engine generator = std::default_random_engine( std::default_random_engine::default_seed );
		std::default_random_engine generator = std::default_random_engine( int( time( nullptr ) ) );

		vector< vec3 > points( 64 );
		for( vec3& point: points )
		{
			point.x = randomFloats( generator );
			point.y = randomFloats( generator );
			point.z = randomFloats( generator );
		}

		// https://www.codeproject.com/Articles/838511/Procedural-seamless-noise-texture-generator
		for( int i = 0; i < voronoiSize; i++ )
		{
			for( int j = 0; j < voronoiSize; j++ )
			{
				float minDist[] = { voronoiSize * 2.0f, voronoiSize * 2.0f };
				float minV[] = { 0, 0 };
				for( int p = 0; p < points.size( ); ++p )
				{
					float pX = points[p].x * voronoiSize;
					float pY = points[p].y * voronoiSize;
					float pV = points[p].z; //0..255
					float dist1X = abs((i - pX));
					float dist1Y = abs((j - pY));
					float dist2X = voronoiSize - dist1X;
					float dist2Y = voronoiSize - dist1Y;
					/*to grant seamless I take the min between distX and wid-distX
						|                       |
						|                       |     ----------- = Dist1X
						|...i-----------X.......|     ..........  = Dist2X
						|                       |
						*/
					dist1X = min(dist1X, dist2X);
					/*to grant seamless I take the min between distY and hei-distY*/
					dist1Y = min(dist1Y, dist2Y);
					float dist = sqrt(pow(dist1X, 2) + pow(dist1Y, 2));

					if( dist < minDist[ 0 ] )
					{
						if (minDist[ 0 ] < minDist[ 1 ] )
						{
							minDist[ 1 ] = minDist[ 0 ];
							minV[ 1 ] = minV[ 0 ];
						}

						minDist[ 0 ] = dist;
						minV[ 0 ] = pV;
					}
					else if (dist < minDist[ 1 ] )
					{
						minDist[ 1 ] = dist;
						minV[ 1 ] = pV;
					}
				}

				const auto index = i * voronoiSize + j;
				voronoiData[ index ] = vec4( minDist[ 0 ], minV[ 0 ], 0, 0 );
			}
		}

		vector< vec4 > vC( voronoiData.size( ) );
		vector< vec4 > vN( voronoiData.size( ) );
		vector< vec4 > vD( voronoiData.size( ) );
		vector< vec4 > vA( voronoiData.size( ) );
		vector< vec4 > vR( voronoiData.size( ) );
		vector< vec4 > vM( voronoiData.size( ) );

		float minSize = 0.0;
		float maxSize = 25.0f;

		auto scaleVD = [ minSize, maxSize, voronoiSize ]( const float vd, const vec2& uv ) -> float
		{
			float n = ( vd - minSize ) / ( maxSize - minSize );

			//return pow( glm::clamp( n, 0.0f, 1.0f ), 2 );

			float t = 1 - pow( glm::clamp( n, 0.0f, 1.0f ), 2 ) * 0.8f;
			//return t * 0.5f + 0.5f;

			vec2 bourder = max( min( abs( ( uv / float( voronoiSize ) ) * 2.0f - 1.0f ) * 4.0f - 2.8f, 1.0f ), 0.4f );

			//return max( max( bourder.x, bourder.y ), t );
			float b = max( bourder.x, bourder.y );
			return ( b + t * ( 1 - b ) );
			//return max( bourder.x, bourder.y );
			//return float( i ) / voronoiSize;
		};

		for( auto i = 0; i < voronoiSize; i++ )
		{
			for( auto j = 0; j < voronoiSize; j++ )
			{
				const auto index = i * voronoiSize + j;

				const auto iN = ( i > 0 ? i : voronoiSize ) - 1;
				const auto iP = i < voronoiSize - 1 ? i + 1 : 0;
				const auto jN = ( j > 0 ? j : voronoiSize ) - 1;
				const auto jP = j < voronoiSize - 1 ? j + 1 : 0;

				float distance = voronoiData[ index ].x;
				float d   = scaleVD( distance, { i, j } );
				//float l = 1 - d;
				/**/
				const auto indexY1 = iN * voronoiSize + j;
				const auto indexY2 = iP * voronoiSize + j;
				const auto indexX1 = i * voronoiSize + jN;
				const auto indexX2 = i * voronoiSize + jP;
				// Welding Spots...

				float dY1 = scaleVD( voronoiData[ indexY1 ].x, { iN, j } );
				float dY2 = scaleVD( voronoiData[ indexY2 ].x, { iP, j } );
				float dX1 = scaleVD( voronoiData[ indexX1 ].x, { i, jN } );
				float dX2 = scaleVD( voronoiData[ indexX2 ].x, { i, jP } );

				const vec2 size = vec2(2.0,0.0);
				const ivec3 off = ivec3(-1,0,1);
				vec3 va = normalize( vec3( 2.0, 0.0, dX2 - dX1 ) );
				vec3 vb = normalize( vec3( 0.0, 2.0, dY1 - dY2 ) );
				vec3 bump = normalize( cross(va,vb) * vec3( 10, 10, 0.3) );

				bump = { ( bump.x + 1 ) * 0.5f, ( bump.y + 1 ) * 0.5f, bump.z };

				float m = 1 - glm::smoothstep( maxSize - 0.05f, maxSize + 0.0f, distance );
				float r = ( 1 - m * 0.95f );
				float ao = max( 1.0f - abs( maxSize - distance ), 0.0f );

				vC[ index ] = vec4( 1, 1, 1, 1 );
				vN[ index ] = vec4( bump, 1 );
				vD[ index ] = vec4( d, d, d, 1 );
				vA[ index ] = vec4( ao, ao, ao, 1 );
				vR[ index ] = vec4( r, r, r, 1 );
				vM[ index ] = vec4( m, 0, 0, 1 );
			}
		}

		data.pVC2D = pImageFactory->createSrv2DFromDataSet( "Voronoi"  , voronoiData, { voronoiSize, voronoiSize } );
		data.pVN2D = pImageFactory->createSrv2DFromDataSet( "VoronoiN" , vN, { voronoiSize, voronoiSize } );
		data.pVD2D = pImageFactory->createSrv2DFromDataSet( "VoronoiD" , vD, { voronoiSize, voronoiSize } );
		data.pVA2D = pImageFactory->createSrv2DFromDataSet( "VoronoiA" , vA, { voronoiSize, voronoiSize } );
		data.pVR2D = pImageFactory->createSrv2DFromDataSet( "VoronoiR" , vR, { voronoiSize, voronoiSize } );
		data.pVM2D = pImageFactory->createSrv2DFromDataSet( "VoronoiM" , vM, { voronoiSize, voronoiSize } );
	}




	/// <summary> Build the 1st cascade opaque shadow pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initLightClusteringPasses( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext, const vec2& size2D ) const
	{
		vector< fx::LightEx > lightCpuList( 6 );

		{
		lightCpuList[ 0 ].colour         = vec4( 5, 5, 5, 0 );
		lightCpuList[ 0 ].positionWS     = vec4( 0, 1, 0, 1 );
		lightCpuList[ 0 ].positionVS     = vec4( 0, 0, 0, 1 );
		lightCpuList[ 0 ].directionWS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 0 ].directionVS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 0 ].spotLightAngle = 0.0f;
		lightCpuList[ 0 ].range          = 15.0f;
		lightCpuList[ 0 ].enabled        = TRUE;
		lightCpuList[ 0 ].type           = 0; // Point 0, Spot 1, Direction 2

		lightCpuList[ 1 ].colour         = vec4( 20, 0, 0, 0 );
		lightCpuList[ 1 ].positionWS     = vec4( 15, 5, 0, 1 );
		lightCpuList[ 1 ].positionVS     = vec4( 0, 0, 0, 1 );
		lightCpuList[ 1 ].directionWS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 1 ].directionVS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 1 ].spotLightAngle = 0.0f;
		lightCpuList[ 1 ].range          = 20.0f;
		lightCpuList[ 1 ].enabled        = TRUE;
		lightCpuList[ 1 ].type           = 0; // Point 0, Spot 1, Direction 2

		lightCpuList[ 2 ].colour         = vec4( 0, 20, 0, 0 );
		lightCpuList[ 2 ].positionWS     = vec4( 0, 5, 15, 1 );
		lightCpuList[ 2 ].positionVS     = vec4( 0, 0, 0, 1 );
		lightCpuList[ 2 ].directionWS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 2 ].directionVS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 2 ].spotLightAngle = 0.0f;
		lightCpuList[ 2 ].range          = 20.0f;
		lightCpuList[ 2 ].enabled        = TRUE;
		lightCpuList[ 2 ].type           = 0; // Point 0, Spot 1, Direction 2

		lightCpuList[ 3 ].colour         = vec4( 0, 0, 20, 0 );
		lightCpuList[ 3 ].positionWS     = vec4( 0, 5, -15, 1 );
		lightCpuList[ 3 ].positionVS     = vec4( 0, 0, 0, 1 );
		lightCpuList[ 3 ].directionWS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 3 ].directionVS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 3 ].spotLightAngle = 0.0f;
		lightCpuList[ 3 ].range          = 20.0f;
		lightCpuList[ 3 ].enabled        = TRUE;
		lightCpuList[ 3 ].type           = 0; // Point 0, Spot 1, Direction 2

		lightCpuList[ 4 ].colour         = vec4( 20, 20, 0, 0 );
		lightCpuList[ 4 ].positionWS     = vec4( -15, 5, 0, 1 );
		lightCpuList[ 4 ].positionVS     = vec4( 0, 0, 0, 1 );
		lightCpuList[ 4 ].directionWS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 4 ].directionVS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 4 ].spotLightAngle = 0.0f;
		lightCpuList[ 4 ].range          = 20.0f;
		lightCpuList[ 4 ].enabled        = TRUE;
		lightCpuList[ 4 ].type           = 0; // Point 0, Spot 1, Direction 2

		lightCpuList[ 5 ].colour         = vec4( 160, 10, 15, 0 );
		lightCpuList[ 5 ].positionWS     = vec4( 9, 15, 7, 1 );
		lightCpuList[ 5 ].positionVS     = vec4( 0, 0, 0, 1 );
		lightCpuList[ 5 ].directionWS    = normalize( vec4( 0, -0.5, 1, 0 ) );
		lightCpuList[ 5 ].directionVS    = vec4( 0, 0, 0, 0 );
		lightCpuList[ 5 ].spotLightAngle = 15.0f;
		lightCpuList[ 5 ].range          = 40.0f;
		lightCpuList[ 5 ].enabled        = TRUE;
		lightCpuList[ 5 ].type           = 1; // Point 0, Spot 1, Direction 2
		}

		const UpdateableSrv lightGpuList = pImageFactory->createUpdateableStructuredBuffer(
			"LIGHTS",
			static_cast< uint32_t >( lightCpuList.size( ) ),
			sizeof( fx::LightEx ),
			lightCpuList.data( )
		);


		const int clusterSlices = 24;
		const float clusterSize = 16;

		const ivec3 tiles( static_cast< int >( ceilf( size2D.x / clusterSize ) ), static_cast< int >( ceilf( size2D.y / clusterSize ) ), clusterSlices );
		const ivec3 size( size2D.x, size2D.y, clusterSlices );

		data.clusteredLighting.cpuLights = lightCpuList;
		data.clusteredLighting.gpuLights = lightGpuList;

		vector< UAVDescriptor > frustumUavDescriptors( 1 );
		frustumUavDescriptors[ 0 ].id         = "LightFrustumClusters";
		frustumUavDescriptors[ 0 ].count      = tiles.x * tiles.y * tiles.z;
		frustumUavDescriptors[ 0 ].stride     = sizeof( fx::Frustum );
		frustumUavDescriptors[ 0 ].addCounter = false;
		frustumUavDescriptors[ 0 ].isUint32   = false;
		frustumUavDescriptors[ 0 ].hasRawView = false;


		vector< UAVDescriptor > clustersUavDescriptors( 3 );
		clustersUavDescriptors[ 0 ].id         = "LightIndexCounter";
		clustersUavDescriptors[ 0 ].count      = 1;
		clustersUavDescriptors[ 0 ].stride     = sizeof( BYTE ) * 4;
		clustersUavDescriptors[ 0 ].addCounter = true;
		clustersUavDescriptors[ 0 ].isUint32   = true;
		clustersUavDescriptors[ 0 ].hasRawView = false;

		clustersUavDescriptors[ 1 ].id         = "LightIndexList";
		clustersUavDescriptors[ 1 ].count      = tiles.x * tiles.y * tiles.z * 256; // TODO: This should match the number of lights I work with in the shader
		clustersUavDescriptors[ 1 ].stride     = sizeof( BYTE ) * 4;
		clustersUavDescriptors[ 1 ].addCounter = true;
		clustersUavDescriptors[ 1 ].isUint32   = true;
		clustersUavDescriptors[ 1 ].hasRawView = false;

		clustersUavDescriptors[ 2 ].id         = "LightGrid";
		clustersUavDescriptors[ 2 ].count      = tiles.x * tiles.y * tiles.z;
		clustersUavDescriptors[ 2 ].stride     = sizeof( fx::LightCluster );
		clustersUavDescriptors[ 2 ].addCounter = true;
		clustersUavDescriptors[ 2 ].isUint32   = false;
		clustersUavDescriptors[ 2 ].hasRawView = false;

		UATargetSPtr pFrustumBuildingUAVs = pTargetFactory->createUATarget( "LightFrustumClusters", frustumUavDescriptors );
		UATargetSPtr pLightClusteringUAVs = pTargetFactory->createUATarget( "LightClusters", clustersUavDescriptors );

		vector< ID3D11ShaderResourceViewSPtr > pFrustumBuildingSRVs;
		vector< ID3D11ShaderResourceViewSPtr > pLightClusteringSRVs = {
			pFrustumBuildingUAVs->getShaderResourceViews( )[ 0 ]
			// TODO: Get the lights... Might have to modify the pass so that I can add the lights later...
			, lightGpuList.pSrv
		};

		vector< ID3D11SamplerStateSPtr > samplers;

		const ClusteredLightingCSBufferSPtr pFrustumCB = pBufferFactory->createConstantBuffer< ClusteredLightingCSBuffer >( "FrustumConstruction", 0 );
		const ClusteredLightingCSBufferSPtr pClusterCB = pBufferFactory->createConstantBuffer< ClusteredLightingCSBuffer >( "LightClustering", 0 );

		pFrustumCB->get( ).numberOfThreads      = tiles;
		pFrustumCB->get( ).numberOfThreadGroups = tiles;

		pClusterCB->get( ).numberOfThreads      = size;
		pClusterCB->get( ).numberOfThreadGroups = tiles;

		data.clusteredLighting.pFrustumConstruction = make_shared< ClusteredLightingComputePass >(
			pContext,
			aData.pClusteringBuildFrustumsCS,

			pFrustumBuildingUAVs,
			samplers,
			pFrustumBuildingSRVs, 0,
			pFrustumCB,
			tiles
		);

		data.clusteredLighting.pLightFrustumCulling = make_shared< ClusteredLightingComputePass >(
			pContext,
			aData.pClusteringLightsCS,

			pLightClusteringUAVs,
			samplers,
			pLightClusteringSRVs, 0,
			pClusterCB,
			tiles
		);

		data.clusteredLighting.clusteredLightingBuffers = pLightClusteringUAVs;
	}

	/// <summary> Build the 1st cascade  shadow pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initOpaqueShadowPass1( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const
	{
		data.pShadowPass1 = make_shared< RenderingPass >( pContext, aData.pShadowTarget1 );

		data.pShadowSubPass1 = make_shared< DepthMeshSubPass >(
			aData.pShadowVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSShadow1Buffer );

		data.pPCGShadowSubPass1 = make_shared< DepthMeshSubPass >(
			aData.pPCGShadowMapVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSShadow1Buffer );

		data.pGeometryShadowSubPass1 = make_shared< DepthGeometryMeshSubPass >(
			aData.pCubeGS,
			aData.pPointSphereVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSShadow1Buffer,
			data.pGSShadow1SceneBuffer );


		data.pShadowPass1->add( data.pShadowSubPass1 );
		data.pShadowPass1->add( data.pPCGShadowSubPass1 );
		data.pShadowPass1->add( data.pGeometryShadowSubPass1 );
	}

	/// <summary> Build the 2nd cascade shadow pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initOpaqueShadowPass2( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const
	{
		data.pShadowPass2 = make_shared< RenderingPass >( pContext, aData.pShadowTarget2 );

		data.pShadowSubPass2 = make_shared< DepthMeshSubPass >(
			aData.pShadowVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSShadow2Buffer );

		data.pPCGShadowSubPass2 = make_shared< DepthMeshSubPass >(
			aData.pPCGShadowMapVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSShadow2Buffer );

		data.pGeometryShadowSubPass2 = make_shared< DepthGeometryMeshSubPass >(
			aData.pCubeGS,
			aData.pPointSphereVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSShadow2Buffer,
			data.pGSShadow2SceneBuffer );

		data.pShadowPass2->add( data.pShadowSubPass2 );
		data.pShadowPass2->add( data.pPCGShadowSubPass2 );
		data.pShadowPass2->add( data.pGeometryShadowSubPass2 );
	}

	/// <summary> Build the 1st cascade transparent shadow pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initTransparentShadowPass1( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const
	{
		data.pTransparentShadowPass1 = make_shared< RenderingPass >( pContext, aData.pTransparentShadowTarget1 );
		data.pTransparentShadowPass1->setRtClearFlags( true, true, true );

		data.pTranslucentShadowSubPass1 = make_shared< ColourMeshSubPass >(
			aData.pTranslucentShadowPS,
			aData.pObjVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSTranslucentShadowBuffer1,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >( ) ), 0
			);

		data.pClippedShadowSubPass1 = make_shared< ColourMeshSubPass >(
			aData.pClippedShadowPS,
			aData.pObjVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSTranslucentShadowBuffer1,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >( ) ), 0 );

		data.pText3DShadowSubPass1 = make_shared< TextSubPass >(
			aData.pFont3DShadowPS,
			aData.pFont3DShadowVS,
			aData.pInputLayoutCharacture,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			//aData.pBlendStd, // TODO: Need as far more complex one than this
			data.pVSTranslucentShadowBuffer1,
			data.pPSLightingBuffer,
			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ LinearClamp ] } ),
			make_shared< PSResources >( ), 0 );

		data.pTransparentShadowPass1->add( data.pTranslucentShadowSubPass1 );
		data.pTransparentShadowPass1->add( data.pClippedShadowSubPass1 );
		data.pTransparentShadowPass1->add( data.pText3DShadowSubPass1 );
	}

	/// <summary> Build the 2nd cascade transparent shadow pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initTransparentShadowPass2( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const
	{
		data.pTransparentShadowPass2 = make_shared< RenderingPass >( pContext, aData.pTransparentShadowTarget2 );
		data.pTransparentShadowPass2->setRtClearFlags( true, false, false );

		data.pTranslucentShadowSubPass2 = make_shared< ColourMeshSubPass >(
			aData.pTranslucentShadowPS,
			aData.pObjVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSTranslucentShadowBuffer2,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >( ) ), 0 );

		data.pClippedShadowSubPass2 = make_shared< ColourMeshSubPass >(
			aData.pClippedShadowPS,
			aData.pObjVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSTranslucentShadowBuffer2,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >( ) ), 0 );

		data.pText3DShadowSubPass2 = make_shared< TextSubPass >(
			aData.pFont3DShadowPS,
			aData.pFont3DShadowVS,
			aData.pInputLayoutCharacture,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			//aData.pBlendStd, // TODO: Need as far more complex one than this
			data.pVSTranslucentShadowBuffer2,
			data.pPSLightingBuffer,
			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ LinearClamp ] } ),
			make_shared< PSResources >( ), 0 );

		data.pTransparentShadowPass2->add( data.pTranslucentShadowSubPass2 );
		data.pTransparentShadowPass2->add( data.pClippedShadowSubPass2 );
		data.pTransparentShadowPass2->add( data.pText3DShadowSubPass2 );
	}

	/// <summary> Build the opaque pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initOpaquePass( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const
	{
		data.pOpaquePass = make_shared< RenderingPass >( pContext, aData.pMRTarget );

		vector< ID3D11ShaderResourceViewSPtr > sceneResources =
		{
			aData.pShadowTarget1->getShaderResourceView( ),				  // Shadow Cascade 1
			aData.pShadowTarget2->getShaderResourceView( ),				  // Shadow Cascade 2
			aData.pTransparentShadowTarget1->getDepthShaderResourceViews( ), // Transparent Shadow Cascade 1
			aData.pTransparentShadowTarget1->getShaderResourceViews( )[ 0 ], // Transparent Shadow Colour 1
			data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 2 ],		  // Lighting Clusters
			data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 1 ],		  // Light Index Lists
			data.clusteredLighting.gpuLights.pSrv													  // Lights //TODO: Get this to change at run time. Add a method to change a SRV of a path at run time. This isn't bound on pipeline creation...
		};

		data.pSphereSubPass = make_shared< GeometryMeshSubPass >(
			aData.pSpherePS,
			aData.pSphereGS,
			aData.pPointSphereVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			//aData.pDepthStencilStateTest,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			data.pGSSceneBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 8 );


		data.pPbrSphereSubPass = make_shared< GeometryMeshSubPass >(
			aData.pPbrSpherePS,
			aData.pSphereGS,
			aData.pPointSphereVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			data.pGSSceneBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 8 );

		data.pPlanetSubPass = make_shared< GeometryMeshSubPass >(
			aData.pPlanetPS,
			aData.pSphereGS,
			aData.pPointSphereVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			data.pGSSceneBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 8 );

		data.pMoonSubPass = make_shared< GeometryMeshSubPass >(
			aData.pMoonPS,
			aData.pSphereGS,
			aData.pPointSphereVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			data.pGSSceneBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 8 );

		data.pCubeSubPass = make_shared< GeometryMeshSubPass >(
			aData.pCubePS,
			aData.pCubeGS,
			aData.pPointSphereVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			data.pGSSceneBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 3 );

		data.pTileSubPass = make_shared< ColourMeshSubPass >(
			aData.pParallaxPS,
			aData.pObjVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerCW,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 9 );

		data.pBasicObjSubPass = make_shared< ColourMeshSubPass >(
			aData.pObjPS,
			aData.pObjVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			//pStateFactory->createRasterizerState( true, false, false ),
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 9 );

		data.pHightMapSubPass = make_shared< ColourMeshSubPass >(
			aData.pHeightMapPS,
			aData.pObjVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 12 );

		data.pPCGHightMapSubPass = make_shared< ColourMeshSubPass >(
			aData.pHeightMapPS,
			aData.pPCGHeightMapVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 12 );

		data.pOpaquePass->add( data.pPbrSphereSubPass );
		data.pOpaquePass->add( data.pPlanetSubPass );
		data.pOpaquePass->add( data.pMoonSubPass );
		data.pOpaquePass->add( data.pCubeSubPass );
		data.pOpaquePass->add( data.pTileSubPass );
		data.pOpaquePass->add( data.pBasicObjSubPass );
		data.pOpaquePass->add( data.pHightMapSubPass );
		data.pOpaquePass->add( data.pPCGHightMapSubPass );
		data.pOpaquePass->add( data.pSphereSubPass );


		// DEBUG NORMALS
		data.pNormalSubPass = make_shared< GeometryMeshSubPass >(
			aData.pColourPS,
			aData.pMeshNormalsGS,
			aData.pMeshNormalsVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			data.pGSSceneBuffer,
			aData.defSamplers,
			make_shared< PSResources >( sceneResources ), 9 );

		data.pOpaquePass->add( data.pNormalSubPass );





		data.pText3DSubPass = make_shared< TextSubPass >(
			aData.pFont3DPS,
			aData.pFont3DVS,
			aData.pInputLayoutCharacture,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			//aData.pBlendStd, // TODO: Need as far more complex one than this
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ LinearClamp ] } ),
			make_shared< PSResources >( sceneResources ), 10 );

		data.pOpaquePass->add( data.pText3DSubPass );
	}

	/// <summary> Build the first basic transparency pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initTransparentPass( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const
	{
		data.pTransparentPass = make_shared< RenderingPass >( pContext, aData.pMRTarget );
		data.pTransparentPass->setRtClearFlags( false, false, false );


		data.pWaterSubPass = make_shared< ColourMeshSubPass >(
			aData.pWaterPS,
			aData.pObjVS,//pMRTVertexPTNTBShader,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >{
				aData.pOpaqueCloneTarget->getShaderResourceViews( )[ 0 ],
				aData.pShadowTarget1->getShaderResourceView( ),
				aData.pShadowTarget2->getShaderResourceView( ),
				aData.pTransparentShadowTarget1->getDepthShaderResourceViews( ),
				aData.pTransparentShadowTarget1->getShaderResourceViews( )[ 0 ],
				data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 2 ],  // Lighting Clusters
				data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 1 ],  // Light Index Lists
				data.clusteredLighting.gpuLights.pSrv
			} ),
			2 );


		data.pPCGWaterSubPass = make_shared< ColourMeshSubPass >(
			aData.pWaterPcgPS,
			aData.pPCGWaterVS,
			aData.pInputLayoutObjMesh,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOn,
			aData.pBlendWriteOver,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >{
				aData.pOpaqueCloneTarget->getShaderResourceViews( )[ 0 ],
				aData.pShadowTarget1->getShaderResourceView( ),
				aData.pShadowTarget2->getShaderResourceView( ),
				aData.pTransparentShadowTarget1->getDepthShaderResourceViews( ),
				aData.pTransparentShadowTarget1->getShaderResourceViews( )[ 0 ],
				data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 2 ],  // Lighting Clusters
				data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 1 ],  // Light Index Lists
				data.clusteredLighting.gpuLights.pSrv
			} ),
			2 );

		data.pTransparentPass->add( data.pWaterSubPass );
		data.pTransparentPass->add( data.pPCGWaterSubPass );
	}

	/// <summary> Build the order independent translucency pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	/// <param name="renderTargetSize"> The render target size. </param>
	void PipelineBuilder::initABufferOITPass( PipelineData& data, AdditionalData& aData,
											  const DeviceContextPtr_t& pContext,
											  const vec2& renderTargetSize ) const
	{
		const uint32_t count = 6;

		vector< UAVDescriptor > uavDescriptors( 2 );
		uavDescriptors[ 0 ].id         = "Translucent_Link";
		uavDescriptors[ 0 ].count      = static_cast< uint32_t >( renderTargetSize.x * renderTargetSize.y ) * count;
		uavDescriptors[ 0 ].stride     = sizeof( float ) * 7 + sizeof( BYTE ) * 4;
		uavDescriptors[ 0 ].addCounter = true;
		uavDescriptors[ 0 ].isUint32   = false;
		uavDescriptors[ 0 ].hasRawView = false;

		uavDescriptors[ 1 ].id         = "Translucent_Index";
		uavDescriptors[ 1 ].count      = static_cast< uint32_t >( renderTargetSize.x * renderTargetSize.y );
		uavDescriptors[ 1 ].stride     = sizeof( BYTE ) * 4;
		uavDescriptors[ 1 ].addCounter = false;
		uavDescriptors[ 1 ].isUint32   = true;
		uavDescriptors[ 1 ].hasRawView = true;


		data.pABufferOITPass = make_shared< ABufferOITPass >(
			pContext,
			pTargetFactory->createUATarget( "TranslucentBuffer", uavDescriptors ),
			aData.pMRTarget,
			pTargetFactory->createShadowMap( "ABufferDepth", uint32_t( renderTargetSize.x ), uint32_t( renderTargetSize.y ) ),
			aData.pRasterizerAll,
			aData.pBlendWriteOver,
			aData.pDepthStencilStateOn,
			aData.pDepthStencilStateOff,
			aData.pInputLayoutObjMesh,
			aData.defSamplers,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >{
				aData.pMRTarget->getDepthShaderResourceViews( ),
				aData.pShadowTarget1->getShaderResourceView( ),
				aData.pShadowTarget2->getShaderResourceView( ),
				aData.pTransparentShadowTarget1->getDepthShaderResourceViews( ),
				aData.pTransparentShadowTarget1->getShaderResourceViews( )[ 0 ],
				data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 2 ],  // Lighting Clusters
				data.clusteredLighting.clusteredLightingBuffers->getShaderResourceViews( )[ 1 ],  // Light Index Lists
				data.clusteredLighting.gpuLights.pSrv
		} ),
			9,
			aData.pOpaqueCloneTarget->getShaderResourceViews(  )[ 0 ],
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,

			aData.pObjVS,
			aData.pABufferConstructPS,

			data.pABufferResolveCBuffer,
			aData.pFullScreenVS,
			aData.pABufferResolvePS );


	}

	/// <summary> Build the sky box passes. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initSkyBoxPass( PipelineData& data, AdditionalData& aData,
										  const DeviceContextPtr_t& pContext ) const
	{
		data.pSkyPass = make_shared< RenderingPass >( pContext, aData.pMRTarget );
		data.pSkyPass->setRtClearFlags( false, false, false );

		vector< ID3D11ShaderResourceViewSPtr > psSkyTextures =
		{
			data.pDefault2D,
			data.pDefault2D
		};


		data.pBasicVolumeSky = make_shared< SkySubPass >(
			aData.pSkyPS,
			aData.pSkyVS,
			aData.pInputLayoutPT,
			aData.pRasterizerCW,
			aData.pDepthStencilStateTest,
			aData.pBlendWriteOver,
			aData.pFullScreenCube,
			make_shared< PSResources >( psSkyTextures ),
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers );

		data.pPolarSky = make_shared< SkySubPass >(
			aData.pPolarSkyPS,
			aData.pSkyVS,
			aData.pInputLayoutPT,
			aData.pRasterizerCW,
			aData.pDepthStencilStateTest,
			aData.pBlendWriteOver,
			aData.pFullScreenCube,
			make_shared< PSResources >( psSkyTextures ),
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers );

		data.pCubeSky = make_shared< SkySubPass >(
			aData.pCubeSkyPS,
			aData.pSkyVS,
			aData.pInputLayoutPT,
			aData.pRasterizerCW,
			aData.pDepthStencilStateTest,
			aData.pBlendWriteOver,
			aData.pFullScreenCube,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >{ data.pDefaultCube } ),
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			aData.defSamplers );

		data.pSkyPass->add( data.pBasicVolumeSky );
		data.pSkyPass->add( data.pPolarSky );
		data.pSkyPass->add( data.pCubeSky );

		data.pBasicVolumeSky->enableDraw( false );
		data.pPolarSky->enableDraw( false );
		data.pCubeSky->enableDraw( true );
	}

	/// <summary> Build the SSAO pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initSSAOPass( PipelineData& data, AdditionalData& aData,
										const DeviceContextPtr_t& pContext ) const
	{
		data.pSSAOPass = make_shared< SSAOPass >(
			pContext,

			aData.pRasterizerCW,
			aData.pBlendWriteOver,
			aData.pBlendAlt,
			//aData.pBlendAdd,
			aData.pDepthStencilStateTest, // todo change to off

			aData.pAORT,
			aData.pVolumeRT,

			aData.pFullScreenVS,
			aData.pSSAOPS,

			data.pSSAOShaderData,

			aData.pSSAOBlurUpPS,
			data.pSSAOUpShaderData,

			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >
			{
				aData.pSamplers[ HiQualityMirror ],
				aData.pSamplers[ Comparison ],
				aData.pSamplers[ HiQualityWrap ],
				aData.pSamplers[ PointClamp ]
			} )
		);

		data.pSSILPass = make_shared< SSAOPass >(
			pContext,

			aData.pRasterizerCW,
			aData.pBlendWriteOver,
			aData.pBlendAdd,
			aData.pDepthStencilStateTest, // todo change to off

			aData.pAORT,
			aData.pVolumeRT,

			aData.pFullScreenVS,
			aData.pSSILPS,

			data.pSSILShaderData,

			aData.pSSILBlurUpPS,
			data.pSSILUpShaderData,

			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >
			{
				aData.pSamplers[ HiQualityMirror ],
				aData.pSamplers[ Comparison ],
				aData.pSamplers[ HiQualityWrap ],
				aData.pSamplers[ PointClamp ]
			} )
		);
	}

	/// <summary> Build the SSR pass. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initSSRPass( PipelineData& data, AdditionalData& aData,
									   const DeviceContextPtr_t& pContext ) const
	{
		data.pSSRPass = make_shared< SSRPass >(
			pContext,

			aData.pRasterizerCW,
			aData.pBlendWriteOver,
			aData.pBlendAdd,
			aData.pDepthStencilStateTest,

			aData.pReflectionRT,
			aData.pVolumeRT,

			aData.pFullScreenVS,
			aData.pSSRPS,

			data.pSSRShaderData,

			aData.pSSRBlurUpPS,
			data.pSSRUpShaderData,

			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >
			{
				aData.pSamplers[ HiQualityMirror ],
				aData.pSamplers[ Comparison ],
				aData.pSamplers[ HiQualityWrap ],
				aData.pSamplers[ PointClamp ]
			} ) );
	}

	/// <summary> Build the volumetric lighting pass using the scene direction light. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initVolumetricLightPass( PipelineData& data, AdditionalData& aData,
												   const DeviceContextPtr_t& pContext ) const
	{
		data.pVolumetricPass = make_shared< RenderingPass >( pContext, aData.pVolumeRT );
		data.pVolumetricPass->setRtClearFlags( false, false, false );



		vector< ID3D11ShaderResourceViewSPtr > psTextures =
		{
			aData.pMRTarget->getShaderResourceViews( )[ 2 ],
			aData.pShadowTarget1->getShaderResourceView( ),
			aData.pShadowTarget2->getShaderResourceView( ),
			aData.pTransparentShadowTarget1->getDepthShaderResourceViews( ),
			aData.pTransparentShadowTarget1->getShaderResourceViews( )[ 0 ],
			data.pRandom1D
		};

		vector< ID3D11SamplerStateSPtr > samplers =
		{
			aData.pSamplers[ PointWrap ],
			aData.pSamplers[ Comparison ]
		};

		PSSamplersSPtr pPSSamplers = make_shared< PSSamplers >( samplers );


		data.pPSSunVolumeBuffer->get( ).density = 1.0;
		data.pPSSunVolumeBuffer->get( ).heightFalloff = 1;
		data.pPSSunVolumeBuffer->get( ).rayDepth = 64.0f;
		data.pPSSunVolumeBuffer->get( ).gScatter = 0.0f;


		data.pVolumetricPass->add( make_shared< VolumetricLightSubPass >(
			aData.pSunVolumePS,
			aData.pSunVolumeVS,
			aData.pInputLayoutPT,
			aData.pRasterizerCW,
			aData.pBlendPreMulA,
			aData.pDepthStencilStateTest,
			aData.pFullScreenCube,
			make_shared< PSResources >( psTextures ),
			data.pVSSceneBuffer,
			data.pPSSunVolumeBuffer,
			pPSSamplers ) );
	}

	/// <summary> Build the bloom and exposure adaption passes. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initBloomPass( PipelineData& data, AdditionalData& aData,
										 const DeviceContextPtr_t& pContext ) const
	{
		data.pBloomPSConsts = pBufferFactory->createConstantBuffer< BlurPSBuffer >( "BLUR_CONSTANTS_PS", 0 );

		BlurVSBufferSPtr pBlur1VSConsts = pBufferFactory->createConstantBuffer< BlurVSBuffer >( "BLUR_1_CONSTANTS_VS", 0 );
		BlurVSBufferSPtr pBlur2VSConsts = pBufferFactory->createConstantBuffer< BlurVSBuffer >( "BLUR_2_CONSTANTS_VS", 0 );
		BlurVSBufferSPtr pBlur3VSConsts = pBufferFactory->createConstantBuffer< BlurVSBuffer >( "BLUR_3_CONSTANTS_VS", 0 );
		BlurVSBufferSPtr pBlur4VSConsts = pBufferFactory->createConstantBuffer< BlurVSBuffer >( "BLUR_4_CONSTANTS_VS", 0 );

		PSResourcesSPtr pFrameBufferPS = make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >{ aData.pMRTarget->getShaderResourceViews( )[ 0 ] } );

		data.pAdaptionConsts = pBufferFactory->createConstantBuffer< AdaptationCSBuffer >( "ADAPTION_CONSTANTS_CS", 0 );

		data.pPostFxPass = make_shared< PostFxPass >(
			pContext,
			aData.pMipMappedColourRT,
			pFrameBufferPS,
			aData.pRasterizerAll,
			aData.pBlendWriteOver,
			aData.pDepthStencilStateOff,
			aData.pSamplers[ LinearMirror ],
			aData.pFullScreenVS,
			aData.pResamplePS,

			aData.pAdaptionCS,
			data.pAdaptionConsts,
			aData.pAdaptationA,
			aData.pAdaptationB,

			aData.pBlur1A,
			aData.pBlur1B,
			aData.pBlur2A,
			aData.pBlur2B,
			aData.pBlur3A,
			aData.pBlur3B,
			aData.pBlur4A,
			aData.pBlur4B,
			aData.pHiPassPS,
			aData.pBlurVVS,
			aData.pBlurHVS,
			aData.pBlurPS,
			pBlur1VSConsts,
			pBlur2VSConsts,
			pBlur3VSConsts,
			pBlur4VSConsts,
			data.pBloomPSConsts );
	}

	/// <summary> Build the histogram construction passes. </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	void PipelineBuilder::initHistogramPass( PipelineData& data, AdditionalData& aData,
											 const DeviceContextPtr_t& pContext,
											 const vec2& renderTargetSize,
											 const vec2& warpSize ) const
	{
		static const uint32_t binCount = 64;
		const float tilesX = ceilf( renderTargetSize.x / warpSize.x );
		const float tilesY = ceilf( renderTargetSize.y / warpSize.y );

		HistogramCSBufferSPtr pHistogramCBuffer = data.pHistogramConstants;
		CSResourcesSPtr pFrameBuffer = make_shared< CSResources >( vector< ID3D11ShaderResourceViewSPtr >{ aData.pMRTarget->getShaderResourceViews( )[ 0 ] } );

		UATargetSPtr pTileHistograms       = pTargetFactory->createUintUATarget( "TileHistogram", static_cast< uint32_t >( tilesX * tilesY * binCount ) );
		UATargetSPtr pHistogram            = pTargetFactory->createUintUATarget( "Histogram", binCount );
		aData.pResponseCurve  = pTargetFactory->createFloatUATarget( "ResponseCurve", binCount );

		data.pHistogramPass = make_shared< ComputeHistogramPass >( pContext,
			aData.pTileHistogramCS,
			aData.pAccumulateHistogramCS,
			aData.pHistogramResponseCurveCS,
			data.pHistogramConstants,
			pFrameBuffer,
			pTileHistograms,
			pHistogram,
			aData.pResponseCurve );
	}

	/// <summary> Build the gpu particle update and draw passes. </summary>
	void PipelineBuilder::initParticlePass( PipelineData& data, AdditionalData& aData,
											const DeviceContextPtr_t& pContext ) const
	{
		data.pParticalPass = make_shared< RenderingPass >( pContext, aData.pMRTarget );
		data.pParticalPass->setRtClearFlags( false, false, false );

		vector< ID3D11SamplerStateSPtr > gsSampler = {
			aData.pSamplers[ PointWrap     ],
			aData.pSamplers[ HiQualityWrap ]
		};

		GSSamplersSPtr pGSSamplers = make_shared< GSSamplers >( gsSampler );
		PSSamplersSPtr pPSSamplers = make_shared< PSSamplers >( gsSampler );

		data.pFrameGSConstants->get( ) =
		{
			{
				{ 1, 0, 0, 0 },
				{ 0, 1, 0, 0 },
				{ 0, 0, 1, 0 },
				{ 0, 0, 0, 1 }
			},
			{
				{ 1, 0, 0, 0 },
				{ 0, 1, 0, 0 },
				{ 0, 0, 1, 0 },
				{ 0, 0, 0, 1 }
			},
			{
				{ 1, 0, 0, 0 },
				{ 0, 1, 0, 0 },
				{ 0, 0, 1, 0 },
				{ 0, 0, 0, 1 }
			},
			{ 0, 0, 0, 0 }
		};

		data.pParticalSubPass = make_shared< ParticleSubPass >(
			aData.pParticleSOVertexShader,
			aData.pParticleSOGeometryShader,
			aData.pBillboardGeometryShader,
			aData.pBillboardPixelShader,
			aData.pInputLayoutParticle,
			aData.pRasterizerAll,
			aData.pBlendStd,
			aData.pDepthStencilStateOff,
			aData.pDepthStencilStateTest,
			data.pFrameGSConstants,
			data.pRandom1D,
			pGSSamplers,
			pPSSamplers );

		data.pParticalPass->add( data.pParticalSubPass );
	}

	/// <summary>
	/// Build a pass the resolves the render targets
	/// to the back buffer to be displayed.
	/// </summary>
	/// <param name="data"> The pipeline data to fill. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	/// <param name="pOutputTarget"> The back buffer output target. </param>
	/// <param name="pUI"> The user interface to render. </param>
	void PipelineBuilder::initResolveBBPass( PipelineData& data, AdditionalData& aData,
											 const DeviceContextPtr_t& pContext,
											 const OutputTargetSPtr& pOutputTarget,
											 const ui::SystemUISPtr& pUI ) const
	{
		data.pBackBufferPass = make_shared< RenderingPass >( pContext, pOutputTarget );
		data.pBackBufferPass->setRtClearFlags( false, false, false );

		vector< ID3D11ShaderResourceViewSPtr > psBackBufferTextures =
		{
			aData.pMRTarget->getShaderResourceViews( )[ 0 ],
			aData.pMRTarget->getShaderResourceViews( )[ 1 ],
			aData.pMRTarget->getShaderResourceViews( )[ 2 ],
			//aData.pTransparentShadowTarget->getShaderResourceViews( )[ 0 ],

			aData.pBlur1A->getShaderResourceViews( )[ 0 ],
			aData.pBlur2A->getShaderResourceViews( )[ 0 ],
			aData.pBlur3A->getShaderResourceViews( )[ 0 ],
			aData.pBlur4A->getShaderResourceViews( )[ 0 ],

			aData.pMRTarget->getDepthShaderResourceViews( ),
			//aData.pShadowTarget1->getDepthShaderResourceViews( ),
			//aData.pTransparentShadowTarget->getDepthShaderResourceViews( ),
			aData.pResponseCurve->getShaderResourceViews( )[ 0 ],
			aData.pAdaptationA->getShaderResourceViews( )[ 0 ]


			//aData.pMRTarget->getShaderResourceViews( )[ 3 ],
			//aData.pMRTarget->getShaderResourceViews( )[ 4 ],
			//aData.pAORT->getShaderResourceViews( )[ 0 ],
//			aData.pTransparentShadowTarget->getShaderResourceViews( )[ 0 ],
//			aData.pMRTarget->getShaderResourceViews( )[ 5 ],
//			pAverageColour->getShaderResourceViews( )[ 0 ],
//			aData.pMRTarget->getShaderResourceViews( )[ 6 ],
//			aData.pMRTarget->getShaderResourceViews( )[ 7 ],
//			aData.pWaterTarget->getShaderResourceViews( )[ 0 ],
//			pMRTarget->getDepthShaderResourceViews( ),
		};

		data.pDebugRTView = make_shared< PostFXSubPass >(
			aData.pDebugRTPS,
			aData.pPassThroughPTVS,
			aData.pInputLayoutPT,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOff,
			aData.pBlendStd,
			aData.pFullScreenPlane,
			make_shared< PSResources >( psBackBufferTextures ),
			data.pPostFXConstants,
			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ Point ], aData.pSamplers[ HiQualityMirror ] } ) );

		data.pBackBufferPass->add( data.pDebugRTView );

		data.pDebugRTView->setEnabled( false );









		data.pLdrInfoSubPass = make_shared< TextSubPass >(
			aData.pFont2DPS,
			aData.pFont2DVS,
			aData.pInputLayoutCharacture,
			aData.pRasterizerAll,
			aData.pDepthStencilStateOff,
			aData.pBlendStd,
			data.pVSSceneBuffer,
			data.pPSLightingBuffer,
			make_shared<PSSamplers>( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ LinearClamp ] } ),
			make_shared< PSResources >( ), 0 );

		data.pBackBufferPass->add( data.pLdrInfoSubPass );


		//R"(C:\Windows\Fonts\arialbd.ttf)";
		//R"(C:\Windows\Fonts\software_tester.ttf)";
		//R"(C:\Windows\Fonts\droidsans.ttf)";
		//R"(C:\Windows\Fonts\droidsans-bold.ttf)";
		//R"(C:\Windows\Fonts\droidsansmonoslashed.ttf)";
		//"C:\\Windows\\Fonts\\dejavusansmono-bold.ttf";
		//"C:\\Windows\\Fonts\\dejavusans-bold.ttf";
		//"C:\\Windows\\Fonts\\brushsci.ttf";
		//"C:\\Windows\\Fonts\\wingding.ttf";
		//"C:\\Windows\\Fonts\\wingdng2.ttf";
		//"C:\\Windows\\Fonts\\wingdng3.ttf";
		//"C:\\Windows\\Fonts\\vineritc.ttf";
		//"C:\\Windows\\Fonts\\hack-bold.ttf";
		//"C:\\Windows\\Fonts\\helvetica.otf"; // no msdf
		//"C:\\Windows\\Fonts\\bookosb.ttf";
		//"C:\\Windows\\Fonts\\bookos.ttf";
		//"C:\\Windows\\Fonts\\constan.ttf";
		//"C:\\Windows\\Fonts\\calibri.ttf";
		//"C:\\Windows\\Fonts\\ubuntu-r.ttf";
		//FontSPtr myFontThingy = pFontFactory->createFont( R"(C:\Windows\Fonts\droidsansmonoslashed.ttf)" );
		FontSPtr myFontThingy = pFontFactory->createFont( R"(C:\Windows\Fonts\arialbd.ttf)" );

		ID3D11BufferSPtr pVSBuffer = pBufferFactory->createConstantBuffer( "DEMO_INFO_MATERIAL_VS", sizeof( BasicVSBuffer::Constants ) );
		ID3D11BufferSPtr pPSBuffer = pBufferFactory->createConstantBuffer( "DEMO_INFO_MATERIAL_PS", sizeof( FontPSBuffer::Constants ) );

		// TODO: Fill in the rest of the font data

		data.pLdrInfoText = make_shared< Text >(
			"Some Stuff",
			myFontThingy,
			pMeshFactory->createDynamicVertexBuffer< VertexChar::Vertex >( "System Info Text", 1024 ),
			make_shared< FontMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ),
				make_shared< FontPSBuffer >( 1, pPSBuffer ),
				make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >{ myFontThingy->charTextureArray } ) ),
			make_shared< DepthMaterialBuffer >(
				make_shared< BasicVSBuffer >( 1, pVSBuffer ) )
			);

		data.pLdrInfoSubPass->add( data.pLdrInfoText );


		ToneMappingPSBufferSPtr pToneMappingPSCBuffer = pBufferFactory->createConstantBuffer< ToneMappingPSBuffer >( "TONE_MAPPING_DATA", 0 );
		ToneMappingPixelShaderBufferSPtr pToneMappingPixelShaderData = make_shared< ToneMappingPixelShaderBuffer >(
			pToneMappingPSCBuffer,
			make_shared< PSResources >(
				vector< ID3D11ShaderResourceViewSPtr >
				{
					aData.pMRTarget->getShaderResourceViews( )[ 0 ],
					aData.pBlur1A->getShaderResourceViews( )[ 0 ],
					aData.pBlur2A->getShaderResourceViews( )[ 0 ],
					aData.pBlur3A->getShaderResourceViews( )[ 0 ],
					aData.pBlur4A->getShaderResourceViews( )[ 0 ],
					aData.pAdaptationA->getShaderResourceViews( )[ 0 ]
				} ) );

		pToneMappingPixelShaderData->setMode( 0 );

		data.pToneMappingPass = make_shared< ToneMappingPass >(
			pContext,
			aData.pRasterizerAll,
			aData.pBlendWriteOver,
			aData.pDepthStencilStateOff,
			pOutputTarget,
			aData.pFullScreenVS,
			aData.pToneMappingPS,
			pToneMappingPixelShaderData,
			make_shared< PSSamplers >( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ PointClamp ], aData.pSamplers[ LinearClamp ] } ) );






















		SceneVSBufferSPtr pVSSceneBuffer = data.pVSSceneBuffer;

		SunVolumePSBufferSPtr      pPSSunVolumeBuffer      = data.pPSSunVolumeBuffer;
		ABufferResolvePSBufferSPtr pABufferResolveCBuffer  = data.pABufferResolveCBuffer;
		AdaptationCSBufferSPtr     pAdaptionConsts         = data.pAdaptionConsts;
		BlurPSBufferSPtr           pBloomPSConsts          = data.pBloomPSConsts;


		UIVSBufferSPtr pVSUIData = pBufferFactory->createConstantBuffer< UIVSBuffer >( "USER_INTERFACE_VS_CB", 0 );
		pVSUIData->get( ).transform = { 2.0f / pOutputTarget->getWidth( ), -2.0f / pOutputTarget->getHeight( ), 0, 0 };


		ui::SystemUI::GetToneMapFn getToneMapping =
			[ pToneMappingPSCBuffer ]( ) -> fx::PSToneMappingBuffer& { return pToneMappingPSCBuffer->get( ); };

		ui::SystemUI::GetAdaptationFn getAdaptation =
			[ pAdaptionConsts ]( ) -> fx::CSAdaptationBuffer& { return pAdaptionConsts->get( ); };

		ui::SystemUI::GetFogFn getFog =
			[ pPSSunVolumeBuffer ]( ) -> fx::PSSunVolumeBuffer& { return pPSSunVolumeBuffer->get( ); };

		ui::SystemUI::GetABufferFn getABuffer =
			[ pABufferResolveCBuffer ]( ) -> fx::PSABufferResolve& { return pABufferResolveCBuffer->get( ); };

		ui::SystemUI::GetBloomFn getBloom =
			[ pBloomPSConsts ]( ) -> fx::PSBlurBuffer& { return pBloomPSConsts->get( ); };

		ui::SystemUI::GetSceneFn getScene =
			[ pVSSceneBuffer ]( ) -> fx::VSSceneBuffer& { return pVSSceneBuffer->get( ); };

		ui::SystemUI::GetUIBufferFn getUIBuffer =
			[ pVSUIData ]( ) -> fx::VSUIBuffer& { return pVSUIData->get( ); };


		PipelineData::ToggleDataSPtr pToggles = data.pToggles;

		ui::SystemUI::GPUDataSPtr pGPUData = make_shared< ui::SystemUI::GPUData >(
			getToneMapping, getAdaptation, getFog, getABuffer, getBloom, getScene,
			[ pToggles ]( ) -> bool& { return pToggles->enableSSR; },
			[ pToggles ]( ) -> bool& { return pToggles->enableSSIL; },
			[ pToggles ]( ) -> bool& { return pToggles->enableSSAO; },
			[ pToggles ]( ) -> bool& { return pToggles->enableOIT; },
			[ pToggles ]( ) -> bool& { return pToggles->enableFog; },
			[ pToggles ]( ) -> bool& { return pToggles->enableDebug; },
			[ pToggles ]( ) -> bool& { return pToggles->enableDebugFPS; },
			[ pToggles ]( ) -> bool& { return pToggles->enableDebugTimes; },
			[ pToggles ]( ) -> bool& { return pToggles->enableDebugNormals; },
			getUIBuffer
			);


		ID3D11ShaderResourceViewSPtr font_texture_view;
		pUI->initGPUData( [ this, &font_texture_view ]( const std::vector< uint32_t >& image, int w, int h ) -> void*
		{
			font_texture_view = pImageFactory->createSrv2DFromDataSet( "UI_ATLAS", image, { w, h } );
			return font_texture_view.get( );
		}, pGPUData );

		uint32_t max_vertex_buffer = 1024 * 1024;
		uint32_t max_index_buffer  = 1024 * 1024 * 4;

		ArrayBuffer vertex_buffer = pMeshFactory->createDynamicVertexBuffer< Vertex3f2f::Vertex >( "UI_VERTEX_BUFFER", static_cast< uint32_t >( max_vertex_buffer ) );
		ArrayBuffer  index_buffer = pMeshFactory->createDynamicIndexBuffer< UINT16 >( "UI_INDEX_BUFFER", static_cast< uint32_t >( max_index_buffer ) );


		data.pUIPass = make_shared< UIPass >(
			pContext,
			aData.pRasterizerAll,
			aData.pBlendStd,
			aData.pDepthStencilStateOff,
			pOutputTarget,
			aData.pInputLayoutUI,
			aData.pUIVS,
			aData.pUIPS,
			pVSUIData,
			make_shared< PSSamplers >( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ LinearClamp ] } ),
			pUI,
			UIPass::Data
			{
				index_buffer, vertex_buffer,
				font_texture_view,
				//pToneMappingPSCBuffer
			} );
	}









	/// <summary>
	/// Clone one render target to another one so that
	/// we can sample the data and render to the source again if needed.
	/// </summary>
	/// <param name="pDestination"> The render target we will send the source data to. </param>
	/// <param name="pSource"> The render target we want to copy. </param>
	/// <param name="data"> The render target that we want to send the data to. </param>
	/// <param name="aData"> The additional data to fill. </param>
	/// <param name="pContext"> The direct x 11 device context. </param>
	RenderingPassSPtr PipelineBuilder::createBlitCopyPass( const MultipleRenderTargetSPtr& pDestination,
														   const MultipleRenderTargetSPtr& pSource,
														   const PipelineData& data, const AdditionalData& aData,
														   const DeviceContextPtr_t& pContext ) const
	{
		RenderingPassSPtr pCopyPass = make_shared< RenderingPass >( pContext, pDestination );
		pCopyPass->setRtClearFlags( false, false, false );

		pCopyPass->add( make_shared< FullScreenSubPass >(
			aData.pFullScreenBlitPS,
			aData.pPassThroughPTVS,
			aData.pInputLayoutPT,
			aData.pRasterizerAll,
			aData.pBlendWriteOver,
			aData.pDepthStencilStateOff,
			aData.pFullScreenPlane,
			make_shared< PSResources >( vector< ID3D11ShaderResourceViewSPtr >
			{
				pSource->getShaderResourceViews( )[ 0 ],
				pSource->getShaderResourceViews( )[ 2 ]
			} ),
			make_shared< PSSamplers >( vector< ID3D11SamplerStateSPtr >{ aData.pSamplers[ Point ] } ) ) );

		return pCopyPass;
	}
} // namespace directX11
} // namespace visual

