#pragma once

// DirectX 11 Pipeline factory.
//
// Build the rendering pipeline that will be used to create our final frame.
//
// Project   : NaKama-Fx
// File Name : PipelineBuilder.h
// Date      : 15/04/2019
// Author    : Nicholas Welters

#ifndef _PIPELINE_BUILDER_DX11_H
#define _PIPELINE_BUILDER_DX11_H

#include "../APITypeDefs.h"
#include "../Passes/GenericComputePass.h"
#include "../Materials/MaterialBuffer.h"
#include "Visual/FX/Types/Light.h"

#include "../Materials/SSAOMaterialBuffer.h"

#include <glm/glm.hpp>
#include "../Types/Samplers.h"

namespace visual
{
namespace ui
{
	FORWARD_DECLARE( SystemUI );
}
namespace fx
{
	FORWARD_DECLARE( Camera );
	FORWARD_DECLARE( Settings );
}

namespace directX11
{
	FORWARD_DECLARE( DeviceFactory );
	FORWARD_DECLARE( ShaderFactory );
	FORWARD_DECLARE( BufferFactory );
	FORWARD_DECLARE( TargetFactory );
	FORWARD_DECLARE(  StateFactory );
	FORWARD_DECLARE(  ImageFactory );
	FORWARD_DECLARE(   MeshFactory );
	FORWARD_DECLARE(   FontFactory );

	FORWARD_DECLARE( OutputTarget );
	FORWARD_DECLARE( DepthStencilDeferredTarget );
	FORWARD_DECLARE( MultipleRenderTarget );
	FORWARD_DECLARE( UATarget );
	FORWARD_DECLARE( RenderingPass );
	FORWARD_DECLARE( ComputePass );
	FORWARD_DECLARE( ABufferOITPass );
	FORWARD_DECLARE( GeometryMeshSubPass );
	FORWARD_DECLARE( ColourMeshSubPass );
	FORWARD_DECLARE( DepthMeshSubPass );
	FORWARD_DECLARE( DepthGeometryMeshSubPass );
	FORWARD_DECLARE( SkySubPass );
	FORWARD_DECLARE( PostFXSubPass );
	FORWARD_DECLARE( SunVolumeSubPass );
	FORWARD_DECLARE( ParticleSubPass );
	FORWARD_DECLARE( TextSubPass );
	FORWARD_DECLARE( ToneMappingPass );
	FORWARD_DECLARE( UIPass );
	FORWARD_DECLARE( ParticleBuffer );
	FORWARD_DECLARE( MeshBuffer );
	FORWARD_DECLARE( Text );

	typedef GenericComputePass< ClusteredLightingCSBuffer > ClusteredLightingComputePass;
	SHARED_PTR_TYPE_DEF( ClusteredLightingComputePass );

	enum SamplerType
	{
		Point = 0, // 1:1 pixel mapping for pass through
		HiQualityWrap  , // The selected quality for texture filtering.
		HiQualityMirror, // The selected quality for texture filtering.

		Comparison,

		PointWrap,
		PointClamp,

		LinearMirror,
		LinearClamp,

		SAMPLER_TYPE_SIZE
	};

	struct ClusteredLightingPass
	{
		ClusteredLightingComputePassSPtr pFrustumConstruction;
		ClusteredLightingComputePassSPtr pLightFrustumCulling;
		UATargetSPtr clusteredLightingBuffers;
		UpdateableSrv gpuLights;
		std::vector< fx::LightEx > cpuLights;
	};

	class PipelineBuilder
	{
	public:
		/// <summary> ctor </summary>
		/// <param name=""> The state factory. </param>
		/// <param name=""> The state factory. </param>
		/// <param name=""> The shader factory. </param>
		/// <param name=""> The buffer factory. </param>
		/// <param name=""> The mesh factory. </param>
		/// <param name=""> The image factory. </param>
		/// <param name=""> The target factory. </param>
		/// <param name=""> The font factory. </param>
		PipelineBuilder( const StateFactorySPtr& pStateFactory,
						 const ShaderFactorySPtr& pShaderFactory,
						 const BufferFactorySPtr& pBufferFactory,
						 const MeshFactorySPtr& pMeshFactory,
						 const ImageFactorySPtr& pImageFactory,
						 const TargetFactorySPtr& pTargetFactory,
						 const FontFactorySPtr& pFontFactory );

		/// <summary> ctor </summary>
		PipelineBuilder( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		PipelineBuilder( const PipelineBuilder& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		PipelineBuilder( PipelineBuilder&& move ) = default;


		/// <summary> dtor </summary>
		~PipelineBuilder( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		PipelineBuilder& operator=( const PipelineBuilder& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		PipelineBuilder& operator=( PipelineBuilder&& move ) = default;





		struct PipelineData
		{
			FORWARD_DECLARE_STRUCT( ToggleData );

			struct ToggleData
			{
				bool enableSSR;
				bool enableSSAO;
				bool enableSSIL;
				bool enableOIT;
				bool enableFog;
				bool enableDebug;
				bool enableDebugNormals;
				bool enableDebugFPS;
				bool enableDebugTimes;
			};

			ToggleDataSPtr pToggles;

			SceneVSBufferSPtr    pVSSceneBuffer;
			LightingPSBufferSPtr pPSLightingBuffer;

			DepthVSBufferSPtr pVSShadow1Buffer;
			DepthVSBufferSPtr pVSShadow2Buffer;

			SceneVSBufferSPtr pVSTranslucentShadowBuffer1;
			SceneVSBufferSPtr pVSTranslucentShadowBuffer2;
			PostFXPSBufferSPtr   pPostFXConstants;
			HistogramCSBufferSPtr pHistogramConstants;

			SceneGSBufferSPtr    pGSSceneBuffer;
			FrameGSBufferSPtr    pFrameGSConstants;

			SSAOShaderBufferSPtr pSSAOShaderData;
			SSAOShaderBufferSPtr pSSAOUpShaderData;

			SSAOShaderBufferSPtr pSSILShaderData;
			SSAOShaderBufferSPtr pSSILUpShaderData;

			SSAOShaderBufferSPtr pSSRShaderData;
			SSAOShaderBufferSPtr pSSRUpShaderData;

			SunVolumePSBufferSPtr pPSSunVolumeBuffer;


			SceneGSBufferSPtr    pGSShadow1SceneBuffer;
			SceneGSBufferSPtr    pGSShadow2SceneBuffer;

			ABufferResolvePSBufferSPtr pABufferResolveCBuffer;
			AdaptationCSBufferSPtr pAdaptionConsts;
			BlurPSBufferSPtr       pBloomPSConsts;


			///////////////////////
			// Light Clustreing //
			/////////////////////
			ClusteredLightingPass clusteredLighting;

			/////////////////////////////
			// Cascade Shadow Mapping //
			///////////////////////////

			/////////////////////////
			// 1st Opaque Cascade //
			///////////////////////
			RenderingPassSPtr pShadowPass1;

			        DepthMeshSubPassSPtr         pShadowSubPass1;
			        DepthMeshSubPassSPtr      pPCGShadowSubPass1;
			DepthGeometryMeshSubPassSPtr pGeometryShadowSubPass1;

			/////////////////////////
			// 2nd Opaque Cascade //
			///////////////////////
			RenderingPassSPtr pShadowPass2;

			        DepthMeshSubPassSPtr         pShadowSubPass2;
			        DepthMeshSubPassSPtr      pPCGShadowSubPass2;
			DepthGeometryMeshSubPassSPtr pGeometryShadowSubPass2;

			//////////////////////////////
			// 1st Transparent Cascade //
			////////////////////////////
			RenderingPassSPtr pTransparentShadowPass1;

			ColourMeshSubPassSPtr pTranslucentShadowSubPass1;
			ColourMeshSubPassSPtr     pClippedShadowSubPass1;
			TextSubPassSPtr            pText3DShadowSubPass1;

			//////////////////////////////
			// 2nd Transparent Cascade //
			////////////////////////////
			RenderingPassSPtr pTransparentShadowPass2;

			ColourMeshSubPassSPtr pTranslucentShadowSubPass2;
			ColourMeshSubPassSPtr     pClippedShadowSubPass2;
			TextSubPassSPtr            pText3DShadowSubPass2;


			///////////////////////////////
			// Opaque Forward Rendering //
			/////////////////////////////
			RenderingPassSPtr pOpaquePass;

			GeometryMeshSubPassSPtr pSphereSubPass;
			GeometryMeshSubPassSPtr pPbrSphereSubPass;
			GeometryMeshSubPassSPtr pPlanetSubPass;
			GeometryMeshSubPassSPtr pMoonSubPass;
			GeometryMeshSubPassSPtr pCubeSubPass;

			ColourMeshSubPassSPtr   pTileSubPass;
			ColourMeshSubPassSPtr   pBasicObjSubPass;
			ColourMeshSubPassSPtr   pHightMapSubPass;
			ColourMeshSubPassSPtr   pPCGHightMapSubPass;

			TextSubPassSPtr pText3DSubPass;

			/////////////////////////////////////////////////
			// Opaque Cloning For Transparency Resampling //
			///////////////////////////////////////////////
			RenderingPassSPtr pOpaqueCopyPass;

			////////////////////////////////////
			// Transparent Forward Rendering //
			//////////////////////////////////
			RenderingPassSPtr pTransparentPass;

			ColourMeshSubPassSPtr pWaterSubPass;
			ColourMeshSubPassSPtr pPCGWaterSubPass;

			////////////////////////////////////////
			// Translucent OIT Forward Rendering //
			//////////////////////////////////////
			ABufferOITPassSPtr pABufferOITPass;

			////////////////////
			// Sky Rendering //
			//////////////////
			RenderingPassSPtr pSkyPass;

			SkySubPassSPtr pPolarSky;
			SkySubPassSPtr pCubeSky;
			SkySubPassSPtr pBasicVolumeSky;

			//////////////////////
			// Post Processing //
			////////////////////
			ComputePassSPtr pSSRPass;
			ComputePassSPtr pSSAOPass;
			ComputePassSPtr pSSILPass;
			ComputePassSPtr pPostFxPass;
			ComputePassSPtr pHistogramPass;

			RenderingPassSPtr pVolumetricPass;

			///////////////////////////
			// GPU Particle Systems //
			/////////////////////////
			RenderingPassSPtr pParticalPass;
			ParticleSubPassSPtr pParticalSubPass;

			//////////////////////////////////////////
			// Render the scene to the back buffer //
			////////////////////////////////////////
			RenderingPassSPtr pBackBufferPass;
			PostFXSubPassSPtr pDebugRTView;

			ToneMappingPassSPtr pToneMappingPass;
			UIPassSPtr pUIPass;


			//////////////////
			// INFO Passes //
			////////////////
			TextSubPassSPtr pLdrInfoSubPass;
			TextSPtr		pLdrInfoText;



			////////////
			// DEBUG //
			//////////
			GeometryMeshSubPassSPtr pNormalSubPass;


			// Default Textures...
			ID3D11ShaderResourceViewSPtr pRandom1D;

			ID3D11ShaderResourceViewSPtr pDefaultCube;
			ID3D11ShaderResourceViewSPtr pDefault2D;
			ID3D11ShaderResourceViewSPtr pWhite2D;
			ID3D11ShaderResourceViewSPtr pBlack2D;
			ID3D11ShaderResourceViewSPtr pGray2D;
			ID3D11ShaderResourceViewSPtr pBlankNormal2D;

			ID3D11ShaderResourceViewSPtr pDot2D;

			ID3D11ShaderResourceViewSPtr pVC2D;
			ID3D11ShaderResourceViewSPtr pVN2D;
			ID3D11ShaderResourceViewSPtr pVD2D;
			ID3D11ShaderResourceViewSPtr pVA2D;
			ID3D11ShaderResourceViewSPtr pVR2D;
			ID3D11ShaderResourceViewSPtr pVM2D;
		};

		struct AdditionalData
		{
			DepthStencilDeferredTargetSPtr pShadowTarget1;
			DepthStencilDeferredTargetSPtr pShadowTarget2;
			MultipleRenderTargetSPtr pTransparentShadowTarget1;
			MultipleRenderTargetSPtr pTransparentShadowTarget2;
			MultipleRenderTargetSPtr pMRTarget;
			MultipleRenderTargetSPtr pOpaqueCloneTarget;
			MultipleRenderTargetSPtr pVolumeRT;
			MultipleRenderTargetSPtr pAORT;
			MultipleRenderTargetSPtr pReflectionRT;
			MultipleRenderTargetSPtr pMipMappedColourRT;
			MultipleRenderTargetSPtr pBlur1A;
			MultipleRenderTargetSPtr pBlur1B;
			MultipleRenderTargetSPtr pBlur2A;
			MultipleRenderTargetSPtr pBlur2B;
			MultipleRenderTargetSPtr pBlur3A;
			MultipleRenderTargetSPtr pBlur3B;
			MultipleRenderTargetSPtr pBlur4A;
			MultipleRenderTargetSPtr pBlur4B;
			UATargetSPtr pAdaptationA;
			UATargetSPtr pAdaptationB;
			UATargetSPtr pResponseCurve;

			ID3D11SamplerStateSPtr pSamplers[ SAMPLER_TYPE_SIZE ];

			ID3D11VertexShaderSPtr pFullScreenVS;
			ID3D11VertexShaderSPtr pShadowVS;
			ID3D11VertexShaderSPtr pObjVS;
			ID3D11VertexShaderSPtr pPCGHeightMapVS;
			ID3D11VertexShaderSPtr pPCGShadowMapVS;
			ID3D11VertexShaderSPtr pPCGWaterVS;
			ID3D11VertexShaderSPtr pPassThroughPTVS;
			ID3D11VertexShaderSPtr pSkyVS;

			ID3D11VertexShaderSPtr pPointSphereVS;

			ID3D11GeometryShaderSPtr pSphereGS;
			ID3D11GeometryShaderSPtr pCubeGS;

			ID3D11PixelShaderSPtr pTranslucentShadowPS;
			ID3D11PixelShaderSPtr pClippedShadowPS;

			ID3D11PixelShaderSPtr pSpherePS;
			ID3D11PixelShaderSPtr pPbrSpherePS;
			ID3D11PixelShaderSPtr pPlanetPS;
			ID3D11PixelShaderSPtr pMoonPS;
			ID3D11PixelShaderSPtr pCubePS;
			ID3D11PixelShaderSPtr pParallaxPS;
			ID3D11PixelShaderSPtr pObjPS;
			ID3D11PixelShaderSPtr pHeightMapPS;

			ID3D11PixelShaderSPtr pWaterPS;
			ID3D11PixelShaderSPtr pWaterPcgPS;
			ID3D11PixelShaderSPtr pFullScreenBlitPS;
			ID3D11PixelShaderSPtr pResamplePS;

			ID3D11PixelShaderSPtr pABufferConstructPS;
			ID3D11PixelShaderSPtr pABufferResolvePS;

			ID3D11PixelShaderSPtr pSkyPS;
			ID3D11PixelShaderSPtr pPolarSkyPS;
			ID3D11PixelShaderSPtr pCubeSkyPS;

			ID3D11PixelShaderSPtr pSSAOPS;
			ID3D11PixelShaderSPtr pSSAOBlurUpPS;

			ID3D11PixelShaderSPtr pSSILPS;
			ID3D11PixelShaderSPtr pSSILBlurUpPS;

			ID3D11PixelShaderSPtr pSSRPS;
			ID3D11PixelShaderSPtr pSSRBlurUpPS;

			ID3D11VertexShaderSPtr pSunVolumeVS;
			ID3D11PixelShaderSPtr  pSunVolumePS;

			ID3D11VertexShaderSPtr pBlurVVS;
			ID3D11VertexShaderSPtr pBlurHVS;
			ID3D11PixelShaderSPtr  pBlurPS;
			ID3D11PixelShaderSPtr  pHiPassPS;

			ID3D11ComputeShaderSPtr pAdaptionCS;

			ID3D11ComputeShaderSPtr pClusteringBuildFrustumsCS;
			ID3D11ComputeShaderSPtr pClusteringLightsCS;

			ID3D11ComputeShaderSPtr pTileHistogramCS;
			ID3D11ComputeShaderSPtr pAccumulateHistogramCS;
			ID3D11ComputeShaderSPtr pHistogramResponseCurveCS;

			ID3D11VertexShaderSPtr   pParticleSOVertexShader;
			ID3D11GeometryShaderSPtr pParticleSOGeometryShader;
			ID3D11GeometryShaderSPtr pBillboardGeometryShader;
			ID3D11PixelShaderSPtr    pBillboardPixelShader;

			ID3D11PixelShaderSPtr  pDebugRTPS;
			ID3D11PixelShaderSPtr  pToneMappingPS;

			ID3D11VertexShaderSPtr pUIVS;
			ID3D11PixelShaderSPtr  pUIPS;

			ID3D11VertexShaderSPtr pFont2DVS;
			ID3D11VertexShaderSPtr pFont3DVS;
			ID3D11VertexShaderSPtr pFont3DShadowVS;
			ID3D11PixelShaderSPtr  pFont2DPS;
			ID3D11PixelShaderSPtr  pFont3DPS;
			ID3D11PixelShaderSPtr  pFont3DShadowPS;

			ID3D11InputLayoutSPtr pInputLayoutObjMesh;
			ID3D11InputLayoutSPtr pInputLayoutPT;
			ID3D11InputLayoutSPtr pInputLayoutParticle;
			ID3D11InputLayoutSPtr pInputLayoutCharacture;
			ID3D11InputLayoutSPtr pInputLayoutUI;


			ID3D11DepthStencilStateSPtr pDepthStencilStateOn;
			ID3D11DepthStencilStateSPtr pDepthStencilStateOff;
			ID3D11DepthStencilStateSPtr pDepthStencilStateTest;


			RasterizerStateSPtr_t pRasterizerCCW;
			RasterizerStateSPtr_t pRasterizerAll;
			RasterizerStateSPtr_t pRasterizerCW;


			BlendStateSPtr_t pBlendWriteOver;
			BlendStateSPtr_t pBlendPreMulA;
			BlendStateSPtr_t pBlendAdd;
			BlendStateSPtr_t pBlendAlt;
			BlendStateSPtr_t pBlendStd;


			PSSamplersSPtr defSamplers;


			MeshBufferSPtr pFullScreenPlane;
			MeshBufferSPtr pFullScreenCube;


			// DEBUG
			ID3D11VertexShaderSPtr   pMeshNormalsVS;
			ID3D11GeometryShaderSPtr pMeshNormalsGS;
			ID3D11PixelShaderSPtr    pColourPS;
		};

		// ///////// //
		// Per Scene //
		// ///////// //
		/// <summary> Copy assignment operator </summary>
		/// <param name="pOutputTarget"> The output target of the pipline. </param>
		/// <param name="pSettings"> The renderer settings object. </param>
		/// <param name="pContext"> The device context. </param>
		/// <param name="pUI"> The user interface to render. </param>
		/// <returns> The rendering pipeline data. </returns>
		PipelineData buildScenePipeline( const OutputTargetSPtr&   pOutputTarget,
										 const fx::SettingsSPtr&   pSettings,
										 const DeviceContextPtr_t& pContext,
										 const ui::SystemUISPtr&  pUI ) const;

	private:
		/// <summary> Build the render targets that are going to be used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="sm1Size"> The 1st cascade shadow map size. </param>
		/// <param name="sm2Size"> The 2nd cascade shadow map size. </param>
		/// <param name="rtSize"> The render target size. </param>
		/// <param name="msaa"> The msaa sample count. </param>
		void initDifferedRenderTargets( AdditionalData& aData,
										const glm::uvec2& sm1Size,
										const glm::uvec2& sm2Size,
										const glm::uvec2& rtSize,
										int msaa ) const;

		/// <summary> Build the texture samplers used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		void initSamplers( AdditionalData& aData ) const;

		/// <summary> Build the constant buffers used. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="rtSize"> The render target size. </param>
		void initConstantBuffers( PipelineData& data, AdditionalData& aData, const glm::uvec2& rtSize ) const;

		/// <summary> Build the shaders used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		void initShaders( AdditionalData& aData ) const;

		/// <summary> Build the input layouts used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		void initInputLayouts( AdditionalData& aData ) const;

		/// <summary> Build the rasterizer states used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		void initRasterizerStates( AdditionalData& aData ) const;

		/// <summary> Build the blend states used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		void initBlendStates( AdditionalData& aData ) const;

		/// <summary> Build the depth stencil states used. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		void initDepthStencilStates( AdditionalData& aData ) const;

		/// <summary> Build geometry to be used in fullscreen passes. </summary>
		/// <param name="aData"> The additional data to fill. </param>
		void initFullScreenGeometry( AdditionalData& aData ) const;

		/// <summary> Build the default textures that can be used in the system. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		void initDefaultTextures( PipelineData& data ) const;


		/// <summary>
		/// Build two compute passes.
		/// The first builds the view fustrum clusters and only needs
		/// to be run once for a given render target size.
		/// The second runs each frame to sort the scene lights in to the view fustrum clusters.
		/// </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		/// <param name="size2D"> The 2d screen size. </param>
		void initLightClusteringPasses( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext, const glm::vec2& size2D ) const;

		/// <summary> Build the 1st cascade opaque shadow pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initOpaqueShadowPass1( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the 2nd cascade opaque shadow pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initOpaqueShadowPass2( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the 1st cascade transparent shadow pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initTransparentShadowPass1( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the 2nd cascade transparent shadow pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initTransparentShadowPass2( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the opaque pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initOpaquePass( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the first basic transparency pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initTransparentPass( PipelineData& data, AdditionalData& aData, const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the order independent translucency pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		/// <param name="renderTargetSize"> The render target size. </param>
		void initABufferOITPass( PipelineData& data, AdditionalData& aData,
								 const DeviceContextPtr_t& pContext,
								 const glm::vec2& renderTargetSize ) const;

		/// <summary> Build the sky box passes. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initSkyBoxPass( PipelineData& data, AdditionalData& aData,
							 const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the SSAO pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initSSAOPass( PipelineData& data, AdditionalData& aData,
						   const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the SSR pass. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initSSRPass( PipelineData& data, AdditionalData& aData,
							const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the volumetric lighting pass using the scene direction light. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initVolumetricLightPass( PipelineData& data, AdditionalData& aData,
									  const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the bloom and exposure adaption passes. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initBloomPass( PipelineData& data, AdditionalData& aData,
							const DeviceContextPtr_t& pContext ) const;

		/// <summary> Build the histogram construction passes. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initHistogramPass( PipelineData& data, AdditionalData& aData,
								const DeviceContextPtr_t& pContext,
								 const glm::vec2& renderTargetSize,
								 const glm::vec2& warpSize ) const;

		/// <summary> Build the gpu particle update and draw passes. </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		void initParticlePass( PipelineData& data, AdditionalData& aData,
							   const DeviceContextPtr_t& pContext ) const;

		/// <summary>
		/// Build a pass the resolves the render targets
		/// to the back buffer to be displayed.
		/// </summary>
		/// <param name="data"> The pipeline data to fill. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		/// <param name="pOutputTarget"> The back buffer output target. </param>
		/// <param name="pUI"> The user interface to render. </param>
		void initResolveBBPass( PipelineData& data, AdditionalData& aData,
								const DeviceContextPtr_t& pContext,
								const OutputTargetSPtr& pOutputTarget,
								const ui::SystemUISPtr& pUI ) const;


		/// <summary>
		/// Clone one render target to another one so that
		/// we can sample the data and render to the source again if needed.
		/// </summary>
		/// <param name="pDestination"> The render target we will send the source data to. </param>
		/// <param name="pSource"> The render target we want to copy. </param>
		/// <param name="data"> The render target that we want to send the data to. </param>
		/// <param name="aData"> The additional data to fill. </param>
		/// <param name="pContext"> The direct x 11 device context. </param>
		RenderingPassSPtr createBlitCopyPass( const MultipleRenderTargetSPtr& pDestination,
											  const MultipleRenderTargetSPtr& pSource,
											  const PipelineData& data, const AdditionalData& aData,
											  const DeviceContextPtr_t& pContext ) const;

	private:
		/// <summary> The state factory. </summary>
		StateFactorySPtr pStateFactory;

		/// <summary> The shader factory. </summary>
		ShaderFactorySPtr pShaderFactory;

		/// <summary> The buffer factory. </summary>
		BufferFactorySPtr pBufferFactory;

		/// <summary> The mesh factory. </summary>
		MeshFactorySPtr pMeshFactory;

		/// <summary> The image factory. </summary>
		ImageFactorySPtr pImageFactory;

		/// <summary> The target factory. </summary>
		TargetFactorySPtr pTargetFactory;

		/// <summary> The font factory. </summary>
		FontFactorySPtr pFontFactory;
	};
} // namespace directX11
} // namespace visual

# endif // _SCENE_DX11_H
