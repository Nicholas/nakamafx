#pragma once
// DX11 API Type Defs
//
// Project   : NaKama-Fx
// File Name : APITypeDefs.h
// Date      : 29/03/2019
// Author    : Nicholas Welters

#ifndef _API_TYPE_DEF_DX11_H
#define _API_TYPE_DEF_DX11_H


#include <Macros.h>
#include <Library/Library.h>
#include <Threading/Buffer.h>

#include <exception>
#include <string>

#ifdef ENABLE_DX_11_4
#include <d3d11_4.h>
#define USE_DX11_1
#define USE_DX11_2
#define USE_DX11_3
#define USE_DX11_4
#elif defined ENABLE_DX_11_3
#include <d3d11_3.h>
#define USE_DX11_1
#define USE_DX11_2
#define USE_DX11_3
#elif defined ENABLE_DX_11_2
#include <d3d11_2.h>
#define USE_DX11_1
#define USE_DX11_2
#elif defined ENABLE_DX_11_1
#include <d3d11_1.h>
#define USE_DX11_1
#elif defined ENABLE_DX_11_0
#include <D3D11.h>
#endif

#include <wrl/client.h>
#define FORWARD_DECLARE_STRUCT_COM_PTR(struct_type) typedef Microsoft::WRL::ComPtr<struct struct_type> PPCAT( struct_type, CPtr );

// COM
FORWARD_DECLARE_STRUCT_COM_PTR( IUnknown );

#define STRUCT_TYPE_DEC( TYPE ) \
			struct TYPE;

#define COM_RELEASER_TYPE( TYPE ) \
			std::function< void (IUnknown*) >	

#define FACTORY_TYPE( TYPE ) PPCAT( TYPE, Factory ) // TypeFactory
#define LIBRARY_TYPE( TYPE ) PPCAT( TYPE, Library ) // TypeLibrary

#define ID3D11_LIBRARY_TYPE_DEF( TYPE ) \
			typedef tools::Library< TYPE, std::string, COM_RELEASER_TYPE( TYPE ), FACTORY_TYPE( TYPE ) > LIBRARY_TYPE( TYPE );

#define FACTORY_TYPE_DEF( TYPE ) \
			typedef std::function< TYPE* () > FACTORY_TYPE( TYPE );

#define BOOK_TYPE( TYPE ) \
			typedef LIBRARY_TYPE( TYPE )::DataSPtr POINTER_TYPE_NAME( PPCAT( TYPE, S ) )

#define DEFINE_ID3D11_LIBRARY_TYPES( TYPE ) \
			STRUCT_TYPE_DEC        ( TYPE ) \
			FACTORY_TYPE_DEF       ( TYPE ) \
			ID3D11_LIBRARY_TYPE_DEF( TYPE ) \
			BOOK_TYPE              ( TYPE )

#define ID3D11_FACTORY( TYPE ) \
	[ ]( FACTORY_TYPE( TYPE ) factory ){ return factory( ); }

// This is only needed if I am storing direct DX11 types in the flyweight structure!
#define DX11_DELETER \
	[ ] ( IUnknown* p ) \
	{ \
		if( p ) \
		{ \
			p->Release( ); \
			p = nullptr; \
		} \
	}


// DXGI 1.0 - 1.1
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDevice );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIFactory );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISurface );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIAdapter );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIObject );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDeviceSubObject );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIResource );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIKeyedMutex );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIOutput );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISwapChain );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDevice1 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIFactory1 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISurface1 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIAdapter1 );


// DX 11
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Device );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11DeviceContext );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11DeviceChild );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Resource );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11View );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Asynchronous );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Query );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Predicate );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Counter );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11ClassInstance );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11ClassLinkage );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11CommandList );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11AuthenticatedChannel );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11CryptoSession );

FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoDevice );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoContext );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoDecoder );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoProcessor );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoProcessorEnumerator );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoProcessorOutputView );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoProcessorInputView );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoDecoderOutputView );

FORWARD_DECLARE_STRUCT( ID3D11Resource );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11Buffer );

DEFINE_ID3D11_LIBRARY_TYPES( ID3D11VertexShader        );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11HullShader          );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11DomainShader        );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11GeometryShader      );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11PixelShader         );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11ComputeShader       );

DEFINE_ID3D11_LIBRARY_TYPES( ID3D11BlendState          );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11RasterizerState     );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11DepthStencilState   );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11InputLayout         );

DEFINE_ID3D11_LIBRARY_TYPES( ID3D11ShaderResourceView  );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11Texture1D           );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11Texture2D           );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11Texture3D           );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11SamplerState        );

DEFINE_ID3D11_LIBRARY_TYPES( ID3D11RenderTargetView    );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11DepthStencilView    );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11UnorderedAccessView );

// DX11 SDK
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Debug );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11SwitchToRef );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11TracingDevice );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11RefTrackingOptions );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11RefDefaultTrackingOptions );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11InfoQueue );

#ifdef USE_DX11_1

// DXGI 1.2
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDisplayControl );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIOutputDuplication );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISwapChain1 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIResource1 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIOutput1 );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDevice2 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIFactory2 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISurface2 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIAdapter2 );


// DX 11.1
FORWARD_DECLARE_STRUCT_COM_PTR( ID3DUserDefinedAnnotation );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3DDeviceContextState );

FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Device1 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11DeviceContext1 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoDevice1 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoContext1 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoProcessorEnumerator1 );


DEFINE_ID3D11_LIBRARY_TYPES( ID3D11BlendState1 );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11RasterizerState1 );

#endif

#ifdef USE_DX11_2

// DXGI 1.3
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIFactoryMedia );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISwapChainMedia );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDecodeSwapChain );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIOutput2 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISwapChain2 );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDevice3 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIFactory3 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIOutput3 );

// DX 11.2
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Device2 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11DeviceContext2 );

#endif

#ifdef USE_DX11_3

// DX 11.3
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11RenderTargetView1 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11UnorderedAccessView1 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Query1 );

FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Device3 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11DeviceContext3 );


DEFINE_ID3D11_LIBRARY_TYPES( ID3D11RasterizerState2 );

DEFINE_ID3D11_LIBRARY_TYPES( ID3D11ShaderResourceView1 );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11Texture2D1 );
DEFINE_ID3D11_LIBRARY_TYPES( ID3D11Texture3D1 );

#endif

#ifdef USE_DX11_4

// DXGI 1.4
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIAdapter3 );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIOutput4 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIFactory4 );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDevice4 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISwapChain4 );

 // DXGI 1.5
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIDevice5 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGISwapChain5 );

FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIOutput5 );
FORWARD_DECLARE_STRUCT_COM_PTR( IDXGIFactory5 );

// DX 11.4
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Multithread );

FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoDevice2 );
FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoContext2 );

FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11VideoContext3 );

FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Device4 );

FORWARD_DECLARE_STRUCT_COM_PTR( ID3D11Device5 );

#endif










namespace visual
{
namespace directX11
{
	struct DX11GPU
	{
		ID3D11DeviceCPtr pDevice;
#ifdef USE_DX11_1
		ID3D11Device1CPtr pDevice1;
#endif
#ifdef USE_DX11_2
		ID3D11Device2CPtr pDevice2;
#endif
#ifdef USE_DX11_3
		ID3D11Device3CPtr pDevice3;
#endif
#ifdef USE_DX11_4
		ID3D11Device4CPtr pDevice4;
		ID3D11Device5CPtr pDevice5;
#endif
	};

	
#ifdef USE_DX11_4
		
	typedef ID3D11DeviceContext3CPtr      DeviceContextPtr_t;
	typedef ID3D11ShaderResourceView1SPtr ShaderResourceViewPtr_t;

	typedef IDXGIFactory5CPtr   FactoryPtr_t;
	typedef IDXGIAdapter3CPtr   AdapterPtr_t;
	typedef IDXGISwapChain4CPtr SwapChainPtr_t;
	typedef IDXGIOutput5CPtr    OutputPtr_t;

	typedef ID3D11RasterizerState2SPtr RasterizerStateSPtr_t;
	typedef ID3D11BlendState1SPtr      BlendStateSPtr_t;

#elif defined USE_DX11_3

	typedef ID3D11DeviceContext3CPtr      DeviceContextPtr_t;
	typedef ID3D11ShaderResourceView1SPtr ShaderResourceViewPtr_t;

	typedef IDXGIFactory3CPtr   FactoryPtr_t;
	typedef IDXGIAdapter2CPtr   AdapterPtr_t;
	typedef IDXGISwapChain2CPtr SwapChainPtr_t;
	typedef IDXGIOutput3CPtr    OutputPtr_t;

	typedef ID3D11RasterizerState2SPtr RasterizerStateSPtr_t;
	typedef ID3D11BlendState1SPtr      BlendStateSPtr_t;

#elif defined USE_DX11_2

	typedef ID3D11DeviceContext2CPtr     DeviceContextPtr_t;
	typedef ID3D11ShaderResourceViewSPtr ShaderResourceViewPtr_t;

	typedef IDXGIFactory3CPtr   FactoryPtr_t;
	typedef IDXGIAdapter2CPtr   AdapterPtr_t;
	typedef IDXGISwapChain2CPtr SwapChainPtr_t;
	typedef IDXGIOutput3CPtr    OutputPtr_t;

	typedef ID3D11RasterizerState1SPtr RasterizerStateSPtr_t;
	typedef ID3D11BlendState1SPtr      BlendStateSPtr_t;

#elif defined USE_DX11_1

	typedef ID3D11DeviceContext1CPtr     DeviceContextPtr_t;
	typedef ID3D11ShaderResourceViewSPtr ShaderResourceViewPtr_t;

	typedef IDXGIFactory2CPtr   FactoryPtr_t;
	typedef IDXGIAdapter2CPtr   AdapterPtr_t;
	typedef IDXGISwapChain1CPtr SwapChainPtr_t;
	typedef IDXGIOutput1CPtr    OutputPtr_t;

	typedef ID3D11RasterizerState1SPtr RasterizerStateSPtr_t;
	typedef ID3D11BlendState1SPtr      BlendStateSPtr_t;

#else

	typedef ID3D11DeviceContextCPtr      DeviceContextPtr_t;
	typedef ID3D11ShaderResourceViewSPtr ShaderResourceViewPtr_t;

	typedef IDXGIFactory1CPtr  FactoryPtr_t;
	typedef IDXGIAdapter1CPtr  AdapterPtr_t;
	typedef IDXGISwapChainCPtr SwapChainPtr_t;
	typedef IDXGIOutputCPtr    OutputPtr_t;
	
	typedef ID3D11RasterizerStateSPtr RasterizerStateSPtr_t;
	typedef ID3D11BlendStateSPtr      BlendStateSPtr_t;

#endif
	
	struct DevicePackage
	{
		ID3D11DeviceCPtr		 pDevice;
		ID3D11DeviceContextCPtr  pContext;
#ifdef USE_DX11_1
		ID3D11Device1CPtr		 pDevice1;
		ID3D11DeviceContext1CPtr pContext1;
#endif
#ifdef USE_DX11_2
		ID3D11Device2CPtr		 pDevice2;
		ID3D11DeviceContext2CPtr pContext2;
#endif
#ifdef USE_DX11_3
		ID3D11Device3CPtr		 pDevice3;
		ID3D11DeviceContext3CPtr pContext3;
#endif
#ifdef USE_DX11_4
		ID3D11Device4CPtr		 pDevice4;
		ID3D11Device5CPtr		 pDevice5;
#endif
	};






	struct PriorityCommandList
	{
		int priority;
		//ID3D11CommandListCPtr pCommandList;
		ID3D11CommandList* pCommandList;
	};

	struct PriorityCommandListComparator
	{
		bool operator( )( const PriorityCommandList& lhs, const PriorityCommandList& rhs ) const
		{
			return lhs.priority < rhs.priority;
		}
	};

	/** The definition of the list used to hold the priority queue of processes */
	typedef std::priority_queue< PriorityCommandList, std::vector< PriorityCommandList >, PriorityCommandListComparator > CommandQueue;
	typedef tools::Buffer< CommandQueue > BufferedCommandQueue;
	typedef std::shared_ptr< BufferedCommandQueue > BufferedCommandQueueSPtr;


	typedef std::function< void( const DeviceContextPtr_t& ) > AsyncEvent;
	typedef std::function< void( const AsyncEvent& ) > AsyncDispatcher;
	typedef tools::Buffer< AsyncEvent > AsyncEventBuffer;
	typedef std::shared_ptr< AsyncEventBuffer > AsyncEventBufferSPtr;

	struct UpdateableSrv
	{
		ID3D11BufferSPtr pBuffer;
		ID3D11ShaderResourceViewSPtr pSrv;
	};

	struct Features
	{
		bool allowTearing = false;
	};

	struct ArrayBuffer
	{
		// This is the number of elements
		unsigned int size;
		// The buffer on the GPU that contains a index or vertex buffer.
		ID3D11BufferSPtr pBuffer;
		// This is the size of an element.
		unsigned int elementSize;
	};

	class DxException : public std::exception
	{
	public:
		DxException( ) = default;
		DxException( 
			const HRESULT result,
			const std::string& functionName,
			const std::string& fileName,
			const int lineNumber
		)
			: std::exception( ( fileName + ": " + std::to_string( lineNumber ) + "\n" + functionName + " --> " + std::to_string( result ) ).c_str( ) )
			//, result( result )
			, functionName( functionName )
			, fileName( fileName )
			//, lineNumber( lineNumber )
		{
			printf( "%s", this->exception::what( ) );
		}

	private:

		//HRESULT result = S_OK;
		std::string functionName;
		std::string fileName;
		//int lineNumber = -1;
	};

#ifndef ThrowIfFailed
#define ThrowIfFailed( x ) \
{\
	HRESULT hr__ = (x);\
	if(FAILED(hr__)){ throw DxException(hr__, #x, __FILE__, __LINE__);}\
}
#endif

inline void setDebugName( ID3D11Device* const pUnknown, const std::string& name )
{
#ifdef _DEBUG
	if( pUnknown )
	{
		pUnknown->SetPrivateData( WKPDID_D3DDebugObjectName, static_cast< UINT >( name.length( ) ), name.c_str( ) );
	}
#endif
}

inline void setDebugName( const ID3D11DeviceCPtr& pUnknown, const std::string& name )
{
#ifdef _DEBUG
	if( pUnknown )
	{
		pUnknown->SetPrivateData( WKPDID_D3DDebugObjectName, static_cast< UINT >( name.length( ) ), name.c_str( ) );
	}
#endif
}

inline void setDebugName( ID3D11DeviceChild* const pUnknown, const std::string& name )
{
#ifdef _DEBUG
	if( pUnknown )
	{
		pUnknown->SetPrivateData( WKPDID_D3DDebugObjectName, static_cast< UINT >( name.length( ) ), name.c_str( ) );
	}
#endif
}

inline void setDebugName( const ID3D11DeviceChildCPtr& pUnknown, const std::string& name )
{
#ifdef _DEBUG
	if( pUnknown )
	{
		pUnknown->SetPrivateData( WKPDID_D3DDebugObjectName, static_cast< UINT >( name.length( ) ), name.c_str( ) );
	}
#endif
}

inline void setDebugName( IDXGIObject* const pUnknown, const std::string& name )
{
#ifdef _DEBUG
	if( pUnknown )
	{
		pUnknown->SetPrivateData( WKPDID_D3DDebugObjectName, static_cast< UINT >( name.length( ) ), name.c_str( ) );
	}
#endif
}

inline void setDebugName( const IDXGIObjectCPtr& pUnknown, const std::string& name )
{
#ifdef _DEBUG
	if( pUnknown )
	{
		pUnknown->SetPrivateData( WKPDID_D3DDebugObjectName, static_cast< UINT >( name.length( ) ), name.c_str( ) );
	}
#endif
}
} // namespace directX11
} // namespace visual

#endif// _API_TYPE_DEF_DX11_H
