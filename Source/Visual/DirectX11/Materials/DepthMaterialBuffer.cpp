#include "DepthMaterialBuffer.h"

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : DepthMaterialBuffer.cpp
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pVSBuffer"> The vertex shader constants. </param>
	DepthMaterialBuffer::DepthMaterialBuffer( const BasicVSBufferSPtr& pVSBuffer )
		: MaterialBuffer( )
		, pVSBuffer( pVSBuffer )
	{ }

	/// <summary>
	/// Copy the changes made to the material
	/// so that it can be used in a shader to render.
	/// </summary>
	/// <param name="pContext"> The context to update the material with. </param>
	void DepthMaterialBuffer::update( const DeviceContextPtr_t& pContext )
	{
		pVSBuffer->update( pContext );
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the material with. </param>
	void DepthMaterialBuffer::set( const DeviceContextPtr_t& pContext )
	{
		pVSBuffer->set( pContext );
	}
} // namespace directX11
} // namespace visual
