#pragma once

// A wrapper for our shader constants.
//
// Project   : NaKama-Fx
// File Name : ColourMaterialBuffer.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _COLOUR_MATERIAL_BUFFER_DX11_H
#define _COLOUR_MATERIAL_BUFFER_DX11_H

#include "MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Manage setting shader resource view sets. </summary>
	class ColourMaterialBuffer: public MaterialBuffer
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pVSBuffer"> The vertex shader constants buffer. </param>
		/// <param name="pPSBuffer"> The pixel shader constants buffer. </param>
		/// <param name="pPSResources"> The pixel shader resources. </param>
		ColourMaterialBuffer( const BasicVSBufferSPtr& pVSBuffer,
							  const BasicPSBufferSPtr& pPSBuffer,
							  const PSResourcesSPtr& pPSResources );

		/// <summary> ctor </summary>
		ColourMaterialBuffer( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ColourMaterialBuffer( const ColourMaterialBuffer& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ColourMaterialBuffer( ColourMaterialBuffer&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~ColourMaterialBuffer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ColourMaterialBuffer& operator=( const ColourMaterialBuffer& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ColourMaterialBuffer& operator=( ColourMaterialBuffer&& move ) = default;


		/// <summary>
		/// Copy the changes made to the material
		/// so that it can be used in a shader to render.
		/// </summary>
		/// <param name="pContext"> The context to update the material with. </param>
		virtual void update( const DeviceContextPtr_t& pContext ) override;

		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The context to set the material with. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Set the model world matrix. </summary>
		/// <param name=""> The world matrix. </param>
		void setModelMatrix( const glm::mat4x4& matrix ) const;

		/// <summary> Set the ambiant reflectivity scaler. </summary>
		/// <param name="Ka"> The ambiant reflectivity scaler. </param>
		void setAmbientReflectivity( const glm::vec3& Ka ) const;

		/// <summary> Set the diffude reflectivity scaler. </summary>
		/// <param name="Kd"> The diffuse reflectivity scaler. </param>
		void setDiffuseReflectivity( const glm::vec3& Kd ) const;

		/// <summary> Set the specular reflectivity scaler. </summary>
		/// <param name="Ks"> The specular reflectivity scaler. </param>
		void setSpecularReflectivity( const glm::vec3& Ks ) const;

		/// <summary> Set the bump mapping scaler. </summary>
		/// <param name="scale"> The bump mapping scaler. </param>
		void setBumpScale( const glm::vec2& scale ) const;

		/// <summary> Set the surface roughness scaler. </summary>
		/// <param name=""> The surface roughness scaler. </param>
		void setRoughnessScale( float scale ) const;

		/// <summary> Set the surface metalness scaler. </summary>
		/// <param name=""> The surface metalness scaler. </param>
		void setMetalnessScale( float scale ) const;

		/// <summary> Set the subsurface angle power scaler. </summary>
		/// <param name=""> The the subsurface angle power. </param>
		void setSsPower( float power ) const;

		/// <summary> Set the subsurface scaler. </summary>
		/// <param name=""> The subsurface scaler. </param>
		void setSsScale( float scale ) const;

		/// <summary> Set the surface transparancy. </summary>
		/// <param name=""> The surface transparancy. </param>
		void setSAlpha( float alpha ) const;

		/// <summary> Set the subsurface transparancy. </summary>
		/// <param name=""> The subsurface transparancy. </param>
		void setSsAlpha( float alpha ) const;

		/// <summary> Set the materials light absorbtion. </summary>
		/// <param name=""> The . </param>
		void setAbsorption( float absorption ) const;

		/// <summary>
		/// Set the index of refaction (sort of) increased values
		/// increase the amount we offset the sampled back buffer.
		/// </summary>
		/// <param name=""> The index of refaction. </param>
		void setIor( float ior ) const;

		/// <summary>
		/// Set the bump mapping scaler that affects how the
		/// surface normal affects the subsurface scattering.
		/// </summary>
		/// <param name=""> The normal scaler. </param>
		void setSsBump( float ssBump ) const;

	private:
		/// <summary> The vertex shader constants buffer. </summary>
		BasicVSBufferSPtr pVSBuffer;

		/// <summary> The pixel shader constants buffer. </summary>
		BasicPSBufferSPtr pPSBuffer;

		/// <summary> The pixel shader resources. </summary>
		PSResourcesSPtr pPSResources;
	};
} // namespace directX11
} // namespace visual

#endif // _COLOUR_MATERIAL_BUFFER_DX11_H

