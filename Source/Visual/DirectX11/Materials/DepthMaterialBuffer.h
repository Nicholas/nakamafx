#pragma once

// A wrapper for our shader constants.
//
// Project   : NaKama-Fx
// File Name : DepthMaterialBuffer.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _DEPTH_MATERIAL_BUFFER_DX11_H
#define _DEPTH_MATERIAL_BUFFER_DX11_H

#include "MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Manage setting shader resource view sets. </summary>
	class DepthMaterialBuffer: public MaterialBuffer
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pVSBuffer"> The vertex shader constants. </param>
		explicit DepthMaterialBuffer( const BasicVSBufferSPtr& pVSBuffer );

		/// <summary> ctor </summary>
		DepthMaterialBuffer( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		DepthMaterialBuffer( const DepthMaterialBuffer& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		DepthMaterialBuffer( DepthMaterialBuffer&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~DepthMaterialBuffer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		DepthMaterialBuffer& operator=( const DepthMaterialBuffer& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		DepthMaterialBuffer& operator=( DepthMaterialBuffer&& move ) = default;


		/// <summary>
		/// Copy the changes made to the material
		/// so that it can be used in a shader to render.
		/// </summary>
		/// <param name="pContext"> The context to update the material with. </param>
		virtual void update( const DeviceContextPtr_t& pContext ) override;

		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The context to set the material with. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

	private:
		/// <summary> The vertex shader constants buffer. </summary>
		BasicVSBufferSPtr pVSBuffer;
	};
} // namespace directX11
} // namespace visual

#endif // _DEPTH_MATERIAL_BUFFER_DX11_H

