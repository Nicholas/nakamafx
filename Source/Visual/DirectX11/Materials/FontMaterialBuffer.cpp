#include "FontMaterialBuffer.h"

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : FontMaterialBuffer.cpp
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	using glm::vec2;
	using glm::vec3;
	using glm::vec4;
	using glm::uvec4;
	using glm::mat4x4;

	/// <summary> ctor </summary>
	/// <param name="pVSBuffer"> The vertex shader constants buffer. </param>
	/// <param name="pPSBuffer"> The pixel shader constants buffer. </param>
	/// <param name="pPSResources"> The pixel shader resources. </param>
	FontMaterialBuffer::FontMaterialBuffer( const BasicVSBufferSPtr& pVSBuffer,
											const FontPSBufferSPtr& pPSBuffer,
											const PSResourcesSPtr& pPSResources )
		: MaterialBuffer( )
		, pVSBuffer( pVSBuffer )
		, pPSBuffer( pPSBuffer )
		, pPSResources( pPSResources )
	{
		BasicVSBuffer::Constants& vsConstants = pVSBuffer->get( );
		FontPSBuffer::Constants& psConstants = pPSBuffer->get( );

		vsConstants.model =
		{
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		psConstants.ka = { 1.0, 1.0, 1.0 };
		psConstants.kd = { 1.0, 1.0, 1.0 };
		psConstants.ks = { 1.0, 1.0, 1.0 };

		psConstants.roughness = 1.0f;
		psConstants.metalness = 1.0f;

		psConstants.bumpScale = vec2( 1.0f, 1.0f );

		psConstants.ssBump = 0;
		psConstants.ssPower = 4;
		psConstants.ssScale = 1.0;

		psConstants.refraction = 1.0;
		psConstants.sAlpha = 1.0;
		psConstants.ssAlpha = 1.0;
		psConstants.absorption = 1.0;

		psConstants.properties = vec4( 0, 0, 0, 0 );
	}

	/// <summary>
	/// Copy the changes made to the material
	/// so that it can be used in a shader to render.
	/// </summary>
	/// <param name="pContext"> The context to update the material with. </param>
	void FontMaterialBuffer::update( const DeviceContextPtr_t& pContext )
	{
		pVSBuffer->update( pContext );
		pPSBuffer->update( pContext );
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the material with. </param>
	void FontMaterialBuffer::set( const DeviceContextPtr_t& pContext )
	{
		pVSBuffer->set( pContext );
		pPSBuffer->set( pContext );
		pPSResources->set( pContext, 0 );
	}

	/// <summary> Set the model world matrix. </summary>
	/// <param name=""> The world matrix. </param>
	void FontMaterialBuffer::setModelMatrix( const mat4x4& matrix ) const
	{
		pVSBuffer->get( ).model = matrix;
	}

	/// <summary> Set the ambiant reflectivity scaler. </summary>
	/// <param name="Ka"> The ambiant reflectivity scaler. </param>
	void FontMaterialBuffer::setAmbientReflectivity( const vec3& Ka ) const
	{
		pPSBuffer->get( ).ka = Ka;
	}

	/// <summary> Set the diffude reflectivity scaler. </summary>
	/// <param name="Kd"> The diffuse reflectivity scaler. </param>
	void FontMaterialBuffer::setDiffuseReflectivity( const vec3& Kd ) const
	{
		pPSBuffer->get( ).kd = Kd;
	}

	/// <summary> Set the specular reflectivity scaler. </summary>
	/// <param name="Ks"> The specular reflectivity scaler. </param>
	void FontMaterialBuffer::setSpecularReflectivity( const vec3& Ks ) const
	{
		pPSBuffer->get( ).ks = Ks;
	}

	/// <summary> Set the bump mapping scaler. </summary>
	/// <param name="scale"> The bump mapping scaler. </param>
	void FontMaterialBuffer::setBumpScale( const vec2& scale ) const
	{
		pPSBuffer->get( ).bumpScale = scale;
	}

	/// <summary> Set the surface roughness scaler. </summary>
	/// <param name=""> The surface roughness scaler. </param>
	void FontMaterialBuffer::setRoughnessScale( const float scale ) const
	{
		pPSBuffer->get( ).roughness = scale;
	}

	/// <summary> Set the surface metalness scaler. </summary>
	/// <param name=""> The surface metalness scaler. </param>
	void FontMaterialBuffer::setMetalnessScale( const float scale ) const
	{
		pPSBuffer->get( ).metalness = scale;
	}

	/// <summary> Set the subsurface angle power scaler. </summary>
	/// <param name=""> The the subsurface angle power. </param>
	void FontMaterialBuffer::setSsPower( const float power ) const
	{
		pPSBuffer->get( ).ssPower = power;
	}

	/// <summary> Set the subsurface scaler. </summary>
	/// <param name=""> The subsurface scaler. </param>
	void FontMaterialBuffer::setSsScale( const float scale ) const
	{
		pPSBuffer->get( ).ssScale = scale;
	}

	/// <summary> Set the surface transparancy. </summary>
	/// <param name=""> The surface transparancy. </param>
	void FontMaterialBuffer::setSAlpha( const float alpha ) const
	{
		pPSBuffer->get( ).sAlpha = alpha;
	}

	/// <summary> Set the subsurface transparancy. </summary>
	/// <param name=""> The subsurface transparancy. </param>
	void FontMaterialBuffer::setSsAlpha( const float alpha ) const
	{
		pPSBuffer->get( ).ssAlpha = alpha;
	}

	/// <summary> Set the materials light absorbtion. </summary>
	/// <param name=""> The . </param>
	void FontMaterialBuffer::setAbsorption( const float absorption ) const
	{
		pPSBuffer->get( ).absorption = absorption;
	}

	/// <summary>
	/// Set the index of refaction (sort of) increased values
	/// increase the amount we offset the sampled back buffer.
	/// </summary>
	/// <param name=""> The index of refaction. </param>
	void FontMaterialBuffer::setIor( const float ior ) const
	{
		pPSBuffer->get( ).refraction = ior;
	}

	/// <summary>
	/// Set the bump mapping scaler that affects how the
	/// surface normal affects the subsurface scattering.
	/// </summary>
	/// <param name=""> The normal scaler. </param>
	void FontMaterialBuffer::setSsBump( const float ssBump ) const
	{
		pPSBuffer->get( ).ssBump = ssBump;
	}

	/// <summary> Set the font rendering properties. </summary>
	/// <param name="properties"> Data properties so that we can make all sorts of fonts. </param>
	void FontMaterialBuffer::setProperties( const uvec4& properties ) const
	{
		pPSBuffer->get( ).properties = properties;
	}
} // namespace directX11
} // namespace visual
