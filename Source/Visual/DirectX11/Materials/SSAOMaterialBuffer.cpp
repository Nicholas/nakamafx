#include "SSAOMaterialBuffer.h"

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : SSAOMaterialBuffer.cpp
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPSBuffer"> The pixel shader constants buffer. </param>
	/// <param name="pPSResources"> The pixel shader resources. </param>
	SSAOShaderBuffer::SSAOShaderBuffer( const SSAOPSBufferSPtr& pPSBuffer,
										const PSResourcesSPtr& pPSResources )
		: MaterialBuffer( )
		, pPSBuffer( pPSBuffer )
		, pPSResources( pPSResources )
	{ }

	/// <summary>
	/// Copy the changes made to the material
	/// so that it can be used in a shader to render.
	/// </summary>
	/// <param name="pContext"> The context to update the material with. </param>
	void SSAOShaderBuffer::update( const DeviceContextPtr_t& pContext )
	{
		pPSBuffer->update( pContext );
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the material with. </param>
	void SSAOShaderBuffer::set( const DeviceContextPtr_t& pContext )
	{
		pPSBuffer->set( pContext );
		pPSResources->set( pContext, 0 );
	}

	/// <summary> Set the size the sampled texture used. </summary>
	/// <param name="size"> Size of the sampled texture. </param>
	void SSAOShaderBuffer::setSize( const glm::vec2& size ) const
	{
		pPSBuffer->get( ).targetSize.x = size.x;
		pPSBuffer->get( ).targetSize.y = size.y;
		pPSBuffer->get( ).targetSize.z = 1 / size.x;
		pPSBuffer->get( ).targetSize.w = 1 / size.y;
	}

	/// <summary> Set the view matrix so that we can trasform the samples. </summary>
	/// <param name="pContext"> The camera view matrix. </param>
	void SSAOShaderBuffer::setView( const glm::mat4x4& view ) const
	{
		pPSBuffer->get( ).view = view;
	}

	/// <summary> Set the projection matrix so that we can trasform the samples for depth testing. </summary>
	/// <param name="pContext"> The camera projection matrix. </param>
	void SSAOShaderBuffer::setProjection( const glm::mat4x4& projection ) const
	{
		pPSBuffer->get( ).projection = projection;
	}
} // namespace directX11
} // namespace visual
