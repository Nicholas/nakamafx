#pragma once

// A wrapper for our shader constants.
//
// Project   : NaKama-Fx
// File Name : MaterialBuffer.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _MATERIAL_BUFFER_DX11_H
#define _MATERIAL_BUFFER_DX11_H

#include "../APITypeDefs.h"
#include "Visual/FX/Types/ConstantBuffers.h"
#include "Visual/DirectX11/Types/ConstantBuffer.h"

#include "Visual/DirectX11/Types/ShaderResources.h"

#include <Macros.h>

#define DECLARE_CONSTANT_BUFFER( SHADER_TYPE, BASE_TYPE, NAME ) \
	typedef ConstantBuffer< fx:: BASE_TYPE, PPCAT( SHADER_TYPE, SetConstant ) > NAME; \
	SHARED_PTR_TYPE_DEF( NAME )

#define DECLARE_SHADER_DATA( SHADER_TYPE, BASE_TYPE, NAME ) \
	typedef TextureShaderBuffer< fx:: BASE_TYPE, PPCAT( SHADER_TYPE, SetConstant ), PPCAT( SHADER_TYPE, SetResources ) > NAME; \
	SHARED_PTR_TYPE_DEF( NAME )

namespace visual
{
namespace directX11
{
	// TODO: Refactor the entire handling of models, tetures,andconstants.
	//template< class TYPE, typename GPU_CONSTANT_SETTER, typename GPU_RESOURCE_SETTER >
	//class TextureShaderBuffer : public ConstantBuffer< TYPE, GPU_CONSTANT_SETTER >
	//						  , public ShaderResources< GPU_RESOURCE_SETTER >
	//{
	//public:
	//	TextureShaderBuffer(
	//		UINT startSlot, const ID3D11BufferSPtr& pBuffer,
	//		const std::vector< ID3D11ShaderResourceViewSPtr >& textures
	//	)
	//		: ConstantBuffer< TYPE, GPU_CONSTANT_SETTER >( startSlot, pBuffer )
	//		, ShaderResources< GPU_RESOURCE_SETTER >( textures )
	//	{
	//	}
	//	virtual ~TextureShaderBuffer( ) = default;
	//
	//	// Set the buffers as active on the GPU so that
	//	// they can be used for the next draw call.
	//	// @param const ContextSPtr& pContext: The context to set the target too.
	//	virtual void set( const DeviceContextPtr_t& pContext ) override
	//	{
	//		ConstantBuffer< TYPE, GPU_CONSTANT_SETTER >::set( pContext );
	//		ShaderResources< GPU_RESOURCE_SETTER >::set( pContext, 0 );
	//	}
	//};



	// Vertex Constant Buffers
	DECLARE_CONSTANT_BUFFER( VS,  VSDepthBuffer, DepthVSBuffer )
	DECLARE_CONSTANT_BUFFER( VS,  VSSceneBuffer, SceneVSBuffer )
	DECLARE_CONSTANT_BUFFER( VS, VSObjectBuffer, BasicVSBuffer )
	DECLARE_CONSTANT_BUFFER( VS,   VSBlurBuffer, BlurVSBuffer  )
	DECLARE_CONSTANT_BUFFER( VS,     VSUIBuffer, UIVSBuffer  )

	// Geometry Constant Buffers
	DECLARE_CONSTANT_BUFFER( GS,    GSFrameBuffer, FrameGSBuffer    )
	DECLARE_CONSTANT_BUFFER( GS, GSParticleBuffer, ParticleGSBuffer )
	DECLARE_CONSTANT_BUFFER( GS,    GSSceneBuffer, SceneGSBuffer    )

	// Pixel Constant Buffers
	DECLARE_CONSTANT_BUFFER( PS,        PSBlurBuffer, BlurPSBuffer           )
	DECLARE_CONSTANT_BUFFER( PS,      PSPostFXBuffer, PostFXPSBuffer         )
	DECLARE_CONSTANT_BUFFER( PS,    PSLightingBuffer, LightingPSBuffer       )
	DECLARE_CONSTANT_BUFFER( PS,    PSABufferResolve, ABufferResolvePSBuffer )
	DECLARE_CONSTANT_BUFFER( PS,   PSSunVolumeBuffer, SunVolumePSBuffer      )
	DECLARE_CONSTANT_BUFFER( PS,    PSParticleBuffer, ParticlePSBuffer       )
	DECLARE_CONSTANT_BUFFER( PS,      PSObjectBuffer, BasicPSBuffer          )
	DECLARE_CONSTANT_BUFFER( PS,        PSSSAOBuffer, SSAOPSBuffer           )
	DECLARE_CONSTANT_BUFFER( PS,        PSFontBuffer, FontPSBuffer           )
	DECLARE_CONSTANT_BUFFER( PS, PSToneMappingBuffer, ToneMappingPSBuffer    )

	// Compute Shader Buffers
	DECLARE_CONSTANT_BUFFER( CS, CSHistogramBuffer  , HistogramCSBuffer  )
	DECLARE_CONSTANT_BUFFER( CS, CSAdaptationBuffer , AdaptationCSBuffer )
	DECLARE_CONSTANT_BUFFER( CS, CSClusteredLighting, ClusteredLightingCSBuffer )



	/// <summary> Manage setting shader resource view sets. </summary>
	class MaterialBuffer
	{
	public:
		/// <summary> ctor </summary>
		MaterialBuffer( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		MaterialBuffer( const MaterialBuffer& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		MaterialBuffer( MaterialBuffer&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~MaterialBuffer( ) = 0;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		MaterialBuffer& operator=( const MaterialBuffer& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		MaterialBuffer& operator=( MaterialBuffer&& move ) = default;


		/// <summary>
		/// Copy the changes made to the material
		/// so that it can be used in a shader to render.
		/// </summary>
		/// <param name="pContext"> The context to update the material with. </param>
		virtual void update( const DeviceContextPtr_t& pContext ) = 0;

		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The context to set the material with. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) = 0;
	};

	inline MaterialBuffer::~MaterialBuffer( ) = default;
} // namespace directX11
} // namespace visual

#endif // _MATERIAL_BUFFER_DX11_H
