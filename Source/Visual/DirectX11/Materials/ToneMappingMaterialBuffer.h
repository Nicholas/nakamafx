#pragma once

// A wrapper for our shader constants.
//
// Project   : NaKama-Fx
// File Name : ToneMappingMaterialBuffer.h
// Date      : 05/08/2019
// Author    : Nicholas Welters

#ifndef _TONE_MAPPING_MATERIAL_BUFFER_DX11_H
#define _TONE_MAPPING_MATERIAL_BUFFER_DX11_H

#include "MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Manage setting shader resource view sets. </summary>
	class ToneMappingPixelShaderBuffer: public MaterialBuffer
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPSBuffer"> The pixel shader constants buffer. </param>
		/// <param name="pPSResources"> The pixel shader resources. </param>
		ToneMappingPixelShaderBuffer( const ToneMappingPSBufferSPtr& pPSBuffer,
									  const PSResourcesSPtr& pPSResources );

		/// <summary> ctor </summary>
		ToneMappingPixelShaderBuffer( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		ToneMappingPixelShaderBuffer( const ToneMappingPixelShaderBuffer& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		ToneMappingPixelShaderBuffer( ToneMappingPixelShaderBuffer&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~ToneMappingPixelShaderBuffer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		ToneMappingPixelShaderBuffer& operator=( const ToneMappingPixelShaderBuffer& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		ToneMappingPixelShaderBuffer& operator=( ToneMappingPixelShaderBuffer&& move ) = default;


		/// <summary>
		/// Copy the changes made to the material
		/// so that it can be used in a shader to render.
		/// </summary>
		/// <param name="pContext"> The context to update the material with. </param>
		virtual void update( const DeviceContextPtr_t& pContext ) override;

		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The context to set the material with. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Set the size the sampled texture used. </summary>
		/// <param name="size"> Size of the sampled texture. </param>
		void setSize( const glm::vec2& size ) const;

		void setShoulderStrength( float strength );

		void setLinearStrength( float strength );
		void setLinearAngle( float angle );
		void setLinearWhite( float white );

		void setToeStrength( float strength );
		void setToeNumerator( float numerator );
		void setToeDenominator( float denominator );

		void setMode( int mode );


		glm::vec2 getSize( );

		float getShoulderStrength( );

		float getLinearStrength( );
		float getLinearAngle( );
		float getLinearWhite( );

		float getToeStrength( );
		float getToeNumerator( );
		float getToeDenominator( );

		int getMode( );

	private:
		/// <summary> The Tone Mapping pixel shader constants. </summary>
		ToneMappingPSBufferSPtr pPSBuffer;

		/// <summary> The Tone Mapping pixel shader resources. </summary>
		PSResourcesSPtr pPSResources;
	};

	SHARED_PTR_TYPE_DEF( ToneMappingPixelShaderBuffer )
} // namespace directX11
} // namespace visual

#endif // _ToneMapping_MATERIAL_BUFFER_DX11_H
