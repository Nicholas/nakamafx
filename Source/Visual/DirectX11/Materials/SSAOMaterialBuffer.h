#pragma once

// A wrapper for our shader constants.
//
// Project   : NaKama-Fx
// File Name : SSAOMaterialBuffer.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _SSAO_MATERIAL_BUFFER_DX11_H
#define _SSAO_MATERIAL_BUFFER_DX11_H

#include "MaterialBuffer.h"

namespace visual
{
namespace directX11
{
	/// <summary> Manage setting shader resource view sets. </summary>
	class SSAOShaderBuffer: public MaterialBuffer
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="pPSBuffer"> The pixel shader constants buffer. </param>
		/// <param name="pPSResources"> The pixel shader resources. </param>
		explicit SSAOShaderBuffer( const SSAOPSBufferSPtr& pPSBuffer,
								   const PSResourcesSPtr& pPSResources );

		/// <summary> ctor </summary>
		SSAOShaderBuffer( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		SSAOShaderBuffer( const SSAOShaderBuffer& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		SSAOShaderBuffer( SSAOShaderBuffer&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~SSAOShaderBuffer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		SSAOShaderBuffer& operator=( const SSAOShaderBuffer& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		SSAOShaderBuffer& operator=( SSAOShaderBuffer&& move ) = default;


		/// <summary>
		/// Copy the changes made to the material
		/// so that it can be used in a shader to render.
		/// </summary>
		/// <param name="pContext"> The context to update the material with. </param>
		virtual void update( const DeviceContextPtr_t& pContext ) override;

		/// <summary>
		/// Set the buffers as active on the GPU so that
		/// they can be used for the next draw call.
		/// </summary>
		/// <param name="pContext"> The context to set the material with. </param>
		virtual void set( const DeviceContextPtr_t& pContext ) override;

		/// <summary> Set the size the sampled texture used. </summary>
		/// <param name="size"> Size of the sampled texture. </param>
		void setSize( const glm::vec2& size ) const;

		/// <summary> Set the projection matrix so that we can trasform the samples. </summary>
		/// <param name="pContext"> The camera projection matrix. </param>
		void setProjection( const glm::mat4x4& projection ) const;

		/// <summary> Set the view matrix so that we can trasform the samples. </summary>
		/// <param name="pContext"> The camera view matrix. </param>
		void setView( const glm::mat4x4& view ) const;

	private:
		/// <summary> The SSAO pixel shader constants. </summary>
		SSAOPSBufferSPtr pPSBuffer;

		/// <summary> The SSAO pixel shader resources. </summary>
		PSResourcesSPtr pPSResources;
	};

	SHARED_PTR_TYPE_DEF( SSAOShaderBuffer )
} // namespace directX11
} // namespace visual

#endif // _SSAO_MATERIAL_BUFFER_DX11_H
