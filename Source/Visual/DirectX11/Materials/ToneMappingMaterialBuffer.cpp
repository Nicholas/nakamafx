#include "ToneMappingMaterialBuffer.h"

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : ToneMappingMaterialBuffer.cpp
// Date      : 05/08/2019
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pPSBuffer"> The pixel shader constants buffer. </param>
	/// <param name="pPSResources"> The pixel shader resources. </param>
	ToneMappingPixelShaderBuffer::ToneMappingPixelShaderBuffer( const ToneMappingPSBufferSPtr& pPSBuffer,
													  const PSResourcesSPtr& pPSResources )
		: MaterialBuffer( )
		, pPSBuffer( pPSBuffer )
		, pPSResources( pPSResources )
	{ }

	/// <summary>
	/// Copy the changes made to the material
	/// so that it can be used in a shader to render.
	/// </summary>
	/// <param name="pContext"> The context to update the material with. </param>
	void ToneMappingPixelShaderBuffer::update( const DeviceContextPtr_t& pContext )
	{
		pPSBuffer->update( pContext );
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the material with. </param>
	void ToneMappingPixelShaderBuffer::set( const DeviceContextPtr_t& pContext )
	{
		pPSBuffer->set( pContext );
		pPSResources->set( pContext, 0 );
	}

	/// <summary> Set the size the sampled texture used. </summary>
	/// <param name="size"> Size of the sampled texture. </param>
	void ToneMappingPixelShaderBuffer::setSize( const glm::vec2& size ) const
	{
		pPSBuffer->get( ).imageSize.x = size.x;
		pPSBuffer->get( ).imageSize.y = size.y;
		pPSBuffer->get( ).invImageSize.x = 1 / size.x;
		pPSBuffer->get( ).invImageSize.y = 1 / size.y;
	}

	void ToneMappingPixelShaderBuffer::setShoulderStrength( float strength )
	{
		pPSBuffer->get( ).shoulderStrength = strength;
	}

	void ToneMappingPixelShaderBuffer::setLinearStrength( float strength )
	{
		pPSBuffer->get( ).linearStrength = strength;
	}

	void ToneMappingPixelShaderBuffer::setLinearAngle( float angle )
	{
		pPSBuffer->get( ).linearAngle = angle;
	}

	void ToneMappingPixelShaderBuffer::setLinearWhite( float white )
	{
		pPSBuffer->get( ).linearWhite = white;
	}

	void ToneMappingPixelShaderBuffer::setToeStrength( float strength )
	{
		pPSBuffer->get( ).toeStrength = strength;
	}

	void ToneMappingPixelShaderBuffer::setToeNumerator( float numerator )
	{
		pPSBuffer->get( ).toeNumerator = numerator;
	}

	void ToneMappingPixelShaderBuffer::setToeDenominator( float denominator )
	{
		pPSBuffer->get( ).toeDenominator = denominator;
	}

	void ToneMappingPixelShaderBuffer::setMode( int mode )
	{
		pPSBuffer->get( ).mode = mode;
	}

	glm::vec2 ToneMappingPixelShaderBuffer::getSize( )
	{
		return pPSBuffer->get( ).imageSize;
	}

	float ToneMappingPixelShaderBuffer::getShoulderStrength( )
	{
		return pPSBuffer->get( ).shoulderStrength;
	}

	float ToneMappingPixelShaderBuffer::getLinearStrength( )
	{
		return pPSBuffer->get( ).linearStrength;
	}

	float ToneMappingPixelShaderBuffer::getLinearAngle( )
	{
		return pPSBuffer->get( ).linearAngle;
	}

	float ToneMappingPixelShaderBuffer::getLinearWhite( )
	{
		return pPSBuffer->get( ).linearWhite;
	}

	float ToneMappingPixelShaderBuffer::getToeStrength( )
	{
		return pPSBuffer->get( ).toeStrength;
	}

	float ToneMappingPixelShaderBuffer::getToeNumerator( )
	{
		return pPSBuffer->get( ).toeNumerator;
	}

	float ToneMappingPixelShaderBuffer::getToeDenominator( )
	{
		return pPSBuffer->get( ).toeDenominator;
	}

	int ToneMappingPixelShaderBuffer::getMode( )
	{
		return pPSBuffer->get( ).mode;
	}
} // namespace directX11
} // namespace visual
