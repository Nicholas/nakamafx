#include "ColourMaterialBuffer.h"

// Manage setting shader resource view sets.
//
// Project   : NaKama-Fx
// File Name : ColourMaterialBuffer.cpp
// Date      : 08/06/2015
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="pVSBuffer"> The vertex shader constants buffer. </param>
	/// <param name="pPSBuffer"> The pixel shader constants buffer. </param>
	/// <param name="pPSResources"> The pixel shader resources. </param>
	ColourMaterialBuffer::ColourMaterialBuffer( const BasicVSBufferSPtr& pVSBuffer,
												const BasicPSBufferSPtr& pPSBuffer,
												const PSResourcesSPtr& pPSResources )
		: MaterialBuffer( )
		, pVSBuffer( pVSBuffer )
		, pPSBuffer( pPSBuffer )
		, pPSResources( pPSResources )
	{
		BasicVSBuffer::Constants& vsConstants = pVSBuffer->get( );
		BasicPSBuffer::Constants& psConstants = pPSBuffer->get( );

		vsConstants.model =
		{
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		psConstants.ka = { 1.0, 1.0, 1.0 };
		psConstants.kd = { 1.0, 1.0, 1.0 };
		psConstants.ks = { 1.0, 1.0, 1.0 };

		psConstants.roughness = 1.0f;
		psConstants.metalness = 1.0f;

		psConstants.bumpScale = glm::vec2( 1.0f, 1.0f );

		psConstants.ssBump = 0;
		psConstants.ssPower = 4;
		psConstants.ssScale = 1.0;

		psConstants.ior = 1.0;
		psConstants.sAlpha = 1.0;
		psConstants.ssAlpha = 1.0;
		psConstants.absorption = 1.0;
	}

	/// <summary>
	/// Copy the changes made to the material
	/// so that it can be used in a shader to render.
	/// </summary>
	/// <param name="pContext"> The context to update the material with. </param>
	void ColourMaterialBuffer::update( const DeviceContextPtr_t& pContext )
	{
		pVSBuffer->update( pContext );
		pPSBuffer->update( pContext );
	}

	/// <summary>
	/// Set the buffers as active on the GPU so that
	/// they can be used for the next draw call.
	/// </summary>
	/// <param name="pContext"> The context to set the material with. </param>
	void ColourMaterialBuffer::set( const DeviceContextPtr_t& pContext )
	{
		pVSBuffer->set( pContext );
		pPSBuffer->set( pContext );
		pPSResources->set( pContext, 0 );
	}

	/// <summary> Set the model world matrix. </summary>
	/// <param name=""> The world matrix. </param>
	void ColourMaterialBuffer::setModelMatrix( const glm::mat4x4& matrix ) const
	{
		pVSBuffer->get( ).model = matrix;
	}

	/// <summary> Set the ambiant reflectivity scaler. </summary>
	/// <param name="Ka"> The ambiant reflectivity scaler. </param>
	void ColourMaterialBuffer::setAmbientReflectivity( const glm::vec3& Ka ) const
	{
		pPSBuffer->get( ).ka = Ka;
	}

	/// <summary> Set the diffude reflectivity scaler. </summary>
	/// <param name="Kd"> The diffuse reflectivity scaler. </param>
	void ColourMaterialBuffer::setDiffuseReflectivity( const glm::vec3& Kd ) const
	{
		pPSBuffer->get( ).kd = Kd;
	}

	/// <summary> Set the specular reflectivity scaler. </summary>
	/// <param name="Ks"> The specular reflectivity scaler. </param>
	void ColourMaterialBuffer::setSpecularReflectivity( const glm::vec3& Ks ) const
	{
		pPSBuffer->get( ).ks = Ks;
	}

	/// <summary> Set the bump mapping scaler. </summary>
	/// <param name="scale"> The bump mapping scaler. </param>
	void ColourMaterialBuffer::setBumpScale( const glm::vec2& scale ) const
	{
		pPSBuffer->get( ).bumpScale = scale;
	}

	/// <summary> Set the surface roughness scaler. </summary>
	/// <param name=""> The surface roughness scaler. </param>
	void ColourMaterialBuffer::setRoughnessScale( const float scale ) const
	{
		pPSBuffer->get( ).roughness = scale;
	}

	/// <summary> Set the surface metalness scaler. </summary>
	/// <param name=""> The surface metalness scaler. </param>
	void ColourMaterialBuffer::setMetalnessScale( const float scale ) const
	{
		pPSBuffer->get( ).metalness = scale;
	}

	/// <summary> Set the subsurface angle power scaler. </summary>
	/// <param name=""> The the subsurface angle power. </param>
	void ColourMaterialBuffer::setSsPower( const float power ) const
	{
		pPSBuffer->get( ).ssPower = power;
	}

	/// <summary> Set the subsurface scaler. </summary>
	/// <param name=""> The subsurface scaler. </param>
	void ColourMaterialBuffer::setSsScale( const float scale ) const
	{
		pPSBuffer->get( ).ssScale = scale;
	}

	/// <summary> Set the surface transparancy. </summary>
	/// <param name=""> The surface transparancy. </param>
	void ColourMaterialBuffer::setSAlpha( const float alpha ) const
	{
		pPSBuffer->get( ).sAlpha = alpha;
	}

	/// <summary> Set the subsurface transparancy. </summary>
	/// <param name=""> The subsurface transparancy. </param>
	void ColourMaterialBuffer::setSsAlpha( const float alpha ) const
	{
		pPSBuffer->get( ).ssAlpha = alpha;
	}

	/// <summary> Set the materials light absorbtion. </summary>
	/// <param name=""> The . </param>
	void ColourMaterialBuffer::setAbsorption( const float absorption ) const
	{
		pPSBuffer->get( ).absorption = absorption;
	}

	/// <summary>
	/// Set the index of refaction (sort of) increased values
	/// increase the amount we offset the sampled back buffer.
	/// </summary>
	/// <param name=""> The index of refaction. </param>
	void ColourMaterialBuffer::setIor( const float ior ) const
	{
		pPSBuffer->get( ).ior = ior;
	}

	/// <summary>
	/// Set the bump mapping scaler that affects how the
	/// surface normal affects the subsurface scattering.
	/// </summary>
	/// <param name=""> The normal scaler. </param>
	void ColourMaterialBuffer::setSsBump( const float ssBump ) const
	{
		pPSBuffer->get( ).ssBump = ssBump;
	}
} // namespace directX11
} // namespace visual
