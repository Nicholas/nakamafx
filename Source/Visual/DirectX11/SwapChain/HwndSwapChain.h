#pragma once
// DirectX 11 HWND Swap Chain.
//
// This is a wrapper around the device swap chain to fit the general display framework so that
// we can test rendering with out having to have any of the other systems.
//
// Project   : NaKama-Fx
// File Name : HwndSwapChain.h
// Date      : 21/12/2016
// Author    : Nicholas Welters

#ifndef _HWND_SWAP_CHAIN_DX11_H
#define _HWND_SWAP_CHAIN_DX11_H

#include "Visual/FX/SwapChain.h"
#include "../APITypeDefs.h"

#include <memory>

namespace visual
{
namespace directX11
{
	FORWARD_DECLARE( OutputTarget );

	class HwndSwapChain: public fx::SwapChain
	{
	public:
		/// <summary> ctor </summary>
		/// <param name="isFullscreen"> The swapchain fullscreen state. </param>
		/// <param name="pOutputTarget"> The render target that is sused to draw to the back buffer. </param>
		/// <param name="pSwapChain"> The dxgi swap chain so that we can control the presentation of the back buffer. </param>
		HwndSwapChain(
			bool isFullscreen,
			const OutputTargetSPtr& pOutputTarget,
			const SwapChainPtr_t& pSwapChain );

		/// <summary> ctor </summary>
		HwndSwapChain( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		HwndSwapChain( const HwndSwapChain& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		HwndSwapChain( HwndSwapChain&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~HwndSwapChain( );


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		HwndSwapChain& operator=( const HwndSwapChain& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		HwndSwapChain& operator=( HwndSwapChain&& move ) = default;


		/// <summary> Get the swapchains output target that we can draw to hardware swapchain </summary>
		/// <returns> The output target that we can use to draw to the swap chain with. </returns>
		OutputTargetSPtr getOutputTarget( ) const;

		/// <summary> All we do here is forward our call to present to the swap chain. </summary>
		virtual void present( ) final override;

	private:
		/// <summary> The swapchain fullscreen state. </summary>
		bool isFullscreen = false;

		/// <summary> The render target that is sused to draw to the back buffer. </summary>
		OutputTargetSPtr pOutputTarget = nullptr;

		/// <summary> The dxgi swap chain so that we can control the presentation of the back buffer. </summary>
		SwapChainPtr_t pSwapChain = nullptr;
	};
} // namespace directX11
} // namespace visual

#endif // _HWND_SWAP_CHAIN_DX11_H
