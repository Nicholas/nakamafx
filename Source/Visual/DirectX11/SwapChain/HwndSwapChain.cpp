#include "HwndSwapChain.h"
// DirectX 11 HWND Swap Chain.
//
// This is a wrapper around the device swap chain to fit the general display framework so that
// we can test rendering with out having to have any of the other systems.
//
// Project   : NaKama-Fx
// File Name : HwndSwapChain.cpp
// Date      : 21/12/2016
// Author    : Nicholas Welters

namespace visual
{
namespace directX11
{
	/// <summary> ctor </summary>
	/// <param name="isFullscreen"> The swapchain fullscreen state. </param>
	/// <param name="pOutputTarget"> The render target that is sused to draw to the back buffer. </param>
	/// <param name="pSwapChain"> The dxgi swap chain so that we can control the presentation of the back buffer. </param>
	HwndSwapChain::HwndSwapChain(
		const bool isFullscreen,
		const OutputTargetSPtr& pOutputTarget,
		const SwapChainPtr_t& pSwapChain )
		: SwapChain( )
		, isFullscreen( isFullscreen )
		, pOutputTarget( pOutputTarget )
		, pSwapChain( pSwapChain )
	{ }

	/// <summary> dtor </summary>
	HwndSwapChain::~HwndSwapChain( )
	{
		if( isFullscreen )
		{
			pSwapChain->SetFullscreenState( false, nullptr );
		}
	}

	/// <summary> Get the swapchains output target that we can draw to hardware swapchain </summary>
	/// <returns> The output target that we can use to draw to the swap chain with. </returns>
	OutputTargetSPtr HwndSwapChain::getOutputTarget( ) const
	{
		return pOutputTarget;
	}

	/// <summary> All we do here is forward our call to present to the swap chain. </summary>
	void HwndSwapChain::present( )
	{
		const unsigned int syncInterval = 0; // 1 enable vysnc/vblank each frame.
		const unsigned int flags = 0;

		if( FAILED( pSwapChain->Present( syncInterval, flags ) ) )
		{
			//std::cout << "Call to Present failed!\n";
			throw std::runtime_error( "Call to Present failed!\n" );
		}
	}
} // namespace directX11
} // namespace visual
