#pragma once

// FX Factory
//
// A base factory that is used to start of the construction of the basic system.
//
// Project   : NaKama-Fx
// File Name : FXFactory.h
// Date      : 03/06/2018
// Author    : Nicholas Welters

#ifndef _FX_Factory_H
#define _FX_Factory_H

#include <Macros.h>
#include <Processing/FunctorProcess.h>
#include <Processing/BalancerDispatchPolicy.h>
#include <Processing/OnRunPolicy.h>
#include <Processing/ProcessExecutor.h>
#include <Processing/Scheduling/ObservedPolicies.h>
#include <Processing/Scheduling/ScheduledFunctorProcess.h>
#include <Processing/Scheduling/EnqueuingObserver.h>


FORWARD_DECLARE( ScheduledFunctor );
namespace tools
{
	SHARED_PTR_TYPE_DEF( FunctorProcess );
}

namespace visual
{
namespace ui
{
	FORWARD_DECLARE( Input );
	FORWARD_DECLARE( WindowChange );
#if defined( __linux__ ) || defined( VK_USE_PLATFORM_XCB_KHR )
	typedef std::shared_ptr< class WindowXCB > WindowSPtr;
#elif defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
	typedef std::shared_ptr< class WindowMS > WindowSPtr;
#endif
	FORWARD_DECLARE( SystemUI );
}
//namespace vulkan
//{
//	FORWARD_DECLARE( DataFactory );
//}
namespace directX11
{
	FORWARD_DECLARE( DataFactory );
}
namespace fx
{
	FORWARD_DECLARE( Settings );
	FORWARD_DECLARE( CommandDispatch );
	FORWARD_DECLARE( SceneRenderer );
	FORWARD_DECLARE( SceneBuilder );
	FORWARD_DECLARE( Camera );
	FORWARD_DECLARE( FreeFlyRig );

	typedef tools::ProcessExecutor< tools::JoinWorkersPolicy, tools::BalancerDispatchPolicy > Executor;
	SHARED_PTR_TYPE_DEF( Executor );

	typedef tools::ScheduledFunctorProcess< tools::CompletedRunPolicySubject > ScheduledFunctor;
	SHARED_PTR_TYPE_DEF( ScheduledFunctor );

	typedef tools::EnqueuingObserver< Executor, tools::SyncProcessPolicy< Executor > > TaskObserver;
	SHARED_PTR_TYPE_DEF( TaskObserver );


	struct CameraSet
	{
		CameraSPtr pCamera;
		FreeFlyRigSPtr pCameraRig;
	};

	struct FXSystem
	{
		//vulkan::DataFactory factoryVK;
#if defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
		directX11::DataFactorySPtr pFactoryDX;
#endif

		SceneRendererSPtr   pSceneRenderer;
		CommandDispatchSPtr pDispatch;

		SceneBuilderSPtr    pSceneBuilder;

		std::function< void( ) > updateRenderTargets;
		std::function< void( float ) > updateScene;
	};

	/// <summary>
	/// A base factory that is used to start of
	/// the construction of the basic system.
	/// </summary>
	class FXFactory
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="name"> The system name. </param>
			FXFactory( ) = default;

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The settings object to copy. </param>
			FXFactory( const FXFactory& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="move"> The settings object to move. </param>
			FXFactory( FXFactory&& move ) = default;

			/// <summary> dtor </summary>
			~FXFactory( ) = default;

			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The settings object to copy. </param>
			/// <returns> A referance to this. </returns>
			FXFactory& operator=( const FXFactory& copy ) = default;

			/// <summary> Move assignment operator </summary>
			/// <param name="move"> The settings object to move. </param>
			/// <returns> A referance to this. </returns>
			FXFactory& operator=( FXFactory&& move ) = default;


			/// <summary> Load the base application settings file. </summary>
			/// <param name="path"> The path to the settings file. </param>
			/// <returns> A pointer to the settings. </returns>
			SettingsSPtr loadSettings( const std::string& path ) const;

			/// <summary> Create the application event executor. </summary>
			/// <param name="pSettings"> The application settings. </param>
			/// <returns> A pointer the applications core engine. </returns>
			ExecutorSPtr buildExecutor( const SettingsSPtr& pSettings ) const;

			/// <summary> Create the application window that we are going to draw to. </summary>
			/// <param name="pSettings"> The application settings. </param>
			/// <returns> A pointer to the new window. </returns>
			ui::WindowSPtr buildWindow( const SettingsSPtr& pSettings ) const;

			/// <summary> Create an input listener so that we can get control input from the window. </summary>
			/// <returns>An input listener. </returns>
			ui::InputSPtr buildInputListerner( ) const;

			/// <summary> Create the default system UI. </summary>
			/// <param name="pSettings"> The application settings. </param>
			/// <param name="pInput"> The window user input listener. </param>
			/// <returns> A system UI. </returns>
			ui::SystemUISPtr buildSystemUI( const SettingsSPtr& pSettings, const ui::InputSPtr& pInput ) const;

			/// <summary> Create an window size change listener so that we can resize the render targets. </summary>
			/// <returns> A window size change listener. </returns>
			ui::WindowChangeSPtr buildWindowChangeListener( ) const;

			/// <summary> Create a camera set that we can use to move around and view the world. </summary>
			/// <param name="pSettings"> The application settings. </param>
			/// <param name="pInput"> The window user input listener. </param>
			/// <returns> A set of objects to control our camera. </returns>
			CameraSet buildCameraSet( const SettingsSPtr& pSettings, const ui::InputSPtr& pInput ) const;

			/// <summary> Create the rendering components that will draw to the window. </summary>
			/// <param name="pSettings"> The application settings. </param>
			/// <param name="Camera"> The camera interface that we use to view the world. </param>
			/// <param name="pWindow"> The application window to draw to. </param>
			/// <param name="pInput"> The window user input listener. </param>
			/// <param name="pWindowChange"> The window change listener, allow us to resize the render targets. </param>
			/// <param name="pUI"> The user interface to render. </param>
			/// <returns> A set of objects that are used to update and draw a scene. </returns>
			FXSystem buildSystem( const SettingsSPtr& pSettings,
								  const CameraSPtr& pCamera,
								  const ui::WindowSPtr& pWindow,
								  const ui::InputSPtr& pInput,
								  const ui::WindowChangeSPtr& pWindowChange,
								  const ui::SystemUISPtr& pUI ) const;
	};
} // namespace fx
} // namespace visual

#endif // _SETTINGS_H