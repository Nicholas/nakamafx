#include "SceneRenderer.h"
// Interface for a rendered scene. This also includes post processing which is relatively
// complex in that it has multiple passes to multiple render targets to get a result.
//
// Project   : NaKama-Fx
// File Name : SceneRenderer.cpp
// Date      : 08/02/2016
// Author    : Nicholas Welters

namespace visual
{
namespace fx
{
	/// <summary> ctor. </summary>
	SceneRenderer::SceneRenderer( )
		: AsyncController( "Scene Renderer" )
	{ }

	/// <summary> This is were we will set up the state of the GPU to render all the object in this scene. </summary>
	void SceneRenderer::render( )
	{ }

	/// <summary> Update the pass and its GPU data. </summary>
	/// <param name="delta"> The time step to take for the update. </param>
	void SceneRenderer::update( float delta )
	{ }

	/// <summary> The derived classes interface to finalize its updates to create a consistent internal state. </summary>
	void SceneRenderer::update( )
	{ }

} // namespace fx
} // namespace visual
