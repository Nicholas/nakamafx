#pragma once
// Some maths helper functions.
//
// Project   : NaKama-Fx
// File Name : Maths.h
// Date      : 03/01/2018
// Author    : Nicholas Welters

#ifndef _MATHS_H
#define _MATHS_H

#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	/// <summary> Holds a useful set of mathematical functions. </summary>
	class Maths
	{		
	public:

		/// <summary> Generate a random unit sphere vector. </summary>
		/// <returns> A random unit vector. </returns>
		static glm::vec3 randomUnitVec3( );

		/// <summary> Limit Angles of Theta to [ 0, 2 * PI ]. </summary>
		/// <param name="theta"> A reference to the angle that needs limiting. </param>
		static void limitAngle( float& theta );
		
		/// <summary> Append the surface normals to the current value in out. </summary>
		/// <param name="out"> The current vector that we are going to add to. </param>
		/// <param name="vertex1"> A vertex on a the plane. </param>
		/// <param name="vertex2"> A vertex on a the plane. </param>
		/// <param name="vertex3"> A vertex on a the plane. </param>
		static void appendNormal( glm::vec3& out,
			glm::vec3& vertex1, glm::vec3& vertex2, glm::vec3& vertex3 );
		
		/// <summary> Append the surface normals to the current value in out. </summary>
		/// <param name="tangent"> The storage space for the surface tangent. </param>
		/// <param name="bitangent"> The storage space for the surface bitangent. </param>
		/// <param name="vertex1"> A vertex on a the plane. </param>
		/// <param name="vertex2"> A vertex on a the plane. </param>
		/// <param name="vertex3"> A vertex on a the plane. </param>
		/// <param name="uv1"> The uv coordinates on a the plane to align the tangent and bitangent to. </param>
		/// <param name="uv2"> The uv coordinates on a the plane to align the tangent and bitangent to. </param>
		/// <param name="uv3"> The uv coordinates on a the plane to align the tangent and bitangent to. </param>
		static void calculateTangents(
			glm::vec3& tangent, glm::vec3& bitangent,
			glm::vec3& vertex1, glm::vec3& vertex2, glm::vec3& vertex3,
			glm::vec2& uv1    , glm::vec2& uv2    , glm::vec2& uv3 );
		
		/// <summary> Determine the ray in world space extending from the cameras position. </summary>
		/// <param name="out"> The mouse ray. </param>
		/// <param name="x"> The mouse screen position. </param>
		/// <param name="y"> The mouse screen position. </param>
		/// <param name="width"> The size of the window. </param>
		/// <param name="height"> The size of the window. </param>
		/// <param name="projection"> The cameras projection matrix. </param>
		/// <param name="view"> The cameras view matrix. </param>
		static void mouseRay(
			glm::vec3& out,
			float x, float y,
			float width, float height,
			const glm::mat4x4& projection,
			const glm::mat4x4& view );
		
		/// <summary> Determine the ray in world space extending from the cameras position. </summary>
		/// <param name="out"> The mouse ray. </param>
		/// <param name="ray"> The ray direction. </param>
		/// <param name="position"> The ray origin. </param>
		/// <param name="point"> A point on the plane. </param>
		/// <param name="normal"> The normal of the plane. </param>
		static bool planeIntersection(
			glm::vec3&       out,
			const glm::vec3& ray,
			const glm::vec3& position,
			const glm::vec3& point,
			const glm::vec3& normal );
	};
} // namespace fx
} // namespace visual

#endif // _MATHS_H