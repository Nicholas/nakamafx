#pragma once

//Mesh class
//
//This holds our RAM based mesh information, a vertex and index buffer.
//
// Project   : NaKama-Fx
// File Name : Mesh.hpp
// Date      : 30/05/2015
// Author    : Nicholas Welters

#include "Mesh.h"

#ifndef _MESH_HPP
#define _MESH_HPP

namespace visual
{
namespace fx
{
	using std::move;

	/// <summary> ctor </summary>
	/// <param name="vertexBuffer"> The mesh vertices list. </param>
	/// <param name="indexBuffer"> The vertex usage order. </param>
	/// <param name="topology"> The mesh basic primitive type. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::Mesh( const VertexBuffer& vertexBuffer, const IndexBuffer& indexBuffer, const MeshPrimitive topology )
		: vertexBuffer( vertexBuffer )
		, indexBuffer( indexBuffer )
		, topology( topology )
	{
	}
	
	/// <summary> ctor </summary>
	/// <param name="vertexBuffer"> The mesh vertices list. </param>
	/// <param name="indexBuffer"> The vertex usage order. </param>
	/// <param name="topology"> The mesh basic primitive type. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::Mesh( const VertexBuffer& vertexBuffer, IndexBuffer&& indexBuffer, const MeshPrimitive topology )
		: vertexBuffer( vertexBuffer )
		, indexBuffer( move( indexBuffer ) )
		, topology( topology )
	{
	}
	
	/// <summary> ctor </summary>
	/// <param name="vertexBuffer"> The mesh vertices list. </param>
	/// <param name="indexBuffer"> The vertex usage order. </param>
	/// <param name="topology"> The mesh basic primitive type. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::Mesh( VertexBuffer&& vertexBuffer, const IndexBuffer& indexBuffer, const MeshPrimitive topology )
		: vertexBuffer( move( vertexBuffer ) )
		, indexBuffer( indexBuffer )
		, topology( topology )
	{
	}

	/// <summary> ctor </summary>
	/// <param name="vertexBuffer"> The mesh vertices list. </param>
	/// <param name="indexBuffer"> The vertex usage order. </param>
	/// <param name="topology"> The mesh basic primitive type. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::Mesh( VertexBuffer&& vertexBuffer, IndexBuffer&& indexBuffer, const MeshPrimitive topology ) noexcept
		: vertexBuffer( move( vertexBuffer ) )
		, indexBuffer( move( indexBuffer ) )
		, topology( topology )
	{
	}
	
	/// <summary> default ctor </summary>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::Mesh( )
		: topology( UNDEFINED )
	{
	}
	
	/// <summary> copy ctor </summary>
	/// <param name="copy"> The object to copy. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::Mesh( const Mesh& copy )
		: vertexBuffer( copy.vertexBuffer )
		, indexBuffer( copy.indexBuffer )
		, topology( copy.topology )
	{
	}

	/// <summary> move ctor </summary>
	/// <param name="temp"> The object to move. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::Mesh( Mesh&& temp ) noexcept
		: vertexBuffer( move( temp.vertexBuffer ) )
		, indexBuffer( move( temp.indexBuffer ) )
		, topology( temp.topology )
	{
	}
	
	/// <summary> dtor </summary>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >::~Mesh( )
	{
	}

	/// <summary> Copy assignment operator </summary>
	/// <param name="copy"> The object to copy. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >& Mesh< VertexType, IndexType >::operator=( const Mesh& copy )
	{
		if( this == &copy )
			return *this;
		vertexBuffer = copy.vertexBuffer;
		indexBuffer  = copy.indexBuffer;
		topology     = copy.topology;
		return *this;
	}

	/// <summary> Copy assignment operator </summary>
	/// <param name="move"> The object to move. </param>
	template< class VertexType, typename IndexType >
	Mesh< VertexType, IndexType >& Mesh< VertexType, IndexType >::operator=( Mesh&& temp ) noexcept
	{
		if( this == &temp )
			return *this;
		vertexBuffer = move( temp.vertexBuffer );
		indexBuffer  = move( temp.indexBuffer );
		topology     = temp.topology;
		return *this;
	}

	/// <summary> Get the mesh vertex buffer. </summary>
	/// <returns> The vertex buffer. </returns>
	template< class VertexType, typename IndexType >
	const typename Mesh< VertexType, IndexType >::VertexBuffer& Mesh< VertexType, IndexType >::getVertexBuffer( ) const
	{
		return vertexBuffer;
	}

	/// <summary> Get the index vertex buffer. </summary>
	/// <returns> The index buffer. </returns>
	template< class VertexType, typename IndexType >
	const typename Mesh< VertexType, IndexType >::IndexBuffer& Mesh< VertexType, IndexType >::getIndexBuffer( ) const
	{
		return indexBuffer;
	}

	/// <summary> Get the mesh primitive topology. </summary>
	/// <returns> The primitive topology. </returns>
	template< class VertexType, typename IndexType >
	MeshPrimitive Mesh< VertexType, IndexType >::getTopology( ) const
	{
		return topology;
	}
} // namespace fx
} // namespace visual

#endif // _MESH_HPP
