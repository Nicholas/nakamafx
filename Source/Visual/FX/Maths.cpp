#include "Maths.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/constants.hpp>
#include <random>
#include <Macros.h>

namespace visual
{
namespace fx
{
	using std::uniform_real_distribution;
	using std::default_random_engine;
	using glm::normalize;
	using glm::two_pi;
	using glm::unProject;
	using glm::dot;
	using glm::vec4;
	using glm::vec3;
	using glm::vec2;
	using glm::mat4x4;
	using tools::almostEqual;
	
	/// <summary> Generate a random unit sphere vector. </summary>
	/// <returns> A random unit vector. </returns>
	vec3 Maths::randomUnitVec3( )
	{
		uniform_real_distribution< float > randomFloats( -1.0, 1.0 );
		default_random_engine generator( static_cast< unsigned int >( time( nullptr ) ) );  // NOLINT

		return normalize( vec3(
				randomFloats( generator ),
				randomFloats( generator ),
				randomFloats( generator )
			) );
	}
	

	/// <summary> Limit Angles of Theta to [ 0, 2 * PI ]. </summary>
	/// <param name="theta"> A reference to the angle that needs limiting. </param>
	void Maths::limitAngle( float& theta )
	{
		if( theta > two_pi< float >( ) )
		{
			theta -= two_pi< float >( );
		}
		if( theta < 0.0f )
		{
			theta += two_pi< float >( );
		}
	}
	
		
	/// <summary> Append the surface normals to the current value in out. </summary>
	/// <param name="out"> The current vector that we are going to add to. </param>
	/// <param name="vertex1"> A vertex on a the plane. </param>
	/// <param name="vertex2"> A vertex on a the plane. </param>
	/// <param name="vertex3"> A vertex on a the plane. </param>
	void Maths::appendNormal( vec3& out, vec3& vertex1, vec3& vertex2, vec3& vertex3 )
	{
		const vec3 vector1 = vertex1 - vertex2;
		const vec3 vector2 = vertex1 - vertex3;

		const vec3 normal = cross( vector1, vector2 );

		if( normal.length > 0 )
		{
			out = normalize( normal ) + out;
		}
	}
		
	/// <summary> Append the surface normals to the current value in out. </summary>
	/// <param name="tangent"> The storage space for the surface tangent. </param>
	/// <param name="bitangent"> The storage space for the surface bitangent. </param>
	/// <param name="vertex1"> A vertex on a the plane. </param>
	/// <param name="vertex2"> A vertex on a the plane. </param>
	/// <param name="vertex3"> A vertex on a the plane. </param>
	/// <param name="uv1"> The uv coordinates on a the plane to align the tangent and bitangent to. </param>
	/// <param name="uv2"> The uv coordinates on a the plane to align the tangent and bitangent to. </param>
	/// <param name="uv3"> The uv coordinates on a the plane to align the tangent and bitangent to. </param>
	void Maths::calculateTangents(
		vec3& tangent, vec3& bitangent,
		vec3& vertex1, vec3& vertex2, vec3& vertex3,
		vec2& uv1, vec2& uv2, vec2& uv3 )
	{
		const vec3 vector1 = vertex2 - vertex1;
		const vec3 vector2 = vertex3 - vertex1;

		vec2 tUv1;
		vec2 tUv2;

		tUv1.x = uv2.x - uv1.x;
		tUv1.y = uv2.y - uv1.y;

		tUv2.x = uv3.x - uv1.x;
		tUv2.y = uv3.y - uv1.y;

		const float determinant = ( tUv1.x * tUv2.y - tUv2.x * tUv1.y );

		if( determinant == 0 )
		{
			vec3 n = cross( vector1, vector2 );
			tangent   = cross( n, normalize( vec3( 0, 1, 0 ) ) );
			bitangent = normalize( cross( n, tangent ) );
			tangent   = normalize( cross( n, bitangent ) );
		}
		else
		{
			const float denom = 1.0f / determinant;

			tangent   = normalize( ( vector1 * tUv2.y - vector2 * tUv1.y ) * denom );
			bitangent = normalize( ( vector2 * tUv1.x - vector1 * tUv2.x ) * denom );
		}
	}
	
	/// <summary> Determine the ray in world space extending from the cameras position. </summary>
	/// <param name="out"> The mouse ray. </param>
	/// <param name="x"> The mouse screen position. </param>
	/// <param name="y"> The mouse screen position. </param>
	/// <param name="width"> The size of the window. </param>
	/// <param name="height"> The size of the window. </param>
	/// <param name="projection"> The cameras projection matrix. </param>
	/// <param name="view"> The cameras view matrix. </param>
	void Maths::mouseRay(
		vec3& out,
		const float x, const float y,
		const float width, const float height,
		const mat4x4& projection,
		const mat4x4& view )
	{
		const vec3 pointN( x, height - y, 0.01f );
		const vec3 pointF( x, height - y, 1.0f );
		const vec4 viewport( 0, height, width, -height );
		const vec3 point1 = unProject( pointN, view, projection, viewport );
		const vec3 point2 = unProject( pointF, view, projection, viewport );
	
		out = normalize( point2 - point1 );
	}

	/// <summary> Determine the ray in world space extending from the cameras position. </summary>
	/// <param name="out"> The mouse ray. </param>
	/// <param name="ray"> The ray direction. </param>
	/// <param name="position"> The ray origin. </param>
	/// <param name="point"> A point on the plane. </param>
	/// <param name="normal"> The normal of the plane. </param>
	bool Maths::planeIntersection(
		vec3& out,
		const vec3& ray,
		const vec3& position,
		const vec3& point,
		const vec3& normal )
	{
		// determine if ray parallel to plane
		const float dotProduct = dot( ray, normal );
		
		if( almostEqual( dotProduct, 0 ) )
		{
			return false;
		}
		
		const float lambda = dot( normal, point - position ) / dotProduct;
		
		if( lambda < -0.000001 )
		{
			return false;
		}
		
		out = ray * lambda + position;
	
		return true;
	}
} // namespace fx
} // namespace visual