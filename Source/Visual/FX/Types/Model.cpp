#include "Model.h"

// Model class
//
// Maintain the models position in the world.
//
// Project   : NaKama-Fx
// File Name : Model.cpp
// Date      : 05/06/2017
// Author    : Nicholas Welters

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

namespace visual
{
namespace fx
{
	using glm::vec2;
	using glm::vec3;
	using glm::mat4x4;
	using glm::translate;
	using glm::yawPitchRoll;
	//using glm::scale;
	using std::function;

	Model::~Model( ) = default;

	/// <summary> Update the model matrix that moves the mesh vertices to world space. </summary>
	void Model::update( )
	{
		if( changed )
		{
			mat4x4 matrix( 1 );
			matrix = translate( matrix, position );
			matrix = matrix * yawPitchRoll( rotation.y, rotation.x, rotation.z );
			onMatrixChanged( glm::scale( matrix, scale ) );

			changed = false;
		}
	}

	/// <summary> Get the model position. </summary>
	/// <returns> The models world position. </returns>
	vec3 Model::getPosition( ) const
	{
		return position;
	}

	/// <summary> Set the model position. </summary>
	/// <param name="newPosition"> The models world position. </param>
	void Model::setPosition( const vec3& newPosition )
	{
		position = newPosition;
		changed = true;
	}

	/// <summary> Get the model orientation. Raw x, y, z rotation values. </summary>
	/// <returns> The models world roation. </returns>
	vec3 Model::getRotation( ) const
	{
		return rotation;
	}

	/// <summary> Set the model orientation. Raw x, y, z rotation values. </summary>
	/// <param name="newRotation"> The model orientation. </param>
	void Model::setRotation( const vec3& newRotation )
	{
		rotation = newRotation;
		changed = true;
	}

	/// <summary> Set the model orientation using polar coordinates. </summary>
	/// <param name="newRotation"> The model orientation in polar coordinates. </param>
	void Model::setRotation( const vec2& newRotation )
	{
		rotation.x = sin( newRotation.y ) * cos( newRotation.x );
		rotation.y = sin( newRotation.y ) * sin( newRotation.x );
		rotation.z = cos( newRotation.x );
	}

	/// <summary> Get the model size. </summary>
	/// <returns> The models world position. </returns>
	vec3 Model::getScale( ) const
	{
		return scale;
	}

	/// <summary> Set the model size. </summary>
	/// <param name="scale"> The model size scale. </param>
	void Model::setScale( const vec3& newScale )
	{
		scale = newScale;
		changed = true;
	}
} // namespace fx
} // namespace visual

