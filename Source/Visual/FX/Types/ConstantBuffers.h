#pragma once

// Constant buffer structures
//
// Project   : NaKama-Fx
// File Name : ConstantBuffers.h
// Date      : 08/06/2015
// Author    : Nicholas Welters

#ifndef _CONSTANT_BUFFERS_H
#define _CONSTANT_BUFFERS_H

#include <Macros.h>
#include <glm/glm.hpp>

#define PAD__32_BITS__4_BYTES \
	float PPCAT( padding, __LINE__ ) = 0

#define PAD__64_BITS__8_BYTES \
	float PPCAT( padding, PPCAT( A, __LINE__ ) ) = 0;\
	float PPCAT( padding, PPCAT( B, __LINE__ ) ) = 0

namespace visual
{
namespace fx
{
	// +---------------+
	// | Vertex Shader |
	// +---------------+

	/// <summary> A view projection only buffer. Per Camera </summary>
	struct VSDepthBuffer
	{
		/// <summary>
		/// The camera view projection so that we can transform vertices for 2D output.
		///  </summary>
		glm::mat4x4 viewProjection = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )
	};
	//--------------------------------------------------------------( 16 * 4 = 64 bytes )


	/// <summary>
	/// A forward+ per scene constants.
	/// Holds the camera view, view * projection,
	/// a single direction light and two shadow projections
	/// for the light.
	/// </summary>
	struct VSSceneBuffer
	{
		/// <summary>
		/// The camera view projection so that we can transform vertices for 2D output.
		///  </summary>
		glm::mat4x4 viewProjection = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The camera view so that we can transform vertices in to view space.
		///  </summary>
		glm::mat4x4 view = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The first shadow cascade projection vertices in to shadow cascade space 1.
		///  </summary>
		glm::mat4x4 shadowProjection1 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The second shadow cascade projection vertices in to shadow cascade space 2.
		///  </summary>
		glm::mat4x4 shadowProjection2 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The camera position in world space.
		/// </summary>
		glm::vec3   cameraPosition = glm::vec3( 0, 0, 0 );

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// A point light position.
		/// TODO: REMOVE THIS.
		/// </summary>
		glm::vec3 pLightP = glm::vec3( 0, 0, 0 );

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The sun light direction linked to the cascade shadow maps.
		/// </summary>
		glm::vec3 dLightD = glm::vec3( 0, 0, 0 );

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 19 = 304 bytes )


	/// <summary>
	/// The per scene world matrix to place the oject.
	/// TODO: Remove... / combine with VSDepthBuffer
	/// </summary>
	struct VSObjectBuffer
	{
		/// <summary>
		/// The models world matrix to position the model in to world space.
		/// </summary>
		glm::mat4x4 model = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )
	};
	//--------------------------------------------------------------( 16 * 4 = 64 bytes )





	// +-----------------+
	// | Geometry Shader |
	// +-----------------+

	/// <summary>
	/// The per scene constants used for pass through vertex expantion.
	/// Includes the tick time and game times.
	/// </summary>
	struct GSSceneBuffer
	{
		/// <summary>
		/// The camera view projection so that we can transform vertices for 2D output.
		///  </summary>
		glm::mat4x4 viewProjection = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The camera view so that we can transform vertices in to view space.
		///  </summary>
		glm::mat4x4 view = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The first shadow cascade projection vertices in to shadow cascade space 1.
		///  </summary>
		glm::mat4x4 shadowProjection1 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The second shadow cascade projection vertices in to shadow cascade space 2.
		///  </summary>
		glm::mat4x4 shadowProjection2 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary> The camera position in world space. </summary>
		glm::vec4   eyePosition = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The game time. This may overflow at some point so be careful with this.
		/// </summary>
		float gGameTime = 0.0f;

		/// <summary> The games frame time delta. </summary>
		float gTimeStep = 0.0f;

		PAD__64_BITS__8_BYTES;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 18 = 288 bytes )


	/// <summary>
	/// The per scene constants used for pass through vertex expantion.
	/// </summary>
	struct GSFrameBuffer
	{
		/// <summary>
		/// The camera view projection so that we can transform vertices for 2D output.
		///  </summary>
		glm::mat4x4 viewProjection = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The first shadow cascade projection vertices in to shadow cascade space 1.
		///  </summary>
		glm::mat4x4 shadowProjection1 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The second shadow cascade projection vertices in to shadow cascade space 2.
		///  </summary>
		glm::mat4x4 shadowProjection2 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The camera position in world space.
		/// </summary>
		glm::vec4 eyePosition = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 13 = 208 bytes )


	/// <summary>
	/// The per particle system constants used
	/// to control the behaviour unique to the particle system.
	/// </summary>
	struct GSParticleBuffer // b1
	{
		/// <summary>
		/// The partical systems world space position.
		/// </summary>
		glm::vec3 emitPosW = glm::vec3( 0, 0, 0 );

		/// <summary>
		/// The game time. This may overflow at some point so be careful with this.
		/// </summary>
		float gameTime = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The partical systems world space direction.
		/// </summary>
		glm::vec3 emitDirW = glm::vec3( 0, 0, 0 );

		/// <summary>
		/// The games frame time delta.
		/// </summary>
		float timeStep = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The partical systems world space position randomed offset scale.
		/// </summary>
		glm::vec3 emitPosWScale = glm::vec3( 0, 0, 0 );

		/// <summary>
		/// The particle systems time to emit a new paricle.
		/// </summary>
		float emitTime = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The partical systems world space direction randomed offset scale.
		/// </summary>
		glm::vec3 emitDirWScale = glm::vec3( 0, 0, 0 );

		/// <summary>
		/// The particle systems flare life time.
		/// </summary>
		float lifeTime = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The particle systems flare base size.
		/// </summary>
		glm::vec2 flareSize = glm::vec2( 0, 0 );

		/// <summary>
		/// The particle systems flare size change based on age scale.
		/// </summary>
		glm::vec2 ageLifeSizeScale = glm::vec2( 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The particle systems flare starting colour.
		/// </summary>
		glm::vec4 startColour = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The particle systems flare ending colour.
		/// </summary>
		glm::vec4 endColour = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The particle systems flare starting colour randomization scale.
		/// </summary>
		glm::vec4 starColourScale = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The particle systems flare ending colour randomization scale.
		/// </summary>
		glm::vec4 endColourScale = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The particle systems gravotational force.
		///
		/// If attractor.w is zero then the attractor.xyz
		/// is a direction force applide equally to all particles.
		/// When it is not 0 then it is the base atraction force
		/// to a point in world space defined by attractor.xyz
		/// </summary>
		glm::vec4 attractor = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 10 = 160 bytes )






	// +--------------+
	// | Pixel Shader |
	// +--------------+

	/// <summary>
	/// A forward+ per scene constants.
	///
	/// Holds basic ambiant lighting and a single direction light
	/// (the sun). Also contains information about the shadow maps
	/// and render target sizes.
	/// </summary>
	struct PSLightingBuffer
	{
		/// <summary> Basic ambiant light colour. </summary>
		glm::vec4 aLightC = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Sun/Direction light colour. </summary>
		glm::vec4 dLightC = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Sun/Direction light direction. </summary>
		glm::vec3 dLightD = glm::vec3( 0, 0, 0 );

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The games frame time delta. </summary>
		float tick = 0.0f;

		/// <summary> The game time. This may overflow at some point so be careful with this. </summary>
		float time = 0.0f;

		/// <summary> The size of the render target. </summary>
		glm::vec2 targetSize = glm::vec2( 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The 1 / size of the render target. </summary>
		glm::vec2 invTargetSize = glm::vec2( 0, 0 );

		/// <summary> The 1 / the first shadow casde size. </summary>
		glm::vec2 shadow1Size = glm::vec2( 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The 1 / the second shadow casde size. </summary>
		glm::vec2 shadow2Size = glm::vec2( 0, 0 );

		/// <summary> The projection depth bounds. </summary>
		glm::vec2 zDepths = glm::vec2( 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 6 = 96 bytes )


	/// <summary>
	/// A forward+ per translucent object material constants.
	///
	/// Provide the forward pas shader the indformation
	/// required to draw our translucent objects.
	///
	/// TODO: Rename this
	/// </summary>
	struct PSObjectBuffer
	{
		/// <summary> Material ambiant reflectivity. </summary>
		glm::vec3 ka = glm::vec3( 0, 0, 0 );

		/// <summary> Material base roughness. </summary>
		float roughness = 0.5f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material diffuse reflectivity. </summary>
		glm::vec3 kd = glm::vec3( 0, 0, 0 );

		/// <summary>
		///  A materials matal state. 1 are metals, 0 is everything else. (0, 1)
		///  emulate layers that have mixtures of metals and non metals like rust
		///  </summary>
		float metalness = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material spcular reflectivity. </summary>
		glm::vec3 ks = glm::vec3( 0, 0, 0 );

		/// <summary> Material sub surface scatering bump scale refraction. </summary>
		float ssBump = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material bump mapping scale. </summary>
		glm::vec2 bumpScale = glm::vec2( 1, 1 );

		/// <summary> Material sub surface scatering power. </summary>
		float ssPower = 0.0f;

		/// <summary> Material sub surface scatering scale. </summary>
		float ssScale = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Materials index of refraction, scales the refaction sample offset. </summary>
		float ior = 0.0f;

		/// <summary> Materials surface transparency, allows us to see the sub surface. </summary>
		float sAlpha = 1.0f;

		/// <summary> Materials sub surface transparency, allows us to see the through the object. </summary>
		float ssAlpha = 0.0f;

		/// <summary> Materials light absorbtion factor. </summary>
		float absorption = 0.0f;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 5 = 80 bytes )


	/// <summary>
	/// A forward+ per physically based object material constants.
	///
	/// Provide the forward pas shader the indformation
	/// required to draw our translucent objects.
	/// </summary>
	struct PSObjectPBRBuffer
	{
		/// <summary> Material ambiant reflectivity. </summary>
		glm::vec3 Ka = glm::vec3( 0, 0, 0 );

		/// <summary> Material bump mapping x scale. </summary>
		float BumpScaleX = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material diffuse reflectivity. </summary>
		glm::vec3 Kd = glm::vec3( 0, 0, 0 );

		/// <summary> Material bump mapping y scale. </summary>
		float BumpScaleY = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material spcular reflectivity. </summary>
		glm::vec3 Ks = glm::vec3( 0, 0, 0 );

		/// <summary> Specular power, depricated (TODO: Remove). </summary>
		float Ns = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material base roughness. </summary>
		float roughness = 0.5f;

		/// <summary>
		///  A materials matal state. 1 are metals, 0 is everything else. (0, 1)
		///  emulate layers that have mixtures of metals and non metals like rust
		///  </summary>
		float metalness = 0.0f;

		/// <summary> Material sub surface scatering power. </summary>
		float ssPower = 0.0f;

		/// <summary> Material sub surface scatering scale. </summary>
		float ssScale = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Materials index of refraction, scales the refaction sample offset. </summary>
		float ior = 0.0f;

		/// <summary> Materials surface transparency, allows us to see the sub surface. </summary>
		float sAlpha = 1.0f;

		/// <summary> Materials sub surface transparency, allows us to see the through the object. </summary>
		float ssAlpha = 0.0f;

		/// <summary> Materials light absorbtion factor. </summary>
		float absorption = 0.0f;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 5 = 80 bytes )


	/// <summary>
	/// A forward+ per physically based font material constants.
	///
	/// Provide the forward pas shader the indformation
	/// required to draw text in 3D space.
	/// </summary>
	struct PSFontBuffer
	{
		/// <summary> Material ambiant reflectivity. </summary>
		glm::vec3 ka = glm::vec3( 0, 0, 0 );

		/// <summary> Material base roughness. </summary>
		float roughness = 0.5f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material diffuse reflectivity. </summary>
		glm::vec3 kd = glm::vec3( 0, 0, 0 );

		/// <summary>
		///  A materials matal state. 1 are metals, 0 is everything else. (0, 1)
		///  emulate layers that have mixtures of metals and non metals like rust
		///  </summary>
		float metalness = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material spcular reflectivity. </summary>
		glm::vec3 ks = glm::vec3( 0, 0, 0 );

		/// <summary> Material sub surface scatering bump scale refraction. </summary>
		float ssBump = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material bump mapping scale. </summary>
		glm::vec2 bumpScale = glm::vec2( 0, 0 );

		/// <summary> Material sub surface scatering power. </summary>
		float ssPower = 0.0f;

		/// <summary> Material sub surface scatering scale. </summary>
		float ssScale = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Materials surface transparency, allows us to see the sub surface. </summary>
		float sAlpha = 1.0f;

		/// <summary> Materials sub surface transparency, allows us to see the through the object. </summary>
		float ssAlpha = 0.0f;

		/// <summary> Materials light absorbtion factor. </summary>
		float absorption = 0.0f;

		/// <summary> Materials index of refraction, scales the refaction sample offset. </summary>
		float refraction = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Font Data A. </summary>
		glm::uvec4 properties = glm::uvec4( 0 );
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 6 = 96 bytes )

	/// <summary>
	/// The per billboar type pixel shader constants used to provide
	/// the info on how to draw the particle billboards.
	///
	/// Supports a simple sprite based animation.
	/// </summary>
	struct PSParticleBuffer
	{
		/// <summary> Material ambiant reflectivity. </summary>
		glm::vec3 Ka = glm::vec3( 0, 0, 0 );

		/// <summary> Material bump mapping x scale. </summary>
		float bumpScaleX = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material diffuse reflectivity. </summary>
		glm::vec3 Kd = glm::vec3( 0, 0, 0 );

		/// <summary> Material bump mapping y scale. </summary>
		float bumpScaleY = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> Material spcular reflectivity. </summary>
		glm::vec3 Ks = glm::vec3( 0, 0, 0 );

		/// <summary> Specular power, depricated (TODO: Remove). </summary>
		float Ns = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The number of sprites in the texture on the u and v axis. </summary>
		glm::vec2 imageCount = glm::vec2( 1, 1 );

		/// <summary> The rate at which we switch sprites. </summary>
		float frameRate = 1.0f;

		/// <summary> The total number of sprites. </summary>
		float frameCount = 1.0f;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 4 = 64 bytes )


	/// <summary>
	/// The final colour correction and tone mapping pass constants.
	/// </summary>
	struct PSPostFXBuffer
	{
		/// <summary> The gamma correction value. </summary>
		float gamma = 0.0f;

		/// <summary> The contrast correction value. </summary>
		float contrast = 0.0f;

		/// <summary> The saturation correction value. </summary>
		float saturation = 0.0f;

		/// <summary> The brightness correction value. </summary>
		float brightness = 0.0f;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 1 = 16 bytes )






	// +-------+
	// | Bloom |
	// +-------+

	/// <summary>
	/// Blur pass screen size information for the vertex shader.
	/// </summary>
	struct VSBlurBuffer
	{
		/// <summary> The 1 / size of the render target for one pixel uv step sizes. </summary>
		glm::vec2 invScreenSize = glm::vec2( 0, 0 );

		PAD__64_BITS__8_BYTES;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 1 = 16 bytes )


	/// <summary>
	/// The first pass in the bloom that performs the  high pass so that we can
	/// blur just the brighest of pixels.
	/// </summary>
	struct PSBlurBuffer
	{
		/// <summary> The screen texel size so that we can down sample... </summary>
		glm::vec2 texelSize = glm::vec2( 1, 1 );

		/// <summary> The high pass brightness threshold. (0 -> 10) </summary>
		float threshold = 0.0f;

		/// <summary> TODO: Use in blur pass to change the sample size. (0 -> 2) </summary>
		float magantude = 0.0f;

		/// <summary> TODO: Use in blur pass to change the sample distribution. (0.5 -> 1.5) </summary>
		float sigma = 0.0f;

		PAD__32_BITS__4_BYTES;
		PAD__64_BITS__8_BYTES;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 1 = 16 bytes )






	// +----------------------+
	// | Translucent A-Buffer |
	// +----------------------+
	/// <summary>
	/// A-Buffer resolve data provides the shader the screen size.
	/// </summary>
	struct PSABufferResolve
	{
		/// <summary> The render target (ABuffers) size. </summary>
		glm::vec2 screenSize = glm::vec2( 0, 0 );

		/// <summary> The scenes fog density. </summary>
		float density = 0.0f;

		/// <summary> The scenes fog height fall off rate. </summary>
		float heightFalloff = 1.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The inverse camera view so that we can transform
		/// the sample normals into world space space.
		/// </summary>
		glm::mat4x4 invView = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )
	};
	//--------------------------------------------------------------( 16 * 5 = 80 bytes )





	// +-------------------+
	// | Ambiant Occlusion |
	// +-------------------+
	/// <summary>
	/// Screen space ambiant occlusion data.
	/// </summary>
	struct PSSSAOBuffer
	{
		/// <summary>
		/// The camera view so that we can transform
		/// the sample normals into view space.
		/// </summary>
		glm::mat4x4 view = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The camera projection so that we can transform
		/// the sample view positions into screen space.
		/// </summary>
		glm::mat4x4 projection = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The size of the textures to sample, stores
		/// the texture size and 1 / texture size.
		/// </summary>
		glm::vec4 targetSize = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 9 = 144 bytes )







	// +---------------------+
	// | Volumetric Lighting |
	// +---------------------+
	/// <summary>
	/// Screen space volumetric lightin constants, used to run a ray allong the shadow maps
	/// to calculate the amound of light added along the view vector.
	/// </summary>
	struct PSSunVolumeBuffer
	{
		/// <summary>
		/// The inverse camera view so that we can transform
		/// the sample normals into world space space.
		/// </summary>
		glm::mat4x4 invView = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The shadow projection so that we can transform
		/// the sample world positions into shadow map space.
		/// </summary>
		glm::mat4x4 shadowProjection1 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The shadow projection so that we can transform
		/// the sample world positions into shadow map space.
		/// </summary>
		glm::mat4x4 shadowProjection2 = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary>
		/// The size of the shadow map texture to sample, stores
		/// the texture size and 1 / texture size.
		/// </summary>
		glm::vec4 shadowSize1 = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary>
		/// The size of the shadow map texture to sample, stores
		/// the texture size and 1 / texture size.
		/// </summary>
		glm::vec4 shadowSize2 = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The sun light colour. </summary>
		glm::vec4 dLightC = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
		/// <summary> The sun light direction. </summary>
		glm::vec3 dLightD = glm::vec3( 0, 0, 0 );

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The camera position in world space. </summary>
		glm::vec3 cameraPos = glm::vec3( 0, 0, 0 );

		/// <summary> The scenes water height so that we can change the fog. </summary>
		float waterHeight = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The scenes fog density. </summary>
		float density = 0.0f;

		/// <summary> The scenes fog height fall off rate. </summary>
		float heightFalloff = 1.0f;

		/// <summary> The maximum depth to ray march to build a volumetric fog. </summary>
		float rayDepth = 0.0f;

		/// <summary> The fog reflectivity direction -1 to 1 with 0 being isotropic. </summary>
		float gScatter = 0.0f;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 18 = 288 bytes )





	// +----------------+
	// | Compute Shader |
	// +----------------+

	/// <summary>
	/// Exposure adaptation constants.
	/// </summary>
	struct CSAdaptationBuffer
	{
		/// <summary> The frame time delta. </summary>
		float timeDelta = 0.0f;

		/// <summary> The rate of change when increasing the exposure. </summary>
		float TauU = 3.75f;

		/// <summary> The rate of change when decreasing the exposure. </summary>
		float TauD = 3.75f;

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 1 = 16 bytes )


	/// <summary>
	/// Histogram constarction constants.
	/// </summary>
	struct CSHistogramBuffer
	{
		/// <summary> The input pixels minimum luminunce to start the first buckets scale. </summary>
		float inputLuminanceMin = 0.0f;

		/// <summary> The input pixels maximum luminunce to end the last buckets scale. </summary>
		float inputLuminanceMax = 0.0f;

		/// <summary> The desired minumum output luminunce for a bucket using adjustments. </summary>
		float outputLuminanceMin = 0.0f;

		/// <summary> The desired maxumum output luminunce for a bucket using adjustments. </summary>
		float outputLuminanceMax = 0.0f;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The render target width. </summary>
		unsigned int outputWidth = 0;

		/// <summary> The render target height. </summary>
		unsigned int outputHeight = 0;

		/// <summary> The total number of histograms constructed per tile. </summary>
		unsigned int numPerTileHistograms = 0;

		/// <summary> The number of tiles used to sub devide the render target on the x axis. </summary>
		unsigned int tilesX = 0;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 2 = 32 bytes )


	/// <summary>
	/// The light sorting compute shader data to sort lights in to each cluster.
	/// </summary>
	struct CSClusteredLighting
	{
		/// <summary> The total number of thread groups dispatched. </summary>
		glm::uvec3 numberOfThreadGroups = glm::uvec3( 0, 0, 0 );

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The total number of threads dispatched. </summary>
		glm::uvec3 numberOfThreads = glm::uvec3( 0, 0, 0 );

		PAD__32_BITS__4_BYTES;
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The inverse of the projection matrix so that we can move from screen space in to world space. </summary>
		glm::mat4x4 invProjection = glm::mat4x4( 1 );
		//--------------------------------------------------------------( 64 bytes )

		/// <summary> The render target size in xy and then 1 / size in zw. </summary>
		glm::vec4 screenSize = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )

		/// <summary> The near plane z depth. </summary>
		float nearZPlane = 0.0f;

		/// <summary> The far plane z depth. </summary>
		float  farZPlane = 0.0f;

		PAD__64_BITS__8_BYTES;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 8 = 128 bytes )



	/// <summary>
	/// Tone Mapping Settings Buffer.
	/// </summary>
	struct PSToneMappingBuffer
	{
		glm::vec2 imageSize    = { 0, 0 };
		glm::vec2 invImageSize = { 0, 0 };
		//--------------------------------------------------------------( 16 bytes )
		float shoulderStrength =  0.22f;

		float linearStrength   =  0.3f;
		float linearAngle      =  0.1f;
		float linearWhite      = 11.2f;
		//--------------------------------------------------------------( 16 bytes )
		float toeStrength      =  0.2f;
		float toeNumerator     =  0.01f;
		float toeDenominator   =  0.3f;

		float gamma            = 1.0f / 2.2f;
		//--------------------------------------------------------------( 16 bytes )
		glm::vec4 bloomFactors = { 0.25, 0.5f, 0.75f, 1.0f };
		//--------------------------------------------------------------( 16 bytes )
		int mode               =  0x0;
		glm::ivec3 p0          = { 0, 0, 0 };
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 5 = 80 bytes )





	/// <summary>
	/// User Interface Vertex Constant Buffer.
	/// </summary>
	struct VSUIBuffer
	{
		glm::vec4 transform  = { 0, 0, 0, 0 };
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 1 = 16 bytes )

} // namespace fx
} // namespace visual

#endif // _SHADER_BUFFER_H
