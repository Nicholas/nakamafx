#pragma once

// Light structure
//
// Project   : NaKama-Fx
// File Name : Light.h
// Date      : 25/03/2019
// Author    : Nicholas Welters

#ifndef _LIGHT_TYPE_H
#define _LIGHT_TYPE_H

#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	/// <summary>
	/// Basic light structure that represents a direction light,
	/// a point light and a simple spot light.
	/// 
	/// This holds the world space values so that we can calculate obect lighting
	/// and then holds the same values in view space so that we can sort the lights
	/// in to view space bucket clusters for forward+ rendering.
	/// </summary>
	struct LightEx
	{
		glm::vec4 positionWS  = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
		glm::vec4 directionWS = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
		glm::vec4 positionVS  = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
		glm::vec4 directionVS = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
		glm::vec4 colour      = glm::vec4( 0, 0, 0, 0 );
		//--------------------------------------------------------------( 16 bytes )
		float    spotLightAngle = 0.0f;
		float    range          = 0.0f;
		uint32_t enabled        = 0;
		uint32_t type           = 0;
		//--------------------------------------------------------------( 16 bytes )
	};
	//--------------------------------------------------------------( 16 * 6 = 96 bytes )
	
	
	/// <summary> A buffer for each cluster to maintain a count and an offset to a light index list. </summary>
	struct LightCluster
	{
		/// <summary> The total number of lights in the cluster. </summary>
		unsigned int count = 0;

		/// <summary>
		/// The cluster offset in the light buffer to get the indecis
		/// of all the lights that affect the cluster.
		/// </summary>
		unsigned int offset = 0;
		//--------------------------------------------------------------( 8 bytes )
	};
} // namespace fx
} // namespace visual

#endif // _LIGHT_TYPE_H
