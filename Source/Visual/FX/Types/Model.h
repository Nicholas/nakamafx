#pragma once

// Model class
//
// Maintain the models position in the world.
//
// Project   : NaKama-Fx
// File Name : Model.cpp
// Date      : 05/06/2017
// Author    : Nicholas Welters


#ifndef _MODEL_H
#define _MODEL_H

#include <glm/glm.hpp>
#include <functional>

namespace visual
{
namespace fx
{
	class Model
	{
	public:
		/// <summary> ctor </summary>
		Model( ) = default;

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		Model( const Model& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		Model( Model&& move ) = default;


		/// <summary> dtor </summary>
		virtual ~Model( ) = 0;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		/// <returns> A referance to this. </returns>
		Model& operator=( const Model& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		/// <returns> A referance to this. </returns>
		Model& operator=( Model&& move ) = default;


		/// <summary> Update the model matrix that moves the mesh vertices to world space. </summary>
		void update( );

		/// <summary> Get the model position. </summary>
		/// <returns> The models world position. </returns>
		glm::vec3 getPosition( ) const;

		/// <summary> Set the model position. </summary>
		/// <param name="newPosition"> The models world position. </param>
		void setPosition( const glm::vec3& newPosition );

		/// <summary> Get the model orientation. Raw x, y, z rotation values. </summary>
		/// <returns> The models world roation. </returns>
		glm::vec3 getRotation( ) const;

		/// <summary> Set the model orientation. Raw x, y, z rotation values. </summary>
		/// <param name="newRotation"> The model orientation. </param>
		void setRotation( const glm::vec3& newRotation );

		/// <summary> Set the model orientation using polar coordinates. </summary>
		/// <param name="newRotation"> The model orientation in polar coordinates. </param>
		void setRotation( const glm::vec2& newRotation );

		/// <summary> Get the model size. </summary>
		/// <returns> The models world position. </returns>
		glm::vec3 getScale( ) const;

		/// <summary> Set the model size. </summary>
		/// <param name="scale"> The model size scale. </param>
		void setScale( const glm::vec3& newScale );

	protected:
		/// <summary> The callback that will be used to update the gpu data when the matrix is updated. </summary>
		/// <param name="world"> The models world matrix. </param>
		virtual void onMatrixChanged( const glm::mat4x4& world ) = 0;

	private:
		/// <summary> The model position. </summary>
		glm::vec3 position = { 0, 0, 0 };

		/// <summary> The models orientation. </summary>
		glm::vec3 rotation = { 0, 0, 0 };

		/// <summary> The model size. </summary>
		glm::vec3 scale    = { 1, 1, 1 };

		/// <summary> A dirty flag to see if we need to re calculate the model matrix. </summary>
		bool changed = true;
	};
} // namespace fx
} // namespace visual

#endif // _MODEL_H
