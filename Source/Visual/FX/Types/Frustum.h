#pragma once

// Frustum structures
//
// Project   : NaKama-Fx
// File Name : Frustum.h
// Date      : 25/03/2019
// Author    : Nicholas Welters

#ifndef _FRUSTUM_TYPT_H
#define _FRUSTUM_TYPT_H

#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	/// <summary> Frustum structure that holds 4 planes and a near and far depth. </summary>
	struct Frustum
	{
		/// <summary> The four planes defining left, right, top and bottom. </summary>
		glm::vec4 planes[ 4 ];
		//--------------------------------------------------------------( 64 bytes )

		/// <summary> The near plane depth. </summary>
		float zNear;

		/// <summary> The far plane depth. </summary>
		float zFar;
		//--------------------------------------------------------------( 8 bytes )
	};
} // namespace fx
} // namespace visual

#endif // _FRUSTUM_TYPT_H
