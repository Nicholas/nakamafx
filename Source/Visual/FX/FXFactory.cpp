#include "FXFactory.h"

// FX Factory
//
// A base factory that is used to start of the construction of the basic system.
//
// Project   : NaKama-Fx
// File Name : FXFactory.cpp
// Date      : 03/06/2018
// Author    : Nicholas Welters

#include "Enums.h"
#include "Settings.h"
#include "Camera/Camera.h"
#include "Camera/FreeFlyRig.h"
#include "Loaders/SettingsParser.h"
#include "Loaders/SceneParser.h"

#include "../UserInterface/Input.h"
#include "../UserInterface/WindowChange.h"
#include "../UserInterface/SystemUI.h"

#if defined( __linux__ ) || defined( VK_USE_PLATFORM_XCB_KHR )
#include "../UserInterface/WindowXCB.h"
#elif defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
#include "../UserInterface/WindowMS.h"
#include "../DirectX11/Factories/DataFactory.h"
#include "../DirectX11/CommandDispatch.h"
#include "../DirectX11/SwapChain/HwndSwapChain.h"
#include "../DirectX11/SceneRenderer.h"
#include "../DirectX11/Builders/SceneBuilder.h"
#endif

#include <Files/FileReader.h>

namespace visual
{
namespace fx
{
	using std::shared_ptr;
	using std::make_shared;
	using std::string;
	using std::function;

	using tools::FileReader;

	using ui::Input;
	using ui::InputSPtr;
	using ui::WindowSPtr;
	using ui::WindowChange;
	using ui::WindowChangeSPtr;
	using ui::SystemUI;
	using ui::SystemUISPtr;

#if defined( __linux__ ) || defined( VK_USE_PLATFORM_XCB_KHR )
#elif defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
	using directX11::DataFactory;
	//using directX11::CommandDispatchSPtr;
	using directX11::HwndSwapChainSPtr;
	using directX11::DataFactorySPtr;
	//using directX11::SceneRendererSPtr;
#endif


	/// <summary> Load the base application settings file. </summary>
	/// <param name="path"> The path to the settings file. </param>
	/// <returns> A pointer to the settings. </returns>
	SettingsSPtr FXFactory::loadSettings( const string& path ) const
	{
		FileReader settingsReader( [ ]( const function< void( ) >& fun ) { fun( ); } );
		const shared_ptr< SettingsParser > pSettingsParser = make_shared< SettingsParser >( );
		settingsReader.add( ".json", pSettingsParser );
		settingsReader.readFile( path );

		return pSettingsParser->getSettings( );
	}

	/// <summary> Create the application event executor. </summary>
	/// <param name="pSettings"> The application settings. </param>
	/// <returns> A pointer the applications core engine. </returns>
	ExecutorSPtr FXFactory::buildExecutor( const SettingsSPtr& pSettings ) const
	{
		ExecutorSPtr pEngine = make_shared< Executor >( pSettings->getNumberOfWorkerThreads( ) );

		pEngine->initialiseDispatcher(
			pSettings->getMinimumSyncJobsToExecute( ),
			pSettings->getMaximumSyncJobsToExecute( ),
			pSettings->getMinimumAsyncJobsToExecute( ),
			pSettings->getMaximumAsyncJobsToExecute( ) );

		return pEngine;
	}

	/// <summary> Create the application window that we are going to draw to. </summary>
	/// <param name="pSettings"> The application settings. </param>
	/// <returns> A pointer to the new window. </returns>
	WindowSPtr FXFactory::buildWindow( const SettingsSPtr& pSettings ) const
	{
#if defined( __linux__ ) || defined( VK_USE_PLATFORM_XCB_KHR )
		return make_shared< ui::WindowXCB >(
			"Linux Renderer Test",
			settings->getWindowWidth( ),
			settings->getWindowHeight( ) );
#elif defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
		HINSTANCE hInstance = static_cast< HINSTANCE >( GetModuleHandle( nullptr ) );
		return make_shared< ui::WindowMS >(
			hInstance,
			pSettings->getSystemName( ),
			pSettings->getBackBufferSize( ).x,
			pSettings->getBackBufferSize( ).y,
			pSettings->isFullscreen( ),
			RGBA8 );
#endif
	}

	/// <summary> Create an input listener so that we can get control input from the window. </summary>
	/// <returns> Create an input listener. </returns>
	InputSPtr FXFactory::buildInputListerner( ) const
	{
		return make_shared< Input >( );
	}

	/// <summary> Create the default system UI. </summary>
	/// <param name="pSettings"> The application settings. </param>
	/// <param name="pInput"> The window user input listener. </param>
	/// <returns> A system UI. </returns>
	ui::SystemUISPtr FXFactory::buildSystemUI( const SettingsSPtr& pSettings, const ui::InputSPtr& pInput ) const
	{
		return make_shared< SystemUI >( pInput, pSettings->getBackBufferSize( ) );
	}

	/// <summary> Create an window size change listener so that we can resize the render targets. </summary>
	/// <returns> A window size change listener. </returns>
	WindowChangeSPtr FXFactory::buildWindowChangeListener( ) const
	{
		return make_shared< WindowChange >( );
	}

	/// <summary> Create a camera set that we can use to move around and view the world. </summary>
	/// <param name="pSettings"> The application settings. </param>
	/// <param name="pInput"> The window user input listener. </param>
	/// <returns> A set of objects to control our camera. </returns>
	CameraSet FXFactory::buildCameraSet( const SettingsSPtr& pSettings, const InputSPtr& pInput ) const
	{
		CameraSet set;

		set.pCamera = make_shared< Camera >(
			pSettings->getBackBufferSize( ).x,
			pSettings->getBackBufferSize( ).y,
			0.01f,   // Near plane
			1000.0f, // Far Plane Rememebr that the sky box needs to be resized as well.
			85.0f ); // FOV

		set.pCameraRig = make_shared< FreeFlyRig >( set.pCamera, pInput );

		return set;
	}

	/**/
	/// <summary> Create the rendering components that will draw to the window. </summary>
	/// <param name="pSettings"> The application settings. </param>
	/// <param name="Camera"> The camera interface that we use to view the world. </param>
	/// <param name="pWindow"> The application window to draw to. </param>
	/// <param name="pInput"> The window user input listener. </param>
	/// <param name="pWindowChange"> The window change listener, allow us to resize the render targets. </param>
	/// <param name="pUI"> The user interface to render. </param>
	/// <returns> A set of objects that are used to update and draw a scene. </returns>
	FXSystem FXFactory::buildSystem( const SettingsSPtr& pSettings,
									 const CameraSPtr& pCamera,
									 const WindowSPtr& pWindow,
									 const InputSPtr& pInput,
									 const WindowChangeSPtr& pWindowChange,
									 const SystemUISPtr& pUI ) const
	{
		FXSystem system;


//#if defined( __linux__ ) || defined( VK_USE_PLATFORM_XCB_KHR )
//		const int api = 0;
//#elif defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
//		const int api = 1;
//#endif
//		if( api == 0 )
//		{
//			visual::vulkan::PlatformInfo platform =
//			{
//#ifdef VK_USE_PLATFORM_XCB_KHR
//				window.getConnection( ),
//				window.getWindow( ),
//				window.getVisualID( )
//#elif defined( VK_USE_PLATFORM_WIN32_KHR )
//				hInstance,
//				window.getHWnd( )
//#endif
//			};
//
//			factoryVK.Initialise( "Vulkan Test", "Vulkan Engine", platform );
//
//			visual::vulkan::CommandDispatchSPtr pVKExecutor = factoryVK.GetFXCommandExecutor( pSettings->getFrameBuffers( ) );
//
//			pSwapchainPass = factoryVK.CreateSwapChain(
//				platform,
//				pSettings->getWindowWidth( ),
//				pSettings->getWindowHeight( ),
//				pSettings->getFrameBuffers( ),
//				pVKExecutor
//			);
//
//			pDispatch = pVKExecutor;
//		}
//		else
		{
#if defined( WIN32 ) || defined( VK_USE_PLATFORM_WIN32_KHR )
			directX11::DataFactorySPtr pFactoryDX = make_shared< DataFactory >( );
			system.pFactoryDX = pFactoryDX;
			system.pFactoryDX->initialise( );

			directX11::CommandDispatchSPtr pExecutor = system.pFactoryDX->getFXCommandExecutor( pSettings->getFrameBuffers( ) );


			const HwndSwapChainSPtr pSwapChain = system.pFactoryDX->createSwapChain(
				pWindow->getHWnd( ),
				pSettings->getBackBufferSize( ).x,
				pSettings->getBackBufferSize( ).y,
				pSettings->getFrameBuffers( )
				);

			const directX11::SceneRendererSPtr pScene = system.pFactoryDX->createSceneRenderer(
				pSettings,
				pSwapChain,
				pExecutor,
				pCamera,
				pUI
			);

			pExecutor->addSwapChain( pSwapChain );

			system.pSceneRenderer = pScene;
			system.pDispatch      = pExecutor;

			directX11::SceneBuilderSPtr pBuilder = system.pFactoryDX->createSceneBuilder( pScene );
			system.pSceneBuilder = pBuilder;

			system.updateRenderTargets = [ pWindowChange, pFactoryDX, pScene, pExecutor, pWindow, pSettings ]( )
			{
				float width = float( pWindowChange->getWidth( ) );
				float height = float( pWindowChange->getHeight( ) );

				// Get the system to destroy the last swap chain before we build a new one.
				pExecutor->clearSwapChain( );

				directX11::OutputTargetSPtr pNull;
				pScene->setOutputTarget( pNull, width, height );

				// Build a new swap chain that we can render our final output to
				HwndSwapChainSPtr pSwapChain = pFactoryDX->createSwapChain(
					pWindow->getHWnd( ),
					pWindowChange->getWidth( ), pWindowChange->getHeight( ),
					pSettings->getFrameBuffers( )
					);

				pExecutor->addSwapChain( pSwapChain );
				pScene->setOutputTarget( pSwapChain->getOutputTarget( ), width, height );
			};

			system.updateScene = [ &pInput, pScene ]( float delta )
			{
				float scaler = 2.0f * delta;
				if( pInput->key( 'N' ) )
				{
					pScene->setGamma( pScene->getGamma( ) - 0.1f * scaler );
				}
				if( pInput->key( 'M' ) )
				{
					pScene->setGamma( pScene->getGamma( ) + 0.1f * scaler );
				}

				if( pInput->key( 'K' ) )
				{
					pScene->setBrightness( pScene->getBrightness( ) - 0.1f * scaler );
				}
				if( pInput->key( 'L' ) )
				{
					pScene->setBrightness( pScene->getBrightness( ) + 0.1f * scaler );
				}

				if( pInput->key( 'O' ) )
				{
					pScene->setContrast( pScene->getContrast( ) - 0.1f * scaler );
				}
				if( pInput->key( 'P' ) )
				{
					pScene->setContrast( pScene->getContrast( ) + 0.1f * scaler );
				}

				if( pInput->key( 'U' ) )
				{
					pScene->setSaturation( pScene->getSaturation( ) - 0.1f * scaler );
				}
				if( pInput->key( 'I' ) )
				{
					pScene->setSaturation( pScene->getSaturation( ) + 0.1f * scaler );
				}

				if( pInput->key( 'Z' ) )
				{
					pScene->setSunDir( pScene->getSunDir( ) - 0.1f * scaler );
				}
				if( pInput->key( 'X' ) )
				{
					pScene->setSunDir( pScene->getSunDir( ) + 0.1f * scaler );
				}

				static bool do1 = true;
				if( pInput->key( '1' ) && do1 )
				{
					pScene->toggleNormals( );
					do1 = false;
				}
				else if( !pInput->key( '1' ) && !do1 )
				{
					do1 = true;
				}

				static bool do2 = true;
				if( pInput->key( '2' ) && do2 )
				{
					pScene->toggleFPS( );
					do2 = false;
				}
				else if( !pInput->key( '2' ) && !do2 )
				{
					do2 = true;
				}

				static bool do3 = true;
				if( pInput->key( '3' ) && do3 )
				{
					pScene->toggleTimes( );
					do3 = false;
				}
				else if( !pInput->key( '3' ) && !do3 )
				{
					do3 = true;
				}
			};
#endif
		}

		return system;
	}
	//*/
} // namespace fx
} // namespace visual
