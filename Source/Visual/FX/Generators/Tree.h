#pragma once

// Tree Generator
//
// Using a set of peramerters, we define a family of trees from which we build a tree to render.
//
// Project   : NaKama-Fx
// File Name : Tree.h
// Date      : 20/06/2019
// Author    : Nicholas Welters

#ifndef _TREE_H
#define _TREE_H

#include <vector>
#include <functional>
#include <random>

#include "../Mesh.h"
#include "../Vertex.h"
#include "../Shapes.h"

#include <Macros.h>

#include <glm/gtx/euler_angles.hpp>

namespace visual
{
namespace fx
{
	using std::uniform_real_distribution;
	using std::default_random_engine;
	using glm::mat4x4;
	using glm::vec4;
	using glm::vec3;
	using glm::vec2;


	struct TreeGeometry
	{
		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer branchVertices = Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer( );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  branchIndecies = Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer( );

		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer leafVertices   = Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer( );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  leafIndecies   = Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer( );
	};

	struct TreeProperties
	{
		float shape      = 0;

		float baseSize   = 0.4f;

		float scale      = 13;
		float scaleV     = 0;

		float zScale     = 1;
		float zScaleV    = 0;

		float ratio      = 0.015f;
		float ratioPower = 1.2f;

		float lobes     = 5;
		float lobeDepth = 0.07f;

		float flare     = 0.6f;

		float tScale  = 1.0f;
		float tScaleV = 0.0f;

		float baseSplits = 0.0f;

		struct Curve
		{
			float length  = 1.0f;
			float lengthV = 0.0f;
			float taper   = 1.0f;

			float segSplits   =  0.0f;
			float splitAngle  = 45.0f;
			float splitAngleV =  0.0f;

			float curveRes    =  3.0f;
			float curve       =  0.0f;
			float curveBack   =  0.0f;
			float curveV      =  0.0f;
		};

		Curve properties = Curve( );

		struct Branch
		{
			float branches;
			float downAngle;
			float downAngleV;

			float rotate;
			float rotateV;

			Curve properties;
		};

		struct Leaf
		{
			float leaves         =  8.0f;
			float shape          =  0.0f;

			float scale          =  0.17f;
			float scaleX         =  1.0f;

			float attractionUp   =  0.5f;

			float pruneRatio     =  0.0f;

			float pruneWidth     =  0.5f;
			float pruneWidthPeak =  0.5f;

			float prunePowerLow  =  0.5f;
			float prunePowerHigh =  0.5f;

			float gap            = 0.1f;
			float gapV           = 0.2f;

			bool smooth = false;
		} leaves;

		std::vector< Branch > branches = {
			{
					10.00f, // branches

					60.00f, // downAngle;
				-50.00f, // downAngleV

					140.00f, // rotate;
					0.00f, // rotateV;
				{
					0.30f, // length
					0.00f, // lengthV

					1.00f, // taper

					0.80f,   // segSplit
					100.00f, // segSplitAngle
					20.00f,  // segSplitAngleV

					3.00f,  // curveRes
					-5.00f,  // curve
					0.00f,  // curveBack
					10.00f, // curveV
				}
			},
			/**/
			{
					4.00f, // branches

					45.00f, // downAngle;
					10.00f, // downAngleV

					140.00f, // rotate;
					0.00f, // rotateV;
				{
					0.60f, // length
					0.00f, // lengthV

					1.00f, // taper

					2.00f,   // segSplit
					100.00f, // segSplitAngle
					20.00f,  // segSplitAngleV

					3.00f,  // curveRes
					-50.00f,  // curve
					0.00f,  // curveBack
					10.00f, // curveV
				}
			},
			//*/
			/**
			{
					0.00f, // branches

					45.00f, // downAngle;
					10.00f, // downAngleV

					77.00f, // rotate;
					0.00f, // rotateV;
				{
					1.00f, // length
					0.00f, // lengthV

					1.00f, // taper

					0.00f,   // segSplit
					0.00f, // segSplitAngle
					0.00f,  // segSplitAngleV

					1.00f,  // curveRes
					0.00f,  // curve
					0.00f,  // curveBack
					0.00f, // curveV
				}
			}
			//*/
		};

	};

	enum ShapeRatios
	{
		CONICAL = 0,
		TAPPERED_CYLINDRICAL = 1,
		SPHERE,
		HEMISPHERICAL,
		CYLINDRICAL,
		FLAME,
		INVERSE_CONICAL,
		TEND_FLAME
	};

	/// <summary>
	/// A procidurally generated tree based on a set of parameters.
	/// </summary>
	class TreeBuilder
	{
	public:
		/// <summary> ctor </summary>
		TreeBuilder( );

		/// <summary> ctor </summary>
		/// <param name="seed"> A seed value  for the tree generator. </param>
		TreeBuilder( int seed );

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The settings object to copy. </param>
		TreeBuilder( const TreeBuilder& copy ) = default;

		/// <summary> move ctor </summary>
		/// <param name="move"> The settings object to move. </param>
		TreeBuilder( TreeBuilder&& move ) = default;

		/// <summary> dtor </summary>
		~TreeBuilder( ) = default;

		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The settings object to copy. </param>
		TreeBuilder& operator=( const TreeBuilder& copy ) = default;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The settings object to move. </param>
		TreeBuilder& operator=( TreeBuilder&& move ) = default;

		/// <summary> Generate a tree. </summary>
		void create( const TreeProperties& tree, TreeGeometry& geometry );

	private:
		/// <summary>
		/// Append the data from the mesh to the vertex and index
		/// buffers and translating by the matrix offset.
		/// </summary>
		void appendMesh(
			const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
			mat4x4 offset,
			Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer& vertices,
			Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer& indecies );;

		/// <summary>
		/// Calculate the curvature to add to the branch segment over the length of the branch.
		/// </summary>
		float curveBranch( const TreeProperties::Curve& properties, int level );

		/// <summary>
		/// Get the rotations for new segments when when a branch splits and creates clones.
		/// </summary>
		vec2 getSplitRotation( int i, const TreeProperties::Curve& properties, vec2 initialRotation, float upRightRatio, bool upright );

		void generateGrowthNode(
			mat4x4& directionMatrix, vec3& direction, vec3& position,
			const mat4x4& branchDirectionMatrix, const vec3& branchDirection,
			const vec3& branchOrigin, const float branchLength,
			const float growthOffset, const float growth,
			const float gapSize, const float rotationOffset,
			const vec2& growthRotation, const vec2& growthDownAngle );

		struct BranchProperties
		{
			unsigned int index;

			float length;
			float radius;

			int branches;
			int leaves;
		};

		struct SegmentProperties
		{
			unsigned int level;

			vec3 origin;
			vec2 rotation;

			float spawnP;
		};

		/// <summary>
		/// Add a new segment to the tree branch, this might spawn a clone and cause the tree to split.
		/// </summary>
		void spawnSegment(
			const TreeProperties& tree,
			TreeGeometry& geometry,
			const BranchProperties& branch,
			const SegmentProperties& segment,
			const TreeProperties::Curve& properties,
			float scale,
			bool trunk,
			vec3 spawnPoint );

		/// <summary>
		/// Add a segment to the tree and then recursivly add more segments, branches and leaves.
		/// </summary>
		void createBranch(
			const TreeProperties& tree,
			TreeGeometry& geometry,
			const BranchProperties& branch,
			const SegmentProperties& segment,
			const TreeProperties::Curve& properties,
			float scale,
			bool trunk );


	private:
		/// <summary> Shaping function scalers. </summary>
		std::vector< std::function< float ( float ) > > shapes;

		/// <summary> Rasndom -1 to 1. </summary>
		std::uniform_real_distribution< float > randomFloats;

		/// <summary> Rasndom 0 to 1. </summary>
		std::uniform_real_distribution< float > randomUFloats;

		/// <summary> Rasndom number generator. </summary>
		std::default_random_engine generator;
	};
} // namespace fx
} // namespace visual

#endif // _SETTINGS_H