#pragma once
// Here we are going to build a scene to display.
//
// Project   : NaKama-Tools
// File Name : SceneGenerator.h
// Date      : 28/06/2019
// Author    : Nicholas Welters

#ifndef _SCENE_GENERATOR_H
#define _SCENE_GENERATOR_H

#include <Macros.h>
#include <functional>

namespace tools
{
	typedef std::function< void( const std::function< void( ) >& ) > Scheduler;
}

namespace visual
{
	namespace fx
	{
		FORWARD_DECLARE( SceneBuilder );
		struct TreePromise;

		/// <summary>
		/// Here we are going to build a scene to display.
		/// </summary>
		class SceneGenerator
		{
			public:
				/// <summary> ctor </summary>
				/// <param name="scheduler"> A functor that will schedule another functor to execute. </param>
				/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
				SceneGenerator( const tools::Scheduler& schedule, const SceneBuilderSPtr& pBuilder );

				/// <summary> ctor </summary>
				SceneGenerator( ) = delete;

				/// <summary> copy ctor </summary>
				/// <param name="copy"> The object to copy. </param>
				SceneGenerator( const SceneGenerator& copy ) = default;

				/// <summary> move ctor </summary>
				/// <param name="move"> The temporary to move. </param>
				SceneGenerator( SceneGenerator&& move ) = default;


				/// <summary> dtor </summary>
				virtual ~SceneGenerator( ) = default;


				/// <summary> Copy assignment operator </summary>
				/// <param name="copy"> The object to copy. </param>
				SceneGenerator& operator=( const SceneGenerator& copy ) = default;

				/// <summary> Move assignment operator </summary>
				/// <param name="move"> The temporary to move. </param>
				SceneGenerator& operator=( SceneGenerator&& move ) = default;


				/// <summary> Generate a scene to display. </summary>
				void generateScene( );

			private:
				/// <summary> Generate a scene to display. </summary>
				/// <param name="seed"> A seed value for the random generators. </param>
				void generateScene( int seed );

				void setFog( );
				void setSky( );
				void setParticles( );
				void setStage( );
				void setWater( );
				void setLights( );
				void setText( );

				std::vector< TreePromise > setTrees( );
				void finaliseTrees( std::vector< TreePromise >& promises );

				/// <summary>
				/// A functor that will schedule another functor to execute.
				/// We are going to send the file loading commands to the executor in a controlled manner.
				/// </summary>
				tools::Scheduler schedule;

				/// <summary>
				/// The scene builder that will create our API specific objects.
				/// This will then forward the new scene to the system.
				/// </summary>
				SceneBuilderSPtr pBuilder;
		};
	} // namespace fx
} // namespace visual

# endif // _SCENE_PARSER_H