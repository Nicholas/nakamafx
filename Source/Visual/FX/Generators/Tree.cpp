
// Tree Generator
//
// Using a set of peramerters, we define a family of trees from which we build a tree to render.
//
// Project   : NaKama-Fx
// File Name : Tree.cpp
// Date      : 20/06/2019
// Author    : Nicholas Welters

#include "Tree.h"

namespace visual
{
namespace fx
{
	TreeBuilder::TreeBuilder( )
		: shapes
		{
			// Conical
			[]( float ratio ){ return 0.2f + 0.8f * ratio; },

			// Tappered
			[]( float ratio ){ return 0.5f + 0.5f * ratio; }
		},
		randomFloats( uniform_real_distribution< float >( -1.0, 1.0 ) )
		, randomUFloats( uniform_real_distribution< float >( 0.0, 1.0 ) )
		, generator( )
	{
		// 1560446953
		int seed = int( time( nullptr ) );
		printf( "---------------------------> Seed %d\n", seed );
		generator = default_random_engine( seed );
		//generator = default_random_engine( default_random_engine::default_seed );
	}

	TreeBuilder::TreeBuilder( const int seed )
		: shapes
		{
			// Conical
			[]( float ratio ){ return 0.2f + 0.8f * ratio; },

			// Tappered
			[]( float ratio ){ return 0.5f + 0.5f * ratio; }
		},
		randomFloats( uniform_real_distribution< float >( -1.0, 1.0 ) )
		, randomUFloats( uniform_real_distribution< float >( 0.0, 1.0 ) )
		, generator( default_random_engine( seed ) )
	{
	}

	/// <summary> Generate a tree. </summary>
	void TreeBuilder::create( const TreeProperties& tree, TreeGeometry& geometry )
	{
		BranchProperties branch = { unsigned( -1 ), 0.0f, 0.0f, tree.branches.empty( ) ? 0 : tree.branches[ 0 ].branches, 0 };
		SegmentProperties segment = { 0,{ 0, 0, 0 },{ 0, glm::pi< float >( ) * randomFloats( generator ) }, tree.properties.segSplits };

		float scale = tree.scale + tree.scaleV * randomFloats( generator );

		branch.length = ( tree.properties.length + tree.properties.lengthV * randomFloats( generator ) ) * scale;
		branch.radius = branch.length * tree.ratio * tree.tScale;

		createBranch( tree, geometry, branch, segment, tree.properties, scale, true );
	}

	/// <summary>
	/// Append the data from the mesh to the vertex and index
	/// buffers and translating by the matrix offset.
	/// </summary>
	void TreeBuilder::appendMesh( const Mesh<Vertex3f2f3f3f3f::Vertex>& mesh, mat4x4 offset, Mesh<Vertex3f2f3f3f3f::Vertex>::VertexBuffer& vertices, Mesh<Vertex3f2f3f3f3f::Vertex>::IndexBuffer& indecies )
	{
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer::value_type indexOffset = Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer::value_type( vertices.size( ) );

		for( const Vertex3f2f3f3f3f::Vertex& vertex : mesh.getVertexBuffer( ) )
		{
			vertices.emplace_back( vertex );
			vertices.back( ).position = offset * vec4( vertices.back( ).position, 1 );

			vertices.back( ).normal = offset * vec4( vertices.back( ).normal, 0 );
			vertices.back( ).tangent = offset * vec4( vertices.back( ).tangent, 0 );
			vertices.back( ).binormal = offset * vec4( vertices.back( ).binormal, 0 );
		}

		for( const Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer::value_type& index : mesh.getIndexBuffer( ) )
		{
			indecies.emplace_back( index + indexOffset );
		}
	}

	/// <summary>
	/// Calculate the curvature to add to the branch segment over the length of the branch.
	/// </summary>
	float TreeBuilder::curveBranch( const TreeProperties::Curve& properties, int level )
	{
		float xRotationRandom = glm::radians< float >( properties.curveV / properties.curveRes ) * randomFloats( generator );

		if( properties.curveBack == 0 )
		{
			return glm::radians< float >( properties.curve / properties.curveRes );// + xRotationRandom;
		}
		else
		{
			int halfWay = int( round( properties.curveRes * 0.5f ) );
			float curve = level < halfWay ? properties.curve : -properties.curveBack;

			return glm::radians< float >( curve / ( properties.curveRes * 0.5f ) ) + xRotationRandom;
		}
	}

	/// <summary>
	/// Get the rotations for new segments when when a branch splits and creates clones.
	/// </summary>
	vec2 TreeBuilder::getSplitRotation( int i, const TreeProperties::Curve& properties, vec2 initialRotation, float upRightRatio, bool upright )
	{
		vec2 spawnRotation = initialRotation;

		float xRotationRandom = glm::radians< float >( properties.splitAngleV ) * randomFloats( generator );
		float yRotationRandom = glm::radians< float >( properties.splitAngleV ) * randomFloats( generator );

		float rotation = glm::radians< float >( properties.splitAngle );

		float t = glm::radians< float >( 20 ) + 0.75 * ( glm::radians< float >( 30 ) + abs( initialRotation.x - glm::pi< float >( ) ) * pow( randomUFloats( generator ), 2 ) );
		spawnRotation.y = t - glm::pi< float >( );
		spawnRotation.y *= randomFloats( generator ) > 0 ? 1 : -1;
		//if( upright )
		//{
		//	spawnRotation.x += i * upRightRatio + yRotationRandom;
		//}
		//else
		{
			spawnRotation.x += ( i * ( rotation + yRotationRandom ) ) - ( rotation * 0.5f * ( properties.segSplits ) );
		}

		return spawnRotation;
	}

	void TreeBuilder::generateGrowthNode( mat4x4& directionMatrix, vec3& direction, vec3& position, const mat4x4& branchDirectionMatrix, const vec3& branchDirection, const vec3& branchOrigin, const float branchLength, const float growthOffset, const float growth, const float gapSize, const float rotationOffset, const vec2& growthRotation, const vec2& growthDownAngle )
	{
		float down = growthDownAngle.x + growthDownAngle.y * randomFloats( generator );
		float roll = 180 * rotationOffset + growthRotation.x * growth + growthRotation.y * randomFloats( generator );
		//float roll = growthRotation.x * growth + growthRotation.y * randomFloats( generator );
		directionMatrix = branchDirectionMatrix * glm::eulerAngleYX( glm::radians< float >( roll ), glm::radians< float >( -down ) );

		direction = normalize( directionMatrix * vec4( 0, 1, 0, 0 ) );

		position = branchOrigin + branchDirection * ( gapSize + 1 ) * branchLength * growthOffset; // TODO: Some random position along the branch
																								   //position = branchOrigin + branchDirection * branchLength * growthOffset; // TODO: Some random position along the branch
		position += direction * gapSize;
	}

	/// <summary>
	/// Add a new segment to the tree branch, this might spawn a clone and cause the tree to split.
	/// </summary>
	void TreeBuilder::spawnSegment( const TreeProperties& tree, TreeGeometry& geometry, const BranchProperties& branch, const SegmentProperties& segment, const TreeProperties::Curve& properties, float scale, bool trunk, vec3 spawnPoint )
	{
		int spawnMax = int( segment.spawnP );
		float spawnP( segment.spawnP - spawnMax );

		int splits = spawnMax * ( spawnP > randomUFloats( generator ) ? 1 : 0 );
		float splitP = spawnMax + spawnP / splits;

		if( trunk && segment.level == 0 )
		{
			splits = int( tree.baseSplits );
			splitP = segment.spawnP;
		}

		if( splits == 0 )
		{
			vec2 spawnRotation = segment.rotation;
			spawnRotation.x += curveBranch( properties, segment.level );

			SegmentProperties nextSegment = { segment.level + 1, spawnPoint, spawnRotation, segment.spawnP };
			createBranch( tree, geometry, branch, nextSegment, properties, scale, trunk );
		}
		else
		{
			bool upRight = tools::almostEqual( segment.rotation.x, 0 );
			float upRightRatio = glm::two_pi< float >( ) / ( splits + 1 );

			//float declination = acos( branchDirection.y );
			//float angleSplitV = glm::radians< float >( tree.splitAngle0V ) * randomFloats( generator );
			//float angleSplit = std::max( glm::radians< float >( tree.splitAngle0 - declination ) + angleSplitV, 0.0f );

			for( int i = 0; i < splits + 1; ++i )
			{
				vec2 spawnRotation = getSplitRotation( i, properties, segment.rotation, upRightRatio, upRight );
				spawnRotation.x += curveBranch( properties, segment.level );

				SegmentProperties nextSegment = { segment.level + 1, spawnPoint, spawnRotation, splitP };
				createBranch( tree, geometry, branch, nextSegment, properties, scale, trunk );
			}
		}
	}

	/// <summary>
	/// Add a segment to the tree and then recursivly add more segments, branches and leaves.
	/// </summary>
	void TreeBuilder::createBranch( const TreeProperties& tree, TreeGeometry& geometry, const BranchProperties& branch, const SegmentProperties& segment, const TreeProperties::Curve& properties, float scale, bool trunk )
	{
		float curveRes = trunk ? tree.properties.curveRes : tree.branches[ branch.index ].properties.curveRes;
		if( segment.level >= curveRes )
		{
			return;
		}

		// Build Trunk
		float length = branch.length / curveRes;
		float radius = branch.radius;

		float minRadus = 0.01f;
		float gapOffset1 = 0;
		float gapOffset2 = 0;

		float segmentDelta = ( 1 / curveRes );
		float segmentMax = segmentDelta * ( curveRes - segment.level );


		// 1 -> Position Branch Segment.
		mat4x4 rotationMatrix = glm::eulerAngleYX( segment.rotation.y, segment.rotation.x );
		vec3 branchDirection = normalize( rotationMatrix * vec4( 0, 1, 0, 0 ) );

		//mat4x4 localMatrix = glm::translate( mat4x4( 1 ), segment.origin );
		mat4x4 localMatrix = glm::translate( mat4x4( 1 ), segment.origin + branchDirection * gapOffset1 * 0.5f );
		localMatrix *= rotationMatrix;

		int faces = trunk ? segment.level == 0 ? 24 : 12 : 4;
		int slices = trunk && segment.level == 0 ? 24 : 2;

		// 2 -> Build and add a branch to the final mesh.
		Mesh< Vertex3f2f3f3f3f::Vertex > mesh;

		if( trunk )
		{
			mesh = Shapes::cylinder( faces, slices, length,
			[ tree, radius, slices, segmentDelta, segmentMax, minRadus ]( int face, int slice )
			{
				float Z = segmentMax - ( segmentDelta * slice / slices );

				float r = Z * radius + minRadus;
				float Y = 1 - 8 * ( 1 - Z );
				float flare = tree.flare * ( pow( 100.0f, Y ) - 1 ) / 100.0f + 1.0f;

				return r * flare;
			} );
		}
		else
		{
			mesh = Shapes::cylinder( faces, slices, length,
			[ tree, radius, slices, segmentDelta, segmentMax, minRadus ]( int face, int slice )
			{
				float Z = segmentMax - ( segmentDelta * slice / slices );
				float r = Z * radius + minRadus;

				return r;
			} );
		}

		appendMesh( mesh, localMatrix, geometry.branchVertices, geometry.branchIndecies );

		/**/
		spawnSegment(
			tree, geometry,
			branch, segment,
			properties,
			scale,
			trunk,
			segment.origin + branchDirection * gapOffset2 + branchDirection * length
		);
		//*/


		/**/
		// Create spawn points along the trees length
		int branchIndex = branch.index + 1;
		if( branchIndex < tree.branches.size( ) )
		{
			float rotationOffset = randomFloats( generator );

			for( int i = 0; i < branch.branches; ++i )
			{
				mat4x4 lRotationMatrix;
				vec3 direction;
				vec3 spawnPoint;
				float segmentOffset = ( branch.branches - i ) / float( branch.branches );
				float segmentRadius = ( segmentMax - ( segmentDelta * segmentOffset ) ) * radius;

				generateGrowthNode(
					lRotationMatrix,
					direction,
					spawnPoint,
					rotationMatrix,
					branchDirection,
					segment.origin,
					length,
					segmentOffset,
					float( i ),
					0,
					rotationOffset,
					{ tree.branches[ branchIndex ].rotate, tree.branches[ branchIndex ].rotateV },
					{ tree.branches[ branchIndex ].downAngle, tree.branches[ branchIndex ].downAngleV } );

				vec2 spawnRotation = { acos( direction.y ), atan2( direction.x, direction.z ) };

				float branchRatio = segmentDelta * ( segmentOffset + segment.level );

				float lengthBase = tree.baseSize * scale;
				float childLengthMax = tree.branches[ branchIndex ].properties.length + tree.branches[ branchIndex ].properties.lengthV * randomFloats( generator );

				int subStems = tree.branches.size( ) > branchIndex + 1 ? int( tree.branches[ branchIndex + 1 ].branches ) : 0;

				BranchProperties stem = { unsigned( branchIndex ), 0.0f, 0.0f, 0, 0 };
				float branchOffset = branchRatio * branch.length;

				if( trunk )
				{
					float ratio = ( branch.length - branchOffset ) / ( branch.length - lengthBase );
					if( ratio < 0 || ratio > 1 )
					{
						continue;
					}

					float shapeScale = shapes[ CONICAL ]( abs( ratio ) );
					stem.length = childLengthMax * branch.length * shapeScale;
					stem.branches = subStems * ( 0.2f + 0.8f * stem.length / branch.length / childLengthMax );
				}
				else
				{
					stem.length = childLengthMax * ( branch.length - 0.6f * ( branchOffset ) );
					stem.branches = subStems * ( 1.0f + 0.5f * ( branch.length - branchOffset ) / branch.length / childLengthMax );
				}

				stem.radius = segmentRadius * pow( ( stem.length / branch.length ), tree.ratioPower );

				stem.branches = std::max( stem.branches, 1 );
				stem.leaves = std::max( tree.leaves.leaves * shapes[ TAPPERED_CYLINDRICAL ]( childLengthMax * ( branch.length - branchOffset ) ), 1.0f );

				SegmentProperties nextSegment = { 0, spawnPoint, spawnRotation, tree.branches[ stem.index ].properties.segSplits };
				//SegmentProperties nextSegment = { segment.level, spawnPoint, spawnRotation, tree.branches[ branch.index ].properties.segSplits };
				createBranch( tree, geometry, stem, nextSegment, tree.branches[ stem.index ].properties, scale, false );
			}
		}
		//*/

		// Leaves
		/**/
		if( branchIndex == tree.branches.size( ) )
		{
			int leafIndex = std::min( branchIndex - 1, 2 );
			float quality = 1.0f;// * ( 1 - offset );
			float leavesPerBranch = quality * branch.leaves;

			//float length = tree.leaves.scale / sqrt( quality );
			//float width  = tree.leaves.scaleX * length;
			//
			//float envelopeTop    = tree.scale;
			//float envelopeBottom = tree.scale * tree.baseSize;
			////float envelopeWidth  = ;
			//
			//float r = glm::length( vec3( spawnOffset ) );
			//float ratio = ( tree.scale - z ) / ( tree.scale * ( 1 - tree.baseSize ) );
			//
			//float scaleRatio = ratio >= ( 1 - tree.leaves.pruneWidthPeak ) ? 0 : ( ratio < ( 1 - tree.leaves.pruneWidthPeak ) ?
			//					pow(       ratio   / ( 1 - tree.leaves.pruneWidthPeak ), tree.leaves.prunePowerHigh ) :
			//					pow( ( 1 - ratio ) / ( 1 - tree.leaves.pruneWidthPeak ), tree.leaves.prunePowerLow ) );
			//bool inside = ( r / tree.scale ) < ( tree.leaves.pruneWidth * scaleRatio );
			//
			//float declination = acos( direction.y );
			//float orientation = atan2( direction.x, direction.z );
			//
			//float curveUp = tree.leaves.attractionUp * declination * cos( orientation ) / tree.branches[ branch ].curveRes;

			float rotationOffset = randomFloats( generator );

			for( int i = 0; i < leavesPerBranch; i++ )
			{
				float childOffset = ( i + 1 ) / ( leavesPerBranch + 1 );
				float childRadius = ( segmentMax - ( segmentDelta * childOffset ) ) * radius;

				mat4x4 lRotationMatrix;
				vec3 leafDirection;
				vec3 spawnOffset;

				generateGrowthNode(
					lRotationMatrix,
					leafDirection,
					spawnOffset,
					rotationMatrix,
					branchDirection,
					segment.origin,
					length,
					1.0f - ( i / leavesPerBranch ),
					float( i ),
					//0,//tree.branches[ branch ].gap,
					( segmentMax - segmentDelta ) * radius,//tree.branches[ branch ].gap,
					rotationOffset,
					{ tree.branches[ leafIndex ].rotate, tree.branches[ leafIndex ].rotateV },
					{ tree.branches[ leafIndex ].downAngle, tree.branches[ leafIndex ].downAngleV } );

				//vec3 leafPosition;
				//vec3 leafNormal;
				//
				//float thetaPosition = atan2( leafPosition.z, leafPosition.x );
				//float thetaBend = thetaPosition - atan2( leafNormal.z, leafNormal.x );
				//
				//float rotationZ = bend * thetaBend;
				//
				//float phiBend = atan2( glm::length( vec2( leafNormal.x, leafNormal.z ) ), leafNormal.y );
				// Rotate Y -orientation
				// Rotate X bend * phiBend
				// Rotate Y orientation

				mat4x4 lScale = glm::scale( mat4x4( 1 ), vec3( 1, 1, 1 ) * tree.leaves.scale );
				mat4x4 lMatrix = glm::translate( mat4x4( 1 ), spawnOffset + leafDirection * ( childRadius + tree.leaves.gap + tree.leaves.gapV * randomFloats( generator ) ) ) * lScale;
				lMatrix *= lRotationMatrix;

				// 2 -> Build and add a branch to the final mesh.
				if( tree.leaves.smooth )
				{
					appendMesh( Shapes::smoothArrowQuad( ), lMatrix, geometry.leafVertices, geometry.leafIndecies );
				}
				else
				{
					appendMesh( Shapes::arrowQuad( ), lMatrix, geometry.leafVertices, geometry.leafIndecies );
				}
			}
		}
		//*/
	}

} // namespace fx
} // namespace visual
