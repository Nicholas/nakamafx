#include "SceneGenerator.h"
// Here we are going to build a scene to display.
//
// Project   : NaKama-Tools
// File Name : SceneGenerator.cpp
// Date      : 28/06/2019
// Author    : Nicholas Welters

#include "../Loaders/SceneBuilder.h"

#include "../Shapes.h"
#include "../Types/ConstantBuffers.h"
#include "../Types/Light.h"
#include "../Generators/Tree.h"

#include <iomanip>
#include <future>

#ifndef PRINT_PARSER_DATA
#define PRINT_PARSER_DATA( STUFF ) \
	std::cout << STUFF
#endif // PRINT_PARSER_DATA

namespace visual
{
	namespace fx
	{
		using tools::Scheduler;
		using fx::Mesh;
		using fx::VertexParticle;
		using fx::GSParticleBuffer;
		using fx::PSParticleBuffer;
		using std::cout;
		using std::endl;
		using std::bind;
		using std::move;
		using std::function;
		using std::vector;
		using std::unordered_map;
		using std::string;
		using std::to_string;
		using std::uniform_real_distribution;
		using std::uniform_int_distribution;
		using std::default_random_engine;
		using glm::vec4;
		using glm::vec3;
		using glm::vec2;
		using glm::ivec3;
		using glm::ivec2;
		using glm::uvec4;
		using glm::mat3x3;
		using glm::mat4x4;

		namespace param = std::placeholders;

		typedef std::promise< TreeGeometry > TreeGeometryPromise;
		typedef std::shared_ptr< TreeGeometryPromise > TreeGeometryPromiseSPtr;
		struct Material
		{
			PSObjectBuffer ps = PSObjectBuffer();
			vector< string > textures = vector< string >( 9 );
		};
		struct TreePromise
		{
			vec3 position = { 0, 0, 0 };
			vec3 rotation = { 0, 0, 0 };
			vec3 scale    = { 1, 1, 1 };
			Material bark;
			Material leaf;
			TreeProperties tree;
			TreeGeometryPromiseSPtr geometry;
		};

		/// <summary> ctor </summary>
		/// <param name="scheduler"> A functor that will schedule another functor to execute. </param>
		/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
		SceneGenerator::SceneGenerator( const Scheduler& schedule, const SceneBuilderSPtr& pBuilder )
			: schedule( schedule )
			, pBuilder( pBuilder )
		{ }

		/// <summary> Generate a scene to display. </summary>
		void SceneGenerator::generateScene( )
		{
			// A start to building obects at the same time...
			schedule( [ this ]( ) -> void
			{
				generateScene( 0 );
			} );
		}

		/// <summary> Generate a scene to display. </summary>
		/// <param name="seed"> A seed value for the random generators. </param>
		void SceneGenerator::generateScene( int seed )
		{
			cout << "Generating Scene - Stared." << endl << endl;

			std::vector< TreePromise > treePromises = setTrees( );

			setFog( );
			setSky( );
			setParticles( );
			setStage( );
			setWater( );
			setLights( );
			setText( );

			finaliseTrees( treePromises );

			pBuilder->bake( );
			PRINT_PARSER_DATA( endl << "Generating Scene - Complete." << endl << endl );
		}

		void SceneGenerator::setFog( )
		{
			//float density       =   0.0f;
			float density       =   0.05f;
			float heightFalloff =   0.01f;
			float rayDepth      = 128.00f;
			float gScatter      =   0.25f;
			float waterHeight   =  -0.50f;

			pBuilder->addFog( density, heightFalloff, rayDepth, gScatter, waterHeight );
		}

		void SceneGenerator::setSky( )
		{
			vec3 sunDirection( 0, 0.5, 0 );
			vector< string > textures = { "SingleTexelBlack" };

			pBuilder->addBasicVolumeSkybox( textures, sunDirection );
		}

		void SceneGenerator::setParticles( )
		{
			Mesh< VertexParticle::Vertex >::VertexBuffer initialState;
			GSParticleBuffer gsProperties;
			PSParticleBuffer psProperties;

			vector< string > textures = { "DefaultDot" };
			unsigned int bufferSize   = 32768;
			string blend              = "add";

			gsProperties.timeStep = 0;
			gsProperties.gameTime = 0;

			psProperties.imageCount = { 1, 1 };
			psProperties.frameRate  = 24;
			psProperties.frameCount = 1;

			gsProperties.emitPosW         = { 0, 1, 0 };
			gsProperties.emitDirW         = { 0, 0, 0 };
			gsProperties.flareSize        = { 0.1f, 0.1f };
			gsProperties.emitPosWScale    = { 30.0f, 2.3f, 30.0f };
			gsProperties.emitDirWScale    = { 0.2f, 0.2f, 0.2f };
			gsProperties.emitTime         = 0.01f;
			gsProperties.lifeTime         = 10.0f;
			gsProperties.ageLifeSizeScale = { 0, 0 };

			gsProperties.attractor        = { 0.0, 0.005, 0.0, 0.0 };

			gsProperties.startColour      = { 1.0, 1.0, 1.0, 0.0 };
			gsProperties.endColour        = { 1.0, 1.0, 1.0, 1.0 };
			gsProperties.starColourScale  = { 5.0, 5.0, 5.0, 1.0 };
			gsProperties.endColourScale   = { 0.0, 0.0, 0.0, 0.0 };

			psProperties.imageCount = { 1, 1 };
			psProperties.frameRate  = 24;
			psProperties.frameCount = 1;


			vec3 velocity = { 0, 0, 0 };
			vec2 size = { 0, 0 };
			float age = 40;

			initialState.emplace_back( vec3(   0.0, 0.0,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(   0.0, 0.0 , -30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(   0.0, 0.0 ,  30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  30.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -30.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  30.0, 0.0 , -30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -30.0, 0.0 ,  30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  30.0, 0.0 ,  30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -30.0, 0.0 , -30 ), velocity, size, age, 0.0f, 0 );

			//initialState.emplace_back( vec3(   0.0, 0.0 , -15 ), velocity, size, age, 0.0f, 0 );
			//initialState.emplace_back( vec3(   0.0, 0.0 ,  15 ), velocity, size, age, 0.0f, 0 );
			//initialState.emplace_back( vec3(  15.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			//initialState.emplace_back( vec3( -15.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			//initialState.emplace_back( vec3(  15.0, 0.0 , -15 ), velocity, size, age, 0.0f, 0 );
			//initialState.emplace_back( vec3( -15.0, 0.0 ,  15 ), velocity, size, age, 0.0f, 0 );
			//initialState.emplace_back( vec3(  15.0, 0.0 ,  15 ), velocity, size, age, 0.0f, 0 );
			//initialState.emplace_back( vec3( -15.0, 0.0 , -15 ), velocity, size, age, 0.0f, 0 );


			pBuilder->addParticleSystem(
				initialState,
				textures,
				gsProperties,
				psProperties,
				bufferSize,
				blend
			);

			/**
			Mesh< VertexParticle::Vertex >::VertexBuffer initialState;
			GSParticleBuffer gsProperties;
			PSParticleBuffer psProperties;

			vector< string > textures = { "DefaultDot" };
			unsigned int bufferSize   = 32768;
			string blend              = "add";

			gsProperties.timeStep = 0;
			gsProperties.gameTime = 0;

			psProperties.imageCount = { 1, 1 };
			psProperties.frameRate  = 24;
			psProperties.frameCount = 1;

			gsProperties.emitPosW         = { 0, 4, 0 };
			gsProperties.emitDirW         = { 0, 0, 0 };
			gsProperties.flareSize        = { 0.1f, 0.1f };
			gsProperties.emitPosWScale    = { 30.0f, 2.3f, 30.0f };
			gsProperties.emitDirWScale    = { 0.2f, 0.2f, 0.2f };
			gsProperties.emitTime         = 0.01f;
			gsProperties.lifeTime         = 10.0f;
			gsProperties.ageLifeSizeScale = { 0, 0 };

			gsProperties.attractor        = { 0.0, 0.005, 0.0, 0.0 };

			gsProperties.startColour      = { 1.0, 1.0, 1.0, 0.0 };
			gsProperties.endColour        = { 1.0, 1.0, 1.0, 1.0 };
			gsProperties.starColourScale  = { 5.0, 5.0, 5.0, 1.0 };
			gsProperties.endColourScale   = { 0.0, 0.0, 0.0, 0.0 };

			psProperties.imageCount = { 1, 1 };
			psProperties.frameRate  = 24;
			psProperties.frameCount = 1;


			vec3 velocity = { 0, 0, 0 };
			vec2 size = { 0, 0 };
			float age = 40;

			initialState.emplace_back( vec3(   0.0, 0.25,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(   0.0, 0.0 , -30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(   0.0, 0.0 ,  30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  30.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -30.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  30.0, 0.0 , -30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -30.0, 0.0 ,  30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  30.0, 0.0 ,  30 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -30.0, 0.0 , -30 ), velocity, size, age, 0.0f, 0 );

			initialState.emplace_back( vec3(   0.0, 0.0 , -15 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(   0.0, 0.0 ,  15 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  15.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -15.0, 0.0 ,   0 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  15.0, 0.0 , -15 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -15.0, 0.0 ,  15 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3(  15.0, 0.0 ,  15 ), velocity, size, age, 0.0f, 0 );
			initialState.emplace_back( vec3( -15.0, 0.0 , -15 ), velocity, size, age, 0.0f, 0 );


			pBuilder->addParticleSystem(
				initialState,
				textures,
				gsProperties,
				psProperties,
				bufferSize,
				blend
			);
			//*/
		}

		void SceneGenerator::setStage( )
		{
			vector< string > textures = {
				"SingleTexelWhite",
				"BlankNormal",
				"SingleTexelWhite",
				"SingleTexelWhite",
				"SingleTexelWhite",
				"SingleTexelBlack",
				"SingleTexelBlack",
				"SingleTexelWhite",
				"SingleTexelBlack"
			};
			vector< string > texturesMetal = {
				"SingleTexelWhite", // Colour
				"BlankNormal",		// Normal
				"SingleTexelWhite",	// Depth
				"SingleTexelWhite",	// AO
				"SingleTexelWhite",	// BUMP
				"SingleTexelBlack", // GLOSS
				"SingleTexelBlack", // GLOSS Hi
				"SingleTexelWhite",	// Roughness
				"SingleTexelWhite"	// Metalness
			};
			vector< string > texturesMetalDots = {
				"SingleTexelWhite", // Colour
				"VoronoiN",			// Normal
				"VoronoiD",			// Depth
				"SingleTexelWhite",	// AO
				"SingleTexelWhite",	// BUMP
				"SingleTexelBlack", // GLOSS
				"SingleTexelBlack", // GLOSS Hi
				"SingleTexelBlack",	// Roughness
				"VoronoiM"			// Metalness
			};

			PSObjectBuffer psProperties;

			psProperties.ka         = { 0, 0, 0 };
			psProperties.kd         = { 1.0, 1.0, 1.0 };
			psProperties.ks         = { 1.0, 1.0, 1.0 };
			psProperties.bumpScale  = { 1, 1 };
			psProperties.ssPower    = 0.0f;
			psProperties.ssScale    = 0.0f;
			psProperties.ior        = 0.0f;
			psProperties.sAlpha     = 1.0f;
			psProperties.ssAlpha    = 0.0f;
			psProperties.absorption = 0.0f;
			psProperties.metalness  = 0.0f;
			psProperties.roughness  = 0.0f;

			vec3 rotation = { 0, 0, 0 };

			auto addCube = [ this, &rotation, &textures, &psProperties ]
			( const vec3& position, const vec3& scale )
			{
				pBuilder->addCube( position, rotation, scale, textures, psProperties );
			};

			auto addTranslucentCube = [ this, &textures, &psProperties ]
			( const int i, const vec3& position, const vec3& scale, const vec3& rotation )
			{
				//pBuilder->addCube( position, rotation, scale, textures, psProperties );
				pBuilder->addTranslucentMesh(
					"Demo Translucent Cube " + to_string( i ),
					Shapes::testSlate( ),
					position, rotation, scale,
					textures, psProperties );
			};

			auto addStand = [ this, &rotation, &textures, &psProperties ]
			( const vec3& position )
			{
				pBuilder->addCube( position, rotation, vec3( 10, 0.5, 10 ), textures, psProperties );
				pBuilder->addCube( position, rotation, vec3(  2, 2.0,  2 ), textures, psProperties );
			};

			auto addStand2 = [ this, &rotation, &psProperties ]
			( int i, const vec3& position, const vec3& scale, const vec3& colour, const vector< string >& textures, bool parralax )
			{
				//pBuilder->addCube( position, rotation, vec3( 10, 0.5, 10 ), textures, psProperties );
				//pBuilder->addCube( position, rotation, vec3(  2, 2.0,  2 ), textures, psProperties );

				Mesh< Vertex3f2f3f3f3f::Vertex > mesh = Shapes::testSlate( );

				//Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer stdCubeVBuffer = mesh.getVertexBuffer( );
				Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( mesh.getVertexBuffer( ).size( ) );

				for( int i = 0; i < mesh.getVertexBuffer( ).size( ); i++ )
				{
					vBuff[ i ] = mesh.getVertexBuffer( )[ i ];
				}

				vec2 XZScale( scale.x, scale.z );
				vec2 XYScale( scale.x, scale.y );
				vec2 ZYScale( scale.z, scale.y );

				vec2 XZOffset( position.x, position.z );
				vec2 XYOffset( position.x, position.y );
				vec2 ZYOffset( position.z, position.y );

				vBuff[ 0 ].textureUV = vBuff[ 0 ].textureUV * XZScale + XZOffset;
				vBuff[ 1 ].textureUV = vBuff[ 1 ].textureUV * XZScale + XZOffset;
				vBuff[ 2 ].textureUV = vBuff[ 2 ].textureUV * XZScale + XZOffset;
				vBuff[ 3 ].textureUV = vBuff[ 3 ].textureUV * XZScale + XZOffset;

				vBuff[ 4 ].textureUV = vBuff[ 4 ].textureUV * XZScale + XZOffset;
				vBuff[ 5 ].textureUV = vBuff[ 5 ].textureUV * XZScale + XZOffset;
				vBuff[ 6 ].textureUV = vBuff[ 6 ].textureUV * XZScale + XZOffset;
				vBuff[ 7 ].textureUV = vBuff[ 7 ].textureUV * XZScale + XZOffset;


				vBuff[ 8  ].textureUV = vBuff[ 8  ].textureUV * ZYScale + ZYOffset;
				vBuff[ 9  ].textureUV = vBuff[ 9  ].textureUV * ZYScale + ZYOffset;
				vBuff[ 10 ].textureUV = vBuff[ 10 ].textureUV * ZYScale + ZYOffset;
				vBuff[ 11 ].textureUV = vBuff[ 11 ].textureUV * ZYScale + ZYOffset;

				vBuff[ 12 ].textureUV = vBuff[ 12 ].textureUV * ZYScale + ZYOffset;
				vBuff[ 13 ].textureUV = vBuff[ 13 ].textureUV * ZYScale + ZYOffset;
				vBuff[ 14 ].textureUV = vBuff[ 14 ].textureUV * ZYScale + ZYOffset;
				vBuff[ 15 ].textureUV = vBuff[ 15 ].textureUV * ZYScale + ZYOffset;


				vBuff[ 16 ].textureUV = vBuff[ 16 ].textureUV * XYScale + XYOffset;
				vBuff[ 17 ].textureUV = vBuff[ 17 ].textureUV * XYScale + XYOffset;
				vBuff[ 18 ].textureUV = vBuff[ 18 ].textureUV * XYScale + XYOffset;
				vBuff[ 19 ].textureUV = vBuff[ 19 ].textureUV * XYScale + XYOffset;

				vBuff[ 20 ].textureUV = vBuff[ 20 ].textureUV * XYScale + XYOffset;
				vBuff[ 21 ].textureUV = vBuff[ 21 ].textureUV * XYScale + XYOffset;
				vBuff[ 22 ].textureUV = vBuff[ 22 ].textureUV * XYScale + XYOffset;
				vBuff[ 23 ].textureUV = vBuff[ 23 ].textureUV * XYScale + XYOffset;

				psProperties.ks = colour;
				psProperties.roughness = 0.5f;
				psProperties.metalness = 0.99f;

				if( parralax )
				{
					pBuilder->addMesh( "Stand " + to_string( i ),
						{ vBuff, mesh.getIndexBuffer( ), mesh.getTopology( ) },
						position, rotation, scale,
						textures, psProperties );
				}
				else
				{
					pBuilder->addBasicMesh( "Stand " + to_string( i ),
						{ vBuff, mesh.getIndexBuffer( ), mesh.getTopology( ) },
						position, rotation, scale,
						textures, psProperties );
				}
			};

			// Floor and center piller
			//pBuilder->addCube( vec3(   0, -2.5,   0 ), rotation, vec3( 1024.0,  1.0, 1024.0 ), texturesV, psProperties );


			addStand2( 4, { -36,  1.0, -36 }, { 1, 1, 1 }, { 1.10f, 0.71f, 0.29f }, texturesMetalDots, true );
			addStand2( 5, {  36,  1.0,  36 }, { 1, 1, 1 }, { 0.95f, 0.64f, 0.54f }, texturesMetalDots, true );
			addStand2( 6, { -36,  1.0,  36 }, { 1, 1, 1 }, { 0.91f, 0.92f, 0.92f }, texturesMetalDots, true );
			addStand2( 7, {  36,  1.0, -36 }, { 1, 1, 1 }, { 0.95f, 0.93f, 0.88f }, texturesMetalDots, true );

			addStand2( 0, { -36, -0.5, -36 }, { 10, 0.5, 10 }, { 1.10f, 0.71f, 0.29f }, texturesMetal, false );
			addStand2( 1, {  36, -0.5,  36 }, { 10, 0.5, 10 }, { 0.95f, 0.64f, 0.54f }, texturesMetal, false );
			addStand2( 2, { -36, -0.5,  36 }, { 10, 0.5, 10 }, { 0.91f, 0.92f, 0.92f }, texturesMetal, false );
			addStand2( 3, {  36, -0.5, -36 }, { 10, 0.5, 10 }, { 0.95f, 0.93f, 0.88f }, texturesMetal, false );

			addStand( {   0, -0.5, -32 } );
			addStand( {   0, -0.5,  32 } );
			addStand( { -32, -0.5,   0 } );
			addStand( {  32, -0.5,   0 } );

			// Lower  wall / path way
			addCube( vec3(   0, -0.5, -13 ), vec3(   10.0,  0.5,    3.0 ) );
			addCube( vec3(   0, -0.5,  13 ), vec3(   10.0,  0.5,    3.0 ) );
			addCube( vec3(  13, -0.5,   0 ), vec3(    3.0,  0.5,   16.0 ) );
			addCube( vec3( -13, -0.5,   0 ), vec3(    3.0,  0.5,   16.0 ) );

			// Wall
			addCube( vec3(   0,  4.5, -64 ), vec3(   63.0, 10.0,    1.0 ) );
			addCube( vec3(   0,  4.5,  64 ), vec3(   63.0, 10.0,    1.0 ) );
			addCube( vec3(  64,  4.5,   0 ), vec3(    1.0, 10.0,   65.0 ) );
			addCube( vec3( -64,  4.5,   0 ), vec3(    1.0, 10.0,   65.0 ) );

			addCube( vec3(   0, -1.0,   0 ), vec3(    1.0,  1.0,    1.0 ) );
			addCube( vec3(   0, -2.5,   0 ), vec3( 1024.0,  1.0, 1024.0 ) );


			psProperties.bumpScale  = { 1.0, 1.0 };
			psProperties.ssBump     = 1.0f;
			psProperties.ssPower    = 4.0f;
			psProperties.ssScale    = 30.0f;
			psProperties.ior        = 1.0f;
			psProperties.sAlpha     = 0.25f;
			psProperties.ssAlpha    = 0.75f;
			psProperties.absorption = 0.05f;
			psProperties.roughness = 1.0f;
			psProperties.metalness = 0.0f;

			vec3 r = { glm::quarter_pi< float >( ), 0, -glm::quarter_pi< float >( ) };

			addTranslucentCube( 0, {   0, 6,  58 }, vec3( 2.5, 2.5, 2.5 ), r );

			psProperties.ssAlpha    = 0.5f;
			psProperties.absorption = 0.2f;
			psProperties.sAlpha     = 0.5f;
			psProperties.roughness  = 0.0f;
			addTranslucentCube(  0, {  32  , 5.05,  62.5 }, vec3( 4.0,  5.0, 0.2 ), rotation );
			addTranslucentCube(  1, { -32  , 5.05,  62.5 }, vec3( 4.0,  5.0, 0.2 ), rotation );
			addTranslucentCube(  2, {  32  , 5.05, -62.5 }, vec3( 4.0,  5.0, 0.2 ), rotation );
			addTranslucentCube(  3, {   0  , 5.05, -62.5 }, vec3( 4.0,  5.0, 0.2 ), rotation );
			addTranslucentCube(  4, { -32  , 5.05, -62.5 }, vec3( 4.0,  5.0, 0.2 ), rotation );
			addTranslucentCube(  5, {  62.5, 5.05,  62.5 }, vec3( 0.2,  5.0, 0.2 ), rotation );
			addTranslucentCube(  6, { -62.5, 5.05, -62.5 }, vec3( 0.2,  5.0, 0.2 ), rotation );
			addTranslucentCube(  7, {  62.5, 5.05, -62.5 }, vec3( 0.2,  5.0, 0.2 ), rotation );
			addTranslucentCube(  8, { -62.5, 5.05,  62.5 }, vec3( 0.2,  5.0, 0.2 ), rotation );
			addTranslucentCube(  9, {  62.5, 5.05, -32   }, vec3( 0.2,  5.0, 4.0 ), rotation );
			addTranslucentCube( 10, {  62.5, 5.05,   0   }, vec3( 0.2,  5.0, 4.0 ), rotation );
			addTranslucentCube( 11, {  62.5, 5.05,  32   }, vec3( 0.2,  5.0, 4.0 ), rotation );
			addTranslucentCube( 12, { -62.5, 5.05,  32   }, vec3( 0.2,  5.0, 4.0 ), rotation );
			addTranslucentCube( 13, { -62.5, 5.05,   0   }, vec3( 0.2,  5.0, 4.0 ), rotation );
			addTranslucentCube( 14, { -62.5, 5.05, -32   }, vec3( 0.2,  5.0, 4.0 ), rotation );


			psProperties.bumpScale  = { 1.0, 1.0 };
			psProperties.ssBump     = 1.0f;
			psProperties.ssPower    = 2.0f;
			psProperties.ssScale    = 0.0f;
			psProperties.ssAlpha    = 1.0f;
			psProperties.absorption = 0.005f;
			psProperties.sAlpha     = 0.0f;
			psProperties.roughness  = 1.0f;

			addTranslucentCube( 15, {  62, 17.0,  62 }, vec3( 2.0, 2.0, 2.0 ), rotation );
			addTranslucentCube( 16, { -62, 17.0, -62 }, vec3( 2.0, 2.0, 2.0 ), rotation );
			addTranslucentCube( 17, {  62, 17.0, -62 }, vec3( 2.0, 2.0, 2.0 ), rotation );
			addTranslucentCube( 18, { -62, 17.0,  62 }, vec3( 2.0, 2.0, 2.0 ), rotation );
		}

		void SceneGenerator::setWater( )
		{
			vec3 position( 0, -0.5, 0 );
			vec3 rotation( 0, 0, 0 );
			vec3 scale( 64.0, 1.0, 64.0 );

			PSObjectBuffer psProperties;

			psProperties.ka         = { 0, 0, 0 };
			psProperties.kd         = { 1.0, 1.0, 1.0 };
			psProperties.ks         = { 1.0, 1.0, 1.0 };
			psProperties.bumpScale  = { 0, 0 };
			psProperties.ssPower    = 0.0f;
			psProperties.ssScale    = 0.0f;
			psProperties.ior        = 0.0f;
			psProperties.sAlpha     = 1.0f;
			psProperties.ssAlpha    = 0.0f;
			psProperties.absorption = 0.0f;

			vector< string > textures =
			{
				"BlankNormal",
				"BlankNormal"
			};

			pBuilder->addPCGWaterPlane( position, rotation, scale, textures, psProperties, false );
		}

		void SceneGenerator::setLights( )
		{
			LightEx light;

			vec3 position( 0, 0, 0 );
			vec3 direction( 0, -1, 0 );

			vec3 white( 1, 1, 1 );
			vec3 red( 1, 0, 0 );
			vec3 green( 0, 1, 0 );
			vec3 blue( 0, 0, 1 );
			vec3 yellow( 1, 1, 0 );

			auto addLight = [ this ] (
				const vec3& position,
				const vec3& colour,
				const vec3& direction,
				float range,
				float spotLightAngle,
				int type,
				bool addDot = true )
			{
				LightEx light;
				light.positionWS.x = position.x;
				light.positionWS.y = position.y;
				light.positionWS.z = position.z;
				light.positionWS.w = 1;

				vec3 nDirection = normalize( direction );
				light.directionWS.x = nDirection.x;
				light.directionWS.y = nDirection.y;
				light.directionWS.z = nDirection.z;
				light.directionWS.w = 0;

				light.colour.r = colour.r;
				light.colour.g = colour.g;
				light.colour.b = colour.b;
				light.colour.a = 1;


				light.spotLightAngle = spotLightAngle;
				light.range = range;
				light.enabled = TRUE;

				light.type = type;

				pBuilder->addLight( light );

				if( addDot )
				{
					PSObjectBuffer properties;

					properties.ka = { spotLightAngle, type, 0.0f };
					properties.kd = colour;
					properties.ks = nDirection;

					pBuilder->addSphere( position, std::min( range * 0.01f, 0.1f ), properties );
				}
			};

			auto addPointLight = [ this, &addLight ]
			( const vec3& position, const vec3& colour, float range, bool addDot = true )
			{
				addLight( position, colour, { 0, 0, 0 }, range, 0, 0, addDot );
			};

			auto addSpotLight = [ this, &addLight ]
			( const vec3& position, const vec3& colour, const vec3& direction, float range, float spotLightAngle, bool addDot = true )
			{
				addLight( position, colour, direction, range, spotLightAngle, 1, addDot );
			};

			auto addDirectionLight = [ this, &addLight ]
			( const vec3& position, const vec3& colour, const vec3& direction, bool addDot = true )
			{
				addLight( position, colour, direction, 0, 0, 2, addDot );
			};

			float lightScale = 500;

			addPointLight( {   0, 3.5,  32 },    red * lightScale,  64.0f, false );
			addPointLight( {   0, 3.5, -32 },  green * lightScale,  64.0f, false );
			addPointLight( {  32, 3.5,   0 }, yellow * lightScale,  64.0f, false );
			addPointLight( { -32, 3.5,   0 },   blue * lightScale,  64.0f, false );
			addPointLight( {   0, 2.0,   0 },  white * lightScale, 128.0f, false );

			addSpotLight( {  62.5, 12,  32   }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( {  62.5, 12, -62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( { -62.5, 12,  62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( { -62.5, 12, -62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );

			addSpotLight( {  32  , 12,  62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			//addSpotLight( {   0  , 12,  62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( { -32  , 12,  62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );

			addSpotLight( {  32  , 12, -62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( {   0  , 12, -62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( { -32  , 12, -62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );

			addSpotLight( {  62.5, 12, -32   }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( {  62.5, 12,   0   }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( {  62.5, 12,  62.5 }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );

			addSpotLight( { -62.5, 12,  32   }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( { -62.5, 12,   0   }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );
			addSpotLight( { -62.5, 12, -32   }, white * 100.0f, { 0, -1, 0 }, 20.0f, 30.0f );

			addSpotLight( {  -3  ,  5,  40   }, red   * 5000.0f, {  0.23,  0.1 , 1 }, 60.0f, 10.0f );
			addSpotLight( {   3  ,  5,  40   }, green * 5000.0f, { -0.23,  0.1 , 1 }, 60.0f, 10.0f );
			addSpotLight( {   0  ,  9,  40   }, blue  * 5000.0f, {  0  , -0.22, 1 }, 60.0f, 10.0f );


			addPointLight( {  32, 1.0,  32 },  white * lightScale, 32.0f );
			addPointLight( { -32, 1.0,  32 },  white * lightScale, 32.0f );
			addPointLight( {  32, 1.0, -32 },  white * lightScale, 32.0f );
			addPointLight( { -32, 1.0, -32 },  white * lightScale, 32.0f );


			addPointLight( {  62, 17.0,  62 },  vec3( 0, 1, 1 ) * lightScale, 16.0f, false );
			addPointLight( { -62, 17.0,  62 },  vec3( 1, 0, 1 ) * lightScale, 16.0f, false );
			addPointLight( {  62, 17.0, -62 },  vec3( 1, 1, 1 ) * lightScale, 16.0f, false );
			addPointLight( { -62, 17.0, -62 },  vec3( 1, 1, 0 ) * lightScale, 16.0f, false );


			std::uniform_real_distribution< float > randomFloats = uniform_real_distribution< float >( -1.0, 1.0 );
			std::uniform_real_distribution< float > randomUFloats = uniform_real_distribution< float >( 0.0, 1.0 );
			std::default_random_engine generator = default_random_engine( default_random_engine::default_seed );

			/**/
			for( int i = 0; i < 64; i++ )
			{
				vec3 position;
				vec3 colour;

				position.x = randomFloats( generator );
				position.y = randomFloats( generator );
				position.z = randomFloats( generator );

				colour.x = randomUFloats( generator );
				colour.y = randomUFloats( generator );
				colour.z = randomUFloats( generator );

				colour.x *= colour.x;
				colour.y *= colour.y;
				colour.z *= colour.z;

				position = position * vec3( 60, 0.5, 60 ) + vec3( 0, 1.1, 0 );

				addPointLight( position, colour * 80.0f, 20.0f );
			}
			//*/
		}

		void SceneGenerator::setText( )
		{
			string font = "C:/Windows/Fonts/arialbd.ttf";
			string font2 = "C:/Windows/Fonts/consolab.ttf";
			string text = "\t\t\t\t\t\t\t\tHere Be Generated Dragons!";
			vec2 position2D( 0, 0 );
			vec2 rotation2D( 0, 0 );
			vec2 scale2D( 1, 1 );

			PSFontBuffer psProperties;
			psProperties.properties = vec4( 0, 0, 0, 0 );

			psProperties.ka = vec3( 0, 0, 0 );
			psProperties.kd = vec3( 1, 1, 1 );
			psProperties.ks = vec3( 1, 1, 1 );

			psProperties.roughness = 0;
			psProperties.metalness = 0;

			psProperties.bumpScale = vec2( 0, 0 );

			psProperties.ssPower = 0;
			psProperties.ssScale = 0;
			psProperties.sAlpha = 0;
			psProperties.ssAlpha = 0;
			psProperties.absorption = 0;
			psProperties.refraction = 0;

			psProperties.properties.x = 0x00; // STD
			vector< string > textures2D = { "SingleTexelWhite", "SingleTexelWhite" };

//			pBuilder->addText2D( font, text, position2D, rotation2D, scale2D, textures2D, psProperties );

			vector< string > textures3D = {
				"SingleTexelWhite",
				"BlankNormal",
				"SingleTexelWhite",
				"SingleTexelWhite",
				"SingleTexelBlack",
				"SingleTexelBlack",
				"SingleTexelBlack",
				"SingleTexelWhite",
				"SingleTexelBlack"
			};


			psProperties.kd = vec3( 2, 2, 2 );
			psProperties.ks = vec3( 2, 2, 2 );

			pBuilder->addText3D( font2, "Don't Panic!", { 63, 18.1, 18 }, { 0, glm::half_pi< float >( ), 0 }, { 15, 15, 15 }, textures3D, psProperties );

			psProperties.properties.x = 0x01; // WARP
			pBuilder->addText3D( font, "42", { -256, 256, 256 }, { -glm::quarter_pi< float >( ), -glm::quarter_pi< float >( ), 0 }, { 24, 24, 24 }, textures3D, psProperties );

			psProperties.kd = vec3( 6, 6, 6 );
			psProperties.ks = vec3( 6, 6, 6 );

			psProperties.properties.x = 0x00; // STD
			string instructions = R"(Controls:
             - Open/Close Menu.
             - Move Forward.
             - Move Back.
             - Move Left.
             - Move Right.
             - Move Down.
             - Move Up.)";

			string controls = R"(
Esc
W
S
A
D
C
Space)";

			string mouse = R"(
Hold the right mouse button
and move the mouse to rotate
the camera.
             - To Rotate The Sun.
             - Display Normals.
             - Display Frames Per Second.
             - Display Mostly Useless Times (If FPS is ON).)";

			string controls2 = R"(



Z/X
1
2
3)";

			pBuilder->addText3D( font, instructions, { -4.5, 4, 13 }, { 0, 0, 0 }, { 0.5, 0.5, 0.5 }, textures3D, psProperties );
			pBuilder->addText3D( font, mouse, { 2, 4, 13 }, { 0, 0, 0 }, { 0.5, 0.5, 0.5 }, textures3D, psProperties );
			psProperties.kd = vec3( 6, 0.3, 0.1 );
			pBuilder->addText3D( font2, controls, { -4.5, 4, 13 }, { 0, 0, 0 }, { 0.5, 0.5, 0.5 }, textures3D, psProperties );
			pBuilder->addText3D( font2, controls2, { 2, 4, 13 }, { 0, 0, 0 }, { 0.5, 0.5, 0.5 }, textures3D, psProperties );
		}

		std::vector< TreePromise > SceneGenerator::setTrees( )
		{
			std::vector< TreePromise > trees( 5 );

			Material bark;
			Material leaf;
			TreeProperties tree;

			vec3 rotation( 0, 0, 0 );
			vec3 scale( 1, 1, 1 );

			tree.branches.clear( );

			tree.shape     = 0;
			tree.baseSize  = 0.4;
			tree.scale     = 13;
			tree.scaleV    = 0;
			tree.zScale    = 1;
			tree.zScaleV   = 0;
			tree.ratio     = 0.015;
			tree.ratioPower= 1.2;
			tree.lobes     = 5;
			tree.lobeDepth = 0.07;
			tree.flare     = 0.6;
			tree.tScale    = 1.0;
			tree.tScaleV   = 0.0;
			tree.baseSplits= 0.0;

			tree.properties.length      = 1.0;
			tree.properties.lengthV     = 0.0;
			tree.properties.taper       = 1.0;

			tree.properties.segSplits   = 0.0;
			tree.properties.splitAngle  = 30;
			tree.properties.splitAngleV = 0;

			tree.properties.curveRes    = 4;
			tree.properties.curve       = 0;
			tree.properties.curveBack   = 0;
			tree.properties.curveV      = 0;

			tree.leaves.leaves         = 2;
			tree.leaves.shape          = 0;
			tree.leaves.scale          = 0.26;
			tree.leaves.scaleX         = 1.0;
			tree.leaves.attractionUp   = 0.5;
			tree.leaves.pruneRatio     = 0.0;
			tree.leaves.pruneWidth     = 0.5;
			tree.leaves.pruneWidthPeak = 0.5;
			tree.leaves.prunePowerLow  = 0.5;
			tree.leaves.prunePowerHigh = 0.5;
			tree.leaves.gap            = 0.025;
			tree.leaves.gapV           = 0.01;
			tree.leaves.smooth         = false;


			TreeProperties::Branch branch1;

			branch1.downAngle              =  65;
			branch1.downAngleV             = -10;

			branch1.rotate                 = 140;
			branch1.rotateV                = 0;
			branch1.branches               =  15;

			branch1.properties.length      = 0.4;
			branch1.properties.lengthV     = 0.0;
			branch1.properties.taper       = 1.0;

			branch1.properties.segSplits   = 0.0;
			branch1.properties.splitAngle  = 60;
			branch1.properties.splitAngleV = 0;

			branch1.properties.curveRes    = 4;
			branch1.properties.curve       = -40;
			branch1.properties.curveBack   = 0;
			branch1.properties.curveV      = 50;


			TreeProperties::Branch branch2;

			branch2.downAngle              =  45;
			branch2.downAngleV             =  10;

			branch2.rotate                 = 140;
			branch2.rotateV                = 0;
			branch2.branches               =  4;

			branch2.properties.length      = 0.4;
			branch2.properties.lengthV     = 0.0;
			branch2.properties.taper       = 1.0;

			branch2.properties.segSplits   = 0.0;
			branch2.properties.splitAngle  = 0;
			branch2.properties.splitAngleV = 0;

			branch2.properties.curveRes    = 2;
			branch2.properties.curve       = -40;
			branch2.properties.curveBack   = 0;
			branch2.properties.curveV      = 75;

			tree.branches.push_back( branch1 );
			tree.branches.push_back( branch2 );

			const auto fillMaterial = [ ]( Material& material ){

				material.textures[ 0 ] = "SingleTexelWhite";
				material.textures[ 1 ] = "BlankNormal";
				material.textures[ 2 ] = "SingleTexelWhite";
				material.textures[ 3 ] = "SingleTexelWhite";
				material.textures[ 4 ] = "SingleTexelBlack";
				material.textures[ 5 ] = "SingleTexelBlack";
				material.textures[ 6 ] = "SingleTexelBlack";
				material.textures[ 7 ] = "SingleTexelWhite";
				material.textures[ 8 ] = "SingleTexelBlack";

				material.ps.ka         = { 1, 1, 1 };
				material.ps.kd         = { 1, 1, 1 };
				material.ps.ks         = { 1, 1, 1 };

				material.ps.bumpScale  = { 1.0, 1.0 };
				material.ps.roughness  = 1.0;
				material.ps.metalness  = 0.0;
				//material.ps.sAlpha     = 1.25;
				material.ps.sAlpha     = 0.1;
				material.ps.ssAlpha    = 1.0;
				material.ps.absorption = 1.0;
				material.ps.ior        = 3.5;
				material.ps.ssScale    = 20;
				material.ps.ssPower    = 2;
				material.ps.ssBump     = 2.0;
			};

			fillMaterial( bark );
			fillMaterial( leaf );

			leaf.ps.roughness  = 0.5;
			leaf.ps.metalness  = 0.01;
			leaf.ps.kd = { 1.0, 0.6, 0.8 };

			trees[ 0 ].position = { 0, 0, 0 };
			trees[ 0 ].bark = bark;
			trees[ 0 ].leaf = leaf;
			trees[ 0 ].tree = tree;
			trees[ 0 ].geometry = std::make_shared< TreeGeometryPromise >( );

			trees[ 1 ].position = {  32, 1.5,   0 };
			trees[ 1 ].bark = bark;
			trees[ 1 ].leaf = leaf;
			trees[ 1 ].tree = tree;
			trees[ 1 ].geometry = std::make_shared< TreeGeometryPromise >( );

			trees[ 2 ].position = { -32, 1.5,   0 };
			trees[ 2 ].bark = bark;
			trees[ 2 ].leaf = leaf;
			trees[ 2 ].tree = tree;
			trees[ 2 ].geometry = std::make_shared< TreeGeometryPromise >( );

			trees[ 3 ].position = {   0, 1.5,  32 };
			trees[ 3 ].bark = bark;
			trees[ 3 ].leaf = leaf;
			trees[ 3 ].tree = tree;
			trees[ 3 ].geometry = std::make_shared< TreeGeometryPromise >( );

			trees[ 4 ].position = {   0, 1.5, -32 };
			trees[ 4 ].bark = bark;
			trees[ 4 ].leaf = leaf;
			trees[ 4 ].tree = tree;
			trees[ 4 ].geometry = std::make_shared< TreeGeometryPromise >( );


			auto startTreeConstruction = [ this ]( int i, int s, const TreeProperties& tree, const TreeGeometryPromiseSPtr& pGeometry )
			{
				cout << "\t Tree " << i << " - Scheduled.\n";
				// Get the trees to build on some other threads
				schedule( [ this, i, s, tree, pGeometry ]( ) -> void
				{
					cout << "\t Tree " << i << " - Stared.\n";
					TreeBuilder treeBuilder( s );
					TreeGeometry geometry;
					treeBuilder.create( tree, geometry );
					pGeometry->set_value( geometry );
					cout << "\t Tree " << i << " - Complete.\n";
				} );
			};

			uniform_int_distribution< int > randomSeed( 0, 100000000 );
			default_random_engine generator( default_random_engine::default_seed );
			int i = 0;
			for( TreePromise& p: trees )
			{
				startTreeConstruction( ++i, randomSeed( generator ), p.tree, p.geometry );
			}

			return trees;
		}

		void SceneGenerator::finaliseTrees( std::vector<TreePromise>& promises )
		{
			int i = 0;
			for( TreePromise& p: promises )
			{
				TreeGeometry geometry = p.geometry->get_future( ).get( );

				pBuilder->addBasicMesh( "Tree " + to_string( i ),
					{ geometry.branchVertices, geometry.branchIndecies, MeshPrimitive::TriangleList },
					p.position, p.rotation, p.scale,
					p.bark.textures, p.bark.ps );

				if( p.tree.leaves.leaves != 0 && ! geometry.leafVertices.empty( ) )
				{
					if( p.leaf.ps.sAlpha >= 1.0f )
					{
						pBuilder->addBasicMesh(
							"Tree Leaves " + to_string( i ),
							{ geometry.leafVertices, geometry.leafIndecies, MeshPrimitive::TriangleList },
							p.position, p.rotation, p.scale,
							p.leaf.textures, p.leaf.ps );
					}
					else
					{
						pBuilder->addTranslucentMesh(
							"Tree Leaves " + to_string( i ),
							{ geometry.leafVertices, geometry.leafIndecies, MeshPrimitive::TriangleList },
							p.position, p.rotation, p.scale,
							p.leaf.textures, p.leaf.ps );
					}
				}
				++i;
			}
		}
	} // namespace fx
} // namespace visual