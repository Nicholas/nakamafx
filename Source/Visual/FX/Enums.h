#pragma once
// Enums
//
// Holds a bunch of enums so that others can use
// them with out being linked to all sorts of other things.
//
// Project   : NaKama-Fx
// File Name : Enums.h
// Date      : 14/03/2019
// Author    : Nicholas Welters

#ifndef _ENUMS_H
#define _ENUMS_H

namespace visual
{
namespace fx
{
	enum ColourFormat
	{
		RGBA8  = 32,
		RGBA16 = 64,
		RGBA32 = 128,
	};

	enum SamplerFilter
	{
		ANISOTROPIC,
		LINEAR,
		POINT
	};

	enum SamplerBounds
	{
		MIRROR,
		WRAP,
		CLAMP
	};

	enum MsaaLevel
	{
		MSAA_X1 = 1,
		MSAA_X2 = 2,
		MSAA_X4 = 4,
		MSAA_X8 = 8
	};

	enum VSyncState
	{
		VSYNC_OFF,
		VSYNC_ON,
		VSYNC_FAST
	};

	enum Blend
	{
		ALPHA_BLEND = 0,
		WRITE_OVER,
		ALT_ALPHA_BLEND,
		PRE_MULTIPLIED_ALPHA_BLEND,
		ADD_BLEND,
	};






	inline std::string toString( const ColourFormat& x )
	{
		std::string str;

		if( x == RGBA8 ){ str = "8bit"; }
		else if( x == RGBA16 ){ str = "16bit"; }
		else if( x == RGBA32 ){ str = "32bit"; }

		return str;
	}

	inline ColourFormat parseColourFormat( const std::string& x )
	{
		ColourFormat format = RGBA8;

		if( x == "8bit" ){ format = RGBA8; }
		else if( x == "16bit" ){ format = RGBA16; }
		else if( x == "32bit" ){ format = RGBA32; }

		return format;
	}

	inline std::string toString( const SamplerFilter& x )
	{
		std::string str;

		if( x == ANISOTROPIC ){ str = "anisotropic"; }
		else if( x == LINEAR ){ str = "linear"; }
		else if( x == POINT  ){ str = "point"; }

		return str;
	}

	inline SamplerFilter parseSamplerFilter( const std::string& x )
	{
		SamplerFilter format = ANISOTROPIC;

		if( x == "anisotropic" ){ format = ANISOTROPIC; }
		else if( x == "linear" ){ format = LINEAR; }
		else if( x == "point"  ){ format = POINT; }

		return format;
	}

	inline std::string toString( const VSyncState& x )
	{
		std::string str;

		if( x == VSYNC_OFF ){ str = "off"; }
		else if( x == VSYNC_ON ){ str = "on"; }
		else if( x == VSYNC_FAST  ){ str = "fast"; }

		return str;
	}

	inline VSyncState parseVSyncState( const std::string& x )
	{
		VSyncState vsync = VSYNC_OFF;

		if( x == "off" ){ vsync = VSYNC_OFF; }
		else if( x == "on" ){ vsync = VSYNC_ON; }
		else if( x == "fast"  ){ vsync = VSYNC_FAST; }

		return vsync;
	}

	inline MsaaLevel parseMsaaLevel( const int x )
	{
		MsaaLevel msaa = MSAA_X1;

		if( x == 2 ){ msaa = MSAA_X2; }
		else if( x == 4 ){ msaa = MSAA_X4; }
		else if( x == 8  ){ msaa = MSAA_X8; }

		return msaa;
	}
}
}

#endif // _ENUMS_H