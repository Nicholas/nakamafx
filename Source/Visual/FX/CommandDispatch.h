#pragma once
// Controls the execute API commands to the GPU.
//
// Project   : NaKama-Tools
// File Name : CommandDispatch.h
// Date      : 24/12/2018
// Author    : Nicholas Welters

#ifndef _COMMAND_DISPATCH_H
#define _COMMAND_DISPATCH_H

namespace visual
{
namespace fx
{
	/// <summary> This class controls the execute API commands to the GPU. </summary>
	class CommandDispatch
	{
	public:
	/// <summary> dtor. </summary>
		virtual ~CommandDispatch( ) = 0;

		/// <summary>
		/// Reset the command allocator memory so that we can reuse it to record new commands.
		///	This should only be called after we know that the commands in the allocator have all
		///	finished executing. see FlushCommandQueue( ) which uses a fence to sync the GPU and CPU
		/// </summary>
		virtual void resetCommandAllocators( ) = 0;

		/// <summary>	Send the command lists to the GPU for execution. </summary>
		virtual void executeCommandList( ) = 0;

		/// <summary>
		/// Wait for the last set of command list to complete execution.
		///	This is to make sure that we can sync the GPU and the CPU.
		///
		///	Use this to make sure the command queue is empty.
		///	This doesn't mean the list has been executed, only that
		///	the command queue has completed what ever was scheduled
		///	before the fence.
		/// </summary>
		virtual void flushCommandQueue( ) = 0; //TODO: Need a better solution for syncing the GPU and CPU.
	};

	inline CommandDispatch::~CommandDispatch( ) = default;
} // namespace fx
} // namespace visual

#endif // _COMMAND_DISPATCH_H
