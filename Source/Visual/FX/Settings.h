#pragma once

// Renderer settings
// 
// All the settings that we will use to build out rendering pipeline.
//
// Project   : NaKama-Fx
// File Name : Settings.h
// Date      : 08/01/2018
// Author    : Nicholas Welters

#ifndef _SETTINGS_H
#define _SETTINGS_H

#include "Enums.h"

#include <glm/glm.hpp>

#include <string>

namespace visual
{
namespace fx
{
	/// <summary>
	/// Holds all the settings that will be used
	/// to build the main objects in the system.
	/// </summary>
	class Settings
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="name"> The system name. </param>
			explicit Settings( std::string name );

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The settings object to copy. </param>
			Settings( const Settings& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="move"> The settings object to move. </param>
			Settings( Settings&& move ) = default;
			
			/// <summary> dtor </summary>
			~Settings( ) = default;
			
			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The settings object to copy. </param>
			Settings& operator=( const Settings& copy ) = default;
			
			/// <summary> Move assignment operator </summary>
			/// <param name="move"> The settings object to move. </param>
			Settings& operator=( Settings&& move ) = default;



			// -------------------------------------------
			// Application Properties
			// -------------------------------------------
			/// <summary> Get The system name. </summary>
			/// <returns> The systems name. </returns>
			const std::string& getSystemName( ) const;

			/// <summary> Get default scene to load on start up. </summary>
			/// <returns> The default startup scene. </returns>
			const std::string& getDefaultScene( ) const;

			/// <summary> Get default scene to load on start up. </summary>
			/// <param name="filePath"> The default startup scene. </param>
			void getDefaultScene( const std::string& filePath );

			
			// -------------------------------------------
			// Rendering Engine Properties
			// -------------------------------------------
			/// <summary> Get the rendering size. </summary>
			/// <returns> The rendering frame buffer size. </returns>
			const glm::uvec2& getRenderTargetSize( ) const;
			
			/// <summary> Rendering frame buffer size. </summary>
			/// <param name="size"> Rendering size. </param>
			void setRenderTargetSize( const glm::uvec2& size );


			/// <summary> Get the rendering frame buffer colour format. </summary>
			/// <returns> Frame buffer colour format. </returns>
			ColourFormat getRenderTargetFormat( ) const;

			/// <summary> Set the rendering frame buffer colour format. </summary>
			/// <param name="format"> Rendering frame buffer colour format. </param>
			void setRenderTargetFormat( ColourFormat format );
			
			/// <summary> Get the render targets MSAA level. </summary>
			/// <returns> MSAA level to use. </returns>
			MsaaLevel getMsaaLevel( ) const;

			/// <summary> Set the render targets MSAA level. </summary>
			/// <param name="msaaLevel"> MSAA level to use. </param>
			void setMsaaLevel( MsaaLevel msaaLevel );




			/// <summary> Get the back buffer size. </summary>
			/// <returns> The back buffer size. </returns>
			const glm::uvec2& getBackBufferSize( ) const;
			
			/// <summary> Back buffer size. </summary>
			/// <param name="size"> The back buffer size. </param>
			void setBackBufferSize( const glm::uvec2& size );


			/// <summary> Get the back buffer colour format. </summary>
			/// <returns> Back buffer colour format. </returns>
			ColourFormat getBackBufferFormat( ) const;

			/// <summary> Set the back buffer colour format. </summary>
			/// <param name="format"> Back buffer colour format. </param>
			void setBackBufferFormat( ColourFormat format );

			/// <summary> Get window and swap chain fullscreen state. </summary>
			/// <returns> The fullscreen state. </returns>
			bool isFullscreen( ) const;
			
			/// <summary> Set window and swap chain fullscreen state. </summary>
			/// <param name="fullscreen"> The fullscreen state. </param>
			void setFullscreen( bool fullscreen );

			/// <summary> Get number of frame buffers to use in the swap chain. </summary>
			/// <returns> Number of framebuffers to use. </returns>
			int  getFrameBuffers( ) const;
			
			/// <summary> Set number of frame buffers to use in the swap chain. </summary>
			/// <param name="frameBuffers"> Number of frame buffers to use. </param>
			void setFrameBuffers( uint32_t frameBuffers );

			/// <summary> Get window and swap chain vsync state. </summary>
			/// <returns> The vsync state. </returns>
			VSyncState getVSyncState( ) const;
			
			/// <summary> Set window and swap chain vsync state. </summary>
			/// <param name="vsync"> The vsync state. </param>
			void setVSyncState( VSyncState vsync );




			/// <summary> Get the 1st shadow cascade size. </summary>
			/// <returns> The 1st shadow cascade size. </returns>
			const glm::uvec2& getShadowCascade1Size( ) const;
			
			/// <summary> Get 1st shadow cascade size. </summary>
			/// <param name="size"> The 1st shadow cascade size. </param>
			void setShadowCascade1Size( const glm::uvec2& size );

			/// <summary> Get the 2nd shadow cascade size. </summary>
			/// <returns> The 2nd shadow cascade size. </returns>
			const glm::uvec2& getShadowCascade2Size( ) const;
			
			/// <summary> Get 2nd shadow cascade size. </summary>
			/// <param name="size"> The 2nd shadow cascade size. </param>
			void setShadowCascade2Size( const glm::uvec2& size );


			// -------------------------------------------
			// Execution Engine Properties
			// -------------------------------------------
			/// <summary> Get the number of additional threads to create. </summary>
			/// <returns> Number of additional threads. </returns>
			int  getNumberOfWorkerThreads( ) const;
			
			/// <summary> Set the number of additional threads to create. </summary>
			/// <param name="workerThreads"> Number of additional threads. </param>
			void setNumberOfWorkerThreads( uint32_t workerThreads );


			/// <summary> Get the minimum number of async threads to try and keep busy in the execution engine. </summary>
			/// <returns> Minimum number of async threads. </returns>
			int  getMinimumAsyncJobsToExecute( ) const;
			
			/// <summary> Set the minimum number of async threads to try and keep busy in the execution engine. </summary>
			/// <param name="minimum"> Minimum number of async threads. </param>
			void setMinimumAsyncJobsToExecute( uint32_t minimum );
			
			/// <summary> Get the maximum number of async threads to run in the execution engine. </summary>
			/// <returns> Maximum number of async threads. </returns>
			int  getMaximumAsyncJobsToExecute() const;
			
			/// <summary> Set the maximum number of async threads to run in the execution engine. </summary>
			/// <param name="maximum"> Maximum number of async threads. </param>
			void setMaximumAsyncJobsToExecute( uint32_t maximum );


			/// <summary> Get the minimum number of sync threads to try and keep busy in the execution engine. </summary>
			/// <returns> Minimum number of async threads. </returns>
			int  getMinimumSyncJobsToExecute( ) const;
			
			/// <summary> Set the minimum number of sync threads to try and keep busy in the execution engine. </summary>
			/// <param name="minimum"> Minimum number of async threads. </param>
			void setMinimumSyncJobsToExecute( uint32_t minimum );
			
			/// <summary> Get the maximum number of sync threads to run in the execution engine. </summary>
			/// <returns> Maximum number of sync threads. </returns>
			int  getMaximumSyncJobsToExecute( ) const;
			
			/// <summary> Set the maximum number of sync threads to run in the execution engine. </summary>
			/// <param name="maximum"> Maximum number of sync threads. </param>
			void setMaximumSyncJobsToExecute( uint32_t maximum );

		private:
			// -------------------------------------------
			// Application Properties
			// -------------------------------------------
			/// <summmary> The system name. </summmary>
			std::string systemName;

			/// <summmary> The default startup scene. </summmary>
			std::string defaultScene;



			// -------------------------------------------
			// Render Target Properties
			// -------------------------------------------
			/// <summmary> The rendering frame buffer resolution. </summmary>
			glm::uvec2 renderTargetSize;
			
			/// <summmary> The rendering frame buffer colour format. </summmary>
			ColourFormat renderTargetFormat;
			
			/// <summmary> The rendering frame buffer MSAA level. </summmary>
			MsaaLevel msaaLevel;

			

			// -------------------------------------------
			// Back Buffer Properties
			// -------------------------------------------
			/// <summmary> The back buffer resolution. </summmary>
			glm::uvec2 backBufferSize;
			
			/// <summmary> The back buffer colour format. </summmary>
			ColourFormat backBufferFormat;
			
			/// <summmary> The fullscreen state. </summmary>
			bool fullscreen;
			
			/// <summmary> Number of frame buffers to use. </summmary>
			uint32_t frameBuffers;
			
			/// <summmary> The vsync state. </summmary>
			VSyncState vsync;

			

			// -------------------------------------------
			// Shadow Cascade Properties
			// -------------------------------------------
			/// <summmary> The first shadow cascade resolution. </summmary>
			glm::uvec2 shadowCascade1TargetSize;
			
			/// <summmary> The second shadow cascade resolution. </summmary>
			glm::uvec2 shadowCascade2TargetSize;



			// -------------------------------------------
			// Execution engine properties
			// -------------------------------------------
			/// <summmary>
			/// Number of additional threads.
			///
			/// When rendering keep the total number of 
			/// threads to n - 1, where n is the number of 
			/// execution units (hyper-threaded cores and such).
			/// Thus numberOfWorkerThreads = n - 2.
			/// </summmary>
			uint32_t numberOfWorkerThreads;

			/// <summmary> Minimum number of async threads. </summmary>
			uint32_t minimumAsyncJobsToExecute;
			/// <summmary> Maximum number of async threads. </summmary>
			uint32_t maximumAsyncJobsToExecute;
			/// <summmary> Minimum number of sync threads. </summmary>
			uint32_t minimumSyncJobsToExecute;
			/// <summmary> Maximum number of sync threads. </summmary>
			uint32_t maximumSyncJobsToExecute;
	};
} // namespace fx
} // namespace visual

#endif // _SETTINGS_H