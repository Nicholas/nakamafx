#pragma once
// Create basic shapes.
//
// Project   : NaKama-Fx
// File Name : Shapes.h
// Date      : 03/01/2018
// Author    : Nicholas Welters

#ifndef _SHAPES_H
#define _SHAPES_H

#include "Vertex.h"
#include "Mesh.h"
#include <functional>

typedef unsigned char BYTE;

namespace visual
{
namespace fx
{
	class Shapes
	{
	public:
		/// <summary> Fill in the vertex array with cube vertices. </summary>
		/// <param name="buffer"> The array to place the vertices in. </param>
		static void fillCube( Vertex3f4f::Vertex( &buffer )[ 36 ] );

		/// <summary> Fill in the vertex array with coordinate star vertices. </summary>
		/// <param name="buffer"> The array to place the vertices in. </param>
		static void fillCoordStar( Vertex3f4f::Vertex( &buffer )[ 72 ] );


		/// <summary> Create a coord star mesh. </summary>
		/// <returns> A new coord mesh. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > coordStar( );

		/// <summary> Create a cube mesh. </summary>
		/// <returns> A new cube mesh. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > testSlate( );

		/// <summary> Create a cube mesh thats shaped like an arrow head. </summary>
		/// <returns> A new arrow with each side with smooth normals. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > smoothArrowQuad( );

		/// <summary> Create a cube mesh thats shaped like an arrow head. </summary>
		/// <returns> A new arrow. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > arrowQuad( );

		/// <summary> Create a fullscreen triangle mesh. </summary>
		/// <returns> A new fullscreen triangle mesh. </returns>
		static Mesh< Vertex3f2f::Vertex > fullScreenTri( );

		/// <summary> Create a fullscreen cube mesh. </summary>
		/// <returns> A new fullscreen cube mesh. </returns>
		static Mesh< Vertex3f2f::Vertex > fullScreenCube( );

		/// <summary> Create a grid mesh. </summary>
		/// <param name="size"> The width/depth of the grid vertices. </param>
		/// <param name="cellWidth"> The size of the cell. </param>
		/// <param name="height"> The height of the grid. </param>
		/// <returns> A new grid mesh. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > grid( int size, float cellWidth, float height );

		/// <summary> Create a height map mesh. </summary>
		/// <param name="heights"> The raw height map array data. </param>
		/// <param name="size"> The width/depth of the grid vertices. </param>
		/// <param name="cellWidth"> The size of the cell. </param>
		/// <param name="minHeight"> The lowest height on the height map. </param>
		/// <param name="maxHeight"> The highest point on the height map. </param>
		/// <returns> A new height map mesh. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > heightMap( const BYTE* heights, int size, float cellWidth, float minHeight, float maxHeight );

		/// <summary> Create a grid mesh with a function to provide a height for a grid vertex. </summary>
		/// <param name="width"> The width of the grid in vertices (cells = vertices - 1). </param>
		/// <param name="depth"> The depth of the grid in vertices. </param>
		/// <param name="cellWidth"> The size of a cell. </param>
		/// <param name="heights"> A function that supplies the heights for each vertex. </param>
		/// <returns> A new grid mesh. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > grid( int width, int depth, float cellWidth, const ::std::function< float ( int ) >& heights );


		/// <summary> Test wall mesh. </summary>
		/// <param name="width"> The width of the wall. </param>
		/// <param name="height"> The height of the wall. </param>
		/// <returns> A test mesh. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > wall( float width, float height );

		/// <summary> Create a grid mesh. </summary>
		/// <param name="faces"> The number of faces in the cylinder. </param>
		/// <param name="slices"> The number of slices in the cylinder. </param>
		/// <param name="length"> The length of the cylinder. </param>
		/// <param name="radius"> A function that will provide a radius offset based on a vertex segment and slice. </param>
		/// <returns> A new grid mesh. </returns>
		static Mesh< Vertex3f2f3f3f3f::Vertex > cylinder( int faces, int slices, float length, const std::function< float ( int, int ) >& radius );
	};
} // namespace fx
} // namespace visual

#endif // _SHAPES_H
