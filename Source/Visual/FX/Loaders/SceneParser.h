#pragma once
// Parse a json file containing a definition
// for the scene to be rendered.
//
// Project   : NaKama-Tools
// File Name : SceneParser.h
// Date      : 06/03/2018
// Author    : Nicholas Welters

#ifndef _SCENE_PARSER_H
#define _SCENE_PARSER_H

#include "JsonParser.h"
#include <Macros.h>

namespace visual
{
	namespace fx
	{
		FORWARD_DECLARE( SceneBuilder );

		/// <summary>
		/// Parse a json file containing a definition
		/// for the scene to be rendered.
		/// </summary>
		class SceneParser: public JsonParser
		{
			public:
				/// <summary> ctor </summary>
				/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
				/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
				SceneParser( const tools::Scheduler& schedule, const SceneBuilderSPtr& pBuilder );

				/// <summary> ctor </summary>
				/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
				/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
				SceneParser( tools::Scheduler&& schedule, SceneBuilderSPtr&& pBuilder );

				/// <summary> ctor </summary>
				/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
				/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
				SceneParser( const tools::Scheduler& schedule, SceneBuilderSPtr&& pBuilder );

				/// <summary> ctor </summary>
				/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
				/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
				SceneParser( tools::Scheduler&& schedule, const SceneBuilderSPtr& pBuilder );

				/// <summary> ctor </summary>
				SceneParser( ) = delete;

				/// <summary> copy ctor </summary>
				/// <param name="copy"> The object to copy. </param>
				SceneParser( const SceneParser& copy ) = default;

				/// <summary> move ctor </summary>
				/// <param name="move"> The temporary to move. </param>
				SceneParser( SceneParser&& move ) = default;


				/// <summary> dtor </summary>
				virtual ~SceneParser( ) = default;


				/// <summary> Copy assignment operator </summary>
				/// <param name="copy"> The object to copy. </param>
				SceneParser& operator=( const SceneParser& copy ) = default;

				/// <summary> Move assignment operator </summary>
				/// <param name="move"> The temporary to move. </param>
				SceneParser& operator=( SceneParser&& move ) = default;

			private:
				/// <summary> The parsing function that will interpret the json data. </summary>
				/// <param name="pRoot"> The json object root node. </param>
				virtual void parseJSON( const cJSONSPtr& pRoot ) final override;

				/// <summary> The parsing function that will interpret the json data for a simple billboard moons. </summary>
				/// <param name="pRoot"> The json moon node. </param>
				virtual void parseMoon( const cJSON * pMoon );

				/// <summary> The parsing function that will interpret the json data for a simple billboard planets. </summary>
				/// <param name="pRoot"> The json planet node. </param>
				virtual void parsePlanet( const cJSON* pPlanet );

				/// <summary> The parsing function that will interpret the json data for a simple billboard sphere. </summary>
				/// <param name="pRoot"> The json sphere node. </param>
				virtual void parseSphere( const cJSON* pSphere );

				/// <summary> The parsing function that will interpret the json data for a zone object. </summary>
				/// <param name="pZone"> The json object zone node. </param>
				virtual void parseZone( const cJSON* pZone );

				/// <summary> The parsing function that will interpret the json data for a model object file. </summary>
				/// <param name="pObjects"> The json object objects node. </param>
				virtual void parseObject( const cJSON* pObjects );

				/// <summary> The parsing function that will interpret the json data for a translucent model object file. </summary>
				/// <param name="pObjects"> The json object objects node. </param>
				virtual void parseTranslucentObject( const cJSON* pObjects );

				/// <summary> The parsing function that will interpret the json data for a translucent model object file. </summary>
				/// <param name="pObjects"> The json object objects node. </param>
				virtual void parseTranslucentTest( const cJSON* pObjects );

				/// <summary> The parsing function that will interpret the json data for a height map. </summary>
				/// <param name="pHeightMap"> The json object height map node. </param>
				virtual void parseHeightMap( const cJSON* pHeightMap );

				/// <summary> The parsing function that will interpret the json data for a pcg height map. </summary>
				/// <param name="pHeightMap"> The json object height map node. </param>
				virtual void parsePCGHeightMap( const cJSON* pHeightMap );

				/// <summary> The parsing function that will interpret the json data for a cube. </summary>
				/// <param name="pCube"> The json object cube node. </param>
				virtual void parseCube( const cJSON* pCube );

				/// <summary> The parsing function that will interpret the json data for a water plane. </summary>
				/// <param name="pWaterPlane"> The json object water pane node. </param>
				virtual void parseWaterPlane( const cJSON* pWaterPlane );

				/// <summary> The parsing function that will interpret the json data for a pcg water plane. </summary>
				/// <param name="pWaterPlane"> The json object water pane node. </param>
				virtual void parsePCGWaterPlane( const cJSON* pWaterPlane );

				/// <summary> The parsing function that will interpret the json data for the sky box. </summary>
				/// <param name="pSkyBox"> The json object sky box node. </param>
				virtual void parseSkyBox( const cJSON* pSkyBox );

				/// <summary> The parsing function that will interpret the json data for a paricle system. </summary>
				/// <param name="pParicleSystem"> The json object paricle system node. </param>
				virtual void parseParicleSystem( const cJSON* pParicleSystem );

				/// <summary> The parsing function that will interpret the json data for the volumetric fog system. </summary>
				/// <param name="pFog"> The json object paricle system node. </param>
				virtual void parseFog( const cJSON* pFog );

				/// <summary> The parsing function that will interpret the json data for a pbr sphere. </summary>
				/// <param name="pSphere"> The json object for a pbr sphere node. </param>
				virtual void parsePbrSphere( const cJSON* pSphere );

				/// <summary> The parsing function that will interpret the json data for 3D text. </summary>
				/// <param name="pText"> The json object for a 3D text node. </param>
				virtual void parseText3D( const cJSON* pText );

				/// <summary> The parsing function that will interpret the json data for 2D text. </summary>
				/// <param name="pText"> The json object for a 2D text node. </param>
				virtual void parseText2D( const cJSON* pText );

				/// <summary> The parsing function that will interpret the json data for a light. </summary>
				/// <param name="pLight"> The json object for a light node. </param>
				virtual void parseLight( const cJSON* pLight );

				/// <summary> The parsing function that will interpret the json data for a pcg tree. </summary>
				/// <param name="pTree"> The json object for a pcg tree node. </param>
				virtual void parsePCGTree( const cJSON* pTree );

			private:
				/// <summary>
				/// The scene builder that will create our API specific objects.
				/// This will then forward the new scene to the system.
				/// </summary>
				SceneBuilderSPtr pBuilder;
		};
	} // namespace fx
} // namespace visual

# endif // _SCENE_PARSER_H