#pragma once
// Parse a json file containing the
// systems settings to initialise with.
//
// Project   : NaKama-Tools
// File Name : SettingsParser.h
// Date      : 06/03/2018
// Author    : Nicholas Welters

#ifndef _SETTINGS_PARSER_H
#define _SETTINGS_PARSER_H

#include "JsonParser.h"
#include "Macros.h"

FORWARD_DECLARE_STRUCT( cJSON );

namespace visual
{
	namespace fx
	{
		FORWARD_DECLARE( Settings );

		/// <summary> 
		/// Parse a json file containing the
		/// systems settings to initialise with.
		/// </summary>
		class SettingsParser final : public JsonParser
		{
			public:
				/// <summary> ctor </summary>
				SettingsParser( );

				/// <summary> copy ctor </summary>
				/// <param name="copy"> The settings object to copy. </param>
				SettingsParser( const SettingsParser& copy ) = default;

				/// <summary> move ctor </summary>
				/// <param name="move"> The settings object to move. </param>
				SettingsParser( SettingsParser&& move ) = default;

				/// <summary> dtor </summary>
				virtual ~SettingsParser( ) = default;
			
				/// <summary> Copy assignment operator </summary>
				SettingsParser& operator=( const SettingsParser& copy ) = default;
				
				/// <summary> Move assignment operator </summary>
				SettingsParser& operator=( SettingsParser&& copy ) = default;

				/// <summary> 
				/// Get the settings loaded from file.
				/// This will return the default settings object if we have not parsed a settings file.
				/// </summary>
				SettingsSPtr getSettings( ) const;

			private:
				/// <summary> The parsing function that will interpret the json data. </summary>
				/// <param name="pRoot"> The json object root node. </param>
				virtual void parseJSON( const cJSONSPtr& pRoot ) override final;

				/// <summary> This is going to point to the settings object that's built from reading the json file. </summary>
				SettingsSPtr pSettings;

		};
	} // namespace fx
} // namespace visual

# endif // _SETTINGS_PARSER_H