#include "SceneParser.h"
// Parse a json file containing a definition
// for the scene to be rendered.
//
// Project   : NaKama-Tools
// File Name : SceneParser.cpp
// Date      : 06/03/2018
// Author    : Nicholas Welters

#include "SceneBuilder.h"
#include "ObjParser.h"
#include "../Shapes.h"
#include "../Types/ConstantBuffers.h"
#include "../Types/Light.h"
#include "../Generators/Tree.h"

#include <cjson.h>
//#include <gli/gli.hpp>
//#include <gli/load.hpp>

#include <filesystem>
#include <iomanip>

#ifndef PRINT_PARSER_DATA
#define PRINT_PARSER_DATA( STUFF ) \
	std::cout << STUFF
#endif // PRINT_PARSER_DATA

namespace visual
{
	namespace fx
	{
		using tools::Scheduler;
		using fx::Mesh;
		using fx::VertexParticle;
		using fx::GSParticleBuffer;
		using fx::PSParticleBuffer;
		using std::endl;
		using std::bind;
		using std::move;
		using std::function;
		using std::vector;
		using std::unordered_map;
		using std::string;
		using std::to_string;
		using std::uniform_real_distribution;
		using std::default_random_engine;
		using glm::vec4;
		using glm::vec3;
		using glm::vec2;
		using glm::ivec3;
		using glm::ivec2;
		using glm::uvec4;
		using glm::mat3x3;
		using glm::mat4x4;

		namespace param = std::placeholders;

		/// <summary> ctor </summary>
		/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
		/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
		SceneParser::SceneParser( const Scheduler& schedule, const SceneBuilderSPtr& pBuilder )
			: JsonParser( schedule )
			, pBuilder( pBuilder )
		{ }

		/// <summary> ctor </summary>
		/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
		/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
		SceneParser::SceneParser( Scheduler&& schedule, SceneBuilderSPtr&& pBuilder )
			: JsonParser( schedule )
			, pBuilder( pBuilder )
		{ }

		/// <summary> ctor </summary>
		/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
		/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
		SceneParser::SceneParser( const Scheduler& schedule, SceneBuilderSPtr&& pBuilder )
			: JsonParser( schedule )
			, pBuilder( pBuilder )
		{ }

		/// <summary> ctor </summary>
		/// <param name="schedule"> A functor that will schedule another functor to execute. </param>
		/// <param name="pBuilder"> A pointer to the scene builder interface. </param>
		SceneParser::SceneParser( Scheduler&& schedule, const SceneBuilderSPtr& pBuilder )
			: JsonParser( schedule )
			, pBuilder( pBuilder )
		{ }

		/// <summary> The parsing function that will interpret the json data. </summary>
		/// <param name="pRoot"> The json object root node. </param>
		void SceneParser::parseJSON(const cJSONSPtr& pRoot )
		{
			PRINT_PARSER_DATA( "Parsing Scene Node - Stared." << endl << endl );

			cJSON* pScene = cJSON_GetObjectItem( pRoot.get( ), "scene" );

			// A function that iterates over all the nodes in an array specified by the name.
			const auto forEach = [ ]( const cJSON* pScene, const char* name, const function< void ( const cJSON* ) >& funCall )
			{
				cJSON* pNode = cJSON_GetObjectItem( pScene, name );

				const int count = cJSON_GetArraySize( pNode );
				for( int i = 0; i < count; ++i )
				{
					funCall( cJSON_GetArrayItem( pNode, i ) );
				}
			};

			forEach( pScene, "spheres"           , bind( &SceneParser::parseSphere           , this, param::_1 ) );
			forEach( pScene, "heightMaps"        , bind( &SceneParser::parseHeightMap        , this, param::_1 ) );
			forEach( pScene, "pcgHeightMaps"     , bind( &SceneParser::parsePCGHeightMap     , this, param::_1 ) );
			forEach( pScene, "cubes"             , bind( &SceneParser::parseCube             , this, param::_1 ) );
			forEach( pScene, "water"             , bind( &SceneParser::parseWaterPlane       , this, param::_1 ) );
			forEach( pScene, "pcgWater"          , bind( &SceneParser::parsePCGWaterPlane    , this, param::_1 ) );
			forEach( pScene, "particleSystems"   , bind( &SceneParser::parseParicleSystem    , this, param::_1 ) );
			forEach( pScene, "pbrSpheres"        , bind( &SceneParser::parsePbrSphere        , this, param::_1 ) );
			forEach( pScene, "lights"            , bind( &SceneParser::parseLight            , this, param::_1 ) );
			forEach( pScene, "planets"           , bind( &SceneParser::parsePlanet           , this, param::_1 ) );
			forEach( pScene, "moons"             , bind( &SceneParser::parseMoon             , this, param::_1 ) );
			forEach( pScene, "zones"             , bind( &SceneParser::parseZone             , this, param::_1 ) );
			forEach( pScene, "objects"           , bind( &SceneParser::parseObject           , this, param::_1 ) );
			forEach( pScene, "translucentObjects", bind( &SceneParser::parseTranslucentObject, this, param::_1 ) );
			forEach( pScene, "translucentTests"  , bind( &SceneParser::parseTranslucentTest  , this, param::_1 ) );
			forEach( pScene, "text3D"            , bind( &SceneParser::parseText3D           , this, param::_1 ) );
			forEach( pScene, "text2D"            , bind( &SceneParser::parseText2D           , this, param::_1 ) );
			forEach( pScene, "pcgTree"           , bind( &SceneParser::parsePCGTree          , this, param::_1 ) );

			SceneParser::parseFog( cJSON_GetObjectItem( pScene, "fog" ) );
			SceneParser::parseSkyBox( cJSON_GetObjectItem( pScene, "skyBox" ) );

			pBuilder->bake( );
			PRINT_PARSER_DATA( endl << "Parsing Scene Node - Complete." << endl << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a simple billboard moons. </summary>
		/// <param name="pRoot"> The json moon node. </param>
		void SceneParser::parseMoon( const cJSON * pMoon )
		{
			PRINT_PARSER_DATA( "\tMoon: " );
			vec3  position;
			float radius;
			vector< string > textures( 2 );

			if( parse( position, pMoon, "position" ) ) { }
			if( parse( radius  , pMoon, "radius"   ) ) { }

			const cJSON* pPlanetTextures = cJSON_GetObjectItem( pMoon, "textures" );

			if( parse( textures[ 0 ], pPlanetTextures, "surface" ) ) { }
			if( parse( textures[ 1 ], pPlanetTextures, "bump" ) ) { }

			pBuilder->addMoon( position, radius, textures );
			PRINT_PARSER_DATA( "[" << position.x << ", " << position.y << ", " << position.z << "] " << radius << "r [ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a simple billboard planets. </summary>
		/// <param name="pRoot"> The json planet node. </param>
		void SceneParser::parsePlanet( const cJSON* pPlanet )
		{
			PRINT_PARSER_DATA( "\tPlanet: " );
			vec3  position;
			float radius;
			vector< string > textures( 5 );

			if( parse( position, pPlanet, "position" ) ) { }
			if( parse( radius  , pPlanet, "radius"   ) ) { }

			const cJSON* pPlanetTextures = cJSON_GetObjectItem( pPlanet, "textures" );

			if( parse( textures[ 0 ], pPlanetTextures, "day" ) ) { }
			if( parse( textures[ 1 ], pPlanetTextures, "night" ) ) { }
			if( parse( textures[ 2 ], pPlanetTextures, "bump" ) ) { }
			if( parse( textures[ 3 ], pPlanetTextures, "specular" ) ) { }
			if( parse( textures[ 4 ], pPlanetTextures, "cloud" ) ) { }

			pBuilder->addPlanet( position, radius, textures );
			PRINT_PARSER_DATA( "[" << position.x << ", " << position.y << ", " << position.z << "] " << radius << "r [ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a simple billboard sphere. </summary>
		/// <param name="pRoot"> The json sphere node. </param>
		void SceneParser::parseSphere( const cJSON * pSphere )
		{
			PRINT_PARSER_DATA( "\tSphere: " );

			vec3  position;
			float radius;
			PSObjectBuffer psProperties;

			if( parse( position, pSphere, "position" ) ) { }
			if( parse( radius, pSphere, "radius" ) ) { }

			if( parse( psProperties.ka, pSphere, "ka" ) ) { }
			if( parse( psProperties.kd, pSphere, "kd" ) ) { }
			if( parse( psProperties.ks, pSphere, "ks" ) ) { }

			pBuilder->addSphere( position, radius, psProperties );

			PRINT_PARSER_DATA( "[" << position.x << ", " << position.y << ", " << position.z << "] " << radius << "r [ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a zone object. </summary>
		/// <param name="pRoot"> The json object zone node. </param>
		void SceneParser::parseZone( const cJSON* pZone )
		{
			string imagepath;
			vec3 position;


			struct Material
			{
				vector< string > textures;
				vec3 ka;
				vec3 kd;
				vec3 ks;
				float ns;

				float parallax;
				vec2 bump;
			};

			struct Index
			{
				ivec3 colour;
				string name;
				vec2 size;

				Material material;
			};

			unordered_map< int, Index > indexMap;


			if( !parse( imagepath, pZone, "file" ) )
			{
				return;
			}
			if( parse( position, pZone, "position" ) ) { }

			cJSON* pIndex = cJSON_GetObjectItem( pZone, "index" );
			int indexCount = cJSON_GetArraySize( pIndex );
			for( int i = 0; i < indexCount; ++i )
			{
				cJSON* pNodeIndex = cJSON_GetArrayItem( pIndex, i );
				cJSON* pMaterial  = cJSON_GetObjectItem( pNodeIndex, "material" );

				Index index;

				if( parse( index.colour, pNodeIndex, "colour" ) ) { }
				if( parse( index.size, pNodeIndex, "size" ) ) { }
				if( parse( index.name, pNodeIndex, "name" ) ) { }


				// MATERIAL
				index.material.textures.resize( 3 );

				cJSON* pTextures = cJSON_GetObjectItem( pMaterial, "textures" );

				if( parse( index.material.textures[ 0 ], pTextures, "albedo" ) ) { }
				if( parse( index.material.textures[ 1 ], pTextures, "normal" ) ) { }
				if( parse( index.material.textures[ 2 ], pTextures, "height" ) ) { }


				if( parse( index.material.ka, pMaterial, "ka" ) ) { }
				if( parse( index.material.kd, pMaterial, "kd" ) ) { }
				if( parse( index.material.ks, pMaterial, "ks" ) ) { }
				if( parse( index.material.ns, pMaterial, "ns" ) ) { }
				if( parse( index.material.parallax, pMaterial, "parallax" ) ) { }
				if( parse( index.material.bump, pMaterial, "bump" ) ) { }


				int colour = 0;
				colour = index.colour.r;
				colour = ( colour << 8 ) + index.colour.g;
				colour = ( colour << 8 ) + index.colour.b;

				indexMap[ colour ] = index;
			}

			/**

			std::experimental::filesystem::path filePath( file );
			std::experimental::filesystem::path aFilePath( std::experimental::filesystem::absolute( filePath ) );


			gli::texture zoneMap( gli::load( filePath.generic_string( ) ) );

			std::cout << "Zone:" << file << std::endl;
			std::cout << "    :" << filePath.string( ) << std::endl;

			if( std::experimental::filesystem::is_regular_file( filePath ) )
			{
				std::cout << "    : Regular File" << std::endl;
			}

			std::cout << "    :" << aFilePath.string( ) << std::endl;

			if( std::experimental::filesystem::is_regular_file( aFilePath ) )
			{
				std::cout << "    : Regular File" << std::endl;
			}

			if( zoneMap.empty( ) )
			{
				std::cout << "    : EMPTY" << std::endl;
			}
			*/


			// Data read from the header of the BMP file
			unsigned char header[ 54 ]; // Each BMP file begins by a 54-bytes header
			unsigned int dataPos;     // Position in the file where the actual data begins
			unsigned int width, height;
			unsigned int imageSize;   // = width*height*3
									  // Actual RGB data
			unsigned char * data;

			// Open the file
#ifdef WIN32
			FILE * file = nullptr;
			if( !fopen_s( &file, imagepath.c_str( ), "rb" ) )
			{ printf( "Image could not be opened\n" ); return; }
#else
			FILE * file = fopen( imagepath.c_str( ), "rb" );
			if( !file ) { printf( "Image could not be opened\n" ); return; }
#endif

			if( fread( header, 1, 54, file ) != 54 )
			{ // If not 54 bytes read : problem
				printf( "Not a correct BMP file\n" );
				return;
			}

			if( header[ 0 ] != 'B' || header[ 1 ] != 'M' )
			{
				printf( "Not a correct BMP file\n" );
				return;
			}

			// Read ints from the byte array
			dataPos    = *(int*)&(header[0x0A]);
			imageSize  = *(int*)&(header[0x22]);
			width      = *(int*)&(header[0x12]);
			height     = *(int*)&(header[0x16]);

			// Some BMP files are misformatted, guess missing information
			if( imageSize == 0 )    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
			if( dataPos == 0 )      dataPos = 54; // The BMP header is done that way

												  // Create a buffer
			data = new unsigned char[ imageSize ];

			// Read the actual data from the file into the buffer
			fread( data, 1, imageSize, file );

			//Everything is in memory now, the file can be closed
			fclose( file );


			unsigned int x = 0;
			unsigned int r = 0;
			unsigned int g = 0;
			unsigned int b = 0;
			for( unsigned int i = 0; i < width; ++i )
			{
				for( unsigned int j = 0; j < height; ++j )
				{
					x = ( i + ( j * height ) ) * 3;

					b = data[ x     ];
					g = data[ x + 1 ];
					r = data[ x + 2 ];

					int colour = 0;
					colour = r;
					colour = ( colour << 8 ) + g;
					colour = ( colour << 8 ) + b;

					unordered_map< int, Index >::iterator search;
					search = indexMap.find( colour );

					if( search != indexMap.end( ) )
					{
						/**
						if( search->second.name == "wall" )
						{
							::std::vector< ::std::string > tex;
							tex.push_back( search->second.material.textures[ 0 ] );
							tex.push_back( search->second.material.textures[ 1 ] );
							tex.push_back( search->second.material.textures[ 2 ] );
							tex.push_back( "../Bin/Data/Textures/Rock/ROCK_AO_4k.jpg" );
							tex.push_back( "../Bin/Data/Textures/Rock/ROCK_Bump_4k.jpg" );
							tex.push_back( "../Bin/Data/Textures/Rock/ROCK_Gloss_4k.jpg" );
							tex.push_back( "../Bin/Data/Textures/Rock/ROCK_Hi_Gloss_4k.jpg" );
							tex.push_back( "../Bin/Data/Textures/Rock/ROCK_Roughness_4k.jpg" );

							Mesh< Vertex3f2f3f3f3f::Vertex > wall = Shapes::wall( 0.5f, 2.0f );
							pBuilder->addMesh( "wall", wall, { i - 0.5, 0, j }, search->second.material.textures );
						}
						else
						{*/
						PSObjectBuffer psProperties;
						psProperties.ka = search->second.material.ka;
						psProperties.kd = search->second.material.kd;
						psProperties.ks = search->second.material.ks;
						psProperties.bumpScale = search->second.material.bump;


							pBuilder->addGsCube(
								{ i, -0.5, j },
								{ 0, 0, 0 },
								{ search->second.size.x, search->second.size.y, 1.0f },
								search->second.material.textures,
								psProperties
							);
						//}
					}
					else
					{
						/**
						if( r == 255 && g == 255 && b == 255 )
						{
							std::cout << "O";
							pBuilder->addSphere( { i, 3.5, j }, 0.5f );
						}
						else if( r == 255 && g == 0 && b == 0 )
						{
							std::cout << "+";
							pBuilder->addSphere( { i, 3, j }, 0.5f );
						}
						else
						{
							pBuilder->addSphere( { i, 3, j }, 0.1f );
							std::cout << " ";
						}
						*/
					}

					//std::cout << "[ " << std::setfill( '0' ) << std::setw( 3 ) << x << " ]";
				}
				//std::cout << std::endl;
			}

			//std::cout << "Size: [ " << width << ", " << height << " ]" << imageSize << std::endl;


			delete[ ] data;
		}

		/// <summary> The parsing function that will interpret the json data for a model object file. </summary>
		/// <param name="pRoot"> The json object objects node. </param>
		void SceneParser::parseObject( const cJSON * pObject )
		{
			PRINT_PARSER_DATA( "\tObject: " );

			string objpath;
			vec3 position( 0, 0, 0 );
			vec3 rotation( 0, 0, 0 );
			vec3 scale( 1, 1, 1 );

			if( !parse( objpath, pObject, "file" ) )
			{
				return;
			}

			if( parse( position, pObject, "position" ) ) { }
			if( parse( rotation, pObject, "rotation" ) ) { }
			if( parse( scale   , pObject, "scale"    ) ) { }

			ObjParser parser;
			const vector< ObjData > objs = parser.parseObj( objpath );

			int i = 0;
			for( const ObjData& obj : objs )
			{
				vector< string > tex;

				tex.push_back( obj.material.map_Kd );
				if( obj.material.normal != "" )
				{
					tex.push_back( obj.material.normal );
				}

				PSObjectBuffer psProperties;
				psProperties.ka = obj.material.Ka;
				psProperties.kd = obj.material.Kd;
				psProperties.ks = obj.material.Ks;

				if( obj.material.illum == 9 )
				{
					pBuilder->addClippedMesh(
						objpath + to_string( ++i ),
						obj.geometry,
						position,
						rotation,
						scale,
						tex,
						psProperties );
				}
				else
				{
					pBuilder->addBasicMesh(
						objpath + to_string( ++i ),
						obj.geometry,
						position,
						rotation,
						scale,
						tex,
						psProperties );
				}
			}

			PRINT_PARSER_DATA( "( " << objs.size( ) << " ) [ OK ] --> \"" << objpath << "\"" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a model object file. </summary>
		/// <param name="pRoot"> The json object objects node. </param>
		void SceneParser::parseTranslucentObject( const cJSON * pObject )
		{
			PRINT_PARSER_DATA( "\tTranslucent Object: " );

			string objpath;
			vec3 position( 0, 0, 0 );
			vec3 rotation( 0, 0, 0 );
			vec3 scale( 1, 1, 1 );

			if( !parse( objpath, pObject, "file" ) )
			{
				return;
			}

			if( parse( position, pObject, "position" ) ) { }
			if( parse( rotation, pObject, "rotation" ) ) { }
			if( parse( scale, pObject, "scale" ) ) { }

			ObjParser parser;
			vector< ObjData > objs = parser.parseObj( objpath );

			PSObjectBuffer material;

			int i = 0;
			for( const ObjData& obj : objs )
			{
				vector< string > tex;
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_Color_4k.jpg" );
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_Normal_4k.jpg" );
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_Depth_4k.png" );
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_AO_4k.jpg" );
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_Bump_4k.jpg" );
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_Gloss_4k.jpg" );
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_Hi_Gloss_4k.jpg" );
				tex.emplace_back( "../Bin/Data/Textures/Rock/ROCK_Roughness_4k.jpg" );

				ZeroMemory( &material, sizeof( material ) );

				material.ka = obj.material.Ka;
				material.kd = obj.material.Kd;
				material.ks = obj.material.Ks;

				tex[ 0 ] = obj.material.map_Kd;
				tex[ 1 ] = obj.material.normal;
				tex[ 2 ] = obj.material.displacement;
				tex[ 3 ] = obj.material.map_Ka;
				tex[ 4 ] = obj.material.normal;
				tex[ 5 ] = obj.material.map_Ks;
				tex[ 6 ] = obj.material.map_Ns;
				tex[ 7 ] = obj.material.map_Pr;

				material.metalness = 0.9f;
				material.roughness = 0.1f;

				material.bumpScale = vec2( 0.5f, 0.5f );
				material.ssPower = 10;
				material.ssScale = 4;

				material.ior = 2.0f;
				material.sAlpha = 0.5f;
				material.ssAlpha = 0.5f;
				material.absorption = 0.25f;

				pBuilder->addTranslucentMesh(
					objpath + to_string( ++i ),
					obj.geometry,
					position,
					rotation,
					scale,
					tex,
					material );
			}

			PRINT_PARSER_DATA( "( " << objs.size( ) << " ) [ OK ] --> \"" << objpath << "\"" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a model object file. </summary>
		/// <param name="pObject"> The json object objects node. </param>
		void SceneParser::parseTranslucentTest( const cJSON * pObject )
		{
			PRINT_PARSER_DATA( "\tTranslucent Test: " );

			string objPath;
			vec3 position( 0, 0, 0 );
			vec3 rotation( 0, 0, 0 );
			vec3 scale( 1, 1, 1 );


			int count = 1;
			vec3 kaStart( 1, 1, 1 );
			vec3 kaEnd( 1, 1, 1 );
			vec3 kdStart( 1, 1, 1 );
			vec3 kdEnd( 1, 1, 1 );
			vec3 ksStart( 1, 1, 1 );
			vec3 ksEnd( 1, 1, 1 );

			vec2 roughness( 1, 1 );
			vec2 metalness( 1, 1 );
			vec2 sAlpha( 1, 1 );
			vec2 ssAlpha( 1, 1 );
			vec2 absorption( 1, 1 );
			vec2 ior( 1, 1 );
			vec2 ssScale( 1, 1 );
			vec2 ssPower( 1, 1 );
			vec2 ssBump( 0, 0 );

			if( !parse( objPath, pObject, "file" ) )
			{
				return;
			}

			if( parse( position, pObject, "position" ) ) { }
			if( parse( rotation, pObject, "rotation" ) ) { }
			if( parse( scale, pObject, "scale" ) ) { }



			if( parse( count, pObject, "count" ) ) { }


			if( parse( kaStart, pObject, "kaStart" ) ) { }
			if( parse( kaEnd, pObject, "kaEnd" ) ) { }
			if( parse( kdStart, pObject, "kdStart" ) ) { }
			if( parse( kdEnd, pObject, "kdEnd" ) ) { }
			if( parse( ksStart, pObject, "ksStart" ) ) { }
			if( parse( ksEnd, pObject, "ksEnd" ) ) { }


			if( parse( roughness, pObject, "roughness" ) ) { }
			if( parse( metalness, pObject, "metalness" ) ) { }
			if( parse( sAlpha, pObject, "sAlpha" ) ) { }
			if( parse( ssAlpha, pObject, "ssAlpha" ) ) { }
			if( parse( absorption, pObject, "absorption" ) ) { }
			if( parse( ior, pObject, "ior" ) ) { }
			if( parse( ssScale, pObject, "ssScale" ) ) { }
			if( parse( ssPower, pObject, "ssPower" ) ) { }
			if( parse( ssBump, pObject, "ssBump" ) ) { }




			ObjParser parser;
			vector< ObjData > objs = parser.parseObj( objPath );

			float step = 1.0f / static_cast< float >( count - 1 );
			PSObjectBuffer material;

			for( int i = 0; i < count; i++ )
			{
				ZeroMemory( &material, sizeof( material ) );

				float a    = i * step;
				float invA = 1 - a;
				material.ka         = kaStart      * invA + kaEnd        * a;
				material.kd         = kdStart      * invA + kdEnd        * a;
				material.ks         = ksStart      * invA + ksEnd        * a;
				material.roughness  = roughness.x  * invA + roughness.y  * a;
				material.metalness  = metalness.x  * invA + metalness.y  * a;
				material.sAlpha     = sAlpha.x     * invA + sAlpha.y     * a;
				material.ssAlpha    = ssAlpha.x    * invA + ssAlpha.y    * a;
				material.absorption = absorption.x * invA + absorption.y * a;
				material.ior        = ior.x        * invA + ior.y        * a;
				material.ssScale    = ssScale.x    * invA + ssScale.y    * a;
				material.ssPower    = ssPower.x    * invA + ssPower.y    * a;
				material.ssBump     = ssBump.x     * invA + ssBump.y     * a;

				vec3 testPosition = position;
				testPosition.z += i * 2;

				int part = 0;
				for( const ObjData& obj : objs )
				{
					vector< string > tex;
					tex.emplace_back( "../Bin/Data/Textures/BlankWhite.bmp" );
					tex.emplace_back( "../Bin/Data/Textures/BlankBlack.bmp" );
					tex.emplace_back( "../Bin/Data/Textures/BlankWhite.bmp" );
					tex.emplace_back( "../Bin/Data/Textures/BlankWhite.bmp" );
					tex.emplace_back( "../Bin/Data/Textures/BlankWhite.bmp" );
					tex.emplace_back( "../Bin/Data/Textures/BlankWhite.bmp" );
					tex.emplace_back( "../Bin/Data/Textures/BlankWhite.bmp" );
					tex.emplace_back( "../Bin/Data/Textures/BlankWhite.bmp" );

					tex[ 0 ] = obj.material.map_Kd;
					tex[ 1 ] = obj.material.normal;
					tex[ 2 ] = obj.material.displacement;
					tex[ 3 ] = obj.material.map_Ka;
					tex[ 4 ] = obj.material.normal;
					tex[ 5 ] = obj.material.map_Ks;
					tex[ 6 ] = obj.material.map_Ns;
					tex[ 7 ] = obj.material.map_Pr;

					pBuilder->addTranslucentMesh(
						objPath + to_string( ++part ) + "Translucent",
						obj.geometry,
						testPosition,
						rotation,
						scale,
						tex,
						material );
				}
			}

			PRINT_PARSER_DATA( "( " << objs.size( ) << " ) [ OK ] --> \"" << objPath << "\"" << endl );
		}


		/// <summary> The parsing function that will interpret the json data for a height map. </summary>
		/// <param name="pHeightMap"> The json object height map node. </param>
		void SceneParser::parseHeightMap( const cJSON* pHeightMap )
		{
			PRINT_PARSER_DATA( "\tHeight Map: " );

			string file;
			ivec2 size;
			vec3 position;
			vec3 scale( 1, 1, 1 );

			PSObjectBuffer psProperties;

			SceneBuilder::Textures textures( 12 );

			for( SceneBuilder::LoadTexture& texture : textures )
			{
				texture.forceSRGB = false;
			}

			if( parse( file, pHeightMap, "file" ) ) { }
			if( parse( size, pHeightMap, "size" ) ) { }
			if( parse( position, pHeightMap, "position" ) ) { }
			if( parse( scale, pHeightMap, "scale" ) ) { }


			cJSON* pLayers = cJSON_GetObjectItem( pHeightMap, "layers" );
			cJSON* pLayer1 = cJSON_GetArrayItem( pLayers, 0 );
			cJSON* pLayer2 = cJSON_GetArrayItem( pLayers, 1 );
			cJSON* pLayer3 = cJSON_GetArrayItem( pLayers, 2 );

			if( parse( textures[  0 ].forceSRGB, pLayer1, "gammaCorrect" ) ) { }
			if( parse( textures[  0 ].fileName, pLayer1, "albedo" ) ) { }
			if( parse( textures[  1 ].fileName, pLayer1, "normal" ) ) { }
			if( parse( textures[  2 ].fileName, pLayer1, "height" ) ) { }
			if( parse( textures[  3 ].fileName, pLayer1, "roughness" ) ) { }

			if( parse( textures[  4 ].forceSRGB, pLayer2, "gammaCorrect" ) ) { }
			if( parse( textures[  4 ].fileName, pLayer2, "albedo" ) ) { }
			if( parse( textures[  5 ].fileName, pLayer2, "normal" ) ) { }
			if( parse( textures[  6 ].fileName, pLayer2, "height" ) ) { }
			if( parse( textures[  7 ].fileName, pLayer2, "roughness" ) ) { }

			if( parse( textures[  8 ].forceSRGB, pLayer3, "gammaCorrect" ) ) { }
			if( parse( textures[  8 ].fileName, pLayer3, "albedo" ) ) { }
			if( parse( textures[  9 ].fileName, pLayer3, "normal" ) ) { }
			if( parse( textures[ 10 ].fileName, pLayer3, "height" ) ) { }
			if( parse( textures[ 11 ].fileName, pLayer3, "roughness" ) ) { }


			if( parse( psProperties.ka, pHeightMap, "ka" ) ) { }
			if( parse( psProperties.kd, pHeightMap, "kd" ) ) { }
			if( parse( psProperties.ks, pHeightMap, "ks" ) ) { }

			pBuilder->addHeightMap( file, position, scale, size, textures, psProperties );

			PRINT_PARSER_DATA( "[" << size.x << ", " << size.y << "] - " << file  << " [ OK ]"<< endl );
		}

		/// <summary> The parsing function that will interpret the json data for a pcg height map. </summary>
		/// <param name="pHeightMap"> The json object height map node. </param>
		void SceneParser::parsePCGHeightMap( const cJSON* pHeightMap )
		{
			PRINT_PARSER_DATA( "\tPCG Height Map: " );

			ivec2 size;
			vec3 scale( 1, 1, 1 );

			PSObjectBuffer psProperties;

			SceneBuilder::Textures textures( 12 );

			for( SceneBuilder::LoadTexture& texture : textures )
			{
				texture.forceSRGB = false;
			}

			if( parse( size, pHeightMap, "size" ) ) { }
			if( parse( scale, pHeightMap, "scale" ) ) { }


			cJSON* pLayers = cJSON_GetObjectItem( pHeightMap, "layers" );
			cJSON* pLayer1 = cJSON_GetArrayItem( pLayers, 0 );
			cJSON* pLayer2 = cJSON_GetArrayItem( pLayers, 1 );
			cJSON* pLayer3 = cJSON_GetArrayItem( pLayers, 2 );

			if( parse( textures[  0 ].forceSRGB, pLayer1, "gammaCorrect" ) ) { }
			if( parse( textures[  0 ].fileName, pLayer1, "albedo" ) ) { }
			if( parse( textures[  1 ].fileName, pLayer1, "normal" ) ) { }
			if( parse( textures[  2 ].fileName, pLayer1, "height" ) ) { }
			if( parse( textures[  3 ].fileName, pLayer1, "roughness" ) ) { }

			if( parse( textures[  4 ].forceSRGB, pLayer2, "gammaCorrect" ) ) { }
			if( parse( textures[  4 ].fileName, pLayer2, "albedo" ) ) { }
			if( parse( textures[  5 ].fileName, pLayer2, "normal" ) ) { }
			if( parse( textures[  6 ].fileName, pLayer2, "height" ) ) { }
			if( parse( textures[  7 ].fileName, pLayer2, "roughness" ) ) { }

			if( parse( textures[  8 ].forceSRGB, pLayer3, "gammaCorrect" ) ) { }
			if( parse( textures[  8 ].fileName, pLayer3, "albedo" ) ) { }
			if( parse( textures[  9 ].fileName, pLayer3, "normal" ) ) { }
			if( parse( textures[ 10 ].fileName, pLayer3, "height" ) ) { }
			if( parse( textures[ 11 ].fileName, pLayer3, "roughness" ) ) { }


			if( parse( psProperties.ka, pHeightMap, "ka" ) ) { }
			if( parse( psProperties.kd, pHeightMap, "kd" ) ) { }
			if( parse( psProperties.ks, pHeightMap, "ks" ) ) { }

			pBuilder->addPCGHeightMap( scale, size, textures, psProperties );

			PRINT_PARSER_DATA( "[" << size.x << ", " << size.y << "] [ OK ]"<< endl );
		}

		/// <summary> The parsing function that will interpret the json data for a height map. </summary>
		/// <param name="pCube"> The json object cube node. </param>
		void SceneParser::parseCube( const cJSON* pCube )
		{
			PRINT_PARSER_DATA( "\tCube: " );

			vec3 position;
			vec3 rotation;
			vec3 scale( 1, 1, 1 );

			PSObjectBuffer psProperties;

			vector< string > textures( 9 );

			if( parse( position, pCube, "position" ) ) { }
			if( parse( rotation, pCube, "rotation" ) ) { }
			if( parse( scale, pCube, "scale" ) ) { }

			cJSON* pTextures = cJSON_GetObjectItem( pCube, "textures" );

			if( parse( textures[ 0 ], pTextures, "albedo" ) ) { }
			if( parse( textures[ 1 ], pTextures, "normal" ) ) { }
			if( parse( textures[ 2 ], pTextures, "height" ) ) { }
			if( parse( textures[ 3 ], pTextures, "ao" ) ) { }
			if( parse( textures[ 4 ], pTextures, "bump" ) ) { }
			if( parse( textures[ 5 ], pTextures, "gloss" ) ) { }
			if( parse( textures[ 6 ], pTextures, "hiGloss" ) ) { }
			if( parse( textures[ 7 ], pTextures, "roughness" ) ) { }
			if( parse( textures[ 8 ], pTextures, "metalness" ) ) { }

			if( parse( psProperties.ka, pCube, "ka" ) ) { }
			if( parse( psProperties.kd, pCube, "kd" ) ) { }
			if( parse( psProperties.ks, pCube, "ks" ) ) { }

			if( parse( psProperties.bumpScale, pCube, "bump" ) ) { }

			pBuilder->addCube( position, rotation, scale, textures, psProperties );

			PRINT_PARSER_DATA( "[ OK ]" << endl );
		}



		/// <summary> The parsing function that will interpret the json data for a water plane. </summary>
		/// <param name="pWaterPlane"> The json object water pane node. </param>
		void SceneParser::parseWaterPlane( const cJSON* pWaterPlane )
		{
			PRINT_PARSER_DATA( "\tWater: " );

			vec3 position;
			vec3 rotation;
			vec3 scale( 1, 1, 1 );

			PSObjectBuffer psProperties;

			vector< string > textures( 2 );

			if( parse( position, pWaterPlane, "position" ) ) { }
			if( parse( rotation, pWaterPlane, "rotation" ) ) { }
			if( parse( scale, pWaterPlane, "scale" ) ) { }

			cJSON* pTextures = cJSON_GetObjectItem( pWaterPlane, "textures" );

			if( parse( textures[ 0 ], pTextures, "layer1" ) ) { }
			if( parse( textures[ 1 ], pTextures, "layer2" ) ) { }

			if( parse( psProperties.ka, pWaterPlane, "ka" ) ) { }
			if( parse( psProperties.kd, pWaterPlane, "kd" ) ) { }
			if( parse( psProperties.ks, pWaterPlane, "ks" ) ) { }

			pBuilder->addWaterPlane( position, rotation, scale, textures, psProperties );
			PRINT_PARSER_DATA( "[ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a water plane. </summary>
		/// <param name="pWaterPlane"> The json object water pane node. </param>
		void SceneParser::parsePCGWaterPlane( const cJSON* pWaterPlane )
		{
			PRINT_PARSER_DATA( "\tPCG Water: " );

			vec3 position;
			vec3 rotation;
			vec3 scale( 1, 1, 1 );
			bool follow = false;

			PSObjectBuffer psProperties;

			vector< string > textures =
			{
				"BlankNormal",
				"BlankNormal"
			};

			if( parse( position, pWaterPlane, "position" ) ) { }
			if( parse( rotation, pWaterPlane, "rotation" ) ) { }
			if( parse( scale, pWaterPlane, "scale" ) ) { }

			cJSON* pTextures = cJSON_GetObjectItem( pWaterPlane, "textures" );

			if( parse( textures[ 0 ], pTextures, "layer1" ) ) { }
			if( parse( textures[ 1 ], pTextures, "layer2" ) ) { }

			if( parse( psProperties.ka, pWaterPlane, "ka" ) ) { }
			if( parse( psProperties.kd, pWaterPlane, "kd" ) ) { }
			if( parse( psProperties.ks, pWaterPlane, "ks" ) ) { }

			if( parse( follow, pWaterPlane, "followCamera" ) ) { }

			pBuilder->addPCGWaterPlane( position, rotation, scale, textures, psProperties, follow );
			PRINT_PARSER_DATA( "[ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for the sky box. </summary>
		/// <param name="pSkyBox"> The json object sky box node. </param>
		void SceneParser::parseSkyBox( const cJSON* pSkyBox )
		{
			// Get the sky type. With out this we cant load it.
			if( ! pSkyBox )
			{
				return;
			}

			PRINT_PARSER_DATA( "\tSky Box: " );

			vec3 sunDirection( 0, 1, 0 );
			vector< string > textures;
			string skyType;

			if( ! parse( skyType, pSkyBox, "type" ) )
			{
				PRINT_PARSER_DATA( "No \"type\" field ( BasicVolume, Cube or Polar ) [ FAILED ]" << endl );
				return;
			}

			if( parse( sunDirection, pSkyBox, "sunDirection" ) ){ }

			if( skyType == "BasicVolume" )
			{
				textures.resize( 1 );
				if( parse( textures[ 0 ], pSkyBox, "stars" ) ) { }

				pBuilder->addBasicVolumeSkybox( textures, sunDirection );
				PRINT_PARSER_DATA( "Basic Volume [ OK ]" << endl );
			}
			else if( skyType == "Cube" )
			{
				textures.resize( 1 );
				if( parse( textures[ 0 ], pSkyBox, "skyBox" ) ) { }

				pBuilder->addCubeSkybox( textures, sunDirection );
				PRINT_PARSER_DATA( "Cube [ OK ]" << endl );
			}
			else if( skyType == "Polar" )
			{
				textures.resize( 1 );
				if( parse( textures[ 0 ], pSkyBox, "skyMap" ) ) { }

				pBuilder->addPolarSkybox( textures, sunDirection );
				PRINT_PARSER_DATA( "Polar [ OK ]" << endl );
			}
			else
			{
				PRINT_PARSER_DATA( skyType << " [ ERROR ]" << endl );
			}
		}



		/// <summary> The parsing function that will interpret the json data for a paricle system. </summary>
		/// <param name="pParicleSystem"> The json object paricle system node. </param>
		void SceneParser::parseParicleSystem( const cJSON* pParicleSystem )
		{
			PRINT_PARSER_DATA( "\tParicle System: " );

			Mesh< VertexParticle::Vertex >::VertexBuffer initialState;
			vector< string > textures( 1 );
			GSParticleBuffer gsProperties;
			PSParticleBuffer psProperties;
			unsigned int bufferSize;
			string blend;

			gsProperties.timeStep = 0;
			gsProperties.gameTime = 0;

			psProperties.imageCount = vec2( 1, 1 );
			psProperties.frameRate = 24;
			psProperties.frameCount = 1;

			auto parseParticleNode = [ this, &initialState ]( const cJSON* pParicle )
			{
				vec3 position;
				vec3 velocity;
				vec2 size;
				float age;
				string type;


				if( !parse( position, pParicle, "position"  ) ) { }
				if( !parse( velocity, pParicle, "velocity" ) ) { }
				if( !parse(     size, pParicle, "size"      ) ) { }
				if( !parse(      age, pParicle, "age"       ) ) { }
				if( !parse(     type, pParicle, "type"      ) ) { }

				initialState.emplace_back( position, velocity, size, age, 0.0f, type == "emitter" ? 0 : 1 );
			};

			if( !parse( gsProperties.emitPosW, pParicleSystem, "position" ) ) { }
			if( !parse( gsProperties.emitDirW, pParicleSystem, "velocity" ) ) { }
			if( !parse( gsProperties.flareSize, pParicleSystem, "size" ) ) { }
			if( !parse( gsProperties.emitPosWScale, pParicleSystem, "positionScale" ) ) { }
			if( !parse( gsProperties.emitDirWScale, pParicleSystem, "directionScale" ) ) { }
			if( !parse( gsProperties.emitTime, pParicleSystem, "emitTime" ) ) { }
			if( !parse( gsProperties.lifeTime, pParicleSystem, "lifeTime" ) ) { }
			if( !parse( gsProperties.ageLifeSizeScale, pParicleSystem, "ageSizeScale" ) ) { }
			if( !parse( gsProperties.attractor, pParicleSystem, "attractor" ) ) { }
			if( !parse( gsProperties.startColour, pParicleSystem, "startColour" ) ) { }
			if( !parse( gsProperties.endColour, pParicleSystem, "endColour" ) ) { }
			if( !parse( gsProperties.starColourScale, pParicleSystem, "starColourScale" ) ) { }
			if( !parse( gsProperties.endColourScale, pParicleSystem, "endColourScale" ) ) { }

			if( !parse( psProperties.imageCount, pParicleSystem, "imageCount" ) ) { }
			if( !parse( psProperties.frameRate, pParicleSystem, "frameRate" ) ) { }
			if( !parse( psProperties.frameCount, pParicleSystem, "frameCount" ) ) { psProperties.frameCount = psProperties.imageCount.x * psProperties.imageCount.y; }

			if( !parse( bufferSize, pParicleSystem, "count" ) ) { }
			if( !parse( textures[ 0 ], pParicleSystem, "texture" ) ) { }
			if( !parse( blend, pParicleSystem, "blend" ) ) { }


			cJSON* pArray = cJSON_GetObjectItem( pParicleSystem, "particles" );

			int count = cJSON_GetArraySize( pArray );
			for( int i = 0; i < count; ++i )
			{
				parseParticleNode( cJSON_GetArrayItem( pArray, i ) );
			}

			pBuilder->addParticleSystem(
				initialState,
				textures,
				gsProperties,
				psProperties,
				bufferSize,
				blend
			);

			PRINT_PARSER_DATA( textures[ 0 ] << " [ OK ]" << endl );
		}


		/// <summary> The parsing function that will interpret the json data for the volumetric fog system. </summary>
		/// <param name="pFog"> The json object paricle system node. </param>
		void SceneParser::parseFog( const cJSON* pFog )
		{
			if (pFog == nullptr)
			{
				return;
			}

			PRINT_PARSER_DATA( "\tScene Fog: " );

			float density = 0.0f;

			float heightFalloff = 1;
			float rayDepth = 0.0f;
			float gScatter = 0.0f;
			float waterHeight = 0.0f;

			if( !parse( density, pFog, "density" ) ) { }
			if( !parse( heightFalloff, pFog, "heightFalloff" ) ) { }
			if( !parse( rayDepth, pFog, "rayDepth" ) ) { }
			if( !parse( waterHeight, pFog, "waterHeight" ) ) { }
			if( !parse( gScatter, pFog, "gScatter" ) ) { }

			pBuilder->addFog( density, heightFalloff, rayDepth, gScatter, waterHeight );

			PRINT_PARSER_DATA( "[ OK ]" << endl );
		}



		/// <summary> The parsing function that will interpret the json data for a pbr sphere. </summary>
		/// <param name="pSphere"> The json object for a pbr sphere node. </param>
		void SceneParser::parsePbrSphere( const cJSON* pSphere )
		{
			PRINT_PARSER_DATA( "\tSphere: " );

			vec3  position( 0, 0, 0 );
			float radius( 0 );

			PSObjectBuffer psProperties;

			vector< string > textures( 7 );

			bool forceSRGB = false;

			if( parse( position, pSphere, "position" ) ) { }
			if( parse( radius, pSphere, "radius" ) ) { }

			if( parse( psProperties.ka, pSphere, "ka" ) ) { }
			if( parse( psProperties.kd, pSphere, "kd" ) ) { }
			if( parse( psProperties.ks, pSphere, "ks" ) ) { }
			if( parse( psProperties.bumpScale, pSphere, "bump" ) ) { }
			if( parse( forceSRGB, pSphere, "gammaCorrect" ) ) { }

			cJSON* pTextures = cJSON_GetObjectItem( pSphere, "textures" );

			if( parse( textures[ 0 ], pTextures, "Reflection" ) ) { }
			if( parse( textures[ 1 ], pTextures, "albedo" ) ) { }
			if( parse( textures[ 2 ], pTextures, "normal" ) ) { }
			if( parse( textures[ 3 ], pTextures, "height" ) ) { }
			if( parse( textures[ 4 ], pTextures, "roughness" ) ) { }
			if( parse( textures[ 5 ], pTextures, "metalness" ) ) { }
			if( parse( textures[ 6 ], pTextures, "occlusion" ) ) { }

			pBuilder->addPbrSphere( position, radius, textures, psProperties, forceSRGB );
			PRINT_PARSER_DATA( "[" << position.x << ", " << position.y << ", " << position.z << "] " << radius << "r [ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a pbr sphere. </summary>
		/// <param name="pLight"> The json object for a light node. </param>
		void SceneParser::parseLight( const cJSON* pLight )
		{
			PRINT_PARSER_DATA( "\tLight: " );

			LightEx light;
			ZeroMemory( &light, sizeof( light ) );

			vec3 position( 0, 0, 0 );
			vec3 direction( 0, -1, 0 );
			vec3 colour( 0, 0, 0 );

			string type;

			light.type = 0xffffffff;
			light.enabled = TRUE;

			if( parse( position, pLight, "position" ) ) { }
			if( parse( direction, pLight, "direction" ) ) { }
			if( parse( colour, pLight, "colour" ) ) { }
			if( parse( light.spotLightAngle, pLight, "spotLightAngle" ) ) { }
			if( parse( light.range, pLight, "range" ) ) { }
			if( parse( light.enabled, pLight, "enabled" ) ) { }
			if( parse( light.type, pLight, "type" ) ) { }
			if( parse( type, pLight, "type" ) ) { }

			light.positionWS.x = position.x;
			light.positionWS.y = position.y;
			light.positionWS.z = position.z;
			light.positionWS.w = 1;

			direction = normalize( direction );
			light.directionWS.x = direction.x;
			light.directionWS.y = direction.y;
			light.directionWS.z = direction.z;
			light.directionWS.w = 0;


			light.colour.r = colour.r;
			light.colour.g = colour.g;
			light.colour.b = colour.b;
			light.colour.a = 1;

			if( light.type == 0xffffffff )
			{
				light.type = type == "point" ? 0 :
							 type == "spot" ? 1 :
							 type == "direction" ? 2 :
							 0xffffffff;
			}

			pBuilder->addLight( light );
			PRINT_PARSER_DATA( type << " [ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for 3D text. </summary>
		/// <param name="pText"> The json object for a 3D text node. </param>
		void SceneParser::parseText3D( const cJSON* pText )
		{
			PRINT_PARSER_DATA( "\t3D Text: " );

			string font;
			string text;
			vec3 position( 0, 0, 0 );
			vec3 rotation( 0, 0, 0 );
			vec3 scale( 1, 1, 1 );
			//uvec4 parameters( 0, 0, 0, 0 );

			string mode;

			PSFontBuffer psProperties;

			vector< string > textures( 9 );

			if( parse( font, pText, "font" ) ) { }
			if( parse( text, pText, "text" ) ) { }
			if( parse( position, pText, "position" ) ) { }
			if( parse( rotation, pText, "rotation" ) ) { }
			if( parse( scale, pText, "scale" ) ) { }

			cJSON* pTextures = cJSON_GetObjectItem( pText, "textures" );

			if( parse( textures[ 0 ], pTextures, "albedo" ) ) { }
			if( parse( textures[ 1 ], pTextures, "normal" ) ) { }
			if( parse( textures[ 2 ], pTextures, "height" ) ) { }
			if( parse( textures[ 3 ], pTextures, "occlusion" ) ) { }
			if( parse( textures[ 4 ], pTextures, "bump" ) ) { }
			if( parse( textures[ 5 ], pTextures, "gloss" ) ) { }
			if( parse( textures[ 6 ], pTextures, "hiGloss" ) ) { }
			if( parse( textures[ 7 ], pTextures, "roughness" ) ) { }
			if( parse( textures[ 8 ], pTextures, "metalness" ) ) { }

			if( parse( psProperties.ka, pText, "ka" ) ) { }
			if( parse( psProperties.kd, pText, "kd" ) ) { }
			if( parse( psProperties.ks, pText, "ks" ) ) { }


			if( parse( mode, pText, "mode" ) ) { }

			if( mode == "std" )
			{
				psProperties.properties.x = 0x00;
			}
			else if( mode == "warp" )
			{
				psProperties.properties.x = 0x01;
			}


			pBuilder->addText3D( font, text, position, rotation, scale, textures, psProperties );
			PRINT_PARSER_DATA( "[ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for 2D text. </summary>
		/// <param name="pText"> The json object for a 2D text node. </param>
		void SceneParser::parseText2D( const cJSON* pText )
		{
			PRINT_PARSER_DATA( "\t2D Text: " );

			string font;
			string text;
			vec2 position( 0, 0 );
			vec2 rotation( 0, 0 );
			vec2 scale( 1, 1 );

			PSFontBuffer psProperties;

			vector< string > textures( 2 );

			if( parse( font, pText, "font" ) ) { }
			if( parse( text, pText, "text" ) ) { }
			if( parse( position, pText, "position" ) ) { }
			if( parse( rotation, pText, "rotation" ) ) { }
			if( parse( scale, pText, "scale" ) ) { }

			cJSON* pTextures = cJSON_GetObjectItem( pText, "textures" );

			if( parse( textures[ 0 ], pTextures, "layer1" ) ) { }
			if( parse( textures[ 1 ], pTextures, "layer2" ) ) { }

			if( parse( psProperties.ka, pText, "ka" ) ) { }
			if( parse( psProperties.kd, pText, "kd" ) ) { }
			if( parse( psProperties.ks, pText, "ks" ) ) { }

			pBuilder->addText2D( font, text, position, rotation, scale, textures, psProperties );
			PRINT_PARSER_DATA( "[ OK ]" << endl );
		}

		/// <summary> The parsing function that will interpret the json data for a pcg tree. </summary>
		/// <param name="pTree"> The json object for a pcg tree node. </param>
		void SceneParser::parsePCGTree( const cJSON* pTree )
		{
			PRINT_PARSER_DATA( "\tPCG Tree: " );

			static int pcgTreeUniqueID = 0;
			++pcgTreeUniqueID;


			vec3 position;
			vec3 rotation;
			vec3 scale( 1, 1, 1 );

			struct Material
			{
				PSObjectBuffer ps = PSObjectBuffer();
				vector< string > textures = vector< string >( 9 );
			};

			Material bark;
			Material leaf;

			TreeGeometry geometry;
			TreeProperties tree;
			tree.branches.clear( );

			if( parse( position, pTree, "position" ) ) { }
			if( parse( rotation, pTree, "rotation" ) ) { }
			if( parse( scale, pTree, "scale" ) ) { }

			if( parse( tree.shape     , pTree, "shape" ) ) { }
			if( parse( tree.baseSize  , pTree, "baseSize" ) ) { }
			if( parse( tree.scale     , pTree, "treeScale" ) ) { }
			if( parse( tree.scaleV    , pTree, "treeScaleV" ) ) { }
			if( parse( tree.zScale    , pTree, "zScale" ) ) { }
			if( parse( tree.zScaleV   , pTree, "zScaleV" ) ) { }
			if( parse( tree.ratio     , pTree, "ratio" ) ) { }
			if( parse( tree.ratioPower, pTree, "ratioPower" ) ) { }
			if( parse( tree.lobes     , pTree, "lobes" ) ) { }
			if( parse( tree.lobeDepth , pTree, "lobeDepth" ) ) { }
			if( parse( tree.flare     , pTree, "flare" ) ) { }
			if( parse( tree.tScale    , pTree, "tScale" ) ) { }
			if( parse( tree.tScaleV   , pTree, "tScaleV" ) ) { }
			if( parse( tree.baseSplits, pTree, "baseSplits" ) ) { }

			if( parse( tree.properties.length     , pTree, "length" ) ) { }
			if( parse( tree.properties.lengthV    , pTree, "lengthV" ) ) { }
			if( parse( tree.properties.taper      , pTree, "taper" ) ) { }
			if( parse( tree.properties.segSplits  , pTree, "segSplits" ) ) { }
			if( parse( tree.properties.splitAngle , pTree, "splitAngle" ) ) { }
			if( parse( tree.properties.splitAngleV, pTree, "splitAngleV" ) ) { }
			if( parse( tree.properties.curveRes   , pTree, "curveRes" ) ) { }
			if( parse( tree.properties.curve      , pTree, "curve" ) ) { }
			if( parse( tree.properties.curveBack  , pTree, "curveBack" ) ) { }
			if( parse( tree.properties.curveV     , pTree, "curveV" ) ) { }

			if( parse( tree.leaves.leaves        , pTree, "leaves" ) ) { }
			if( parse( tree.leaves.shape         , pTree, "leafShape" ) ) { }
			if( parse( tree.leaves.scale         , pTree, "leafScale" ) ) { }
			if( parse( tree.leaves.scaleX        , pTree, "leafScaleX" ) ) { }
			if( parse( tree.leaves.attractionUp  , pTree, "attractionUp" ) ) { }
			if( parse( tree.leaves.pruneRatio    , pTree, "pruneRatio" ) ) { }
			if( parse( tree.leaves.pruneWidth    , pTree, "pruneWidth" ) ) { }
			if( parse( tree.leaves.pruneWidthPeak, pTree, "pruneWidthPeak" ) ) { }
			if( parse( tree.leaves.prunePowerLow , pTree, "prunePowerLow" ) ) { }
			if( parse( tree.leaves.prunePowerHigh, pTree, "prunePowerHigh" ) ) { }
			if( parse( tree.leaves.gap           , pTree, "gap" ) ) { }
			if( parse( tree.leaves.gapV          , pTree, "gapV" ) ) { }
			if( parse( tree.leaves.smooth        , pTree, "smoothLeaf" ) ) { }

			const auto readMaterial = [ pTree ]( const string& name, Material& material ){

				cJSON* pMaterial = cJSON_GetObjectItem( pTree, name.c_str( ) );

				cJSON* pTextures = cJSON_GetObjectItem( pMaterial, "textures" );

				if( parse( material.textures[ 0 ], pTextures, "albedo" ) ) { }
				if( parse( material.textures[ 1 ], pTextures, "normal" ) ) { }
				if( parse( material.textures[ 2 ], pTextures, "height" ) ) { }
				if( parse( material.textures[ 3 ], pTextures, "occlusion" ) ) { }
				if( parse( material.textures[ 4 ], pTextures, "bump" ) ) { }
				if( parse( material.textures[ 5 ], pTextures, "gloss" ) ) { }
				if( parse( material.textures[ 6 ], pTextures, "hiGloss" ) ) { }
				if( parse( material.textures[ 7 ], pTextures, "roughness" ) ) { }
				if( parse( material.textures[ 8 ], pTextures, "metalness" ) ) { }

				if( parse(  material.ps.ka       , pMaterial, "ka" ) ) { }
				if( parse(  material.ps.kd       , pMaterial, "kd" ) ) { }
				if( parse(  material.ps.ks       , pMaterial, "ks" ) ) { }

				if( parse( material.ps.bumpScale , pMaterial, "bump" ) ) { }
				if( parse( material.ps.roughness , pMaterial, "roughness" ) ) { }
				if( parse( material.ps.metalness , pMaterial, "metalness" ) ) { }
				if( parse( material.ps.sAlpha    , pMaterial, "sAlpha" ) ) { }
				if( parse( material.ps.ssAlpha   , pMaterial, "ssAlpha" ) ) { }
				if( parse( material.ps.absorption, pMaterial, "absorption" ) ) { }
				if( parse( material.ps.ior       , pMaterial, "refraction" ) ) { }
				if( parse( material.ps.ssScale   , pMaterial, "ssScale" ) ) { }
				if( parse( material.ps.ssPower   , pMaterial, "refraction" ) ) { }
				if( parse( material.ps.ssBump    , pMaterial, "refraction" ) ) { }
			};


			const auto readBranch = [ pTree ]( const cJSON* pBranch, const int i, TreeProperties& tree ){

				TreeProperties::Branch branch;

				if( parse( branch.branches, pBranch, "branches" ) ) { }
				if( parse( branch.downAngle, pBranch, "downAngle" ) ) { }
				if( parse( branch.downAngleV, pBranch, "downAngleV" ) ) { }

				if( parse( branch.rotate                , pBranch, "rotate" ) ) { }
				if( parse( branch.rotateV               , pBranch, "rotateV" ) ) { }
				if( parse( branch.properties.length     , pBranch, "length" ) ) { }
				if( parse( branch.properties.lengthV    , pBranch, "lengthV" ) ) { }
				if( parse( branch.properties.taper      , pBranch, "taper" ) ) { }
				if( parse( branch.properties.segSplits  , pBranch, "segSplits" ) ) { }
				if( parse( branch.properties.splitAngle , pBranch, "splitAngle" ) ) { }
				if( parse( branch.properties.splitAngleV, pBranch, "splitAngleV" ) ) { }
				if( parse( branch.properties.curveRes   , pBranch, "curveRes" ) ) { }
				if( parse( branch.properties.curve      , pBranch, "curve" ) ) { }
				if( parse( branch.properties.curveBack  , pBranch, "curveBack" ) ) { }
				if( parse( branch.properties.curveV     , pBranch, "curveV" ) ) { }

				tree.branches.push_back( branch );
			};

			cJSON* pBranches = cJSON_GetObjectItem( pTree, "branches" );
			const int branches = cJSON_GetArraySize( pBranches );
			for( int i = 0; i < branches; ++i )
			{
				readBranch( cJSON_GetArrayItem( pBranches, i ), i, tree );
			}

			readMaterial( "bark", bark );
			readMaterial( "leaf", leaf );


			static TreeBuilder treeBuilder;

			treeBuilder.create( tree, geometry );

			pBuilder->addBasicMesh(
				"PCG_Tree_" + to_string( ++pcgTreeUniqueID ),
				{ geometry.branchVertices, geometry.branchIndecies, MeshPrimitive::TriangleList },
				position, rotation, scale,
				bark.textures, bark.ps );

			if( tree.leaves.leaves != 0 && ! geometry.leafVertices.empty( ) )
			{
				//if( tools::almostEqual( leaf.ps.sAlpha, 1.0f ) )
				if( leaf.ps.sAlpha >= 1.0f )
				{
					pBuilder->addBasicMesh(
						"PCG_Tree_Leaves_" + to_string( pcgTreeUniqueID ),
						{ geometry.leafVertices, geometry.leafIndecies, MeshPrimitive::TriangleList },
						position, rotation, scale,
						leaf.textures, leaf.ps );
				}
				else
				{
					pBuilder->addTranslucentMesh(
						"PCG_Tree_Leaves_" + to_string( pcgTreeUniqueID ),
						{ geometry.leafVertices, geometry.leafIndecies, MeshPrimitive::TriangleList },
						position, rotation, scale,
						leaf.textures, leaf.ps );
				}
				/**/
				//*/
			}

			PRINT_PARSER_DATA( "[ OK ]" << endl );
		}
	} // namespace fx
} // namespace visual