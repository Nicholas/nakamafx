#include "ObjParser.h"
// Parse a .obj file and provide the mesh and materials.
//
// Project   : NaKama-Fx
// File Name : ObjParser.cpp
// Date      : 27/06/2015
// Author    : Nicholas Welters

#include "../Maths.h"

#include <functional>
#include <filesystem>
#include <fstream>

namespace visual
{
	namespace fx
	{
		using std::filesystem::path;
		using std::ios;
		using std::to_string;
		using std::make_shared;
		using std::string;
		using std::unordered_map;
		using std::shared_ptr;
		using std::ifstream;
		using std::function;
		using std::vector;
		using glm::vec3;
		using glm::vec2;
		using glm::cross;

		enum LineElements
		{
			v = 0,
			vn,
			vt,
			f,
			mtllib,
			usemtl,
			g,

			element_count
		};

		/// <summary> ctor </summary>
		ObjMaterial::ObjMaterial( )
			: name( )
			, index( )

			, Ka( 0, 0, 0 )
			, Kd( 0, 0, 0 )
			, Ks( 0, 0, 0 )
			, Tf( 0, 0, 0 )
			, illum( HIGHTLIGHT )
			, Sharpness( 0 )
			, Ns( 1 )
			, Ni( 1 )
			, d( 1 )

			, map_Ka( "SingleTexelWhite" )
			, map_Kd( "SingleTexelWhite" )
			, map_Ks( "SingleTexelWhite" )
			, map_Ns( "SingleTexelWhite" )
			, map_d( "SingleTexelWhite" )
			, normal( "SingleTexelBlack" )
			, displacement( "SingleTexelWhite" )

			, Pr( 1 )
			, Pm( 0 )
			, Ps( 1 )
			, Pc( 0 )
			, Pcr( 1 )
			, Ke( 0 )

			, map_Pr( "SingleTexelWhite" )
			, map_Pm( "SingleTexelBlack" )
			, map_Ps( "SingleTexelWhite" )
			, map_Ke( "SingleTexelBlack" )

			, aniso( 0 )
			, anisor( 0 )
		{}

		/// <summary> ctor </summary>
		/// <param name="geometry"> The sub mesh geometry. </param>
		/// <param name="material"> The sub mesh material. </param>
		ObjData::ObjData( const Mesh<Vertex3f2f3f3f3f::Vertex>& geometry, const ObjMaterial& material )
			: geometry( geometry )
			, material( material )
		{
		}

		/// <summary> ctor </summary>
		/// <param name="geometry"> The sub mesh geometry. </param>
		/// <param name="material"> The sub mesh material. </param>
		ObjData::ObjData( Mesh<Vertex3f2f3f3f3f::Vertex>&& geometry, ObjMaterial&& material )
			: geometry( geometry )
			, material( material )
		{
		}

		/// <summary> The parsing function that will interpret the json data. </summary>
		/// <param name="pRoot"> The json object root node. </param>
		vector< ObjData > ObjParser::parseObj( const string& filePath )
		{
			path objPath( filePath );

			/////////////////
			// open file. //
			///////////////
			ifstream file( filePath );
			string line;
			LineElements element;

			//////////////////////
			// get array sizes //
			////////////////////
			size_t positionCount = 0;
			size_t textureUVCount = 0;
			size_t normalCount = 0;

			if( file.is_open( ) )
			{
				while( getline( file, line ) )
				{
					if( line.size( ) > 2 && line[ 0 ] == 'v' )
					{
						     if( line[ 1 ] == ' ' ) { ++positionCount;  }
						else if( line[ 1 ] == 't' ) { ++textureUVCount; }
						else if( line[ 1 ] == 'n' ) { ++normalCount;    }
					}
					else if( line.size( ) > 1 && line[ 0 ] == 'f' ) { }
					else if( line.size( ) > 6
							 && line[ 0 ] == 'm'
							 && line[ 1 ] == 't'
							 && line[ 2 ] == 'l'
							 && line[ 3 ] == 'l'
							 && line[ 4 ] == 'i'
							 && line[ 5 ] == 'b'
							 )
					{ }
					else if( line.size( ) > 6
							 && line[ 0 ] == 'u'
							 && line[ 1 ] == 's'
							 && line[ 2 ] == 'e'
							 && line[ 3 ] == 'm'
							 && line[ 4 ] == 't'
							 && line[ 5 ] == 'l'
							 )
					{ }
				}
			}


			struct VertexCacheElement
			{
				int index = -1;
				Vertex3f2f3f3f3f::Vertex vertex;
			};
			typedef unordered_map< string, VertexCacheElement > VertexCache;

			struct MeshCacheElement
			{
				int index = -1;
				VertexCache vertexCache;

				ObjMaterialSPtr pMaterial;
				Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer indexBuffer;
			};
			typedef shared_ptr< MeshCacheElement > MeshCacheElementSPtr;
			typedef unordered_map< string, MeshCacheElementSPtr > MeshCache;


			vector< vec3 > positions( positionCount );
			vector< vec2 > textureUVs( textureUVCount );
			vector< vec3 > normals( normalCount );

			positionCount = 0;
			textureUVCount = 0;
			normalCount = 0;
			string mtl;

			MeshCache meshCache;
			MtlCache mtlCache;

			/////////////////////////
			// File loading logic //
			///////////////////////
			vector< function< void( const string& ) > > translator( element_count );


			auto addFace = [ &meshCache, &mtlCache, &positions, &textureUVs, &normals ]
			( const string& mtl,
			  int vIndices1[ 3 ],
			  int vIndices2[ 3 ],
			  int vIndices3[ 3 ],
			  char _1[ MAX_PATH ],
			  char _2[ MAX_PATH ],
			  char _3[ MAX_PATH ]
			  )
			{
				static int c = 0;
				c++;
				bool calcNormal = false;

				MeshCacheElementSPtr pData = meshCache[ mtl ];
				if( pData == nullptr )
				{
					meshCache[ mtl ] = make_shared< MeshCacheElement >( );
					pData = meshCache[ mtl ];
					pData->pMaterial = mtlCache[ mtl ];
				}

				VertexCacheElement& vCashe1 = pData->vertexCache[ _1 + to_string( c ) ];
				if( vCashe1.index == -1 )
				{
					vCashe1.index = ( DWORD ) pData->vertexCache.size( ) - 1;

					if( vIndices1[ 0 ] > 0 ) { vCashe1.vertex.position = positions[ vIndices1[ 0 ] - 1 ]; }
					if( vIndices1[ 1 ] > 0 ) { vCashe1.vertex.textureUV = textureUVs[ vIndices1[ 1 ] - 1 ]; }
					if( vIndices1[ 2 ] > 0 ) { vCashe1.vertex.normal = normals[ vIndices1[ 2 ] - 1 ]; }
					else { calcNormal = true; }
				}

				VertexCacheElement& vCashe2 = pData->vertexCache[ _2 + to_string( c ) ];
				if( vCashe2.index == -1 )
				{
					vCashe2.index = ( DWORD ) pData->vertexCache.size( ) - 1;

					if( vIndices2[ 0 ] > 0 ) { vCashe2.vertex.position = positions[ vIndices2[ 0 ] - 1 ]; }
					if( vIndices2[ 1 ] > 0 ) { vCashe2.vertex.textureUV = textureUVs[ vIndices2[ 1 ] - 1 ]; }
					if( vIndices2[ 2 ] > 0 ) { vCashe2.vertex.normal = normals[ vIndices2[ 2 ] - 1 ]; }
					else { calcNormal = true; }
				}

				VertexCacheElement& vCashe3 = pData->vertexCache[ _3 + to_string( c ) ];
				if( vCashe3.index == -1 )
				{
					vCashe3.index = ( DWORD ) pData->vertexCache.size( ) - 1;

					if( vIndices3[ 0 ] > 0 ) { vCashe3.vertex.position = positions[ vIndices3[ 0 ] - 1 ]; }
					if( vIndices3[ 1 ] > 0 ) { vCashe3.vertex.textureUV = textureUVs[ vIndices3[ 1 ] - 1 ]; }
					if( vIndices3[ 2 ] > 0 ) { vCashe3.vertex.normal = normals[ vIndices3[ 2 ] - 1 ]; }
					else { calcNormal = true; }
				}

				if( calcNormal )
				{
					vec3 vec1 = vCashe1.vertex.position - vCashe2.vertex.position;
					vec3 vec2 = vCashe1.vertex.position - vCashe3.vertex.position;
					vec3 normal = cross( vec1, vec2 );

					vCashe3.vertex.normal = normal;
					vCashe2.vertex.normal = normal;
					vCashe1.vertex.normal = normal;
				}

				// Generate Tangents and Bitangents
				Maths::calculateTangents(
					vCashe1.vertex.tangent, vCashe1.vertex.binormal,
					vCashe1.vertex.position, vCashe2.vertex.position, vCashe3.vertex.position,
					vCashe1.vertex.textureUV, vCashe2.vertex.textureUV, vCashe3.vertex.textureUV
				);

				vCashe3.vertex.tangent = vCashe2.vertex.tangent = vCashe1.vertex.tangent;
				vCashe3.vertex.binormal = vCashe2.vertex.binormal = vCashe1.vertex.binormal;

				pData->indexBuffer.emplace_back( vCashe1.index );
				pData->indexBuffer.emplace_back( vCashe2.index );
				pData->indexBuffer.emplace_back( vCashe3.index );
			};


			translator[ v ] = [ &positions, &positionCount ]( const string& str )
			{
				vec3& position = positions[ positionCount ];
				sscanf_s( str.c_str( ), "v %f %f %f", &position.x, &position.y, &position.z ); // Normal
				//sscanf_s( str.c_str( ), "v %f %f %f", &position.y, &position.x, &position.z );   // HAWK-EYE
				++positionCount;
			};

			translator[ vt ] = [ &textureUVs, &textureUVCount ]( const string& str )
			{
				vec2& textureUV = textureUVs[ textureUVCount ];
				sscanf_s( str.c_str( ), "vt %f %f", &textureUV.x, &textureUV.y );

				textureUV.y = 1 - textureUV.y; // Invert the texture coord.

				++textureUVCount;
			};

			translator[ vn ] = [ &normals, &normalCount ]( const string& str )
			{
				vec3& normal = normals[ normalCount ];
				sscanf_s( str.c_str( ), "vn %f %f %f", &normal.x, &normal.y, &normal.z ); // Norm
				//sscanf_s( str.c_str( ), "vn %f %f %f", &normal.y, &normal.x, &normal.z ); // hWK
																						  //normal.z *= -1;
				++normalCount;
			};

			translator[ f ] = [ &meshCache, &mtlCache, &mtl, &positions, &textureUVs, &normals, &addFace ]( const string& str )
			{
				char _1[ MAX_PATH ] = "";
				char _2[ MAX_PATH ] = "";
				char _3[ MAX_PATH ] = "";
				char _4[ MAX_PATH ] = "";
				sscanf_s( str.c_str( ), "f %s %s %s %s", _1, MAX_PATH, _2, MAX_PATH, _3, MAX_PATH, _4, MAX_PATH );

				//static int c = 0;
				//c++;

				int vIndices1[ 3 ];
				int vIndices2[ 3 ];
				int vIndices3[ 3 ];
				int vIndices4[ 3 ];

				if( 12 == sscanf_s( str.c_str( ), "f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d",
								   &vIndices1[ 0 ], &vIndices1[ 1 ], &vIndices1[ 2 ],
								   &vIndices2[ 0 ], &vIndices2[ 1 ], &vIndices2[ 2 ],
								   &vIndices3[ 0 ], &vIndices3[ 1 ], &vIndices3[ 2 ],
								   &vIndices4[ 0 ], &vIndices4[ 1 ], &vIndices4[ 2 ]
				) )
				{
					addFace( mtl, vIndices3, vIndices1, vIndices4, _3, _1, _4 );
				}
				else if( 9 == sscanf_s( str.c_str( ), "f %d/%d/%d %d/%d/%d %d/%d/%d",
								   &vIndices1[ 0 ], &vIndices1[ 1 ], &vIndices1[ 2 ],
								   &vIndices2[ 0 ], &vIndices2[ 1 ], &vIndices2[ 2 ],
								   &vIndices3[ 0 ], &vIndices3[ 1 ], &vIndices3[ 2 ]
				) )
				{
				}
				else if( 6 == sscanf_s( str.c_str( ), "f %d//%d %d//%d %d//%d",
										&vIndices1[ 0 ], &vIndices1[ 2 ],
										&vIndices2[ 0 ], &vIndices2[ 2 ],
										&vIndices3[ 0 ], &vIndices3[ 2 ]
				) )
				{
					vIndices1[ 1 ] = 0;
					vIndices2[ 1 ] = 0;
					vIndices3[ 1 ] = 0;
				}
				else if( 6 == sscanf_s( str.c_str( ), "f %d/%d/ %d/%d/ %d/%d/",
										&vIndices1[ 0 ], &vIndices1[ 1 ],
										&vIndices2[ 0 ], &vIndices2[ 1 ],
										&vIndices3[ 0 ], &vIndices3[ 1 ]
				) )
				{
					vIndices1[ 2 ] = 0;
					vIndices2[ 2 ] = 0;
					vIndices3[ 2 ] = 0;
				}
				else if( 6 == sscanf_s( str.c_str( ), "f %d/%d %d/%d %d/%d",
										&vIndices1[ 0 ], &vIndices1[ 1 ],
										&vIndices2[ 0 ], &vIndices2[ 1 ],
										&vIndices3[ 0 ], &vIndices3[ 1 ]
				) )
				{
					vIndices1[ 2 ] = 0;
					vIndices2[ 2 ] = 0;
					vIndices3[ 2 ] = 0;
				}
				else if( 3 == sscanf_s( str.c_str( ), "f %d// %d// %d//",
										&vIndices1[ 0 ],
										&vIndices2[ 0 ],
										&vIndices3[ 0 ]
				) )
				{
					vIndices1[ 1 ] = 0; vIndices1[ 2 ] = 0;
					vIndices2[ 1 ] = 0; vIndices2[ 2 ] = 0;
					vIndices3[ 1 ] = 0; vIndices3[ 2 ] = 0;
				}
				else if( 3 == sscanf_s( str.c_str( ), "f %d %d %d",
										&vIndices1[ 0 ],
										&vIndices2[ 0 ],
										&vIndices3[ 0 ]
				) )
				{
					vIndices1[ 1 ] = 0; vIndices1[ 2 ] = 0;
					vIndices2[ 1 ] = 0; vIndices2[ 2 ] = 0;
					vIndices3[ 1 ] = 0; vIndices3[ 2 ] = 0;
				}
				else
				{
					vIndices1[ 0 ] = 0; vIndices1[ 1 ] = 0; vIndices1[ 2 ] = 0;
					vIndices2[ 0 ] = 0; vIndices2[ 1 ] = 0; vIndices2[ 2 ] = 0;
					vIndices3[ 0 ] = 0; vIndices3[ 1 ] = 0; vIndices3[ 2 ] = 0;
				}

				addFace( mtl, vIndices1, vIndices2, vIndices3, _1, _2, _3 );
			};

			translator[ mtllib ] = [ this, &mtlCache, &objPath ]( const string& str )
			{
				parseMtl( objPath.replace_filename( str.substr( 7 ) ).string( ), mtlCache );
			};

			translator[ usemtl ] = [ &mtl, &mtlCache ]( const string& str )
			{
				MtlCache::iterator iter;
				iter = mtlCache.find( str.substr( 7 ) );
				if( iter != mtlCache.end( ) )
				{
					mtl = iter->second->name;
				}
				//else
				//{
				//	printf( "Material NOT FOUND: %s\n", str.c_str( ) );
				//}
			};

			translator[ g ] = [ &mtl, &mtlCache ]( const string& str )
			{
				MtlCache::iterator iter;
				iter = mtlCache.find( str.substr( 2 ) );
				if( iter != mtlCache.end( ) )
				{
					mtl = iter->second->name;
				}
				//else
				//{
				//	printf( "Material NOT FOUND: %s\n", str.c_str( ) );
				//}
			};


			if( file.is_open( ) )
			{
				/////////////////
				// reset file //
				///////////////
				file.clear( );
				file.seekg( 0, ios::beg );

				///////////////
				// get data //
				/////////////
				while( getline( file, line ) )
				{
					element = element_count;

					if( line.size( ) > 2 && line[ 0 ] == 'v' )
					{
						if( line[ 1 ] == ' ' ) { element = v; }
						else if( line[ 1 ] == 't' ) { element = vt; }
						else if( line[ 1 ] == 'n' ) { element = vn; }
					}
					else if( line.size( ) > 1 && line[ 0 ] == 'f' ) { element = f; }
					else if( line.size( ) > 1 && line[ 0 ] == 'g' ) { element = g; }
					else if( line.size( ) > 6
							 && line[ 0 ] == 'm'
							 && line[ 1 ] == 't'
							 && line[ 2 ] == 'l'
							 && line[ 3 ] == 'l'
							 && line[ 4 ] == 'i'
							 && line[ 5 ] == 'b'
							 )
					{
						element = mtllib;
					}
					else if( line.size( ) > 6
							 && line[ 0 ] == 'u'
							 && line[ 1 ] == 's'
							 && line[ 2 ] == 'e'
							 && line[ 3 ] == 'm'
							 && line[ 4 ] == 't'
							 && line[ 5 ] == 'l'
							 )
					{
						element = usemtl;
					}

					if( element != element_count )
					{
						translator[ element ]( line );
					}
				}
			}

			vector< ObjData > objData;
			int i = 0;
			for( const MeshCache::value_type& cache : meshCache )
			{
				Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vertexBuffer( cache.second->vertexCache.size( ) );
				for( const VertexCache::value_type& vertex : cache.second->vertexCache )
				{
					vertexBuffer[ vertex.second.index ] = vertex.second.vertex;
				}

				if( cache.second->pMaterial == nullptr )
				{
					cache.second->pMaterial = make_shared< ObjMaterial >( );
					cache.second->pMaterial->index = mtlCache.size( );
					cache.second->pMaterial->name = "DEFAULT";
				}

				objData.emplace_back(
					Mesh< Vertex3f2f3f3f3f::Vertex >( vertexBuffer, cache.second->indexBuffer, TriangleList ),
					*cache.second->pMaterial
				);
			}

			/////////////////////////
			// return mesh object //
			///////////////////////
			return objData;
		}

		/// <summary> Read the mtl files for the obj. </summary>
		/// <param name="filePath"> Path to the mtl file. </param>
		void ObjParser::parseMtl( const string & filePath, MtlCache& mtlCache )
		{
			string mtlPath = path( filePath ).remove_filename( ).string( );

			// File input
			char strCommand[ 256 ] = { 0 };
			ifstream InFile( filePath );
			if( !InFile )
				return;

			ObjMaterialSPtr pMaterial = nullptr;

			for(;;)
			{
				InFile >> strCommand;
				if( !InFile )
					break;

				if( 0 == strcmp( strCommand, "newmtl" ) )
				{
					// Switching active materials
					char strName[ MAX_PATH ] = { 0 };
					InFile >> strName;

					pMaterial = make_shared< ObjMaterial >( );
					pMaterial->index = mtlCache.size( );
					pMaterial->name = strName;

					mtlCache[ strName ] = pMaterial;
				}

				// The rest of the commands rely on an active material
				if( pMaterial == nullptr )
					continue;

				if( 0 == strcmp( strCommand, "#" ) )
				{
					// Comment
				}
				else if( 0 == strcmp( strCommand, "Ka" ) )
				{
					float r, g, b;
					InFile >> r >> g >> b;
					pMaterial->Ka = vec3( r, g, b );
				}
				else if( 0 == strcmp( strCommand, "Kd" ) )
				{
					// Diffuse color
					float r, g, b;
					InFile >> r >> g >> b;
					pMaterial->Kd = vec3( r, g, b );
				}
				else if( 0 == strcmp( strCommand, "Ks" ) )
				{
					float r, g, b;
					InFile >> r >> g >> b;
					pMaterial->Ks = vec3( r, g, b );
				}
				else if( 0 == strcmp( strCommand, "d" ) ||
						 0 == strcmp( strCommand, "Tr"        ) ) { InFile >> pMaterial->d; }
				else if( 0 == strcmp( strCommand, "Ns"        ) ) { InFile >> pMaterial->Ns; }
				else if( 0 == strcmp( strCommand, "Ni"        ) ) { InFile >> pMaterial->Ni; }
				else if( 0 == strcmp( strCommand, "Sharpness" ) ) { InFile >> pMaterial->Sharpness; }
				else if( 0 == strcmp( strCommand, "Pr"        ) ) { InFile >> pMaterial->Pr; }
				else if( 0 == strcmp( strCommand, "Pm"        ) ) { InFile >> pMaterial->Pm; }
				else if( 0 == strcmp( strCommand, "Ps"        ) ) { InFile >> pMaterial->Ps; }
				else if( 0 == strcmp( strCommand, "Pc"        ) ) { InFile >> pMaterial->Pc; }
				else if( 0 == strcmp( strCommand, "Pcr"       ) ) { InFile >> pMaterial->Pcr; }
				else if( 0 == strcmp( strCommand, "Ke"        ) ) { InFile >> pMaterial->Ke; }
				else if( 0 == strcmp( strCommand, "aniso"     ) ) { InFile >> pMaterial->aniso; }
				else if( 0 == strcmp( strCommand, "anisor"    ) ) { InFile >> pMaterial->anisor; }
				else if( 0 == strcmp( strCommand, "illum" ) )
				{
					int illumination;
					InFile >> illumination;
					pMaterial->illum = static_cast< Illum >( illumination );
				}
				else if( 0 == strcmp( strCommand, "map_Ka" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_Ka = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_Kd" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_Kd = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_Ks" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_Ks = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_opacity" ) ||
						 0 == strcmp( strCommand, "map_d" ) ||
						 0 == strcmp( strCommand, "map_Tr" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_d = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_bump" ) ||
						 0 == strcmp( strCommand, "bump" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->normal = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_Pr" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_Pr = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_Pm" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_Pm = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_Ps" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_Ps = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "map_Ke" ) )
				{
					// Texture
					char fileNameC[ MAX_PATH ] = { 0 };
					InFile >> fileNameC;

					string fileName( fileNameC );
					if( fileName.size( ) > 0 )
					{
						pMaterial->map_Ke = buildTexturePath( mtlPath, fileName );
					}
				}
				else if( 0 == strcmp( strCommand, "bump" ) )
				{
					// Texture
					float bump;
					InFile >> bump;

					//pMaterial->bump = bump;
				}
				else
				{
					// Unimplemented or unrecognized command
				}

				InFile.ignore( 1000, L'\n' );
			}

			InFile.close( );
		}

		/// <summary> build a valid file path to a texture. </summary>
		/// <param name="objPath"> The current folder path of the obj. </param>
		/// <param name="textureFileName"> The file name to find a valid path for. </param>
		string ObjParser::buildTexturePath( const string& objPath, const string& textureFileName )
		{
			return objPath + "/" + textureFileName;
		}
} // namespace fx
} // namespace visual