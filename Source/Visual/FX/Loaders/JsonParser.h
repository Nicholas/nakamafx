#pragma once
// Intermediate interface so that we can
// add some useful functions for our
// renders json loaders.
//
// Project   : NaKama-Tools
// File Name : JsonParser.h
// Date      : 06/03/2018
// Author    : Nicholas Welters

#ifndef _FX_JSON_PARSER_H
#define _FX_JSON_PARSER_H

#include <Files/JsonParser.h>
#include <glm/glm.hpp>

struct cJSON;

namespace visual
{
	namespace fx
	{
		/// <summary> 
		/// Parse a json file containing the
		/// systems settings to initialise with.
		/// </summary>
		class JsonParser : public tools::JsonParser
		{
			public:
				/// <summary> ctor </summary>
				/// <param name="scheduler"> A functor that will schedule another functor to execute the parsing of the json nodes. </param>
				explicit JsonParser( const tools::Scheduler& scheduler );

				/// <summary> copy ctor </summary>
				/// <param name="copy"> The settings object to copy. </param>
				JsonParser( const JsonParser& copy ) = default;

				/// <summary> move ctor </summary>
				/// <param name="move"> The settings object to move. </param>
				JsonParser( JsonParser&& move ) = default;

				/// <summary> dtor </summary>
				virtual ~JsonParser( ) = default;
			
				/// <summary> Copy assignment operator </summary>
				JsonParser& operator=( const JsonParser& copy ) = default;
				
				/// <summary> Move assignment operator </summary>
				JsonParser& operator=( JsonParser&& copy ) = default;
				
				/// <summary> Parse a json node to an int. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( int& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 2 integers. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::ivec2& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 3 integers. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::ivec3& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 4 integers. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::ivec4& result, const cJSON* pNode, const std::string& field );
				
				
				/// <summary> Parse a json node to an unsigned integer. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( unsigned int& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 2 unsigned integers. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::uvec2& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 3 unsigned integers. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::uvec3& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 4 unsigned integers. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::uvec4& result, const cJSON* pNode, const std::string& field );
				
				
				/// <summary> Parse a json node to a float. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( float& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 2 floats. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::vec2& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 3 floats. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::vec3& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to 4 floats. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( glm::vec4& result, const cJSON* pNode, const std::string& field );
				
				
				/// <summary> Parse a json node to a string. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( std::string& result, const cJSON* pNode, const std::string& field );
				
				/// <summary> Parse a json node to a boolean. </summary>
				/// <param name="result"> A reference to the memory where we are going to store the value. </param>
				/// <param name="pNode"> The json node to get the value from. </param>
				/// <param name="field"> The field name in the json node that contains the value. </param>
				/// <returns> True if there was a value and it was parsed. False otherwise </returns>
				static bool parse( bool& result, const cJSON* pNode, const std::string& field );
		};
	} // namespace fx
} // namespace visual

# endif // _FX_JSON_PARSER_H