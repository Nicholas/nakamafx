#include "JsonParser.h"
// Intermediate interface so that we can
// add some useful functions for our
// renders json loaders.
//
// Project   : NaKama-Tools
// File Name : JsonParser.cpp
// Date      : 06/03/2018
// Author    : Nicholas Welters

#include <cjson.h>
#include <glm/glm.hpp>

#include <string>

namespace visual
{
	namespace fx
	{
		using std::string;
		using glm::ivec2;
		using glm::ivec3;
		using glm::ivec4;
		using glm::uvec2;
		using glm::uvec3;
		using glm::uvec4;
		using glm::vec2;
		using glm::vec3;
		using glm::vec4;

		/// <summary> ctor </summary>
		/// <param name="scheduler"> A functor that will schedule another functor to execute the parsing of the json nodes. </param>
		JsonParser::JsonParser( const tools::Scheduler& scheduler )
				: tools::JsonParser( scheduler )
		{ }
		
		/// <summary> Parse a json node to an int. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( int& result, const cJSON* pNode, const string& field )
		{
			cJSON* number = cJSON_GetObjectItem( pNode, field.c_str( ) );
			
			if( cJSON_IsNumber( number ) )
			{
				result = number->valueint;
				return true;
			}
			return false;
		}
		
		/// <summary> Parse a json node to 2 integers. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( ivec2& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 2 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) )
				{
					result.x = x->valueint;
					result.y = y->valueint;

					return true;
				}
			}
			return false;
		}
		
		/// <summary> Parse a json node to 3 integers. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( ivec3& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 3 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );
				cJSON* z = cJSON_GetArrayItem( vector, 2 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) && cJSON_IsNumber( z ) )
				{
					result.x = x->valueint;
					result.y = y->valueint;
					result.z = z->valueint;

					return true;
				}
			}
			return false;
		}
		
		/// <summary> Parse a json node to 4 integers. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( ivec4& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 4 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );
				cJSON* z = cJSON_GetArrayItem( vector, 2 );
				cJSON* w = cJSON_GetArrayItem( vector, 3 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) && cJSON_IsNumber( z ) && cJSON_IsNumber( w ) )
				{
					result.x = x->valueint;
					result.y = y->valueint;
					result.z = z->valueint;
					result.w = w->valueint;

					return true;
				}
			}
			return false;
		}
		
				
		/// <summary> Parse a json node to an unsigned integer. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( unsigned int& result, const cJSON* pNode, const string& field )
		{
			cJSON* number = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_IsNumber( number ) )
			{
				result = static_cast< unsigned int >( number->valueint );
				return true;
			}
			return false;
		}
		
		/// <summary> Parse a json node to 2 unsigned integers. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( uvec2& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 2 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) )
				{
					result.x = x->valueint;
					result.y = y->valueint;

					return true;
				}
			}
			return false;
		}
		
		/// <summary> Parse a json node to 3 unsigned integers. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( uvec3& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 3 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );
				cJSON* z = cJSON_GetArrayItem( vector, 2 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) && cJSON_IsNumber( z ) )
				{
					result.x = x->valueint;
					result.y = y->valueint;
					result.z = z->valueint;

					return true;
				}
			}
			return false;
		}
		
		/// <summary> Parse a json node to 4 unsigned integers. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( uvec4& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 4 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );
				cJSON* z = cJSON_GetArrayItem( vector, 2 );
				cJSON* w = cJSON_GetArrayItem( vector, 3 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) && cJSON_IsNumber( z ) && cJSON_IsNumber( w ) )
				{
					result.x = x->valueint;
					result.y = y->valueint;
					result.z = z->valueint;
					result.w = w->valueint;

					return true;
				}
			}
			return false;
		}
		
				
		/// <summary> Parse a json node to a float. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( float& result, const cJSON* pNode, const string& field )
		{
			cJSON* number = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_IsNumber( number ) )
			{
				result = static_cast< float >( number->valuedouble );
				return true;
			}

			return false;
		}
		
		/// <summary> Parse a json node to 2 floats. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( vec2& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 2 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) )
				{
					result.x = static_cast< float >( x->valuedouble );
					result.y = static_cast< float >( y->valuedouble );

					return true;
				}
			}
			return false;
		}

		/// <summary> Parse a json node to 3 floats. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( vec3& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 3 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );
				cJSON* z = cJSON_GetArrayItem( vector, 2 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) && cJSON_IsNumber( z ) )
				{
					result.x = static_cast< float >( x->valuedouble );
					result.y = static_cast< float >( y->valuedouble );
					result.z = static_cast< float >( z->valuedouble );

					return true;
				}
			}
			return false;
		}

		/// <summary> Parse a json node to 4 floats. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( vec4& result, const cJSON* pNode, const string& field )
		{
			cJSON* vector = cJSON_GetObjectItem( pNode, field.c_str( ) );

			if( cJSON_GetArraySize( vector ) == 4 )
			{
				cJSON* x = cJSON_GetArrayItem( vector, 0 );
				cJSON* y = cJSON_GetArrayItem( vector, 1 );
				cJSON* z = cJSON_GetArrayItem( vector, 2 );
				cJSON* w = cJSON_GetArrayItem( vector, 3 );

				if( cJSON_IsNumber( x ) && cJSON_IsNumber( y ) && cJSON_IsNumber( z ) && cJSON_IsNumber( w ) )
				{
					result.x = static_cast< float >( x->valuedouble );
					result.y = static_cast< float >( y->valuedouble );
					result.z = static_cast< float >( z->valuedouble );
					result.w = static_cast< float >( w->valuedouble );

					return true;
				}
			}
			return false;
		}

		
		/// <summary> Parse a json node to a string. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( string& result, const cJSON* pNode, const string& field )
		{
			cJSON* value = cJSON_GetObjectItem( pNode, field.c_str( ) );
			if( cJSON_IsString( value ) )
			{
				result = value->valuestring;
				return true;
			}

			return false;
		}

		/// <summary> Parse a json node to a boolean. </summary>
		/// <param name="result"> A reference to the memory where we are going to store the value. </param>
		/// <param name="pNode"> The json node to get the value from. </param>
		/// <param name="field"> The field name in the json node that contains the value. </param>
		/// <returns> True if there was a value and it was parsed. False otherwise </returns>
		bool JsonParser::parse( bool& result, const cJSON* pNode, const string& field )
		{
			cJSON* value = cJSON_GetObjectItem( pNode, field.c_str( ) );
			if( cJSON_IsBool( value ) )
			{
				result = cJSON_IsFalse( value ) == 0;
				return true;
			}

			return false;
		}
	} // namespace fx
} // namespace visual