#pragma once
// Interface for building a scene and that will
// then insert the scene into the system.
//
// Project   : NaKama-Tools
// File Name : SceneBuilder.h
// Date      : 07/03/2018
// Author    : Nicholas Welters

#ifndef _SCENE_BUILDER_H
#define _SCENE_BUILDER_H

#include "../Vertex.h"
#include "../Mesh.h"

#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	struct GSParticleBuffer;
	struct PSParticleBuffer;
	struct PSObjectBuffer;
	struct LightEx;
	struct PSFontBuffer;

	/// <summary>
	/// Interface for building a scene and that will
	/// then insert the scene into the system.
	/// </summary>
	class SceneBuilder
	{
		public:
			/// <summary>
			/// A set to load a texture using a file name andto flag the texture to force SRGB format
			/// </summary>
			struct LoadTexture
			{
				std::string fileName;
				bool forceSRGB;
			};

			typedef std::vector< LoadTexture > Textures;

			/// <summary> dtor </summary>
			virtual ~SceneBuilder( ) = 0;

			/// <summary> add a sphere to the new scene. </summary>
			/// <param name="position"> the position of the sphere. </param>
			/// <param name="radius"> The radius of the sphere. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addSphere(
				const glm::vec3& position,
				const float& radius,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a sphere to the new scene. </summary>
			/// <param name="position"> the position of the sphere. </param>
			/// <param name="radius"> The radius of the sphere. </param>
			/// <param name="textures"> The textures for the sphere. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			/// <param name="forceSRGB"> force the albido texture to be gamma corrected. </param>
			virtual void addPbrSphere(
				const glm::vec3& position,
				const float& radius,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties,
				const bool& forceSRGB
			) = 0;

			/// <summary> add a planet to the new scene. </summary>
			/// <param name="position"> the position of the sphere. </param>
			/// <param name="radius"> The radius of the sphere. </param>
			/// <param name="textures"> The textures to load and use for the planets surface. </param>
			virtual void addPlanet(
				const glm::vec3& position,
				const float& radius,
				const std::vector< std::string >& textures
			) = 0;

			/// <summary> add a moon to the new scene. </summary>
			/// <param name="position"> the position of the sphere. </param>
			/// <param name="radius"> The radius of the sphere. </param>
			/// <param name="textures"> The textures to load and use for the planets surface. </param>
			virtual void addMoon(
				const glm::vec3& position,
				const float& radius,
				const std::vector< std::string >& textures
			) = 0;

			/// <summary> add a height map to the new scene. </summary>
			/// <param name="fileName"> The file name of the raw image. </param>
			/// <param name="position"> The position of the height map. </param>
			/// <param name="size"> The size of the raw image. </param>
			/// <param name="textures"> The textures for the 3 layers. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addHeightMap(
				const std::string& fileName,
				const glm::vec3& position,
				const glm::vec3& scale,
				const glm::ivec2& size,
				const Textures& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a randomly generated height map to the new scene. </summary>
			/// <param name="size"> The size of the raw image. </param>
			/// <param name="textures"> The textures for the 3 layers. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addPCGHeightMap(
				const glm::vec3& scale,
				const glm::ivec2& size,
				const Textures& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a geometry shader cube to the new scene. </summary>
			/// <param name="position"> the position of the sphere. </param>
			/// <param name="rotation"> The rotation of the cube. </param>
			/// <param name="scale"> The size of the cube. </param>
			/// <param name="textures"> The textures to load and use for the cube surface. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addGsCube(
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a cube to the new scene. </summary>
			/// <param name="position"> the position of the sphere. </param>
			/// <param name="rotation"> The rotation of the cube. </param>
			/// <param name="scale"> The size of the cube. </param>
			/// <param name="textures"> The textures to load and use for the cube surface. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addCube(
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a complex custom mesh to the new scene. </summary>
			/// <param name="id"> The mesh id. </param>
			/// <param name="mesh"> the position of the sphere. </param>
			/// <param name="position"> The mesh position. </param>
			/// <param name="rotation"> The mesh rotation. </param>
			/// <param name="scale"> The mesh scale. </param>
			/// <param name="textures"> The textures to load and use for the cube surface. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addMesh(
				const std::string& id,
				const fx::Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a complex custom mesh to the new scene that uses basic textures. </summary>
			/// <param name="id"> The mesh id. </param>
			/// <param name="mesh"> the position of the sphere. </param>
			/// <param name="position"> The mesh position. </param>
			/// <param name="rotation"> The mesh rotation. </param>
			/// <param name="scale"> The mesh scale. </param>
			/// <param name="textures"> The textures to load and use for the cube surface. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addBasicMesh(
				const std::string& id,
				const fx::Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a complex custom mesh to the new scene that uses basic textures. </summary>
			/// <param name="id"> The mesh id. </param>
			/// <param name="mesh"> the position of the sphere. </param>
			/// <param name="position"> The mesh position. </param>
			/// <param name="rotation"> The mesh rotation. </param>
			/// <param name="scale"> The mesh scale. </param>
			/// <param name="textures"> The textures to load and use for the cube surface. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addClippedMesh(
				const std::string& id,
				const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a complex custom translucent mesh to the new scene. </summary>
			/// <param name="id"> The mesh id. </param>
			/// <param name="mesh"> the position of the sphere. </param>
			/// <param name="position"> The mesh position. </param>
			/// <param name="rotation"> The mesh rotation. </param>
			/// <param name="scale"> The mesh scale. </param>
			/// <param name="textures"> The textures to load and use for the cube surface. </param>
			/// <param name="psProperties"> The objects material properties. </param>
			virtual void addTranslucentMesh(
				const std::string& id,
				const Mesh< Vertex3f2f3f3f3f::Vertex >& mesh,
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a water plane to the new scene. </summary>
			/// <param name="position"> the position of the water plane. </param>
			/// <param name="rotation"> The rotation of the water plane. </param>
			/// <param name="scale"> The size of the water plane. </param>
			/// <param name="textures"> The textures to load and use for the water plane surface. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addWaterPlane(
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties
			) = 0;

			/// <summary> add a water plane that follows the camera to the new scene. </summary>
			/// <param name="position"> the position of the water plane. </param>
			/// <param name="rotation"> The rotation of the water plane. </param>
			/// <param name="scale"> The size of the water plane. </param>
			/// <param name="textures"> The textures to load and use for the water plane surface. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			/// <param name="follow"> Set the plane to follow the camera. </param>
			virtual void addPCGWaterPlane(
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSObjectBuffer& psProperties,
				bool follow
			) = 0;

			/// <summary> Set the sky box for the scene to use a single polar mapped texture for the sky. </summary>
			/// <param name="textures"> The sky texture. </param>
			/// <param name="direction"> The sun direction in the sky. </param>
			virtual void addPolarSkybox(
				const std::vector< std::string >& textures,
				const glm::vec3& direction
			) = 0;

			/// <summary> Set the sky box for the scene to use a cube mapped texture for the sky. </summary>
			/// <param name="textures"> The sky texture. </param>
			/// <param name="direction"> The sun direction in the sky. </param>
			virtual void addCubeSkybox(
				const std::vector< std::string >& textures,
				const glm::vec3& direction
			) = 0;

			/// <summary> Set the sky box for the scene to use a cube mapped texture for the sky. </summary>
			/// <param name="textures"> The stars texture. </param>
			/// <param name="direction"> The starting sun direction. </param>
			virtual void addBasicVolumeSkybox(
				const std::vector< std::string >& textures,
				const glm::vec3& direction
			) = 0;

			/// <summary> Add a particle system to the scene. </summary>
			/// <param name="initialState"> The initial state of the particle system. </param>
			/// <param name="textures"> The particles texture. </param>
			/// <param name="gsProperties"> The geometry shader constants. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			/// <param name="bufferSize"> The size of the particle buffer. </param>
			/// <param name="blend"> The blend used for the particle system. </param>
			virtual void addParticleSystem(
				const Mesh< VertexParticle::Vertex >::VertexBuffer& initialState,
				const std::vector< std::string >& textures,
				const GSParticleBuffer& gsProperties,
				const PSParticleBuffer& psProperties,
				unsigned int bufferSize,
				const std::string& blend
			) = 0;

			/// <summary> add a 3D text the new scene. </summary>
			/// <param name="font"> the font type to use for the string. </param>
			/// <param name="text"> the string contents. </param>
			/// <param name="position"> the position of the text. </param>
			/// <param name="rotation"> The rotation of the text. </param>
			/// <param name="scale"> The size of the text. </param>
			/// <param name="textures"> The textures to load and use for the text. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addText3D(
				const std::string& font,
				const std::string& text,
				const glm::vec3& position,
				const glm::vec3& rotation,
				const glm::vec3& scale,
				const std::vector< std::string >& textures,
				const PSFontBuffer& psProperties
			) = 0;

			/// <summary> add a 2D text the new scene. </summary>
			/// <param name="font"> the font type to use for the string. </param>
			/// <param name="text"> the string contents. </param>
			/// <param name="position"> the position of the text. </param>
			/// <param name="rotation"> The rotation of the text. </param>
			/// <param name="scale"> The size of the text. </param>
			/// <param name="textures"> The textures to load and use for the text. </param>
			/// <param name="psProperties"> The pixel shader constants. </param>
			virtual void addText2D(
				const std::string& font,
				const std::string& text,
				const glm::vec2& position,
				const glm::vec2& rotation,
				const glm::vec2& scale,
				const std::vector< std::string >& textures,
				const PSFontBuffer& psProperties
			) = 0;

			/// <summary> Set the worlds fog interactions. </summary>
			/// <param name="density"> The fog density. </param>
			/// <param name="heightFalloff"> The scenes fog height fall off rate. </param>
			/// <param name="rayDepth"> The maximum depth to ray march to build a volumetric fog. </param>
			/// <param name="gScatter"> The fog reflectivity direction -1 to 1 with 0 being isotropic. </param>
			/// <param name="waterHeight"> The scenes water height so that we can change the fog. </param>
			virtual void addFog(
				float density,
				float heightFalloff,
				float rayDepth,
				float gScatter,
				float waterHeight
			) = 0;

			/// <summary>
			/// Add a light to the scene.
			///
			/// Type 0: A point light
			/// Type 1: A spot light
			/// Type 2: A direction light
			///
			/// The light structure is reused between lights... for now
			/// </summary>
			/// <param name="light"> The light to add to the scene. </param>
			virtual void addLight( const LightEx& light ) = 0;

			/// <summary>
			/// Finalise the new scene object and
			/// had it off to the scene renderer.
			/// </summary>
			virtual void bake( ) = 0;
	};

	inline SceneBuilder::~SceneBuilder( ) = default;
} // namespace fx
} // namespace visual

# endif // _SCENE_BUILDER_H