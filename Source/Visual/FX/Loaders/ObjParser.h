#pragma once
// Parse a .obj file and provide the mesh and materials.
//
// Project   : NaKama-Fx
// File Name : ObjParser.h
// Date      : 27/06/2015
// Author    : Nicholas Welters

#ifndef _OBJ_PARSER_H
#define _OBJ_PARSER_H

#include "../Mesh.h"
#include "../Vertex.h"

#include <Macros.h>

#include <unordered_map>

namespace visual
{
	namespace fx
	{
		enum Illum 
		{
			COLOUR												 = 0,
			COLOUR_AMBIENT										 = 1,
			HIGHTLIGHT											 = 2,
			REFLECTION_RAY_TRACE								 = 3,
			TRANSPARENCY_GLASS_REFLECTION_RAY_TRACE				 = 4,
			REFLECTION_Fresnel_RAY_TRACE						 = 5,
			TRANSPARENCY_REFRACTION_REFLECTION_RAY_TRACE		 = 6,
			TRANSPARENCY_REFRACTION_REFLECTION_Fresnel_RAY_TRACE = 7,
			REFLECTION											 = 8,
			TRANSPARENCY_GLASS									 = 9,
			CASTS_SHADOWS_ONTO_INVISIBLE_SURFACES				 = 10
		};

		/// <summary> 
		/// Object material data.
		/// </summary>
		struct ObjMaterial
		{
			std::string name;
			size_t index;

			glm::vec3 Ka; // Ambiant Colour
			glm::vec3 Kd; // Diffuse Colour
			glm::vec3 Ks; // Specular Colour
			glm::vec3 Tf; // Transmission Filter
			Illum illum;
			float Sharpness; // sharpness of the reflections
			float Ns; // The specular exponent
			float Ni; // The optical density
			float d; // Dissolve: [ 0.0 Transparent, 1.0 Opaque ]
					 // d -halo factor
					 // dissolve = 1.0 - (N*v)(1.0-factor)
			
			std::string map_Ka;
			std::string map_Kd;
			std::string map_Ks;
			std::string map_Ns;
			std::string map_d;
			std::string normal;
			std::string displacement;


			// PBR
			float Pr;  // Roughness
			float Pm;  // metallic
			float Ps;  // sheen
			float Pc;  // clear coat thickness
			float Pcr; // clear coat roughness
			float Ke;  // emissive

			std::string map_Pr; // Roughness
			std::string map_Pm; // metallic
			std::string map_Ps; // sheen
			std::string map_Ke; // emissive

			float aniso;  // anisotropy
			float anisor; // anisotropy rotation


			
			/// <summary> ctor </summary>
			ObjMaterial( );

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			ObjMaterial( const ObjMaterial& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			ObjMaterial( ObjMaterial&& temporary ) = default;


			/// <summary> dtor. </summary>
			~ObjMaterial( ) = default;

			
			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			ObjMaterial& operator=( const ObjMaterial& copy ) = default;
			
			/// <summary> Move assignment operator </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			ObjMaterial& operator=( ObjMaterial&& temporary ) = default;
		};

		/// <summary> 
		/// Object model data.
		/// </summary>
		struct ObjData
		{
			/// <summary> The sub mesh geometry. </summary>
			Mesh< Vertex3f2f3f3f3f::Vertex > geometry;

			/// <summary> The sub mesh material. </summary>
			ObjMaterial material;

			/// <summary> ctor </summary>
			/// <param name="geometry"> The sub mesh geometry. </param>
			/// <param name="material"> The sub mesh material. </param>
			ObjData( const Mesh< Vertex3f2f3f3f3f::Vertex >& geometry, const ObjMaterial& material );

			/// <summary> ctor </summary>
			/// <param name="geometry"> The sub mesh geometry. </param>
			/// <param name="material"> The sub mesh material. </param>
			ObjData( Mesh< Vertex3f2f3f3f3f::Vertex >&& geometry, ObjMaterial&& material );

			/// <summary> ctor </summary>
			ObjData( ) = default;

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			ObjData( const ObjData& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			ObjData( ObjData&& temporary ) = default;


			/// <summary> dtor. </summary>
			~ObjData( ) = default;

			
			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			ObjData& operator=( const ObjData& copy ) = default;
			
			/// <summary> Move assignment operator </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			ObjData& operator=( ObjData&& temporary ) = default;
		};

		FORWARD_DECLARE_STRUCT( ObjMaterial );
		typedef std::unordered_map< std::string, ObjMaterialSPtr > MtlCache;

		/// <summary> 
		/// Parse a .obj file and provide the mesh and materials.
		/// </summary>
		class ObjParser
		{
			public:
				/// <summary> 
				/// Read the .obj file and create a set of models to load on the GPU.
				/// </summary>
				/// <param name="filePath"> Path to the obj file. </param>
				::std::vector< ObjData > parseObj( const ::std::string& filePath );

			private:
				/// <summary> Read the mtl files for the obj. </summary>
				/// <param name="filePath"> Path to the mtl file. </param>
				/// <param name="mtlCache"> The materials list. </param>
				virtual void parseMtl( const ::std::string& filePath, MtlCache& mtlCache );

				/// <summary> build a valid file path to a texture. </summary>
				/// <param name="objPath"> The current folder path of the obj. </param>
				/// <param name="textureFileName"> The file name to find a valid path for. </param>
				::std::string buildTexturePath( const ::std::string& objPath, const ::std::string& textureFileName );

			private:
		};
	} // namespace fx
} // namespace visual

# endif // _OBJ_PARSER_H