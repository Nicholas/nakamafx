#include "SettingsParser.h"
// Parse a json file containing the
// systems settings to initialise with.
//
// Project   : NaKama-Tools
// File Name : SettingsParser.h
// Date      : 06/03/2018
// Author    : Nicholas Welters

#include <Visual/FX/Settings.h>
#include <glm/glm.hpp>
#include <cjson.h>

namespace visual
{
	namespace fx
	{
		using std::make_shared;
		using std::string;
		using glm::ivec2;
		using glm::uvec2;

		/// <summary> ctor </summary>
		SettingsParser::SettingsParser( )
			: JsonParser( [ ]( const ::std::function< void( ) >& fun ) { fun( ); } )
			, pSettings( make_shared< Settings >( "visualFX" ) )
		{ }

		/// <summary> The parsing function that will interpret the json data. </summary>
		/// <param name="pRoot"> The json object root node. </param>
		void SettingsParser::parseJSON(const cJSONSPtr& pRoot )
		{
			std::cout << "Processing settings json file\n";

			cJSON* settings = cJSON_GetObjectItem( pRoot.get( ), "settings" );
			cJSON* threads  = cJSON_GetObjectItem( settings    , "threads"  );


			string title;
			if( !parse( title, settings, "title" ) )
			{
				title = "visualFX";
			}

			pSettings = make_shared< Settings >( title );
			
			string filePath;

			uvec2 windowSize;
			string windowFormat;
			bool fullscreen;
			int frameBuffers;
			string vsync;

			uvec2 renderingSize;
			string renderingFormat;
			int msaa;

			uvec2 shadowCascade1Size;
			uvec2 shadowCascade2Size;

			int workerThreadCount = 0;
			ivec2 sync;
			ivec2 async;

			if( parse( filePath, settings, "defaultScene" ) )
			{
				pSettings->getDefaultScene( filePath );
			}


			if( parse( windowSize, settings, "windowSize" ) )
			{
				pSettings->setBackBufferSize( windowSize );
			}

			if( parse( windowFormat, settings, "windowColourFormat" ) )
			{
				pSettings->setBackBufferFormat( parseColourFormat( windowFormat ) );
			}

			if( parse( frameBuffers, settings, "frameBuffers" ) )
			{
				pSettings->setFrameBuffers( frameBuffers );
			}

			if( parse( fullscreen, settings, "fullscreen" ) )
			{
				pSettings->setFullscreen( fullscreen );
			}

			if( parse( vsync, settings, "vsync" ) )
			{
				pSettings->setVSyncState( parseVSyncState( vsync ) );
			}


			if( parse( renderingSize, settings, "renderingSize" ) )
			{
				pSettings->setRenderTargetSize( renderingSize );
			}

			if( parse( renderingFormat, settings, "renderingColourFormat" ) )
			{
				pSettings->setRenderTargetFormat( parseColourFormat( renderingFormat ) );
			}

			if( parse( msaa, settings, "msaa" ) )
			{
				pSettings->setMsaaLevel( parseMsaaLevel( msaa ) );
			}


			if( parse( shadowCascade1Size, settings, "shadowCascade1Size" ) )
			{
				pSettings->setShadowCascade1Size( shadowCascade1Size );
			}

			if( parse( shadowCascade2Size, settings, "shadowCascade2Size" ) )
			{
				pSettings->setShadowCascade2Size( shadowCascade2Size );
			}


			if( parse( workerThreadCount, threads, "workers" ) )
			{
				pSettings->setNumberOfWorkerThreads( workerThreadCount );
			}

			if( parse( sync, threads, "sync" ) )
			{
				pSettings->setMaximumSyncJobsToExecute( sync.y );
				pSettings->setMinimumSyncJobsToExecute( sync.x );
			}

			if( parse( async, threads, "async" ) )
			{
				pSettings->setMaximumAsyncJobsToExecute( async.y );
				pSettings->setMinimumAsyncJobsToExecute( async.x );
			}
		}

		/// <summary> 
		/// Get the settings loaded from file.
		/// This will return the default settings object if we have not parsed a settings file.
		/// </summary>
		SettingsSPtr SettingsParser::getSettings( ) const
		{
			return pSettings;
		}
	} // namespace fx
} // namespace visual