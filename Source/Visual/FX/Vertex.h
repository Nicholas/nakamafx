#pragma once
// Vertex types with there element counts
//
// Project   : NaKama-Fx
// File Name : Vertex.h
// Date      : 03/01/2018
// Author    : Nicholas Welters


#ifndef _VERTEX_H
#define _VERTEX_H

#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	class Vertex3f4f
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec3 position = glm::vec3( 0, 0, 0 );
					glm::vec4 colour   = glm::vec4( 0, 0, 0, 0 );
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 2;
	};

	class Vertex3f2f
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec3 position  = glm::vec3( 0, 0, 0 );
					glm::vec2 textureUV = glm::vec2( 0, 0 );
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 2;
	};

	class Vertex3f2f3f
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec3 position  = glm::vec3( 0, 0, 0 );
					glm::vec2 textureUV = glm::vec2( 0, 0 );
					glm::vec3 normal    = glm::vec3( 0, 0, 0 );
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 3;
	};

	class Vertex3f2f4f
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec3 position  = glm::vec3( 0, 0, 0 );
					glm::vec2 textureUV = glm::vec2( 0, 0 );
					glm::vec4 colour    = glm::vec4( 0, 0, 0, 0 );
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 3;
	};

	class Vertex3f4f2f3f
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec3 position  = glm::vec4( 0, 0, 0, 0 );
					glm::vec4 colour    = glm::vec4( 0, 0, 0, 0 );
					glm::vec2 textureUV = glm::vec2( 0, 0 );
					glm::vec3 normal    = glm::vec3( 0, 0, 0 );
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 4;
	};

	class Vertex3f2f3f3f3f
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec3 position  = glm::vec3( 0, 0, 0 );
					glm::vec2 textureUV = glm::vec2( 0, 0 );
					glm::vec3 normal    = glm::vec3( 0, 0, 0 );
					glm::vec3 tangent   = glm::vec3( 0, 0, 0 );
					glm::vec3 binormal  = glm::vec3( 0, 0, 0 );
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 5;
	};

	class Vertex3f2f2f1f1f1u
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec3 initialPos = glm::vec3( 0, 0, 0 );
					glm::vec3 initialVel = glm::vec3( 0, 0, 0 );
					glm::vec2 size       = glm::vec2( 0, 0 );
					float age            = 0;
					float id             = 0;
					unsigned int type    = 0;


					/// <summary> ctor </summary>
					/// <param name="initialPos"> The particles initial position. </param>
					/// <param name="initialVel"> The particles initial velocity. </param>
					/// <param name="size"> The particles size. </param>
					/// <param name="age"> The particles age. </param>
					/// <param name="id"> The particles id. </param>
					/// <param name="type"> The particles type. </param>
					Vertex(
						const glm::vec3& initialPos,
						const glm::vec3& initialVel,
						const glm::vec2& size,
						const float& age,
						const float& id,
						const unsigned int& type
					)
						: initialPos( initialPos )
						, initialVel( initialVel )
						, size( size )
						, age( age )
						, id( id )
						, type( type )
					{}

					/// <summary> ctor </summary>
					/// <param name="initialPos"> The particles initial position. </param>
					/// <param name="initialVel"> The particles initial velocity. </param>
					/// <param name="size"> The particles size. </param>
					/// <param name="age"> The particles age. </param>
					/// <param name="id"> The particles id. </param>
					/// <param name="type"> The particles type. </param>
					Vertex(
						glm::vec3&& initialPos,
						glm::vec3&& initialVel,
						glm::vec2&& size,
						float&& age,
						float&& id,
						unsigned int&& type
					)
						: initialPos( initialPos )
						, initialVel( initialVel )
						, size( size )
						, age( age )
						, id( id )
						, type( type )
					{}

					/// <summary> ctor </summary>
					Vertex( ) = default;

					/// <summary> copy ctor </summary>
					/// <param name="copy"> The object to copy. </param>
					Vertex( const Vertex& copy ) = default;

					/// <summary> move ctor </summary>
					/// <param name="move"> The temporary to move. </param>
					Vertex( Vertex&& move ) = default;


					/// <summary> dtor </summary>
					~Vertex( ) = default;


					/// <summary> Copy assignment operator </summary>
					/// <param name="copy"> The object to copy. </param>
					Vertex& operator=( const Vertex& copy ) = default;

					/// <summary> Move assignment operator </summary>
					/// <param name="move"> The temporary to move. </param>
					Vertex& operator=( Vertex&& move ) = default;
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 6;
	};

	typedef Vertex3f2f2f1f1f1u VertexParticle;


	class Vertex4f4f4f1u
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::vec4 position  = glm::vec4( 0, 0, 0, 0 );
					glm::vec4 textureUV = glm::vec4( 0, 0, 0, 0 );
					glm::vec4 colour    = glm::vec4( 0, 0, 0, 0 );
					unsigned int charID = 0;


					/// <summary> ctor </summary>
					/// <param name="position"> The position of the characture. </param>
					/// <param name="textureUV"> The charactures texture uv properties. </param>
					/// <param name="colour"> The charactures colour. </param>
					/// <param name="charID"> The characture id so that we can get its properties from data arrays. </param>
					Vertex(
						const glm::vec4& position,
						const glm::vec4& textureUV,
						const glm::vec4& colour,
						const unsigned int charID
					)
						: position( position )
						, textureUV( textureUV )
						, colour( colour )
						, charID( charID )
					{}

					/// <summary> ctor </summary>
					/// <param name="position"> The position of the characture. </param>
					/// <param name="textureUV"> The charactures texture uv properties. </param>
					/// <param name="colour"> The charactures colour. </param>
					/// <param name="charID"> The characture id so that we can get its properties from data arrays. </param>
					//Vertex(
					//	const glm::vec4&& position,
					//	const glm::vec4&& textureUV,
					//	const glm::vec4&& colour,
					//	const unsigned int&& charID
					//)
					//	: position( position )
					//	, textureUV( textureUV )
					//	, colour( colour )
					//	, charID( charID )
					//{}

					/// <summary> ctor </summary>
					Vertex( ) = default;

					/// <summary> copy ctor </summary>
					/// <param name="copy"> The object to copy. </param>
					Vertex( const Vertex& copy ) = default;

					/// <summary> move ctor </summary>
					/// <param name="move"> The temporary to move. </param>
					Vertex( Vertex&& move ) = default;


					/// <summary> dtor </summary>
					~Vertex( ) = default;


					/// <summary> Copy assignment operator </summary>
					/// <param name="copy"> The object to copy. </param>
					Vertex& operator=( const Vertex& copy ) = default;

					/// <summary> Move assignment operator </summary>
					/// <param name="move"> The temporary to move. </param>
					Vertex& operator=( Vertex&& move ) = default;
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 4;
	};

	typedef Vertex4f4f4f1u VertexChar;


	class Vertex4x4f
	{
		public:
			/// <summary> The vertex class that we will make an array of and send to the GFX card. </summary>
			class Vertex
			{
				public:
					glm::mat4x4 world  = glm::mat4x4( 1 );

					/// <summary> ctor </summary>
					/// <param name="world"> The instance world matrix. </param>
					Vertex( const glm::mat4x4& world)
						: world( world )
					{}

					/// <summary> ctor </summary>
					/// <param name="world"> The instance world matrix. </param>
					Vertex( const glm::mat4x4&& world)
						: world( world )
					{}

					/// <summary> ctor </summary>
					Vertex( ) = default;

					/// <summary> copy ctor </summary>
					/// <param name="copy"> The object to copy. </param>
					Vertex( const Vertex& copy ) = default;

					/// <summary> move ctor </summary>
					/// <param name="move"> The temporary to move. </param>
					Vertex( Vertex&& move ) = default;


					/// <summary> dtor </summary>
					~Vertex( ) = default;


					/// <summary> Copy assignment operator </summary>
					/// <param name="copy"> The object to copy. </param>
					Vertex& operator=( const Vertex& copy ) = default;

					/// <summary> Move assignment operator </summary>
					/// <param name="move"> The temporary to move. </param>
					Vertex& operator=( Vertex&& move ) = default;
			};

			/// <summary> The number of elements in the vertex. </summary>
			static const size_t size = 4;
	};

	typedef Vertex4x4f VertexBasicInstance;

} // namespace fx
} // namespace visual

#endif // _VERTEX