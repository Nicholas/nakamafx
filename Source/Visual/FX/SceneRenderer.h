#pragma once
// Interface for a rendered scene. This also includes post processing which is relatively
// complex in that it has multiple passes to multiple render targets to get a result.
//
// Project   : NaKama-Fx
// File Name : SceneRenderer.h
// Date      : 08/02/2016
// Author    : Nicholas Welters

#ifndef _RENDERING_PASS_H
#define _RENDERING_PASS_H

#include <Threading/AsyncController.h>

namespace visual
{
namespace fx
{
	/// <summary>
	/// Interface for a rendered scene. This also includes post processing which is relatively
	/// complex in that it has multiple passes to multiple render targets to get a result.
	/// </summary>
	class SceneRenderer : protected tools::AsyncController
	{
	public:
		/// <summary> ctor. </summary>
		SceneRenderer( );

		/// <summary> copy ctor </summary>
		/// <param name="copy"> The object to copy. </param>
		SceneRenderer( const SceneRenderer& copy ) = delete;

		/// <summary> move ctor </summary>
		/// <param name="move"> The temporary to move. </param>
		SceneRenderer( SceneRenderer&& move ) = delete;


		/// <summary> dtor. </summary>
		virtual ~SceneRenderer( ) = default;


		/// <summary> Copy assignment operator </summary>
		/// <param name="copy"> The object to copy. </param>
		SceneRenderer& operator=( const SceneRenderer& copy ) = delete;

		/// <summary> Move assignment operator </summary>
		/// <param name="move"> The temporary to move. </param>
		SceneRenderer& operator=( SceneRenderer&& move ) = delete;


		/// <summary> This is were we will set up the state of the GPU to render all the object in this scene. </summary>
		virtual void render( ) = 0;

		/// <summary> Update the pass and its GPU data. </summary>
		/// <param name="delta"> The time step to take for the update. </param>
		virtual void update( float delta ) = 0;

	private:
		/// <summary> The derived classes interface to finalize its updates to create a consistent internal state. </summary>
		virtual void update( ) final override;
	};
} // namespace fx
} // namespace visual

#endif // _RENDERING_PASS_H
