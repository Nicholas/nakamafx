// A projection class type.
// Defines all the methods required for objects that need to build projection matrices
//
// Project   : NaKama-Fx
// File Name : Projection.cpp
// Date      : 20-07-2011
// Author    : Nicholas Welters

#include "Projection.h"

#include <glm/gtc/matrix_transform.hpp>

namespace visual
{
namespace fx
{
	using glm::perspectiveFovLH;
	using glm::orthoLH;
	using glm::radians;
	using glm::scale;
	using glm::mat4x4;
	using glm::vec2;

	/// <summary> ctor </summary>
	/// <param name="width"> The projection width. </param>
	/// <param name="height"> The projection height. </param>
	/// <param name="nearPlane"> The projections near plane depth. </param>
	/// <param name="farPlane"> The projections far plane depth. </param>
	/// <param name="angle"> The projections viewing angle, 0 sets up an orthographic projection. </param>
	Projection::Projection( const int width, const int height, const float nearPlane, const float farPlane, const float angle )
		: size( width, height )
		, nearPlane( nearPlane )
		, farPlane( farPlane )
		, viewAngle( angle )
		, orthographic( false )
		, projection( )
	{
		if( angle == 0.0f )
		{
			orthographic = true;
		}

		resize( );
	}

	/// <summary> Update the matrices for the new screen size Set by pixel size </summary>
	/// <param name="width"> The projection width. </param>
	/// <param name="height"> The projection height. </param>
	void Projection::resize( const int width, const int height )
	{
		size.x = static_cast< float >( width );
		size.y = static_cast< float >( height );
		resize( );
	}

	/// <summary> Update the matrices for the new screen size. </summary>
	void Projection::resize( )
	{
		if( orthographic )
		{
			glm::vec4 dim = glm::vec4( -size.x, size.x, -size.y, size.y ) * 0.5f;
			projection = orthoLH( dim.x, dim.y, dim.z, dim.w, nearPlane, farPlane );
		}
		else
		{
			const float angle = radians( viewAngle );
			projection = perspectiveFovLH( angle, size.x, size.y, nearPlane, farPlane );
		}


		if( true )
		{
			projection = scale( projection, { 1, 1, 0.5 } );
//			projection = ::glm::translate( projection, { 0, 0, 0.5 } );
		}
	}

	/// <summary> Returns a reference to the projection matrix </summary>
	/// <returns> The projection matrix. </returns>
	const mat4x4& Projection::getProjection( ) const
	{
		return projection;
	}

	/// <summary> Return the rendering planes size in pixels. </summary>
	/// <returns> The projection width and height. </returns>
	vec2 Projection::getSize( ) const
	{
		return size;
	}

	/// <summary> Return the near plane distance from the camera position. </summary>
	/// <returns> The projection matrix near plane. </returns>
	float Projection::getNearPlane( ) const
	{
		return nearPlane;
	}

	/// <summary> Return the far plane distance from the camera position. </summary>
	/// <returns> The projection matrix far plane. </returns>
	float Projection::getFarPlane( ) const
	{
		return farPlane;
	}

	/// <summary> Return the field of view angle for the camera. </summary>
	/// <returns> The projection matrix field of view. </returns>
	float Projection::getFOV( ) const
	{
		return viewAngle;
	}
} // namespace fx
} // namespace visual
