#pragma once

// A viewer super class type.
// Defines all the methods required for objects that need to build view * projection
// matrices
//
// Project   : NaKama-Fx
// File Name : Input.h
// Date      : 20-07-2011
// Author    : Nicholas Welters

#ifndef _CAMERA_H
#define _CAMERA_H

#include "Projection.h"

#include <vector>
#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	/// <summary>
	/// A viewer super class type.
	/// Defines all the methods required for objects that need to build view * projection
	/// matrices
	///  </summary>
	class Camera
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="width"> The projection width. </param>
			/// <param name="height"> The projection height. </param>
			/// <param name="nearPlane"> The projection near plane depth. </param>
			/// <param name="farPlane"> The projection far plane depth. </param>
			/// <param name="angle"> THe projections field of view. </param>
			Camera( int width, int height, float nearPlane, float farPlane, float angle );

			/// <summary> copy ctor. </summary>
			/// <param name="copy"> The object to copy. </param>
			Camera( const Camera& copy ) = default;

			/// <summary> move ctor. </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			Camera( Camera&& temporary ) = default;
			

			/// <summary> dtor. </summary>
			~Camera( ) = default;

			
			/// <summary> Copy assignment operator. </summary>
			/// <param name="copy"> The object to copy. </param>
			Camera& operator=( const Camera& copy ) = default;
			
			/// <summary> Move assignment operator. </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			Camera& operator=( Camera&& temporary ) = default;


			/// <summary> Update the camera Matrices. </summary>
			void update( );

			/// <summary> Return the view projection matrix. </summary>
			/// <param name="i"> The base projection to use. </param>
			/// <returns> The view matrix multiplied to the selected projection matrix. </returns>
			glm::mat4x4& getViewProjection( int i );

			/// <summary> Return the projection matrix. </summary>
			/// <param name="i"> The base projection to use. </param>
			/// <returns> The selected projection matrix. </returns>
			const glm::mat4x4& getProjection( int i ) const;

			/// <summary> Return the view matrix. </summary>
			/// <returns> The view matrix. </returns>
			glm::mat4x4& getView( );

			/// <summary> Return the eye position </summary>
			/// <returns> The cameras position. </returns>
			glm::vec3& getEye( );

			/// <summary> Return the eye view vector </summary>
			/// <returns> The cameras viewing direction. </returns>
			glm::vec3& getAt( );

			/// <summary> Return the eye up vector </summary>
			/// <returns> The cameras up vector. </returns>
			glm::vec3& getUp( );

			/// <summary> Return the cameras rotation vector </summary>
			/// <returns> The cameras overall rotation vector. </returns>
			glm::vec3& getRotation( );

			/// <summary> Return the cameras orientation matrix - ONLY USED BY THE FREE FLY CAM? NOT WORKING </summary>
			/// <returns> The orientation matrix. </returns>
			glm::mat4x4& getOrientation( );

			/// <summary> Update the size of the projection matrix to match the output window. </summary>
			/// <param name="i"> The base projection to use. </param>
			/// <param name="width"> The projection width. </param>
			/// <param name="height"> The projection height. </param>
			void resize( int i, int width, int height );

			/// <summary> Return the rendering planes size in pixels. </summary>
			/// <param name="i"> The base projection to use. </param>
			/// <returns> The projection size. </returns>
			glm::vec2 getSize( int i );

			/// <summary> Return the near plane distance from the camera position. </summary>
			/// <param name="i"> The base projection to use. </param>
			/// <returns> The projection near plane depth. </returns>
			float getNearPlane( int i );

			/// <summary> Return the far plane distance from the camera position. </summary>
			/// <param name="i"> The base projection to use. </param>
			/// <returns> The projection far plane depth. </returns>
			float getFarPlane( int i );

			/// <summary> Return the feild of view angle for the camera. </summary>
			/// <param name="i"> The base projection to use. </param>
			/// <returns> The projection field of view. </returns>
			float getFOV( int i );


		private:
			/// <summary> The camera lens projections. </summary>
			std::vector< Projection > projections;

			/// <summary> The pre multiplied view and projection matrices. </summary>
			std::vector< ::glm::mat4x4 > viewProjections;
			
			/// <summary> The cameras view matrix. </summary>
			::glm::mat4x4 view;
			
			/// <summary> The cameras original orientation matrix. </summary>
			::glm::mat4x4 orientation;
		
			/// <summary> Viewers Direction Angles. </summary>
			::glm::vec3 rotation;

			/// <summary> Viewers position. </summary>
			::glm::vec3 eye;

			/// <summary> Viewers view direction. </summary>
			::glm::vec3 at;

			/// <summary> Viewers up direction. </summary>
			::glm::vec3 up;
	};
} // namespace fx
} // namespace visual

#endif