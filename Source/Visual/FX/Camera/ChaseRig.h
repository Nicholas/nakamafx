#pragma once



#ifndef _CHASE_RIG_H
#define _CHASE_RIG_H


#include "CameraRig.h"
#include <Macros.h>
#include <memory>
#include <glm/glm.hpp>


namespace visual
{
namespace ui
{
	FORWARD_DECLARE( Input )
} // namespace ui

namespace fx
{
	FORWARD_DECLARE( Camera )

	/// <summary> The chase camera rig to move the camera behind a point. </summary>
	class ChaseRig: public CameraRig
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="position"> The point to start behind. </param>
			/// <param name="distance"> The distance behind the point. </param>
			/// <param name="pCamera"> The camera to control. </param>
			/// <param name="pInput"> The input buffer to read. </param>
			ChaseRig( 
				const glm::vec3& position,
				float distance,
				const CameraSPtr& pCamera,
				const ui::InputSPtr& pInput
			);

			/// <summary> copy ctor. </summary>
			/// <param name="copy"> The object to copy. </param>
			ChaseRig( const ChaseRig& copy ) = default;

			/// <summary> move ctor. </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			ChaseRig( ChaseRig&& temporary ) = default;
			

			/// <summary> dtor. </summary>
			virtual ~ChaseRig( ) = default;

			
			/// <summary> Copy assignment operator. </summary>
			/// <param name="copy"> The object to copy. </param>
			ChaseRig& operator=( const ChaseRig& copy ) = default;
			
			/// <summary> Move assignment operator. </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			ChaseRig& operator=( ChaseRig&& temporary ) = default;

			
			/// <summary> Update the cameras internal state. </summary>
			/// <param name="delta"> The rate of change to apply to the update. </param>
			virtual void updatePosition( float delta ) final override;

			/// <summary> Move in the direction of the view vector. </summary>
			/// <param name="delta"> Amount to move in the view direction. </param>
			void move( float delta );

			/// <summary> Move parallel to the view and up vector. </summary>
			/// <param name="delta"> Amount to strafe in the parallel
			///		   to the view and up vector direction.
			/// </param>
			void strafe( float delta );

			/// <summary> Move up. </summary>
			/// <param name="delta"> Amount to move up. </param>
			void climb( float delta );

			/// <summary> Rotate around the Y axis </summary>
			/// <param name="delta"> Amount to rotate on the Y axis. </param>
			void rotateYaw( float delta ) const;

			/// <summary> Rotate around the X axis </summary>
			/// <param name="delta"> Amount to rotate on the X axis. </param>
			void rotatePitch( float delta ) const;

			/// <summary> Rotate around the Z axis </summary>
			/// <param name="delta"> Amount to rotate on the Z axis. </param>
			void rotateRoll( float delta ) const;

		private:
			/// <summary> Move the camera behind the point. </summary>
			void updateCameraPosition( );

		private:
			/// <summary> The point to start behind. </summary>
			glm::vec3 position;

			/// <summary> The distance behind the point. </summary>
			float distance;

			/// <summary> The camera to control. </summary>
			CameraSPtr pCamera;

			/// <summary> The input buffer to read. </summary>
			ui::InputSPtr pInput;
	};
} // namespace fx
} // namespace visual

#endif // _CHASE_RIG_H