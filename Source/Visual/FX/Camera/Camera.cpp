
// A viewer super class type.
// Defines all the methods required for objects that need to build view * projection
// matrices
//
// Project   : NaKama-Fx
// File Name : Input.h
// Date      : 20-07-2011
// Author    : Nicholas Welters

#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

namespace visual
{
namespace fx
{
	using glm::mat4x4;
	using glm::mat3x3;
	using glm::vec3;
	using glm::vec2;
	using glm::yawPitchRoll;
	using glm::lookAtLH;

	/// <summary> ctor. </summary>
	/// <param name="width"> The projection width. </param>
	/// <param name="height"> The projection height. </param>
	/// <param name="nearPlane"> The projection near plane depth. </param>
	/// <param name="farPlane"> The projection far plane depth. </param>
	/// <param name="angle"> THe projections field of view. </param>
	Camera::Camera( int width, int height, float nearPlane, float farPlane, float angle )
		: projections( )
		, viewProjections( )
		, view( )
		, orientation( 
		{
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		} )
		, rotation( 0.0f, 0.0f, 0.0f )
		, eye( 0.0f, 2.0f, -8.0f )
		, at ( 0.0f, 0.0f,  1.0f )
		, up ( 0.0f, 1.0f,  0.0f )
	{
		projections.emplace_back( width, height, nearPlane, farPlane, angle );
		viewProjections.emplace_back( );
	}

	/// <summary> Update the camera Matrices </summary>
	void Camera::update( )
	{
		const mat3x3 rotationMatrix = yawPitchRoll( rotation.y, rotation.x, rotation.z );

		at = rotationMatrix * vec3( 0, 0, 1 );
		up = rotationMatrix * vec3( 0, 1, 0 );

		view = lookAtLH( eye, at + eye, up );

		for( int i = 0; i < viewProjections.size( ); ++i )
		{
			viewProjections[ i ] = projections[ i ].getProjection( ) * view;
		}
	}

	/// <summary> Return the view projection matrix. </summary>
	/// <param name="i"> The base projection to use. </param>
	/// <returns> The view matrix multiplied to the selected projection matrix. </returns>
	mat4x4& Camera::getViewProjection( int i )
	{
		return viewProjections[ i ];
	}
	
	/// <summary> Return the projection matrix. </summary>
	/// <param name="i"> The base projection to use. </param>
	/// <returns> The selected projection matrix. </returns>
	const mat4x4& Camera::getProjection( int i ) const
	{
		return projections[ i ].getProjection( );
	}
	
	/// <summary> Return the view matrix. </summary>
	/// <returns> The view matrix. </returns>
	mat4x4& Camera::getView( )
	{
		return view;
	}
	
	/// <summary> Return the eye position </summary>
	/// <returns> The cameras position. </returns>
	vec3& Camera::getEye( )
	{
		return eye;
	}

	/// <summary> Return the eye view vector </summary>
	/// <returns> The cameras viewing direction. </returns>
	vec3& Camera::getAt( )
	{
		return at;
	}

	/// <summary> Return the eye up vector </summary>
	/// <returns> The cameras up vector. </returns>
	vec3& Camera::getUp( )
	{
		return up;
	}

	/// <summary> Return the cameras rotation vector </summary>
	/// <returns> The cameras overall rotation vector. </returns>
	vec3& Camera::getRotation( )
	{
		return rotation;
	}

	/// <summary> Return the cameras orientation matrix - ONLY USED BY THE FREE FLY CAM? NOT WORKING </summary>
	/// <returns> The orientation matrix. </returns>
	mat4x4& Camera::getOrientation()
	{
		return orientation;
	}

	/// <summary> Update the size of the projection matrix to match the output window. </summary>
	/// <param name="i"> The base projection to use. </param>
	/// <param name="width"> The projection width. </param>
	/// <param name="height"> The projection height. </param>
	void Camera::resize( int i, int width, int height )
	{
		projections[ i ].resize( width, height );
	}

	/// <summary> Return the rendering planes size in pixels. </summary>
	/// <param name="i"> The base projection to use. </param>
	/// <returns> The projection size. </returns>
	vec2 Camera::getSize( int i )
	{
		return projections[ i ].getSize( );
	}

	/// <summary> Return the near plane distance from the camera position. </summary>
	/// <param name="i"> The base projection to use. </param>
	/// <returns> The projection near plane depth. </returns>
	float Camera::getNearPlane( int i )
	{
		return projections[ i ].getNearPlane( );
	}

	/// <summary> Return the far plane distance from the camera position. </summary>
	/// <param name="i"> The base projection to use. </param>
	/// <returns> The projection far plane depth. </returns>
	float Camera::getFarPlane( int i )
	{
		return projections[ i ].getFarPlane( );
	}

	/// <summary> Return the feild of view angle for the camera. </summary>
	/// <param name="i"> The base projection to use. </param>
	/// <returns> The projection field of view. </returns>
	float Camera::getFOV( int i )
	{
		return projections[ i ].getFOV( );
	}
} // namespace fx
} // namespace visual