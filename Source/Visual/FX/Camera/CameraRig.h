#pragma once

// Camera Rig Interface.
//
// Project   : NaKama-Fx
// File Name : CameraRig.h
// Date      : 25/06/2015
// Author    : Nicholas Welters

#ifndef _CAMERA_RIG_H
#define _CAMERA_RIG_H

namespace visual
{
namespace fx
{
	/// <summary> Camera Rig Interface. </summary>
	class CameraRig
	{
		public:
			/// <summary> dtor. </summary>
			virtual ~CameraRig( ) = 0;

			/// <summary> Update the cameras internal state. </summary>
			/// <param name="delta"> The rate of change to apply to the update. </param>
			virtual void updatePosition( float delta ) = 0;
	};

	inline CameraRig::~CameraRig( ) = default;
} // namespace fx
} // namespace visual

#endif // _CAMERA_RIG_H