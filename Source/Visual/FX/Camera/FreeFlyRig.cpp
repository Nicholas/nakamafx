#include "FreeFlyRig.h"

#include "Camera.h"
#include "../Maths.h"
#include "../../UserInterface/Input.h"
#include <glm/gtc/matrix_transform.hpp>
#include <utility>

namespace visual
{
namespace fx
{
	using ui::InputSPtr;
	using glm::vec3;
	using glm::normalize;
	using glm::rotate;
	
	
	/// <summary> ctor. </summary>
	/// <param name="pCamera"> The camera to control. </param>
	/// <param name="pInput"> The input buffer to read. </param>
	FreeFlyRig::FreeFlyRig( CameraSPtr pCamera, InputSPtr pInput )
	: pCamera( std::move( pCamera ) )
	, pInput( std::move( pInput ) )
	{
	}
	
	/// <summary> Update the cameras internal state. </summary>
	/// <param name="delta"> The rate of change to apply to the update. </param>
	void FreeFlyRig::updatePosition( const float delta )
	{
		float speed = 10.0f * delta; // 0.5

		if( pInput->key( VK_SHIFT ) )
		{
			speed *= 20.0f;
		}

		// Move forwards and backwards.
		if( pInput->key( 'W' ) )
		{
			move( speed );
		}
		if( pInput->key( 'S' ) )
		{
			move( -speed );
		}

		// Move right and left.
		if( pInput->key( 'D' ) )
		{
			strafe( speed );
		}
		if( pInput->key( 'A' ) )
		{
			strafe( -speed );
		}

		// Move up and down.
		if( pInput->key( VK_SPACE ) )
		{
			climb( speed );
		}
		if( pInput->key( 'C' ) )
		{
			climb( -speed );
		}

		// View up and down.
		if( pInput->key( VK_NUMPAD8 ) )
		{
			rotatePitch( speed * 0.1f );
		}
		if( pInput->key( VK_NUMPAD2 ) )
		{
			rotatePitch( -speed * 0.1f );
		}

		// View right and left.
		if( pInput->key( VK_NUMPAD6 ) )
		{
			rotateYaw( speed * 0.2f );
		}
		if( pInput->key( VK_NUMPAD4 ) )
		{
			rotateYaw( -speed * 0.2f );
		}

		// View roll right and left.
		if( pInput->key( VK_NUMPAD9 ) )
		{
			rotateRoll( speed * 0.1f );
		}
		if( pInput->key( VK_NUMPAD7 ) )
		{
			rotateRoll( -speed * 0.1f );
		}

		// -----------------------------------------------------------------------------------
		// Mouse Input!
		// -----------------------------------------------------------------------------------
		// View up and down. left and right
		const float sensitivity = 0.005f;

		if( pInput->button( 1 ) )
		{
			rotatePitch( pInput->getYMovement( ) * -sensitivity );
			rotateYaw( pInput->getXMovement( ) * sensitivity );
		}

		pCamera->update( );
	}
			
	/// <summary> Move in the direction of the view vector. </summary>
	/// <param name="delta"> Amount to move in the view direction. </param>
	void FreeFlyRig::move( const float factor ) const
	{
		// Scale the direction vector
		const vec3 direction = normalize( pCamera->getAt( ) ) * factor;

		// Move the eye position by the direction vector.
		pCamera->getEye( ) = pCamera->getEye( ) + direction;
	}
			
	/// <summary> Move parallel to the view and up vector. </summary>
	/// <param name="delta"> Amount to strafe in the parallel
	///		   to the view and up vector direction.
	/// </param>
	void FreeFlyRig::strafe( const float factor ) const
	{
		// Calculate strafe direction vector
		vec3 direction = cross( pCamera->getUp( ), pCamera->getAt( ) );
		direction = normalize( direction ) * factor;

		// Move the eye position by the direction vector.
		pCamera->getEye( ) = pCamera->getEye( ) + direction;
	}
			
	/// <summary> Move up. </summary>
	/// <param name="delta"> Amount to move up. </param>
	void FreeFlyRig::climb( const float factor ) const
	{
		// Scale the direction vector
		const vec3 direction = normalize( pCamera->getUp( ) ) * factor;

		// Move the eye position by the direction vector.
		pCamera->getEye( ) = pCamera->getEye( ) + direction;
	}
			
	/// <summary> Rotate around the X axis </summary>
	/// <param name="delta"> Amount to rotate on the X axis. </param>
	void FreeFlyRig::rotatePitch( const float radians ) const
	{
		vec3& rotation = pCamera->getRotation( );
		rotation.x -= radians;
		Maths::limitAngle( rotation.x );

		pCamera->getOrientation( ) = rotate( pCamera->getOrientation( ), radians, vec3( -1.0f, 0.0f, 0.0f ) );
	}
			
	/// <summary> Rotate around the Y axis </summary>
	/// <param name="delta"> Amount to rotate on the Y axis. </param>
	void FreeFlyRig::rotateYaw( const float radians ) const
	{
		vec3& rotation = pCamera->getRotation( );
		rotation.y += radians;
		Maths::limitAngle( rotation.y );

		pCamera->getOrientation( ) = rotate( pCamera->getOrientation( ), radians, vec3( 0.0f, 1.0f, 0.0f ) );
	}
			
	/// <summary> Rotate around the Z axis </summary>
	/// <param name="delta"> Amount to rotate on the Z axis. </param>
	void FreeFlyRig::rotateRoll( const float radians ) const
	{
		vec3& rotation = pCamera->getRotation( );
		rotation.z -= radians;
		Maths::limitAngle( rotation.z );

		pCamera->getOrientation( ) = rotate( pCamera->getOrientation( ), radians, vec3( 0.0f, 0.0f, 1.0f ) );
	}
} // namespace fx
} // namespace visual
