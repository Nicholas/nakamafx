#include "ChaseRig.h"

#include "Camera.h"
#include "../Maths.h"
#include "../../UserInterface/Input.h"
#include <glm/gtc/matrix_transform.hpp>

namespace visual
{
namespace fx
{
	using ui::InputSPtr;
	using glm::vec3;
	using glm::normalize;
	using glm::rotate;
	using glm::cross;
	using glm::half_pi;
	
	/// <summary> ctor. </summary>
	/// <param name="position"> The point to start behind. </param>
	/// <param name="distance"> The distance behind the point. </param>
	/// <param name="pCamera"> The camera to control. </param>
	/// <param name="pInput"> The input buffer to read. </param>
	ChaseRig::ChaseRig(
		const vec3& position,
		const float distance,
		const CameraSPtr& pCamera,
		const InputSPtr& pInput )
	: position( position )
	, distance( distance )
	, pCamera( pCamera )
	, pInput( pInput )
	{
	}
	
	/// <summary> Update the cameras internal state. </summary>
	/// <param name="delta"> The rate of change to apply to the update. </param>
	void ChaseRig::updatePosition( float delta )
	{
		float speed = 2.0f * delta;

		if( pInput->key( VK_SHIFT ) )
		{
			speed *= 10.0f;
		}

		// -----------------------------------------------------------------------------------
		// Move forwards and backwards.
		// -----------------------------------------------------------------------------------
		if( pInput->key( 'W' ) )
		{
			move( speed );
		}
		if( pInput->key( 'S' ) )
		{
			move( -speed );
		}

		// -----------------------------------------------------------------------------------
		// Move right and left.
		// -----------------------------------------------------------------------------------
		if( pInput->key( 'D' ) )
		{
			strafe( speed );
		}
		if( pInput->key( 'A' ) )
		{
			strafe( -speed );
		}

		// -----------------------------------------------------------------------------------
		// Move up and down.
		// -----------------------------------------------------------------------------------
		if( pInput->key( VK_SPACE ) )
		{
			climb( speed );
		}
		if( pInput->key( 'C' ) )
		{
			climb( -speed );
		}

		// -----------------------------------------------------------------------------------
		// Mouse Input!
		// -----------------------------------------------------------------------------------
		// View up and down. left and right
		// -----------------------------------------------------------------------------------
		float sensitivaty = 0.005f;

		if( pInput->button( 1 ) )
		{
			rotatePitch( pInput->getYMovement( ) * -sensitivaty );
			rotateYaw( pInput->getXMovement( ) * sensitivaty );
		}



		// -----------------------------------------------------------------------------------
		// Distance.
		// -----------------------------------------------------------------------------------
		if( pInput->key( 'M' ) )
		{
			distance += 2.0f * speed;
		}
		if( pInput->key( 'N' ) )
		{
			distance -= 2.0f *  speed;
		}
		//distance += pInput->getWheel( );

		updateCameraPosition( );
		pCamera->update( );
	}
	
	/// <summary> Move the camera behind the point. </summary>
	void ChaseRig::updateCameraPosition( )
	{
		vec3 offset = pCamera->getAt( ) * -distance;
		pCamera->getEye( ) = position + offset;
	}
	
	/// <summary> Move in the direction of the view vector. </summary>
	/// <param name="delta"> Amount to move in the view direction. </param>
	void ChaseRig::move( const float factor )
	{
		vec3 direction = pCamera->getAt( );
		direction.y = 0;

		// Scale the direction vector
		direction = normalize( pCamera->getAt( ) ) * factor;

		// Move the eye position by the direction vector.
		position = position + direction;
	}
	
	/// <summary> Move parallel to the view and up vector. </summary>
	/// <param name="delta"> Amount to strafe in the parallel
	///		   to the view and up vector direction.
	/// </param>
	void ChaseRig::strafe( const float factor )
	{
		// Calculate strafe direction vector
		vec3 direction = cross( pCamera->getUp( ), pCamera->getAt( ) );
		direction = normalize( direction );
		direction.y = 0;

		direction *= factor;

		// Move the eye position by the direction vector.
		position = position + direction;
	}
	
	/// <summary> Move up. </summary>
	/// <param name="delta"> Amount to move up. </param>
	void ChaseRig::climb( const float factor )
	{
		vec3 direction( 0.0f, factor, 0.0f );

		// Move the eye position by the direction vector.
		position = position + direction;
	}
	
	/// <summary> Rotate around the X axis </summary>
	/// <param name="delta"> Amount to rotate on the X axis. </param>
	void ChaseRig::rotatePitch( const float radians ) const
	{
		vec3& rotation = pCamera->getRotation( );
		rotation.x -= radians;

		if( rotation.x > half_pi< float >( ) - 0.001f )
		{
			rotation.x = half_pi< float >( ) - 0.001f;
		}
		else if( rotation.x < -half_pi< float >( ) + 0.001f )
		{
			rotation.x = -half_pi< float >( ) + 0.001f;
		}
	}
	
	/// <summary> Rotate around the Y axis </summary>
	/// <param name="delta"> Amount to rotate on the Y axis. </param>
	void ChaseRig::rotateYaw( const float radians ) const
	{
		vec3& rotation = pCamera->getRotation( );
		rotation.y += radians;
		Maths::limitAngle( rotation.y );
	}
	
	/// <summary> Rotate around the Z axis </summary>
	/// <param name="delta"> Amount to rotate on the Z axis. </param>
	void ChaseRig::rotateRoll( const float radians ) const
	{
		vec3& rotation = pCamera->getRotation( );
		rotation.z -= radians;
		Maths::limitAngle( rotation.z );
	}
} // namespace fx
} // namespace visual
