#pragma once

// A projection class type.
// Defines all the methods required for objects that need to build projection matrices
//
// Project   : NaKama-Fx
// File Name : Projection.h
// Date      : 20-07-2011
// Author    : Nicholas Welters

#ifndef _PROJECTION_H
#define _PROJECTION_H

#include <glm/glm.hpp>

namespace visual
{
namespace fx
{
	/// <summary> 
	/// A projection class type.
	/// Defines all the methods required for objects that need to build projection matrices
	/// </summary>
	class Projection
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="width"> The projection width. </param>
			/// <param name="height"> The projection height. </param>
			/// <param name="nearPlane"> The projections near plane depth. </param>
			/// <param name="farPlane"> The projections far plane depth. </param>
			/// <param name="angle"> The projections viewing angle, 0 sets up an orthographic projection. </param>
			Projection( int width, int height, float nearPlane = 0.1f, float farPlane = 100.0f, float angle = 70.0f );

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			Projection( const Projection& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			Projection( Projection&& temporary ) = default;


			/// <summary> dtor </summary>
			~Projection( ) = default;

			
			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			Projection& operator=( const Projection& copy ) = default;
			
			/// <summary> Move assignment operator </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			Projection& operator=( Projection&& temporary ) = default;

			
			/// <summary> Update the matrices for the new screen size Set by pixel size </summary>
			/// <param name="width"> The projection width. </param>
			/// <param name="height"> The projection height. </param>
			void resize( int width, int height );
			
			/// <summary> Returns a reference to the projection matrix </summary>
			/// <returns> The projection matrix. </returns>
			const glm::mat4x4& getProjection( ) const;

			/// <summary> Return the rendering planes size in pixels. </summary>
			/// <returns> The projection width and height. </returns>
			glm::vec2 getSize( ) const;

			/// <summary> Return the near plane distance from the camera position. </summary>
			/// <returns> The projection matrix near plane. </returns>
			float getNearPlane( ) const;

			/// <summary> Return the far plane distance from the camera position. </summary>
			/// <returns> The projection matrix far plane. </returns>
			float getFarPlane( ) const;

			/// <summary> Return the field of view angle for the camera. </summary>
			/// <returns> The projection matrix field of view. </returns>
			float getFOV( ) const;

		private:
			/// <summary> Update the matrices for the new screen size. </summary>
			void resize( );

		private:
			/// <summary> The projection width and height. </summary>
			glm::vec2 size;
			
			/// <summary> The projection matrix near plane. </summary>
			float nearPlane;
			/// <summary> The projection matrix far plane. </summary>
			float farPlane;
			
			/// <summary> The projection matrix field of view. </summary>
			float viewAngle;
			
			/// <summary> A flag set to true when the projection id orthographic. </summary>
			bool orthographic;
			
			/// <returns> The calculated projection matrix. </returns>
			glm::mat4x4 projection;
	};
} // namespace fx
} // namespace visual

# endif