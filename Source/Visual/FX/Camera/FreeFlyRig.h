#pragma once



#ifndef _FREE_FLY_RIG_H
#define _FREE_FLY_RIG_H


#include "CameraRig.h"
#include "Camera.h"
#include "../../UserInterface/Input.h"
#include <Macros.h>
#include <memory>


namespace visual
{
namespace ui
{
	FORWARD_DECLARE( Input )
} // namespace ui

namespace fx
{
	FORWARD_DECLARE( Camera )

	class FreeFlyRig final: public CameraRig
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="pCamera"> The camera to control. </param>
			/// <param name="pInput"> The input buffer to read. </param>
			FreeFlyRig( CameraSPtr pCamera, ui::InputSPtr pInput );

			/// <summary> ctor. </summary>
			FreeFlyRig( ) = delete;

			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			FreeFlyRig( const FreeFlyRig& copy ) = default;

			/// <summary> move ctor </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			FreeFlyRig( FreeFlyRig&& temporary ) = default;


			/// <summary> dtor. </summary>
			virtual ~FreeFlyRig( ) = default;


			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			FreeFlyRig& operator=( const FreeFlyRig& copy ) = default;
			
			/// <summary> Move assignment operator </summary>
			/// <param name="temporary"> The temporary object to move. </param>
			FreeFlyRig& operator=( FreeFlyRig&& temporary ) = default;

			
			/// <summary> Update the cameras internal state. </summary>
			/// <param name="delta"> The rate of change to apply to the update. </param>
			virtual void updatePosition( float delta ) final override;
			
			/// <summary> Move in the direction of the view vector. </summary>
			/// <param name="delta"> Amount to move in the view direction. </param>
			void move( float delta ) const;
			
			/// <summary> Move parallel to the view and up vector. </summary>
			/// <param name="delta"> Amount to strafe in the parallel
			///		   to the view and up vector direction.
			/// </param>
			void strafe( float delta ) const;
			
			/// <summary> Move up. </summary>
			/// <param name="delta"> Amount to move up. </param>
			void climb( float delta ) const;
			
			/// <summary> Rotate around the Y axis </summary>
			/// <param name="delta"> Amount to rotate on the Y axis. </param>
			void rotateYaw( float delta ) const;
			
			/// <summary> Rotate around the X axis </summary>
			/// <param name="delta"> Amount to rotate on the X axis. </param>
			void rotatePitch( float delta ) const;
			
			/// <summary> Rotate around the Z axis </summary>
			/// <param name="delta"> Amount to rotate on the Z axis. </param>
			void rotateRoll( float delta ) const;

		private:
			/// <summary> The camera to control. </summary>
			CameraSPtr pCamera;

			/// <summary> The input buffer to read. </summary>
			ui::InputSPtr pInput;
	};
} // namespace fx
} // namespace visual

#endif // _FREE_FLY_RIG_H