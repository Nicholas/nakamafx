
// Renderer settings
//
// All the settings that we will use to build out rendering pipeline.
//
// Project   : NaKama-Fx
// File Name : Settings.cpp
// Date      : 08/01/2018
// Author    : Nicholas Welters

#include "Settings.h"

namespace visual
{
namespace fx
{
	using std::move;
	using std::string;
	using glm::uvec2;

	/// <summary> ctor </summary>
	/// <param name="name"> The system name. </param>
	Settings::Settings( string name )
		: systemName( move( name ) )
		, defaultScene( "DEMO" )

		, renderTargetSize( 1280, 720 )
		, renderTargetFormat( RGBA16 )
		, msaaLevel( MSAA_X1 )


		, backBufferSize( renderTargetSize )
		, backBufferFormat( RGBA8 )
		, fullscreen( false )
		, frameBuffers( 2 )
		, vsync( VSYNC_OFF )

		, shadowCascade1TargetSize( 4096, 4096 )
		, shadowCascade2TargetSize( 2048, 2048 )

		, numberOfWorkerThreads( 6 )

		, minimumAsyncJobsToExecute( 1 )
		, maximumAsyncJobsToExecute( 4 )
		, minimumSyncJobsToExecute( 2 )
		, maximumSyncJobsToExecute( 6 )
	{ }



	/// <summary> Get The system name. </summary>
	/// <returns> The systems name. </returns>
	const string & Settings::getSystemName( ) const
	{
		return systemName;
	}



	/// <summary> Get default scene to load on start up. </summary>
	/// <returns> The default startup scene. </returns>
	const string& Settings::getDefaultScene( ) const
	{
		return defaultScene;
	}

	/// <summary> Get default scene to load on start up. </summary>
	/// <param name="filePath"> The default startup scene. </param>
	void Settings::getDefaultScene( const string& filePath )
	{
		defaultScene = filePath;
	}



	/// <summary> Get the rendering size. </summary>
	/// <returns> The rendering frame buffer size. </returns>
	const uvec2& Settings::getRenderTargetSize( ) const
	{
		return renderTargetSize;
	}

	/// <summary> Rendering frame buffer dimensions. </summary>
	/// <param name="size"> Rendering size. </param>
	void Settings::setRenderTargetSize( const uvec2& size )
	{
		renderTargetSize = size;
	}



	/// <summary> Get the rendering frame buffer colour format. </summary>
	/// <returns> Frame buffer colour format. </returns>
	ColourFormat Settings::getRenderTargetFormat( ) const
	{
		return renderTargetFormat;
	}

	/// <summary> Set the rendering frame buffer colour format. </summary>
	/// <param name="format"> Rendering frame buffer colour format. </param>
	void Settings::setRenderTargetFormat( const ColourFormat format )
	{
		renderTargetFormat = format;
	}

	/// <summary> Get the render targets MSAA level. </summary>
	/// <returns> MSAA level to use. </returns>
	MsaaLevel Settings::getMsaaLevel( ) const
	{
		return msaaLevel;
	}

	/// <summary> Set the render targets MSAA level. </summary>
	/// <param name="msaaLevel"> MSAA level to use. </param>
	void Settings::setMsaaLevel( const MsaaLevel msaaLevel )
	{
		this->msaaLevel = msaaLevel;
	}




	/// <summary> Get the back buffer size. </summary>
	/// <returns> The back buffer size. </returns>
	const uvec2& Settings::getBackBufferSize( ) const
	{
		return backBufferSize;
	}

	/// <summary> Back buffer size. </summary>
	/// <param name="size"> The back buffer size. </param>
	void Settings::setBackBufferSize( const uvec2& size )
	{
		backBufferSize = size;
	}



	/// <summary> Get the rendering frame buffer colour format. </summary>
	/// <returns> Frame buffer colour format. </returns>
	ColourFormat Settings::getBackBufferFormat( ) const
	{
		return backBufferFormat;
	}

	/// <summary> Set the back buffer colour format. </summary>
	/// <param name="format"> Back buffer colour format. </param>
	void Settings::setBackBufferFormat( const ColourFormat format )
	{
		backBufferFormat = format;
	}

	/// <summary> Get window and swap chain fullscreen state. </summary>
	/// <returns> The full screen state. </returns>
	bool Settings::isFullscreen( ) const
	{
		return fullscreen;
	}

	/// <summary> Set window and swap chain fullscreen state. </summary>
	/// <param name="fullscreen"> The full screen state. </param>
	void Settings::setFullscreen( const bool fullscreen )
	{
		this->fullscreen = fullscreen;
	}

	/// <summary> Get number of frame buffers to use in the swap chain. </summary>
	/// <returns> Number of frame buffers to use. </returns>
	int  Settings::getFrameBuffers( ) const
	{
		return frameBuffers;
	}

	/// <summary> Set number of frame buffers to use in the swap chain. </summary>
	/// <param name="frameBuffers"> Number of frame buffers to use. </param>
	void Settings::setFrameBuffers( const uint32_t frameBuffers )
	{
		this->frameBuffers = frameBuffers;
	}

	/// <summary> Get window and swap chain vsync state. </summary>
	/// <returns> The vsync state. </returns>
	VSyncState Settings::getVSyncState( ) const
	{
		return vsync;
	}

	/// <summary> Set window and swap chain vsync state. </summary>
	/// <param name="vsync"> The vsync state. </param>
	void Settings::setVSyncState( VSyncState vsync )
	{
		this->vsync = vsync;
	}


	/// <summary> Get the 1st shadow cascade size. </summary>
	/// <returns> The 1st shadow cascade size. </returns>
	const uvec2& Settings::getShadowCascade1Size( ) const
	{
		return shadowCascade1TargetSize;
	}

	/// <summary> Get 1st shadow cascade size. </summary>
	/// <param name="size"> The 1st shadow cascade size. </param>
	void Settings::setShadowCascade1Size( const uvec2& size )
	{
		shadowCascade1TargetSize = size;
	}

	/// <summary> Get the 2nd shadow cascade size. </summary>
	/// <returns> The 2nd shadow cascade size. </returns>
	const uvec2& Settings::getShadowCascade2Size( ) const
	{
		return shadowCascade2TargetSize;
	}

	/// <summary> Get 2nd shadow cascade size. </summary>
	/// <param name="size"> The 2nd shadow cascade size. </param>
	void Settings::setShadowCascade2Size( const uvec2& size )
	{
		shadowCascade2TargetSize = size;
	}



	/// <summary> Get the number of additional threads to create. </summary>
	/// <returns> Number of additional threads. </returns>
	int  Settings::getNumberOfWorkerThreads( ) const
	{
		return numberOfWorkerThreads;
	}

	/// <summary> Set the number of additional threads to create. </summary>
	/// <param name="workerThreads"> Number of additional threads. </param>
	void Settings::setNumberOfWorkerThreads( const uint32_t workerThreads )
	{
		numberOfWorkerThreads = workerThreads;

		setMaximumAsyncJobsToExecute( maximumAsyncJobsToExecute );
		setMaximumSyncJobsToExecute( maximumSyncJobsToExecute );

		setMinimumAsyncJobsToExecute( minimumAsyncJobsToExecute );
		setMinimumSyncJobsToExecute( minimumSyncJobsToExecute );
	}


	/// <summary> Get the minimum number of async threads to try and keep busy in the execution engine. </summary>
	/// <returns> Minimum number of async threads. </returns>
	int  Settings::getMinimumAsyncJobsToExecute( ) const
	{
		return minimumAsyncJobsToExecute;
	}

	/// <summary> Set the minimum number of async threads to try and keep busy in the execution engine. </summary>
	/// <param name="minimum"> Minimum number of async threads. </param>
	void Settings::setMinimumAsyncJobsToExecute( const uint32_t minimum )
	{
		minimumAsyncJobsToExecute = minimum > maximumAsyncJobsToExecute ? maximumAsyncJobsToExecute : minimum;
	}

	/// <summary> Get the maximum number of async threads to run in the execution engine. </summary>
	/// <returns> Maximum number of async threads. </returns>
	int  Settings::getMaximumAsyncJobsToExecute() const
	{
		return maximumAsyncJobsToExecute;
	}

	/// <summary> Set the maximum number of async threads to run in the execution engine. </summary>
	/// <param name="maximum"> Maximum number of async threads. </param>
	void Settings::setMaximumAsyncJobsToExecute( const uint32_t maximum )
	{
		maximumAsyncJobsToExecute = maximum > numberOfWorkerThreads ? numberOfWorkerThreads : maximum;
		setMinimumAsyncJobsToExecute( minimumAsyncJobsToExecute );
	}


	/// <summary> Get the minimum number of sync threads to try and keep busy in the execution engine. </summary>
	/// <returns> Minimum number of sync threads. </returns>
	int  Settings::getMinimumSyncJobsToExecute( ) const
	{
		return minimumSyncJobsToExecute;
	}

	/// <summary> Set the minimum number of sync threads to try and keep busy in the execution engine. </summary>
	/// <param name="minimum"> Minimum number of async threads. </param>
	void Settings::setMinimumSyncJobsToExecute( const uint32_t minimum )
	{
		minimumSyncJobsToExecute = minimum > maximumSyncJobsToExecute ? maximumSyncJobsToExecute : minimum;
	}

	/// <summary> Get the maximum number of sync threads to run in the execution engine. </summary>
	/// <returns> Maximum number of sync threads. </returns>
	int  Settings::getMaximumSyncJobsToExecute( ) const
	{
		return maximumSyncJobsToExecute;
	}

	/// <summary> Set the maximum number of sync threads to run in the execution engine. </summary>
	/// <param name="maximum"> Maximum number of sync threads. </param>
	void Settings::setMaximumSyncJobsToExecute( const uint32_t maximum )
	{
		maximumSyncJobsToExecute = maximum > numberOfWorkerThreads ? numberOfWorkerThreads : maximum;
		setMinimumSyncJobsToExecute( minimumSyncJobsToExecute );
	}
} // namespace fx
} // namespace visual
