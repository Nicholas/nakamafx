#pragma once

//Mesh class
//
//This holds our RAM based mesh information, a vertex and index buffer.
//
// Project   : NaKama-Fx
// File Name : Mesh.h
// Date      : 30/05/2015
// Author    : Nicholas Welters


#ifndef _MESH_H
#define _MESH_H

#include <vector>

namespace visual
{
namespace fx
{
	
	/// <summary> Mesh vertex primitive topology </summary>
	enum MeshPrimitive
	{
		PointList = 0,
		LineList,
		LineStrip,
		TriangleList,
		TriangleStrip,
		UNDEFINED,

		SIZE
	};
	
	/// <summary> This holds our RAM based mesh information, a vertex and index buffer. </summary>
	/// <template name="VertexType"> The underlying vertex structure type. </template>
	template< class VertexType, typename IndexType = unsigned int >
	class Mesh
	{
		public:
			/// <summary> The vertex buffer vector type. </summary>
			typedef std::vector< VertexType > VertexBuffer;

			/// <summary> The index buffer vector type. </summary>
			typedef std::vector< IndexType > IndexBuffer;

		public:
			/// <summary> ctor </summary>
			/// <param name="vertexBuffer"> The mesh vertices list. </param>
			/// <param name="indexBuffer"> The vertex usage order. </param>
			/// <param name="topology"> The mesh basic primitive type. </param>
			Mesh( const VertexBuffer& vertexBuffer, const IndexBuffer& indexBuffer, MeshPrimitive topology );

			/// <summary> ctor </summary>
			/// <param name="vertexBuffer"> The mesh vertices list. </param>
			/// <param name="indexBuffer"> The vertex usage order. </param>
			/// <param name="topology"> The mesh basic primitive type. </param>
			Mesh( const VertexBuffer& vertexBuffer, IndexBuffer&& indexBuffer, MeshPrimitive topology );

			/// <summary> ctor </summary>
			/// <param name="vertexBuffer"> The mesh vertices list. </param>
			/// <param name="indexBuffer"> The vertex usage order. </param>
			/// <param name="topology"> The mesh basic primitive type. </param>
			Mesh( VertexBuffer&& vertexBuffer, const IndexBuffer& indexBuffer, MeshPrimitive topology );

			/// <summary> ctor </summary>
			/// <param name="vertexBuffer"> The mesh vertices list. </param>
			/// <param name="indexBuffer"> The vertex usage order. </param>
			/// <param name="topology"> The mesh basic primitive type. </param>
			Mesh( VertexBuffer&& vertexBuffer, IndexBuffer&& indexBuffer, MeshPrimitive topology ) noexcept;
			
			/// <summary> default ctor </summary>
			Mesh( );
			
			/// <summary> copy ctor </summary>
			/// <param name="copy"> The object to copy. </param>
			Mesh( const Mesh& copy );
			
			/// <summary> move ctor </summary>
			/// <param name="temp"> The object to move. </param>
			Mesh( Mesh&& temp ) noexcept;


			/// <summary> dtor </summary>
			~Mesh( );


			/// <summary> Copy assignment operator </summary>
			/// <param name="copy"> The object to copy. </param>
			Mesh& operator=( const Mesh& copy );
			
			/// <summary> Copy assignment operator </summary>
			/// <param name="temp"> The object to move. </param>
			Mesh& operator=( Mesh&& temp ) noexcept;


			/// <summary> Get the mesh vertex buffer. </summary>
			/// <returns> The vertex buffer. </returns>
			const VertexBuffer& getVertexBuffer( ) const;

			/// <summary> Get the index vertex buffer. </summary>
			/// <returns> The index buffer. </returns>
			const IndexBuffer& getIndexBuffer( ) const;
			

			/// <summary> Get the mesh primitive topology. </summary>
			/// <returns> The primitive topology. </returns>
			MeshPrimitive getTopology( ) const;


		private:
			/// <summary> The vertex buffer. </summary>
			VertexBuffer vertexBuffer;

			/// <summary> The index buffer. </summary>
			IndexBuffer  indexBuffer;

			/// <summary> The primitive topology. </summary>
			MeshPrimitive topology;
	};
} // namespace fx
} // namespace visual

#include "Mesh.hpp"

#endif // _MESH_H
