#pragma once
// This represents the swap chain that is responsible for presenting the data to an output device.
//
// Project   : NaKama-Fx
// File Name : SwapChain.h
// Date      : 28/10/2015
// Author    : Nicholas Welters

#ifndef _SWAP_CHAIN_H
#define _SWAP_CHAIN_H

namespace visual
{
namespace fx
{
	/// <summary>
	/// This represents the swap chain that is responsible for presenting the data to an output device.
	/// </summary>
	class SwapChain
	{
	public:
		/// <summary> dtor </summary>
		virtual ~SwapChain( ) = 0;

		/// <summary>
		///	This is called at the end of rendering and any other GPU work (eg. copying textures).
		///	Here is where we will swap render targets and textures around so that we can access
		///	the data from one thread while we are still rendering (hopefully).
		/// </summary>
		virtual void present( ) = 0;
	};

	inline SwapChain::~SwapChain( ) = default;
} // fx
} // namespace visual

#endif // _SWAP_CHAIN_H
