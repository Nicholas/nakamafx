// Create basic shapes.
//
// Project   : NaKama-Fx
// File Name : Shapes.cpp
// Date      : 03/01/2018
// Author    : Nicholas Welters

#include "Shapes.h"
#include "Maths.h"

#include <glm/gtc/constants.hpp>

namespace visual
{
namespace fx
{
#define XYZ(_x_, _y_, _z_) { (_x_), (_y_), (_z_) }
#define RGB1(_x_, _y_, _z_) { (_x_), (_y_), (_z_), 1.0f }
#define UV(_u_, _v_) (_u_), (_v_)

	using ::glm::vec2;
	using ::glm::vec3;
	using ::glm::vec4;

	static const vec4 white   = { 1, 1, 1, 1 };
	static const vec4 black   = { 0, 0, 0, 1 };
	static const vec4 red     = { 1, 0, 0, 1 };
	static const vec4 green   = { 0, 1, 0, 1 };
	static const vec4 blue    = { 0, 0, 1, 1 };
	static const vec4 yellow  = { 1, 1, 0, 1 };
	static const vec4 magenta = { 1, 0, 1, 1 };
	static const vec4 cyan    = { 0, 1, 1, 1 };


	/// <summary> Fill in the vertex array with cube vertices. </summary>
	/// <param name="buffer"> The array to place the vertices in. </param>
	void Shapes::fillCube( Vertex3f4f::Vertex( &buffer )[ 36 ] )
	{
		//red face
		buffer[  0 ] = { XYZ( -1, -1,  1 ), red     };
		buffer[  1 ] = { XYZ( -1,  1,  1 ), red     };
		buffer[  2 ] = { XYZ(  1, -1,  1 ), red     };
		buffer[  3 ] = { XYZ(  1, -1,  1 ), red     };
		buffer[  4 ] = { XYZ( -1,  1,  1 ), red     };
		buffer[  5 ] = { XYZ(  1,  1,  1 ), red     };
		//green face
		buffer[  6 ] = { XYZ( -1, -1, -1 ), green   };
		buffer[  7 ] = { XYZ(  1, -1, -1 ), green   };
		buffer[  8 ] = { XYZ( -1,  1, -1 ), green   };
		buffer[  9 ] = { XYZ( -1,  1, -1 ), green   };
		buffer[ 10 ] = { XYZ(  1, -1, -1 ), green   };
		buffer[ 11 ] = { XYZ(  1,  1, -1 ), green   };
		//blue face
		buffer[ 12 ] = { XYZ( -1,  1,  1 ), blue    };
		buffer[ 13 ] = { XYZ( -1, -1,  1 ), blue    };
		buffer[ 14 ] = { XYZ( -1,  1, -1 ), blue    };
		buffer[ 15 ] = { XYZ( -1,  1, -1 ), blue    };
		buffer[ 16 ] = { XYZ( -1, -1,  1 ), blue    };
		buffer[ 17 ] = { XYZ( -1, -1, -1 ), blue    };
		//yellow face
		buffer[ 18 ] = { XYZ(  1,  1,  1 ), yellow  };
		buffer[ 19 ] = { XYZ(  1,  1, -1 ), yellow  };
		buffer[ 20 ] = { XYZ(  1, -1,  1 ), yellow  };
		buffer[ 21 ] = { XYZ(  1, -1,  1 ), yellow  };
		buffer[ 22 ] = { XYZ(  1,  1, -1 ), yellow  };
		buffer[ 23 ] = { XYZ(  1, -1, -1 ), yellow  };
		//magenta face
		buffer[ 24 ] = { XYZ(  1,  1,  1 ), magenta };
		buffer[ 25 ] = { XYZ( -1,  1,  1 ), magenta };
		buffer[ 26 ] = { XYZ(  1,  1, -1 ), magenta };
		buffer[ 27 ] = { XYZ(  1,  1, -1 ), magenta };
		buffer[ 28 ] = { XYZ( -1,  1,  1 ), magenta };
		buffer[ 29 ] = { XYZ( -1,  1, -1 ), magenta };
		//cyan face
		buffer[ 30 ] = { XYZ(  1, -1,  1 ), cyan    };
		buffer[ 31 ] = { XYZ(  1, -1, -1 ), cyan    };
		buffer[ 32 ] = { XYZ( -1, -1,  1 ), cyan    };
		buffer[ 33 ] = { XYZ( -1, -1,  1 ), cyan    };
		buffer[ 34 ] = { XYZ(  1, -1, -1 ), cyan    };
		buffer[ 35 ] = { XYZ( -1, -1, -1 ), cyan    };
	}

	/// <summary> Fill in the vertex array with coordinate star vertices. </summary>
	/// <param name="buffer"> The array to place the vertices in. </param>
	void Shapes::fillCoordStar( Vertex3f4f::Vertex( &buffer )[ 72 ] )
	{
		float starScale = 1.0f;
		float cubeScale = 0.04f;
		float hlf = 0.5f;

		vec3 pX( starScale, 0, 0 );
		vec3 nX(-starScale, 0, 0 );
		vec3 pY( 0, starScale, 0 );
		vec3 nY( 0,-starScale, 0 );
		vec3 pZ( 0, 0, starScale );
		vec3 nZ( 0, 0,-starScale );

		vec3 pXpYpZ = ( pX + pY + pZ ) * cubeScale;
		vec3 pXnYpZ = ( pX + nY + pZ ) * cubeScale;
		vec3 pXnYnZ = ( pX + nY + nZ ) * cubeScale;
		vec3 pXpYnZ = ( pX + pY + nZ ) * cubeScale;
		vec3 nXpYpZ = ( nX + pY + pZ ) * cubeScale;
		vec3 nXnYpZ = ( nX + nY + pZ ) * cubeScale;
		vec3 nXnYnZ = ( nX + nY + nZ ) * cubeScale;
		vec3 nXpYnZ = ( nX + pY + nZ ) * cubeScale;

		////////////
		/// + X ///
		//////////
		buffer[  0 ] = { pX    , red   };
		buffer[  1 ] = { pXpYnZ, black };
		buffer[  2 ] = { pXnYnZ, black };

		buffer[  3 ] = { pX    , red   };
		buffer[  4 ] = { pXnYpZ, black };
		buffer[  5 ] = { pXpYpZ, black };

		buffer[  6 ] = { pX    , red   };
		buffer[  7 ] = { pXnYnZ, black };
		buffer[  8 ] = { pXnYpZ, black };

		buffer[  9 ] = { pX    , red   };
		buffer[ 10 ] = { pXpYpZ, black };
		buffer[ 11 ] = { pXpYnZ, black };


		////////////
		/// - X ///
		//////////
		buffer[ 12 ] = { nX*hlf, cyan  };
		buffer[ 13 ] = { nXpYpZ, black };
		buffer[ 14 ] = { nXnYpZ, black };

		buffer[ 15 ] = { nX*hlf, cyan  };
		buffer[ 16 ] = { nXnYnZ, black };
		buffer[ 17 ] = { nXpYnZ, black };

		buffer[ 18 ] = { nX*hlf, cyan  };
		buffer[ 19 ] = { nXpYnZ, black };
		buffer[ 20 ] = { nXpYpZ, black };

		buffer[ 21 ] = { nX*hlf, cyan  };
		buffer[ 22 ] = { nXnYpZ, black };
		buffer[ 23 ] = { nXnYnZ, black };


		////////////
		/// + Y ///
		//////////
		buffer[ 24 ] = { pY    , green };
		buffer[ 25 ] = { nXpYnZ, black };
		buffer[ 26 ] = { pXpYnZ, black };

		buffer[ 27 ] = { pY    , green };
		buffer[ 28 ] = { pXpYpZ, black };
		buffer[ 29 ] = { nXpYpZ, black };

		buffer[ 30 ] = { pY    , green };
		buffer[ 31 ] = { nXpYpZ, black };
		buffer[ 32 ] = { nXpYnZ, black };

		buffer[ 33 ] = { pY    , green };
		buffer[ 34 ] = { pXpYnZ, black };
		buffer[ 35 ] = { pXpYpZ, black };


		////////////
		/// - Y ///
		//////////
		buffer[ 36 ] = { nY*hlf, magenta };
		buffer[ 37 ] = { nXnYpZ, black   };
		buffer[ 38 ] = { pXnYpZ, black   };

		buffer[ 39 ] = { nY*hlf, magenta };
		buffer[ 40 ] = { pXnYnZ, black   };
		buffer[ 41 ] = { nXnYnZ, black   };

		buffer[ 42 ] = { nY*hlf, magenta };
		buffer[ 43 ] = { pXnYpZ, black   };
		buffer[ 44 ] = { pXnYnZ, black   };

		buffer[ 45 ] = { nY*hlf, magenta };
		buffer[ 46 ] = { nXnYnZ, black   };
		buffer[ 47 ] = { nXnYpZ, black   };


		////////////
		/// + Z ///
		//////////
		buffer[ 48 ] = { pZ    , blue  };
		buffer[ 49 ] = { pXpYpZ, black };
		buffer[ 50 ] = { pXnYpZ, black };

		buffer[ 51 ] = { pZ    , blue  };
		buffer[ 52 ] = { pXnYpZ, black };
		buffer[ 53 ] = { nXnYpZ, black };

		buffer[ 54 ] = { pZ    , blue  };
		buffer[ 55 ] = { nXnYpZ, black };
		buffer[ 56 ] = { nXpYpZ, black };

		buffer[ 57 ] = { pZ    , blue  };
		buffer[ 58 ] = { nXpYpZ, black };
		buffer[ 59 ] = { pXpYpZ, black };


		////////////
		/// - Z ///
		//////////
		buffer[ 60 ] = { nZ*hlf, yellow };
		buffer[ 61 ] = { pXnYnZ, black  };
		buffer[ 62 ] = { pXpYnZ, black  };

		buffer[ 63 ] = { nZ*hlf, yellow };
		buffer[ 64 ] = { nXnYnZ, black  };
		buffer[ 65 ] = { pXnYnZ, black  };

		buffer[ 66 ] = { nZ*hlf, yellow };
		buffer[ 67 ] = { nXpYnZ, black  };
		buffer[ 68 ] = { nXnYnZ, black  };

		buffer[ 69 ] = { nZ*hlf, yellow };
		buffer[ 70 ] = { pXpYnZ, black  };
		buffer[ 71 ] = { nXpYnZ, black  };
	}


	/// <summary> Create a coord star mesh. </summary>
	/// <returns> A new coord mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::coordStar( )
	{
		const int verticesSize = 72;
		const int indicesSize  = 72;

		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( verticesSize );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( indicesSize );

		Vertex3f4f::Vertex buffer[ verticesSize ];
		fillCoordStar( buffer );

		for( int i = 0; i < verticesSize; i += 3 )
		{
			vBuff[ i + 0 ].position = buffer[ i + 0 ].position;
			vBuff[ i + 1 ].position = buffer[ i + 1 ].position;
			vBuff[ i + 2 ].position = buffer[ i + 2 ].position;

			vBuff[ i + 0 ].textureUV = { 0.4f, 1.0f };
			vBuff[ i + 1 ].textureUV = { 1.0f, 0.0f };
			vBuff[ i + 2 ].textureUV = { 0.6f, 0.0f };

			vBuff[ i + 0 ].normal = { 0, 0, 0 };

			Maths::appendNormal(
				vBuff[ i + 0 ].normal,
				vBuff[ i + 0 ].position,
				vBuff[ i + 1 ].position,
				vBuff[ i + 2 ].position
			);
			vBuff[ i + 0 ].normal = -vBuff[ i + 0 ].normal;
			vBuff[ i + 1 ].normal =  vBuff[ i + 0 ].normal;
			vBuff[ i + 2 ].normal =  vBuff[ i + 0 ].normal;


			Maths::calculateTangents(
				vBuff[ i + 0 ].tangent  , vBuff[ i + 0 ].binormal,
				vBuff[ i + 0 ].position , vBuff[ i + 1 ].position,  vBuff[ i + 2 ].position,
				vBuff[ i + 0 ].textureUV, vBuff[ i + 1 ].textureUV, vBuff[ i + 2 ].textureUV
			);
			vBuff[ i + 2 ].tangent  = vBuff[ i + 1 ].tangent  = vBuff[ i + 0 ].tangent;
			vBuff[ i + 2 ].binormal = vBuff[ i + 1 ].binormal = vBuff[ i + 0 ].binormal;


			iBuff[ i     ] = i;
			iBuff[ i + 1 ] = i + 1;
			iBuff[ i + 2 ] = i + 2;
		}

		return Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleList );
	}

	/// <summary> Create a cube mesh. </summary>
	/// <returns> A new cube mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::testSlate( )
	{
		//Mesh< Vertex3f2f::Vertex > cube = fullScreenCube( );

		const int verticesSize = 24;
		const int indicesSize  = 36;

		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( verticesSize );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( indicesSize );



		vec3 pX(  1,  0,  0 );
		vec3 nX( -1,  0,  0 );
		vec3 pY(  0,  1,  0 );
		vec3 nY(  0, -1,  0 );
		vec3 pZ(  0,  0,  1 );
		vec3 nZ(  0,  0, -1 );

		vec3 _XYZ(  1.0f,  1.0f,  1.0f );
		vec3 _xYZ( -1.0f,  1.0f,  1.0f );
		vec3 _xYz( -1.0f,  1.0f, -1.0f );
		vec3 _XYz(  1.0f,  1.0f, -1.0f );

		vec3 _XyZ(  1.0f, -1.0f,  1.0f );
		vec3 _xyZ( -1.0f, -1.0f,  1.0f );
		vec3 _xyz( -1.0f, -1.0f, -1.0f );
		vec3 _Xyz(  1.0f, -1.0f, -1.0f );

		vec2 _uv( 0, 0 );
		vec2 _uV( 0, 1 );
		vec2 _UV( 1, 1 );
		vec2 _Uv( 1, 0 );

		vec3 empty( 0, 0, 0 );


		vBuff[ 0 ] = { _XYZ, _uV, pY, empty, empty };
		vBuff[ 1 ] = { _xYZ, _uv, pY, empty, empty };
		vBuff[ 2 ] = { _xYz, _Uv, pY, empty, empty };
		vBuff[ 3 ] = { _XYz, _UV, pY, empty, empty };

		vBuff[ 4 ] = { _XyZ, _uV, nY, empty, empty };
		vBuff[ 5 ] = { _xyZ, _uv, nY, empty, empty };
		vBuff[ 6 ] = { _xyz, _Uv, nY, empty, empty };
		vBuff[ 7 ] = { _Xyz, _UV, nY, empty, empty };


		vBuff[ 8  ] = { _xYZ, _uV, nX, empty, empty };
		vBuff[ 9  ] = { _xyZ, _uv, nX, empty, empty };
		vBuff[ 10 ] = { _xyz, _Uv, nX, empty, empty };
		vBuff[ 11 ] = { _xYz, _UV, nX, empty, empty };

		vBuff[ 12 ] = { _XYZ, _uV, pX, empty, empty };
		vBuff[ 13 ] = { _XyZ, _uv, pX, empty, empty };
		vBuff[ 14 ] = { _Xyz, _Uv, pX, empty, empty };
		vBuff[ 15 ] = { _XYz, _UV, pX, empty, empty };


		vBuff[ 16 ] = { _XYz, _uV, nZ, empty, empty };
		vBuff[ 17 ] = { _Xyz, _uv, nZ, empty, empty };
		vBuff[ 18 ] = { _xyz, _Uv, nZ, empty, empty };
		vBuff[ 19 ] = { _xYz, _UV, nZ, empty, empty };

		vBuff[ 20 ] = { _XYZ, _uV, pZ, empty, empty };
		vBuff[ 21 ] = { _XyZ, _uv, pZ, empty, empty };
		vBuff[ 22 ] = { _xyZ, _Uv, pZ, empty, empty };
		vBuff[ 23 ] = { _xYZ, _UV, pZ, empty, empty };



		// 1
		iBuff[ 0 ] = 0;
		iBuff[ 1 ] = 3;
		iBuff[ 2 ] = 1;

		iBuff[ 3 ] = 1;
		iBuff[ 4 ] = 3;
		iBuff[ 5 ] = 2;

		// 2
		iBuff[ 6 ] = 4;
		iBuff[ 7 ] = 5;
		iBuff[ 8 ] = 7;

		iBuff[ 9 ]  = 5;
		iBuff[ 10 ] = 6;
		iBuff[ 11 ] = 7;

		// 3
		iBuff[ 12 ] = 8;
		iBuff[ 13 ] = 11;
		iBuff[ 14 ] = 9;

		iBuff[ 15 ] = 9;
		iBuff[ 16 ] = 11;
		iBuff[ 17 ] = 10;

		//4
		iBuff[ 18 ] = 12;
		iBuff[ 19 ] = 13;
		iBuff[ 20 ] = 15;

		iBuff[ 21 ] = 13;
		iBuff[ 22 ] = 14;
		iBuff[ 23 ] = 15;

		//
		iBuff[ 24 ] = 16;
		iBuff[ 25 ] = 17;
		iBuff[ 26 ] = 19;

		iBuff[ 27 ] = 17;
		iBuff[ 28 ] = 18;
		iBuff[ 29 ] = 19;

		//
		iBuff[ 30 ] = 20;
		iBuff[ 31 ] = 23;
		iBuff[ 32 ] = 21;

		iBuff[ 33 ] = 21;
		iBuff[ 34 ] = 23;
		iBuff[ 35 ] = 22;

		for( int i = 0; i < verticesSize; i += 4 )
		{
			Maths::calculateTangents(
				vBuff[ i + 0 ].tangent  , vBuff[ i + 0 ].binormal,
				vBuff[ i + 0 ].position , vBuff[ i + 1 ].position , vBuff[ i + 2 ].position,
				vBuff[ i + 0 ].textureUV, vBuff[ i + 1 ].textureUV, vBuff[ i + 2 ].textureUV
			);

			vBuff[ i + 3 ].tangent  = vBuff[ i + 2 ].tangent  = vBuff[ i + 1 ].tangent  = vBuff[ i + 0 ].tangent;
			vBuff[ i + 3 ].binormal = vBuff[ i + 2 ].binormal = vBuff[ i + 1 ].binormal = vBuff[ i + 0 ].binormal;
		}


		return Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleList );
	}

	/// <summary> Create a cube mesh thats shaped like an arrow head. </summary>
	/// <returns> A new arrow with each side with smooth normals. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::smoothArrowQuad( )
	{
		const int verticesSize = 10;
		const int indicesSize  = 24;

		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( verticesSize );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( indicesSize );

		float uvL = 0.0f;
		float uvR = 1.0f;
		float uvB = 0.0f;
		float uvM = 0.33f;
		float uvT = 1.0f;

		vec3 empty( 0, 0, 0 );

		const int m1 = 3;
		const int l1 = 2;
		const int r1 = 1;
		const int t1 = 4;
		const int b1 = 0;

		const int m2 = 8;
		const int l2 = 7;
		const int r2 = 6;
		const int t2 = 9;
		const int b2 = 5;

		float width = 0.50f;
		float depth = 0.25f;

		float height = 1;
		float offset = height * 0.33f;

		vBuff[ b1 ] = { {      0,      0,      0 }, { 0.5, uvB }, empty, empty, empty };
		vBuff[ r1 ] = { {  width, offset,      0 }, { uvL, uvM }, empty, empty, empty };
		vBuff[ l1 ] = { { -width, offset,      0 }, { uvR, uvM }, empty, empty, empty };
		vBuff[ m1 ] = { {      0, offset,  depth }, { 0.5, uvM }, empty, empty, empty };
		vBuff[ t1 ] = { {      0, height,      0 }, { 0.5, uvT }, empty, empty, empty };

		vBuff[ b2 ] = { {      0,      0,      0 }, { 0.5, uvB }, empty, empty, empty };
		vBuff[ r2 ] = { {  width, offset,      0 }, { uvR, uvM }, empty, empty, empty };
		vBuff[ l2 ] = { { -width, offset,      0 }, { uvL, uvM }, empty, empty, empty };
		vBuff[ m2 ] = { {      0, offset, -depth }, { 0.5, uvM }, empty, empty, empty };
		vBuff[ t2 ] = { {      0, height,      0 }, { 0.5, uvT }, empty, empty, empty };


		iBuff[  0 ] = m1;
		iBuff[  1 ] = b1;
		iBuff[  2 ] = r1;

		iBuff[  3 ] = m1;
		iBuff[  4 ] = l1;
		iBuff[  5 ] = b1;

		iBuff[  6 ] = m1;
		iBuff[  7 ] = t1;
		iBuff[  8 ] = l1;

		iBuff[  9 ] = m1;
		iBuff[ 10 ] = r1;
		iBuff[ 11 ] = t1;


		iBuff[ 12 ] = m2;
		iBuff[ 13 ] = r2;
		iBuff[ 14 ] = b2;

		iBuff[ 15 ] = m2;
		iBuff[ 16 ] = b2;
		iBuff[ 17 ] = l2;

		iBuff[ 18 ] = m2;
		iBuff[ 19 ] = l2;
		iBuff[ 20 ] = t2;

		iBuff[ 21 ] = m2;
		iBuff[ 22 ] = t2;
		iBuff[ 23 ] = r2;

		for( int i = 0; i < iBuff.size( ); i += 3 )
		{
			const int i0 = iBuff[ i + 0 ];
			const int i1 = iBuff[ i + 1 ];
			const int i2 = iBuff[ i + 2 ];

			const vec3 vector1 = vBuff[ i0 ].position - vBuff[ i1 ].position;
			const vec3 vector2 = vBuff[ i0 ].position - vBuff[ i2 ].position;

			const vec3 normal = cross( vector1, vector2 );

			vBuff[ i0 ].normal += normal;
			vBuff[ i1 ].normal += normal;
			vBuff[ i2 ].normal += normal;

			Maths::calculateTangents(
				vBuff[ i0 ].tangent  , vBuff[ i0 ].binormal,
				vBuff[ i0 ].position , vBuff[ i1 ].position , vBuff[ i2 ].position,
				vBuff[ i0 ].textureUV, vBuff[ i1 ].textureUV, vBuff[ i2 ].textureUV
			);
		}

		return Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleList );
	}

	/// <summary> Create a cube mesh thats shaped like an arrow head. </summary>
	/// <returns> A new cube mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::arrowQuad( )
	{
		const int verticesSize = 24;
		const int indicesSize  = 24;

		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer v( 10 );

		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( verticesSize );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( indicesSize );

		float uvL = 0.0f;
		float uvR = 1.0f;
		float uvB = 0.0f;
		float uvM = 0.33f;
		float uvT = 1.0f;

		vec3 empty( 0, 0, 0 );

		const int m1 = 3;
		const int l1 = 2;
		const int r1 = 1;
		const int t1 = 4;
		const int b1 = 0;

		const int m2 = 8;
		const int l2 = 7;
		const int r2 = 6;
		const int t2 = 9;
		const int b2 = 5;

		float width = 0.50f;
		float depth = 0.25f;

		float height = 1;
		float offset = height * 0.33f;

		v[ b1 ] = { {      0,      0,      0 }, { 0.5, uvB }, empty, empty, empty };
		v[ r1 ] = { {  width, offset,      0 }, { uvL, uvM }, empty, empty, empty };
		v[ l1 ] = { { -width, offset,      0 }, { uvR, uvM }, empty, empty, empty };
		v[ m1 ] = { {      0, offset,  depth }, { 0.5, uvM }, empty, empty, empty };
		v[ t1 ] = { {      0, height,      0 }, { 0.5, uvT }, empty, empty, empty };

		v[ b2 ] = { {      0,      0,      0 }, { 0.5, uvB }, empty, empty, empty };
		v[ r2 ] = { {  width, offset,      0 }, { uvR, uvM }, empty, empty, empty };
		v[ l2 ] = { { -width, offset,      0 }, { uvL, uvM }, empty, empty, empty };
		v[ m2 ] = { {      0, offset, -depth }, { 0.5, uvM }, empty, empty, empty };
		v[ t2 ] = { {      0, height,      0 }, { 0.5, uvT }, empty, empty, empty };




		vBuff[  0 ] = v[ m1 ];
		vBuff[  1 ] = v[ b1 ];
		vBuff[  2 ] = v[ r1 ];

		vBuff[  3 ] = v[ m1 ];
		vBuff[  4 ] = v[ l1 ];
		vBuff[  5 ] = v[ b1 ];

		vBuff[  6 ] = v[ m1 ];
		vBuff[  7 ] = v[ t1 ];
		vBuff[  8 ] = v[ l1 ];

		vBuff[  9 ] = v[ m1 ];
		vBuff[ 10 ] = v[ r1 ];
		vBuff[ 11 ] = v[ t1 ];


		vBuff[ 12 ] = v[ m2 ];
		vBuff[ 13 ] = v[ r2 ];
		vBuff[ 14 ] = v[ b2 ];

		vBuff[ 15 ] = v[ m2 ];
		vBuff[ 16 ] = v[ b2 ];
		vBuff[ 17 ] = v[ l2 ];

		vBuff[ 18 ] = v[ m2 ];
		vBuff[ 19 ] = v[ l2 ];
		vBuff[ 20 ] = v[ t2 ];

		vBuff[ 21 ] = v[ m2 ];
		vBuff[ 22 ] = v[ t2 ];
		vBuff[ 23 ] = v[ r2 ];



		iBuff[  0 ] = m1;
		iBuff[  1 ] = b1;
		iBuff[  2 ] = r1;

		iBuff[  3 ] = m1;
		iBuff[  4 ] = l1;
		iBuff[  5 ] = b1;

		iBuff[  6 ] = m1;
		iBuff[  7 ] = t1;
		iBuff[  8 ] = l1;

		iBuff[  9 ] = m1;
		iBuff[ 10 ] = r1;
		iBuff[ 11 ] = t1;


		iBuff[ 12 ] = m2;
		iBuff[ 13 ] = r2;
		iBuff[ 14 ] = b2;

		iBuff[ 15 ] = m2;
		iBuff[ 16 ] = b2;
		iBuff[ 17 ] = l2;

		iBuff[ 18 ] = m2;
		iBuff[ 19 ] = l2;
		iBuff[ 20 ] = t2;

		iBuff[ 21 ] = m2;
		iBuff[ 22 ] = t2;
		iBuff[ 23 ] = r2;

		for( int i = 0; i < iBuff.size( ); i += 3 )
		{
			const int i0 = i;
			const int i1 = i + 1;
			const int i2 = i + 2;

			iBuff[ i0 ] = i0;
			iBuff[ i1 ] = i1;
			iBuff[ i2 ] = i2;

			const vec3 vector1 = vBuff[ i0 ].position - vBuff[ i1 ].position;
			const vec3 vector2 = vBuff[ i0 ].position - vBuff[ i2 ].position;

			const vec3 normal = cross( vector1, vector2 );

			vBuff[ i0 ].normal += normal;
			vBuff[ i1 ].normal += normal;
			vBuff[ i2 ].normal += normal;

			Maths::calculateTangents(
				vBuff[ i0 ].tangent  , vBuff[ i0 ].binormal,
				vBuff[ i0 ].position , vBuff[ i1 ].position , vBuff[ i2 ].position,
				vBuff[ i0 ].textureUV, vBuff[ i1 ].textureUV, vBuff[ i2 ].textureUV
			);
		}

		return Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleList );
	}

	/// <summary> Create a fullscreen triangle mesh. </summary>
	/// <returns> A new fullscreen triangle mesh. </returns>
	Mesh< Vertex3f2f::Vertex > Shapes::fullScreenTri( )
	{
		Mesh< Vertex3f2f::Vertex >::VertexBuffer vBuff( 3 );
		Mesh< Vertex3f2f::Vertex >::IndexBuffer  iBuff( 3 );

		vBuff[ 0 ] = { vec3( -2.0f,  2.0f, 0 ), vec2( -0.5f, -0.5f ) };
		vBuff[ 1 ] = { vec3(  4.0f,  2.0f, 0 ), vec2(  2.5f, -0.5f ) };
		vBuff[ 2 ] = { vec3( -2.0f, -4.0f, 0 ), vec2( -0.5f,  2.5f ) };

		iBuff[ 0 ] = 0;
		iBuff[ 1 ] = 1;
		iBuff[ 2 ] = 2;

		return Mesh< Vertex3f2f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleStrip );
	}

	/// <summary> Create a fullscreen cube mesh. </summary>
	/// <returns> A new fullscreen cube mesh. </returns>
	Mesh< Vertex3f2f::Vertex > Shapes::fullScreenCube( )
	{
		fx::Mesh< Vertex3f2f::Vertex >::VertexBuffer vBuff( 8 );
		fx::Mesh< Vertex3f2f::Vertex >::IndexBuffer  iBuff( 36 );

		vBuff[ 0 ] = { vec3(  1.0f,  1.0f,  1.0f ), vec2( 0.0f, 0.0f ) };
		vBuff[ 1 ] = { vec3( -1.0f,  1.0f,  1.0f ), vec2( 0.0f, 0.0f ) };
		vBuff[ 2 ] = { vec3( -1.0f,  1.0f, -1.0f ), vec2( 0.0f, 0.0f ) };
		vBuff[ 3 ] = { vec3(  1.0f,  1.0f, -1.0f ), vec2( 0.0f, 0.0f ) };

		vBuff[ 4 ] = { vec3(  1.0f, -1.0f,  1.0f ), vec2( 0.0f, 0.0f ) };
		vBuff[ 5 ] = { vec3( -1.0f, -1.0f,  1.0f ), vec2( 0.0f, 0.0f ) };
		vBuff[ 6 ] = { vec3( -1.0f, -1.0f, -1.0f ), vec2( 0.0f, 0.0f ) };
		vBuff[ 7 ] = { vec3(  1.0f, -1.0f, -1.0f ), vec2( 0.0f, 0.0f ) };


		iBuff[ 0 ] = 0;
		iBuff[ 1 ] = 1;
		iBuff[ 2 ] = 3;

		iBuff[ 3 ] = 1;
		iBuff[ 4 ] = 2;
		iBuff[ 5 ] = 3;


		iBuff[ 6 ] = 4;
		iBuff[ 7 ] = 7;
		iBuff[ 8 ] = 5;

		iBuff[ 9 ] = 5;
		iBuff[ 10 ] = 7;
		iBuff[ 11 ] = 6;


		iBuff[ 12 ] = 1;
		iBuff[ 13 ] = 5;
		iBuff[ 14 ] = 2;

		iBuff[ 15 ] = 2;
		iBuff[ 16 ] = 5;
		iBuff[ 17 ] = 6;


		iBuff[ 18 ] = 0;
		iBuff[ 19 ] = 3;
		iBuff[ 20 ] = 7;

		iBuff[ 21 ] = 0;
		iBuff[ 22 ] = 7;
		iBuff[ 23 ] = 4;


		iBuff[ 24 ] = 0;
		iBuff[ 25 ] = 4;
		iBuff[ 26 ] = 1;

		iBuff[ 27 ] = 4;
		iBuff[ 28 ] = 5;
		iBuff[ 29 ] = 1;


		iBuff[ 30 ] = 2;
		iBuff[ 31 ] = 6;
		iBuff[ 32 ] = 3;

		iBuff[ 33 ] = 6;
		iBuff[ 34 ] = 7;
		iBuff[ 35 ] = 3;

		return Mesh< Vertex3f2f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleList );
	}

	/// <summary> Create a grid mesh. </summary>
	/// <param name="size"> The width/depth of the grid vertices. </param>
	/// <param name="cellWidth"> The size of the cell. </param>
	/// <param name="height"> The height of the grid. </param>
	/// <returns> A new grid mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::grid( const int size, const float cellWidth, const float height )
	{
		return grid( size, size, cellWidth, [ height ]( int ) { return height; } );
	}

	/// <summary> Create a height map mesh. </summary>
	/// <param name="heights"> The raw height map array data. </param>
	/// <param name="size"> The width/depth of the grid vertices. </param>
	/// <param name="cellWidth"> The size of the cell. </param>
	/// <param name="minHeight"> The lowest height on the height map. </param>
	/// <param name="maxHeight"> The highest point on the height map. </param>
	/// <returns> A new height map mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::heightMap( const BYTE* const heights, const int size, const float cellWidth, const float minHeight, const float maxHeight )
	{
		float diffHeight = maxHeight - minHeight;
		return grid( size, size, cellWidth, [ heights, minHeight, diffHeight ]( const int index ) { return float( heights[ index ] ) / 255.0f * diffHeight + minHeight; } );
	}

	/// <summary> Create a grid mesh with a function to provide a height for a grid vertex. </summary>
	/// <param name="width"> The width of the grid in vertices (cells = vertices - 1). </param>
	/// <param name="depth"> The depth of the grid in vertices. </param>
	/// <param name="cellWidth"> The size of a cell. </param>
	/// <param name="heights"> A function that supplies the heights for each vertex. </param>
	/// <returns> A new grid mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::grid( const int width, const int depth, const float cellWidth, const ::std::function< float( int ) >& heights )
	{
		// -----------------------------------------------------------------------------------
		// Mesh Vertices Size and set-up memory space
		// -----------------------------------------------------------------------------------
		const int verticesSize = width * depth;
		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( verticesSize );


		// -----------------------------------------------------------------------------------
		// Centre the mesh
		// -----------------------------------------------------------------------------------
		const float offsetSize = cellWidth;
		const float xOffset = -1 * ( ( static_cast< float >( width ) - 1 ) / 2 ) * offsetSize;
		const float yOffset = -1 * ( ( static_cast< float >( depth ) - 1 ) / 2 ) * offsetSize;

		const float textureU = 1.0f / ( width - 1 );
		const float textureV = 1.0f / ( depth - 1 );

		// -----------------------------------------------------------------------------------
		// Calculate mesh points
		// -----------------------------------------------------------------------------------
		int index;
		for( int d = 0; d < depth; d++ )
		{
			for( int w = 0; w < width; w++ )
			{
				index = w + ( d * width );

				vBuff[ index ].position.x = xOffset + static_cast< float >( w * offsetSize );
				vBuff[ index ].position.z = yOffset + static_cast< float >( d * offsetSize );
				vBuff[ index ].position.y = heights( index );

				vBuff[ index ].textureUV = vec2( textureU * w, textureV * d );
				vBuff[ index ].normal    = vec3( 0.0f, 1.0f, 0.0f );
			}
		}

		// Doesn't work on a rectangle only squares
		//for( int x = 0; x < depth - 1; x++ )
		//{
		//	for( int y = 0; y < width - 1; y++ )
		//	{
		//		const int indexX0Y0 = x + ( y             * width );
		//		const int indexX1Y0 = x + 1 + ( y         * width );
		//		const int indexX0Y1 = x + ( ( y + 1 )     * width );
		//		const int indexX1Y1 = x + 1 + ( ( y + 1 ) * width );
		//
		//		Maths::appendNormal( vBuff[ indexX0Y0 ].normal  , vBuff[ indexX0Y0 ].position,
		//							 vBuff[ indexX0Y1 ].position, vBuff[ indexX1Y0 ].position );
		//
		//		Maths::appendNormal( vBuff[ indexX0Y1 ].normal  , vBuff[ indexX0Y1 ].position,
		//							 vBuff[ indexX1Y0 ].position, vBuff[ indexX0Y0 ].position );
		//
		//		Maths::appendNormal( vBuff[ indexX1Y0 ].normal  , vBuff[ indexX1Y0 ].position,
		//							 vBuff[ indexX0Y0 ].position, vBuff[ indexX0Y1 ].position );
		//
		//		Maths::appendNormal( vBuff[ indexX1Y1 ].normal  , vBuff[ indexX1Y1 ].position,
		//							 vBuff[ indexX1Y0 ].position, vBuff[ indexX0Y1 ].position );
		//
		//		Maths::appendNormal( vBuff[ indexX0Y1 ].normal  , vBuff[ indexX0Y1 ].position,
		//							 vBuff[ indexX1Y1 ].position, vBuff[ indexX1Y0 ].position );
		//
		//		Maths::appendNormal( vBuff[ indexX1Y0 ].normal  , vBuff[ indexX1Y0 ].position,
		//							 vBuff[ indexX0Y1 ].position, vBuff[ indexX1Y1 ].position );
		//	}
		//}


		/**/
		// -----------------------------------------------------------------------------------
		// Mesh Indices Size and set-up memory space
		// -----------------------------------------------------------------------------------
		const int indicesSize = ( width - 1 ) * ( depth - 1 ) * 6;
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( indicesSize );

		// -----------------------------------------------------------------------------------
		// Calculate mesh index points and tangents
		// -----------------------------------------------------------------------------------
		index = 0;
		for( int d = 0; d < depth - 1; d++ )
		{
			for( int w = 0; w < width - 1; w++ )
			{
				const int curVertex = w + ( d * width );

				iBuff[ index ] = curVertex;
				iBuff[ index + 1 ] = curVertex + width;
				iBuff[ index + 2 ] = curVertex + 1;

				iBuff[ index + 3 ] = curVertex + 1;
				iBuff[ index + 5 ] = curVertex + width + 1;
				iBuff[ index + 4 ] = curVertex + width;


				Maths::calculateTangents(
					vBuff[ iBuff[ index + 0 ] ].tangent,   vBuff[ iBuff[ index + 0 ] ].binormal,
					vBuff[ iBuff[ index + 0 ] ].position,  vBuff[ iBuff[ index + 1 ] ].position,  vBuff[ iBuff[ index + 2 ] ].position,
					vBuff[ iBuff[ index + 0 ] ].textureUV, vBuff[ iBuff[ index + 1 ] ].textureUV, vBuff[ iBuff[ index + 2 ] ].textureUV
				);

				vBuff[ iBuff[ index + 2 ] ].tangent  = vBuff[ iBuff[ index + 1 ] ].tangent = vBuff[ iBuff[ index + 0 ] ].tangent;
				vBuff[ iBuff[ index + 2 ] ].binormal = vBuff[ iBuff[ index + 1 ] ].binormal = vBuff[ iBuff[ index + 0 ] ].binormal;

				Maths::calculateTangents(
					vBuff[ iBuff[ index + 3 ] ].tangent,   vBuff[ iBuff[ index + 3 ] ].binormal,
					vBuff[ iBuff[ index + 3 ] ].position,  vBuff[ iBuff[ index + 4 ] ].position,  vBuff[ iBuff[ index + 5 ] ].position,
					vBuff[ iBuff[ index + 3 ] ].textureUV, vBuff[ iBuff[ index + 4 ] ].textureUV, vBuff[ iBuff[ index + 5 ] ].textureUV
				);

				vBuff[ iBuff[ index + 5 ] ].tangent  = vBuff[ iBuff[ index + 4 ] ].tangent = vBuff[ iBuff[ index + 3 ] ].tangent;
				vBuff[ iBuff[ index + 5 ] ].binormal = vBuff[ iBuff[ index + 4 ] ].binormal = vBuff[ iBuff[ index + 3 ] ].binormal;

				index += 6;
			}
		}

		for( int i = 0; i < iBuff.size( ); i += 3 )
		{
			const int i0 = iBuff[ i + 0 ];
			const int i1 = iBuff[ i + 1 ];
			const int i2 = iBuff[ i + 2 ];

			const vec3 vector1 = vBuff[ i0 ].position - vBuff[ i1 ].position;
			const vec3 vector2 = vBuff[ i0 ].position - vBuff[ i2 ].position;

			const vec3 normal = cross( vector1, vector2 );

			vBuff[ i0 ].normal += normal;
			vBuff[ i1 ].normal += normal;
			vBuff[ i2 ].normal += normal;
		}

		return Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleList );
		//*/

		/**
		// -----------------------------------------------------------------------------------
		// Mesh Indices Size and set-up memory space
		// -----------------------------------------------------------------------------------
		int indicesSize = ( ( ( width ) * 2 ) * ( depth - 1 ) ) + ( depth - 2 );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( indicesSize );

		// -----------------------------------------------------------------------------------
		// Calculate mesh index points and tangents
		// -----------------------------------------------------------------------------------
		int index = 0;
		for( int indiceDepth = 0; indiceDepth < depth - 1; indiceDepth++ )
		{
			// -----------------------------------------------------------------------------------
			// Even Rows
			// -----------------------------------------------------------------------------------
			if( indiceDepth % 2 == 0 )
			{
				for( int indiceWidth = 0; indiceWidth < width; indiceWidth++ )
				{
					iBuff[ index ] = indiceWidth + ( indiceDepth * width );
					index++;
					iBuff[ index ] = iBuff[ index - 1 ] + width;
					index++;

					// -----------------------------------------------------------------------------------
					// Triangle Rotation Correction
					// -----------------------------------------------------------------------------------
					if( indiceWidth == width - 1 && indiceDepth < depth - 2 )
					{
						iBuff[ index ] = iBuff[ index - 1 ];
						index++;
					}
				}
			}
			// -----------------------------------------------------------------------------------
			// Odd Rows
			// -----------------------------------------------------------------------------------
			else
			{
				for( int indiceWidth = width - 1; indiceWidth >= 0; indiceWidth-- )
				{
					iBuff[ index ] = indiceWidth + ( indiceDepth * width );
					index++;
					iBuff[ index ] = iBuff[ index - 1 ] + width;
					index++;

					// -----------------------------------------------------------------------------------
					// Triangle Rotation Correction
					// -----------------------------------------------------------------------------------
					if( indiceWidth == 0 && indiceDepth < depth - 2 )
					{
						iBuff[ index ] = iBuff[ index - 1 ];
						index++;
					}
				}
			}
		}

		return Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleStrip );
		//*/
	}

	/// <summary> Test wall mesh. </summary>
	/// <param name="width"> The width of the wall. </param>
	/// <param name="height"> The height of the wall. </param>
	/// <returns> A test mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::wall( float width, float height )
	{
		const vec3 pX(  1,  0,  0 );
		const vec3 nX( -1,  0,  0 );
		const vec3 pY(  0,  1,  0 );
		const vec3 nY(  0, -1,  0 );
		const vec3 pZ(  0,  0,  1 );
		const vec3 nZ(  0,  0, -1 );

		const vec3 _XYZ(  0.5f, height,  0.5f );
		const vec3 _xYZ( -0.5f, height,  0.5f );
		const vec3 _xYz( -0.5f, height, -0.5f );
		const vec3 _XYz(  0.5f, height, -0.5f );

		const vec3 _XyZ(  0.5f, 0.0f,  0.5f );
		const vec3 _xyZ( -0.5f, 0.0f,  0.5f );
		const vec3 _xyz( -0.5f, 0.0f, -0.5f );
		const vec3 _Xyz(  0.5f, 0.0f, -0.5f );

		const vec2 _uv( 0, 0 );
		const vec2 _uV( 0, 1 );
		const vec2 _UV( 1, 1 );
		const vec2 _Uv( 1, 0 );

		const vec3 empty( 0, 0, 0 );

		const vec3 offset1Y( 0, 0.2, 0 );
		const vec3 offset2Y( 0, 0.1, 0 );
		const vec3 offset1X( 0.30, 0, 0 );
		const vec3 offset2X( 0.25, 0, 0 );
		const vec3 offset  ( 0.5f * width, 0, 0 );

		const vec2 scaleUV( 1, height );

		const int faces = 13;

		const float floorWidth = 0.5f - ( 0.5f * width );


		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vertices( faces * 4 );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  indecies( faces * 6 );


		const vec2 lowerLip( 0.1f, 0.1f );
		const vec2 upperLip( 0.1f, 0.1f );

		const float heights[ ] = {
			0,
			0.1f,
			height - 0.1f,
			height
		};

		const float widths[ ] = {
			width * 0.5f + 0.1f,
			width * 0.5f,
			width * 0.5f + 0.1f,
			0.5f
		};

		// Wall Top
		vertices[  0 ] = { {  widths[ 0 ], heights[ 3 ],  0.5 }, {  widths[ 0 ],  0.5 }, pY, empty, empty };
		vertices[  1 ] = { {  widths[ 0 ], heights[ 3 ], -0.5 }, {  widths[ 0 ], -0.5 }, pY, empty, empty };
		vertices[  2 ] = { { -widths[ 0 ], heights[ 3 ], -0.5 }, { -widths[ 0 ], -0.5 }, pY, empty, empty };
		vertices[  3 ] = { { -widths[ 0 ], heights[ 3 ],  0.5 }, { -widths[ 0 ],  0.5 }, pY, empty, empty };

		// Floor Segments
		vertices[  4 ] = { {  widths[ 3 ], heights[ 0 ],  0.5 }, {  widths[ 3 ],  0.5 }, pY, empty, empty };
		vertices[  5 ] = { {  widths[ 3 ], heights[ 0 ], -0.5 }, {  widths[ 3 ], -0.5 }, pY, empty, empty };
		vertices[  6 ] = { {  widths[ 2 ], heights[ 0 ], -0.5 }, {  widths[ 2 ], -0.5 }, pY, empty, empty };
		vertices[  7 ] = { {  widths[ 2 ], heights[ 0 ],  0.5 }, {  widths[ 2 ],  0.5 }, pY, empty, empty };

		vertices[  8 ] = { { -widths[ 2 ], heights[ 0 ],  0.5 }, { -widths[ 2 ],  0.5 }, pY, empty, empty };
		vertices[  9 ] = { { -widths[ 2 ], heights[ 0 ], -0.5 }, { -widths[ 2 ], -0.5 }, pY, empty, empty };
		vertices[ 10 ] = { { -widths[ 3 ], heights[ 0 ], -0.5 }, { -widths[ 3 ], -0.5 }, pY, empty, empty };
		vertices[ 11 ] = { { -widths[ 3 ], heights[ 0 ],  0.5 }, { -widths[ 3 ],  0.5 }, pY, empty, empty };


		// Bottom Lip Tops
		vertices[ 12 ] = { {  widths[ 2 ], heights[ 1 ],  0.5 }, {  widths[ 2 ],  0.5 }, pY, empty, empty };
		vertices[ 13 ] = { {  widths[ 2 ], heights[ 1 ], -0.5 }, {  widths[ 2 ], -0.5 }, pY, empty, empty };
		vertices[ 14 ] = { {  widths[ 1 ], heights[ 1 ], -0.5 }, {  widths[ 1 ], -0.5 }, pY, empty, empty };
		vertices[ 15 ] = { {  widths[ 1 ], heights[ 1 ],  0.5 }, {  widths[ 1 ],  0.5 }, pY, empty, empty };

		vertices[ 16 ] = { { -widths[ 1 ], heights[ 1 ],  0.5 }, { -widths[ 1 ],  0.5 }, pY, empty, empty };
		vertices[ 17 ] = { { -widths[ 1 ], heights[ 1 ], -0.5 }, { -widths[ 1 ], -0.5 }, pY, empty, empty };
		vertices[ 18 ] = { { -widths[ 2 ], heights[ 1 ], -0.5 }, { -widths[ 2 ], -0.5 }, pY, empty, empty };
		vertices[ 19 ] = { { -widths[ 2 ], heights[ 1 ],  0.5 }, { -widths[ 2 ],  0.5 }, pY, empty, empty };


		// Bottom Lip Sides
		vertices[ 20 ] = { { -widths[ 0 ], heights[ 1 ],  0.5 }, { heights[ 1 ],  0.5 }, nX, empty, empty };
		vertices[ 21 ] = { { -widths[ 0 ], heights[ 1 ], -0.5 }, { heights[ 1 ], -0.5 }, nX, empty, empty };
		vertices[ 22 ] = { { -widths[ 0 ], heights[ 0 ], -0.5 }, { heights[ 0 ], -0.5 }, nX, empty, empty };
		vertices[ 23 ] = { { -widths[ 0 ], heights[ 0 ],  0.5 }, { heights[ 0 ],  0.5 }, nX, empty, empty };

		vertices[ 24 ] = { {  widths[ 0 ], heights[ 0 ],  0.5 }, { heights[ 0 ],  0.5 }, pX, empty, empty };
		vertices[ 25 ] = { {  widths[ 0 ], heights[ 0 ], -0.5 }, { heights[ 0 ], -0.5 }, pX, empty, empty };
		vertices[ 26 ] = { {  widths[ 0 ], heights[ 1 ], -0.5 }, { heights[ 1 ], -0.5 }, pX, empty, empty };
		vertices[ 27 ] = { {  widths[ 0 ], heights[ 1 ],  0.5 }, { heights[ 1 ],  0.5 }, pX, empty, empty };


		// Top Lip Bottom
		vertices[ 28 ] = { {  widths[ 1 ], heights[ 2 ],  0.5 }, { widths[ 1 ],  0.5 }, nY, empty, empty };
		vertices[ 29 ] = { {  widths[ 1 ], heights[ 2 ], -0.5 }, { widths[ 1 ], -0.5 }, nY, empty, empty };
		vertices[ 30 ] = { {  widths[ 2 ], heights[ 2 ], -0.5 }, { widths[ 2 ], -0.5 }, nY, empty, empty };
		vertices[ 31 ] = { {  widths[ 2 ], heights[ 2 ],  0.5 }, { widths[ 2 ],  0.5 }, nY, empty, empty };

		vertices[ 32 ] = { { -widths[ 2 ], heights[ 2 ],  0.5 }, { -widths[ 2 ],  0.5 }, nY, empty, empty };
		vertices[ 33 ] = { { -widths[ 2 ], heights[ 2 ], -0.5 }, { -widths[ 2 ], -0.5 }, nY, empty, empty };
		vertices[ 34 ] = { { -widths[ 1 ], heights[ 2 ], -0.5 }, { -widths[ 1 ], -0.5 }, nY, empty, empty };
		vertices[ 35 ] = { { -widths[ 1 ], heights[ 2 ],  0.5 }, { -widths[ 1 ],  0.5 }, nY, empty, empty };


		// Top Lip Sides
		vertices[ 36 ] = { { -widths[ 0 ], heights[ 3 ],  0.5 }, { heights[ 3 ],  0.5 }, nX, empty, empty };
		vertices[ 37 ] = { { -widths[ 0 ], heights[ 3 ], -0.5 }, { heights[ 3 ], -0.5 }, nX, empty, empty };
		vertices[ 38 ] = { { -widths[ 0 ], heights[ 2 ], -0.5 }, { heights[ 2 ], -0.5 }, nX, empty, empty };
		vertices[ 39 ] = { { -widths[ 0 ], heights[ 2 ],  0.5 }, { heights[ 2 ],  0.5 }, nX, empty, empty };

		vertices[ 40 ] = { {  widths[ 0 ], heights[ 2 ],  0.5 }, { heights[ 2 ],  0.5 }, pX, empty, empty };
		vertices[ 41 ] = { {  widths[ 0 ], heights[ 2 ], -0.5 }, { heights[ 2 ], -0.5 }, pX, empty, empty };
		vertices[ 42 ] = { {  widths[ 0 ], heights[ 3 ], -0.5 }, { heights[ 3 ], -0.5 }, pX, empty, empty };
		vertices[ 43 ] = { {  widths[ 0 ], heights[ 3 ],  0.5 }, { heights[ 3 ],  0.5 }, pX, empty, empty };


		// Wall Sides
		vertices[ 44 ] = { { -widths[ 1 ], heights[ 2 ],  0.5 }, { heights[ 2 ],  0.5 }, nX, empty, empty };
		vertices[ 45 ] = { { -widths[ 1 ], heights[ 2 ], -0.5 }, { heights[ 2 ], -0.5 }, nX, empty, empty };
		vertices[ 46 ] = { { -widths[ 1 ], heights[ 1 ], -0.5 }, { heights[ 1 ], -0.5 }, nX, empty, empty };
		vertices[ 47 ] = { { -widths[ 1 ], heights[ 1 ],  0.5 }, { heights[ 1 ],  0.5 }, nX, empty, empty };

		vertices[ 48 ] = { {  widths[ 1 ], heights[ 1 ],  0.5 }, { heights[ 1 ],  0.5 }, pX, empty, empty };
		vertices[ 49 ] = { {  widths[ 1 ], heights[ 1 ], -0.5 }, { heights[ 1 ], -0.5 }, pX, empty, empty };
		vertices[ 50 ] = { {  widths[ 1 ], heights[ 2 ], -0.5 }, { heights[ 2 ], -0.5 }, pX, empty, empty };
		vertices[ 51 ] = { {  widths[ 1 ], heights[ 2 ],  0.5 }, { heights[ 2 ],  0.5 }, pX, empty, empty };

		for( int i = 0, j = 0; i < indecies.size( ); i += 6, j += 4 )
		{
			indecies[ i + 0 ] = j + 0;
			indecies[ i + 1 ] = j + 1;
			indecies[ i + 2 ] = j + 3;

			indecies[ i + 3 ] = j + 1;
			indecies[ i + 4 ] = j + 2;
			indecies[ i + 5 ] = j + 3;
		}

		for( int i = 0; i < vertices.size( ); i += 4 )
		{
			Maths::calculateTangents(
				vertices[ i + 0 ].tangent  , vertices[ i + 0 ].binormal,
				vertices[ i + 0 ].position , vertices[ i + 1 ].position , vertices[ i + 2 ].position,
				vertices[ i + 0 ].textureUV, vertices[ i + 1 ].textureUV, vertices[ i + 2 ].textureUV
			);

			vertices[ i + 3 ].tangent  = vertices[ i + 2 ].tangent  = vertices[ i + 1 ].tangent  = vertices[ i + 0 ].tangent;
			vertices[ i + 3 ].binormal = vertices[ i + 2 ].binormal = vertices[ i + 1 ].binormal = vertices[ i + 0 ].binormal;
		}


		return Mesh< Vertex3f2f3f3f3f::Vertex >( vertices, indecies, MeshPrimitive::TriangleList );
	}



	/// <summary> Create a grid mesh. </summary>
	/// <param name="faces"> The number of faces in the cylinder. </param>
	/// <param name="slices"> The number of slices in the cylinder. </param>
	/// <param name="length"> The length of the cylinder. </param>
	/// <param name="radius"> A function that will provide a radius offset based on a vertex segment and slice. </param>
	/// <returns> A new grid mesh. </returns>
	Mesh< Vertex3f2f3f3f3f::Vertex > Shapes::cylinder( int faces, int slices, float length, const std::function< float ( int, int ) >& radius )
	{
		faces  = std::max(  faces + 1, 4 );
		slices = std::max( slices + 1, 2 );
		Mesh< Vertex3f2f3f3f3f::Vertex > mesh = grid( slices, faces, 1.0f, []( int ){ return 0.0f; } );

		// -----------------------------------------------------------------------------------
		// Calculate mesh points
		// -----------------------------------------------------------------------------------
		Mesh< Vertex3f2f3f3f3f::Vertex >::VertexBuffer vBuff( mesh.getVertexBuffer( ).size( ) + 2 );
		Mesh< Vertex3f2f3f3f3f::Vertex >::IndexBuffer  iBuff( mesh.getIndexBuffer( ).size( ) + ( faces - 1 ) * 6 );

		int index;
		float theta = glm::two_pi< float >( ) / ( faces - 1 );
		float h = length / ( slices - 1 );

		vBuff[ vBuff.size( ) - 2 ].position = vec3( 0,  0, 0 );
		vBuff[ vBuff.size( ) - 2 ].normal   = vec3( 0, -1, 0 );

		vBuff.back( ).position = vec3( 0, length, 0 );
		vBuff.back( ).normal   = vec3( 0, 1, 0 );

		for( int w = 0; w < faces; w++ )
		{
			for( int d = 0; d < slices; d++ )
			{
				index = d + ( w * slices );

				float r = radius( w, d );

				float rD = d > 1 ? radius( w, d - 1 ) : r;
				float rU = d < slices - 1 ? radius( w, d + 1 ) : r;

				float yD = d > 1          ? ( d - 1 ) * h : d * h;
				float yU = d < slices - 1 ? ( d + 1 ) * h : d * h;

				vBuff[ index ].normal.x = sin( theta * w );
				vBuff[ index ].normal.z = cos( theta * w );
				vBuff[ index ].normal.y = - ( rU - rD ) / ( yU - yD );

				vBuff[ index ].position.x = vBuff[ index ].normal.x * r;
				vBuff[ index ].position.z = vBuff[ index ].normal.z * r;
				vBuff[ index ].position.y = d * h;

				vBuff[ index ].normal = normalize( vBuff[ index ].normal );
				vBuff[ index ].tangent  = mesh.getVertexBuffer( )[ index ].tangent;
				vBuff[ index ].binormal = mesh.getVertexBuffer( )[ index ].binormal;
			}
		}

		for( size_t i = 0; i < mesh.getIndexBuffer( ).size( ); ++i )
		{
			iBuff[ i ] = mesh.getIndexBuffer( )[ i ];
		}

		int indexOffset = static_cast< int >( mesh.getIndexBuffer( ).size( ) );
		unsigned int facesOffset = ( faces - 1 ) * 3;

		for( unsigned int i = 0, j = 0; i < facesOffset; i += 3, ++j )
		{
			int index = indexOffset + i;

			iBuff[               index + 1 ] =   j       * slices;
			iBuff[               index     ] = ( j + 1 ) * slices;
			iBuff[               index + 2 ] = static_cast< unsigned int >( vBuff.size( ) ) - 2;

			iBuff[ facesOffset + index     ] =   j       * slices + slices - 1;
			iBuff[ facesOffset + index + 1 ] = ( j + 1 ) * slices + slices - 1;
			iBuff[ facesOffset + index + 2 ] = static_cast< unsigned int >( vBuff.size( ) ) - 1;
		}

		return Mesh< Vertex3f2f3f3f3f::Vertex >( vBuff, iBuff, MeshPrimitive::TriangleList );
	}
} // fx
} // namespace visual
